<?php


session_start();
require('condb.php');


$sqlfifth_score4tea = "SELECT * FROM `fifth_score4tea` 
INNER JOIN `login` ON fifth_score4tea.ID=login.ID
WHERE fifth_score4tea.fifth_score4tea_code='A' 
AND fifth_score4tea.fifth_score4tea_data LIKE '%79.%' 
AND login.occup_code LIKE 'OcAA%'
AND fifth_score4tea.ID=5269 ";



echo mysqli_num_rows($reFS4T = mysqli_query($con, $sqlfifth_score4tea)).'<br>';

$reFS4T = mysqli_query($con, $sqlfifth_score4tea);



while($rowFS4T = mysqli_fetch_array($reFS4T)) {

	$fifth_score4tea_data = $rowFS4T['fifth_score4tea_data'];

	// หมวดครองตน NOTE: มี9ตัวบ่งชี้
	for($x=1; $x<=38; $x++) {
		$i = strpos($fifth_score4tea_data, '.'); // Find position of each score
		$j = substr($fifth_score4tea_data, $i+1, 1); // Return value of each score

		$score[$x] = $j; // Set valuable for each score
		$selfscore += $score[$x]; // Accumulate all score
		
		if($x>=1 && $x<=5) { // ตัวบ่งชี้ที่1: รัก เข้าใจ ห่วงใยลูกศิษย์
			$selfscore_sub1 += $score[$x];
		}elseif($x>=6 && $x<=10) { //ตัวบ่งชี้ที่2: หน้าที่ครูต่อศิษย์ตามหลักทิศ 6
			$selfscore_sub2 += $score[$x];
		}elseif($x>=11 && $x<=14) { //ตัวบ่งชี้ที่3: ไม่สร้างหนี้เพิ่ม
			$selfscore_sub3 += $score[$x];
		}elseif($x>=15 && $x<=18) { //ตัวบ่งชี้ที่4: จิตสาธารณะ
			$selfscore_sub4 += $score[$x];
		}elseif($x>=19 && $x<=26) { //ตัวบ่งชี้ที่5: ปฏิบัติตนอยู่ในศีล 5 และความดีสากล 5
			$selfscore_sub5 += $score[$x];
		}elseif($x>=27 && $x<=30) { //ตัวบ่งชี้ที่6: มีความรับผิดชอบ พูดอย่างไรทำอย่างนั้น ทำอย่างไรพูดอย่างนั้น
			$selfscore_sub6 += $score[$x];
		}elseif($x>=31 && $x<=33) { //ตัวบ่งชี้ที่7: เรียนรู้ตลอดชีวิต
			$selfscore_sub7 += $score[$x];
		}elseif($x>=34 && $x<=36) { //ตัวบ่งชี้ที่8: รู้เท่าทันตนเอง
			$selfscore_sub8 += $score[$x];
		}elseif($x>=37 && $x<=38) { //ตัวบ่งชี้ที่9: ละอายและเกรงกลัวต่อบาป
			$selfscore_sub9 += $score[$x];
		}
		
		$fifth_score4tea_data = substr($fifth_score4tea_data, $i+1);
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// หมวดครองคน NOTE: มี3ตัวบ่งชี้
	for($x=39; $x<=49; $x++) {
		$i = strpos($fifth_score4tea_data, '.'); // Find position of each score
		$j = substr($fifth_score4tea_data, $i+1, 1); // Return value of each score

		$score[$x] = $j; // Set valuable for each score
		$peoplescore += $score[$x]; // Accumulate all score

		if($x>=39 && $x<=41) { //ตัวบ่งชี้ที่10: เป็นผู้หวังดีให้ด้วยจิตเมตตา
			$peoplescore_sub10 += $score[$x];
		}elseif($x>=42 && $x<=44) { //ตัวบ่งชี้ที่11: ปฏิบัติตนตามแบบแผน และปรับแนวคิดให้ถูกต้องตรงกัน
			$peoplescore_sub11 += $score[$x];
		}elseif($x>=45 && $x<=49) { //ตัวบ่งชี้ที่12: ปฏิบัติตนกับบุคคลรอบข้างอย่างเหมาะสม
			$peoplescore_sub12 += $score[$x];
		}

		$fifth_score4tea_data = substr($fifth_score4tea_data, $i+1);
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// หมวดครองงาน NOTE: มี7ตัวบ่งชี้
	for($x=50; $x<=79; $x++) {
		$i = strpos($fifth_score4tea_data, '.'); // Find position of each score
		$j = substr($fifth_score4tea_data, $i+1, 1); // Return value of each score

		$score[$x] = $j; // Set valuable for each score
		$workscore += $score[$x]; // Accumulate all score
		
		if($x>=50 && $x<=52) { //ตัวบ่งชี้ที่13: มีความคิดสร้างสรรค์ (Creativity)
			$workscore_sub13 += $score[$x];
		}elseif($x>=53 && $x<=58) { //ตัวบ่งชี้ที่14: มีการคิดวิเคราะห์ (Critical Thinking)
			$workscore_sub14 += $score[$x];
		}elseif($x>=59 && $x<=62) { //ตัวบ่งชี้ที่15: มีทักษะด้านความร่วมมือและการทำงานเป็นทีม (Collaboration and Teamwork)
			$workscore_sub15 += $score[$x];
		}elseif($x>=63 && $x<=65) { //ตัวบ่งชี้ที่16: มีทักษะสื่อสาร (Communication)
			$workscore_sub16 += $score[$x];
		}elseif($x>=66 && $x<=68) { //ตัวบ่งชี้ที่17: รู้คอมพิวเตอร์และไอซีที (Computing and ICT Literacy)
			$workscore_sub17 += $score[$x];
		}elseif($x>=69 && $x<=76) { //ตัวบ่งชี้ที่18: มีทักษะอาชีพและทักษะการเรียนรู้ (Career and Learning Skill)
			$workscore_sub18 += $score[$x];
		}elseif($x>=77 && $x<=79) { //ตัวบ่งชี้ที่19: ผสมผสานทางวัฒนธรรม (Cross-cultural)
			$workscore_sub19 += $score[$x];
		}
		$fifth_score4tea_data = substr($fifth_score4tea_data, $i+1);
	}

	// สรุปคะแนนรวม แยกเป็นรายหมวด
	$selfscore_avg = number_format($selfscore/38, 2);
	$peoplescore_avg = number_format($peoplescore/11, 2);
	$workscore_avg = number_format($workscore/30, 2);




	$fifth_score4tea_detail = $selfscore_avg.','.$peoplescore_avg.','.$workscore_avg;
	$ID = $rowFS4T['ID'];

	echo $ID.' = '.$fifth_score4tea_detail.'<br>';

	

	$sql = "UPDATE `fifth_score4tea` SET fifth_score4tea_detail='$fifth_score4tea_detail' WHERE ID='$ID' AND fifth_score4tea_code='A' ";
	$re = $con->query($sql) or die($con->error); //Check error


	$selfscore = 0;
	$peoplescore = 0;
	$workscore = 0;

}





?>