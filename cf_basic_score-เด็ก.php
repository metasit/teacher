<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	date_default_timezone_set("Asia/Bangkok");
	$basic_score_doc_date = date('Y-m-d H:i:s');

	if($_GET['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ') { // สำหรับเด็กที่โดนปฏิเสธมา ให้เข้า loop นี้
		$sql = "UPDATE `login` SET basic_score_status='ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ', basic_score_doc_date='$basic_score_doc_date' WHERE ID='$ID' ";
		$re = mysqli_query($con,$sql);
		$row = mysqli_fetch_array($re);
		
		/* Log User Action */
		$basic_score_remark = $_GET['basic_score_remark'];
		$sqllog="INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID','กดให้เจ้าหน้าที่ตรวจสอบ','เด็ก,ขั้นพื้นฐาน,โดนปฏิเสธมา,$basic_score_remark') ";
		$relog = $con->query($sqllog) or die($con->error); //Check error

	}else{ // สำหรับครั้งแรกที่กดปุ่มให้ครูที่ปรึกษาตรวจสอบ จะเข้า Loop ทั่วไป ก็คือ Loop นี้
		$sql = "UPDATE `login` SET basic_score_status='ยืนยันแล้ว/กำลังตรวจสอบ', basic_score_doc_date='$basic_score_doc_date' WHERE ID='$ID' ";
		$re = mysqli_query($con,$sql);
		$row = mysqli_fetch_array($re);
		
		/* Log User Action */
		$sqllog="INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID','กดให้เจ้าหน้าที่ตรวจสอบ','เด็ก,ขั้นพื้นฐาน') ";
		$relog = $con->query($sqllog) or die($con->error); //Check error
	}
	
	header('location: registZidol.php');

?>