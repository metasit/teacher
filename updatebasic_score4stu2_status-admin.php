

<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");

	$ID = $_GET['ID'];
	$basic_score_status = $_GET['basic_score_status'];
	$winScroll = $_GET['winScroll'];
	$page = $_GET['page'];
	$basic_score_date = date("Y-m-d H:i:s");

	$ID_ad = $_SESSION['ID'];

	$sqlcheck = "SELECT * FROM `login` WHERE ID='$ID' ";
	$recheck = mysqli_query($con, $sqlcheck);
	$rowcheck = mysqli_fetch_array($recheck);

	if($basic_score_status == 'ปฏิเสธ') {
		$basic_score_remark = $rowcheck['basic_score_remark']; // เหตุผลที่ปฏิเสธ

		if(is_null($basic_score_remark) || empty($basic_score_remark)) {
			echo '<script>';
				echo "alert('กรุณาใส่เหตุผลที่ปฏิเสธที่ช่องหมายเหตุด้วยค่ะ ข้อความตรงนี้จะไปแสดงให้นักเรียนรับทราบเพื่อดำเนินการแก้ไข และแนบเอกสารเข้ามาใหม่ค่ะ');";
				echo "window.location.replace('ระบบหลังบ้านเด็กพื้นฐาน-มีครูที่ปรึกษา.php?winScroll=$winScroll&page=$page')";
			echo '</script>';
		}else{
			/* Update Status */
			$sql = "UPDATE `login` SET basic_score_status='ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ' WHERE ID='$ID' ";
			$res = $con->query($sql) or die($con->error);

			/* Log Admin Action */
			$sqllog = "INSERT INTO `adscorelog` (`ID_ad`,`adscorelog_task`,`ID_user`,`adscorelog_detail`) VALUES ('$ID_ad','ปฏิเสธ','$ID','เด็ก,ขั้นพื้นฐาน,$basic_score_remark') ";
			$relog = $con->query($sqllog) or die($con->error);

			header('location: ระบบหลังบ้านเด็กพื้นฐาน-มีครูที่ปรึกษา.php?winScroll='.$winScroll.'&page='.$page);
		}
	}else{		
		/* Update Status */
		$sql = "UPDATE `login` SET basic_score_status='ยืนยันแล้ว/Approve แล้ว', basic_score_date='$basic_score_date' WHERE ID='$ID' ";
		$res= $con->query($sql) or die($con->error);
		/* Log Admin Action */
		$sqllog = "INSERT INTO `adscorelog` (`ID_ad`,`adscorelog_task`,`ID_user`,`adscorelog_detail`) VALUES ('$ID_ad','Approve แล้ว','$ID','เด็ก,ขั้นพื้นฐาน') ";
		$relog = $con->query($sqllog) or die($con->error);
		
		header('location: ระบบหลังบ้านเด็กพื้นฐาน-มีครูที่ปรึกษา.php?winScroll='.$winScroll.'&page='.$page);
	}
?>