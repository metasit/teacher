<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start NoAccount Session -->
<?php
      if (empty($_SESSION['email']))
      {
?>
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
          <li class="faicon-login"><a href="login-en.php"><i class="fas fa-user-lock"></i> <text1>Log in</text1></a></li>
          <li class="faicon-login"><a href="signup-en.php"><i class="fas fa-file-signature"></i> <text1>Sign up</text1></a></li>
          <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
            <ul>
              <li><a href="โครงการอาสาของแผ่นดิน.php">ภาษาไทย</a></li>
              <!--<li><a href="#">Chinese</a></li>-->
            </ul>
          </li>
          <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
              <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
        </ul>
      </nav>
      <!-- End NoAccount Session -->
      <!-- ################################################################################################ -->
      <!-- Start Admin Session -->
<?php
      }
      elseif ($_SESSION['email'])
      {
        if ($_SESSION["level"]=="admin")
        {
?>
        <nav id="mainav2" class="PreMenu_fl_right">
          <ul class="PreMenufaico2">
            <li style="background-color:rgb(228,0,0);"><i class="fas fa-user-edit"></i> Admin</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการอาสาของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Admin Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member General Session -->
<?php
        if ($_SESSION["level"]=="memberGeneral")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการอาสาของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member General Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member SILVER Session -->
<?php
        if ($_SESSION["level"]=="memberSilver")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> Silver Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการอาสาของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member SILVER Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member GOLD Session -->
<?php
        if ($_SESSION["level"]=="memberGold")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> Gold Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการอาสาของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member GOLD Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member DIAMOND Session -->
<?php
        if ($_SESSION["level"]=="memberDiamond")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> Diamond Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการอาสาของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
      }
?>
      <!-- End Member DIAMOND Session -->
      <!-- ################################################################################################ -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index-en.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index-en.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index-en.php">Home</a></li>
          <li><a href="เกี่ยวกับเรา-en.php">About us</a></li>
          <li class="joinus-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>Join us</a>
            <ul>
              <li><a href="ร่วมโครงการ-en.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <li><a href="โครงการครูดีของแผ่นดิน-en.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา-en.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA-en.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน-en.php">โครงการอาสาของแผ่นดิน</a></li>
            </ul>
          </li>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">Innovation Library</a></li>
          <li><a href="ติดต่อเรา-en.php">Contact us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="ร่วมโครงการ-en.php"> Join Us</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> โครงการอาสาของแผ่นดิน</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - โครงการอาสาของแผ่นดิน -->
<div class="wrapper row3">
  <main class="hoc container3 clear">
    <article style="text-align:center;">
      <!-- ################################################################################################ -->
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px"><strong>อาสาของแผ่นดิน</strong></p>
        <img src="images/อาสาของแผ่นดินBG.jpg" alt="อาสาของแผ่นดินlogo">
      <p class="fs-32" style="font-family:RSUText; line-height:30pt; text-align:left;">
        สังคมไทยในอดีตเป็นสังคมที่คนมีความเอื้อเฟื้อแบ่งปัน มีน้ำใจโอบอ้อมอารี รักการให้เมตตาเอื้ออาทรซึ่งกันและกัน หล่อหลอมในวิถีชีวิตและจิตวิญญาณมาตั้งแต่ครั้งอดีต จนกลายเป็นเอกลักษณ์ทางวัฒนธรรมของคนไทย ในเรื่องของการต้อนรับขับสู้แขกผู้มาเยือนด้วยรอยยิ้มเปี่ยมมิตรไมตรีเสมอ จนได้รับขนานนามไปทั่วโลกว่า <strong>“สยามเมืองยิ้ม”</strong> 
        <br><br>
        ทว่าในรอบยี่สิบปีที่ผ่านมา ตั้งแต่ปลายศตวรรษที่ 19 โลกเข้าสู่ยุคทุนนิยมโลกาภิวัฒน์ ทั้งทุนการเงิน ทุนอุตสาหกรรม และการสื่อสารไร้พรมแดน เกิดการแข่งขันเรื่องความก้าวหน้าทางวิทยาศาสตร์และเทคโนโลยี ทำให้ขณะนี้โลกได้พัฒนาไปเรื่อย ๆ อย่างไม่หยุดยั้ง เพราะการต่อสู้ของประชาชนที่ต้องการชีวิตที่ดีกว่า ชีวิตในโลกจินตนาการ ปรารถนาโลกอนาคตที่ดีกว่าดีที่สุด แต่สิ่งที่ตามมาคือสงครามและการค้าซึ่งทวีความรุนแรงขึ้นเรื่อยๆ สภาพสังคมเปลี่ยนไป กลายเป็นสังคมที่ยื้อแย่งแข่งขันสูง น้ำใจและรอยยิ้มเริ่มเลือนหายไปจากใบหน้าผู้คน ความเซ็ง เศร้า เหงา เครียดรุมเร้า เป็นสังคมโดดเดี่ยวมากขึ้น ไม่สามัคคีกันดังเดิม นับวันจะกลายเป็นปัญหาทับซ้อนที่ยากต่อการเยียวยาแก้ไข
        <br><br>
        ความเปลี่ยนแปลงที่เราเห็นได้ชัดเจนในสังคมไทยในยุคปัจจุบัน เป็นไปตามกฏสัจธรรมของความแปรผัน ยิ่งความเจริญก้าวหน้าทางเทคโนโลยีเพิ่มมากขึ้นเท่าไหร่ ย่อมแปรผกผันกับความเสื่อมถอยทางศีลธรรมมากขึ้นเท่านั้น สมกับยุคกึ่งพุทธกาลที่มนุษย์มีกิเลสเฟื่องฟู ดังจะเห็นตัวอย่างมากมายตามหน้าหนังสือพิมพ์ ปัญหาอาชญากรรม ปัญหายาเสพติด ปัญหาเด็กและเยาวชน ปัญหาเศรษฐกิจและสังคมทุกระดับชั้น ล 
        <br><br>
        ล่าสุดเมื่อวันที่ 8 กุมภาพันธ์ พ.ศ.2563 ที่ผ่านมา ซึ่งตรงกับวันมาฆบูชา ประเทศไทยเกิดข่าวใหญ่ เหตุกราดยิงที่โคราช เป็นข่าวช็อคโลกอย่างคาดไม่ถึง เหตุโศกนาฏกรรมนี้มีผู้บาดเจ็บล้มตายรวมแล้วกว่า 88 คน กว่าจะจัดการปัญหาได้ก็ผ่านไปถึง 17 ชั่วโมง ข้ามไปอีกหนึ่งวันคือ คือวันที่ 9 กุมภาพันธ์ จนผู้นำประเทศต่างๆ ต้องออกมาแสดงความเสียใจกับประเทศไทย เหตุโศกนาฏกรรมนี้ยังไม่รวม Top 10 อื่นๆ ที่ประเทศไทยติดโผอยู่หลายตำแหน่ง เช่น คนไทยดื่มเหล้ามากที่สุดเป็นอันดับ 5 ของโลก เผยเด็กไทยถูกรังแกในโรงเรียน หรือพฤติกรรม bully ติดอันดับ 2 ของโลก ไทยติดอันดับเด็กวัยรุ่นท้องก่อนแต่งอันดับ 1 ของเอเชีย และอัน 2 ของโลก อายุน้อยที่สุด 10 ขวบ ไทยติดอันดับความรุนแรงในครอบครัวอันดับ 7 ของโลก เป็นต้น
        <br><br>
        <h7><strong>เกิดอะไรขึ้นกับสังคมไทย?</strong></h7>
        <br>
        ต้องบอกว่าทุกวันนี้สังคมไทยเข้าสู่ภาวะวิกฤตที่ไม่ควรมองข้าม ควรตระหนักและช่วยกันแก้ไขปัญหาอย่างเร่งด่วนที่สุด ถ้ามองว่าเป็นปัญหาใหญ่เกินไป ลำพังตัวเราคนเดียวจะทำอะไรได้ การพัฒนาก้าวหน้าคงไม่ไม่มีวันเกิดขึ้น เราจะปล่อยให้เป็นหน้าที่ของใคร ของรัฐบาล ของฝ่ายปกครอง หรือเฝ้าโทษโชคชะตา ล 
        <br><br>
        แต่ถ้าเรามองว่าการแก้ไขปัญหาสังคมเป็นหน้าที่ของเราทุกคน ต้องบอกว่าเรามา <strong>“ถูกทาง”</strong> แล้ว ดังนั้น <strong>การสร้างจิตสำนึกเพื่อช่วยเหลือผู้อื่นและส่วนรวม จึงเป็นสิ่งที่สังคมไทยต้องการอย่างยิ่ง</strong> จึงเป็นที่มาของคำว่า <strong>จิตอาสา อาสาของแผ่นดิน</strong> เพื่อฝึกให้เราทำความดีด้วยหัวใจ มีความสุขจากการเป็นผู้ให้โดยไม่หวังสิ่งตอบแทนใดๆ และมุ่งหวังประโยชน์สุขทั้งตัวเราและสิ่งแวดล้อมรอบข้าง เน้นคุณค่าทางจิตวิญญาณที่ทำให้เรามีความสุขในชีวิตได้อย่างยั่งยืน ซึ่งเป็นสิ่งที่เงินทองหาซื้อไม่ได้ 
        <br><br>
        คำว่าจิตอาสา ทำให้เรามีความสุข มีแรงบันดาลใจในการทำงานทุกวัน เพราะความดีที่เกิดจากสิ่งที่เราทำนั้น ย่อมเกิดในดวงใจผ่องใสของเราก่อนเป็นลำดับแรก เหมือนในภาษาพระท่านว่า การทำความดีนั้น ดีต่อเราทั้งในโลกนี้และโลกหน้า ดีต่อไปถึงลูกหลานได้มีแบบอย่างในการทำความดีไม่มีที่สิ้นสุด  
        <br><br>
        <h7><strong>ทำความดีเท่จะตาย</strong></h7>
        <br>
        คุณกำลังเป็นคนหนึ่งที่รู้สึกเบื่องานประจำที่ทำอยู่หรือเปล่า ต่อให้เงินเดือนสูง ตำแหน่งสูง แต่ความสุขน้อยลงเรื่อยๆ ความเครียดมากขึ้น จนบางครั้งรู้สึกว่าทรัพย์สินที่เราหามาได้ไม่มีความหมายอะไรกับเราเลย หากวันหนึ่งจู่ๆ เราต้องหมดลมหายใจไปอย่างไม่คาดฝัน แม้เข็มเล่มเดียวเราคงเอาติดตัวไปไม่ได้ เราจะรู้สึกเป็นห่วงกังวล หวงแหน โดดเดี่ยวและอ้างว้างขนาดไหน หากเราใช้ชีวิตอย่างประมาทมัวเมา โอกาสดีๆ อาจจะผ่านชีวิตเราไปอย่างไม่มีวันหวนกลับ
        <br><br>
        หากคุณเป็นคนหนึ่งที่กำลังรู้สึกแบบนี้ อยากจะเชิญชวนมาเป็น <strong>จิตอาสา อาสาของแผ่นดิน</strong> ทำความดีด้วยดวงใจผ่องใสพิสุทธิ์ เพื่ออนาคตของครูไทยและเด็กนักเรียนไทยทั่วประเทศ เป็นองค์กรการกุศลที่ไม่หวังผลกำไร แต่ก็สามารถอยู่ได้อย่างมีความสุขและทรงคุณค่าตลอดเส้นทางชีวิต ที่นึกถึงครั้งใด ปิติและภาคภูมิใจทุกครั้ง 
        <br><br>
        จะเห็นว่าโลกทุกวันนี้ คือ ตัวอย่างยุคมืดแห่งจิตใจที่ผู้คนกำลังประสบอยู่ จนหลายฝ่ายต้องหันหน้ามาร่วมมือกัน ทบทวนแก้ไขปรับปรุงสภาพการณ์ที่กำลังจะเลวร้ายให้ดีขึ้น กระแสทางเลือกใหม่ของเหล่าผู้นำที่มีชื่อเสียงระดับโลกต่างหันมาให้ความสนใจ นั่นคือ <strong>การปลูกฝังความกล้าทางจริยธรรมลงใจจิตใจมนุษย์ ให้คนเราเห็นคุณค่าของชีวิต เลือกจะก้าวเดินสู่ความเจริญด้วยรากฐานคุณธรรม มากกว่าความก้าวล้ำทางวัตถุและเทคโนโลยี</strong> ความกล้าทางศีลธรรมนี้ กระทำได้ด้วยการฝึกฝนใจให้มีความเกรงกลัว และความละอายต่อการทำความชั่ว ตระหนักในการทำความดี ซึ่งส่งผลกระทบตามมาต่อสังคมแวดล้อมรอบข้าง อย่างประมาณมิได้ สังคมในอุดมคติย่อมเกิดขึ้นได้อย่างแน่นอน 
        <br><br>
        คุณสามารถมีส่วนร่วมเป็นส่วนหนึ่งกับมูลนิธิครูดีของแผ่นดิน เจริญรอยตามเบื้องพระยุคลบาทได้หลายช่องทาง หนึ่งในนั้นคือการเป็น <strong>จิตอาสา อาสาของแผ่นดิน</strong> ซึ่งมีหลายสาขาตามความสนใจและความถนัด เช่น สาขากัลยาณมิตร สาขาสารสนเทศ เป็นต้น 
        <br><br>
        <center><h8><strong>ขอเชิญร่วมเป็นส่วนหนึ่งกับเรา ปันเวลา แบ่งความสุข เพราะโลกที่อ่อนแอใบนี้<br>กำลังต้องการกระบอกเสียง ต้องการการแก้ไข ต้องการความเปลี่ยนแปลง</h8><br><h7>“โลกต้องการคุณ”</h7></strong></center>
      </p>
    </article>
  </main>
  <!-- ################################################################################################ -->
  <img src="images/อาสาของแผ่นดินBG02.jpg" style="width:100%">
	<!-- ################################################################################################ -->
  <div class="hoc container2 clear">
    <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px">
			<strong>สาขาสารสนเทศ</strong>
			<a name="secA"></a> <!-- from ข่าวสาร015.php -->
    </p>
    <ul class="fs-32 textlink" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
      <li><a href="คุณสมบัติอาสาสารสนเทศ.php">คุณสมบัติ</a></li>
      <li><a href="pop noregis.php">สมัครเข้าร่วมโครงการ</a></li>
      <li><a href="docs/จดหมาย/จดหมาย12/แต่งตั้งคณะทำงานสารสนเทศของมูลนิธิครูดีของแผ่นดิน (อาสาของแผ่นดิน สาขาสารสนเทศ).pdf">รายชื่ออาสาของแผ่นดิน สาขาสารสนเทศ รุ่นที่ 1</a></li>
    </ul>
    <!-- ################################################################################################ -->
    <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px">
      <strong>สาขากัลยาณมิตร</strong>
    </p>
    <ul class="fs-32 textlink" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
      <li><a href="คุณสมบัติอาสากัลยาณมิตร.php">คุณสมบัติ</a></li>
      <li><a href="https://forms.gle/oZAqeuBtGwYRfpnz8">สมัครเข้าร่วมโครงการ</a></li>
      <li><a href="docs/จดหมาย/จดหมาย14/ประกาศอาสากัลยาณมิตร1.pdf">รายชื่ออาสาของแผ่นดิน สาขากัลยาณมิตร รุ่นที่ 1</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End Content 01 - โครงการอาสาของแผ่นดิน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear">
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Related Department</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Foundation of Thai Suprateacher</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>