<?php
//คำสั่ง connect db เขียนเพิ่มเองนะ

$strExcelFileName="การประเมินครูดี.xls";

header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

set_time_limit(0);
// ini_set("memory_limit","10M");
session_start();


require_once('condb.php');

// occup_code LIKE ''
$sql_list  =
"
    SELECT * From login as lg 
    LEFT JOIN fifth_score4tea as fifth ON lg.ID = fifth.ID
    WHERE  (lg.occup_code like 'OcAA%' OR lg.occup_code like 'OcAB%')  AND   fifth.fifth_score4tea_code = 'A' 
";
// WHERE  hs.honor_score4sup_code  = 'A' AND lg.occup_code like 'OcAD%'
$list = $con->query($sql_list);
// $sql=mysql_query("select * from tb_member");fifth_score4tea_data
// $num=mysql_num_rows($sql);
// echo $selfscore_sub1_avg;
// exit();
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<script type="text/javascript">
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var allscore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "ครองตน", value: <?php echo $selfscore_avg; ?>}, 
				{axis: "ครองคน", value: <?php echo $peoplescore_avg; ?>}, 
				{axis: "ครองงาน", value: <?php echo $workscore_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var selfscore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "<?php echo $selfscore_sub1_topic; ?>", value: <?php echo $selfscore_sub1_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub2_topic; ?>", value: <?php echo $selfscore_sub2_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub3_topic; ?>", value: <?php echo $selfscore_sub3_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub4_topic; ?>", value: <?php echo $selfscore_sub4_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub5_topic; ?>", value: <?php echo $selfscore_sub5_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub6_topic; ?>", value: <?php echo $selfscore_sub6_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub7_topic; ?>", value: <?php echo $selfscore_sub7_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub8_topic; ?>", value: <?php echo $selfscore_sub8_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub9_topic; ?>", value: <?php echo $selfscore_sub9_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var peoplescore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "<?php echo $peoplescore_sub10_topic; ?>", value: <?php echo $peoplescore_sub10_avg; ?>}, 
				{axis: "<?php echo $peoplescore_sub11_topic; ?>", value: <?php echo $peoplescore_sub11_avg; ?>}, 
				{axis: "<?php echo $peoplescore_sub12_topic; ?>", value: <?php echo $peoplescore_sub12_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var workscore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "<?php echo $workscore_sub13_topic; ?>", value: <?php echo $workscore_sub13_avg; ?>}, 
				{axis: "<?php echo $workscore_sub14_topic; ?>", value: <?php echo $workscore_sub14_avg; ?>}, 
				{axis: "<?php echo $workscore_sub15_topic; ?>", value: <?php echo $workscore_sub15_avg; ?>}, 
				{axis: "<?php echo $workscore_sub16_topic; ?>", value: <?php echo $workscore_sub16_avg; ?>}, 
				{axis: "<?php echo $workscore_sub17_topic; ?>", value: <?php echo $workscore_sub17_avg; ?>}, 
				{axis: "<?php echo $workscore_sub18_topic; ?>", value: <?php echo $workscore_sub18_avg; ?>}, 
				{axis: "<?php echo $workscore_sub19_topic; ?>", value: <?php echo $workscore_sub19_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
</script>


<br>
<div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
<table x:str border=1 cellpadding=0 cellspacing=1 width=100% style="border-collapse:collapse">
    <tr>
        <td style="text-align:center;">คำนำหน้านาม</td>
        <td style="text-align:center;">ชื่อ</td>
        <td style="text-align:center;">นามสกุล</td>
        <td style="text-align:center;">อายุ</td>
        <td style="text-align:center;">สังกัดหลัก</td>
        <td style="text-align:center;">สังกัดย่อย</td>
        <!-- <td>จังหวัด</td> -->
        <td style="text-align:center;" colspan="">ขั้นพื้นฐาน	</td>
        <td style="text-align:center;" colspan="">ขั้นพื้นที่5	</td>
        <td style="text-align:center;" colspan="19">ระดับเกียรติคุณ</td>
        <td style="text-align:center;" >คะแนนรวมทั้ง 19 ตัวบ่งชี้</td>
        <td style="text-align:center;" >เด็กดีที่ดูแล ใช้ระบบต้นกล้า/ทฤษฎี 21 วัน</td>
        <td style="text-align:center;" >จำนวนเด็กดีของแผ่นดินที่ดูแล</td>
        <td style="text-align:center;" >จำนวนเด็กดีของแผ่นดินที่ผ่าน</td>
    </tr>
    <?php

        $number_token = 0;
        while($row = $list->fetch_assoc()) {
            // var_dump($row['DD']);
            // exit();
            $ID = $row['ID'];
            // $_SESSION['ID'];
            // $workscore_sub10
            $selfscore_sub1_avg = 0;
            include('includes/calfifth_score-ครู.php'); 
    

    ?>
      <tr>

        <td width="94" height="30" align="center" valign="middle" >
            <?php
                echo $number_token
            ?>
        </td>

        <td width="94" height="30" align="center" valign="middle" >
            <?php
            //  echo $row['prename_remark']
            ?>
            <?php
            // if(isset($row['fifth_score4tea_detail'])){
            //     $token_score  = explode(",",$row['fifth_score4tea_detail'])[0];                
            //     if($token_score=="") {
            //         echo "0";
            //     }
            //     else {
            //         echo $token_score;
            //     }
                
            // } 
            // else {
            //     echo "0";
            // }
            // echo $row['fifth_score4tea_detail'];
             ?>

             <?php
                // echo $row['fifth_score_status'];
                // echo $row['fifth_score_status'];
             ?>
        </td>
        <td width="200" align="center" valign="middle" >
            <strong>
                <?php 
                    // echo $row['firstname'];
                    // if(isset($row['fifth_score4tea_detail'])){
                    //     $token_score  = explode(",",$row['fifth_score4tea_detail'])[1];                
                    //     if($token_score == ""){
                    //         echo "0";
                    //     }
                    //     echo $token_score;
                    // } 
                    // else {
                    //     echo "0";
                    // }
                ?>


            </strong>
        </td>
        <td width="181" align="center" valign="middle" >
            <strong>
                <?php
                //  echo  $row['lastname'];
                // if(isset($row['fifth_score4tea_detail'])){
                //     $token_score  = explode(",",$row['fifth_score4tea_detail'])[2];
                //     if($token_score==""){
                //         echo "0";
                //     }
                //     else {
                //         echo $token_score;
                //     }
                // } 
                // else {
                //     echo "0";
                // }
                ?>
            </strong>
        </td>
        <td width="181" align="center" valign="middle" >
            <strong>
                <?php
                    // $age =  2021 - explode("-",$row['birthdate'])[0];
                    // echo $age;
                ?>
            </strong>
        </td>
        <td width="181" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $row['affil_name'];
                ?>
            </strong>
        </td>
        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $row['affil_name']
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    echo $row['basic_score_status']
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $row['basic_score_total']
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php
                //   echo $selfscore_sub1_avg; 
                // echo  (float) $selfscore_sub1_avg +
                //       (float) $selfscore_sub2_avg +
                //       (float) $selfscore_sub3_avg +
                //       (float) $selfscore_sub4_avg +
                //       (float) $selfscore_sub5_avg +
                //       (float) $selfscore_sub6_avg +
                //       (float) $selfscore_sub7_avg +
                //       (float) $selfscore_sub8_avg +
                //       (float) $selfscore_sub9_avg +
                //       (float) $peoplescore_sub10_avg +
                //       (float) $peoplescore_sub11_avg +
                //       (float) $peoplescore_sub12_avg +
                //       (float) $workscore_sub13_avg + 
                //       (float) $workscore_sub14_avg +
                //       (float) $workscore_sub15_avg + 
                //       (float) $workscore_sub16_avg + 
                //       (float) $workscore_sub17_avg +
                //       (float) $workscore_sub18_avg + 
                //       (float) $workscore_sub19_avg
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php
                //   echo $selfscore_sub2_avg 
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $selfscore_sub3_avg
                ?>
            </strong>
        </td>


        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $selfscore_sub4_avg
                ?>
            </strong>
        </td>


        <td width="185" align="center" valign="middle" >
            <strong>
                <?php 
                    // echo $selfscore_sub5_avg
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $selfscore_sub6_avg  
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $selfscore_sub7_avg 
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php 
                    // echo $selfscore_sub8_avg  
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $selfscore_sub9_avg 
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $peoplescore_sub10_avg
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $peoplescore_sub11_avg
                ?>
            </strong>
        </td>
        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $peoplescore_sub12_avg
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $workscore_sub13_avg;
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $workscore_sub14_avg;
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $workscore_sub15_avg;
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php
                    // echo $workscore_sub16_avg;
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $workscore_sub17_avg;
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $workscore_sub18_avg;
                ?>
            </strong>
        </td>
        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $workscore_sub19_avg;
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo       
                    // (float) $selfscore +
                    // (float) $selfscore_sub1 +
                    // (float) $selfscore_sub2 +
                    // (float) $selfscore_sub3 +
                    // (float) $selfscore_sub4 +
                    // (float) $selfscore_sub5 +
                    // (float) $selfscore_sub6 +
                    // (float) $selfscore_sub7 +
                    // (float) $selfscore_sub8 +
                    // (float) $selfscore_sub9 +
                    // (float) $peoplescore_sub10 +
                    // (float) $peoplescore_sub11 +
                    // (float) $peoplescore_sub12 +
                    // (float) $workscore_sub13  + 
                    // (float) $workscore_sub14 +
                    // (float) $workscore_sub15 +
                    // (float) $workscore_sub16 +
                    // (float) $workscore_sub17 +
                    // (float) $workscore_sub18 +
                    // (float) $workscore_sub19 ;
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
            <?php
                // $sql_list_login  = "SELECT  count(*) as 'count' From login  WHERE basic_score_total = '$ID'";
                // // echo $sql_list_login;
                // $list_login = $con->query($sql_list_login);
                // if($list_login){
                //     while($row1 = $list_login->fetch_assoc()){
                //         echo $row1['count'];
                //     }
                // }
                // else {
                //     echo "0";
                //     // var_dump($list_login);
                // }
            ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php
                    // $token_kid = 0;
                    // $email = $row['email'];
                    // $sql_list_kid  = "SELECT  *  From login as lg LEFT JOIN  fifth_score4tea AS fifth ON  lg.ID = fifth.ID WHERE  lg.email = '$email' AND fifth.fifth_score4tea_code = 'B'  ";
                    // $list_kid = $con->query($sql_list_kid);
                    // if($list_kid){
                    //     while($row1 = $list_kid->fetch_assoc()){
                    //         $token_kid = $row1['fifth_score4tea_data'];
                            
                    //         // var_dump($row1['fifth_score4tea_data']);
                            
                    //         // echo $row1['fifth_score4tea_data'];
                    //     }
                    //     echo $token_kid;
                        
                    // }
                    // else {
                    //     echo "0";
                    // }
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // $sql_list_kid  = "SELECT  count(*) as 'count' From login  WHERE basic_score_total = '$ID' AND basic_score_status  like '%Approve%' ";
                    // //echo $sql_list_kid;
                    // $list_kid = $con->query($sql_list_kid);
                    // if($list_kid){
                    //     while($row1 = $list_kid->fetch_assoc()){
                    //         echo $row1['count'];
                    //     }
                    // }
                    // else {
                    //     echo "0";
                    //     // var_dump($list_kid);
                    // }
                ?>
            </strong>
        </td>

    </tr>
      <?php 
      $selfscore = 0;
      $selfscore_sub1 = 0;
      $selfscore_sub2 = 0;
      $selfscore_sub3 = 0;
      $selfscore_sub4 = 0;
      $selfscore_sub5 = 0;
      $selfscore_sub6 = 0;
      $selfscore_sub7 = 0;
      $selfscore_sub8 = 0;
      $selfscore_sub9 = 0;
      $peoplescore_sub10 = 0;
      $peoplescore_sub11 = 0;
      $peoplescore_sub12 = 0;
      $workscore_sub13  = 0 ;
      $workscore_sub14 = 0;
      $workscore_sub15 = 0;
      $workscore_sub16 = 0;
      $workscore_sub17 = 0;
      $workscore_sub18 = 0;
      $workscore_sub19 = 0;
      $number_token++;
    };
      ?>
</table>
</div>
<?php 

echo  
"
<script>
    window.onbeforeunload = function(){return false;};
setTimeout(function(){window.close();}, 10000);
</script>
";

?>

</body>
</html>