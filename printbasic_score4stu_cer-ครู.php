<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if($_POST['CFP'] == 'ระบบติดตามสถานะนักเรียนขั้นพื้นฐาน-ครู') {

			/*
				Code A: Directory เกียรติบัตรเด็กขั้นพื้นฐาน
				Code B: Running Number (ดดผ.6300XXXXX1)
			*/

			$ID_stu = $_POST['ID_stu'];
			$basic_score4stu_cer_code = $_POST['system_id'];

			$sqlbasic_score4stu_cer = "SELECT * FROM `basic_score4stu_cer` WHERE ID='$ID_stu' AND `basic_score4stu_cer_code`='A' ";
			$reBS4SC = mysqli_query($con, $sqlbasic_score4stu_cer);
			
			if(mysqli_num_rows($reBS4SC) == 0) { // first time click print basic_score4stu_cer
	
					/***********************************************************************************************************************************/
					/**************************************************** Start Print & Save เกียรติบัตร **************************************************/
					/***********************************************************************************************************************************/
	
					/* Save เกียรติบัตรออนไลน์ with user name */
					$jpg_image = imagecreatefromjpeg('images/เกียรติบัตร/พื้นฐาน/เด็ก/เกียรติบัตรขั้นพื้นฐาน-เด็ก.jpg'); // Create Image From Existing File
					$font_color = imagecolorallocate($jpg_image, 0, 0, 0); // Set font color
					$font_path = 'layout/styles/fonts/THSarabunIT๙ Bold.ttf'; // Set font file path
					
					/* Set Text that need to be Printed On Image */
					$sqlstu = "SELECT * FROM `login` WHERE `ID`='$ID_stu' ";
					$restu = mysqli_query($con, $sqlstu);
					$rowstu = mysqli_fetch_array($restu);
					// Set pename
					if($rowstu['prename'] == 'A') {
						$prename = 'นาย';
					}elseif($rowstu['prename'] == 'B') {
						$prename = 'นาง';
					}elseif($rowstu['prename'] == 'C') {
						$prename = 'นางสาว';
					}elseif($rowstu['prename'] == 'D') {
						$prename = 'ด.ช.';
					}elseif($rowstu['prename'] == 'E') {
						$prename = 'ด.ญ.';
					}elseif($rowstu['prename'] == 'O') {
						$prename = $rowstu['prename_remark'];
					}
					$name = $rowstu['firstname'].' '.$rowstu['lastname']; // Set name
					$namewithprename = $prename.$name; // Set prename
	
					// Set School
					$affil_name = $rowstu['affil_name'];
	
					$school = 'โรงเรียน'.substr($affil_name, strrpos($affil_name, '*')+1);

					// Set Running Number for basic certificate
					$sqlbasic_score4stu_cer_codeB_check_exist = "SELECT basic_score4stu_cer_data1 FROM `basic_score4stu_cer` WHERE ID='$ID_stu' AND `basic_score4stu_cer_code`='B' ";
					$reBS4SC_CBCE = mysqli_query($con, $sqlbasic_score4stu_cer_codeB_check_exist);

					if(mysqli_num_rows($reBS4SC_CBCE) == 1) {
						$rowBS4SC_CBCE = mysqli_fetch_array($reBS4SC_CBCE);

						$basic_score4stu_cer_run_num = $rowBS4SC_CBCE['basic_score4stu_cer_data1'];
					}else{
						$year = substr(date('Y')+543,-2);
						$find_run_num = 'ดดผ.'.$year.'00%';

						$sqlbasic_score4stu_cer_codeB = "SELECT * FROM `basic_score4stu_cer` WHERE `basic_score4stu_cer_code` LIKE 'B' AND `basic_score4stu_cer_data1` LIKE '$find_run_num' ORDER BY `basic_score4stu_cer_id` DESC LIMIT 1 ";
						$reBS4SCCB = mysqli_query($con, $sqlbasic_score4stu_cer_codeB);
						$rowBS4SCCB = mysqli_fetch_array($reBS4SCCB);
					
						$basic_score4stu_cer_type = 'ดดผ.';
						$basic_score4stu_cer_num = sprintf('%06d', intval(substr($rowBS4SCCB['basic_score4stu_cer_data1'], -5))+1);
						$basic_score4stu_cer_run_num = $basic_score4stu_cer_type.$year.'00'.$basic_score4stu_cer_num;
					}
	
					// Set date
					$date = date('Y-m-d', strtotime($rowstu['basic_score_date']));
	
					/* Array for Thai Date */
					$arabicnum = array("1","2","3","4","5","6","7","8","9","0");
					$thainum = array("๑","๒","๓","๔","๕","๖","๗","๘","๙","๐");
					$test = str_replace($numthai,$numarabic,$message);
					/* Array for Thai Month */
					$month_arr=array(
						"1"=>"มกราคม",
						"2"=>"กุมภาพันธ์",
						"3"=>"มีนาคม",
						"4"=>"เมษายน",
						"5"=>"พฤษภาคม",
						"6"=>"มิถุนายน", 
						"7"=>"กรกฎาคม",
						"8"=>"สิงหาคม",
						"9"=>"กันยายน",
						"10"=>"ตุลาคม",
						"11"=>"พฤศจิกายน",
						"12"=>"ธันวาคม"                 
					);
					$date_cer = str_replace($arabicnum, $thainum, date('j', strtotime($date))).' '.$month_arr[date('n', strtotime($date))].' '.str_replace($arabicnum, $thainum, (date('Y', strtotime($date))+543)); // Set certicate date in Thai
	
					$font_size = 40; //Set font size
					$angle = 0; //Set angle
					/* Set x-position on certificate for name */
					$dimen4name = imagettfbbox($font_size, $angle, $font_path, $namewithprename);
					$text4name = (abs($dimen4name[4] - $dimen4name[0]))/2;
					$x4name = 758 - $text4name;
					/* Set x-position on certificate for school */
					$dimen4school = imagettfbbox($font_size, $angle, $font_path, $school);
					$text4school = (abs($dimen4school[4] - $dimen4school[0]))/2;
					$x4school = 758 - $text4school;
					/* Print school name and date on certificate */
					imagettftext($jpg_image, $font_size, $angle, $x4name, 385, $font_color, $font_path, $namewithprename);
					imagettftext($jpg_image, $font_size, $angle, $x4school, 455, $font_color, $font_path, $school);
					imagettftext($jpg_image, $font_size, $angle, 650, 675, $font_color, $font_path, $date_cer);
					imagettftext($jpg_image, 27, $angle, 1200, 110, $font_color, $font_path, $basic_score4stu_cer_run_num);
	
					$target_dir = 'images/เกียรติบัตร/พื้นฐาน/เด็ก/'.$date.'/'; // Set target_directory
	
					if(!is_dir($target_dir)) { // if there's not folder in target_directory
						mkdir($target_dir); // Create folder name is today_date
					}
	
					$target_file = $target_dir.'เกียรติบัตรเด็กขั้นพื้นฐาน '.$name.'.jpg';
	
					imagejpeg($jpg_image, $target_file);// Send Image to Browser or save in directory on client
					imagedestroy($jpg_image); // Clear Memory
	
					/* INSERT data to `basic_score4stu_cer` table */
					$basic_score4stu_cer_code_A = 'A';
					$basic_score4stu_cer_data1_A = $target_file;

					$basic_score4stu_cer_code_B = 'B';
					$basic_score4stu_cer_data1_B = $basic_score4stu_cer_run_num;

					$basic_score4stu_cer_remark = 'IDครูที่ปรึกษา:'.$ID;

					if(mysqli_num_rows($reBS4SC_CBCE) == 1) {

						$sql = "INSERT INTO `basic_score4stu_cer` (`ID`, `basic_score4stu_cer_code`, `basic_score4stu_cer_data1`, `basic_score4stu_cer_remark`) 
						VALUES ('$ID_stu', '$basic_score4stu_cer_code_A', '$basic_score4stu_cer_data1_A', '$basic_score4stu_cer_remark') ";
						$res = $con->query($sql) or die($con->error); //Check error

					}else{

						$sql = "INSERT INTO `basic_score4stu_cer` (`ID`, `basic_score4stu_cer_code`, `basic_score4stu_cer_data1`, `basic_score4stu_cer_remark`) 
						VALUES ('$ID_stu', '$basic_score4stu_cer_code_A', '$basic_score4stu_cer_data1_A', '$basic_score4stu_cer_remark'),
						('$ID_stu', '$basic_score4stu_cer_code_B', '$basic_score4stu_cer_data1_B', '$basic_score4stu_cer_remark') ";
						$res = $con->query($sql) or die($con->error); //Check error

					}
	
					header('location: '.$target_file);
	
					/***********************************************************************************************************************************/
					/****************************************************** End Print & Save เกียรติบัตร **************************************************/
					/***********************************************************************************************************************************/


			}else{

				$rowBS4SC = mysqli_fetch_array($reBS4SC);
				header('location: '.$rowBS4SC['basic_score4stu_cer_data1']);

			}


		}else{
			echo "<script>window.history.go(-1)</script>";
		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>