<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>
<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop-en.php'); ?>
                  <ul>
                    <li><a href="จดหมายรวม002.php">ภาษาไทย</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom-en.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false">Letter/Declaration</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 03 - จดหมาย -->
<div style="background-color:rgb(226,255,224);">
  <div class="hoc container clear">
    <table class="table1 table2" style="padding-bottom:20px;">
      <thead>
        <tr>
          <th</th>
          <th></th>
        </tr>
      </thead>
      <!-- ################################################################################################ -->
      <tbody>
        <!-- จดหมาย 013 -->
        <tr>
          <td>
            <a class="linkfortable" href="จดหมาย013-en.php">สพฐ. เชิญเข้าร่วมโครงการศึกษานิเทศก์ดีของแผ่นดิน</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 012 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย012-en.php">แต่งตั้งคณะทำงานสารสนเทศของมูลนิธิครูดีของแผ่นดิน (อาสาของแผ่นดิน สาขาสารสนเทศ)</td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 011 -->
        <tr>
          <td>
            <a class="linkfortable" href="จดหมาย011-en.php">สพฐ.ขอความร่วมมือประชาสัมพันธ์หลักสูตร Be Internet Awesome และโครงการเครือข่ายครูดีของแผ่นดิน</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 010-->
        <tr>
          <td><a class="linkfortable" href="จดหมาย010-en.php">ประกาศรางวัลครูดีของแผ่นดิน ชั้นที่ 5 ปี 2563</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 009 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย009-en.php">ประกาศขอความร่วมมือผู้ที่เป็นสมาชิกเครือข่ายครูดีของแผ่นดิน เปลี่ยนแปลงและปรับปรุงฐานข้อมูล</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 008 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย008-en.php">ขอเชิญเป็นอาสาของแผ่นดิน สาขาสารสนเทศ</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 007 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย007-en.php">ประกาศรายชื่อผู้บริหารและครูที่มีสิทธิ์เข้าอบรมสัมมนาเสริมสร้างการใช้เทคโนโลยีดิจิทัลอย่างสร้างสรรค์และปลอดภัย ผ่านหลักสูตร Be Internet Awesome by Google</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 006 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย006-en.php">หนังสือเชิญโรงเรียนเข้าร่วมสัมมนาเสริมสร้างการใช้เทคโนโลยีดิจิทัลอย่างสร้างสรรค์และปลอดภัย ผ่านหลักสูตร Be Internet Awesome by Google</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 005 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย005-en.php">หนังสือเชิญร่วมนำเสนอผลงานในงานวันครู ครั้งที่ 64 ปี 2563</td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 004 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย004-en.php">หนังสือเชิญร่วมนำเสนอผลงานใน "งานวันเด็กแห่งชาติ 2563"</td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<!-- End Content 03 - จดหมาย -->
<!-- ################################################################################################ -->
<!-- Start Pagination -->
<div class="wrapper row3 coloured" style="background-color:rgb(226,255,224);">
  <main class="hoc container clear">
    <div class="content">
      <nav class="pagination">
        <ul>
          <li><a href="จดหมายรวมlatest-en.php">3</a></li>
          <li class="current"><strong>2</strong></li>
          <li><a href="จดหมายรวม001-en.php">1</a></li>
        </ul>
      </nav>
    </div>
  </main>
</div>
<!-- End Pagination -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer-en.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>