<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
  $sql_list_project  ="SELECT * FROM join_project";
  $list_project = $con->query($sql_list_project);

?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
				<?php if (strpos($_SESSION['email'], '@') !== false) {
								if ($_SESSION["level"]=="admin") { //Admin Session ?>
									<li style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
											<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*A*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*B*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
										</ul>
									</li>
          <?php }elseif ($_SESSION['level']=='memberGeneral') { //General Member Session ?>
								<li style="color:black; background-color:white;"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> สมาชิกทั่วไป</a>
									<ul>
										<li><a href="editprofile.php">แก้ไขข้อมูลส่วนตัว</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberSilver') { //Member Silver Session ?>
								<li style="color:white; background-color:rgb(169,169,169);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</a>
									<ul>
										<li><a href="editprofile.php">แก้ไขข้อมูลส่วนตัว</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberGold') { //Member Gold Session ?>
								<li style="color:white; background-color:rgb(180,147,31);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</a>
									<ul>
										<li><a href="editprofile.php">แก้ไขข้อมูลส่วนตัว</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberDiamond') { //Member Diamond Session ?>
								<li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</a>
									<ul>
										<li><a href="editprofile.php">แก้ไขข้อมูลส่วนตัว</a></li>
									</ul>
								</li>
          <?php } ?>
								<li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
        <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
                <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
				<?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
                  <ul>
                    <li><a href="ข่าวสาร017-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
                    <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึก</a></li>
                  </ul>
                </li>
        </ul>
			</nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px; ">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <?php 
                while($row = $list_project->fetch_assoc()){
                  $id_join_project = $row['id_join_project'];
                  $title_menu = $row['title_menu']; 
              ?>
              <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
              <?php } ?>
              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          </li>
          
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="ข่าวสารรวมlatest.php"> ข่าวสาร</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> ปรึกษาแนวทางการดำเนินโครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - ข่าวสาร017 -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <article style="text-align:center;">
      <!-- ################################################################################################ -->
      <p class="font-x3">
        <span style="color:rgb(180,147,31); line-height:80px">
        <strong>ปรึกษาแนวทางการดำเนินโครงการศึกษานิเทศก์ดีของแผ่นดิน</strong>
      </p>      
      <!-- ################################################################################################ -->
      <p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				วันนี้ (15 มิถุนายน 2563) นายฐกร พฤฒิปูรณี กรรมการและเลขานุการมูลนิธิครูดีของแผ่นดิน ได้เข้าพบ ดร.กิตติกร คัมภีรปรีชา ศึกษานิเทศก์เชี่ยวชาญพิเศษ ที่ปรึกษาโครงการฯ เพื่อปรึกษาหารือแนวทางการดำเนินโครงการศึกษานิเทศก์ดีของแผ่นดิน ณ สนามบินดอนเมือง กรุงเทพฯ
				<br><br>
				<img src="images/รูปข่าวสาร17/รูปข่าวสาร17-01.jpg" alt="รูปข่าวสาร17-01">
      </p>
      <!-- ################################################################################################ -->
    </article>
  </main>
</div>
<!-- End Content 01 - ข่าวสาร017 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<?php include('includes/footer.php'); ?>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>