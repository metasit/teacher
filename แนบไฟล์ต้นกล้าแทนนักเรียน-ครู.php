<?php 
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];
	$occup_code = $_SESSION['occup_code'];
	
	if(isset($ID)) {

		if(substr($occup_code, 0, 4) == 'OcAA' || substr($occup_code, 0, 4) == 'OcAB') {

			if(isset($_POST['ID_stu'])) {
				$ID_stu = $_POST['ID_stu'];
			}else{
				$ID_stu = $_GET['ID_stu'];
			}

			$sqllogin = "SELECT * FROM `login` WHERE ID='$ID_stu' ";
			$relogin = mysqli_query($con,$sqllogin);
			$rowlogin = mysqli_fetch_array($relogin);

			if(strpos($rowlogin['basic_score_status'], 'ยืนยันแล้ว') !== false) {
				date_default_timezone_set("Asia/Bangkok");

				$stu_name = $rowlogin['firstname'].' '.$rowlogin['lastname'];

				$system_id = substr($rowlogin['basic_score_ans'],0,1);
				$devmain_id = substr($rowlogin['basic_score_ans'],2,1);
				$devsub = substr($rowlogin['basic_score_ans'],4);

				if($system_id == 'A') {
					$system_name = 'ระบบต้นกล้าแห่งความดี';
				}elseif($system_id == 'B') {
					$system_name = 'ระบบทฤษฎี 21 วัน (แต่ในปีนี้เนื่องจากโควิด 19 จะลดเหลือ 14 ครั้ง)';
				}

				if($devmain_id == 'A') {
					$devmain_name = 'การรู้จัก/จัดการตนเอง';
				}elseif($devmain_id == 'B') {
					$devmain_name = 'การเข้าใจผู้อื่น';
				}elseif($devmain_id == 'C') {
					$devmain_name = 'การเข้าสังคม';
				}elseif($devmain_id == 'D') {
					$devmain_name = 'การสื่อสารและการสร้างแรงบันดาลใจ';
				}

			}else{
				echo "<script>window.history.go(-1)</script>"; //Protect policy
			}

		}else{
			echo "<script>window.history.go(-1)</script>"; //Protect policy
		}
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>


<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="#" onclick="return false"> แนบไฟล์ต้นกล้าแห่งความดีแทนนักเรียน (<?php echo $stu_name; ?>)</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - แนบไฟล์ทำความดี-เด็ก.php -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
	<!-- ################################################################################################ -->
	<!-- Start Slip Upload  -->
	<div class="cart-box-main">
    <div class="container" style="padding-top: 0;">
      <div class="row">
				<!-- ################################################################################################ -->
				<!-- Start Left Content -->
				<?php

					/* Let set DateTime format before check more than 30 days can do basic_score again */
					$last_basic_score_date = new DateTime($rowlogin['basic_score_date']);
					$expire_basic_score_date = new DateTime($rowlogin['basic_score_date']);
					$datetoday = new DateTime(date('Y-m-d'));
					$last_basic_score_date->settime(0,0); // No need time to check
					$expire_basic_score_date->settime(0,0); // No need time to check
					date_add($expire_basic_score_date, date_interval_create_from_date_string('30 days'));
					$diff = date_diff($datetoday, $expire_basic_score_date);
					$expire_basic_score_date = date_format($expire_basic_score_date, 'd-m-Y');
					$check_basic_score_date = $diff->format('%a');

					$sqlcountrow = "SELECT count(*) AS countrow FROM `basic_score4stu` WHERE ID='$ID_stu' AND basic_score4stu_system_id='$system_id' ORDER BY basic_score4stu_file_date ASC ";
					$recountrow = mysqli_query($con,$sqlcountrow);
					$rowcountrow = mysqli_fetch_array($recountrow);

					$sqlbasic_score4stu_system_id_C = "SELECT * FROM `basic_score4stu` WHERE ID='$ID_stu' AND basic_score4stu_system_id='C' ORDER BY basic_score4stu_id DESC LIMIT 1 ";
					$reBS4SC = mysqli_query($con, $sqlbasic_score4stu_system_id_C);
					$rowBS4SC = mysqli_fetch_array($reBS4SC);

					// Set to show อนุมัติ, ปฏิเสธ or แก้ไข status
					$basic_score4stu_check_status = $rowBS4SC['basic_score4stu_check_status'];
				?>
				<div class="col-sm-5 col-lg-5 mb-3">
					<div class="col-md-12 col-lg-12">
						<div class="title-left">
							<h3>รายละเอียดสิ่งที่ต้องทำเพื่อได้รับรางวัล</h3>
						</div>
						<!-- ################################################################################################ -->
						<p class="m-t-10 text-black"><strong>วันที่เริ่มสมัครโครงการ:</strong> <?php echo date("d-m-Y", strtotime($rowlogin['basic_score_date'])); ?></p>
						<p class="m-t-10 text-black"><strong>วันสุดท้ายที่สามารถส่งผลการพัฒนาตนเอง:</strong> <?php echo $expire_basic_score_date.' (เหลือ '.$check_basic_score_date; ?> วัน)</p>
						
						<?php
							if(strpos($rowBS4SC['basic_score4stu_check_status'], 'ปฏิเสธ,') !== false) { ?>
								<p class="m-t-10 text-black bold" style="display: inline">สิ่งที่ต้องการพัฒนาตนเอง
									<div class="text-red1 bold" style="display: inline"> <?php
										echo str_replace(',', ' ', $basic_score4stu_check_status); ?>
									</div>
								</p>
								<p class="m-t-10 text-black">หัวข้อหลัก: <?php echo $devmain_name; ?></p>
								<p class="m-t-10 text-black">หัวข้อย่อย: <?php echo $devsub; ?></p> <?php

							}elseif(strpos($rowBS4SC['basic_score4stu_check_status'], 'อนุมัติ,') !== false) { ?>
								<p class="m-t-10 text-black bold" style="display: inline">สิ่งที่ต้องการพัฒนาตนเอง
									<div class="text-green1 bold" style="display: inline"> <?php
										echo 'ได้รับการ'.str_replace(',', ' ', $basic_score4stu_check_status); ?> <i class="fas fa-check-circle"></i>
									</div>
								</p>
								
								<p class="m-t-10 text-black">หัวข้อหลัก: <?php echo $devmain_name; ?></p>
								<p class="m-t-10 text-black">หัวข้อย่อย: <?php echo $devsub; ?></p> <?php

							}elseif(strpos($rowBS4SC['basic_score4stu_check_status'], 'แก้ไขไฟล์,') !== false) {
								$new_devsub_index = substr($rowBS4SC['basic_score4stu_check_status'], strpos($rowBS4SC['basic_score4stu_check_status'], ',')+1);
								$new_devsub = substr($new_devsub_index, 0, strpos($new_devsub_index, ','))	?>

								<p class="m-t-10 bold" style="display: inline">สิ่งที่ต้องการพัฒนาตนเอง
									<div class="text-yellow2 bold" style="display: inline">รอตรวจ</div>
								</p>
								<p class="m-t-10 text-black">หัวข้อหลัก: <?php echo $devmain_name; ?></p>
								<p class="m-t-10 text-black">หัวข้อย่อย: <?php echo $devsub; ?></p> <?php

							}else{ ?>
								<p class="m-t-10 text-black bold" style="display: inline">สิ่งที่ต้องการพัฒนาตนเอง</p>
								
								<p class="m-t-10 text-black">หัวข้อหลัก: <?php echo $devmain_name; ?></p>
								<p class="m-t-10 text-black">หัวข้อย่อย: <?php echo $devsub; ?></p> <?php

							}
							
						?>
						
						<p class="m-t-10 text-black"><strong>ระบบที่ใช้ส่งผลงาน:</strong> <?php echo $system_name; ?></p>
						<p class="m-t-10 text-black"><strong>จำนวนผลการพัฒนาตนเองที่ส่งแล้ว:</strong> <?php echo $rowcountrow['countrow']; ?>/<?php if($system_id=='A'){echo '1';}else{echo '14';} ?> ครั้ง</label>
						<?php
							if($system_id == 'B') { ?>
								<p>* กรุณาอ่านและทำความเข้าใจอย่างละเอียด ก่อนกดอัพโหลดไฟล์</p>
								<p>
									กฎเกณฑ์ในการส่งผลงานหรือการอัพโหลดไฟล์
									<br>
									1. มีเวลาในการทำความดีและส่งผลงานให้ครบ 14 ครั้ง ภายใน 30 วัน
									<br>
									2. ระบบจะถือว่า หลังจากอัพโหลดไฟล์แรกเสร็จสิ้น เป็นวันที่1ของการทำความดี และจะทำการนับถอยหลังจนครบ 30 วัน
									<br>
									3. การอัพโหลดไฟล์ต้องระบุทั้งวันและเวลาที่ได้ทำความดี
									<br>
									4. ความดีที่ทำนั้นทำในวันใด วันที่ทำต้องไม่ซ้ำกัน
									<br>
									5. สามารถแนบภาพ/ข้อความ/วีดีโอสั้นๆได้
									<br>
									6. ในกรณีที่ระบบนับถอยหลังจนครบ 30 วัน แต่ยังคงอัพโหลดความดีไม่ครบ 14 ครั้ง จะถือว่า การส่งผลงานล้มเหลว
								</p> <?php
							}
						?>
					</div>
				</div>
				<!-- End Left Content -->
				<!-- ################################################################################################ -->
				<!-- ################################################################################################ -->
				<!-- ################################################################################################ -->
				<!-- Start Right Content -->
				<div class="col-sm-7 col-lg-7 mb-3">
					<div class="col-md-12 col-lg-12">
						<div class="input-slip-upload">
							<!-- Start form input -->
							<?php
								if($rowcountrow['countrow'] < 1) { ?>

									<div class="title-left">
										<h3>แนบไฟล์ต้นกล้าแห่งความดี</h3>
									</div>
									<form action="uploadDogood_system_id_A-ครู.php" method="POST" enctype="multipart/form-data" class="m-b-20 p-b-20" style="border-bottom:2px solid white;">
										<div class="row m-t-20">
											<div class="col-md-3 mb-3">
												<label for="basic_score4stu_file_date">วันที่ทำความดี *</label>
												<input type="date" name="basic_score4stu_file_date" class="form-control" min="2020-07-01" max="<?php echo date('Y-m-d'); ?>" required/>
											</div>
											<div class="col-md-3 mb-3">
												<label for="basic_score4stu_file_date">เวลาที่ทำความดี *</label>
												<input type="time" name="basic_score4stu_file_time" class="form-control" required/>
											</div>
											<div class="col-md-6 mb-3">
												<label for="basic_score4stu_file">แนบหลักฐานการทำความดี *</label>
												<input type="file" id="basic_score4stu_file" name="basic_score4stu_file" class="form-control" required/>
											</div>
											<div class="col-md-12 mb-3">
												<label for="basic_score4stu_text">รายงานการทำความดี</label>
												<textarea class="col-md-12" rows="1" name="basic_score4stu_text"></textarea>
											</div>
										</div>
										<div class="row m-t-12">
											<div class="col-md-12">
												<button type="submit" class="ml-auto btn hvr-hover col-md-12" style="border-color:transparent; border-radius:50px; padding:1px 0;"
													onmousemove="this.style.cursor='pointer'">
													แนบไฟล์ใหม่
												</button>
											</div>
										</div>

										<input type="hidden" name="ID_stu" value="<?php echo $ID_stu; ?>">
									</form> <?php
									
									
								}else{
									$sqlbasic_score4stu = "SELECT * FROM `basic_score4stu` WHERE ID='$ID_stu' AND basic_score4stu_system_id='A' ";
									$reBS4S = mysqli_query($con,$sqlbasic_score4stu);
									$rowBS4S = mysqli_fetch_array($reBS4S);

									$basic_score4stu_file_date = $rowBS4S['basic_score4stu_file_date'];
									$createDate = new DateTime($basic_score4stu_file_date);
									$date = $createDate->format('Y-m-d');
									$time = $createDate->format('h:i'); ?>
									
									<div class="title-left">
										<h3>แนบไฟล์ต้นกล้าแห่งความดีแทนนักเรียน</h3>
									</div>
									<form action="uploadDogood_system_id_A-ครู.php" method="POST" enctype="multipart/form-data">
										<div class="row m-t-20">
											<div class="col-md-12 mb-3">
												<?php
													$basic_score4stu_check_status = $rowBS4S['basic_score4stu_check_status'];

													if(strpos($basic_score4stu_check_status, 'ปฏิเสธ,') !== false) { ?>
														<p class="text-red1 bold m-t-0">สถานะการตรวจ: ถูกปฏิเสธ เนื่องจาก <?php echo substr($basic_score4stu_check_status, strpos($basic_score4stu_check_status, ',')+1); ?></p> <?php
													}elseif(strpos($basic_score4stu_check_status, 'อนุมัติ,') !== false) { ?>
														<p class="text-green1 bold m-t-0">สถานะการตรวจ: ผ่านแล้ว <i class="fas fa-check-circle"></i></p> <?php
													}elseif(strpos($basic_score4stu_check_status, 'แก้ไขไฟล์,') !== false || strpos($basic_score4stu_check_status, 'แก้ไขรายงาน,') !== false) { ?>
														<p class="text-yellow2 bold m-t-0">สถานะการตรวจ: รอตรวจ
															<?php
																if(strpos($basic_score4stu_check_status, 'แก้ไขไฟล์,') !== false) {
																	echo '<br>แก้ไขไฟล์ไปเมื่อ '.substr($basic_score4stu_check_status, strpos($basic_score4stu_check_status, 'แก้ไขไฟล์,')+28, 19);
																}
															?>
														</p> <?php
													}
												?>
											</div>
											<div class="col-md-3 mb-3"> <?php
												if(strpos($basic_score4stu_check_status, 'อนุมัติ,') !== false) { ?>
													<label class="bold">วันที่ทำความดี <?php echo $i; ?></label>
													<br>
													<label><?php echo $createDate->format('d M Y'); ?></label> <?php
												}else{ ?>
													<label class="bold">วันที่ทำความดี <?php echo $i; ?> *</label>
													<input type="date" name="basic_score4stu_file_date" class="form-control" value="<?php echo $date; ?>" min="2020-07-01" max="<?php echo date('Y-m-d'); ?>" required/> <?php
												} ?>
											</div>
											<div class="col-md-3 mb-3"> <?php
												if(strpos($basic_score4stu_check_status, 'อนุมัติ,') !== false) { ?>
													<label class="bold">เวลาที่ทำความดี <?php echo $i; ?></label>
													<br>
													<label><?php echo $createDate->format('g:i A'); ?></label> <?php
												}else{ ?>
													<label class="bold">เวลาที่ทำความดี <?php echo $i; ?> *</label>
													<input type="time" name="basic_score4stu_file_time" class="form-control" value="<?php echo $time; ?>" required/> <?php
												} ?>
											</div>
											<div class="col-md-6 mb-3"> <?php
												if(strpos($basic_score4stu_check_status, 'อนุมัติ,') !== false) { ?>
													<label class="bold">หลักฐานการทำความดี <?php echo $i; ?></label>
													<br>
													<button class="col-md-12 stu-file-btn" style="color: white" type="button"
													onclick=" window.open('<?php echo $rowBS4S['basic_score4stu_file']; ?>','_blank') ">
													ดูไฟล์แนบ <?php echo $i; ?>
												</button> <?php
												}else{ ?>
													<label class="bold">แนบหลักฐานการทำความดี <?php echo $i; ?></label>
													<input type="file" id="basic_score4stu_file" name="basic_score4stu_file" class="form-control" required/> <?php
												} ?>
											</div>
											<div class="col-md-12 mb-3"> <?php
												if(strpos($basic_score4stu_check_status, 'อนุมัติ,') !== false) {
													
												}else{ ?>
												<label for="uploadfilebtn" style="color:transparent">.</label> <?php
													if($check_basic_score_date <= 0) { ?>
														<button type="submit" class="col-md-12" style="color:black; background-color:white; border-radius:50px; border-color:transparent;" disabled>หมดเวลาส่งผลงาน</button> <?php
													}else{ ?>
														<button type="submit" class="col-md-12 stu-up-btn" onmousemove="this.style.cursor='pointer'"> แนบไฟล์ <?php echo $i; ?> (ถ้าทำการแนบไฟล์ซ้ำ จะทำให้ไฟล์เก่าหาย)</button> <?php
													}
												} ?>
											</div>
											<div class="col-md-12 mb-3"> <?php
												if(strpos($basic_score4stu_check_status, 'อนุมัติ,') !== false) {

												}else{ ?>
													<label for="basic_score4stu_text" style="color:transparent"></label>
													<button class="col-md-12 stu-file-btn" style="color: white" type="button"
														onclick=" window.open('<?php echo $rowBS4S['basic_score4stu_file']; ?>','_blank') ">
														ดูไฟล์แนบ <?php echo $i; ?>
													</button> <?php
												} ?>
											</div>
										</div>

										<input type="hidden" name="ID_stu" value="<?php echo $ID_stu; ?>">
									</form>
									<form action="addbasic_score4stu_text_system_id_A-ครู.php" method="POST" enctype="multipart/form-data">
										<div class="row p-b-20" style="border-bottom:4px solid black">
											<div class="col-md-12 mb-3"> <?php
												if(strpos($basic_score4stu_check_status, 'อนุมัติ,') !== false) { ?>
													<label class="bold">รายงานการทำความดี <?php echo $i; ?></label>
													<br>
													<label><?php if($rowBS4S['basic_score4stu_text'] == ''){echo 'ไม่พบการเขียนรายงานการทำความดี';}else{echo $rowBS4S['basic_score4stu_text'];} ?></label> <?php														
												}else{ ?>
													<label class="text-yellow2 m-t-20 bold">
														<?php
															if(strpos($basic_score4stu_check_status, 'แก้ไขรายงาน,') !== false) {
																echo 'แก้ไขรายงานไปเมื่อ '.substr($basic_score4stu_check_status, strpos($basic_score4stu_check_status, 'แก้ไขรายงาน,')+34, 19);
															}
														?>
													</label>
													<label class="col-md-12 bold">รายงานการทำความดี <?php echo $i; ?></label>
													<textarea class="col-md-12" rows="1" name="basic_score4stu_text"><?php echo $rowBS4S['basic_score4stu_text']; ?></textarea>
													<label for="addbasic_score4stu_textbtn" style="color:transparent">.</label>
													<button type="submit" class="col-md-12 stu-up-btn"> อัพเดทรายงานความดี <?php echo $i; ?></button> <?php
												} ?>
											</div>
										</div>

										<input type="hidden" name="ID_stu" value="<?php echo $ID_stu; ?>">
										<input type="hidden" name="basic_score4stu_system_id" value="<?php echo $system_id; ?>">
									</form>	<?php
								}
							?>
							<!-- ################################################################################################ -->
							<!-- End form input -->
						</div>
					</div>
      	</div>
				<!-- End Right Content -->
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
  <!-- End Slip Upload -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - แนบไฟล์ทำความดี-เด็ก.php -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

</body>
</html>