<?php
	session_start();
	if($_POST['check'] == 1) {
		require_once('condb.php');
	}else{
		header("Location: javascript:history.go(-1);");
	}
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>Foundation of Thai Suprateacher</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>

<body>

<?php
	$oldEmail = $_SESSION['email'];
	$newEmail = strtolower(trim($_POST["email"]));
	$newPassword = $_POST["password"];
	$newFirstname = trim($_POST["firstname"]);
	$newLastname = trim($_POST["lastname"]);
	$newPhonenum = $_POST["phonenum"];
	$Birthdate = $_POST["birthdate"];
	$newBirthdate = str_replace("-",'',$Birthdate);
	$newDocaddress = $_POST["docaddress"];


	$sql = "UPDATE `login` SET `email`='$newEmail',`password`='$newPassword',`firstname`='$newFirstname',`lastname`='$newLastname',`phonenum`='$newPhonenum'
	,`birthdate`='$newBirthdate',`docaddress`='$newDocaddress' WHERE email='$oldEmail' ";

	$res= $con->query($sql) or die($con->error); //Check error
?>
	<!-- Start Password match and save to database -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="index-en.php"><i class="fas fa-arrow-left"></i> Back to Home</a>
					<br>
					<a href="editprofile-en.php"><i class="fas fa-arrow-left"></i> Back to edit my profile</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<div class="signup-stat m-t-20">
					<h8>Profile has been updated</h8>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	<!-- End Password match and save to databas -->