<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="all_project.php"> ร่วมโครงการ</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="โครงการเด็กดีของแผ่นดิน.php"> โครงการเด็กดีของแผ่นดิน</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - โครงการเด็กดีของแผ่นดิน -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <!-- ################################################################################################ -->
    <article style="text-align:center; padding-bottom:20px; border-bottom:3px solid #59A209;">
      <!-- ################################################################################################ -->
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px"><strong>เด็กดีของแผ่นดิน (Z-iDol)</strong></p>
      <img src="images/เด็กดีของแผ่นดิน Logo.jpg" alt="เด็กดีของแผ่นดิน Logo">
      <p class="fs-32" style="font-family:RSUText; line-height:30pt; text-align:left;">
        เด็กรุ่นใหม่ ที่เรียกกันว่า Generation Z หรือ Gen Z คือ นิยามของเด็กที่เกิดหลังปี ค.ศ. 1995 หรือ ปี พ.ศ. 2538 ลงมา เด็กกลุ่มนี้เติบโตมาพร้อมกับสิ่งอำนวยความสะดวกมากมาย แวดล้อมด้วยการเรียนรู้
        และการดำเนินชีวิตในสังคมดิจิทัล การติดต่อสื่อสารไร้สาย และสื่อสมัยใหม่ ซึ่งสิ่งต่างๆ เหล่านี้มีคุณอนันต์ แต่ก็มีโทษมหันต์หากใช้ไม่ถูกทิศทาง ซึ่งการดูแลเด็กยุคนี้ ต้องใช้ “ความรัก” “ความเข้าใจ” 
        บนพื้นฐานของความ “หลากหลาย” และ “แตกต่าง” จะใช้การบังคับอย่างที่ผ่านๆมามิได้อีกต่อไป
        <br><br>
        โครงการ <strong>“เด็กดีของแผ่นดิน”</strong> จึงเกิดขึ้น โดยให้ครูดีของแผ่นดิน ได้สร้างเด็กดีของแผ่นดิน หรือชื่อภาษาอังกฤษว่า Z-iDol คือ เด็กและเยาวชนรุ่นใหม่ ในยุค Generation Z ที่สามารถค้นพบความต้องการ
        ของตนเอง สามารถพัฒนาตนเองตามความต้องการนั้น จนประสบความสำเร็จในโลกศตวรรษที่ 21 และเป็นแบบอย่างที่ดีให้กับเพื่อนๆ ได้อย่างภาคภูมิใจ 
        <br><br>
        ในมุมมองของการพัฒนาและปรัชญา เด็กดีของแผ่นดิน หรือ Z-iDol คำว่า Z (ซี) พ้องเสียงกับคำว่า See ที่แปลว่าเห็น ซึ่งสอดคล้องกับปรัชญาของการศึกษาที่ การจะพัฒนาตนเองได้นั้น 
        ต้องเริ่มจากการมองตนเอง เห็นตนเอง เข้าใจตนเอง จึงจะพัฒนาตนเองได้ และสอดคล้องกับความหมายของคำว่า ศึกษา ที่มาจากภาษาบาลีว่า สิกขา ซึ่งมาจาก ส กับ อิกข และอาปัจจัย ที่แปลว่า 
        การเห็นตนเอง นั่นเอง โดยจะต้องมองตนเองก่อน ทั้งในเรื่องพฤติกรรม (ศีล) ความคิด ความตั้งใจ (สมาธิ) และสติปัญญา (ปัญญา) ซึ่งเป็นหัวใจสำคัญในการพัฒนาทรัพยากรมนุษย์ให้มีคุณค่าในทุกระดับชั้น
        <br><br>
        ซึ่งการจะพัฒนาตนเองได้อย่างยั่งยืนนั้น โครงการมุ่งเน้นไปที่การเปลี่ยนแปลงนิสัย ไม่ว่าจะเป็นการละเลิกนิสัยที่ไม่ดี หรือสร้างนิสัยดีๆ ให้เกิดขึ้น หรือการทำจิตใจให้เข้มแข็ง (ละชั่ว ทำดี ทำจิตใจให้ผ่องใส) 
        โดยใช้ทฤษฎีเปลี่ยนนิสัยใน 21 วัน จากนั้นขยายผลสู่การมีอาชีพ มีงานทำ ตามพระบรมราโชบายด้านการศึกษาของพระบาทสมเด็จพระวชิรเกล้าเจ้าอยู่หัว รวมถึงทักษะชีวิตในศตวรรษที่ 21 เช่น ความคิดสร้างสรรค์ 
        การทำงานเป็นทีม เป็นต้น ไม่ใช่เรียนดี กิจกรรมเด่น คุณธรรมเลิศ แต่ไม่สามารถเลี้ยงชีพได้ ช่วยเหลือตัวเองไม่ได้ ช่วยเหลือผู้อื่นไม่ได้ หากแต่ต้องดี เก่ง และประกอบสัมมาอาชีพได้ นำมาซึ่งประสิทธิภาพและ
        ประสิทธิผลสูงสุด นั่นคือ ความสุขและความสำเร็จในชีวิตที่แท้จริงนั่นเอง
      </p>
    </article>
    <!-- ################################################################################################ -->
    <div style="border-bottom:3px solid #59A209;">
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px;"><strong>9 ขั้นตอนการเข้าร่วมกิจกรรม Z-iDol</strong></span></p>
      <div id="gallery" style="margin-top:50px">
        <ul class="nospace clear">
          <li class="one_third first zoom11"><a href="images/ขั้นตอนร่วม ZiDol01.jpg"><img src="images/ขั้นตอนร่วม ZiDol01.jpg" alt="ขั้นตอนร่วม ZiDol01"></a></li>
          <li class="one_third zoom11"><a href="images/ขั้นตอนร่วม ZiDol02.jpg"><img src="images/ขั้นตอนร่วม ZiDol02.jpg" alt="ขั้นตอนร่วม ZiDol02"></a></li>
          <li class="one_third zoom11"><a href="images/ขั้นตอนร่วม ZiDol03.jpg"><img src="images/ขั้นตอนร่วม ZiDol03.jpg" alt="ขั้นตอนร่วม ZiDol03"></a></li>
          <li class="one_third first zoom11"><a href="images/ขั้นตอนร่วม ZiDol04.jpg"><img src="images/ขั้นตอนร่วม ZiDol04.jpg" alt="ขั้นตอนร่วม ZiDol04"></a></li>
          <li class="one_third zoom11"><a href="images/ขั้นตอนร่วม ZiDol05.jpg"><img src="images/ขั้นตอนร่วม ZiDol05.jpg" alt="ขั้นตอนร่วม ZiDol05"></a></li>
          <li class="one_third zoom11"><a href="images/ขั้นตอนร่วม ZiDol06.jpg"><img src="images/ขั้นตอนร่วม ZiDol06.jpg" alt="ขั้นตอนร่วม ZiDol06"></a></li>
          <li class="one_third first zoom11"><a href="images/ขั้นตอนร่วม ZiDol07.jpg"><img src="images/ขั้นตอนร่วม ZiDol07.jpg" alt="ขั้นตอนร่วม ZiDol07"></a></li>
          <li class="one_third zoom11"><a href="images/ขั้นตอนร่วม ZiDol08.jpg"><img src="images/ขั้นตอนร่วม ZiDol08.jpg" alt="ขั้นตอนร่วม ZiDol08"></a></li>
          <li class="one_third zoom11"><a href="images/ขั้นตอนร่วม ZiDol09.jpg"><figure><img src="images/ขั้นตอนร่วม ZiDol09.jpg" alt="ขั้นตอนร่วม ZiDol09"></figure></a></li>
        </ul>
      </div>
    </div>
    <!-- ################################################################################################ -->
    <ul class="fs-32 textlink" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
      <li><a href="images/เด็กดีของแผ่นดิน Logo.jpg">สัญลักษณ์เด็กดีของแผ่นดิน</a></li>
      <li><a href="docs/ร่วมโครงการ/เด็กดี/รายละเอียดโครงการเด็กดี.pdf">ดาวน์โหลดรายละเอียดโครงการ</a></li>
      <li>ต้นกล้าแห่งแผ่นดิน 6 แบบ
        <ul style="list-style-type:disc; text-decoration:none;">
          <li><a href="images/ต้นกล้าของแผ่นดิน/แบบที่ 1.jpg">แบบที่ 1</a></li>
          <li><a href="images/ต้นกล้าของแผ่นดิน/แบบที่ 2.jpg">แบบที่ 2</a></li>
          <li><a href="images/ต้นกล้าของแผ่นดิน/แบบที่ 3.jpg">แบบที่ 3</a></li>
          <li><a href="images/ต้นกล้าของแผ่นดิน/แบบที่ 4.jpg">แบบที่ 4</a></li>
          <li><a href="images/ต้นกล้าของแผ่นดิน/แบบที่ 5.jpg">แบบที่ 5</a></li>
          <li><a href="images/ต้นกล้าของแผ่นดิน/แบบที่ 6.jpg">แบบที่ 6</a></li>
        </ul>
			</li>
			<li><a href="docs/จดหมาย/จดหมาย17/จม.63ผู้เกี่ยวข้อง.pdf">หนังสือแจ้งปฏิทินการดำเนินโครงการฯ ปี 2563</a></li>
			<li style="text-align: center;"><a href="https://www.instagram.com/zidol_2563/?hl=th"><img src="images/ZiDol_IG.jpg" alt="ZiDol_IG"><br>กดติดตามได้ที่นี้</a></li>
    </ul>
		<!-- ################################################################################################ -->



		<div class="one_first">
			<div class="btnJoin" style="background-color: gray">
				<a href="images/ประกาศปิดโครงการ.jpg" target="_blank"><h7>โครงการเด็กดีของแผ่นดิน</h7></a>
			</div>
		</div>





		<?php
			$sqllogin = "SELECT * FROM `login` 
			WHERE (ID='$ID' AND occup_code LIKE 'OcF%') 
			OR (ID='$ID' AND occup_code IS NULL)
			OR (ID='$ID' AND occup_code='') ";
			$relogin = mysqli_query($con,$sqllogin);
			$rowlogin = mysqli_fetch_array($relogin);

			if(mysqli_num_rows($relogin) != 0 || !isset($ID)) { ?>
				<div class="one_first">
					<div class="btnJoin"> <?php
						if(strpos($rowlogin['basic_score_status'], 'Approve แล้ว') !== false) { // กรณีที่ได้รับการ Approve ให้เข้าหน้า Print เกียรติบัตร ?>
							<a href="popPrintbasic_score4stu_cer.php"><h7 class="blink3">ผ่านโครงการเด็กดีของแผ่นดินขั้นพื้นฐานแล้วค่ะ</h7></a> <?php
						}elseif(strpos($rowlogin['basic_score_status'], 'ยืนยันแล้ว') !== false) { // กรณีที่มีการยืนยันตัวตนแล้ว ให้เข้าหน้า registZidol.php ?>
							<a href="registZidol.php"><h6 class="blink3">โครงการเด็กดีของแผ่นดินขั้นพื้นฐาน</h6></a> <?php
						}else{ // กรณีที่เข้ามาครั้งแรก ให้เข้าหน้ายืนยันตัวตน ที่หน้า registZidol_step1.php ?>
							<a href="registZidol_step1.php"><h6>เข้าร่วมโครงการเด็กดีของแผ่นดิน ขั้นพื้นฐาน</h6></a> <?php
						}
						// NOTE: ที่ใช้ strpos ในการกรอง เพราะอยากจะให้เคสที่ไม่มีคำว่า "Approve แล้ว" ทั้งหมดไปเข้า Loop ที่2 แต่ถ้ามีคำว่า "Approve แล้ว" ใน basic_score_status ก็ให้เข้าหน้า print เกียรติบัตร ?>
					</div>
				</div> <?php
			}
		?>
    <div class="one_first m-t-50">
      <div class="btnJoin">
        <a href="เกี่ยวกับการบริจาค.php"><h6>สนับสนุนมูลนิธิ</h6></a>
      </div>
    </div>
    <!-- ################################################################################################ -->
  </main>
</div>
<!-- End Content 01 - โครงการเด็กดีของแผ่นดิน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>