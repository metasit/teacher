<?php 
	session_start();
	if(!isset($_SESSION['email'])) { // Check if user doesn't log in, then go to login page
		header('location: login.php');
	}else{
		if($_SESSION['level'] == 'admin') {
			include ('condb.php');
			$order_group = $_GET['order_group'];
		}else{
			header("Location: javascript:history.go(-1);");
		}
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body class="BG-diamond">
<div class="hoc clear" style="width:150px;">
		<div class="BG-green1 m-t-50 center fs-20" style="color:white; padding:10px; border-radius:5px; cursor:pointer;" 
		onmousemove="this.style.backgroundColor='transparent'; this.style.border='1px solid white';" 
		onmouseout="this.style.backgroundColor='rgb(72,160,0)'; this.style.border='none';"
		onclick="javascript:this.style.display='none'; window.print();">พิมพ์ใบปะหน้า</div>
</div>

	<div class="BG-white m-l-r-auto m-t-50 m-b-50" style="width:984px;">
		<div class="hoc clear p-t-60 p-b-80">
			<!-- Sender -->
			<div style="color:black; font-family: RSUText; padding:40px 0; line-height:70px; letter-spacing:5px;">
				<strong style="font-size:70px;">
					ผู้ส่ง: <br>
					มูลนิธิครูดีของแผ่นดิน<br>
				</strong>
				<div style="font-size:30px; font-family:initial;">
					103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม เขตพระนคร กรุงเทพ 10200<br>
					Tel: 02-0011515<br>
					www.thaisuprateacher.org<br>
				</div>
			</div>
			<!-- Reciever -->
			<?php
				$sqlorders="SELECT * FROM orders WHERE order_group='$order_group' ";
				$resultorders=mysqli_query($con,$sqlorders);
				$roworders=mysqli_fetch_array($resultorders);
			?>
			<div style="color:black; font-family: RSUText; padding:40px 0; margin-top:70px; line-height:70px; letter-spacing:5px; border-top:10px double black;">
				<strong style="font-size:40px;">
					เลขที่ใบสั่งซื้อ: <?php echo $roworders['order_number']; ?> <br>
				</strong>
				<strong style="font-size:70px; margin-top:20px;">
					ผู้รับ: <br>
					<?php echo $roworders['contact_name']; ?><br>
				</strong>
				<div style="font-size:30px; font-family:initial;">
					Tel: <?php echo $roworders['contact_number']; ?><br>
					<?php echo $roworders['shipping_address']; ?>
				</div>
			</div>
		</div>
	</div>

</body>
</html>