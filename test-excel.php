<?php
 
require 'vendor/autoload.php';
 
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
 
$inputFileName = 'test.xlsx';//ชื่อไฟล์ Excel ที่ต้องการอ่านข้อมูล
$spreadsheet = IOFactory::load('test.xlsx');
$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
 exit();
$i = 0;
$j = 1;
$data = [];
foreach($sheetData as $s => $k){
    foreach($k as $g){
        $i++;
        $data[$j][] = $g;
    }
    $j++;
}
?>
 
<table border='1'>
    <tr>
        <th>Customer Code</th>
        <th>Customer Name</th>
        <th>Customer Address</th>
    </tr>
    <?php
        $i = 1;
        foreach($data as $q){
            
            if($i > 1){
                $b = trim($q[0]);
                $c = trim($q[1]);
                $d = trim($q[2]);
                
                echo "<tr>";
                echo "<td>$b</td>";
                echo "<td>$c</td>";
                echo "<td>$d</td>";
                echo "</tr>";
            }
            $i++;
        }
    ?>
</table>    