<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
				<?php if (strpos($_SESSION['email'], '@') !== false) {
								if ($_SESSION["level"]=="admin") { //Admin Session ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า-en.php">ระบบหลังบ้านร้านค้า</a></li>
											<li><a href="ระบบหลังบ้านประเมินครู-en.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*A*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านประเมินครู-en.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*B*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า-en.php">ระบบหลังบ้านร้านค้า</a></li>
										</ul>
									</li>
          <?php }elseif ($_SESSION['level']=='memberGeneral') { //General Member Session ?>
								<li style="color:black; background-color:white;"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> General Member</a>
									<ul>
										<li><a href="editprofile-en.php">Edit your Profile</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberSilver') { //Member Silver Session ?>
								<li style="color:white; background-color:rgb(169,169,169);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> Silver Member</a>
									<ul>
										<li><a href="editprofile-en.php">Edit your Profile</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberGold') { //Member Gold Session ?>
								<li style="color:white; background-color:rgb(180,147,31);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> Gold Member</a>
									<ul>
										<li><a href="editprofile-en.php">Edit your Profile</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberDiamond') { //Member Diamond Session ?>
								<li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> Diamond Member</a>
									<ul>
										<li><a href="editprofile-en.php">Edit your Profile</a></li>
									</ul>
								</li>
          <?php } ?>
								<li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
        <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login-en.php"><i class="fas fa-user-lock"></i> <text1>Log in</text1></a></li>
                <li class="faicon-login"><a href="signup-en.php"><i class="fas fa-file-signature"></i> <text1>Sign up</text1></a></li>
				<?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
                  <ul>
                    <li><a href="ข่าวสาร018.php">ภาษาไทย</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                    <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก-en.php">สนับสนุนของที่ระลึก</a></li>
                  </ul>
                </li>
        </ul>
			</nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index-en.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index-en.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index-en.php">Home</a></li>
          <li><a href="เกี่ยวกับเรา-en.php">About us</a></li>
          <li class="joinus-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>Join us</a>
            <ul>
              <li><a href="ร่วมโครงการ-en.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <li><a href="โครงการครูดีของแผ่นดิน-en.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา-en.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA-en.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน-en.php">โครงการอาสาของแผ่นดิน</a></li>
            </ul>
          </li>
          
          <li><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">Innovation Library</a></li>
          <li><a href="ติดต่อเรา-en.php">Contact us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="ข่าวสารรวมlatest-en.php"> News</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> ประชุมพัฒนาตัวชี้วัดและเครื่อมมือวัดศน.ดีของแผ่นดิน</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - ข่าวสาร018 -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <article style="text-align:center;">
      <!-- ################################################################################################ -->
      <p class="font-x3">
        <span style="color:rgb(180,147,31); line-height:80px">
        <strong>ประชุมพัฒนาตัวชี้วัดและเครื่อมมือวัดศน.ดีของแผ่นดิน</strong>
      </p>      
      <!-- ################################################################################################ -->
      <p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				วันนี้ (18 มิ.ย.63) มูลนิธิครูดีของแผ่นดิน ได้เชิญผู้ทรงคุณวุฒิและศึกษานิเทศก์จากหลายหน่วยงาน เช่น
				<br>
				ดร.ชื่นฤดี บุตะเขียว ผอ.กลุ่มขับเคลื่อนการปฎิรูปประเทศยุทธศาสตร์ชาติ
				<br>
				นางสาวอารีย์ อัจฉริยวนิช ผู้อำนวยการกลุ่มโครงการพระราชดำริ สำนักบูรณาการการศึกษา สป.ศธ.
				<br>
				นางมารศรี เหล่าบุญชัย ศึกษานิเทศก์ชำนาญการพิเศษ สำนักงานศึกษาธิการจังหวัดนครปฐม
				<br>
				นายประเทือง เข็มเพชร และว่าที่ร้อยตรีหญิงเสาวลักษณ์ สินสมุทร์ ศึกษานิเทศก์ชำนาญการพิเศษ สพป.พิจิตร เขต 2
				<br>
				และนายจิรประวัติ ศรีวัฒนทรัพย์ ศึกษานิเทศก์ชำนาญการ สำนักงาน กศน.
				<br>
				ประชุมปรึกษาหารือพัฒนาตัวชี้วัดและเครื่องมือวัดศึกษานิเทศก์ดีของแผ่นดิน ณ สำนักงานมูลนิธิครูดีของแผ่นดิน กรุงเทพมหานคร
				<br><br>
				<img src="images/รูปข่าวสาร18/รูปข่าวสาร18-01.jpg" alt="รูปข่าวสาร18-01">
				<br><br>
				<img src="images/รูปข่าวสาร18/รูปข่าวสาร18-04.jpg" alt="รูปข่าวสาร18-04">
        <br><br>
				<img src="images/รูปข่าวสาร18/รูปข่าวสาร18-02.jpg" alt="รูปข่าวสาร18-02">
				<br><br>
				<img src="images/รูปข่าวสาร18/รูปข่าวสาร18-03.jpg" alt="รูปข่าวสาร18-03">
				<br><br>
				<img src="images/รูปข่าวสาร18/รูปข่าวสาร18-05.jpg" alt="รูปข่าวสาร18-05">
				<br><br>
				<img src="images/รูปข่าวสาร18/รูปข่าวสาร18-06.jpg" alt="รูปข่าวสาร18-06">
				<br><br>
				<img src="images/รูปข่าวสาร18/รูปข่าวสาร18-07.jpg" alt="รูปข่าวสาร18-07">
				<br><br>
				<img src="images/รูปข่าวสาร18/รูปข่าวสาร18-08.jpg" alt="รูปข่าวสาร18-08">
      </p>
      <!-- ################################################################################################ -->
    </article>
  </main>
</div>
<!-- End Content 01 - ข่าวสาร018 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<?php include('includes/footer-en.php'); ?>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>