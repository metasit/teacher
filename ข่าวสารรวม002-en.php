<?php
	session_start();
	require_once('condb.php');
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop-en.php'); ?>
                  <ul>
                    <li><a href="ข่าวสารรวม002.php">ภาษาไทย</a></li>
                  </ul>
<?php include('includes/headerBottom-en.php'); ?>
<!-- End Header -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#"  onclick="window.location.reload(true);">News</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - ข่าวสารรวม002 -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
  <section class="hoc container clear">
    <!-- ################################################################################################ -->
		<div style="background-color:rgb(0,6,27); padding:20px 20px 0; border-radius:10px;">
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
      <div class="group latest">
        <article class="one_third first row4">
          <div class="center">
            <figure><a href="ข่าวสาร013-en.php"><img src="images/รูปหน้าปกข่าวสาร13.jpg" alt="รูปหน้าปกข่าวสาร13"></a>
              <figcaption>
                <time datetime="2020-2-13T08:15+00:00"><strong><font size="5.5px">13</font></strong><em>Mar</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">รองเลขา กคศ. พร้อมหนุนครูดีของแผ่นดิน ให้ก้าวหน้าในเส้นทางวิชาชีพ<br></h4>
            <p style="color:rgb(243, 243, 243)">วันนี้ (13 มีนาคม 2563) ผู้แทนมูลนิธิครูดีของแผ่นดิน นำโดย นางปาลิดา กุลรุ่งโรงจน์ ประธานคณะอนุกรรมการโครงการเครือข่ายครูดีของแผ่นดิน นายฐกร พฤฒิปูรณี [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร013-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร012-en.php"><img src="images/รูปหน้าปกข่าวสาร12.jpg" alt="รูปหน้าปกข่าวสาร12"></a>
              <figcaption>
                <time datetime="2020-02-14T08:15+00:00"><strong><font size="5.5px">8</font></strong><em>Mar</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31);">“พล.อ.เอกชัย” ประชุมกรรมการมูลนิธิครูดีของแผ่นดิน พร้อมขับเคลื่อนโครงการสร้างคนดี ตลอดปี 2563</h4>
            <p style="color:rgb(243, 243, 243)">เมื่อวันที่ 8 มิ.ย. 2563 ที่สำนักงานมูลนิธิครูดีของแผ่นดิน พล.อ.เอกชัย ศรีวิลาศ ประธานมูลนิธิครูดีของแผ่นดิน เป็นประธานในการประชุมคณะกรรมการมูลนิธิ ครั้งที่ 1/2563 [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร012-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร011-en.php"><img src="images/ดร.กวินทร์เกียรติ นนธ์พละ.jpg" alt="ดร.กวินทร์เกียรติ นนธ์พละ"></a>
              <figcaption>
                <time datetime="2020-02-14T08:15+00:00"><strong><font size="5.5px">3</font></strong><em>Mar</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31);">แสดงความยินดีกับรองเลขาธิการ ผู้ตรวจราชการ และผู้เชี่ยวชาญกระทรวงศึกษาธิการ</h4>
            <p style="color:rgb(243, 243, 243)">ผู้แทนมูลนิธิครูดีของแผ่นดิน นำโดย นางปาลิดา กุลรุ่งโรจน์ ประธานอนุกรรมการโครงการเครือข่ายครูดีของแผ่นดิน และนายฐกร พฤฒิปูรณี กรรมการและเลขานุการมูลนิธิครูดีของแผ่นดิน นำคณะเข้าเยี่ยมคารวะแสดงความยินดีเนื่องในโอกาสเข้ารับตำแหน่งใหม่ของผู้บริหารระดับสูงของกระทรวงศึกษาธิการ [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร011-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
      </div>
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
      <div class="group latest">
        <article class="one_third first row4">
          <div class="center">
            <figure><a href="ข่าวสาร010-en.php"><img src="images/รูปข่าวประชุมครูเอกชน-01.jpg" alt="รูปข่าวประชุมครูเอกชน-01"></a>
              <figcaption>
                <time datetime="2020-2-13T08:15+00:00"><strong><font size="5.5px">3</font></strong><em>Mar</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">“Design Thinking เพื่อการสร้างวินัยผู้เรียน” หนึ่งเดียวเพื่อพัฒนาครูโรงเรียนเอกชนด้านคุณธรรมจริยธรรม ปี 2563<br></h4>
            <p style="color:rgb(243, 243, 243)">ตามที่นางกนกวรรณ วิลาวัลย์ รัฐมนตรีช่วยว่าการกระทรวงศึกษาธิการ มีนโยบายที่จะพัฒนาครูเอกชนให้มีทักษะการจัดการศึกษาในศตวรรษที่ 21 จึงจัดทำหลักสูตรการอบรมเป็น 3 ระยะ [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร010-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร009-en.php"><img src="images/รูปข่าวBIA_11.jpg" alt="รูปข่าวBIA_11"></a>
              <figcaption>
                <time datetime="2020-02-14T08:15+00:00"><strong><font size="5.5px">14</font></strong><em>Feb</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31);">มูลนิธิครูดีของแผ่นดิน จับมือกับ Google จัดโรดโชว์ “กิจกรรมสร้างเสริมการใช้เทคโนโลยีดิจิทัลอย่างสร้างสรรค์ ปลอดภัย ผ่านหลักสูตร Be Internet Awesome”</h4>
            <p style="color:rgb(243, 243, 243)">วันที่ 14 กุมภาพันธ์ พ.ศ.2563 ณ โรงเรียนพญาไท กรุงเทพ มูลนิธิครูดีของแผ่นดิน ร่วมกับ Google Thailand ได้จัดงานสร้างเสริมการใช้ [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร009-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร008-en.php"><img src="images/รูปข่าว007_01.jpg" alt="รูปข่าว007_01"></a>
              <figcaption>
                <time datetime="2020-2-13T08:15+00:00"><strong><font size="5.5px">13</font></strong><em>Feb</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">อบรม “อาสาของแผ่นดิน” สาขาสารสนเทศ รุ่นที่1<br></h4>
            <p style="color:rgb(243, 243, 243)">เมื่อเวลาและโอกาสของคุณมาถึง คุณจะได้เป็นส่วนหนึ่งในการแสดงศักยภาพ เพื่อพัฒนาวงการศึกษาไทยให้ยั่งยืน ด้วยสองมือของเหล่าจิตอาสาของแผ่นดิน  [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร008-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
			</div>
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
      <div class="group latest">
        <article class="one_third first row4">
          <div class="center">
            <figure style="background-color:white;"><a href="ข่าวสาร007-en.php"><img src="images/อาสากัลยาณมิตร-01.jpg" alt=""></a>
              <figcaption>
                <time datetime="2020-02-8T08:15+00:00"><strong><font size="5.5px">8</font></strong><em>Feb</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">รับสมัคร “อาสาของแผ่นดิน” สาขากัลยาณมิตร</h4>
            <p style="color:rgb(243,243,243)">ขอเชิญผู้บริหารและครู ที่มีจิตอาสา ร่วมสมัครเป็น “อาสาของแผ่นดิน” สาขากัลยาณมิตร เพื่อทำหน้าที่สื่อสารการดำเนินโครงการ แก่เพื่อนผู้บริหารและครูที่เข้าร่วมโครงการ รับจำนวนจำกัดเพียง 60 ท่าน [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร007-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร006-en.php"><img src="images/รูปข่าว006-01.jpg" alt="รูปข่าว006-01"></a>
              <figcaption>
                <time datetime="2020-02-06T08:15+00:00"><strong><font size="5.5px">6</font></strong><em>Feb</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">“ข่าวดีของชาว กศน.- สช.” มูลนิธิครูดีของแผ่นดิน เข้าพบท่านปลัดกระทรวงศึกษาธิการเพื่อปรึกษาหารือโครงการ</h4>
            <p style="color:rgb(243, 243, 243)">วันที่ 6 กุมภาพันธ์ พ.ศ.2563 ผู้แทนมูลนิธิครูดีของแผ่นดิน นำโดย นางปาลิดา กุลรุ่งโรจน์ และนายฐกร พฤฒิปูรณี 
              เข้าพบท่านปลัดกระทรวงศึกษาธิการ [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร006-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร005-en.php"><img src="images/มูลPosterหน้ารวม.png" alt=""></a>
              <figcaption>
                <time datetime="2020-01-27T08:15+00:00"><strong><font size="5.5px">27</font></strong><em>Jan</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">มูลนิธิครูดีของแผ่นดิน ร่วมกับ สถาบันการแพทย์ฉุกเฉินแห่งชาติ ร่วมกันจัดกิจกรรมการเรียนรู้ด้านการกู้ชีพขั้นพื้นฐาน (CPR)</h4>
            <p style="color:rgb(243, 243, 243)">เมื่อวันที่ 27 มกราคม พ.ศ.2563 มูลนิธิครูดีของแผ่นดิน ร่วมกับ สถาบันการแพทย์ฉุกเฉินแห่งชาติ ร่วมกันจัดกิจกรรมการเรียนรู้ด้านการกู้ชีพขั้นพื้นฐาน (CPR) และ [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร005-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
			</div>
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
		</div>
		<!-- ################################################################################################ -->
  </section>
</div>
<!-- End Content 01 - ข่าวสารรวม002 -->
<!-- ################################################################################################ -->
<!-- Start Pagination -->
<div class="wrapper row3 coloured" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
  <main class="hoc container clear">
    <!-- ################################################################################################ -->
    <div class="content">
      <nav class="pagination">
				<ul>
          <li><a href="ข่าวสารรวมlatest-en.php">&laquo; Previous</a></li>
          <li><a href="ข่าวสารรวมlatest-en.php">3</a></li>
					<li class="current"><strong>2</strong></li>
          <li><a href="ข่าวสารรวม001-en.php">1</a></li>
          <li><a href="ข่าวสารรวม001-en.php">Next &raquo;</a></li>
				</ul>
      </nav>
    </div>
    <!-- ################################################################################################ -->
  </main>
</div>
<!-- End Pagination -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer-en.php'); ?>
<!-- End FooterCopyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>