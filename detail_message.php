<?php
	session_start();
	require_once('condb.php');
    $id = $_GET['id'];
    $sql_list  ="SELECT * FROM message WHERE id_message = $id ";

    $list = $con->query($sql_list) ;
    while($row = $list->fetch_assoc()) {
        $data_list[] = $row; 
    }
    $title  = $data_list[0]['title'];
    $detail = $data_list[0]['Detail'];
    // $detail = $data_list[0]['detail_first'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">

<style>
@font-face {
  font-family: 'korbauMed'; /*a name to be used later*/
  src:  url('content-setting/backend/font/korbau.ttf')  format('truetype') 
}
@font-face {
  font-family: 'Niramit'; /*a name to be used later*/
  src:  url('content-setting/backend/font/TH Niramit AS.ttf')  format('truetype') 
}
@font-face {
  font-family: 'CSChatThai'; /*a name to be used later*/
  src:  url('content-setting/backend/font/CSChatThai.ttf')  format('truetype') 
}
@font-face {
  font-family: 'Emmal'; /*a name to be used later*/
  src:  url('content-setting/backend/font/EmmaliDEMO-Thin.otf')  format('truetype') 
}
</style>

</head>
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="ข่าวสาร022-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="ข่าวสารรวมlatest.php"> ข่าวสาร</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> <?php echo $title; ?> </a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - จดหมาย022 -->
<div class="wrapper row3 ">
  <main class="hoc container clear"> 
    <article style="" >
      
      <p class="font-x3"><span style="color:black; line-height:80px;">
        <strong><?php echo $title; ?></strong>
      </p>

      <div style="color:black!important">
        <?php 
          echo $detail;
        ?>
      </div>
    </article>
  </main>
  
</div>

    

<!-- End Content 01 - จดหมาย022 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->






<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<!-- <script src="js/custom.js"></script> -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>