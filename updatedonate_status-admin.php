

<?php
	session_start();
	require_once('condb.php');

	$donate_status = $_GET['donate_status'];
	$donate_id = $_GET['donate_id'];
	$win_scroll = $_GET['win_scroll'];

	$sqldonate = "SELECT donate_amount, donate_remark FROM `donate` WHERE donate_id='$donate_id' ";
	$reDN = mysqli_query($con, $sqldonate);
	$rowDN = mysqli_fetch_array($reDN);

	if($donate_status == 'Approve แล้ว') {

		if($rowDN['donate_amount'] == '') {

			echo '<script>';
				echo "alert('กรุณาใส่จำนวนเงินที่บริจาคเพื่อนำไปพิมพ์ใบเสร็จได้ค่ะ');";
				echo "window.location.replace('ระบบหลังบ้านบริจาค.php?win_scroll=$winScroll')";
			echo '</script>';

		}else{

			// Set bill_code
			$sqlsetbillvalue = "SELECT MAX(CAST(SUBSTRING(bill_code,-5) AS UNSIGNED)) AS max_bill_code FROM `donate` ";
			$resbv = mysqli_query($con, $sqlsetbillvalue);
			$rowsbv = mysqli_fetch_array($resbv);

			$bill_type = 'DN';
			$slip_date = date("Y-m-d H:i:s");
			$slip_year = substr(date('Y', strtotime($slip_date))+543,-2);
			$bill_num = sprintf('%05d', $rowsbv['max_bill_code']+1);
			$bill_code = $bill_type.$slip_year.$bill_num;

			/* Update donate_status in donate table */
			$sql = "UPDATE `donate` SET donate_status='$donate_status', bill_code='$bill_code' WHERE donate_id='$donate_id' ";
			$re = $con->query($sql) or die($con->error);

			header('location: ระบบหลังบ้านบริจาค.php?win_scroll='.$win_scroll);

		}

	}elseif($donate_status == 'ปฏิเสธSlip') {

		if($rowDN['donate_remark'] == '') {

			echo '<script>';
				echo "alert('กรุณาใส่เหตุผลที่ปฏิเสธที่ช่องหมายเหตุด้วยค่ะ');";
				echo "window.location.replace('ระบบหลังบ้านบริจาค.php?win_scroll=$winScroll')";
			echo '</script>';

		}else{

			/* Update donate_status in donate table */
			$sql = "UPDATE `donate` SET donate_status='$donate_status' WHERE donate_id='$donate_id' ";
			$re = $con->query($sql) or die($con->error);

			header('location: ระบบหลังบ้านบริจาค.php?win_scroll='.$win_scroll);

		}

	}else{

		echo '<script>';
			echo "alert('เกิดข้อผิดพลาด กรุณาติดต่อมูลนิธิ // ErrorAt67: updatedonate_status-admin.php');";
			echo "<script>window.history.go(-1)</script>";
		echo '</script>';

	}

?>