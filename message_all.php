<?php
	session_start();
	require_once('condb.php');

  
    $pg = 0;
    if(in_array("pg", array_keys($_GET))){
        $pg = $_GET['pg'];
    }
    else {
        $pg = 1;
    }

    if($pg==1){
        $start = $pg -1;
    }
    else {
        $start =(($pg - 1) * 9 ) +1;
    }
    $end = $pg *9;
    if($start!=0) $start - 1;

    // echo $start;
    // exit();
    if($start>0){
      $t_start = $start - 1;
    }
    else {
      $t_start = $start;
    }
     
    
    $sql_list  ="SELECT * FROM message  order by day_event desc LIMIT $t_start,9  ";
    $list = $con->query($sql_list);  
    
    $number_count = "SELECT count(*) FROM message";
    $result = $con->query($number_count);  
    while($row_token = $result->fetch_assoc()){
      $number_count =  $row_token['count(*)'];
    }
    // var_dump($number_count);
    // exit();
    // while($row = $list->fetch_assoc()) {
    //         $title = $row['title'];
    //         $detail = $row['detail'];
    //         $name = $row['name'];
    // }
    if($pg>3){
      $st = $pg -2;
      $en = $pg +2;
      for ($x=$st; $x <=$en ; $x++) { 
        if( ($x - 1) <=$number_count/9)
          $paginetion[] =$x; 
      } 
    }
    else {
      if(true){
        $pg_token = ceil($number_count/9);
        for ($x=1; $x <=$pg_token ; $x++) { 
          if(($x-1)<=$number_count/9)
          $paginetion[] = $x;
          # code...
        } 
      }
    }
    // var_dump($paginetion);
    // echo "<br>";
    // var_dump($pg_token);
    // echo "<br/>";
    // var_dump($number_count);
    // echo "<br/>";
    // var_dump(ceil($number_count/9));
    // exit();


?>
<!DOCTYPE html>
<html lang="thai">
<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="ข่าวสารรวมlatest-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#"  onclick="window.location.reload(true);">ข่าวสาร</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - ข่าวสารรวมlatest -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
  <section class="hoc container clear">
    <!-- ################################################################################################ -->
    <div style="background-color:rgb(0,6,27); padding:20px 20px 0; border-radius:10px;">
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
      <div class="group latest">
          <?php 
          $number = 1;

          while($row = $list->fetch_assoc()){
            // one_third first row4
            $all_month = ["T","ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."];
            $number_month = explode("-",explode(" ",$row['day_event'])[0])[1];
            $month = $all_month[$number_month+0];
          ?>
        <article class="<?php  if($number % 3 == 1 || $number == 1  ) echo "one_third first row4"; else echo "one_third row4";  ?>">
          <div class="center">
            <figure>
            <a href="<?php echo "detail_message.php?id=".$row['id_message'] ?>">
            <?php echo '<img style="min-width:100%; height:308px;"    id="message-img" src="content-setting/backend/galangal/image/'.$row['file_name'].'">'; ?>  
                <!-- <img src="images/รูปหน้าปกข่าวสาร22.jpg" alt="รูปหน้าปกข่าวสาร22"> -->
            </a>
              <figcaption>
                <time datetime="2020-10-8T08:15+00:00"><strong><font size="5.5px">
                  <?php echo  explode("-",explode(" ",$row['day_event'])[0])[2]   ?>
                </font></strong><em><?php  echo $month; ?></em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)"> <?php  echo $row["title"]; ?> <br></h4>
            <p style="color:rgb(243, 243, 243)">
            <?php  echo $row["detail_first"]; ?>
            
            <footer><a style="font-size:17px" class="readnext" 
            href="<?php echo "detail_message.php?id=".$row['id_message'] ?>">อ่านต่อ &raquo;</a></footer>
          </div>
	    </article>
        <?php $number++; } ?>
        <!-- <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร021.php"><img src="images/รูปหน้าปกข่าวสาร21.jpg" alt="รูปหน้าปกข่าวสาร21"></a>
              <figcaption>
                <time datetime="2020-10-6T08:15+00:00"><strong><font size="5.5px">6</font></strong><em>ต.ค.</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">มูลนิธิครูดีของแผ่นดิน แสดงความยินดีผู้บริหารระดับสูง กระทรวงศึกษาธิการ<br></h4>
            <p style="color:rgb(243, 243, 243)">วันที่ 6 ตุลาคม 2563 ผู้แทนมูลนิธิครูดีของแผ่นดิน นำโดย นางปาลิดา กุลรุ่งโรจน์ ดร.กิตติกร คัมภีรปรีชา นายฐกร พฤฒิปูรณี เข้าแสดงความยินดี กับผู้บริหารระดับสูงกระทรวงศึกษาธิการ เนื่องใน [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร021.php">อ่านต่อ &raquo;</a></footer>
          </div>
        </article> -->
      </div>
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
      <div class="group latest">
        

			</div>
			<!-- ################################################################################################ -->
      <!-- ################################################################################################ -->
   
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
		</div>
		<!-- ################################################################################################ -->
  </section>
</div>
<!-- End Content 01 - ข่าวสารรวมlatest -->
<!-- ################################################################################################ -->
<!-- Start Pagination -->
<div class="wrapper row3 coloured" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
  <main class="hoc container clear">
    <!-- ################################################################################################ -->
    <div class="content">
      <nav class="pagination">
        <ul>
          <!-- <li><a href="message_all.php?pg=<?php echo $value-1   ?>">&raquo; ก่อนหน้า </a></li> -->
            <?php  foreach ($paginetion as $key => $value) { ?>
                <li class="<?php  if($value==$pg) echo "current" ?>" ><a href="message_all.php?pg=<?php echo $value   ?>"><?php echo $value; ?></a></li>
            <?php }  ?>
          <!-- <li><a href="message_all.php?pg=<?php echo $value+1   ?>">ถัดไป &raquo;</a></li> -->
        </ul>
      </nav>
    </div>
    <!-- ################################################################################################ -->
  </main>
</div>
<!-- End Pagination -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>
<script>
  document.body.style.backgroundColor = 'yellow';
  document.getElementById('')
  document.getElementsByClassName('');
  document.querySelector('');
  document.createElement('span')
</script>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>