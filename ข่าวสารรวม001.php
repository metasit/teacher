<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="ข่าวสารรวม001-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="window.location.reload(true);">ข่าวสาร</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - ข่าวสารรวม001 -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
  <section class="hoc container clear">
		<!-- ################################################################################################ -->
		<div style="background-color:rgb(0,6,27); padding:20px 20px 0; border-radius:10px;">
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
      <div class="group latest">
				<article class="one_third first row4">
          <div class="center">
            <figure><a href="ข่าวสาร004.php"><img src="images/ท่าน01.png" alt=""></a>
              <figcaption>
                <time datetime="2020-01-16T08:15+00:00"><strong><font size="5.5px">16</font></strong><em>ม.ค.</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">ท่านรัฐมนตรีว่าการกระทรวงศึกษาธิการ ให้เกียรติเยี่ยมชมบูธกิจกรรมของมูลนิธิครูดีของแผ่นดิน เนื่องในงานวันครู ครั้งที่ 64 <br></h4>
            <p style="color:rgb(243, 243, 243)">งานวันครู ครั้งที่ 64 วันที่ 16 มกราคม พ.ศ.2563 สำนักงานเลขาธิการคุรุสภา กำหนดหัวข้อแก่นสาระว่า “โลกก้าวไกล ครูไทยก้าวทัน สร้างสรรค์คุณภาพเด็กไทย” [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร004.php">อ่านต่อ &raquo;</a></footer>
          </div>
				</article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร003.php"><img src="images/ท่านอำนาจ วิชยานุวัติ.jpg" alt="ท่านอำนาจ วิชยานุวัติ"></a>
              <figcaption>
                <time datetime="2020-12-11T08:15+00:00"><strong><font size="5.5px">1</font></strong><em>ม.ค.</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">มูลนิธิครูดีของแผ่นดิน เข้าคารวะและขอพร เนื่องในวาระดิถีขึ้นปีใหม่ 2563 จากผู้บริหารระดับสูงกระทรวงศึกษาธิการ</h4>
            <p style="color:rgb(243, 243, 243)">ผู้แทนมูลนิธิครูดีของแผ่นดิน เข้าเยี่ยมคารวะและนำกระเช้าของขวัญมอบแด่ผู้บริหารระดับสูงกระทรวงศึกษาธิการ เนื่องในวารดิถีขึ้นปีใหม่ และ [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร003.php">อ่านต่อ &raquo;</a></footer>
          </div>
				</article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร002.php"><img src="images/รูปข่าวครบรอบ4ปี-02.jpg" alt=""></a>
              <figcaption>
                <time datetime="2019-12-11T08:15+00:00"><strong><font size="5.5px">14</font></strong><em>ธ.ค.</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">ครบรอบ 4 ปี โครงการเครือข่ายครูดีของแผ่นดิน และครบรอบ 2 ปี มูลนิธิครูดีของแผ่นดิน</h4>
            <p style="color:rgb(243, 243, 243)">มูลนิธิครูดีของแผ่นดิน โดย พลเอกเอกชัย ศรีวิลาศ ประธานมูลนิธิ ได้จัดงานครบรอบ 4 ปี โครงการเครือข่ายครูดีของแผ่นดิน เจริญรอยตามเบื้องพระยุคลบาท และ ครบรอบ 2 ปี [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร002.php">อ่านต่อ &raquo;</a></footer>
          </div>
				</article>
			</div>
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
      <div class="group latest">
        <article class="one_third first row4">
          <div class="center">
            <figure><a href="ข่าวสาร001.php"><img src="images/เตรียมพบ01.jpg" alt=""></a>
              <figcaption>
                <time datetime="2019-11-15T08:15+00:00"><strong><font size="5.5px">15</font></strong><em>พ.ย.</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31);">เตรียมพบกับโครงการศึกษานิเทศก์ดีของแผ่นดินที่จะเปิดตัวเร็วๆนี้</h4>
            <p style="color:rgb(243, 243, 243)">วันที่ 15 พฤศจิกายน พ.ศ.2562 มูลนิธิครูดีของแผ่นดิน นำโดย นายฐกร พฤฒิปูรณี กรรมการและเลขานุการมูลนิธิครูดีของแผ่นดิน [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร001.php">อ่านต่อ &raquo;</a></footer>
          </div>
				</article>
				<!--
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร003.php"><img src="images/ท่านอำนาจ วิชยานุวัติ.jpg" alt="ท่านอำนาจ วิชยานุวัติ"></a>
              <figcaption>
                <time datetime="2020-12-11T08:15+00:00"><strong><font size="5.5px">1</font></strong><em>ม.ค.</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">มูลนิธิครูดีของแผ่นดิน เข้าคารวะและขอพร เนื่องในวาระดิถีขึ้นปีใหม่ 2563 จากผู้บริหารระดับสูงกระทรวงศึกษาธิการ</h4>
            <p style="color:rgb(243, 243, 243)">ผู้แทนมูลนิธิครูดีของแผ่นดิน เข้าเยี่ยมคารวะและนำกระเช้าของขวัญมอบแด่ผู้บริหารระดับสูงกระทรวงศึกษาธิการ เนื่องในวารดิถีขึ้นปีใหม่ และ [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร003.php">อ่านต่อ &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร002.php"><img src="images/รูปข่าวครบรอบ4ปี-02.jpg" alt=""></a>
              <figcaption>
                <time datetime="2019-12-11T08:15+00:00"><strong><font size="5.5px">14</font></strong><em>ธ.ค.</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">ครบรอบ 4 ปี โครงการเครือข่ายครูดีของแผ่นดิน และครบรอบ 2 ปี มูลนิธิครูดีของแผ่นดิน</h4>
            <p style="color:rgb(243, 243, 243)">มูลนิธิครูดีของแผ่นดิน โดย พลเอกเอกชัย ศรีวิลาศ ประธานมูลนิธิ ได้จัดงานครบรอบ 4 ปี โครงการเครือข่ายครูดีของแผ่นดิน เจริญรอยตามเบื้องพระยุคลบาท และ ครบรอบ 2 ปี [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร002.php">อ่านต่อ &raquo;</a></footer>
          </div>
        </article>
			</div>
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
			<!--
      <div class="group latest">
				<article class="one_third first row4">
          <div class="center">
            <figure><a href="ข่าวสาร001.php"><img src="images/เตรียมพบ01.jpg" alt=""></a>
              <figcaption>
                <time datetime="2019-11-15T08:15+00:00"><strong><font size="5.5px">15</font></strong><em>พ.ย.</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31);">เตรียมพบกับโครงการศึกษานิเทศก์ดีของแผ่นดินที่จะเปิดตัวเร็วๆนี้</h4>
            <p style="color:rgb(243, 243, 243)">วันที่ 15 พฤศจิกายน พ.ศ.2562 มูลนิธิครูดีของแผ่นดิน นำโดย นายฐกร พฤฒิปูรณี กรรมการและเลขานุการมูลนิธิครูดีของแผ่นดิน [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร001.php">อ่านต่อ &raquo;</a></footer>
          </div>
				</article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร002.php"><img src="images/รูปข่าวครบรอบ4ปี-02.jpg" alt=""></a>
              <figcaption>
                <time datetime="2019-12-11T08:15+00:00"><strong><font size="5.5px">14</font></strong><em>ธ.ค.</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">ครบรอบ 4 ปี โครงการเครือข่ายครูดีของแผ่นดิน และครบรอบ 2 ปี มูลนิธิครูดีของแผ่นดิน</h4>
            <p style="color:rgb(243, 243, 243)">มูลนิธิครูดีของแผ่นดิน โดย พลเอกเอกชัย ศรีวิลาศ ประธานมูลนิธิ ได้จัดงานครบรอบ 4 ปี โครงการเครือข่ายครูดีของแผ่นดิน เจริญรอยตามเบื้องพระยุคลบาท และ ครบรอบ 2 ปี [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร002.php">อ่านต่อ &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร001.php"><img src="images/เตรียมพบ01.jpg" alt=""></a>
              <figcaption>
                <time datetime="2019-11-15T08:15+00:00"><strong><font size="5.5px">15</font></strong><em>พ.ย.</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31);">เตรียมพบกับโครงการศึกษานิเทศก์ดีของแผ่นดินที่จะเปิดตัวเร็วๆนี้</h4>
            <p style="color:rgb(243, 243, 243)">วันที่ 15 พฤศจิกายน พ.ศ.2562 มูลนิธิครูดีของแผ่นดิน นำโดย นายฐกร พฤฒิปูรณี กรรมการและเลขานุการมูลนิธิครูดีของแผ่นดิน [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร001.php">อ่านต่อ &raquo;</a></footer>
          </div>
				</article>-->
			</div>
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
		</div>
		<!-- ################################################################################################ -->
  </section>
</div>
<!-- End Content 01 - ข่าวสารรวม001 -->
<!-- ################################################################################################ -->
<!-- Start Pagination -->
<div class="wrapper row3 coloured" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
  <main class="hoc container clear">
    <!-- ################################################################################################ -->
    <div class="content">
      <nav class="pagination">
        <ul>
          <li><a href="ข่าวสารรวม002.php">&laquo; ก่อนหน้า</a></li>
					<li><a href="ข่าวสารรวมlatest.php">3</a></li>
					<li><a href="ข่าวสารรวม002.php">2</a></li>
          <li class="current"><strong>1</strong></li>
        </ul>
      </nav>
    </div>
    <!-- ################################################################################################ -->
  </main>
</div>
<!-- End Pagination -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>