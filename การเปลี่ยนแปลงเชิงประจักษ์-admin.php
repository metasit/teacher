<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		date_default_timezone_set("Asia/Bangkok");

		$ID_sup = $_GET['ID_sup'];
		$sup_name = $_GET['sup_name'];
		$sup_email = $_GET['sup_email'];

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['winScroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการครูดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="ระบบหลังบ้านศน.ขั้นเกียรติคุณ.php">ระบบหลังบ้านศึกษานิเทศก์ดีขั้นเกียรติคุณ</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">การเปลี่ยนแปลงเชิงประจักษ์</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Parent Content -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Child Content -->
	<div class="cart-box-main">
		<div style="max-width: 80%; margin: 30px auto 130px;">
			<!-- Start Child2 Content -->
			<div class="row">
				<div class="col-lg-12">
					<div class="table-main table-responsive">
						<!-- ################################################################################################ -->
						<?php
							$sqlhonor_score4sup_codeD = "SELECT * FROM `honor_score4sup` WHERE ID='$ID_sup' AND honor_score4sup_code='D' ";
							$reHS4SCD = mysqli_query($con, $sqlhonor_score4sup_codeD);
							$rowHS4SCD = mysqli_fetch_array($reHS4SCD);

							$honor_score4sup_data1 = $rowHS4SCD['honor_score4sup_data1'];
							if($honor_score4sup_data1 == 'most') {
								$honor_score4sup_part4_1_answer = 'มากที่สุด';
							}elseif($honor_score4sup_data1 == 'much') {
								$honor_score4sup_part4_1_answer = 'มาก';
							}elseif($honor_score4sup_data1 == 'neutral') {
								$honor_score4sup_part4_1_answer = 'ปานกลาง';
							}elseif($honor_score4sup_data1 == 'less') {
								$honor_score4sup_part4_1_answer = 'น้อย';
							}elseif($honor_score4sup_data1 == 'least') {
								$honor_score4sup_part4_1_answer = 'น้อยที่สุด';
							}

							$honor_score4sup_part4_2_answer = $rowHS4SCD['honor_score4sup_data2'];
							$honor_score4sup_part4_3_answer = $rowHS4SCD['honor_score4sup_data3'];
							$honor_score4sup_part4_4_answer = $rowHS4SCD['honor_score4sup_data4'];
							$honor_score4sup_part4_5_answer = $rowHS4SCD['honor_score4sup_data5'];
						?>
						<section class="text-gold center" style="line-height:80px;">
							<h8>การนำความรู้มาใช้ในการปฏิบัติงานอย่างจริงจังอย่างน้อย 1 พฤติกรรม/โครงการ/กิจกรรม</h8>
						</section>
						<div class="fs-20 inline lh-1-7">
							<p><strong>ชื่อศึกษานิเทศก์:</strong> <?php echo $sup_name.' ('.$sup_email; ?>)</p>
							<br>
							<p><strong>1. ท่านนำความรู้มาใช้ในการปฏิบัติงานอย่างจริงจัง ในระดับใด</strong>
								<br>
								<strong><u>ตอบ</u></strong> <?php echo $honor_score4sup_part4_1_answer; ?>
							</p>
							<br>
							<p><strong>2. ก่อนการอบรมท่านมีคุณสมบัติ/ทักษะ/สมรรถนะ/พฤติกรรมนั้นอย่างไร</strong>
								<br>
								<strong><u>ตอบ</u></strong> <?php echo $honor_score4sup_part4_2_answer; ?>
							</p>
							<br>
							<p><strong>3. หลังการอบรมท่านมีคุณสมบัติ/ทักษะ/สมรรถนะ/พฤติกรรมนั้นอย่างไร</strong>
								<br>
								<strong><u>ตอบ</u></strong> <?php echo $honor_score4sup_part4_3_answer; ?>
							</p>
							<br>
							<p><strong>4. การนำความรู้ไปใช้ในการปฏิบัติงานอย่างจริงจังนั้น คืออะไร จงบรรยายพร้อมภาพประกอบไม่เกิน 2 หน้า A4</strong>
								<br>
								<a class="btn" href="<?php echo $honor_score4sup_part4_4_answer; ?>" target="_blank">ดูไฟล์แนบ</a>
							</p>
							<p><strong>5. เอกสารหรือหลักฐาน เช่น ภาพ วีดีทัศน์ หรือบทสัมภาษณ์ผู้เกี่ยวข้องที่ แสดงให้เห็นถึงการเปลี่ยนแปลงอย่างชัดเจน</strong>
								<br>
								<a class="btn" href="<?php echo $honor_score4sup_part4_5_answer; ?>" target="_blank">ดูไฟล์แนบ</a>
							</p>
							<br>
						</div>
					</div>
				</div>
			</div>
			<!-- End Child2 Content -->
		</div>
	</div>
	<!-- End Child Content -->
	<!-- ################################################################################################ -->
</div>
<!-- End Parent Content -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

</body>
</html>