<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">
<!-- To declare your language - read more here: https://www.w3.org/International/questions/qa-html-language-declarations -->

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>


<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop-en.php'); ?>
                  <ul>
                    <li><a href="index.php">ภาษาไทย</a></li>
                  </ul>
<?php include('includes/headerBottom4Home-en.php'); ?>
<!-- End Header -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<div class="margin-for-fixedMenu"></div>

<!-- Start Poster Slider -->
<div class="slider-container">
	<!--
	<a href="#" onclick="return false"><img class="imgSlides" src="images/PosterHome-11.jpg" alt="PosterHome วันแม่12ส.ค." style="width:100%"></a>
	<a href="#" onclick="return false"><img class="imgSlides" src="images/PosterHome-10.jpg" alt="PosterHome ในหลวง" style="width:100%"></a>
	<a href="บำรุงค่าสมาชิก.php"><img class="imgSlides" src="images/PosterHome-09.jpg" alt="PosterHome บำรุงสมาชิก" style="width:100%"></a>
	<a href="สนับสนุนของที่ระลึก.php"><img class="imgSlides" src="images/PosterHome-08.jpg" alt="PosterHome PromoteShop3" style="width:100%"></a>
	<a href="สนับสนุนของที่ระลึก.php"><img class="imgSlides" src="images/PosterHome-07.jpg" alt="PosterHome PromoteShop2" style="width:100%"></a>
	<a href="สนับสนุนของที่ระลึก.php"><img class="imgSlides" src="images/PosterHome-06.jpg" alt="PosterHome PromoteShop1" style="width:100%"></a>
	<a href="#" onclick="return false"><img class="imgSlides" src="images/PosterHome-05.jpg" alt="PosterHome ราชินี" style="width:100%"></a>
	<a href="#" onclick="return false"><img class="imgSlides" src="images/PosterHome-04.jpg" alt="PosterHome ยินดีต้อนรับ ศ.ดร.พฤทธิ์และดร.ดิเรก" style="width:100%"></a>
	-->
  <a href="โครงการครูดีของแผ่นดิน.php"><img class="imgSlides" src="images/PosterHome-03.jpg" alt="PosterHome โครงการครูดีของแผ่นดิน" style="width:100%"></a>
  <a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php"><img class="imgSlides" src="images/PosterHome-02.jpg" alt="PosterHome โครงการศึกษานิเทศก์ดีของแผ่นดิน" style="width:100%"></a>
	<a href="โครงการBIA.php"><img class="imgSlides" src="images/BInAw Poster01.png" alt="PosterHome Be Internet Awesome project" style="width:100%"></a>
  <div class="navi-container">
    <div class="prev fl_left" onclick="plusDivs(-1)">&#10094;</div>
    <div class="next fl_right" onclick="plusDivs(1)">&#10095;</div>
  </div>
	<div class="pagi-container" style="width:100%">
    <span class="pagi-style check-current" onclick="currentDiv(1)"></span>
    <span class="pagi-style check-current" onclick="currentDiv(2)"></span>
		<span class="pagi-style check-current" onclick="currentDiv(3)"></span>
  </div>
</div>
<!-- End Poster Slider -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - เข้าร่วมโครงการ -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
  <section class="hoc container clear"> 
    <!-- ################################################################################################ -->
    <div class="left">
      <h7 class="heading">Join us</h7>
    </div>
    <div style="background-color:rgb(0,6,27); padding:20px 20px 0; border-radius:10px;">
      <div class="group latest">
        <article class="one_third first row4">
          <div class="center">
            <figure><a href="โครงการครูดีของแผ่นดิน-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน ร่วมงาน_ครูดี.jpg" alt="มูลนิธิครูดีของแผ่นดิน ร่วมงาน_ครูดี"></a></figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31);">โครงการเครือข่ายครูดีของแผ่นดิน</h4>
            <p style="color:rgb(243,243,243)">ลึกลงไปในมิติของความมนุษย์ที่มีความปรารถนาดีซึ่งกันและกันเป็นพื้นฐาน ทำให้สังคมของการเรียนรู้ไม่เคยจบสิ้น [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="โครงการครูดีของแผ่นดิน-en.php">See program detail &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="โครงการเด็กดีของแผ่นดิน-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน ร่วมงาน_นักเรียนดี.jpg" alt="มูลนิธิครูดีของแผ่นดิน ร่วมงาน_นักเรียนดี"></a></figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">โครงการเด็กดีของแผ่นดิน<br></h4>
            <p style="color:rgb(243,243,243)">เด็กรุ่นใหม่ ที่เรียกกันว่า Generation Z หรือ Gen Z คือ นิยามของเด็กที่เกิดหลังปี ค.ศ. 1995 หรือ ปี พ.ศ. 2538 ลงมา เด็กกลุ่มนี้เติบโตมา [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="โครงการเด็กดีของแผ่นดิน-en.php">See program detail &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน ร่วมงาน_ศึกษานิเทศน์ดี.jpg" alt="มูลนิธิครูดีของแผ่นดิน ร่วมงาน_ศึกษานิเทศน์"></a></figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">โครงการศึกษานิเทศก์ดีของแผ่นดิน</h4>
            <p style="color:rgb(243,243,243)">ศึกษานิเทศก์ (Supervisor) คือ ผู้ทำหน้าที่แนะนำ ชี้แนะแนวทางให้ครูและผู้บริหารสถานศึกษาเกิดความตระหนักรู้ มีทักษะในการบริหารจัดการ และการจัดการเรียนการสอน [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">See program detail &raquo;</a></footer>
          </div>
        </article>
      </div>
    </div>
    <!-- ################################################################################################ -->
    <div class="font-x1" style="text-align:right; padding:15px 0 18px 0;">
      <a class="readall" href="ร่วมโครงการ-en.php"><strong>See all program</strong></a>
    </div>
  </section>
</div>
<!-- End Content 00 - เข้าร่วมโครงการ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - ข่าวสาร -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
  <section class="hoc container clear"> 
    <!-- ################################################################################################ -->
    <div class="left">
      <h7 class="heading">News</h7>
    </div>
    <div style="background-color:rgb(0,6,27); padding:20px 20px 0; border-radius:10px;">
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
      <div class="group latest">
        <article class="one_third first row4">
          <div class="center">
            <figure><a href="ข่าวสาร022-en.php"><img src="images/รูปหน้าปกข่าวสาร22.jpg" alt="รูปหน้าปกข่าวสาร22"></a>
              <figcaption>
                <time datetime="2020-10-8T08:15+00:00"><strong><font size="5.5px">8</font></strong><em>Oct</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">มูลนิธิครูดีของแผ่นดิน แสดงความยินดีผู้บริหารระดับสูง กระทรวงศึกษาธิการ<br></h4>
            <p style="color:rgb(243, 243, 243)">วันที่ 8 ตุลาคม 2563 ผู้แทนมูลนิธิครูดีของแผ่นดิน นำโดย  นายฐกร พฤฒิปูรณี กรรมการและเลขานุการ และเจ้าหน้าที่มูลนิธิครูดีของแผ่นดิน เข้าแสดงความยินดี กับผู้บริหารระดับสูง [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร022-en.php">Continue Read &raquo;</a></footer>
          </div>
				</article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร021-en.php"><img src="images/รูปหน้าปกข่าวสาร21.jpg" alt="รูปหน้าปกข่าวสาร21"></a>
              <figcaption>
                <time datetime="2020-10-6T08:15+00:00"><strong><font size="5.5px">6</font></strong><em>Oct</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">มูลนิธิครูดีของแผ่นดิน แสดงความยินดีผู้บริหารระดับสูง กระทรวงศึกษาธิการ<br></h4>
            <p style="color:rgb(243, 243, 243)">วันที่ 6 ตุลาคม 2563 ผู้แทนมูลนิธิครูดีของแผ่นดิน นำโดย นางปาลิดา กุลรุ่งโรจน์ ดร.กิตติกร คัมภีรปรีชา นายฐกร พฤฒิปูรณี เข้าแสดงความยินดี กับผู้บริหารระดับสูงกระทรวงศึกษาธิการ เนื่องใน [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร021-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร020-en.php"><img src="images/รูปหน้าปกข่าวสาร20.jpg" alt="รูปหน้าปกข่าวสาร20"></a>
              <figcaption>
                <time datetime="2020-2-13T08:15+00:00"><strong><font size="5.5px">27</font></strong><em>Jun</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">ประชุมผู้ทรงคุณวุฒิเพื่อการพัฒนาเครื่องมือวัดผู้บริหารดีของแผ่นดิน<br></h4>
            <p style="color:rgb(243, 243, 243)">เมื่อวันที่27 มิถุนายน 2563 ที่ผ่านมา นางปาลิดา กุลรุ่งโรจน์ ประธานอนุกรรมการโครงการเครือข่ายครูดีของแผ่นดิน ฯ ได้จัดประชุมผู้ทรงคุณวุฒิเพื่อพัฒนาเครื่องมือวัดผู้บริหารดีของแผ่นดิน [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร020-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
      </div>
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="font-x1" style="text-align:right; padding:15px 0 18px 0;">
      <a class="readall" href="ข่าวสารรวมlatest-en.php"><strong>Read More</strong></a>
    </div>
  </section>
</div>
<!-- End Content 01 - ข่าวสาร -->
<!-- ################################################################################################ -->
<!-- Start Content 02 หอเกียรติยศ -->
<div class="wrapper bgded coloured" style="background-image:url('images/มูลนิธิครูดีของแผ่นดิน BG หน้าแรก หอเกียรติยศ.jpg')">
  <div class="hoc container testimonials clear">
    <!-- ################################################################################################ -->
    <div class="left">
      <h7 class="heading" style="color:rgb(46, 46, 46);">Hall of Fame</h7>
    </div>
    <div style="background-color:rgb(0,6,27); padding:20px 20px 0; border-radius:10px;">
      <div class="group">
        <article class="one_third first">
          <a href="หอเกียรติยศ001-en.php">
            <img class="zoom108" src="images/Posterประธาน.jpg" alt="Posterประธาน">
            <h9 class="heading" style="color:white;">พลเอกเอกชัย ศรีวิลาศ</h9>
            <em style="color:white;">ประธานมูลนิธิครูดีของแผ่นดิน</em>
            <blockquote>“As the Future will catches you! เมื่ออนาคตไล่ล่าคุณ” 
              แนวโน้มการปฏิรูปการศึกษาไทยในศตวรรษที่ 21 ที่ต้องก้าวตามให้ทันพัฒนาการของเทคโนโลยี 3 อย่างที่กำลังครอบงำโลกใบนี้ และสิ่งที่น่าตกใจยิ่งไปกว่านั้น คือ เทคโนโลยีเหล่านี้สามารถส่งพลังมหาศาล
              ต่อการเปลี่ยนแปลงวิถีชีวิตของคนเราและลูกหลานเราอย่างคาดไม่ถึง หากวงการศึกษาไทยยังไม่ตื่นตัว ยากจะคาดเดาถึงผลกระทบที่จะตามมา</blockquote>
          </a>
        </article>
        <article class="one_third">
          <a href="หอเกียรติยศ007-en.php">
            <img class="zoom108" src="images/Posterอุดมการณ์ที่ดี.png" alt="Posterอุดมการณ์ที่ดี">
            <h9 class="heading" style="color:white;">ดร.วิระ แข็งกสิการ</h9>
            <em style="color:white;">ผู้ตรวจราชการกระทรวงศึกษาธิการ</em>
            <blockquote>ครูดีมันเป็นคีย์เวิร์ดของการที่จะส่งเสริมคนดีของแผ่นดินอยู่แล้ว วันนี้ท่านปลัดกระทรวงศึกษาธิการ ท่านประเสริฐ บุญเรือง มีความตั้งใจที่จะมาพบกับพวกเรา ถ้าท่านเสร็จภารกิจจะรีบกลับมาเยี่ยมชม
              กับพวกเรา ท่านก็เลยมอบหมายให้ผมมาทำหน้าที่แทน</blockquote>
          </a>
        </article>
        <article class="one_third">
          <a href="หอเกียรติยศ006-en.php">
            <img class="zoom108" src="images/Posterระบบการใช้.png" alt="Posterระบบการใช้">
            <h9 class="heading" style="color:white;">ดร.อโณทัย ไทยวรรณศรี</h9>
            <em style="color:white;">ผู้อำนวยการสำนักพัฒนานวัตกรรมการจัดการศึกษา สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน</em>
            <blockquote>สมาชิกครูดีของแผ่นดิน คือ เรื่องของการใช้อำนาจในโรงเรียน สมัยก่อนเรายังมองไม่เห็นระบบชัดเจน จึงเป็นการจัดการเรียนการสอนทั่วไป แต่เรื่องการใช้อำนาจในโรงเรียน 
              สถานการณ์นี้มีความจำเป็นอย่างยิ่งในปัจจุบันครับ </blockquote>
          </a>
        </article>
      </div>
    </div>
    <!-- ################################################################################################ -->
    <div class="font-x1" style="text-align:right; padding:15px 0 18px 0;">
      <a class="readall" href="หอเกียรติยศรวมlatest-en.php"><strong>See More</strong></a>
    </div>
  </div>
</div>
<!-- End Content 02 หอเกียรติยศ -->
<!-- ################################################################################################ -->
<!-- Start Content 03 - จดหมาย -->
<div style="background-color:rgb(226,255,224);">
  <div class="hoc container clear">
    <div class="left">
      <h7 class="heading" style="color:rgb(58, 58, 58);">Letter/Declaration</h7>
    </div>
    <table class="table1 table2" style="padding-bottom:20px;">
      <thead>
        <tr>
          <th</th>
          <th></th>
        </tr>
      </thead>
      <!-- ################################################################################################ -->
      <tbody>
        <!-- จดหมาย 024 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย024-en.php">จดหมายการทำหน้าที่อาสาฯ</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 023 -->
        <tr>
          <td>
						<a class="blink1">Hot</a>
            <a class="linkfortable" href="จดหมาย023-en.php">ประกาศรางวัลครูดีของแผ่นดินขั้นพื้นฐาน ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 022 -->
				<!--
        <tr>
          <td>
						<a class="blink1">Hot</a>
            <a class="linkfortable" href="จดหมาย022-en.php">ประกาศรางวัลครูดีของแผ่นดิน ชั้นที่ 5 ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
				-->
        <!-- จดหมาย 021 -->
        <tr>
          <td>
						<a class="blink1">Hot</a>
            <a class="linkfortable" href="จดหมาย021-en.php">ประกาศรางวัลศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 020 -->
				<!--
        <tr>
          <td>
						<a class="blink1">Hot</a>
            <a class="linkfortable" href="จดหมาย020-en.php">ประกาศรางวัลศึกษานิเทศก์ดีของแผ่นดิน ระดับเกียรติคุณ ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
				-->
        <!-- จดหมาย 019 -->
        <tr>
          <td>
            <a class="linkfortable" href="จดหมาย019-en.php">สพฐ. แจ้งแนวทางการคัดเลือกรางวัล คุรุชน คนคุณธรรม (ร่วมกับมูลนิธิครูดีของแผ่นดิน)</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
				<!-- จดหมาย 018 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย018-en.php">หนังสือโครงการเครือข่ายครูดีของแผ่นดิน ถึงหน่วยงานที่ MOU ร่วมกับมูลนิธิครูดีของแผ่นดิน</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 017 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย017-en.php">หนังสือแจ้งปฏิทินการดำเนินโครงการฯ ปี 2563</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 016 -->
        <tr>
          <td>
            <a class="linkfortable" href="จดหมาย016-en.php">ประกาศรางวัลครูดีของแผ่นดิน ชั้นที่ 4 ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
				<!-- จดหมาย 015 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย015-en.php">ทุนการศึกษาสำหรับเด็กและเยาวชนดีของแผ่นดิน ช่วงวิกฤตโรคติดต่อโควิด-๑๙</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
      </tbody>
    </table>
    <div class="font-x1" style="text-align:right; padding-right:18px;">
      <a class="readall" href="จดหมายรวมlatest-en.php"><strong>Read More</strong></a>
    </div>
  </div>
</div>
<!-- End Content 03 - จดหมาย -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 04 - เอกสารและจดหมาย -->

<!-- End Content 04 - เอกสารและจดหมาย -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start View Counter-->
<div class="viewcount">
  <div class="hoc" style="margin-top:-50px;">
    <h9>Site Stats</h9> (Start 10 Mar 2020)<br>
    <div style="margin-left:35px;">
      <script type="text/javascript" src="https://freecountercode.com/service/kcuwmQpW9bTod0TnWz5B/3768"></script>
    </div>
  </div>
</div>
<!-- End View Counter-->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer-en.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<!-- JS for slider -->
<script src="js/slider.js"></script>


</body>
</html>