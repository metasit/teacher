<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <div class="btn PreMenu_fl_right">
        <a href="สนับสนุนมูลนิธิ.php">สนับสนุนมูลนิธิ</a>
      </div>
      <!-- ################################################################################################ -->
      <nav id="mainav2" class="fl_right">
        <ul class="PreMenufaico2">
          <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> เข้าสู่ระบบ</a></li>
          <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> ภาษาไทย</a>
            <ul>
              <li><a href="หอเกียรติยศรวม001-en.php">English</a></li>
              <!--<li><a href="#">Chinese</a></li>-->
            </ul>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <div class="search1 fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px; ">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li><a href="all_project.php">ร่วมโครงการ</a></li>
          <li class="active"><a href="#" onclick="window.location.reload(true);">หอเกียรติยศ</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="window.location.reload(true);">หอเกียรติยศ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - หอเกียรติยศ -->
<div class="wrapper bgded overlay coloured" style="background-color:rgba(189,255,159,0.829)">
  <div class="hoc container testimonials clear">
    <!-- ################################################################################################ -->
    <div style="background-color:rgb(0,6,27); padding:20px 20px 0; border-radius:10px;">
      <div class="group">
        <article class="one_third first">
          <a href="หอเกียรติยศ002.php">
            <img class="zoom108" src="images/Posterหอพลังเสียง.png" alt="Posterหอพลังเสียง">
            <h9 class="heading" style="color:white;">นายนิติกรณ์ ตั้งหลัก</h9>
            <em style="color:white;">โรงเรียนดอนเมืองทหารอากาศบำรุง สพม. เขต 2</em>
            <blockquote>หากครูเปลี่ยนท่าทีและน้ำเสียงที่ขึงขังน่ากลัวปรับตัวใช้น้ำเสียงที่นุ่มนวล อ่อนหวาน ในการจัดการเรียนรู้ได้ฉันใด น้ำเสียงแห่งการสวดโอ้เอ้วิหารรายก็เป็นสิ่งที่สามารถเปลี่ยนเด็กติดเม เด็กที่อ่านไม่คล่อง และเดสมาธิสั้นได้ฉันนั้น</blockquote>
          </a>
        </article>
        <article class="one_third">
          <a href="หอเกียรติยศ001.php">
            <img class="zoom108" src="images/Posterหอกระดาษนิสัย.png" alt="Posterหอกระดาษนิสัย">
            <h9 class="heading" style="color:white;">นางสาวกนกรัตน์ พรหมเดช</h9>
            <em style="color:white;">โรงเรียนวัดหัวกรูด สพป. ชุมพร เขต 1</em>
            <blockquote>กระดาษกับสีที่นำมาบวกกับความตั้งใจจริงที่จะเปลี่ยนพฤติกรรม จะสร้างผลลัพธ์ที่น่าเหลือเชือให้กับทุกคนโดยใช้เทคนิค การตั้งคำถามแบบ Wh-guestion</blockquote>
          </a>
        </article><!--
        <article class="one_third">
          <a href="หอเกียรติยศ003.php">
            <img class="zoom108" src="images/Posterหอครูปรับเด็กเปลี่ยน.png" alt="Posterหอครูปรับเด็กเปลี่ยน">
            <h9 class="heading" style="color:white;">นางณัฐณิชา กล้าหาญ</h9>
            <em style="color:white;">โรงเรียนชากังราววิทยา (อินทร์-ชุ่ม ดีสารอุปถัมภ์)</em>
            <blockquote>เมื่อบรรยากาศห้องเรียนเต็มไปด้วยพลังลบ เพียงครูเปิดใจ ปรับแนวคิด หาจ้อบกพร่องของตัวครูเอง พัฒนาตนเอง นำมาปรับกระบวนการเรียนรู้สร้างสังคม กัลยาณมิตร ในห้องเรียน ให้เด็กมีพลังบวก 
              เป็นกันเอง ก็ทำให้ เด็กสนใจเรียนมากขึ้น กล้าคิด กล้าตอบ เป็นก้องเรียนคณิตที่มีชีวิตได้</blockquote>
          </a>
        </article>
      </div>
      <div class="group">
        <article class="one_third first">
          <a href="หอเกียรติยศ002.php">
            <img class="zoom108" src="images/Posterหอพลังเสียง.png" alt="Posterหอพลังเสียง">
            <h9 class="heading" style="color:white;">นายนิติกรณ์ ตั้งหลัก</h9>
            <em style="color:white;">โรงเรียนดอนเมืองทหารอากาศบำรุง สพม. เขต 2</em>
            <blockquote>หากครูเปลี่ยนท่าทีและน้ำเสียงที่ขึงขังน่ากลัวปรับตัวใช้น้ำเสียงที่นุ่มนวล อ่อนหวาน ในการจัดการเรียนรู้ได้ฉันใด น้ำเสียงแห่งการสวดโอ้เอ้วิหารรายก็เป็นสิ่งที่สามารถเปลี่ยนเด็กติดเม เด็กที่อ่านไม่คล่อง และเดสมาธิสั้นได้ฉันนั้น</blockquote>
          </a>
        </article>
        <article class="one_third">
          <a href="หอเกียรติยศ001.php">
            <img class="zoom108" src="images/Posterหอกระดาษนิสัย.png" alt="Posterหอกระดาษนิสัย">
            <h9 class="heading" style="color:white;">นางสาวกนกรัตน์ พรหมเดช</h9>
            <em style="color:white;">โรงเรียนวัดหัวกรูด สพป. ชุมพร เขต 1</em>
            <blockquote>กระดาษกับสีที่นำมาบวกกับความตั้งใจจริงที่จะเปลี่ยนพฤติกรรม จะสร้างผลลัพธ์ที่น่าเหลือเชือให้กับทุกคนโดยใช้เทคนิค การตั้งคำถามแบบ Wh-guestion</blockquote>
          </a>
        </article>
        <article class="one_third">
          <a href="หอเกียรติยศ008.php">
            <img src="images/Posterครูจ้างสร้างเด็ก.png" alt="Posterครูจ้างสร้างเด็ก">
            <h9 class="heading" style="color:white;">นางเดือนเพ็ญ บัวศรี</h9>
            <em style="color:white;">โรงเรียนวัดโบสถ์สมพรชัย สพป.พระนครศรีอยุธยา เขต 2</em>
            <blockquote>ตำแหน่งจะสูงต่ำแค่ไหน ถ้าครูมีใจรักศิษย์และศรัทธาในอาชีพ “ครู” ก็สามารถสร้างเด็กดีให้แผ่นดินได้ “เด็กที่มีความตั้งใจที่เปลี่ยนแปลง ตนเอง ในการมาโรงเรียนเช้าขึ้นเพื่อมาช่วยเพื่อๆทำเวร 
              เมื่อเขาเปลี่ยนพฤตกรรมได้ เขามีความภูมิใจและอยากสร้างเด็กดีต่อๆไป</blockquote>
          </a>
        </article>
      </div>
      <div class="group">
        <article class="one_third first">
          <a href="หอเกียรติยศ007.php">
            <img src="images/Posterหอปัญหาตนเอง.png" alt="Posterหอปัญหาตนเอง">
            <h9 class="heading" style="color:white;">นางสาวกมลวรรณ ปานเมือง</h9>
            <em style="color:white;">โรงเรียนทุ่งคาพิทยาคาร สพม. เขต 11</em>
            <blockquote>ค้นหาปัญหาของตนเองให้พบ ร่วมกลุ่มกันแก้ปัญหา นำพาซึ่งการเปลี่ยนแปลง ครูใช้การจัดการเรียนรู้โดยใช้ปัญหาเป็นฐาน (Problem-Based Learning) ร่วมกับรูปแบบการจัดการเรียนรู้แบบร่วมมือ 
              (Learning Together) โดยให้นักเรียนค้นหาปัญหาที่เกิดขึ้นกับตนเองและการแก้ปัญหานั้นจับกลุ่มร่วมกันแก้ไขปัญหาที่มีความคล้ายถึงกัน ใช้กระบวนการกลุ่มเพื่อน ช่วยเพื่อนและครูติดตามดูแลเอาใจใส่ 
              ให้กำลังใจนักเรียน นำมาสู่การเปลี่ยนแปลงของนักเรียนในทางที่ดีขึ้น</blockquote>
          </a>
        </article>
        <article class="one_third">
          <a href="หอเกียรติยศ006.php">
            <img src="images/Posterหอสร้างเด็กนิทาน.png" alt="Posterหอสร้างเด็กนิทาน">
            <h9 class="heading" style="color:white;">นางสาวโสรญา คงรักษา</h9>
            <em style="color:white;">โรงเรียนประชานิคม 4 สพป.ชุมพร เขต 1</em>
            <blockquote>กุศโลบายง่ายๆ ในการสร้างพฤติกรรมใหม่ แก้นิสัยเก่าเพียงทำผ่านการเล่านิทาน นอกจากช่วยกระตุ้นการสร้างจินตนาการแล้ว ยังช่วยบ่มเพาะคุณธรรมพื้นฐานให้ประทับใจจิตวิญญาณของเด็ก</blockquote>
          </a>
        </article>
        <article class="one_third">
          <a href="หอเกียรติยศ005.php">
            <img src="images/Posterหอของหายแก้ได้.png" alt="Posterหอของหายแก้ได้">
            <h9 class="heading" style="color:white;">นางสาววิไลลักษณ์ อยู่ดี</h9>
            <em style="color:white;">โรงเรียนวัดหนามแดง สพป.ฉะเชิงเทรา เขต 1</em>
            <blockquote>การเปลี่ยนแปลงพฤติกรรมจากเด็กที่ไม่เคยเก็บรักษาของเลยกลายเป็นเด็กที่รู้จักเก็บรักษาของได้ดีขึ้นด้วยสมุดบันทึก</blockquote>
          </a>
        </article>
            -->
      </div>
    </div>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End Content 01 - หอเกียรติยศ -->
<!-- ################################################################################################ -->
<!-- Start Pagination -->
<div class="wrapper row3 overlay coloured" style="background-color:rgba(189,255,159,0.829)">
  <main class="hoc container clear">
    <!-- ################################################################################################ -->
    <div class="content">
      <nav class="pagination">
        <ul>
          <li><a href="หอเกียรติยศรวม002.php">&laquo; ก่อนหน้า</a></li>
          <li><a href="หอเกียรติยศรวมlatest.php">3</a></li>
          <li><a href="หอเกียรติยศรวม002.php">2</a></li>
          <li class="current"><strong>1</strong></li>
        </ul>
      </nav>
    </div>
  </main>
</div>
<!-- End Pagination -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End Copyright Tab -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Back2Top Button -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Back2Top Button -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>