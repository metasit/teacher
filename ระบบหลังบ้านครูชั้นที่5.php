<?php 
	session_start();
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if(strpos($_SESSION['permis_system'], '*all*') !== false || strpos($_SESSION['permis_system'], '*A00*') !== false || strpos($_SESSION['permis_system'], '*A02*') !== false) {
			require_once('condb.php');
			$email = $_SESSION['email'];
		}else{
			header("Location: javascript:history.go(-1);");
		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['winScroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการครูดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ระบบหลังบ้านครู ชั้นที่ 5</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ระบบหลังบ้านครู ชั้นที่ 5 -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row" style="margin:0 20px; overflow-x:auto;">
			<div class="col-lg-12">
				<div class="table-main table-responsive" style="margin-bottom:200px">
					<!-- ################################################################################################ -->
					<table class="table" style="background-color:rgb(240,240,240);">
						<thead>
							<tr>
								<th>ลำดับ</th>
								<th>ชื่อ-นามสกุล</th>
								<th>วันที่ครูส่งเรื่อง</th>
								<th>สถานะ</th>
								<th class="BG-blue1">ครูพื้นฐาน</th>
								<th class="BG-blue1">เด็กพื้นฐาน</th>
								<th class="BG-blue1">ประเมินชั้น5</th>
								<th class="BG-gold1">อัพเดท</th>
								<th class="BG-gold1">หมายเหตุ</th>
							</tr>
						</thead>
						<tbody class="disable-margin">
							<!-- Filter -->
							<form method="POST">
								<input type="submit" class="btnfilter1" name="filter" value="แสดงทั้งหมด" />
								<input type="submit" class="btnfilter1" name="filter" value="Approve แล้ว" />
								<input type="submit" class="btnfilter1" name="filter" value="ปฏิเสธ" />
								<input type="submit" class="btnfilter1" name="filter" value="ส่งให้มูลนิธิตรวจสอบ" />
							</form>
							<!-- Filter -->
							<?php
								/* Set number product per page */
								$results_per_page = 50;
								if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
								$start_from = ($page-1) * $results_per_page;

								/* Call login table data from mySQL */
								switch($_POST['filter'])
								{
									case 'Approve แล้ว':
										$sql = "SELECT * FROM `login` 
										WHERE (occup_code LIKE 'OcAA%' AND fifth_score_status='Approve แล้ว') 
										OR (occup_code LIKE 'OcAB%' AND fifth_score_status='Approve แล้ว') 
										ORDER BY fifth_score_date DESC LIMIT $start_from, $results_per_page ";
									break;
									case 'ปฏิเสธ':
										$sql = "SELECT * FROM `login` 
										WHERE (occup_code LIKE 'OcAA%' AND fifth_score_status='ปฏิเสธ') 
										OR (occup_code LIKE 'OcAB%' AND fifth_score_status='ปฏิเสธ') 
										ORDER BY fifth_score_date DESC LIMIT $start_from, $results_per_page ";
									break;
									case 'ส่งให้มูลนิธิตรวจสอบ':
										$sql = "SELECT * FROM `login` 
										WHERE (occup_code LIKE 'OcAA%' AND fifth_score_status='ส่งให้มูลนิธิตรวจสอบ') 
										OR (occup_code LIKE 'OcAB%' AND fifth_score_status='ส่งให้มูลนิธิตรวจสอบ') 
										ORDER BY fifth_score_date DESC LIMIT $start_from, $results_per_page ";
									break;
									default:
										$sql = "SELECT * FROM `login` 
										WHERE (occup_code LIKE 'OcAA%' AND fifth_score_status='ส่งให้มูลนิธิตรวจสอบ') 
										OR (occup_code LIKE 'OcAA%' AND fifth_score_status='Approve แล้ว') 
										OR (occup_code LIKE 'OcAA%' AND fifth_score_status='ปฏิเสธ') 
										OR (occup_code LIKE 'OcAB%' AND fifth_score_status='ส่งให้มูลนิธิตรวจสอบ') 
										OR (occup_code LIKE 'OcAB%' AND fifth_score_status='Approve แล้ว') 
										OR (occup_code LIKE 'OcAB%' AND fifth_score_status='ปฏิเสธ') 
										ORDER BY FIELD(fifth_score_status, 'ส่งให้มูลนิธิตรวจสอบ', 'ปฏิเสธ', 'Approve แล้ว') ASC, fifth_score_date DESC LIMIT $start_from, $results_per_page ";
								}
								$re = mysqli_query($con, $sql);

								$n = $start_from+1; // Get number of row // ใช้ $n เพราะหน้านี้ calhonor_score-ศน.php มีใช้ตัวแปล $i ไปแล้ว
								while($row = $re->fetch_assoc()) {
									
									$ID_tea = $row['ID'];
									$teacher_name = trim($row['firstname']).' '.trim($row['lastname']);

									// เรียก adscorelog เพื่อดูว่า ได้รับครูดีขั้นพื้นฐานมาเมื่อไหร่
									$sqladscorelog = "SELECT * FROM `adscorelog` WHERE ID_user='$ID_tea' AND adscorelog_task='Approve แล้ว' AND adscorelog_detail='ขั้นพื้นฐาน' ORDER BY adscorelog_id DESC LIMIT 1 ";
									$readscorelog = mysqli_query($con,$sqladscorelog);
									$rowadscorelog = mysqli_fetch_array($readscorelog);

									$check_basic_score = mysqli_num_rows($readscorelog);

									if($check_basic_score == 0) { // เช็คว่า มีการ Approve ไปแล้วที่ adscorelog table ไปรึยัง

										if($row['basic_score_remark'] == 'Fast-Track') { // ถ้าไม่มี ให้เช็คอีกว่า เป็นกลุ่ม Fast-Track หรือไม่

											$basic_score_date = 'Fast-Track';
											$basic_score_status = 'Fast-Track';
											$expire_basic_score_date = 'Fast-Track';

										}else{ // ถ้าไม่มี แสดงว่า ไม่เคยได้รับการ Approve ครูขั้นพื้นฐานมาเลย

											$basic_score_date = 'ไม่พบข้อมูลในระบบ';
											$basic_score_status = 'ไม่พบข้อมูลในระบบ';
											$expire_basic_score_date = 'ไม่พบข้อมูลในระบบ';

										}

									}else{

										/* Find and Set the latest basic_score_date */
										$date = new DateTime($rowadscorelog['adscorelog_date']);
										$basic_score_date = $date->format("d-m-Y"); // Set date format
										/* Set the expire_basic_score_date */
										$expire_basic_score_date = $date->add(new DateInterval('P1Y1D')); // Find expire date for basic_score		
										/* Check that this user basic_score expire? and Set $basic_score_status */
										$date_today = new DateTime('today');

										if($expire_basic_score_date >= $date_today) {

											$basic_score_status = 'ได้รับการอนุมัติ'; // Set basic_score_status to Approved

										}else{

											$basic_score_status = 'หมดอายุ';

										}

										$expire_basic_score_date = $date->format("d-m-Y"); // Set date format

									}

									////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
									/* เช็คว่า จำนวนนร.ที่ดูแล ผ่านเกณฑ์หรือไม่ fifth_score4tea_part2 */
									// จำนวนนร.ที่ครูคนนี้ดูแลทั้งหมด
									$sqlfifth_score4tea_codeB = "SELECT ID FROM `fifth_score4tea` WHERE ID='$ID_tea' AND fifth_score4tea_code='B' ";
									$reB = mysqli_query($con, $sqlfifth_score4tea_codeB);
									$rowB = mysqli_fetch_array($reB);
									$stu_num = $rowB['fifth_score4tea_data'];

									// จำนวนนร.ขั้นพื้นฐาน ของครูคนนี้ทั้งหมดที่ได้รับการ Approve จากมูลนิธิแล้ว
									$affil_code = $row['affil_code'];
									$sqllogin = "SELECT `basic_score_status` FROM `login` WHERE affil_code='$affil_code' AND basic_score_total='$ID_tea' AND basic_score_status='ยืนยันแล้ว/Approve แล้ว' AND ID!='$ID_tea' ";
									$relogin = mysqli_query($con, $sqllogin);
									$countstupass = mysqli_num_rows($relogin);

									////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
									/* เช็คว่า ทั้ง3part ผ่านรึยัง */
									if($basic_score_status == 'ได้รับการอนุมัติ' || $basic_score_status == 'Fast-Track') {

										$fifth_score4tea_part1 = 'Pass';

									}

									if($stu_num != 0) {

										if(ceil(number_format($countstupass/$stu_num*100, 2)) >= 50) {

											$fifth_score4tea_part2 = 'Pass';

										}

									}

									if($row['fifth_score_remark'] == 'Fast-Track') {

										$fifth_score4tea_part3 = 'Pass';

									}elseif(substr($row['occup_code'], 0, 4) == 'OcAA') {

										$ID = $ID_tea;
										include('includes/calfifth_score-ครู.php');
										$ID = $_SESSION['ID'];

										if($numberpass_sub >= 1) {

											$fifth_score4tea_part3 = 'Pass';
											
										}else{

											$fifth_score4tea_part3 = 'Fail';

										}

									}elseif(substr($row['occup_code'], 0, 4) == 'OcAB') {

										$ID = $ID_tea;
										include('includes/calfifth_score-ผู้บริหาร-ครู.php');
										$ID = $_SESSION['ID'];

										if($numberpass_sub >= 1) {

											$fifth_score4tea_part3 = 'Pass';

										}else{

											$fifth_score4tea_part3 = 'Fail';

										}
									} ?>
									<tr>
										<!-- No. -->
										<td class="price-pr">
											<p><?php echo $n; ?></p>
										</td>
										<!-- Teacher Name -->
										<td class="name-pr lh-1-0">
											<p><?php echo $row['firstname'].'<br>'.$row["lastname"]; ?></p>
										</td>
										<!-- วันที่ครูส่งมาให้ตรวจ -->
										<td class="price-pr">
											<p><?php echo date("d-m-Y", strtotime($row['fifth_score_date'])); ?></p>
										</td>
										<!-- Status -->
										<td class="name-pr">
											<?php
												if($row['fifth_score_status']  == 'ส่งให้มูลนิธิตรวจสอบ') { ?>

													<p>รอมูลนิธิตรวจสอบ</p> <?php

												}elseif($row['fifth_score_status'] == 'ปฏิเสธ') { ?>

													<p>ถูกปฏิเสธ</p> <?php

												}elseif($row['fifth_score_status'] == 'Approve แล้ว') { ?>

													<p style="color:rgb(72,160,0);"><i class="fas fa-check-circle"></i><br>ผ่านแล้ว</p> <?php

												}
											?>
										</td>
										<!-- ครูพื้นฐาน -->
										<td class="name-pr">
											<?php
												if($fifth_score4tea_part1 == 'Pass') { ?>

													<p>ผ่านแล้ว <?php if($basic_score_status == 'Fast-Track') {echo '(FT)';} ?></p> <?php

												}else{ ?>

													<p>-</p> <?php

												}
											?>
										</td>
										<!-- เด็กพื้นฐาน -->
										<?php
											if($row['fifth_score_remark'] == 'Fast-Track') { ?>

												<td class="name-pr">
													<p>FT</p>
												</td> <?php

											}else{ ?>

												<td class="slip-upload-btn">
													<a href="ตารางแสดงสถานะนักเรียนขั้นพื้นฐาน-admin.php?ID_tea=<?php echo $ID_tea; ?>&affil_code=<?php echo $row['affil_code']; ?>" target="_blank">ดูรายงาน</a>
												</td> <?php

											}
										?>
										<!-- ประเมินชั้น5 -->
										<td class="name-pr">
											<?php
												if($fifth_score4tea_part3 == 'Pass') { ?>

													<p>ผ่านแล้ว <?php if($row['fifth_score_remark'] == 'Fast-Track') {echo '(FT)';} ?></p> <?php

												}else{ ?>

													<p>-</p> <?php

												}
											?>
										</td>
										<!-- Update Status Button -->
										<td>
											<div class="up-stat-sty">

												<button class="up-stat-btn"><i class="fas fa-caret-down"></i></button>

												<div class="dropdown">
													<?php
														if($row['fifth_score_status'] == 'ส่งให้มูลนิธิตรวจสอบ' || $row['fifth_score_status'] == 'ปฏิเสธ') { ?>

															<a onclick="getWinScroll_changeStatusBtn_updatefifth_score4tea_status('ปฏิเสธ', '<?php echo $ID_tea; ?>', '<?php echo $page; ?>')">ปฏิเสธ</a>
															<a onclick="getWinScroll_changeStatusBtn_updatefifth_score4tea_status('Approve แล้ว', '<?php echo $ID_tea; ?>', '<?php echo $page; ?>')">Approve</a> <?php

														}elseif($row['fifth_score_status'] == 'Approve แล้ว'){ ?>

															<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">Approve แล้ว</a> <?php

														}
													?>
												</div>
											</div>
										</td>
										<!-- Remark -->
										<form action="addfifth_score4tea_remark-admin.php" method="POST">
											<td class="order_remark-sty">
												<textarea rows="1" cols="15" name="fifth_score_remark"><?php echo $row['fifth_score_remark']; ?></textarea>
												<div class="order-detail-btn">
													<input type="hidden" name="ID" value=<?php echo $ID_tea; ?>>

													<input type="hidden" id="winScroll_input" name="winScroll">
													<input type="hidden" id="page" name="page" value="<?php echo $page; ?>">
													<input type="submit" id="fifth_score_remark_submitbtn" class="order_remark-submit" value="บันทึก">
													
													<script>
														var winScroll_input = document.querySelectorAll("#winScroll_input");
														var fifth_score_remark_submitbtn = document.querySelectorAll("#fifth_score_remark_submitbtn");

														fifth_score_remark_submitbtn[<?php echo $n-1; ?>].onmouseover = function() {
															var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
															winScroll_input[<?php echo $n-1; ?>].value = winScroll;
														}
													</script>
												</div>
											</td>
										</form>
									</tr> <?php
									$n++;
									$selfscore = 0;
									$peoplescore = 0;
									$workscore = 0;
								}
							?>
						</tbody>
					</table>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ระบบหลังบ้านครู ชั้นที่ 5 -->
<?php
switch($_POST['filter'])
{
  case 'Approve แล้ว':
    $sql = "SELECT COUNT(ID) AS total FROM `login` WHERE fifth_score_status='Approve แล้ว' ";
  break;
  case 'ปฏิเสธ':
		$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE fifth_score_status='ปฏิเสธ' ";
	break;
  case 'ส่งให้มูลนิธิตรวจสอบ':
    $sql = "SELECT COUNT(ID) AS total FROM `login` WHERE fifth_score_status='ส่งให้มูลนิธิตรวจสอบ' ";
  break;
  default:
		$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE fifth_score_status='ส่งให้มูลนิธิตรวจสอบ' OR fifth_score_status='Approve แล้ว' OR fifth_score_status='ปฏิเสธ' ";
}

$result = $con->query($sql);
$row = $result->fetch_assoc();
$total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results

?>
            <div class="wrapper row3 coloured" style="background-color:rgb(226,255,224);">
              <main class="hoc container clear">
                <div class="content">
                  <nav class="pagination">
                    <ul>
<?php
                        for ($n=1; $n<=$total_pages; $n++) {  // print links for all pages
                          if ($n==$page)
                          {
                            echo "<li class='current'>";
                          }else{
                            echo "<li>";
                          }
                          echo "<a href='ระบบหลังบ้านครูชั้นที่5.php?page=".$n."'";
                          if ($n==$page)  echo " class='curPage'";
                          echo ">".$n."</a> ";
                        }; 
?>
                      </li>
                    </ul>
                  </nav>
                </div>
              </main>
            </div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<!-- JS for getWinScroll -->
<script src="js/getWinScroll_changeStatusBtn.js"></script>

</body>
</html>