<?php
	session_start();
	require_once('condb.php');
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop-en.php'); ?>
                  <ul>
                    <li><a href="ข่าวสารรวมlatest.php">ภาษาไทย</a></li>
                  </ul>
<?php include('includes/headerBottom-en.php'); ?>
<!-- End Header -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="window.location.reload(true);">News</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - ข่าวสารรวมlatest -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
  <section class="hoc container clear">
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div style="background-color:rgb(0,6,27); padding:20px 20px 0; border-radius:10px;">
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
      <div class="group latest">
        <article class="one_third first row4">
          <div class="center">
            <figure><a href="ข่าวสาร022-en.php"><img src="images/รูปหน้าปกข่าวสาร22.jpg" alt="รูปหน้าปกข่าวสาร22"></a>
              <figcaption>
                <time datetime="2020-10-8T08:15+00:00"><strong><font size="5.5px">8</font></strong><em>Oct</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">มูลนิธิครูดีของแผ่นดิน แสดงความยินดีผู้บริหารระดับสูง กระทรวงศึกษาธิการ<br></h4>
            <p style="color:rgb(243, 243, 243)">วันที่ 8 ตุลาคม 2563 ผู้แทนมูลนิธิครูดีของแผ่นดิน นำโดย  นายฐกร พฤฒิปูรณี กรรมการและเลขานุการ และเจ้าหน้าที่มูลนิธิครูดีของแผ่นดิน เข้าแสดงความยินดี กับผู้บริหารระดับสูง [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร022-en.php">Continue Read &raquo;</a></footer>
          </div>
				</article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร021-en.php"><img src="images/รูปหน้าปกข่าวสาร21.jpg" alt="รูปหน้าปกข่าวสาร21"></a>
              <figcaption>
                <time datetime="2020-10-6T08:15+00:00"><strong><font size="5.5px">6</font></strong><em>Oct</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">มูลนิธิครูดีของแผ่นดิน แสดงความยินดีผู้บริหารระดับสูง กระทรวงศึกษาธิการ<br></h4>
            <p style="color:rgb(243, 243, 243)">วันที่ 6 ตุลาคม 2563 ผู้แทนมูลนิธิครูดีของแผ่นดิน นำโดย นางปาลิดา กุลรุ่งโรจน์ ดร.กิตติกร คัมภีรปรีชา นายฐกร พฤฒิปูรณี เข้าแสดงความยินดี กับผู้บริหารระดับสูงกระทรวงศึกษาธิการ เนื่องใน [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร021-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร020-en.php"><img src="images/รูปหน้าปกข่าวสาร20.jpg" alt="รูปหน้าปกข่าวสาร20"></a>
              <figcaption>
                <time datetime="2020-2-13T08:15+00:00"><strong><font size="5.5px">27</font></strong><em>Jun</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">ประชุมผู้ทรงคุณวุฒิเพื่อการพัฒนาเครื่องมือวัดผู้บริหารดีของแผ่นดิน<br></h4>
            <p style="color:rgb(243, 243, 243)">เมื่อวันที่27 มิถุนายน 2563 ที่ผ่านมา นางปาลิดา กุลรุ่งโรจน์ ประธานอนุกรรมการโครงการเครือข่ายครูดีของแผ่นดิน ฯ ได้จัดประชุมผู้ทรงคุณวุฒิเพื่อพัฒนาเครื่องมือวัดผู้บริหารดีของแผ่นดิน [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร020-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
      </div>
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
      <div class="group latest">
        <article class="one_third first row4">
          <div class="center">
            <figure><a href="ข่าวสาร019-en.php"><img src="images/รูปหน้าปกข่าวสาร19.jpg" alt="รูปหน้าปกข่าวสาร19"></a>
              <figcaption>
                <time datetime="2020-2-13T08:15+00:00"><strong><font size="5.5px">19</font></strong><em>Jun</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">ชี้แจงโครงการฯ แก่หัวหน้ากลุ่มพัฒนาการศึกษา สำนักงานศึกษาธิการจังหวัด กลุ่มจังหวัดภาคกลาง<br></h4>
            <p style="color:rgb(243, 243, 243)">วันนี้ (19 มิ.ย. 63)  เวลา 10.30-12.00 น. สำนักงานปลัดกระทรวงศึกษาธิการ เรียนเชิญ นายฐกร พฤฒิปูรณี  กรรมการและเลขานุการมูลนิธิครูดีของแผ่นดิน เป็นวิทยากร [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร019-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร018-en.php"><img src="images/รูปหน้าปกข่าวสาร18.jpg" alt="รูปหน้าปกข่าวสาร18"></a>
              <figcaption>
                <time datetime="2020-02-14T08:15+00:00"><strong><font size="5.5px">18</font></strong><em>Jun</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31);">ประชุมพัฒนาตัวชี้วัดและเครื่อมมือวัดศน.ดีของแผ่นดิน</h4>
            <p style="color:rgb(243, 243, 243)">วันนี้ (18 มิ.ย. 63) มูลนิธิครูดีของแผ่นดิน ได้เชิญผู้ทรงคุณวุฒิและศึกษานิเทศก์จากหลายหน่วยงาน เช่น ดร.ชื่นฤดี บุตะเขียว ผอ.กลุ่มขับเคลื่อนการปฎิรูปประเทศยุทธศาสตร์ชาติ [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร018-en.php">Continue Read &raquo;</a></footer>
					</div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร017-en.php"><img src="images/รูปหน้าปกข่าวสาร17.jpg" alt="รูปหน้าปกข่าวสาร17"></a>
              <figcaption>
                <time datetime="2020-2-13T08:15+00:00"><strong><font size="5.5px">15</font></strong><em>Jun</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31)">ปรึกษาแนวทางการดำเนินโครงการศึกษานิเทศก์ดีของแผ่นดิน<br></h4>
            <p style="color:rgb(243, 243, 243)">วันนี้ (15 มิถุนายน 2563) นายฐกร พฤฒิปูรณี กรรมการและเลขานุการมูลนิธิครูดีของแผ่นดิน ได้เข้าพบ ดร.กิตติกร คัมภีรปรีชา ศึกษานิเทศก์เชี่ยวชาญพิเศษ [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร017-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
			</div>
			<!-- ################################################################################################ -->
      <!-- ################################################################################################ -->
      <div class="group latest">
        <article class="one_third first row4">
          <div class="center">
            <figure><a href="ข่าวสาร016-en.php"><img src="images/รูปหน้าปกข่าวสาร16.jpg" alt="รูปหน้าปกข่าวสาร16"></a>
              <figcaption>
                <time datetime="2020-02-14T08:15+00:00"><strong><font size="5.5px">8</font></strong><em>Jun</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31);">หารือแนวทางการขับเคลื่อนโครงการเครือข่ายครูดีของแผ่นดิน กับเลขานุการรัฐมนตรีว่าการกระทรวงศึกษาธิการ</h4>
            <p style="color:rgb(243, 243, 243)">เมื่อวันที่ 8 มิถุนายน 2563 เวลา 16.00 น. พลเอกเอกชัย ศรีวิลาศ ประธานมูลนิธิครูดีของแผ่นดิน พร้อมด้วย นางปาลิดา กุลรุ่งโรจน์ ประธานโครงการ [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร016-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร015-en.php"><img src="images/รูปหน้าปกข่าวสาร15.jpg" alt="รูปหน้าปกข่าวสาร15"></a>
              <figcaption>
                <time datetime="2020-02-14T08:15+00:00"><strong><font size="5.5px">30</font></strong><em>May</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31);">ประชุมอาสาของแผ่นดิน สาขากัลยาณมิตร</h4>
            <p style="color:rgb(243, 243, 243)">วันนี้ (30 พฤษภาคม 63) เวลา 10.00-10.40 น. มูลนิธิครูดีของแผ่นดิน นำโดย นายฐกร พฤฒิปูรณี กรรมการและเลขานุการมูลนิธิ ประชุมชี้แจงภาพรวม และ [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร015-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
        <article class="one_third row4">
          <div class="center">
            <figure><a href="ข่าวสาร014-en.php"><img src="images/รูปหน้าปกข่าวสาร14.jpg" alt="รูปหน้าปกข่าวสาร14"></a>
              <figcaption>
                <time datetime="2020-02-14T08:15+00:00"><strong><font size="5.5px">13</font></strong><em>Mar</em></time>
              </figcaption>
            </figure>
          </div>
          <div class="txtwrap">
            <h4 class="heading" style="color:rgb(180,147,31);">ปลัดกระทรวงศึกษาธิการ เชิญผู้แทน สช. และ กศน. ร่วมประชุมขับเคลื่อนโครงการเครือข่ายครูดีของแผ่นดินสู่ ครู สช. และกศน.</h4>
						<p style="color:rgb(243, 243, 243)">วันนี้ (13 มี.ค. 2563) ปลัดกระทรวงศึกษาธิการ (นายประเสริฐ บุญเรือง) ได้เชิญผู้แทนสำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน (สช.) [&hellip;]</p>
            <footer><a style="font-size:17px" class="readnext" href="ข่าวสาร014-en.php">Continue Read &raquo;</a></footer>
          </div>
        </article>
      </div>
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
		</div>
		<!-- ################################################################################################ -->
  </section>
</div>
<!-- End Content 01 - ข่าวสารรวมlatest -->
<!-- ################################################################################################ -->
<!-- Start Pagination -->
<div class="wrapper row3 coloured" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
  <main class="hoc container clear">
    <!-- ################################################################################################ -->
    <div class="content">
      <nav class="pagination">
				<ul>
          <li class="current"><strong>3</strong></li>
					<li><a href="ข่าวสารรวม002-en.php">2</a></li>
					<li><a href="ข่าวสารรวม001-en.php">1</a></li>
          <li><a href="ข่าวสารรวม002-en.php">Next &raquo;</a></li>
        </ul>
      </nav>
    </div>
    <!-- ################################################################################################ -->
  </main>
</div>
<!-- End Pagination -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer-en.php'); ?>
<!-- End FooterCopyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>