<?php 
	session_start();
	require_once('condb.php');

	$email = $_SESSION['email'];

	/* SUM all products price */
	$sqlcount="SELECT SUM(product_total) AS price_total FROM cart WHERE email='$email' ";
	$ressultcount=mysqli_query($con,$sqlcount);
	$rowcount=mysqli_fetch_array($ressultcount);
	$price_total=$rowcount['price_total'];

	if($price_total==0) { // Check if user still not add any product in cart, then go to shop page
		echo "<script>
			alert('คุณยังไม่มีสินค้าในตะกร้า สามารถเลือกสินค้าในร้านค้าของมูลนิธิได้เลยค่ะ');
			window.location.href='สนับสนุนของที่ระลึก-en.php';
			</script>";
	}
	
	$doc_address = $_SESSION['docaddress'];

?>


<!DOCTYPE html>

<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</style>
</head>


<body id="top" onLoad="window.scroll(0, <?php echo $_GET['win_scroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
				<?php if (strpos($_SESSION['email'], '@') !== false) {
								if ($_SESSION["level"]=="admin") { //Admin Session ?>
								<li style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> Admin</a>
									<ul>
										<li><a href="ระบบหลังบ้านร้านค้า-en.php">ระบบหลังบ้านร้านค้า</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberGeneral') { //General Member Session ?>
								<li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> General Member</li>
          <?php }elseif ($_SESSION['level']=='memberSilver') { //Member Silver Session ?>
								<li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> Silver Member</li>
          <?php }elseif ($_SESSION['level']=='memberGold') { //Member Gold Session ?>
								<li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> Gold Member</li>
          <?php }elseif ($_SESSION['level']=='memberDiamond') { //Member Diamond Session ?>
								<li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> Diamond Member</li>
          <?php } ?>
								<li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
        <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login-en.php"><i class="fas fa-user-lock"></i> <text1>Log in</text1></a></li>
                <li class="faicon-login"><a href="signup-en.php"><i class="fas fa-file-signature"></i> <text1>Sign up</text1></a></li>
				<?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
                  <ul>
                    <li><a href="ตะกร้า.php">ภาษาไทย</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                    <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก-en.php">สนับสนุนของที่ระลึก</a></li>
                  </ul>
                </li>
								<li class="faicon-basket"><i class="fas fa-caret-down"></i><i class="fa fa-shopping-basket"></i> <?php echo number_format($price_total,2); ?>
									<ul>
										<li><a href="สนับสนุนของที่ระลึก-en.php" >Shopping Online</a></li>
										<li><a href="ตะกร้า-en.php">View Cart</a></li>
										<li><a href="ตรวจสอบตะกร้า-en.php">Checkout</a></li>
										<li><a href="ประวัติและสถานะการสั่งซื้อ-en.php">Order History and Status</a></li>
									</ul>
								</li>
        </ul>
			</nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index-en.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index-en.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index-en.php">Home</a></li>
          <li><a href="เกี่ยวกับเรา-en.php">About us</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>Join us</a>
            <ul>
              <li><a href="ร่วมโครงการ-en.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <li><a href="โครงการครูดีของแผ่นดิน-en.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา-en.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA-en.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน-en.php">โครงการอาสาของแผ่นดิน</a></li>
            </ul>
          </li>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">Innovation Library</a></li>
          <li><a href="ติดต่อเรา-en.php">Contact us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index-en.php">Home</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="สนับสนุนของที่ระลึก-en.php">Shopping Online</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">My Cart</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ตะกร้าของฉัน -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<form action="ตรวจสอบตะกร้า-en.php" method="POST">
		<div class="cart-box-main">
			<div class="container">
				<!-- Start table of my product detail -->
				<div class="row">
					<div class="col-lg-12">
						<div class="table-main table-responsive">
							<!-- ################################################################################################ -->
							<?php
								$sqlcart = "SELECT `product_id` FROM cart WHERE email='$email' AND product_id IN ('1','11'); ";
								$recart = mysqli_query($con,$sqlcart);
								$rowcart = mysqli_fetch_array($recart);
							?>
							<table class="table">
								<thead>
									<tr>
										<th></th>
										<th class="product-explain">ของที่ระลึก</th>
										<?php if(isset($rowcart['product_id'])) { ?>
														<th>ขนาดเสื้อ</th>
										<?php } ?>
										<th>ราคา/ชิ้น</th>
										<th>จำนวน</th>
										<th>ราคารวม</th>
										<th>ยกเลิกสินค้า</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$results_per_page = 9; // number of results per page
									if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
									$start_from = ($page-1) * $results_per_page;

									$sql = "SELECT * FROM cart WHERE email='$email' ORDER BY product_id ASC LIMIT $start_from, ".$results_per_page;
									$rs_result = $con->query($sql);

									$i=1;
									while($row = $rs_result->fetch_assoc()) {
										/* Set product_size and product_detail by call sql_shelf with product_id */
										$product_id = $row['product_id'];
										$sql_shelf = "SELECT * FROM shelf WHERE product_id='$product_id' ";
										$rs_shelf = $con->query($sql_shelf);
										$row_shelf = $rs_shelf->fetch_assoc();
										?>
										<tr>
											<!-- Product Image -->
											<td class="thumbnail-img">
												<a href="#" onclick="return false">
													<img class="img-fluid" src="<?php echo $row['product_image']; ?>" alt="<?php echo $row['product_image']; ?>"/>
												</a>
											</td>
											<!-- Product Detail -->
											<td class="name-pr product-explain lh-1-0" <?php if(isset($rowcart['product_id']) && $row['product_id'] != 1 && $row['product_id'] != 11) {?> colspan="2" <?php } ?> >
												<a href="#" onclick="return false">
													<?php echo $row['product_name']; ?>
													<div class="fs-15">
														<?php
															if(isset($row_shelf['product_size'])) { echo $row_shelf['product_size']; }
															if(isset($row_shelf['product_detail'])) { echo $row_shelf['product_detail']; }
														?>
													</div>
												</a>
											</td>
											<!-- In case of Product Set, need to choose shirt size -->
											<?php if($row['product_id'] == 1) { ?>
															<td>
																<select name="shirtsize1" required>
																	<option value="" disabled="disabled" selected="selected">โปรดเลือก</option>
																	<option value="SS (34 นิ้ว)">SS (34 นิ้ว)</option>
																	<option value="S (36 นิ้ว)">S (36 นิ้ว)</option>
																	<option value="M (38 นิ้ว)">M (38 นิ้ว)</option>
																	<option value="L (40 นิ้ว)">L (40 นิ้ว)</option>
																	<option value="XL (42 นิ้ว)">XL (42 นิ้ว)</option>
																	<option value="XXL (44 นิ้ว)">XXL (44 นิ้ว)</option>
																</select>
															</td>
											<?php }
														if($row['product_id'] == 11) { ?>
															<td>
																<select name="shirtsize11" required>
																	<option value="" disabled="disabled" selected="selected">โปรดเลือก</option>
																	<option value="SS (34 นิ้ว)">SS (34 นิ้ว)</option>
																	<option value="S (36 นิ้ว)">S (36 นิ้ว)</option>
																	<option value="M (38 นิ้ว)">M (38 นิ้ว)</option>
																	<option value="L (40 นิ้ว)">L (40 นิ้ว)</option>
																	<option value="XL (42 นิ้ว)">XL (42 นิ้ว)</option>
																	<option value="XXL (44 นิ้ว)">XXL (44 นิ้ว)</option>
																</select>
															</td>
											<?php } ?>
											<!-- Price per unit -->
											<td class="price-pr">
												<p><?php echo number_format($row['product_price'],2); ?></p>
											</td>
											<!-- Product Amount -->
											<td class="quantity-box">
												<!-- Set valuable for pass to updateProductAmount script -->
												<?php $x = $row['product_amount']; ?>
												<?php $y = $row['product_price']; ?>
												<?php $z = $row['cart_id']; ?>
												<?php if($row['product_amount'] == 1) { ?>
																<div class="update-product_amount-btn" style="cursor:not-allowed; box-shadow:none; outline:none;" 
																onclick="return false">&#10094;</div>
												<?php }else{ ?>
																<div class="update-product_amount-btn" 
																onclick="updateProductAmount(-1,<?php echo $i;?>,<?php echo $x;?>,<?php echo $y;?>,<?php echo $z;?>)">&#10094;</div>
												<?php } ?>
												<div class="update-product_amount-btn number" id="id-for-update-product_amount<?php echo $i;?>"> <?php echo $row['product_amount']; ?> </div>
												<div class="update-product_amount-btn" onclick="updateProductAmount(1,<?php echo $i;?>,<?php echo $x;?>,<?php echo $y;?>,<?php echo $z;?>)">&#10095;</div>
											</td>
											<!-- Total -->
											<td class="price-pr">
												<p><?php echo number_format($row['product_price']*$row['product_amount'],2); ?></p>
											</td>
											<!-- Cancel Order Button -->
											<td class="remove-pr">
												<a href="delcart-en.php?cart_id=<?php echo $row['cart_id'] ?>">
													<i class="fas fa-times"></i>
												</a>
											</td>
										</tr>
									<?php $i++; } ?>
								</tbody>
							</table>
							<!-- ################################################################################################ -->
						</div>
					</div>				
				</div>
				<!-- End table of my product detail -->
				<!-- ################################################################################################ -->
				<!-- Start Order Summary -->
				<div class="row my-5">
					<div class="col-lg-6 col-sm-12"></div>
					<div class="col-lg-6 col-sm-12">
						<div class="order-box">
							<!-- Total price -->
							<h3>สรุปยอดการสนับสนุน</h3>
							<div class="d-flex">
								<div class="ml-auto font-weight-bold"> <?php echo number_format($price_total,2); ?> บาท </div>
							</div>
							<hr>
							<!-- Shipping choices -->
							<div class="d-flex">
								<h3>เลือกวิธีการจัดส่ง</h3>
								<label>
									
									ฟรีค่าจัดส่ง
									<!--
									<br>
									ระยะเวลาการจัดส่ง: 3 - 7 วันทำการ
									<input type="radio" name="shipping_kind" value="normal" checked>
									+ ค่าจัดส่ง 50 บาท
									<img src="images/ThailandPost_Logo.png" alt="ThailandPost_Logo">
									พัสดุลงทะเบียน
									<p>ระยะเวลาการจัดส่ง: 3 - 7 วันทำการ</p>
								</label>
								<label>
									<input type="radio" name="shipping_kind" value="ems">
									+ ค่าจัดส่ง 80 บาท
									<img src="images/ThailandPost_Logo.png" alt="ThailandPost_Logo">
									พัสดุด่วนพิเศษ EMS
									<p>ระยะเวลาการจัดส่ง: 1 - 3 วันทำการ</p>
									-->
								</label>
							</div>
							<hr>
							<!-- Shipping Address -->
							<div class="d-flex">
								<?php if (strpos($_SESSION['email'], '@') !== false) { ?>
									<h3>ที่อยู่ในการจัดส่ง (ข้อมูลตอนสมัครสมาชิก)</h3>
									<div class="location-box">
										<?php echo $_SESSION['docaddress']; ?>
									</div>
									หรือ<br>ระบุที่อยู่ในการจัดส่ง โดยการกรอกข้อมูลตามช่องด้านล่าง
									<input class="location-box new" type="text" name="new_address" placeholder="ที่อยู่ในการจัดส่ง" />
								<?php }else{ ?>
									ระบุที่อยู่ในการจัดส่ง
									<input class="location-box new" type="text" name="nomem_address" placeholder="ที่อยู่ในการจัดส่ง" required/>
								<?php } ?>
							</div>
							<hr>
							<!-- Receiver First-Lastname -->
							<?php if (strpos($_SESSION['email'], '@') !== false) {}else{ ?>
							<div class="d-flex">
								ระบุชื่อ-นามสกุลผู้รับ
								<input class="phonenum-box new" type="tel" name="nomem_name" placeholder="ชื่อ-นามสกุล" required/>
							</div>
							<hr>
							<?php } ?>
							<!-- Receiver Email -->
							<?php if (strpos($_SESSION['email'], '@') !== false) {}else{ ?>
							<div class="d-flex">
								ระบุอีเมล
								<input class="phonenum-box new" type="email" name="nomem_email" placeholder="อีเมล" required/>
							</div>
							<hr>
							<?php } ?>
							<!-- Receiver Phone Number -->
							<div class="d-flex">
								<?php if (strpos($_SESSION['email'], '@') !== false) { ?>
									<h3>เบอร์ติดต่อ ผู้รับ (ข้อมูลตอนสมัครสมาชิก)</h3>
									<div class="phonenum-box">
										<?php echo $_SESSION['phonenum']; ?>
									</div>
									หรือ<br>ระบุเบอร์ติดต่อ
									<input class="phonenum-box new" type="tel" name="nomem_phonenum" placeholder="เบอร์ติดต่อ" />
									<?php }else{ ?>
										ระบุเบอร์ติดต่อ
										<input class="phonenum-box new" type="tel" name="nomem_phonenum" placeholder="เบอร์ติดต่อ" required/>
								<?php } ?>
							</div>
						</div>
						<div class="d-flex shopping-box" style="text-align: right">
							<button type="submit" class="ml-auto btn hvr-hover">Order</button>
						</div>
					</div>
				</div>
				<!-- End Order Summary -->
			</div>
		</div>
	</form>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ตะกร้าของฉัน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear">
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Related Department</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Foundation of Thai Suprateacher</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->



<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>



<!-- JS for update (plus minus button) product_amount -->
<script src="js/updateProductAmount.js"></script>


</body>
</html>