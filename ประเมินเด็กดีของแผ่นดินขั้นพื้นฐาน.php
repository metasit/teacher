<?php 
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];
	date_default_timezone_set("Asia/Bangkok");

	if(isset($_SESSION['ID'])) {
		/* Call login table data from mySQL */
		$sql = "SELECT * FROM `basic_score4stu2` WHERE ID='$ID' AND basic_score4stu2_code='B' ";
		$re = mysqli_query($con,$sql);
		$row = mysqli_fetch_array($re);
		$check_fail = substr($row['basic_score4stu2_data'], 0, 4);
		
		if($check_fail == 'Fail') {

			/* Let set DateTime format before check more than 30 days can do basic_score again */
			$last_basic_score_date = new DateTime($row['basic_score4stu2_date']);
			$today_basic_score_date = new DateTime('tomorrow');
			$last_basic_score_date = $last_basic_score_date->settime(0,0); // No need time to check
			$today_basic_score_date  = $today_basic_score_date->settime(0,0); // No need time to check
			$diff = date_diff($last_basic_score_date,$today_basic_score_date); // Find interval date between last did basic_score and today basic_score
			$check_basic_score_date = $diff->format("%a");

			if($check_basic_score_date < 14) {
				header('location: popDobasic_score_again-เด็ก.php');
			}
		}elseif($check_fail == 'Pass') {
			echo '<script>';
				echo "alert('กรณีที่เคยผ่านแล้ว ไม่สามารถทำซ่ำได้ค่ะ');";
				echo "window.location.replace('แนบหลักฐานประกอบการพิจารณา-เด็ก.php')";
			echo '</script>';
		}
		
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบ เพื่อทำแบบประเมินคุณสมบัติพื้นฐานเด็กดีของแผ่นดินได้ค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>
<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li><a href="all_project.php"> ร่วมโครงการ</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการเด็กดีของแผ่นดิน.php"> โครงการเด็กดีของแผ่นดิน</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="registZidol.php"> เด็กดีขั้นพื้นฐาน</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">หลักฐานประกอบการพิจารณา</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ประเมินครูดีของแผ่นดินขั้นพื้นฐาน -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row">
			<div class="col-lg-12">
				<div class="table-main table-responsive hoc container">
					<!-- ################################################################################################ -->
					<h2 class="center m-b-50">แบบประเมินคุณสมบัติพื้นฐานของเด็กดีของแผ่นดิน</h2>
					<form action="uploadOthers-เด็ก.php?basic_score4stu2_code=B" method="POST">
						<div class="fs-20">
							<p><strong>1. ปัจจุบันนักเรียน/นักศึกษาดื่มสุรามากน้อยเพียงใด</strong></p>
							<input type="radio" id="never" name="basic_score1" value="3" required>
							<label for="never">ไม่เคยดื่มสุราเลย</label>
							<input type="radio" id="seldom" name="basic_score1" value="2" required>
							<label for="seldom">เคยดื่มสุรา แต่ปัจจุบันเลิกแล้ว</label>
							<input type="radio" id="sometimes" name="basic_score1" value="1" required>
							<label for="sometimes">ดื่มบ้างเป็นครั้งคราว เพื่อการเข้าสังคม</label>
							<input type="radio" id="always" name="basic_score1" value="0" required>
							<label for="always">ดื่มเป็นปกติ เพราะยุคสมัยนี้ ต้องดื่มเป็น</label>

							<p><strong>2. ปัจจุบันนักเรียน/นักศึกษาสูบบุหรี่มากน้อยเพียงใด</strong></p>
							<input type="radio" id="never" name="basic_score2" value="3" required>
							<label for="never">ไม่เคยสูบบุหรี่เลย</label>
							<input type="radio" id="seldom" name="basic_score2" value="2" required>
							<label for="seldom">เคยสูบบุหรี่ แต่ปัจจุบันเลิกแล้ว</label>
							<input type="radio" id="sometimes" name="basic_score2" value="1" required>
							<label for="sometimes">สูบบ้างเป็นครั้งคราว</label>
							<input type="radio" id="always" name="basic_score2" value="0" required>
							<label for="always">สูบเป็นปกติ</label>

							<p><strong>3. ปัจจุบันนักเรียน/นักศึกษายุ่งเกี่ยวกับยาเสพติด หรือการพนันมากน้อยเพียงใด</strong></p>
							<input type="radio" id="never" name="basic_score3" value="3" required>
							<label for="never">ไม่เคยเลย</label>
							<input type="radio" id="seldom" name="basic_score3" value="2" required>
							<label for="seldom">เคยยุ่งเกี่ยว แต่ปัจจุบันเลิกแล้ว</label>
							<input type="radio" id="sometimes" name="basic_score3" value="1" required>
							<label for="sometimes">ยุ่งเกี่ยวบ้างเป็นครั้งคราว </label>
							<input type="radio" id="always" name="basic_score3" value="0" required>
							<label for="always">ยุ่งเกี่ยวเป็นปกติ </label>
						</div>

						<button type="submit" class="btnJoin" style="color:white; cursor:pointer; width:80%; margin-top:50px;"><h1>ส่งคะแนนประเมิน</h1></button>

					</form>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ประเมินครูดีของแผ่นดินขั้นพื้นฐาน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

</body>
</html>