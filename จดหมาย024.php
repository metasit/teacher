<?php

	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];

?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="จดหมาย024-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="จดหมายรวมlatest.php">จดหมาย/ประกาศ</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="จดหมาย024.php"> จดหมายการทำหน้าที่อาสาฯ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - จดหมาย024 -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <article style="text-align:center" >
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px;">
        <strong>จดหมายการทำหน้าที่อาสาฯ</strong>
      </p>
      <ul class="font-x2plus textlink" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
        <li><a href="docs/จดหมาย/จดหมาย24/จม.การทำหน้าที่อาสาฯ.pdf" target="_blank">1. จดหมายการทำหน้าที่อาสาฯ.pdf</a></li>
        <li><a href="docs/จดหมาย/จดหมาย24/ฉบับที่ 1 ประก่าศขยายเวลา อาสา.pdf" target="_blank">2. ฉบับที่ 1 ประก่าศขยายเวลา อาสา</a></li>
        <li><a href="docs/จดหมาย/จดหมาย24/ฉบับที่ 2 ประกาศขอลาออกอาสา.pdf" target="_blank">3. ฉบับที่ 2 ประกาศขอลาออกอาสา</a></li>
        <li><a href="docs/จดหมาย/จดหมาย24/ฉบับที่ 3 ประกาศยุติการเป็นอาสา.pdf" target="_blank">4. ฉบับที่ 3 ประกาศยุติการเป็นอาสา</a></li>
      </ul>
    </article>
  </main>
</div>
<!-- End Content 01 - จดหมาย024 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>