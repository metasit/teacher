<?php
	session_start();
	if(isset($_SESSION['ID'])) {
		echo "<script>window.history.go(-1)</script>";
	}else{
		require_once('condb.php');
	}


?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">

	<style>
input[type="date"]:before {

content: "วันเกิด" !important;
margin-right:15px;

}
input[type=date]:required:invalid::-webkit-datetime-edit {
    color: transparent;
}
input[type=date]:focus::-webkit-datetime-edit {
    color: black !important;
}

	</style>

</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100">
			<!-- ################################################################################################ -->
			<div class="textlink3 PreMenu_fl_left">
				<a href="index.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าหลัก</a>
			</div>
			<!-- ################################################################################################ -->
			<div class="textlink3 PreMenu_fl_right" style="padding:2px;">
				<a href="login.php">ไปหน้าเข้าสู่ระบบสมาชิก</a>
			</div>
			<!-- ################################################################################################ -->
			<div class="login100-pic">
				<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
			</div>
			<!-- ################################################################################################ -->
			<form class="login100-form validate-form" name="formsignup" action="savesignup.php" method="POST">
				<!-- ################################################################################################ -->
				<span class="login100-form-title">
					ระบบสมัครสมาชิก
				</span>
				<!-- ################################################################################################ -->
				<div class="wrap-input100 validate-input">
					<input class="input100" type="email" name="email" class="form-control" placeholder="อีเมล" 
					pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$" title="โปรดใส่อีเมลให้ถูกต้อง" required />
					<span class="symbol-input100">
						<i class="fas fa-user" aria-hidden="true"></i>
					</span>
				</div>
				<!-- ################################################################################################ -->
				<div class="wrap-input100 validate-input">
					<input class="input100" type="password" name="password" class="form-control" placeholder="รหัสผ่าน" 
					pattern=".{8,}" title="โปรดใส่จำนวนรหัสผ่านเท่ากับหรือมากกว่า 8 ตัว" required />
					<span class="symbol-input100">
						<i class="fa fa-lock" aria-hidden="true"></i>
					</span>
				</div>
				<!-- ################################################################################################ -->
				<div class="wrap-input100 validate-input">
					<input class="input100" type="password" name="conpassword" class="form-control" placeholder="กรอกรหัสผ่านอีกครั้ง" 
					pattern=".{8,}" title="โปรดใส่จำนวนรหัสผ่านเท่ากับหรือมากกว่า 8 ตัว" required />
					<span class="symbol-input100">
						<i class="fa fa-lock" aria-hidden="true"></i>
					</span>
				</div>
				<!-- ################################################################################################ -->
				<select name="pre_name" class="form-control" style="margin-bottom:10px; height:40px" onchange="PickPrename(this.value);" required>
					<option value="" disabled="disabled" selected="selected">เลือกคำหน้านาม</option>
					<option value="A">นาย</option>
					<option value="B">นาง</option>
					<option value="C">นางสาว</option>
					<option value="D">ด.ช.</option>
					<option value="E">ด.ญ.</option>
					<option value="O">อื่นๆ</option>
				</select>
				<div id="pre_name_text" class="wrap-input100 validate-input" style="display:none">
					<input class="input100" type="text" id="pre_name_text_required" name="pre_name_text" class="form-control" placeholder="โปรดระบุคำหน้านาม" max-lenght="30" />
					<span class="symbol-input100">
						<i class="fas fa-file-signature" aria-hidden="true"></i>
					</span>
				</div>
				<!-- ################################################################################################ -->
				<div class="wrap-input100 validate-input">
					<input class="input100" type="text" name="firstname" class="form-control" placeholder="ชื่อ" max-lenght="100" required />
					<span class="symbol-input100">
						<i class="fas fa-file-signature" aria-hidden="true"></i>
					</span>
				</div>
				<!-- ################################################################################################ -->
				<div class="wrap-input100 validate-input">
					<input class="input100" type="text" name="lastname" class="form-control" placeholder="นามสกุล" max-lenght="100" required />
					<span class="symbol-input100">
						<i class="fas fa-file-signature" aria-hidden="true"></i>
					</span>
				</div>
				<!-- ################################################################################################ -->
				<div class="wrap-input100 validate-input">
					<input 
					id="tel_ck"
					class="input100" 
					type="text" name="phonenum" class="form-control" 
					placeholder="เบอร์โทรศัพท์" pattern="[0][0-9]{9}" maxlength="10"
					title="โปรดใส่จำนวนตัวเลขให้ครบ 10 ตัว (รวมเลข 0 ตัวแรก)" required />
					<span class="symbol-input100">
						<i class="fas fa-mobile-alt" aria-hidden="true"></i>
					</span>
					<em>
				</div>
				<!-- ################################################################################################ -->
				<div class="font-10 font-weight-bold text-dark mb-2 pl-4 ">ที่อยู่จัดส่งเอกสาร</div>
				
				
				<div class="wrap-input100 validate-input">
					<input class="input100" type="text" name="docaddress" class="form-control" placeholder="ที่อยู่จัดส่งเอกสาร" max-lenght="100" required />
					<span class="symbol-input100">
						<i class="fas fa-file-signature" aria-hidden="true"></i>
					</span>
				</div>

				
				<div class="wrap-input100 validate-input">
				<select id="province_token"   name="province" class="form-control" style="margin-bottom:10px; height:40px"  required>
					<option value="" selected> เลือกจังหวัด </option>
					<option value="กรุงเทพมหานคร">กรุงเทพมหานคร</option>
					<option value="กระบี่">กระบี่ </option>
					<option value="กาญจนบุรี">กาญจนบุรี </option>
					<option value="กาฬสินธุ์">กาฬสินธุ์ </option>
					<option value="กำแพงเพชร">กำแพงเพชร </option>
					<option value="ขอนแก่น">ขอนแก่น</option>
					<option value="จันทบุรี">จันทบุรี</option>
					<option value="ฉะเชิงเทรา">ฉะเชิงเทรา </option>
					<option value="ชัยนาท">ชัยนาท </option>
					<option value="ชัยภูมิ">ชัยภูมิ </option>
					<option value="ชุมพร">ชุมพร </option>
					<option value="ชลบุรี">ชลบุรี </option>
					<option value="เชียงใหม่">เชียงใหม่ </option>
					<option value="เชียงราย">เชียงราย </option>
					<option value="ตรัง">ตรัง </option>
					<option value="ตราด">ตราด </option>
					<option value="ตาก">ตาก </option>
					<option value="นครนายก">นครนายก </option>
					<option value="นครปฐม">นครปฐม </option>
					<option value="นครพนม">นครพนม </option>
					<option value="นครราชสีมา">นครราชสีมา </option>
					<option value="นครศรีธรรมราช">นครศรีธรรมราช </option>
					<option value="นครสวรรค์">นครสวรรค์ </option>
					<option value="นราธิวาส">นราธิวาส </option>
					<option value="น่าน">น่าน </option>
					<option value="นนทบุรี">นนทบุรี </option>
					<option value="บึงกาฬ">บึงกาฬ</option>
					<option value="บุรีรัมย์">บุรีรัมย์</option>
					<option value="ประจวบคีรีขันธ์">ประจวบคีรีขันธ์ </option>
					<option value="ปทุมธานี">ปทุมธานี </option>
					<option value="ปราจีนบุรี">ปราจีนบุรี </option>
					<option value="ปัตตานี">ปัตตานี </option>
					<option value="พะเยา">พะเยา </option>
					<option value="พระนครศรีอยุธยา">พระนครศรีอยุธยา </option>
					<option value="พังงา">พังงา </option>
					<option value="พิจิตร">พิจิตร </option>
					<option value="พิษณุโลก">พิษณุโลก </option>
					<option value="เพชรบุรี">เพชรบุรี </option>
					<option value="เพชรบูรณ์">เพชรบูรณ์ </option>
					<option value="แพร่">แพร่ </option>
					<option value="พัทลุง">พัทลุง </option>
					<option value="ภูเก็ต">ภูเก็ต </option>
					<option value="มหาสารคาม">มหาสารคาม </option>
					<option value="มุกดาหาร">มุกดาหาร </option>
					<option value="แม่ฮ่องสอน">แม่ฮ่องสอน </option>
					<option value="ยโสธร">ยโสธร </option>
					<option value="ยะลา">ยะลา </option>
					<option value="ร้อยเอ็ด">ร้อยเอ็ด </option>
					<option value="ระนอง">ระนอง </option>
					<option value="ระยอง">ระยอง </option>
					<option value="ราชบุรี">ราชบุรี</option>
					<option value="ลพบุรี">ลพบุรี </option>
					<option value="ลำปาง">ลำปาง </option>
					<option value="ลำพูน">ลำพูน </option>
					<option value="เลย">เลย </option>
					<option value="ศรีสะเกษ">ศรีสะเกษ</option>
					<option value="สกลนคร">สกลนคร</option>
					<option value="สงขลา">สงขลา </option>
					<option value="สมุทรสาคร">สมุทรสาคร </option>
					<option value="สมุทรปราการ">สมุทรปราการ </option>
					<option value="สมุทรสงคราม">สมุทรสงคราม </option>
					<option value="สระแก้ว">สระแก้ว </option>
					<option value="สระบุรี">สระบุรี </option>
					<option value="สิงห์บุรี">สิงห์บุรี </option>
					<option value="สุโขทัย">สุโขทัย </option>
					<option value="สุพรรณบุรี">สุพรรณบุรี </option>
					<option value="สุราษฎร์ธานี">สุราษฎร์ธานี </option>
					<option value="สุรินทร์">สุรินทร์ </option>
					<option value="สตูล">สตูล </option>
					<option value="หนองคาย">หนองคาย </option>
					<option value="หนองบัวลำภู">หนองบัวลำภู </option>
					<option value="อำนาจเจริญ">อำนาจเจริญ </option>
					<option value="อุดรธานี">อุดรธานี </option>
					<option value="อุตรดิตถ์">อุตรดิตถ์ </option>
					<option value="อุทัยธานี">อุทัยธานี </option>
					<option value="อุบลราชธานี">อุบลราชธานี</option>
					<option value="อ่างทอง">อ่างทอง </option>
					<option value="อื่นๆ">อื่นๆ</option>
				</select>
					<!-- <input class="input100" type="text" name="province" class="form-control" placeholder="จังหวัด" required /> -->

				</div>
				
				
				<div class="wrap-input100 validate-input">

				<!-- <select name="district" id="district" class=" d-none form-control">
					<option value="">เลือกอำเภอ</option>
				</select> -->

				<select name="district" id="district" class=" d-none form-control" style="margin-bottom:10px; height:40px"  required>
					<option value="" disabled="disabled" selected="selected">เลือกอำเภอ</option>
				</select>

				<!-- <input class="input100" type="text" name="district_sub" class="form-control" placeholder="อำเภอ" required />
					<span class="symbol-input100">
						<i class="fas fa-address-book" aria-hidden="true"></i>
					</span> -->


				</div>
				
				<div class="wrap-input100 validate-input">
					<!-- <select name="district_sub" id="district_sub" class=" d-none form-control"> -->
						<!-- <option value="">เลือกอำเภอ</option> -->
					<!-- </select>	 -->
				<select name="district_sub" id="district_sub" class=" d-none  form-control" style="margin-bottom:10px; height:40px"  required>
					<!-- <option value="" disabled="disabled" selected="selected">เลือกอำเภอ</option> -->
				</select>

				<!-- <input class="input100" type="text" name="district" class="form-control" placeholder="ตำบล" required />
					<span class="symbol-input100">
						<i class="fas fa-address-book" aria-hidden="true"></i>
					</span> -->
				</div>


				<div class="wrap-input100 validate-input">
					<input disabled class="input100" type="text" id="zip_code_token" name="zipcode" class="form-control" placeholder="รหัสไปรษณีย์" required />
					<span class="symbol-input100">
						<i class="fas fa-address-book" aria-hidden="true"></i>
					</span>
				</div>





				<!-- docaddress -->
				<!-- ################################################################################################ -->
				<div class="wrap-input100 validate-input">
					<input  
					placeholder="วันเกิด"  
					class="input100" 
					type="date" 
					name="birthdate" 
					min='1950-01-01' 
					max="<?php echo date('Y-m-d'); ?>" 
					required />
					<span class="symbol-input100">
					<span class="symbol-input100">
						<i class="fas fa-birthday-cake" aria-hidden="true"></i>
					</span>
						
						<!-- <i class="fas fa-birthday-cake" aria-hidden="true"></i> -->
					</span>
				</div>
				<!-- ################################################################################################ -->
				<div class="container-login100-form-btn">
					<button class="login100-form-btn" type="submit" id="register">
						สมัครสมาชิก
					</button>
				</div>
				<!-- ################################################################################################ -->
				<input type="hidden" name="CFP" value="20">
			</form>
			<!-- ################################################################################################ -->
		</div>
	</div>
</div>

<script>
      if(localStorage.getItem('production')=="1") {
        window.url = "https://localhost/";
      }
      else if(localStorage.getItem('production')=="2"){
        window.url = "https://www.thaisuprateacher.org/dev/";
      }

	function PickPrename(val) {
		var elePrename = document.getElementById('pre_name');
		var elePrenameText = document.getElementById('pre_name_text');
		var elePrenameTextReq = document.getElementById('pre_name_text_required');
		if(val == 'O') {
			elePrenameText.style.display = 'block';
			elePrenameTextReq.required = true;
		}else{
			elePrenameText.style.display = 'none';
			elePrenameText.value = '';
			elePrenameTextReq.required = false;
		}
	}

	var url_token
		if(localStorage.getItem('production')=="1") {
			url_token = `${window.url}Thaisuprateacher/signup_data.php`
		}
		else {
			url_token = `${window.url}signup_data.php`
		}

	let province_token = document.getElementById('province_token')
	province_token.addEventListener('change',e =>{
		console.log(e.target.value);
		var formData = new FormData();
		formData.append('province', e.target.value);
		if(e.target.value!=""){
			let district = document.getElementById('district');
			 district.classList.remove("d-none");
		}



		fetch(url_token, {
		  method:"post",
		  body:formData
		})
		.then(function(response) {
		  return response.json();
		})
		.then((value)=>{
			console.log(value,'value');
			let str = "<option value =''>เลือกอำเภอ</option>";
			let district = document.getElementById('district')
			district.innerHTML = '';

			value.forEach(value_str => {
					console.log(value_str.district);
				str+= `<option value='${value_str.district+"/"+value_str.zip_code}' >`+value_str.district+"</option>";
			});

			district.innerHTML = str;
		})
		.catch(function(error) {
		  console.log("Request failed", error);
		});
	})

	let district = document.getElementById('district');
	district.addEventListener('change',e =>{
		
		console.log(e.target.value);
		var formData = new FormData();
		if(e.target.value!=""){
			let district_sub = document.getElementById('district_sub');
			 district_sub.classList.remove("d-none");
		}


		// console.log(e.target.value,'e.target.value');
		formData.append('district', e.target.value);
		if(e.target.value!=""){
			let district_sub = document.getElementById('district');
			district_sub.classList.remove("d-none");
		}
		fetch(url_token, {
		  method:"post",
		  body:formData
		})
		.then(function(response) {
		  return response.json();
		})
		.then((value)=>{
			// console.log(value);
			console.log(value,'value');
			let str = "<option value =''>เลือกตำบล</option>";
			let district = document.getElementById('district_sub')
			district.innerHTML = '';
			value.forEach(value_str => {
					console.log(value_str.district_sub);
				str+= `<option value='${value_str.district_sub+"/"+value_str.zip_code}' >`+value_str.district_sub+"</option>";
			});
			
			district.innerHTML = str;
		})
		.catch(function(error) {
		  console.log("Request failed", error);
		});

	})

	
	let district_sub = document.getElementById('district_sub');
	district_sub.addEventListener('change',e =>{
	
		let zip_code_token = document.getElementById('zip_code_token');
		zip_code_token.value = e.target.value.split("/")[1];
	})
	let tel_ck =  document.getElementById('tel_ck');

	tel_ck.addEventListener('keyup',e =>{
		let ans = (Number.isInteger(parseInt(event.target.value)));
		if (ans) {
			let str = e.target.value;
			if(str.length >10){
				e.target.value = str.substr(0,10);
			}
		}
		else {
			e.target.value = e.target.value.substr(0,+e.target.value.length-1)
		}
	})





</script>

</body>
</html>