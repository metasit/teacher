<?php 
	session_start();
	require_once('condb.php');
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  

<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom4Inno.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">คลังสื่อนวัตกรรม</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - คลังสื่อนวัตกรรม -->
<div class="wrapper bgded coloured" style="background-color:rgb(226,255,224);">
  <div class="hoc container testimonials clear">
    <!-- Filter -->
    <form method="POST">
      <input type="submit" class="btnfilter1" name="filter" value="แสดงทั้งหมด" />
      <input type="submit" class="btnfilter1" name="filter" value="การพัฒนาคุณลักษณะอันพึงประสงค์" />
      <input type="submit" class="btnfilter1" name="filter" value="หลักสูตรและวิธีการสอน" />
      <input type="submit" class="btnfilter1" name="filter" value="จิตวิทยาและการแนะแนว" />
      <input type="submit" class="btnfilter1" name="filter" value="เทคโนโลยีการศึกษา" />
    </form>
    <!-- Filter -->
    
<?php


    $results_per_page = 9; // number of results per page
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
    $start_from = ($page-1) * $results_per_page;
    
    switch($_POST['filter'])
    {
      case 'การพัฒนาคุณลักษณะอันพึงประสงค์':
        $sql = "SELECT * FROM innovation WHERE cateFilter LIKE 'devatt' ORDER BY ID ASC LIMIT $start_from, ".$results_per_page;
      break;
      case 'หลักสูตรและวิธีการสอน':
        $sql = "SELECT * FROM innovation WHERE cateFilter LIKE 'coutea' ORDER BY ID ASC LIMIT $start_from, ".$results_per_page;
      break;
      case 'จิตวิทยาและการแนะแนว':
        $sql = "SELECT * FROM innovation WHERE cateFilter LIKE 'psygui' ORDER BY ID ASC LIMIT $start_from, ".$results_per_page;
      break;
      case 'เทคโนโลยีการศึกษา':
        $sql = "SELECT * FROM innovation WHERE cateFilter LIKE 'tecedu' ORDER BY ID ASC LIMIT $start_from, ".$results_per_page;
      break;
      default:
        $sql = "SELECT * FROM innovation ORDER BY ID ASC LIMIT $start_from, ".$results_per_page;        
		}
    // $sql = "SELECT * FROM innovation ";
    // $result = $con->query($sql);
    // var_dump($result);
    // while($row = $rs_result->fetch_assoc())
    // {
    //   var_dump($row);
    //   echo "<br><br>";
    // }
    // exit();
		// var_dump($sql);
    // exit();
    $rs_result = $con->query($sql);
    // echo "DDD<br/>";
    // var_dump($rs_result);
    // exit();
    // echo "AAA";
    while($row = $rs_result->fetch_assoc())
    {
      // var_dump($row);
      // echo "<br/><br/>";
      // exit();
?>
      <div class="show-fil <?php echo $row["cateFilter"];?>-fil">
        <article class="center">
          <a href="<?php echo $row["contentPage"]; ?>.php">
            <img class="zoom108" src="<?php echo $row["posterDir"]; ?>" alt="<?php echo $row["posterDir"]; ?>">
            <div>
              <h85 class="heading"><?php echo $row["headerName"]; ?></h85>
              <br>
              <h class="text-gold fs-25" style="font-family:RSUText"><?php echo $row["authorFName"]." ".$row["authorLName"]; ?></h>
            </div>
            <em><?php echo $row["schoolName"] ?></em>
            <div class="text-black" style="margin-top:10px">
              <?php if($row["youtube"] == 1){echo '<i class="fab fa-youtube"></i>';} ?>
              <?php if($row["document"] == 1){echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp<i class="fas fa-file-signature"></i>';} ?>
            </div>
            <div class="tag">
              <ul class="nospace meta">
                <!--<li><i class="fa fa-user"></i> <a href="#">Admin</a></li>-->
                <li><a class="<?php echo $row["cateFilter"];?>-sty" href="#"><i class="fa fa-tag"></i> <?php echo $row["cateName"]; ?></a></li>
              </ul>
            </div><!--
            <div style="color:black; margin:5px 20px">
              <i class="fas fa-quote-left"></i>As the Future will catches you! เมื่ออนาคตไล่ล่าคุณแนวโน้มการปฏิรูปการศึกษาไทยในศตวรรษที่ 21 
              ที่ต้องก้าวตามให้ทันพัฒนาการของเทคโนโลยี 3 อย่างที่กำลังครอบงำโลกใบนี้ และสิ่งที่น่าตกใจยิ่งไปกว่านั้น คือ เทคโนโลยีเหล่านี้สามารถส่งพลังมหาศาลต่อ
              การเปลี่ยนแปลงวิถีชีวิตของคนเราและลูกหลานเราอย่างคาดไม่ถึง หากวงการศึกษาไทยยังไม่ตื่นตัว ยากจะคาดเดาถึงผลกระทบที่จะตามมา
            </div>-->
          </a>
        </article>
      </div>
<?php 
    }
?>
  </div>
</div>
<!-- End Content 01 - คลังสื่อนวัตกรรม -->
<!-- ################################################################################################ -->
<!-- Start Pagination -->
<?php
switch($_POST['filter'])
{
  case 'การพัฒนาคุณลักษณะอันพึงประสงค์':
    $sql = "SELECT COUNT(ID) AS total FROM innovation WHERE cateFilter LIKE 'devatt' ";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    $total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results
  break;
  case 'หลักสูตรและวิธีการสอน':
    $sql = "SELECT COUNT(ID) AS total FROM innovation WHERE cateFilter LIKE 'coutea' ";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    $total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results
  case 'จิตวิทยาและการแนะแนว':
    $sql = "SELECT COUNT(ID) AS total FROM innovation WHERE cateFilter LIKE 'psygui' ";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    $total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results
  break;
  case 'เทคโนโลยีการศึกษา':
    $sql = "SELECT COUNT(ID) AS total FROM innovation WHERE cateFilter LIKE 'tecedu' ";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    $total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results
  break;
  default:
    $sql = "SELECT COUNT(ID) AS total FROM innovation";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    $total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results
}
?>
            <div class="wrapper row3 coloured" style="background-color:rgb(226,255,224);">
              <main class="hoc container clear">
                <div class="content">
                  <nav class="pagination">
                    <ul>
<?php
                        for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
                          if ($i==$page)
                          {
                            echo "<li class='current'>";
                          }else{
                            echo "<li>";
                          }
                          echo "<a href='คลังสื่อนวัตกรรมรวมlatest.php?page=".$i."'";
                          if ($i==$page)  echo " class='curPage'";
                          echo ">".$i."</a> ";
                        }; 
?>
                      </li>
                    </ul>
                  </nav>
                </div>
              </main>
            </div>
<!-- End Pagination -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>
<script src="js/custom.js"></script>
<script src="js/filter.js"></script>


<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>