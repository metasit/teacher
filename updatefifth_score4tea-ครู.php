

<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");

	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$fifth_score4tea_code = $_GET['fifth_score4tea_code'];
		/*
			code B = จำนวน นร. ที่ครูดูแล เพื่อผ่านโครงการครูดีชั้นที่ 5
			code C = เอกสารยืนยัน นร. ที่ดูแล
		*/
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if($fifth_score4tea_code == 'C') {
			$name = $_SESSION['firstname'].' '.$_SESSION['lastname'];
			$target_dir = 'images/fifth_score_doc/ครู/'.date('Y-m-d').'/'; // Set target_directory

			if(!is_dir($target_dir)) { // if there's not folder in target_directory
				mkdir($target_dir); // Create folder name is today_date
			}

			$target_file = $target_dir.$name.basename($_FILES["fifth_score4tea_data"]["name"]); // Save image in the target folder
			move_uploaded_file($_FILES["fifth_score4tea_data"]["tmp_name"], $target_file);

			$fifth_score4tea_data = $target_file;
		}else{
			$fifth_score4tea_data = $_POST['fifth_score4tea_data'];
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		if(substr($_SESSION['occup_code'], 0, 4) == 'OcAB' && $fifth_score4tea_data < 5) { // กรณีที่เป็นผู้บริหารสถานศึกษา ให้เช็คก่อนว่า อัพเดทมาอย่างน้อย 5 คนหรือไม่
			echo '<script>';
				echo "alert('เนื่องจากท่านเป็นผู้บริหารสถานศึกษา ต้องมีนักเรียนที่ดูแลอย่างน้อย 5 คนค่ะ');";
				echo "window.location.replace('โครงการครูดีของแผ่นดินชั้นที่5-lobby.php')";
			echo '</script>';
		}else{

			$fifth_score4tea_date = date('Y-m-d h:i:s');
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			$sqlfifth_score4tea = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='$fifth_score4tea_code' ";
			$reFS4T = mysqli_query($con, $sqlfifth_score4tea);
			$rowFS4T = mysqli_fetch_array($reFS4T);
			$countrow = mysqli_num_rows($reFS4T);

			if($countrow != 0) {
				$sql = "UPDATE `fifth_score4tea` SET fifth_score4tea_data='$fifth_score4tea_data', fifth_score4tea_date='$fifth_score4tea_date' WHERE ID='$ID' AND fifth_score4tea_code='$fifth_score4tea_code' ";
				$re = $con->query($sql) or die($con->error); //Check error
			}else{
				$sql = "INSERT INTO `fifth_score4tea` (ID, fifth_score4tea_code, fifth_score4tea_data) VALUES ('$ID', '$fifth_score4tea_code', '$fifth_score4tea_data') ";
				$re = $con->query($sql) or die($con->error); //Check error
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* Log User Action */
			if($fifth_score4tea_code == 'B') {
				$sqllog="INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', 'อัพเดทจำนวน นร.', 'ครู,ชั้น5,$fifth_score4tea_data') ";
				$relog = $con->query($sqllog) or die($con->error); //Check error
			}elseif($fifth_score4tea_code == 'C') {
				$sqllog="INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', 'แนบเอกสาร', 'ครู,ชั้น5,ยืนยันจำนวน นร. ที่ดูแล,$fifth_score4tea_data') ";
				$relog = $con->query($sqllog) or die($con->error); //Check error
			}

			header('location: โครงการครูดีของแผ่นดินชั้นที่5-lobby.php');

		}


	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}

?>