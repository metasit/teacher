<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$now_date = date("d-m-Y H:i:s");

	$ID = $_SESSION['ID']; // teacher ID

	/* Set all value */
	$winScroll = $_POST['winScroll'];
	$basic_score4stu_id = $_POST['basic_score4stu_id'];
	$reject_reason = $_POST['reject_reason'];

	$basic_score4stu_check_status = 'ปฏิเสธ,'.$reject_reason.','.$now_date;

	$sql = "UPDATE `basic_score4stu` SET `basic_score4stu_check_status`='$basic_score4stu_check_status' WHERE `basic_score4stu_id`='$basic_score4stu_id' ";
	$res= $con->query($sql) or die($con->error); //Check error




	/* Log User Action */
	$scorelog_task = 'ตรวจความดีเด็กพื้นฐาน,'.$_POST['basic_score4stu_system_id'].$_POST['basic_score4stu_file_no'];
	$scorelog_detail = 'ครู,ชั้น5,'.$basic_score4stu_check_status;
	$scorelog_total = $_POST['ID_stu']; // ID of student who do Good
	$scorelog_date = date("Y-m-d H:i:s");

	$sqllog="INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail, scorelog_total, scorelog_date) 
	VALUES ('$ID', '$scorelog_task', '$scorelog_detail', '$scorelog_total', '$scorelog_date') ";
	$relog = $con->query($sqllog) or die($con->error); //Check error


	header('location: รายละเอียดนักเรียนทำความดี-ครู.php?winScroll='.$winScroll.'&ID_stu='.$_POST['ID_stu'].'&system_id='.$_POST['basic_score4stu_system_id']);
?>