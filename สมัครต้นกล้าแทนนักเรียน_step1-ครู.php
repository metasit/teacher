<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if($_GET['CFP'] == 'สมัครต้นกล้าแทนนักเรียน_step2-ครู') {
			/* Set prename */
			$prename_stu = $_GET['prename_stu'];
			/* Set firstname */
			$firstname_stu = $_GET['firstname_stu'];
			/* Set lastname */
			$lastname_stu = $_GET['lastname_stu'];
			/* Set birthdate */
			$birthdate_stu = $_GET['birthdate_stu'];
			/* Set special child btn */
			$spe_child_btn = $_GET['spe_child_btn'];
			/* Set pomain_id */
			$pomain_id = $_GET['pomain_id'];
		}

	}else{

		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
		
	}

?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
<!--===============================================================================================-->

	<style>
		.spe_child_btn_sty *{
			font-family: RSUText;
			color: black;
			font-size: 28px;
			display: inline-block;
		}
		.spe_child_btn_sty .input_sty {
			width: 32%;
			text-align: right;
			margin-right: 3px;
		}
		.spe_child_btn_sty label {
			width: 66%;
			text-align: left;
		}
	</style>
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="ระบบติดตามสถานะนักเรียนขั้นพื้นฐาน-ครู.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าระบบติดตามสถานะนักเรียนขั้นพื้นฐาน</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/เด็กดีของแผ่นดิน Logo.jpg" style="width:70%;" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-form-title fs-30 lh-1-1">
					กรุณาระบุ ชื่อ-นามสกุล, อาชีพ, ตำแหน่ง และสังกัด ของนักเรียนที่ต้องการสมัครการส่งงานเป็นระบบต้นกล้าแห่งความดี ของโครงการเด็กดีของแผ่นดินขั้นพื้นฐาน
					<br><br>
					<!-- Start show step -->
					<div class="center">
						<span class="dot BG-green1"></span>
						<span class="line" style="width:25%"></span>
						<span class="dot"></span>
						<span class="line" style="width:25%"></span>
						<span class="dot"></span>
						<span class="line" style="width:25%"></span>
						<span class="dot"></span>
					</div>
					<div style="text-align:left;">
						<p style="color:rgb(94,177,26);">ขั้นตอนที่ 1: ชื่อ, นามสกุล และวันเกิดนักเรียน</p>
					</div>
					<!-- End show step -->
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form" action="สมัครต้นกล้าแทนนักเรียน_step2-ครู.php" method="POST">
					<select name="prename" class="form-control" style="margin-bottom:10px; height:40px;" onchange="PickPrename(this.value);" required>
						<option value="" disabled="disabled" selected="selected">เลือกคำหน้านาม</option>
						<option value="D" <?php if($prename_stu == 'D'){echo 'selected="selected" ';} ?> >ด.ช.</option>
						<option value="E" <?php if($prename_stu == 'E'){echo 'selected="selected" ';} ?> >ด.ญ.</option>
					</select>
					<!--
					<div id="pre_name_text" class="wrap-input100 validate-input">
						<input class="input100" type="text" id="pre_name_text_required" name="prename_text" class="form-control" 
						placeholder="<?php if($rowlogin['prename_remark'] != ''){echo $rowlogin['prename_remark'];}else{echo 'โปรดระบุคำหน้านาม"';}?>" max-lenght="30" />
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="firstname" class="form-control" placeholder="ชื่อ (ไม่ต้องใส่คำหน้านามในช่องนี้)" max-lenght="100" value="<?php echo $firstname_stu; ?>" />
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="lastname" class="form-control" placeholder="นามสกุล" max-lenght="100" value="<?php echo $lastname_stu; ?>" />
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="date" name="birthdate" class="form-control" placeholder="วันเกิด"
						min='1950-01-01' max="<?php echo date('Y-m-d'); ?>" value="<?php echo $birthdate_stu; ?>" required />
						<span class="symbol-input100">
							<i class="fas fa-birthday-cake" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<!-- Start ยืนยันข้อมูลเป็นจริง -->
					<div class="spe_child_btn_sty">
						<div class="input_sty">
							<input type="checkbox" id="spe_child_btn" name="spe_child_btn" <?php if($spe_child_btn == 'on'){echo 'checked';} ?> >
						</div>
						<label for="spe_child_btn">กรณีเป็นเด็กพิเศษให้เลือกช่องนี้</label>
					</div>
					<!-- End ยืนยันข้อมูลเป็นจริง -->
					<!-- ################################################################################################ -->
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit" id="register">
							ขั้นตอนต่อไป
						</button>
					</div>

					<input type="hidden" name="pomain_id" value="<?php echo $pomain_id; ?>">
					<input type="hidden" name="CFP" value="สมัครต้นกล้าแทนนักเรียน_step1-ครู">
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

<!--
<script>
	function PickPrename(val) {
		var elePrename = document.getElementById('pre_name');
		var elePrenameText = document.getElementById('pre_name_text');
		var elePrenameTextReq = document.getElementById('pre_name_text_required');
		if(val == 'O') {
			elePrenameText.style.display = 'block';
			elePrenameTextReq.required = true;
		}else{
			elePrenameText.style.display = 'none';
			elePrenameText.value = '';
			elePrenameTextReq.required = false;
		}
	}
</script>
-->

</body>
</html>