<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		$affil_code = $_SESSION['affil_code'];
		$occup_code = $_SESSION['occup_code'];

		if(isset($affil_code) && isset($occup_code)) {

			$sqlbia_cer = "SELECT * FROM `bia_cer` WHERE `bia_cer_affil_code`='$affil_code' ";
			$rebia_cer = mysqli_query($con, $sqlbia_cer);

			if(mysqli_num_rows($rebia_cer) == 0) { // Check ว่าสังกัดที่เข้ามาโรงเรียน ครั้งแรกหรือไม่ ถ้าใช่ ให้เข้ามาหน้านี้

				$sqlaffil_name = "SELECT `affil_name` FROM `login` WHERE `ID`='$ID' ";
				$reaffil_name = mysqli_query($con, $sqlaffil_name);
				$rowaffil_name = mysqli_fetch_array($reaffil_name);

				$affil_name = $rowaffil_name['affil_name'];
				$school_name = 'โรงเรียน'.substr($affil_name, strrpos($affil_name, '*')+1);

			}else{ // สังกัดที่เคยมีการเข้าหน้านี้มาก่อนแล้ว ให้ไปหน้า มอบเกียรติบัตรBIA.php เลย
				header('location: มอบเกียรติบัตรBIA.php');
			}

		}else{
			echo '<script>';
				echo "alert('คุณต้องเคยยืนยันตำแหน่งและสังกัดตัวเองมาก่อนค่ะ');";
				echo "window.history.go(-1)";
			echo '</script>';
		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}

?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>

	<style>
		.BIA_user_comment_sty *{
			font-family: KanitLight;
			color: black;
			font-size: 18px;
			display: inline-block;
			line-height: 30px;
		}
		.BIA_user_comment_sty .question2, .BIA_user_comment_sty .question3 {
			width: 100%;
		}
		.BIA_user_comment_sty .question1 p, .BIA_user_comment_sty .question2 p, .BIA_user_comment_sty .question3 p {
			font-weight: 700;
		}
		.BIA_user_comment_sty .question2 input, .BIA_user_comment_sty .question3 input {
			background-color: rgba(206, 206, 206, 0.5);
			border-radius: 18px;
			padding: 5px 20px;
			width: 100%;
		}
		
		.line_separate_section {
			color: rgb(94,177,26);
			display: block;
			width: 100%;
			height: 1px;
			border: solid;
			border-width: 2px 0 0 0;
			margin: 20px 0;
		}

		.BIA_user_confirm_true_sty *{
			font-family: RSUText;
			color: black;
			font-size: 30px;
			display: inline-block;
			font-weight: 700;
		}
		.BIA_user_confirm_true_sty .input_sty {
			width: 19%;
			text-align: right;
			margin-right: 3px;
		}
		.BIA_user_confirm_true_sty label {
			width: 79%;
			text-align: left;
		}
	</style>

<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/เด็กดีของแผ่นดิน Logo.jpg" style="width:70%;" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" id="Affform" action="addBIA_cer.php" method="POST" enctype="multipart/form-data">
					<!-- ################################################################################################ -->
					<span class="login100-form-title">
						กรอกข้อมูลเพื่อรับเกียรติบัตร Be Internet Awesome
					</span>
					<p class="fs-18 center text-black" style="font-family: KanitLight"><strong>โรงเรียนของท่าน:</strong><?php echo ' '.$school_name; ?></p>
					<!-- ################################################################################################ -->
					<!-- Start User กรอกข้อมูลเพื่อนำไปคำนวณจำนวนการได้รับเกียรติบัตร -->
					<label for="stu_num_all" class="center m-t-10 text-black bold">จำนวนนักเรียนทั้งโรงเรียน</label>
					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="number" name="stu_num_all" class="form-control" placeholder="จำนวนนักเรียนทั้งโรงเรียน" min="1" required/>
						<span class="symbol-input100">
							<i class="fa fa-play" aria-hidden="true"></i>
						</span>
					</div>
					<label for="stu_num_p3+" class="center m-t-10 text-black bold">จำนวนนักเรียนตั้งแต่ประถมศึกษาปีที่ 3 ขึ้นไป</label>
					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="number" name="stu_num_p3+" class="form-control" placeholder="จำนวนนักเรียนตั้งแต่ประถมศึกษาปีที่ 3 ขึ้นไป" min="1" required/>
						<span class="symbol-input100">
							<i class="fa fa-play" aria-hidden="true"></i>
						</span>
					</div>
					<label for="stu_num_BIA" class="center m-t-10 text-black bold">จำนวนนักเรียนที่ใช้หลักสูตร Be Internet Awesome</label>
					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="number" name="stu_num_BIA" class="form-control" placeholder="จำนวนนักเรียนที่ใช้หลักสูตร Be Internet Awesome" min="1" required/>
						<span class="symbol-input100">
							<i class="fa fa-play" aria-hidden="true"></i>
						</span>
					</div>
					<!-- End User กรอกข้อมูลเพื่อนำไปคำนวณจำนวนการได้รับเกียรติบัตร -->
					<!-- ################################################################################################ -->
					<!-- Start ถามความคิดเห็นต่อ BIA -->
					<div class="line_separate_section"></div>

					<div class="BIA_user_comment_sty">

						<div class="question1">
							<p>1. ท่านคิดว่าหลักสูตร Be Internet Awesome มีประโยชน์ต่อผู้เรียนมากน้อยเพียงใด</p>
							<br>
							<input type="radio" id="Strongly Agree" name="BIA_question1" value="5" required>
							<label for="Strongly Agree">มากที่สุด</label>
							<br>
							<input type="radio" id="Agree" name="BIA_question1" value="4" required>
							<label for="Agree">มาก</label>
							<br>
							<input type="radio" id="Neutral" name="BIA_question1" value="3" required>
							<label for="Neutral">ปานกลาง</label>
							<br>
							<input type="radio" id="Disagree" name="BIA_question1" value="2" required>
							<label for="Disagree">น้อย</label>
							<br>
							<input type="radio" id="Strongly Disagree" name="BIA_question1" value="1" required>
							<label for="Strongly Disagree">น้อยที่สุด</label>
						</div>

						<br>

						<div class="question2">
							<p><strong>2. ข้อดีของหลักสูตร Be Internet Awesome คืออะไร</strong></p>
							<br>
							<label for="BIA_question3" class="col-1">ตอบ: </label>
							<input type="text" class="col-10 m-l-5" name="BIA_question2" required>
						</div>

						<br>

						<div class="question3">
							<p><strong>3. สิ่งที่หลักสูตร Be Internet Awesome ควรพัฒนาคืออะไร</strong></p>
							<br>
							<label for="BIA_question3"  class="col-1">ตอบ: </label>
							<input type="text" class="col-10 m-l-5" name="BIA_question3" required>
						</div>
						<!-- ################################################################################################ -->
					</div>
					<!-- End ถามความคิดเห็นต่อ BIA -->
					<!-- ################################################################################################ -->
					<!-- Start ยืนยันข้อมูลเป็นจริง -->
					<div class="BIA_user_confirm_true_sty m-t-40">
						<div class="input_sty">
							<input type="radio" id="confirm_true" name="BIA_confirm_true" required>	
						</div>
						<label for="confirm_true">ขอรับรองว่าข้อมูลดังกล่าวข้างต้นเป็นจริงทุกประการ</label>
					</div>
					<!-- End ยืนยันข้อมูลเป็นจริง -->
					<!-- ################################################################################################ -->
					<div class="container-login100-form-btn m-t-20">
						<button class="login100-form-btn" style="width:30%; margin:0 10px;" type="submit" id="register">ยืนยัน</button>
					</div>
					<!-- ################################################################################################ -->
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>

<!-- JS for multi-sub-dropdown -->
<script src="js/jquery.min.js"></script>
<script src="js/projectRegist4affA.js" type="text/javascript"></script>

</body>
</html>