<?php 
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if(isset($_POST['ID_stu'])) {
			$ID_stu = $_POST['ID_stu'];
		}else{
			$ID_stu = $_GET['ID_stu'];
		}

		$sqlstu = "SELECT * FROM `login` WHERE ID='$ID_stu' ";
		$restu = mysqli_query($con, $sqlstu);
		$rowstu = mysqli_fetch_array($restu);

		$stu_name = $rowstu['firstname'].' '.$rowstu['lastname'];

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>


<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li class="linkst"><a href="#" onclick="return false">แนบหลักฐานประกอบการพิจารณาแทนนักเรียน (<?php echo $stu_name; ?>)</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - แนบหลักฐานประกอบการพิจารณา -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
	<!-- ################################################################################################ -->
	<!-- Start Slip Upload  -->
	<div class="cart-box-main">
    <div class="container">
      <div class="row">
				<!-- ################################################################################################ -->
				<!-- Start Left Content -->
				<div class="col-sm-6 col-lg-6 mb-3">
					<div class="slip-page">
						<div class="title-left">
							<h3>รายละเอียดหลักฐานประกอบการพิจารณา</h3>
						</div>
						<div class="row">
							<div class="col-md-12 mb-3">
								<p>
									<strong>1. เกียรติบัตรหลักสูตร Be Internet Awesome</strong>
									<br>
									เกียรติบัตรนี้ นักเรียนสามารถแนบเกียติบัตรใด เกียรติบัตรหนึ่งที่ได้จากการเรียนรู้ตามหลักสูตร  Be Internet Awesome ซึ่งมาจาก 
									<a href="https://beinternetawesome.withgoogle.com/th_th" style="color:blue">https://beinternetawesome.withgoogle.com/th_th</a> หรือจากเมนู Be Internet Awesome ในเวปนี้
								</p>
								<p class="p-t-20" style="border-top:2px solid white;">
									<strong>2. เรียงความ หรือคลิปวิดีโอ เรื่องการเปลี่ยนแปลงของฉัน เมื่อเข้าร่วมโครงการ</strong>
									<br>
									เพื่อให้เห็นการเปลี่ยนแปลงของนักเรียน ว่าก่อนและหลังเข้าร่วมโครงการ นักเรียนมีการเปลี่ยนแปลงในประเด็นที่นักเรียนพัฒนาอย่างไรบ้าง หากเป็นเรียงความ ต้องมีความยาวไม่น้อยกว่า ครึ่งหน้า A4 แต่ไม่เกิน 1 หน้า A4 
									หากเป็นคลิปวิดีโอ ขอเป็นคลิป ที่มีความยาว ไม่เกิน 3 นาที
								</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End Left Content -->
				<!-- ################################################################################################ -->
				<!-- Start Right Content -->
				<div class="col-sm-6 col-lg-6 mb-3">
					<div class="col-md-12 col-lg-12">
						<div class="input-slip-upload">
							<div class="title-left">
								<h3>หลักฐานประกอบการพิจารณา</h3>
							</div>
							<!-- Start form input -->
							<?php
								$sqlbasic_score4stu2_codeA = "SELECT * FROM `basic_score4stu2` WHERE ID='$ID_stu' AND basic_score4stu2_code='A' ";
								$reA = mysqli_query($con, $sqlbasic_score4stu2_codeA);
								$rowA = mysqli_fetch_array($reA);
							?>
							<form action="uploadOthersแทนนักเรียน-ครู.php?basic_score4stu2_code=A" method="POST" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-12 mb-3">
										<label for="BIA"><strong>เกียรติบัตรหลักสูตร Be Internet Awesome</strong></label>
									</div>
									<div class="col-md-6 mb-3">
										<input type="file" id="BIA" name="basic_score4stu2_data" class="form-control" required />
									</div>
									<div class="col-md-6 mb-3 m-t-8">
										<?php
											if($rowA['basic_score4stu2_data'] != '') { ?>
												<button class="col-md-12 stu-file-btn" onclick="window.location.href='<?php echo $rowA['basic_score4stu2_data']; ?>', '_blank'">ดูไฟล์แนบ</button> <?php
											}else{ ?>
												<button class="col-md-12 BG-silver" style="border:1px solid rgb(100,100,100); border-radius:50px;" onclick="return false">รอแนบไฟล์</button> <?php
											}
										?>
									</div>
									<div class="col-md-12 m-t-12">
										<button type="submit" class="col-md-12 stu-up-btn"
											onmousemove="this.style.cursor='pointer'">
											แนบไฟล์ <?php echo $i; ?> (ถ้าทำการแนบไฟล์ซ่ำ จะทำให้ไฟล์เก่าหาย)
										</button>
									</div>
								</div>

								<input type="hidden" name="ID_stu" value="<?php echo $ID_stu; ?>">
							</form>
							<!-- ################################################################################################ -->
							<?php
								$sqlbasic_score4stu2_codeC = "SELECT * FROM `basic_score4stu2` WHERE ID='$ID_stu' AND basic_score4stu2_code='C' ";
								$reC = mysqli_query($con, $sqlbasic_score4stu2_codeC);
								$rowC = mysqli_fetch_array($reC);
							?>
							<form action="uploadOthersแทนนักเรียน-ครู.php?basic_score4stu2_code=C" method="POST" enctype="multipart/form-data">
								<div class="row m-t-20 p-t-20" style="border-top:2px solid white;">
									<div class="col-md-12 mb-3">
										<label for="change"><strong>เรียงความ/คลิปวีดีโอ เรื่อง การเปลี่ยนแปลงของฉันเมื่อเข้าร่วมโครงการ</strong></label>
									</div>
									<div class="col-md-6 mb-3">
										<input type="file" id="change" name="basic_score4stu2_data" class="form-control" required />
									</div>
									<div class="col-md-6 mb-3 m-t-8">
										<?php
											if($rowC['basic_score4stu2_data'] != '') { ?>
												<button class="col-md-12 stu-file-btn" onclick="window.location.href='<?php echo $rowC['basic_score4stu2_data']; ?>'">ดูไฟล์แนบ</button> <?php
											}else{ ?>
												<button class="col-md-12 BG-silver" style="border:1px solid rgb(100,100,100); border-radius:50px;" onclick="return false">รอแนบไฟล์</button> <?php
											}
										?>
									</div>
									<div class="col-md-12 m-t-12">
										<button type="submit" class="col-md-12 stu-up-btn"
											onmousemove="this.style.cursor='pointer'">
											แนบไฟล์ <?php echo $i; ?> (ถ้าทำการแนบไฟล์ซ่ำ จะทำให้ไฟล์เก่าหาย)
										</button>
									</div>
								</div>

								<input type="hidden" name="ID_stu" value="<?php echo $ID_stu; ?>">
							</form>
							<!-- End form input -->
						</div>
					</div>
      	</div>
				<!-- End Right Content -->
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
  <!-- End Slip Upload -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - แนบหลักฐานประกอบการพิจารณา -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<script src="js/pickBillType.js"></script>

</body>
</html>