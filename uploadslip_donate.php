<?php	
	session_start();
	require_once("condb.php");
	date_default_timezone_set("Asia/Bangkok");


	if(isset($_SESSION['ID'])) { // In the case of Login User

		/* Set values for insert into donate table */
		// Set ID
		$ID = $_SESSION['ID'];
		// Set donate_topic
		$donate_topic = $_POST['donate_topic'];
		// Set donate_status
		$donate_status = 'รอตรวจSlip';
		// Set donate_date
		$date_donate = $_POST['date_donate']; 
		$time_donate = $_POST['time_donate'];
		$donate_date = $date_donate.' '.$time_donate;
		// Set bill_tax_kind
		$bill_tax_kind = $_POST['bill_tax_kind'];
		// Set bill_tax_id
		if($_POST['bill_tax_id_Personal'] != '') {

			$bill_tax_id = $_POST['bill_tax_id_Personal'];

		}elseif($_POST['bill_tax_id_Corporation'] != '') {

			$bill_tax_id = $_POST['bill_tax_id_Corporation'];

		}elseif($_POST['bill_tax_id_Government'] != '') {

			$bill_tax_id = $_POST['bill_tax_id_Government'];

		}else{

			$bill_tax_id = NULL;

		}

		$target_dir = 'images/slips/donate/'.$date_donate.'/'; // Set target_directory

		if(!is_dir($target_dir)) { // if there's not folder in target_directory
			mkdir($target_dir); // Create folder name is today_date
		}

		$target_file = $target_dir.basename($_FILES["slip_image"]["name"]); // Save image in the target folder

		if (move_uploaded_file($_FILES["slip_image"]["tmp_name"], $target_file)) {

			/* Save slip location in donate table */
			$sql = "INSERT INTO `donate` (ID, donate_topic, donate_status, donate_slip_image, donate_date, bill_tax_kind, bill_tax_id) 
			VALUES ('$ID', '$donate_topic', '$donate_status', '$target_file', '$donate_date', '$bill_tax_kind', '$bill_tax_id') ";
			$res= $con->query($sql) or die($con->error);

			echo '<script>';
				echo "alert(\"ทำรายการแจ้งการบริจาคเรียบร้อยแล้วค่ะ\");";
				echo "window.location.replace('บริจาคสนับสนุนโครงการต่างๆ.php')";
			echo '</script>';

		}else{

			echo "<script>";
				echo "alert(\"โปรดติดต่อผู้พัฒนาระบบของมูลนิธิครูดีของแผ่นดิน<br>เพื่อดำเนินการแก้ไข<br>ขอบคุณครับ\");";
				echo "window.location.replace('บริจาคสนับสนุนโครงการต่างๆ.php')";
			echo "</script>";

		}






	}else{ // In the case of Non-Login User

		// Set ID
		$ID = 'GUEST';
		// Set donate_topic
		$donate_topic = $_POST['donate_topic'];
		// Set donate_status
		$donate_status = 'รอตรวจSlip';
		// Set donate_firstname
		$donate_firstname = trim($_POST['donate_firstname']);
		// Set donate_lastname
		$donate_lastname = trim($_POST['donate_lastname']);
		// Set donate_address
		$donate_address = trim($_POST['donate_address']);
		// Set donate_date
		$date_donate = $_POST['date_donate']; 
		$time_donate = $_POST['time_donate'];
		$donate_date = $date_donate.' '.$time_donate;
		// Set bill_tax_kind
		$bill_tax_kind = $_POST['bill_tax_kind'];
		// Set bill_tax_id
		if($_POST['bill_tax_id_Personal'] != '') {

			$bill_tax_id = $_POST['bill_tax_id_Personal'];

		}elseif($_POST['bill_tax_id_Corporation'] != '') {

			$bill_tax_id = $_POST['bill_tax_id_Corporation'];

		}elseif($_POST['bill_tax_id_Government'] != '') {

			$bill_tax_id = $_POST['bill_tax_id_Government'];

		}else{

			$bill_tax_id = NULL;

		}

		$target_dir = 'images/slips/donate/'.$date_donate.'/'; // Set target_directory

		if(!is_dir($target_dir)) { // if there's not folder in target_directory
			mkdir($target_dir); // Create folder name is today_date
		}

		$target_file = $target_dir.basename($_FILES["slip_image"]["name"]); // Save image in the target folder

		if (move_uploaded_file($_FILES["slip_image"]["tmp_name"], $target_file)) {

			/* Save slip location in donate table */
			$sql = "INSERT INTO `donate` (ID, donate_topic, donate_status, donate_slip_image, donate_firstname, donate_lastname, donate_address, donate_date, bill_tax_kind, bill_tax_id) 
			VALUES ('$ID', '$donate_topic', '$donate_status', '$target_file', '$donate_firstname', '$donate_lastname', '$donate_address', '$donate_date', '$bill_tax_kind', '$bill_tax_id') ";
			$res= $con->query($sql) or die($con->error);

			echo '<script>';
				echo "alert(\"ทำรายการแจ้งการบริจาคเรียบร้อยแล้วค่ะ\");";
				echo "window.location.replace('บริจาคสนับสนุนโครงการต่างๆ.php')";
			echo '</script>';

		}else{

			echo "<script>";
				echo "alert(\"โปรดติดต่อผู้พัฒนาระบบของมูลนิธิครูดีของแผ่นดิน<br>เพื่อดำเนินการแก้ไข<br>ขอบคุณครับ\");";
				echo "window.location.replace('บริจาคสนับสนุนโครงการต่างๆ.php')";
			echo "</script>";

		}

	}

?>