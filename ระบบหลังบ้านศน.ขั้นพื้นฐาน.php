<?php 
	session_start();
	$ID = $_SESSION['ID'];
	
	if(isset($ID)) {

		if(strpos($_SESSION['permis_system'], '*all*') !== false || strpos($_SESSION['permis_system'], '*B00*') !== false || strpos($_SESSION['permis_system'], '*B01*') !== false) {
			require_once('condb.php');
			$email = $_SESSION['email'];
		}else{
			header("Location: javascript:history.go(-1);");
		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['win_scroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="index-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ระบบหลังบ้านศน.พื้นฐาน</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row" style="margin:0 20px; overflow-x:auto;">
			<div class="col-lg-12">
				<div class="table-main table-responsive" style="margin-bottom:200px">
					<!-- ################################################################################################ -->
					<table class="table" style="background-color:rgb(240,240,240);">
						<thead>
							<tr>
								<th>ลำดับ</th>
								<th>ชื่อ</th>
								<th>วันที่ทำแบบประเมิน</th>
								<th>เวลา</th>
								<th>สถานะ</th>
								<th>หนังสือรับรอง</th>
								<th>อัพเดท</th>
								<th>Print</th>
								<th>หมายเหตุ</th>
							</tr>
						</thead>
						<tbody class="disable-margin">
						<!-- Filter -->
						<form method="POST">
							<input type="submit" class="btnfilter1" name="filter" value="แสดงทั้งหมด" />
							<input type="submit" class="btnfilter1" name="filter" value="Approve แล้ว" />
							<input type="submit" class="btnfilter1" name="filter" value="ปฏิเสธหนังสือรับรอง" />
							<input type="submit" class="btnfilter1" name="filter" value="กำลังตรวจสอบ" />
							<input type="submit" class="btnfilter1" name="filter" value="รอแนบเอกสาร" />
						</form>
						<!-- Filter -->
							<?php
							/* Set number product per page */
							$results_per_page = 100;
							if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
							$start_from = ($page-1) * $results_per_page;

							/* Call login table data from mySQL */
							switch($_POST['filter'])
							{
								case 'Approve แล้ว':
									$sql = "SELECT * FROM `login` WHERE (occup_code LIKE 'OcAD%' AND ID!='$ID' AND basic_score_status='Approve แล้ว')
									 OR (occup_code LIKE 'OcAD%' AND ID!='$ID' AND basic_score_status='Approve แล้ว/ยืนยันแล้ว')
									 ORDER BY basic_score_date DESC LIMIT $start_from, $results_per_page";
								break;
								case 'ปฏิเสธหนังสือรับรอง':
									$sql = "SELECT * FROM `login` WHERE occup_code LIKE 'OcAD%' AND ID!='$ID' AND basic_score_status LIKE 'ปฏิเสธหนังสือรับรอง' ORDER BY basic_score_date DESC LIMIT $start_from, $results_per_page";
								break;
								case 'กำลังตรวจสอบ':
									$sql = "SELECT * FROM `login` WHERE occup_code LIKE 'OcAD%' AND ID!='$ID' AND basic_score_status LIKE 'กำลังตรวจสอบ' ORDER BY basic_score_date DESC LIMIT $start_from, $results_per_page";
								break;
								case 'รอแนบเอกสาร':
									$sql = "SELECT * FROM `login` WHERE occup_code LIKE 'OcAD%' AND ID!='$ID' AND basic_score_status LIKE 'รอแนบเอกสาร' ORDER BY basic_score_date DESC LIMIT $start_from, $results_per_page";
								break;
								default:
									$sql = "SELECT * FROM `login` WHERE occup_code LIKE 'OcAD%' AND basic_score_total IS NOT NULL AND ID!='$ID' ORDER BY basic_score_date DESC LIMIT $start_from, $results_per_page";
							}
							$re = mysqli_query($con,$sql);

							$maxrow = $start_from+1; // Get number of row
							while($row = $re->fetch_assoc()) {
							?>
								<tr>
									<!-- No. -->
									<td class="price-pr">
										<p><?php echo $maxrow; ?></p>
									</td>
									<!-- Name -->
									<?php if(strpos($row['level'], 'admin') !== false) { ?>
													<td class="name-pr lh-1-0" style="background-color:rgb(228,0,0);">
														<p style="color:white;"><?php echo $row['firstname'].'<br>'.$row["lastname"]; ?></p>
													</td>
									<?php }elseif($row['level'] == 'memberGeneral') { ?>
													<td class="name-pr lh-1-0" style="background-color:white;">
														<p><?php echo $row['firstname'].'<br>'.$row["lastname"]; ?></p>
													</td>
									<?php }elseif($row['level'] == 'memberSilver') { ?>
													<td class="name-pr lh-1-0" style="background-color:rgb(169,169,169);">
														<p style="color:white;"><?php echo $row['firstname'].'<br>'.$row["lastname"]; ?></p>
													</td>
									<?php }elseif($row['level'] == 'memberGold') { ?>
													<td class="name-pr lh-1-0" style="background-color:rgb(180,147,31);">
														<p style="color:white;"><?php echo $row['firstname'].'<br>'.$row["lastname"]; ?></p>
													</td>
									<?php }elseif($row['level'] == 'memberDiamond') { ?>
													<td class="name-pr lh-1-0" style="background-color:rgb(52,52,53);">
														<p style="color:white;"><?php echo $row['firstname'].'<br>'.$row["lastname"]; ?></p>
													</td>
									<?php } ?>
									<!-- basic_score Date -->
									<td class="price-pr">
										<p><?php echo date("d-m-Y", strtotime($row['basic_score_date'])); ?></p>
									</td>
									<!-- basic_score Time -->
									<td class="price-pr">
										<p><?php echo date("H:i", strtotime($row['basic_score_date'])); ?></p>
									</td>
									<!-- Status -->
									<td class="name-pr">
										<?php
										
											if(strpos($row['level'], 'admin') !== false || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') { // For high level member
												
												if($row['basic_score_status'] == 'ถึงปลายทางแล้ว') { ?>
													
													<p><i class="fas fa-check-circle"></i><br>ถึงปลายทางแล้ว</p> <?php

												}else{ ?>

													<p><?php echo $row['basic_score_status']; ?></p> <?php
													
												}

											}else{ // For low level member

												if($row['basic_score_status'] == 'Approve แล้ว' || $row['basic_score_status'] == 'Approve แล้ว/ยืนยันแล้ว') { ?>

													<p><i class="fas fa-check-circle"></i><br><?php echo $row['basic_score_status']; ?></p> <?php
												
												}else{ ?>

													<p><?php echo $row['basic_score_status']; ?></p> <?php
													
												}
											}
										?>
									</td>
									<!-- basic_score_doc Upload -->
									<td class="slip-upload-btn">
										<?php if($row['basic_score_status'] == 'รอแนบเอกสาร' || $row['basic_score_status'] == 'ปฏิเสธหนังสือรับรอง') { ?>
														<a href="#" style="background-color:transparent; color:black; font-weight:700; border: none;" onclick="return false">รอแนบเอกสาร</a>
										<?php }elseif($row['basic_score_status'] == 'คะแนนต่ำกว่าเกณฑ์'){ ?>
														<a href="#" style="background-color:transparent; color:black; font-weight:700; border: none;" onclick="return false">รอทำแบบประเมินใหม่</a>
										<?php }else{ ?>
														<a href="<?php echo $row['basic_score_doc'];?>" target="_blank">ดูหนังสือรับรอง</a>
										<?php	} ?>
									</td>
									<!-- Update Status Button -->
									<?php
										/* Let set DateTime format before check more than 30 days can do basic_score again */
										$next_basic_score_date = new DateTime($row['basic_score_date']);
										date_add($next_basic_score_date,date_interval_create_from_date_string('30 days')); // Set Date for user can do basic_score next time
										$today = new DateTime(date("Y-m-d"));
										$remain_basic_score_date = date_diff($today,$next_basic_score_date);
									?>
									<td>
										<div class="up-stat-sty">
											<button class="up-stat-btn"><i class="fas fa-caret-down"></i></button>
											<div class="dropdown">
												<?php if($row['basic_score_status'] == 'กำลังตรวจสอบ') { ?>
																<a onclick="getWinScroll_changeStatusBtn_updatebasic_score_status('ปฏิเสธหนังสือรับรอง','<?php echo $row['ID'];?>','<?php echo $row['basic_score_doc'];?>')">ปฏิเสธหนังสือรับรอง</a>
																<a onclick="getWinScroll_changeStatusBtn_updatebasic_score_status('Approve แล้ว','<?php echo $row['ID'];?>','<?php echo $row['basic_score_doc'];?>')">Approve</a>
													<?php if(strpos($row['level'], 'admin') !== false || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') { // For high level member ?>
																<!--
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ส่งเกียรติบัตรพิเศษแล้ว</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ถึงปลายทางแล้ว</a>
																-->
													<?php	}

															}elseif($row['basic_score_status'] == 'ปฏิเสธหนังสือรับรอง'){ ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ปฏิเสธหนังสือรับรอง</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">Approve</a>
													<?php if(strpos($row['level'], 'admin') !== false || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') { // For high level member ?>
																<!--
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ส่งเกียรติบัตรพิเศษแล้ว</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ถึงปลายทางแล้ว</a>
																-->
													<?php	}

															}elseif($row['basic_score_status'] == 'Approve แล้ว' || $row['basic_score_status'] == 'Approve แล้ว/ยืนยันแล้ว'){ ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ปฏิเสธหนังสือรับรอง</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">Approve</a>
													<?php if(strpos($row['level'], 'admin') !== false || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') { // For high level member ?>
																<!--
																<a onclick="getWinScroll_changeStatusBtn_updatebasic_score_status('ส่งเกียรติบัตรพิเศษแล้ว','<?php echo $row['ID'];?>','<?php echo $row['basic_score_doc'];?>')">ส่งเกียรติบัตรพิเศษแล้ว</a>
																<a onclick="getWinScroll_changeStatusBtn_updatebasic_score_status('ถึงปลายทางแล้ว','<?php echo $row['ID'];?>','<?php echo $row['basic_score_doc'];?>')">ถึงปลายทางแล้ว</a>
																-->
													<?php	}

													}elseif($row['basic_score_status'] == 'Approve แล้ว'){ ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ปฏิเสธหนังสือรับรอง</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">Approve</a>
													<?php if(strpos($row['level'], 'admin') !== false || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') { // For high level member ?>
																<!--
																<a onclick="getWinScroll_changeStatusBtn_updatebasic_score_status('ส่งเกียรติบัตรพิเศษแล้ว','<?php echo $row['ID'];?>','<?php echo $row['basic_score_doc'];?>')">ส่งเกียรติบัตรพิเศษแล้ว</a>
																<a onclick="getWinScroll_changeStatusBtn_updatebasic_score_status('ถึงปลายทางแล้ว','<?php echo $row['ID'];?>','<?php echo $row['basic_score_doc'];?>')">ถึงปลายทางแล้ว</a>
																-->
													<?php	}

															}elseif($row['basic_score_status'] == 'คะแนนต่ำกว่าเกณฑ์') { ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">รอคุณครูทำแบบประเมิน เหลือ <?php echo $remain_basic_score_date->format("%a"); ?> วัน</a>

												<?php	}elseif($row['basic_score_status'] == 'ส่งเกียรติบัตรพิเศษแล้ว'){ ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ปฏิเสธหนังสือรับรอง</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">Approve</a>
													<?php if(strpos($row['level'], 'admin') !== false || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') { // For high level member ?>
																<!--
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ส่งเกียรติบัตรพิเศษแล้ว</a>
																<a onclick="getWinScroll_changeStatusBtn_updatebasic_score_status('ถึงปลายทางแล้ว','<?php echo $row['ID'];?>','<?php echo $row['basic_score_doc'];?>')">ถึงปลายทางแล้ว</a>
																-->
													<?php	}
												
															}elseif($row['basic_score_status'] == 'ถึงปลายทางแล้ว'){ ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ปฏิเสธหนังสือรับรอง</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">Approve</a>
													<?php if(strpos($row['level'], 'admin') !== false || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') { // For high level member ?>
																<!--
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ส่งเกียรติบัตรพิเศษแล้ว</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ถึงปลายทางแล้ว</a>
																-->
													<?php	}

															}else{ ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">รอแนบเอกสาร</a>
												<?php	} ?>
											</div>
										</div>
									</td>
									<!-- Printing -->
									<td class="print-btn">
										<?php if(strpos($row['level'], 'admin') !== false || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') {
														if($row['basic_score_status'] == 'Approve แล้ว' || $row['basic_score_status'] == 'Approve แล้ว/ยืนยันแล้ว' || $row['basic_score_status'] == 'ส่งเกียรติบัตรพิเศษแล้ว' || $row['basic_score_status'] == 'ถึงปลายทางแล้ว') { ?>
															<a href="spe cer-ครู.php?name=<?php echo $_SESSION['firstname'].' '.$_SESSION['lastname'];?>&basic_score_code"
															target="_blank" class="tooltip4"><i class="fas fa-print"><span class="tooltiptext4">พิมพ์เกียรติบัตรพิเศษ</span></i></a>
											<?php	}else{ ?>
															<a href="#" onclick="return false" style="background-color:rgb(206,206,206); pointer-events:none;"><i class="fas fa-print"></i></a>
											<?php }
													}else{ ?>
														<a href="#" onclick="return false" style="background-color:rgb(206,206,206); pointer-events:none;"><i class="fas fa-print"></i></a>
											<?php	} ?>
									</td>
									<!-- Remark -->
									<form action="addbasic_score_remark.php" method="POST">
										<td class="order_remark-sty">
											<textarea rows="1" cols="15" name="basic_score_remark"><?php echo $row['basic_score_remark']; ?></textarea>
											<div class="order-detail-btn">
												<input type="hidden" name="ID" value=<?php echo $row['ID']; ?>>
												<input class="order_remark-submit" type="submit" value="บันทึก">
											</div>
										</td>
									</form>
								</tr>
								<?php $maxrow++; }  ?>
							</tbody>
						</table>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 -->
<?php
switch($_POST['filter'])
{
  case 'Approve แล้ว':
    $sql = "SELECT COUNT(ID) AS total FROM `login` WHERE SELECT * FROM `login` WHERE (occup_code LIKE 'OcAD%' AND ID!='$ID' AND basic_score_status='Approve แล้ว')
		OR (occup_code LIKE 'OcAD%' AND ID!='$ID' AND basic_score_status='Approve แล้ว/ยืนยันแล้ว') ";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    $total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results
  break;
  case 'ปฏิเสธหนังสือรับรอง':
    $sql = "SELECT COUNT(ID) AS total FROM `login` WHERE occup_code LIKE 'OcAD%' AND ID!='$ID' AND basic_score_status LIKE 'ปฏิเสธหนังสือรับรอง' ";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    $total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results
  case 'กำลังตรวจสอบ':
    $sql = "SELECT COUNT(ID) AS total FROM `login` WHERE occup_code LIKE 'OcAD%' AND ID!='$ID' AND basic_score_status LIKE 'กำลังตรวจสอบ' ";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    $total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results
  break;
  case 'รอแนบเอกสาร':
    $sql = "SELECT COUNT(ID) AS total FROM `login` WHERE occup_code LIKE 'OcAD%' AND ID!='$ID' AND basic_score_status LIKE 'รอแนบเอกสาร' ";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    $total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results
  break;
  default:
    $sql = "SELECT COUNT(ID) AS total FROM `login` WHERE occup_code LIKE 'OcAD%' AND basic_score_total IS NOT NULL AND ID!='$ID' ";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    $total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results
}
?>
            <div class="wrapper row3 coloured" style="background-color:rgb(226,255,224);">
              <main class="hoc container clear">
                <div class="content">
                  <nav class="pagination">
                    <ul>
<?php
                        for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
                          if ($i==$page)
                          {
                            echo "<li class='current'>";
                          }else{
                            echo "<li>";
                          }
                          echo "<a href='ระบบหลังบ้านศน.ขั้นพื้นฐาน.php?page=".$i."'";
                          if ($i==$page)  echo " class='curPage'";
                          echo ">".$i."</a> ";
                        }; 
?>
                      </li>
                    </ul>
                  </nav>
                </div>
              </main>
            </div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<!-- JS for getWinScroll -->
<script src="js/getWinScroll_changeStatusBtn.js"></script>

</body>
</html>