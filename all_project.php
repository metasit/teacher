<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];

    $sql_list  ="SELECT * FROM join_project order by order_data asc   ";
    $list = $con->query($sql_list) ;
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<style>

        @font-face {
          font-family: 'korbauMed'; /*a name to be used later*/
          src:  url('content-setting/backend/font/korbau.ttf')  format('truetype') 
        }
        @font-face {
          font-family: 'Niramit'; /*a name to be used later*/
          src:  url('content-setting/backend/font/TH Niramit AS.ttf')  format('truetype') 
        }
        @font-face {
          font-family: 'CSChatThai'; /*a name to be used later*/
          src:  url('content-setting/backend/font/CSChatThai.ttf')  format('truetype') 
        }
        @font-face {
          font-family: 'Emmal'; /*a name to be used later*/
          src:  url('content-setting/backend/font/EmmaliDEMO-Thin.otf')  format('truetype') 
        }

p.big {
line-height: 30px;
}
::-webkit-scrollbar {
    width: 10px;
}
 
::-webkit-scrollbar-track {
    background-color: #ebebeb;
    -webkit-border-radius: 10px;
    border-radius: 10px;
}

::-webkit-scrollbar-thumb {
    -webkit-border-radius: 10px;
    border-radius: 10px;
    background: #6d6d6d; 
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="ร่วมโครงการ-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom4Joinus.php');
 ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> ร่วมโครงการ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - ร่วมโครงการ ครู -->
<?php 
$number = 0;
while($row = $list->fetch_assoc()) {
    $id = $row['id_join_project'];
    $file_background_summary = $row['file_background_summary'];
    $title_menu =  $row['title_menu'];
     if($number % 2 == 0) {
    ?>
<div  <?php  echo "style='background-image: url(content-setting/backend/join_project/bg-background/$file_background_summary);'" ?> class="bgJoin bgSizeTeacher overlay">
  <div class="hoc container3">
    <div class=""  style="display:flex; justify-content: center; ">
      <span class="font-weight-bold" style="font-size:60px;">
        <?php  echo $title_menu;   ?>
     </span>
    </div>
    <h5 class="center">
        <?php  
            // echo $row['title_menu'];
        ?>
    </h5>

      <div class="one_half right" style="
      font-family:ChulaCharasNew; 
      letter-spacing:3px; 
      line-height:35px; 
      padding-right:25px; 
      margin-bottom:50px;
      max-height: 400px;
      min-height:400px;
      overflow-y: scroll;;
    ">
        <h9>
          <?php echo $row['detail_summary'] ?>
        </h9>

        <div style="         
          bottom: 28%;
          position: absolute;
          display: inline;
          left: 30.5%;
          font-size:20px;
        " class="textlink4 font-weight-bold">
            <a href="<?php  echo "project_db.php?id=".$id; ?>">ดูรายละเอียดโครงการเพิ่มเติม</a>
       </div>
      </div>
          <?php  if($id==29) { ?>
          <!-- <div class="one_first" style="margin-top:25px;">
            <div class="btnJoin" >
              <a href="https://beinternetawesome.withgoogle.com/th_th">
                <h7 style="color: white!important;">เข้าสู่เว็ปไซต์ Be Internet Awesome by Google</h7>
              </a>
            </div>
          </div> -->
      <?php  } ?>
  </div>
</div>
<?php 
     } 
     else {

?>

<div 
<?php  echo "style='background-image: url(content-setting/backend/join_project/bg-background/$file_background_summary);'" ?>
class="bgJoin bgSizeStudent overlay">
 <div class="hoc container3"> 
    <div   style="display:flex; justify-content: center; ">
      <span  class="font-weight-bold" style="font-size:60px;">
        <?php 
          echo $row['title_menu'];
        ?>
      </span>
    </div>
    <div class="one_half first">
      <br>
    </div>
    <div class="one_half left" 
    style="
    font-family:ChulaCharasNew; 
    letter-spacing:3px; line-height:35px; 
    padding-left:25px; 
    margin-bottom:50px;
    max-height: 400px;
    min-height:400px;
    overflow-y: scroll;
    direction:rtl; 
    ">
      <h9><?php 
    //   echo $row['title_project'];
      ?>
        <br><br>
        <?php 
          echo $row['detail_summary'];
         ?>
        <br><br>

        <div style="         
          bottom: 28%;
          position: absolute;
          display: inline;
          left: 52.5%;
          font-size:20px;
        " class="textlink4 font-weight-bold">
            <a href="<?php  echo "project_db.php?id=".$id; ?>">ดูรายละเอียดโครงการเพิ่มเติม</a>
       </div>
        <br>
      </h9>
    </div>

     <?php 
        if($id==23) {
     ?>
    <!-- <div class="one_first" style="margin-top:25px;">
      <div class="btnJoin">
        <a href="popWaitTraining.php">
          <h7  style="color:white!important;">สมัครเข้าร่วมโครงการ</h7>
        </a>
      </div>
    </div> -->
    <?php   
    }  
    ?>


  <?php  if($id==27) {
    ?>
    
<div class="group latest" >
  <!-- <article class="one_half first" style="margin-top:30px;">
    <div class="btnJoin" onclick="window.open('pop noregis.php','popup'); return false;">
      <a href="#">
        <h8 style="color:white!important;" >สมัครเข้าร่วมโครงการ<br>สาขา สารสนเทศ</h7>
      </a>
    </div>
  </article> -->
  <!-- <article class="one_half" style="margin-top:30px;">
    <div class="btnJoin">
      <a href="https://docs.google.com/forms/d/e/1FAIpQLSdENTcJkX3JFSvqpFKmE0aGYnYZZ1l1DIvSNBt9ZyCOnkvMdQ/viewform">
      <h8 style="color:white!important;">สมัครเข้าร่วมโครงการ<br>สาขา กัลยาณมิตร</h6></a>
    </div>
  </article> -->
  </div>
</div>
<?php   } ?>




  </div>
</div> 

 
<?php  }
    $number++;
}   ?>
<!-- End Content 01 - ร่วมโครงการ ครู -->
<!-- ################################################################################################ -->
<!-- Start Content 02 - ร่วมโครงการ นักเรียน -->





<!-- End Content 02 - ร่วมโครงการ นักเรียน -->
<!-- ################################################################################################ -->
<!-- Start Content 03 - ร่วมโครงการ ศึกษานิเทศก์ -->


<!-- <div class="bgJoin bgSizeSupervisor overlay">
  <div class="hoc container3">
    <h5 class="center">โครงการศึกษานิเทศก์ดีของแผ่นดิน</h5>
    <div class="one_half right" style="border-right:3px solid rgb(72,160,0); font-family:ChulaCharasNew; letter-spacing:3px; line-height:35px; padding-right:25px; margin-bottom:20px;">
      <br>
      <h9>ศึกษานิเทศก์คือใคร?
        <br><br>
        ศึกษานิเทศก์ (Supervisor) คือ ผู้ทำหน้าที่แนะนำ ชี้แนะแนวทางให้ครูและผู้บริหารสถานศึกษาเกิดความตระหนักรู้ มีทักษะในการบริหารจัดการ และการจัดการเรียนการสอนอย่างเป็นระบบ 
        รวมทั้งสามารถเป็นที่ปรึกษาทางวิชาการสำหรับนักบริหารระดับสูงในองค์กรที่สังกัด เช่น ศึกษาธิการจังหวัด ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษา ผู้อำนวยการกลุ่มงานต่าง ๆ เป็นต้น
        <br><br>
        <div class="textlink4">
          <a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">ดูรายละเอียดโครงการเพิ่มเติม</a>
        </div>
        <br><br>
      </h9>
    </div>
    
    <div class="one_first">
      <div class="btnJoin">
        <a href="images/ปรับปรุงSkillmeo.jpg"><h6>สมัครเข้าร่วมโครงการ</h6></a>
      </div>
    </div>
  </div>
</div> -->
<!-- End Content 03 - ร่วมโครงการ ศึกษานิเทศก์ -->
<!-- ################################################################################################ -->
<!-- Start Content 04 - ร่วมโครงการ อบรมพัฒนา -->
<!-- <div class="bgJoin bgSizeDevelop overlay">
  <div class="hoc container3">
    <h5 class="center">โครงการอบรม/สัมมนา</h5>
    <div class="one_half first">
      <br>
    </div>
    <div class="one_half left" style="border-left:3px solid rgb(72,160,0); font-family:ChulaCharasNew; letter-spacing:3px; line-height:35px; padding-left:25px; margin-bottom:20px;">
      <br>
      <h9>อบรมสัมมนา
        <br><br>
        ครูและบุคลากรทางการศึกษาทุกระดับชั้น นับเป็นกลไกสำคัญในการพัฒนาคุณภาพการศึกษาไทยให้มีความเจริญก้าวหน้าทัดเทียมอารยประเทศ ทั้งนี้เพื่อตอบสนองต่อการพัฒนาทรัพยากรมนุษย์ด้านการศึกษาอย่างมีระบบ 
        มีประสิทธิภาพ สอดคล้องกับแผนยุทธศาสตร์ชาติ (พ.ศ.2561–2580) และแผนการศึกษาแห่งชาติ (พ.ศ.2560-2579) ตลอดจนตอบโจทย์สมรรถนะครูตามตัวบ่งชี้ในโครงการเครือข่ายครูดีของแผ่นดิน
        <br><br>
        <div class="textlink4">
          <a href="โครงการอบรมสัมมนา.php">ดูรายละเอียดโครงการเพิ่มเติม</a>
        </div>
        <br>
      </h9>
    </div>
    <div class="one_first">
      <div class="btnJoin">
        <a href="popWaitTraining.php"><h6>สมัครเข้าร่วมโครงการ</h6></a>
      </div>
    </div>
  </div>
</div> -->
<!-- End Content 04 - ร่วมโครงการ อบรมพัฒนา -->
<!-- ################################################################################################ -->
<!-- Start Content 05 - ร่วมโครงการ Be Internet Awesome -->
<!-- <div class="bgJoin bgSizeBInAw01 overlay">
  <div class="hoc container3">
    <h5 class="center">โครงการ Be Internet Awesome</h5>
    <div class="one_half right" style="border-right:3px solid rgb(72,160,0); font-family:ChulaCharasNew; font-size:15px; font-weight:bold; letter-spacing:3px; line-height:35px; padding-right:25px; margin-bottom:20px;">
      ทำไมต้องเรียนรู้ Be internet awesome (by google)
        <br>
        เป็นที่ทราบกันดีว่าในยุคเทคโนโลยีปัจจุบัน เราต่างเริ่มตระหนักถึงปัญหาของเด็กไทยกับพิษภัยจากอินเตอร์เน็ตมากขึ้น ซึ่งเปรียบเสมือนดาบสองคมที่มีทั้งคุณอนันต์และโทษมหันต์ในเวลาเดียวกัน การใช้อินเตอร์เน็ตนั้นก่อประโยชน์มหาศาล แต่ก็แอบแฝงไปด้วยอันตรายต่างๆ มากมาย ไม่ว่าจะเป็นเรื่องการเข้าถึงข้อมูลที่ไม่เหมาะสมของเด็ก ค่านิยมผิดๆ ในเรื่องเพศ การล่อลวงในวงสนทนา นอกจากนี้ยังมีเว็บไซต์ที่เต็มไปด้วยเรื่องลามก และเรื่องความรุนแรงต่างๆ มากมาย สื่อสีดำและสีเทามีมากมายในเกมส์โทรศัพท์มือถือที่อยู่ใกล้บุตรหลานเราเสียยิ่งกว่าพ่อแม่ ซึ่งยากต่อการควบคุมตรวจสอบได้ อาชญากรรมทางอินเตอร์เน็ตเหล่านี้เป็นภัยที่อันตรายมากพอที่จะกำหนดเส้นทางอนาคตของเด็กได้เลยทีเดียว 
        <div class="textlink4">
          <a href="โครงการBIA.php">ดูรายละเอียดโครงการเพิ่มเติม</a>
        </div>
      </h10>
    </div>
    <div class="one_first">
      <div class="btnJoin">
        <a href="https://beinternetawesome.withgoogle.com/th_th"><h6>เข้าสู่เว็ปไซต์ Be Internet Awesome by Google</h6></a>
      </div>
    </div>
  </div>
</div> -->
<!-- End Content 05 - ร่วมโครงการ Be Internet Awesome -->
<!-- ################################################################################################ -->
<!-- Start Content 06 - ร่วมโครงการ อาสาของแผ่นดิน -->
<!-- <div class="bgJoin bgSizeVolun overlay">
  <div class="hoc container3">

    <h5 class="center">โครงการอาสาของแผ่นดิน</h5>
    <div class="one_half first">
      <br>
    </div>
    <div class="one_half left" style="border-left:3px solid rgb(72,160,0); font-family:ChulaCharasNew; letter-spacing:3px; line-height:35px; padding-left:25px; margin-bottom:20px;">
      <br>
      <h9>อาสาของแผ่นดิน
        <br><br>
        สังคมไทยในอดีตเป็นสังคมที่คนมีความเอื้อเฟื้อแบ่งปัน มีน้ำใจโอบอ้อมอารี รักการให้เมตตาเอื้ออาทรซึ่งกันและกัน หล่อหลอมในวิถีชีวิตและจิตวิญญาณมาตั้งแต่ครั้งอดีต 
        จนกลายเป็นเอกลักษณ์ทางวัฒนธรรมของคนไทย ในเรื่องของการต้อนรับขับสู้แขกผู้มาเยือนด้วยรอยยิ้มเปี่ยมมิตรไมตรีเสมอ จนได้รับขนานนามไปทั่วโลกว่า “สยามเมืองยิ้ม” 
        <br><br>
        <div class="textlink4">
          <a href="โครงการอาสาของแผ่นดิน.php">ดูรายละเอียดโครงการเพิ่มเติม</a>
        </div>
        <br>
      </h9>
    </div>

    <div class="group latest">
      <article class="one_half first">
        <div class="btnJoin" onclick="window.open('pop noregis.php','popup'); return false;">
          <a href="#"><h8>สมัครเข้าร่วมโครงการ<br>สาขา สารสนเทศ</h6></a>
        </div>
      </article>
      <article class="one_half">
        <div class="btnJoin">
          <a href="https://docs.google.com/forms/d/e/1FAIpQLSdENTcJkX3JFSvqpFKmE0aGYnYZZ1l1DIvSNBt9ZyCOnkvMdQ/viewform"><h8>สมัครเข้าร่วมโครงการ<br>สาขา กัลยาณมิตร</h6></a>
        </div>
      </article>
      </div>
    </div>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End Content 06 - ร่วมโครงการ อาสาของแผ่นดิน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="font-family:CHULALONGKORNReg; color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="font-family:CHULALONGKORNReg; color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>