<?php 
	session_start();
	require_once('condb.php');
	$sql_list_project  ="SELECT * FROM join_project";
	$list_project = $con->query($sql_list_project);

	$email = $_SESSION['email'];
	$sqlorders = "SELECT * FROM orders WHERE email='$email' AND order_group='$order_group' ";
	$reorders = mysqli_query($con,$sqlorders);
	$roworders = mysqli_fetch_array($reorders);
	
	$doc_address = $_SESSION['docaddress'];
	$order_group = $_GET['order_group'];
	$grand_total = $_GET['grand_total'];
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>


<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
        <?php if (strpos($_SESSION['email'], '@') !== false) { //Check Account is signed in
                if ($_SESSION["level"]=="admin") { //Admin Session ?>
									<li style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
											<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*A*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*B*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
										</ul>
									</li>
          <?php }elseif ($_SESSION['level']=='memberGeneral') { //General Member Session ?>
								<li style="color:black; background-color:white;"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> สมาชิกทั่วไป</a>
									<ul>
										<li><a href="editprofile.php">แก้ไขข้อมูลส่วนตัว</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberSilver') { //Member Silver Session ?>
								<li style="color:white; background-color:rgb(169,169,169);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</a>
									<ul>
										<li><a href="editprofile.php">แก้ไขข้อมูลส่วนตัว</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberGold') { //Member Gold Session ?>
								<li style="color:white; background-color:rgb(180,147,31);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</a>
									<ul>
										<li><a href="editprofile.php">แก้ไขข้อมูลส่วนตัว</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberDiamond') { //Member Diamond Session ?>
								<li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</a>
									<ul>
										<li><a href="editprofile.php">แก้ไขข้อมูลส่วนตัว</a></li>
									</ul>
								</li>
          <?php } ?>
								<li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
        <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
                <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
				<?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
                  <ul>
                    <li><a href="ส่งหลักฐานการโอนB-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
                    <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึก</a></li>
                  </ul>
                </li>
        </ul>
			</nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
			  	  <?php 
                    while($row = $list_project->fetch_assoc()){
                      $id_join_project = $row['id_join_project'];
                      $title_menu = $row['title_menu']; 
                  ?>
                  <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
                  <?php } ?>

              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="index.php">ร้านค้า</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ส่งหลักฐานการโอน</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ส่งหลักฐานการโอน -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
	<!-- ################################################################################################ -->
	<!-- Start Slip Upload  -->
	<div class="cart-box-main">
    <div class="container">
      <div class="row">
				<!-- ################################################################################################ -->
				<!-- Start Left Content -->
				<div class="col-sm-6 col-lg-6 mb-3">
					<div class="slip-page">
						<div class="title-left">
							<h3>ช่องทางการชำระเงิน</h3>
						</div>
						<div class="row">
							<div class="col-md-6 mb-3">
								<img src="images/มูลนิธิครูดีของแผ่นดิน KTB Logo.jpg" alt="Logo KTB">
							</div>
							<div class="col-md-6 mb-3">
								<p>
									<strong>ธนาคารกรุงไทย</strong>
									<br>
									<strong>ชื่อบัญชี:</strong> มูลนิธิครูดีของแผ่นดิน สาขาถนนวิสุทธิกษัตริย์
									<br>
									<strong>เลขที่บัญชี:</strong> 006 – 0 – 20390 – 0
								</p>
							</div>
						</div>
						<div class="center m-t-30 m-b-30">
							<img style="width:50%;" src="images/คิวอาร์โคด บัญชีมูลนิธิ.jpg" alt="คิวอาร์โคด บัญชีมูลนิธิ">
						</div>
					</div>
				</div>
				<!-- End Left Content -->
				<!-- ################################################################################################ -->
				<!-- Start Right Content -->
				<div class="col-sm-6 col-lg-6 mb-3">
					<div class="col-md-12 col-lg-12">
						<div class="input-slip-upload">
							<div class="title-left">
								<h3>แจ้งชำระเงิน</h3>
							</div>
							<!-- Start form input -->
							<form action="uploadslipB.php" method="POST" enctype="multipart/form-data">
								<div class="row m-t-20">
									<div class="col-md-6 mb-3">
										<label for="date-pay">วันที่ชำระเงิน *</label>
										<input type="date" name="date_pay" class="form-control" placeholder="" min='2020-04-14' max='<?php echo date("Y-m-d") ?>' required/>
									</div>
									<div class="col-md-6 mb-3">
										<label for="time-pay">เวลา (โดยประมาณ) *</label>
										<input type="time" name="time_pay" class="form-control" placeholder="" required/>
									</div>
								</div>
								<div class="row m-t-20">
									<div class="col-md-6 mb-3">
										<label for="grand-total">เลขที่ใบสั่งซื้อ * ตัวอย่าง 0141</label>
										<input type="text" name="nomem_code" class="form-control" placeholder="XXXX" maxlength="4" pattern="[0-9]{4}" required/>
									</div>
									<div class="col-md-6 mb-3">
										<label for="slip_image">หลักฐานการโอน *</label>
										<input type="file" id="slip_image" name="slip_image" class="form-control" accept="image/*" required/>
									</div>
								</div>
								<div class="row m-t-20">
									<div class="col-md-6 mb-3">
										<label for="bill_tax_kind">นามในการออกใบเสร็จ</label>
										<select id="bill_tax_kind" name="bill_tax_kind" class="form-control" onchange="PickBillIN(this.value);" required>
											<option value="" disabled="disabled" selected="selected">โปรดเลือก</option>
											<option value="Personal">บุคคล</option>
											<option value="Corporation">นิติบุคคล บริษัท/ห้าง/ร้าน</option>
											<option value="Government">หน่วยงานราชการ</option>
										</select>
									</div>
									<div class="col-md-6 mb-3" style="display:none" id="ID-Personal">
										<label for="ID-Personal">เลขบัตรประชาชน *</label>
										<input type="text" pattern="[0-9]{13,13}" maxlength="13" title="โปรดใส่ตัวเลข13ตัว" name="bill_tax_id_Personal" class="form-control" id="ID-Personal-required" />
									</div>
									<div class="col-md-6 mb-3" style="display:none" id="ID-Corporation">
										<label for="ID-Corporation">เลขประจำตัวผู้เสียภาษี *</label>
										<input type="text" pattern="[0-9]{13,13}" maxlength="13" title="โปรดใส่ตัวเลข13ตัว" name="bill_tax_id_Corporation" class="form-control" id="ID-Corporation-required" />
									</div>
									<div class="col-md-6 mb-3" style="display:none" id="ID-Government">
										<label for="ID-Government">ชื่อหน่วยงานของท่าน *</label>
										<input type="text" title="โปรดใส่ชื่อหน่วยงานของท่าน" name="bill_tax_id_Government" class="form-control" id="ID-Government-required" />
									</div>
								</div>
								<div class="center m-t-30 m-b-30 fs-16">
									*กรุณาตรวจทานรายละเอียดให้ถูกต้องอีกครั้ง ก่อนยืนยันการแจ้งชำระเงิน
								</div>
								<div class="shopping-box">
									<input type="hidden" name="CFP" value="10">
									<input type="hidden" name="order_group" value="<?php echo $order_group ?>" >
									<button type="submit" name="submit" class="ml-auto btn hvr-hover">แจ้งชำระเงิน</button>
								</div>
							</form>
							<!-- End form input -->
						</div>
					</div>
      	</div>
				<!-- End Right Content -->
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
  <!-- End Slip Upload -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ส่งหลักฐานการโอน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->


<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<script src="js/pickBillType.js"></script>

</body>
</html>