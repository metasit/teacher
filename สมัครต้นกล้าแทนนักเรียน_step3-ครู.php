<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if($_POST['CFP'] == 'สมัครต้นกล้าแทนนักเรียน_step2-ครู') {

			/* Set prename */
			$prename_stu = $_POST['prename_stu'];
			/* Set firstname */
			$firstname_stu = $_POST['firstname_stu'];
			/* Set lastname */
			$lastname_stu = $_POST['lastname_stu'];
			/* Set birthdate */
			$birthdate_stu = $_POST['birthdate_stu'];
			/* Set special child btn */
			$spe_child_btn = $_POST['spe_child_btn'];
			/* Set pomain_id */
			$pomain_id = $_POST['pomain_id'];
			/* Set occup_code */
			$occup_code = 'OcF'.$pomain_id;
			/* occup_name */
			if($pomain_id == 'A') {

				$pomain_name = 'ประธาน/รองประธาน/กรรมการ นักเรียน-นักศึกษา';

			}elseif($pomain_id == 'B') {

				$pomain_name = 'ประธาน/รองประธาน/กรรมการ ชมรม-ชุมนุม-สโมสร-กิจกรรม';

			}elseif($pomain_id == 'C') {

				$pomain_name = 'หัวหน้า/รองหัวหน้า ห้อง-กลุ่ม-ชั้นปี';

			}elseif($pomain_id == 'D') {

				$pomain_name = 'นักเรียน/นักศึกษาทั่วไป';

			}

			$occup_name = 'นักเรียน/นักศึกษา*'.$pomain_name.'**';

			/* Set ID_stu */
			if($_POST['ID_stu'] != '') {

				$ID_stu = $_POST['ID_stu'];

			}else{

				$sqllogin = "SELECT ID FROM `login` WHERE firstname='$firstname_stu' AND lastname='$lastname_stu' AND basic_score_remark2 LIKE '%B,yes%' ";
				$relogin = mysqli_query($con, $sqllogin);
				$rowlogin = mysqli_fetch_array($relogin);

				$ID_stu = $rowlogin['ID'];

			}
			
			/* Set affiliation */
			$affil_code = $_SESSION['affil_code'];

			include('includes/convert2afftext.php');

			$sql = "UPDATE `login` SET occup_code='$occup_code', occup_name='$occup_name' WHERE ID='$ID_stu' ";
			$re = $con->query($sql) or die($con->error);
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* Log User Action */
			$scorelog_task = 'สมัครต้นกล้าแทนนักเรียน_step2';
			$scorelog_detail = 'ครู,ชั้น5,เด็ก,ขั้นพื้นฐาน,'.$occup_code.','.$occup_name;
			$scorelog_total = intval($ID_stu);
			$sqllog = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail, scorelog_total) VALUES ('$ID', '$scorelog_task', '$scorelog_detail', '$scorelog_total') ";
			$relog = $con->query($sqllog) or die($con->error);

		}elseif($_GET['CFP'] == 'สมัครต้นกล้าแทนนักเรียน_step4-ครู') {

			$prename_stu = $_GET['prename_stu'];
			$firstname_stu = $_GET['firstname_stu'];
			$lastname_stu = $_GET['lastname_stu'];
			$birthdate_stu = $_GET['birthdate_stu'];
			$spe_child_btn = $_GET['spe_child_btn'];
			$ID_stu = $_GET['ID_stu'];
			$pomain_id = $_GET['pomain_id'];
			$affil_code = $_SESSION['affil_code'];
			include('includes/convert2afftext.php');

		}else{

			echo "<script>window.history.go(-1)</script>";

		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/เด็กดีของแผ่นดิน Logo.jpg" style="width:70%;" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" action="สมัครต้นกล้าแทนนักเรียน_step4-ครู.php" method="POST" enctype="multipart/form-data">
					<!-- ################################################################################################ -->
					<span class="login100-form-title">
						ยืนยันสังกัดนักเรียน
					</span>
					<!-- ################################################################################################ -->
					<!-- Start show step -->
					<div class="center">
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
						<span class="line" style="width:25%"></span>
						<span class="dot"></span>
					</div>
					<div class="center">
						<p style="color:rgb(94,177,26);">ขั้นตอนที่ 3: ยืนยันสังกัด</p>
					</div>
					<!-- End show step -->
					<!-- ################################################################################################ -->
					<!-- Start สังกัด Content -->
					<!-- เลือกสังกัด -->
					<label for="affiliation_id" class="center m-t-10 text-black bold">สังกัดหลัก</label>
					<select name="affiliation_id" id="affiliation_id" class="form-control" style="height:40px;" required>
						<option value="" disabled="disabled" selected="selected">ไม่สามารถแก้ไขได้ นักเรียนต้องมีสังกัดตรงกับครูที่ปรึกษาเท่านั้น</option>
						<option value="AfA" <?php if($affiliation_id=='AfA'){echo 'selected="selected" ';}else{echo 'style="display: none;" ';} ?> >สำนักงานปลัดกระทรวงศึกษาธิการ</option>
						<option value="AfB" <?php if($affiliation_id=='AfB'){echo 'selected="selected" ';}else{echo 'style="display: none;" ';} ?> >สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน</option>
						<option value="AfC" <?php if($affiliation_id=='AfC'){echo 'selected="selected" ';}else{echo 'style="display: none;" ';} ?> >สำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน</option>
						<option value="AfD" <?php if($affiliation_id=='AfD'){echo 'selected="selected" ';}else{echo 'style="display: none;" ';} ?> >สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย</option>
						<option value="AfE" <?php if($affiliation_id=='AfE'){echo 'selected="selected" ';}else{echo 'style="display: none;" ';} ?> >สำนักงานคณะกรรมการการอาชีวศึกษา</option>
						<option value="AfF" <?php if($affiliation_id=='AfF'){echo 'selected="selected" ';}else{echo 'style="display: none;" ';} ?> >สำนักงานคณะกรรมการการอุดมศึกษา</option>
						<option value="AfG" <?php if($affiliation_id=='AfG'){echo 'selected="selected" ';}else{echo 'style="display: none;" ';} ?> >กระทรวงมหาดไทย กรมส่งเสริมการปกครองส่วนท้องถิ่น</option>
						<option value="AfH" <?php if($affiliation_id=='AfH'){echo 'selected="selected" ';}else{echo 'style="display: none;" ';} ?> >กรุงเทพมหานคร</option>
						<option value="AfI" <?php if($affiliation_id=='AfI'){echo 'selected="selected" ';}else{echo 'style="display: none;" ';} ?> >เมืองพัทยา</option>
						<option value="AfJ" <?php if($affiliation_id=='AfJ'){echo 'selected="selected" ';}else{echo 'style="display: none;" ';} ?> >สำนักงานตำรวจแห่งชาติ</option>
						<option value="AfO" <?php if($affiliation_id=='AfO'){echo 'selected="selected" ';}else{echo 'style="display: none;" ';} ?> >อื่นๆ โปรดระบุ....</option>
					</select>
					<label for="affsub_id" id="affsubhead" class="center m-t-10 text-black bold">สังกัดย่อย</label>
					<select name="affsub_id" id="affsub_id" class="form-control" style="height:40px; <?php if($affsub_name!=''){echo 'display:block;';}else{echo 'display:none;';} ?>">
						<option value="" disabled="disabled" selected="selected">ไม่สามารถแก้ไขได้ นักเรียนต้องมีสังกัดตรงกับครูที่ปรึกษาเท่านั้น</option>
						<option value="<?php echo $affsub_id; ?>" selected="selected"><?php echo $affsub_name; ?></option>
					</select>
					<div id="affsub_text" <?php if($affsub_ans_id!=''){echo 'style="display:block" ';}else{echo 'style="display:none" ';} ?>>
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="affsub_ans_id" id="affsub_ans_id" type="text" class="form-control" 
							placeholder="<?php if($affsub_ans_id!=''){echo $affsub_ans_id;}else{echo 'โปรดระบุสังกัด';}?>" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<select name="affsub2_id" id="affsub2_id" class="form-control" style="height:40px; <?php if($affsub2_name!=''){echo 'display:block;';}else{echo 'display:none;';} ?>">
						<option value="" disabled="disabled" selected="selected">ไม่สามารถแก้ไขได้ นักเรียนต้องมีสังกัดตรงกับครูที่ปรึกษาเท่านั้น</option>
						<option value="<?php echo $affsub2_id; ?>" selected="selected"><?php echo $affsub2_name; ?></option>
					</select>
					<select name="affsub3_id" id="affsub3_id" class="form-control" style="height:40px; <?php if($affsub3_name!=''){echo 'display:block;';}else{echo 'display:none;';} ?>">
						<option value="" disabled="disabled" selected="selected">ไม่สามารถแก้ไขได้ นักเรียนต้องมีสังกัดตรงกับครูที่ปรึกษาเท่านั้น</option>
						<option value="<?php echo $affsub3_id; ?>" selected="selected"><?php echo $affsub3_name; ?></option>
					</select>
					<select name="affsub4_id" id="affsub4_id" class="form-control" style="height:40px; <?php if($affsub4_name!=''){echo 'display:block;';}else{echo 'display:none;';} ?>">
						<option value="" disabled="disabled" selected="selected">ไม่สามารถแก้ไขได้ นักเรียนต้องมีสังกัดตรงกับครูที่ปรึกษาเท่านั้น</option>
						<option value="<?php echo $affsub4_id; ?>" selected="selected"><?php echo $affsub4_name; ?></option>
					</select>
					<div id="affsub4_text" <?php if($affsub4_ans_id!=''){echo 'style="display:block" ';}else{echo 'style="display:none" ';} ?>>
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="affsub4_ans_id" id="affsub4_ans_id" type="text" class="form-control" 
							placeholder="<?php if($affsub4_ans_id!=''){echo $affsub4_ans_id;}else{echo 'โปรดระบุสังกัด';}?>" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- End สังกัด Content -->
					<div class="container-login100-form-btn m-t-20">
						<a href="สมัครต้นกล้าแทนนักเรียน_step2-ครู.php?
							CFP=สมัครต้นกล้าแทนนักเรียน_step3-ครู
							&pomain_id=<?php echo $pomain_id; ?>
							&ID_stu=<?php echo $ID_stu; ?>
							&prename_stu=<?php echo $prename_stu; ?>
							&firstname_stu=<?php echo $firstname_stu; ?>
							&lastname_stu=<?php echo $lastname_stu; ?>
							&birthdate_stu=<?php echo $birthdate_stu; ?>
							&spe_child_btn=<?php echo $spe_child_btn; ?>" 
							class="login100-form-btn" style="width:30%; margin:0 10px;">ย้อนกลับ
						</a>
						<button class="login100-form-btn" style="width:30%; margin:0 10px;" type="submit" id="register">ยืนยัน</button>
					</div>
					<!-- ################################################################################################ -->

					<input type="hidden" name="prename_stu" value="<?php echo $prename_stu; ?>">
					<input type="hidden" name="firstname_stu" value="<?php echo $firstname_stu; ?>">
					<input type="hidden" name="lastname_stu" value="<?php echo $lastname_stu; ?>">
					<input type="hidden" name="birthdate_stu" value="<?php echo $birthdate_stu; ?>">
					<input type="hidden" name="spe_child_btn" value="<?php echo $spe_child_btn; ?>">
					<input type="hidden" name="pomain_id" value="<?php echo $pomain_id; ?>">
					<input type="hidden" name="ID_stu" value="<?php echo $ID_stu; ?>">
					<input type="hidden" name="CFP" value="สมัครต้นกล้าแทนนักเรียน_step3-ครู">
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>

<!-- JS for multi-sub-dropdown -->
<script src="js/jquery.min.js"></script>

</body>
</html>