<?php
    session_start();
    require_once('../../../condb.php');
    date_default_timezone_set("Asia/Bangkok");

    function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
    $name = $_FILES['photo']['name'];
    $name = explode(".",$name)[1];
    $gen_uuid = gen_uuid().".".$name;
    $title = $_POST['title'];
    $file_name = $gen_uuid;
    $detail_first = $_POST['detail_first'] ;
    $detail = $_POST['detail'];
    $day_event = $_POST['day_event'];
    

    move_uploaded_file($_FILES["photo"]['tmp_name'],"../../backend/galangal/image/$gen_uuid");
    $sql_insert = "INSERT INTO `message` (`id_message`, `title`, `file_name`, `detail_first` , `Detail`,`day_event`, `create_at`, `update_at`) VALUES (NULL, '$title', '$gen_uuid','$detail_first' ,'$detail','$day_event', current_timestamp(), current_timestamp()); ";
    $query =  mysqli_query($con, $sql_insert);
    if($query){
        $age = array("status"=>true);
        echo json_encode($age);
    }
    else {
        $age = array("status"=>false);
        echo json_encode($age);
    }
?>