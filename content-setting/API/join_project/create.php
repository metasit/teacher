<?php
	session_start();
	require_once('../../../condb.php');
	date_default_timezone_set("Asia/Bangkok");
    function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }


    $name = $_FILES["data_file"]["name"];
    $name = explode(".",$name)[1];
    $gen_uuid = gen_uuid().".".$name;

    $name_bg = $_FILES["file_background"]["name"];
    $name_bg = explode(".",$name_bg)[1];
    $gen_uuid_bg = gen_uuid().".".$name_bg;

    // file_background
    
    
    $title_menu = $_POST['title_menu'];
    $title_project = $_POST['title_project'];
    $title_gallery = $_POST['title_gallery'];
    $title_pdf = $_POST['title_pdf'] ;
    $title_video = $_POST['title_video'];
    $link_video = $_POST['link_video'];
    $detail = $_POST['detail'];
    $detail_summary = $_POST['detail_summary'];




    $sql_innovation = "INSERT INTO
     `join_project`(`id_join_project`, `title_menu`, `title_project`, `img_top_project`, `detail`,`detail_summary` ,`file_background_summary`,`title_gallery`,	`title_pdf` ,`title_video`,`link_video`  ,`create_at`,`update_at`) 
     VALUES (null, '$title_menu', '$title_project', '$gen_uuid', '$detail','$detail_summary','$gen_uuid_bg', '$title_gallery','$title_pdf','$title_video','$link_video',current_timestamp(), current_timestamp());";
    
    $query =  mysqli_query($con, $sql_innovation); 
    if($query){
        move_uploaded_file($_FILES["data_file"]['tmp_name'],"../../backend/join_project/image/$gen_uuid");
        move_uploaded_file($_FILES["file_background"]['tmp_name'],"../../backend/join_project/bg-background/$gen_uuid_bg");
        
        $sql_list  ="SELECT * FROM join_project order by id_join_project desc  LIMIT 0,1 ";
        $list = $con->query($sql_list) ;
        while($row = $list->fetch_assoc()) {
            $id_join_project = $row['id_join_project']; 
        }

        $len_file = count($_FILES["data_file_multiple"]["name"])  ;
        for ($x=0; $x < $len_file ; $x++) { 
            $name = $_FILES["data_file_multiple"]["name"][$x];
            $name = explode(".",$name)[1];
            $name_token = $_FILES["data_file_multiple"]["name"][$x];
            $gen_uuid = gen_uuid().".".$name;
            move_uploaded_file($_FILES["data_file_multiple"]['tmp_name'][$x],"../../backend/join_project/image/$gen_uuid");

            $sql_join_pdf = "
            INSERT INTO `join_project_pdf` (`id_pdf`, `name_file`, `name_show`, `id_join_project`,`type_file` , `create_at`, `update_at`) 
            VALUES (null, '$gen_uuid', '$name_token', '$id_join_project','1', current_timestamp(), current_timestamp());";
            mysqli_query($con,$sql_join_pdf); 
        }

        $len_file = count($_FILES["file_pdf"]["name"])  ;
        for ($x=0; $x < $len_file ; $x++) { 
            $name = $_FILES["file_pdf"]["name"][$x];
            $name = explode(".",$name)[1];
            $name_token = $_FILES["file_pdf"]["name"][$x];
            $gen_uuid = gen_uuid().".".$name;
            move_uploaded_file($_FILES["file_pdf"]['tmp_name'][$x],"../../backend/join_project/pdf/$gen_uuid");
            $sql_join_pdf = "
            INSERT INTO `join_project_pdf` (`id_pdf`, `name_file`, `name_show`, `id_join_project`,`type_file` ,`create_at`, `update_at`) 
            VALUES (null, '$gen_uuid', '$name_token', '$id_join_project','2', current_timestamp(), current_timestamp());";
            mysqli_query($con,$sql_join_pdf);
        }

        $arr = array('status' => true);
        echo json_encode($arr);


        // $sql_list  ="SELECT * FROM banner";
        // $list = $con->query($sql_list) ;
        // while($row = $list->fetch_assoc()) {
        //     $data_list[] = $row; 
        // }
        // $arr = array('status' => true,'data'=>$data_list);

    }
    else {
        $arr = array('status' => false,"sql"=>$sql_innovation );
        echo json_encode($arr);
    }
    // echo json_encode($arr);
    // var_dump($_FILES["json"])

    // array(5) { ["name"]=> array(5) { [0]=> string(12) "download.jpg" [1]=> string(56) "abstract-banner-background-with-red-shapes_1361-3348.jpg" [2]=> string(52) "blue-copy-space-digital-background_23-2148821698.jpg" [3]=> string(71) "hand-painted-watercolor-background-with-sky-clouds-shape_24972-1095.jpg" [4]=> string(36) "af8d63a477078732b79ff9d9fc60873f.jpg" } ["type"]=> array(5) { [0]=> string(10) "image/jpeg" [1]=> string(10) "image/jpeg" [2]=> string(10) "image/jpeg" [3]=> string(10) "image/jpeg" [4]=> string(10) "image/jpeg" } ["tmp_name"]=> array(5) { [0]=> string(23) "C:\xampp\tmp\php2E2.tmp" [1]=> string(23) "C:\xampp\tmp\php2E3.tmp" [2]=> string(23) "C:\xampp\tmp\php2E4.tmp" [3]=> string(23) "C:\xampp\tmp\php2E5.tmp" [4]=> string(23) "C:\xampp\tmp\php2E6.tmp" } ["error"]=> array(5) { [0]=> int(0) [1]=> int(0) [2]=> int(0) [3]=> int(0) [4]=> int(0) } ["size"]=> array(5) { [0]=> int(5357) [1]=> int(23302) [2]=> int(49449) [3]=> int(51843) [4]=> int(124887) } }


?>


