<?php
	session_start();
	require_once('../../../condb.php');
	date_default_timezone_set("Asia/Bangkok");
    function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
    
            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),
    
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,
    
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,
    
            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
    
    $title_menu = $_POST['title_menu'];
    $title_project = $_POST['title_project'];
    $detail = $_POST['detail'];
    $title_gallery = $_POST['title_gallery'];
    $title_pdf = $_POST['title_pdf'];
    $title_video = $_POST['title_video'];
    $link_video = $_POST['link_video'];
    $detail_summary = $_POST['detail_summary'];
    $detail_background_summary = ['detail_background_summary'] ;
    $pre_message_project = $_POST['pre_message_project'];

    $txt_pre_message_project =  " pre_message = '$pre_message_project',  ";

    $id = $_POST['id'];
    $file_background_summary = "";
    if(in_array("detail_background_summary", array_keys($_FILES))){
        $name_bg = $_FILES["detail_background_summary"]["name"];
        $name_bg = explode(".",$name_bg)[1];
        $gen_uuid_bg = gen_uuid().".".$name_bg;
        move_uploaded_file($_FILES["detail_background_summary"]['tmp_name'],"../../backend/join_project/bg-background/$gen_uuid_bg");
        $file_background_summary = " file_background_summary = '$gen_uuid_bg',  ";
        // $img_top = " img_top_project = '$gen_uuid'  ";
    }




    if(in_array("data_file", array_keys($_FILES))){
        $name = $_FILES["data_file"]["name"];
        $name = explode(".",$name)[1];
        $gen_uuid = gen_uuid().".".$name;
        move_uploaded_file($_FILES["data_file"]['tmp_name'],"../../backend/join_project/image/$gen_uuid");

        $img_bg = " img_top_project = '$gen_uuid'  ";
    }

    if(in_array("data_file", array_keys($_FILES)))
    $sql_list = " UPDATE join_project SET 
    $file_background_summary
    $txt_pre_message_project
    title_menu = '$title_menu' , 
    title_project = '$title_project',
    title_pdf = '$title_pdf',
    img_top_project = '$gen_uuid',
    detail = '$detail', 
    title_gallery = '$title_gallery',
    title_video = '$title_video',
    link_video  = '$link_video',
    detail_summary =  '$detail_summary'
    
    WHERE id_join_project  =  $id
     ";
    else  $sql_list = " UPDATE join_project SET 
    $file_background_summary
    $txt_pre_message_project
    title_menu = '$title_menu' , 
    title_project = '$title_project',
    title_pdf = '$title_pdf',
    detail = '$detail' ,
    title_gallery = '$title_gallery',
    title_video = '$title_video',
    link_video  = '$link_video',
    detail_summary =  '$detail_summary'
    WHERE id_join_project  =  $id
     ";
    $list = mysqli_query($con, $sql_list);
    if($list){
        
        if(in_array("data_file_multiple", array_keys($_FILES))){
            $len_file = count($_FILES["data_file_multiple"]["name"])  ;
            $sql_delete = " DELETE FROM join_project_pdf WHERE  id_join_project = $id  AND type_file = '1'";
            mysqli_query($con, $sql_delete);
            for ($x=0; $x < $len_file ; $x++) { 
                $name = $_FILES["data_file_multiple"]["name"][$x];
                $name = explode(".",$name)[1];
                $name_token = $_FILES["data_file_multiple"]["name"][$x];
                $gen_uuid = gen_uuid().".".$name;
                move_uploaded_file($_FILES["data_file_multiple"]['tmp_name'][$x],"../../backend/join_project/image/$gen_uuid");
                $sql_join_pdf = "INSERT INTO `join_project_pdf` (`id_pdf`, `name_file`, `name_show`,`id_join_project`, `type_file`,`create_at`, `update_at`) VALUES (null, '$gen_uuid', '$name_token', '$id', '1', current_timestamp(), current_timestamp());";
                 $result =  mysqli_query($con,$sql_join_pdf);
            }
        }

        if(in_array("file_pdf", array_keys($_FILES))){

            $sql_delete = " DELETE FROM join_project_pdf WHERE  id_join_project = $id AND type_file = '2' ";
            mysqli_query($con, $sql_delete);
            $len_file = count($_FILES["file_pdf"]["name"]);
            for ($x=0; $x < $len_file ; $x++) { 
                $name = $_FILES["file_pdf"]["name"][$x];
                $name = explode(".",$name)[1];
                $name_token = $_FILES["file_pdf"]["name"][$x];
                $gen_uuid = gen_uuid().".".$name;
                move_uploaded_file($_FILES["file_pdf"]['tmp_name'][$x],"../../backend/join_project/pdf/$gen_uuid");
                $sql_join_pdf = "
                INSERT INTO `join_project_pdf`
                 (`id_pdf`, `name_file`, `name_show`, `id_join_project`,`type_file`, `create_at`, `update_at`) 
                VALUES (null, '$gen_uuid', '$name_token', '$id', '2', current_timestamp(), current_timestamp());";
                mysqli_query($con,$sql_join_pdf);
            }
        }
        
        $arr = array('status' => true);   
        echo json_encode($arr);
    }
    else {
        $arr = array('status' => false,'sql'=>$sql_list);   
        echo json_encode($arr);
    }


    
    exit();

    // $id = $_POST['id'];
    // $name = $_POST['name'];
    // $title_first = $_POST['title_first'];
    // $school = $_POST['school'];
    // $category = $_POST['category'];
    // $color = $_POST['color'];
    // $title_content = $_POST['title_content'];
    // $summernote = $_POST['summernote'];
    // $link_youtube = $_POST['link_youtube'];




    
    
    // $query =  mysqli_query($con, $sql_innovation); 
    if($query){
        // $fileTmpName  = $_FILES["json"]['tmp_name'];
        // if(in_array("data_file", array_keys($_FILES))){
        //     move_uploaded_file($_FILES["data_file"]['tmp_name'],"../../backend/innovation_library/image/$gen_uuid");
        // }
        // if(in_array("data_video", array_keys($_FILES))){
        //     move_uploaded_file($_FILES["data_video"]['tmp_name'],"../../backend/innovation_library/video/$gen_uuid_data_video");
        // }
        
        // $sql_list  ="SELECT * FROM innovation_data";
        // $list = $con->query($sql_list) ;
        // while($row = $list->fetch_assoc()) {
        //     $data_list[] = $row; 
        // }
        // $arr = array('status' => true,'data'=>$data_list);
    }
    else {
        $arr = array('status' => false,'sql'=>$sql_innovation);
    }
    echo json_encode($arr);
    // var_dump($_FILES["json"])
?>


