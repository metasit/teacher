<?php 
	session_start();
	require_once('../../../condb.php');
	date_default_timezone_set("Asia/Bangkok");
    function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
    $nick_name = $_FILES["photo"]['name'];
    $gen_uuid = gen_uuid().".". explode(".",$nick_name)[1] ;
    $title = $_POST['title'];
    $status_mail = $_POST['status_mail'];
    $day_open = $_POST['day_open'];
    
    move_uploaded_file($_FILES["photo"]['tmp_name'],"../../backend/mail_announcement/image/$gen_uuid");
    $sql_letter = "INSERT INTO `letter` (`id_letter`, `title`,`day_open`,`nick_name_pdf` ,`pdf_name`,`status`, `create_at`, `update_at`) 
    VALUES (NULL, '$title','$day_open','$nick_name','$gen_uuid','$status_mail',current_timestamp(), current_timestamp());";
    $query =  mysqli_query($con, $sql_letter); 
    if($query){
        $age = array("status"=>true);
        echo json_encode($age);
    }
    else {
        $age = array("status"=>false,"sql"=>$sql_letter);
        echo json_encode($age);
    }
?>