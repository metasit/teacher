<?php
	session_start();
	require_once('../../../condb.php');
	date_default_timezone_set("Asia/Bangkok");
    function gen_uuid() {
        return sprintf( '%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
    
            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),
    
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,
    
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,
    
            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
    

    $name = $_POST['name'];
    $title_first = $_POST['title_first'];
    $school = $_POST['school'];
    $category = $_POST['category'];
    $color = $_POST['color'];
    $title_content = $_POST['title_content'];
    $summernote = $_POST['summernote'];

    $link_youtube = "";

    if(in_array("link_youtube", array_keys($_POST))){
        $link_youtube = $_POST['link_youtube'];
    }

    



    $data_file = $_FILES["data_file"]["name"];
    $data_file = explode(".",$data_file)[1];
    $gen_uuid = gen_uuid().".".$data_file;
    
    if(in_array("data_video", array_keys($_FILES))){
        $data_video = $_FILES["data_video"]["name"];
        $data_video = explode(".",$data_video)[1];
        $gen_uuid_data_video = gen_uuid().".".$data_video;
    }

    if(in_array("data_doc_pdf", array_keys($_FILES))){
        $data_pdf = $_FILES["data_doc_pdf"]["name"];
        $data_pdf = explode(".",$data_pdf)[1];
        $gen_uuid_data_pdf = gen_uuid().".".$data_pdf;
    }

    $data_doc = $_FILES["data_doc"]["name"];
    $data_doc = explode(".",$data_doc)[1];
    $gen_uuid_data_doc = gen_uuid().".".$data_doc;


    // var_dump($gen_uuid_data_doc);
    // exit();

    
    

    $sql_innovation = "INSERT INTO `innovation_data` (`id_innovation`, `photo`, `title_first`, `name`, `school`, `color`, `title_content`, `detail_content`, `link_youtube`, `video`,`file_doc` ,`file_pdf`, `id_category`, `create_at`, `update_at`) 
    VALUES (NULL, '$gen_uuid', '$title_first', '$name', '$school', '$color', '$title_content', '$summernote', '$link_youtube', '$gen_uuid_data_video','$gen_uuid_data_doc','$gen_uuid_data_pdf' ,'$category', current_timestamp(), current_timestamp());";
    $query =  mysqli_query($con, $sql_innovation); 
    if($query){
        // $fileTmpName  = $_FILES["json"]['tmp_name'];
        move_uploaded_file($_FILES["data_file"]['tmp_name'],"../../backend/innovation_library/image/$gen_uuid");
        
        if(in_array("data_video", array_keys($_FILES)))
        move_uploaded_file($_FILES["data_video"]['tmp_name'],"../../backend/innovation_library/video/$gen_uuid_data_video");

        if(in_array("data_doc_pdf", array_keys($_FILES))){

        move_uploaded_file($_FILES["data_doc_pdf"]['tmp_name'],"../../backend/innovation_library/doc_pdf/$gen_uuid_data_pdf");
        }
        
        move_uploaded_file($_FILES["data_doc"]['tmp_name'],"../../backend/innovation_library/doc/$gen_uuid_data_doc");
        $sql_list  ="SELECT * FROM innovation_data";
        $list = $con->query($sql_list) ;
        while($row = $list->fetch_assoc()) {
            $data_list[] = $row; 
        }
        $arr = array('status' => true,'data'=>$data_list);
    }
    else {
        $arr = array('status' => false,'sql'=>$sql_innovation);
    }
    echo json_encode($arr);
    // var_dump($_FILES["json"])
?>


