<?php
	session_start();
	require_once('../../../condb.php');
	date_default_timezone_set("Asia/Bangkok");
    function gen_uuid() {
        return sprintf( '%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
    
            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),
    
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,
    
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,
    
            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
    
    $id = $_POST['id'];
    $name = $_POST['name'];
    $title_first = $_POST['title_first'];
    $school = $_POST['school'];
    $category = $_POST['category'];
    $color = $_POST['color'];
    $title_content = $_POST['title_content'];
    $summernote = $_POST['summernote'];
    $link_youtube = $_POST['link_youtube'];


    if(in_array("data_file", array_keys($_FILES))){
        $data_file = $_FILES["data_file"]["name"];
        $data_file = explode(".",$data_file)[1];
        $gen_uuid = gen_uuid().".".$data_file;        
    }

    
    if(in_array("data_video", array_keys($_FILES))){
        $data_video = $_FILES["data_video"]["name"];
        $data_video = explode(".",$data_video)[1];
        $gen_uuid_data_video = gen_uuid().".".$data_video;        
    }

    if(in_array("data_doc", array_keys($_FILES))){
        $data_doc = $_FILES["data_doc"]["name"];
        $data_doc = explode(".",$data_doc)[1];
        $gen_uuid_data_doc = gen_uuid().".".$data_doc;   
        
        
    }

    $pdf_str = "";
    if(in_array("data_doc_pdf", array_keys($_FILES))){
        $data_pdf = $_FILES["data_doc_pdf"]["name"];
        $data_pdf = explode(".",$data_pdf)[1];
        $gen_uuid_data_pdf = gen_uuid().".".$data_pdf;
        $pdf_str = " file_pdf = '$gen_uuid_data_pdf', ";
    }

    

    



    if(in_array("data_file", array_keys($_FILES)) && in_array("data_video", array_keys($_FILES))){
        
        if(in_array("data_file", array_keys($_FILES))) {

            $sql_innovation = "UPDATE innovation_data set 
            photo = '$gen_uuid',
            $pdf_str
            video = '$gen_uuid_data_video',
            file_doc = '$gen_uuid_data_doc',
            title_first = '$title_first',
            name = '$name',
            school =  '$school',
            color = '$color',
            title_content = '$title_content',
            detail_content = '$summernote',
            link_youtube = '$link_youtube',
            id_category = '$category'
            WHERE id_innovation  = $id
            "; 

        }
        else {
            $sql_innovation = "UPDATE innovation_data set 
            photo = '$gen_uuid',
            $pdf_str
            video = '$gen_uuid_data_video',
            title_first = '$title_first',
            name = '$name',
            school =  '$school',
            color = '$color',
            title_content = '$title_content',
            detail_content = '$summernote',
            link_youtube = '$link_youtube',
            id_category = '$category'
            WHERE id_innovation  = $id
            "; 
        }
        
       
    }
    
    elseif(in_array("data_file", array_keys($_FILES))){


        if(in_array("data_doc", array_keys($_FILES))){
            $sql_innovation = "UPDATE innovation_data set 
            photo = '$gen_uuid',
            $pdf_str
            title_first = '$title_first',
            file_doc = '$gen_uuid_data_doc',
            name = '$name',
            school =  '$school',
            color = '$color',
            title_content = '$title_content',
            detail_content = '$summernote',
            link_youtube = '$link_youtube',
            id_category = '$category'
            WHERE id_innovation  = $id
            ";
        }
        else {
            $sql_innovation = "UPDATE innovation_data set 
            photo = '$gen_uuid',
            $pdf_str
            title_first = '$title_first',
            name = '$name',
            school =  '$school',
            color = '$color',
            title_content = '$title_content',
            detail_content = '$summernote',
            link_youtube = '$link_youtube',
            id_category = '$category'
            WHERE id_innovation  = $id
            ";
        }


    }
    elseif(in_array("data_video", array_keys($_FILES))){

        if(in_array("data_doc", array_keys($_FILES))) {
            $sql_innovation = "UPDATE innovation_data set 
            video = '$gen_uuid_data_video',
            $pdf_str
            title_first = '$title_first',
            file_doc = '$gen_uuid_data_doc',
            name = '$name',
            school =  '$school',
            color = '$color',
            title_content = '$title_content',
            detail_content = '$summernote',
            link_youtube = '$link_youtube',
            id_category = '$category'
            WHERE id_innovation  = $id
            ";
        }
        else {
            $sql_innovation = "UPDATE innovation_data set 
            video = '$gen_uuid_data_video',
            $pdf_str
            title_first = '$title_first',
            name = '$name',
            school =  '$school',
            color = '$color',
            title_content = '$title_content',
            detail_content = '$summernote',
            link_youtube = '$link_youtube',
            id_category = '$category'
            WHERE id_innovation  = $id
            ";
        }


    }
    else {
        if(in_array("data_doc", array_keys($_FILES))){
            $sql_innovation = 
            "UPDATE innovation_data set 
            title_first = '$title_first',
            name = '$name',
            $pdf_str
            file_doc = '$gen_uuid_data_doc',
            school =  '$school',
            color = '$color',
            title_content = '$title_content',
            detail_content = '$summernote',
            link_youtube = '$link_youtube',
            id_category = '$category'
            WHERE id_innovation  = $id
            ";
        }
        else {
            $sql_innovation = 
            "UPDATE innovation_data set 
            title_first = '$title_first',
            name = '$name',
            $pdf_str
            school =  '$school',
            color = '$color',
            title_content = '$title_content',
            detail_content = '$summernote',
            link_youtube = '$link_youtube',
            id_category = '$category'
            WHERE id_innovation  = $id
            ";
        }
    }



    
    $query =  mysqli_query($con, $sql_innovation); 
    if($query){
        // $fileTmpName  = $_FILES["json"]['tmp_name'];
        if(in_array("data_file", array_keys($_FILES))){
            move_uploaded_file($_FILES["data_file"]['tmp_name'],"../../backend/innovation_library/image/$gen_uuid");
        }
        if(in_array("data_video", array_keys($_FILES))){
            move_uploaded_file($_FILES["data_video"]['tmp_name'],"../../backend/innovation_library/video/$gen_uuid_data_video");
        }
        if(in_array("data_doc", array_keys($_FILES))){
            move_uploaded_file($_FILES["data_doc"]['tmp_name'],"../../backend/innovation_library/doc/$gen_uuid_data_doc");
        }

        if(in_array("data_doc_pdf", array_keys($_FILES))){
            move_uploaded_file($_FILES["data_doc_pdf"]['tmp_name'],"../../backend/innovation_library/doc_pdf/$gen_uuid_data_pdf");
        }
        
        $sql_list  ="SELECT * FROM innovation_data";
        $list = $con->query($sql_list) ;
        while($row = $list->fetch_assoc()) {
            $data_list[] = $row; 
        }
        $arr = array('status' => true,'data'=>$data_list);
    }
    else {
        $arr = array('status' => false,'sql'=>$sql_innovation);
    }
    echo json_encode($arr);
    // var_dump($_FILES["json"])
?>


