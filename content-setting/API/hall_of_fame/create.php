<?php 
	session_start();
	require_once('../../../condb.php');
	date_default_timezone_set("Asia/Bangkok");
    // var_dump($_POST); echo "<br/><br/>";
    // var_dump($_FILES);
    // exit();
    function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
    $name = $_FILES['photo']['name'];
    $name = explode(".",$name)[1];
    $gen_uuid = gen_uuid().".".$name;

    if(in_array("file_doc",array_keys($_FILES))){
        $name_doc = $_FILES['file_doc']['name'];
        $name_doc = explode(".",$name_doc)[1];
        $gen_uuid_doc = gen_uuid().".".$name_doc;
    }



    $name = $_POST['name'];
    $title = $_POST['title'];
    $position = $_POST['position'];
    $detail = $_POST['detail'];
    $title_page =$_POST['title_page'];
    move_uploaded_file($_FILES["photo"]['tmp_name'],"../../backend/Hall_of_fame/image/$gen_uuid");
    if(in_array("file_doc",array_keys($_FILES)))
    move_uploaded_file($_FILES["file_doc"]['tmp_name'],"../../backend/Hall_of_fame/doc/$gen_uuid_doc");
    
    if(in_array("file_doc",array_keys($_FILES)))
    $sql_hall = "INSERT INTO `hall_of_fame` 
    (`id_hall_of_fame`, `file_name`, `name`, `position`, `detail`, `title`, `title_page` , `file_doc` , `create_at`, `update_at`) VALUES
     (NULL, '$gen_uuid', '$name', '$position', '$detail', '$title', '$title_page' ,'$gen_uuid_doc', current_timestamp(), current_timestamp());";
    else {
        $sql_hall = "INSERT INTO `hall_of_fame` 
        (`id_hall_of_fame`, `file_name`, `name`, `position`, `detail`, `title`, `title` , `create_at`, `update_at`) VALUES
         (NULL, '$gen_uuid', '$name', '$position', '$detail', '$title','$title_page', current_timestamp(), current_timestamp());";
    }

    $query =  mysqli_query($con, $sql_hall); 
    if($query){
        $age = array("status"=>true);
        echo json_encode($age);
    }
    else {
        $age = array("status"=>false);
        echo json_encode($age);
    }



?>