<?php
	session_start();
	require_once('../../condb.php');
	date_default_timezone_set("Asia/Bangkok");
    header('Access-Control-Allow-Origin: *');

    function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
    
            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),
    
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,
    
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,
    
            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    $name = $_FILES["json"]["name"];
    $name = explode(".",$name)[1];
    $gen_uuid = gen_uuid().".".$name;
    

    $size = $_FILES["json"]["size"];
    $type = $_FILES["json"]["type"];
    $type = explode("/",$type)[1];
    $id_project = $_POST['id_project'];
    

    $sql_banner = "INSERT INTO `banner` (`id_banner`, `file_name`, `size`, `format`,`id_project`,`update_at`, `create_at`) 
    VALUES (null, '$gen_uuid', $size, '$type' ,$id_project, current_timestamp(), current_timestamp())";
    $query =  mysqli_query($con, $sql_banner); 
    if($query){
        // $fileTmpName  = $_FILES["json"]['tmp_name'];
        move_uploaded_file($_FILES["json"]['tmp_name'],"../backend/banner/image/$gen_uuid");
        $sql_list  ="SELECT * FROM banner";
        $list = $con->query($sql_list) ;
        while($row = $list->fetch_assoc()) {
            $data_list[] = $row; 
        }
        $arr = array('status' => true,'data'=>$data_list);
    }
    else {
        $arr = array('status' => false,);
    }
    echo json_encode($arr);
    // var_dump($_FILES["json"])
?>
