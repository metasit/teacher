function into_create(){
  if(localStorage.getItem('production')=="1"){
    window.location.href=window.url+"Thaisuprateacher/content-setting/backend/galangal/create.html";   
  }
  else {
    window.location.href=window.url+"content-setting/backend/galangal/create.html";
  }
}

function into_home(){
  if(localStorage.getItem('production')=="1"){
    window.location.href=window.url+"Thaisuprateacher/content-setting/backend/galangal/";   
  }
  else {
    window.location.href=window.url+"content-setting/backend/galangal/";
  }
}



async function click_file(event){
    let element = event.target;
    element = element.previousSibling.previousSibling
    console.log(element,'element');
    element.click()
}

async function set_value_file(event){


  if(ReuseNormal.verifyFile(event,{},2,"")==false){
    alert("File มีขนาด เกิน 2 MB")
    return 
  }
    base64 = await ReuseNormal.base64_input(event)
    document.getElementById('data_message').src = base64;
    data_file = event.target.files[0];
    console.log(data_file);
    console.log(base64);
}

async function gal_home(){

  if(localStorage.getItem('production')=="1"){
    window.location.href=window.url+"Thaisuprateacher/content-setting/backend/galangal/";   
  }
  else {
    window.location.href=window.url+"content-setting/backend/galangal/";
  }
}

async function save_message(status){

    if(  new Date(document.getElementById('datepicker').value) > new Date() )
      {
        alert("ใส่วันเริ่มกิจกรรมไม่ถูกต้อง");
        return
      }
    let ck = 0;
    if(!document.getElementById('title').value){
      ReuseNormal.add_class(document.getElementById('title'),'is-invalid');
      ck = 1;
    }
    if(!document.getElementById('datepicker').value   ){
      ReuseNormal.add_class(document.getElementById('datepicker'),'is-invalid');
      ck = 1;
    }
    if(!data_file   ){
      ReuseNormal.remove_class(document.getElementById('img-photo'),'d-none');
      ck = 1;
    }
    if(!document.getElementById('text-message').value){
      ReuseNormal.add_class(document.getElementById('text-message'),'is-invalid');
      ck = 1;
    }
    
    var htmlContent = $('#summernote').summernote('code');
    var plainText = $(htmlContent).text();;
    if(!plainText){
      ReuseNormal.remove_class(document.getElementById('detailx'),'d-none');
      ck = 1;
    }
    
    if(ck==1){
      return 
    }

    let sp1 = document.getElementById('sp1');
    let btn_sp1 = document.getElementById('btn_sp1');
    ReuseNormal.remove_class(sp1,'d-none');
    btn_sp1.disabled = true;

    let day =   document.getElementById('datepicker').value.split("/")[1];
    let month = document.getElementById('datepicker').value.split("/")[0];
    let year = document.getElementById('datepicker').value.split("/")[2];

    let day_event = year+"/"+month+"/"+day;
    var formData = new FormData();   
    formData.set('title', document.getElementById('title').value);
    if(data_file)
    formData.set('photo', data_file);
    
    formData.set('detail', $('#summernote').summernote('code'));
    formData.set('detail_first', document.querySelector('#text-message').value);
    formData.set('day_event',day_event);
    console.log(formData,'formData');
    // console.log(document.getElementById('title').value);
    // console.log(data_file);
    // console.log($('#summernote').summernote('code'));

    let url_token 
    if(localStorage.getItem('production')=="1") {
       url_token = `${window.url}Thaisuprateacher/content-setting/API/galangal/create.php`
    }
    else {
       url_token = `${window.url}content-setting/API/galangal/create.php`
    }
    fetch(url_token, {
        method:"POST",
      //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
        body:formData
    })
      .then(function(response) {
        return response.json();
      })
      .then((value)=>{
        console.log(value.status,'value');
        if(value.status == false ){
            // console.log('FALSE');
            alert("รูปภาพเนื้อหามีขนาดใหญ่เกินไป ไม่สามารถ upload ได้")
            ReuseNormal.add_class(sp1,'d-none');
            btn_sp1.disabled = false;
          return

        }  
        if(value.status){
            Swal.fire(
                'เพิ่มข้อมูลข่าวสารสำเร็จ',
                '',
                'success'
            ).then((value)=>{
                  gal_home()
            })
          }
      })
      .catch(function(error) {
        console.log("Request failed", error);
      });
}


 function  cover_date(str_date){
    let day = str_date.split("/")[2];
    let month = str_date.split("/")[1];
    let year = str_date.split("/")[0];
    return day+"/"+month+"/"+year;
}

async function into_update(id){
  // window.location.href = window.url+"Thaisuprateacher/content-setting/backend/galangal/update.html?id="+id;
}
function update_welcom(id){
  if(localStorage.getItem('production')=="1"){
    window.location.href=window.url+"Thaisuprateacher/content-setting/backend/galangal/update.html?id="+id;   
  }
  else {
    window.location.href=window.url+"content-setting/backend/galangal/update.html?id="+id;
  }
}

async function message_list (){
  // console.log('message_list');
    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/galangal/message_list.php`
    }
    else {
      url_token = `${window.url}content-setting/API/galangal/message_list.php`
    }

    fetch(url_token, {
      method:"POST"
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        let message_table = document.getElementById('message_list')
        message_table.innerHTML = ""
        let str = ""
        value.data.forEach((value,index) => {
            str = "<tr>";
            str +=`<td>${index+1}</td>`;
            str +=`<td>${value.title}</td>`;
            str +=`<td><img style="object-fit:cover;" width="80" height="80" src="../galangal/image/${value.file_name}" /></td>`;
            str +=`<td>${cover_date(value.day_event.split(" ")[0].split("-").join("/")) }</td>`;
            str +=`<td    class="text-right">
            
            <button onclick="update_welcom(${value.id_message})" class="btn btn-primary mr-2">
              Update 
            </button>
            <button  
            data-toggle="modal" data-target="#exampleModal"
            onclick="SetId_delete(${value.id_message})" class="btn btn-danger" >Delete</button>
            </td>`;    
            str +="</tr>";
            message_table.innerHTML += str;
        });
        
    }).catch(function(error) {
      console.log("Request failed", error);
    });
}
async function SetId_delete(id){
  // console.log('SetId');
  console.log(id);
  id_value = id
}

async function delete_message(){
  console.log(id_value,'id_value');
  var formData = new FormData();
  formData.set('id', id_value);

  let url_token 
  if(localStorage.getItem('production')=="1") {
    url_token = `${window.url}Thaisuprateacher/content-setting/API/galangal/delete.php`
  }
  else {
    url_token = `${window.url}content-setting/API/galangal/delete.php`
  }

  fetch(url_token, {
    method:"POST",
    body:formData
  })
  .then(function(response) {
    return response.json();
  })
  .then((value)=>{
      let message_table = document.getElementById('message_list')
      message_table.innerHTML = ""
      let str = ""
      if(ReuseNormal.ckvalue(value.data))
      value.data.forEach((value,index) => {
          str = "<tr>";
          str +=`<td>${index+1}</td>`;
          str +=`<td>${value.title}</td>`;
          
          str +=`<td><img  style="object-fit:cover;" width="80" height="80" src="../galangal/image/${value.file_name}" /></td>`;
          str +=`<td>${value.day_event}</td>`;
          str +=`<td    class="text-right">
        <button onclick="update_welcom(${value.id_message})" class="btn btn-primary mr-2">
          Update
        </button>
          <button  
          data-toggle="modal" data-target="#exampleModal"
          onclick="SetId_delete(${value.id_message})" class="btn btn-danger" >Delete</button>
          </td>`;    
          str +="</tr>";
          message_table.innerHTML += str;
      });
      document.getElementById('popup-message').click()
  }).catch(function(error) {
    console.log("Request failed", error);
  });
}

 function set_value_update(value){
  document.getElementById('title').value = value.title;
  let day = value.day_event.split(" ")[0].split("-").join("/").split("/")[2]
  let month = value.day_event.split(" ")[0].split("-").join("/").split("/")[1]
  let year = value.day_event.split(" ")[0].split("-").join("/").split("/")[0]

  document.getElementById('datepicker').value =  month+"/"+day+"/"+year
  // value.day_event.split(" ")[0].split("-").join("/");
  document.getElementById('data_message').src = "image/"+value.file_name;
  document.getElementById('text-message').value = value.detail_first;
  $('#summernote').summernote('code',value.Detail);
}


async function update_message(status){
 
  if(  new Date(document.getElementById('datepicker').value) > new Date() )
  {
    alert("ใส่วันเริ่มกิจกรรมไม่ถูกต้อง");
    return
  }

  let ck = 0;
  if(!document.getElementById('title').value){
    ReuseNormal.add_class(document.getElementById('title'),'is-invalid');
    ck = 1;
  }
  if(!document.getElementById('datepicker').value  || status != true ){
    ReuseNormal.add_class(document.getElementById('datepicker'),'is-invalid');
    ck = 1;
    console.log(document.getElementById('datepicker').value,'picker');
  }
  if(!data_file && status !=true ){
    ReuseNormal.remove_class(document.getElementById('img-photo'),'d-none');
    ck = 1;
  }
  if(!document.getElementById('text-message').value){
    ReuseNormal.add_class(document.getElementById('text-message'),'is-invalid');
    ck = 1;
  }

  let sp1 = document.getElementById('sp1');
  let btn_sp1 = document.getElementById('btn_sp1');
  ReuseNormal.remove_class(sp1,'d-none');
  btn_sp1.disabled = true;
  
  var htmlContent = $('#summernote').summernote('code');
  var plainText = $(htmlContent).text();
  console.log(plainText,'plainText');
  // console.log($('#summernote').summernote('code'));
  if(!plainText){
    ReuseNormal.remove_class(document.getElementById('detailx'),'d-none');
    ck = 1;
  }
  
  if(ck==1){
    return 
  }

  let day =   document.getElementById('datepicker').value.split("/")[1];
  let month = document.getElementById('datepicker').value.split("/")[0];
  let year = document.getElementById('datepicker').value.split("/")[2];

  let day_event = year+"-"+month+"-"+day;
  var formData = new FormData();   
    formData.set('title', document.getElementById('title').value);
  if(data_file){
    formData.set('photo', data_file);
  }
  else {
    formData.set('photo',"1");
  }
  formData.set('detail', $('#summernote').summernote('code'));
  formData.set('detail_first', document.querySelector('#text-message').value);
  formData.set('day_event',day_event);
  formData.set('id',new URLSearchParams(window.location.search).get('id'));
  // console.log(document.getElementById('title').value);
  // console.log(data_file);
  // console.log($('#summernote').summernote('code'));

  let url_token 
  if(localStorage.getItem('production')=="1") {
    url_token = `${window.url}Thaisuprateacher/content-setting/API/galangal/update.php`
  }
  else {
    url_token = `${window.url}content-setting/API/galangal/update.php`
  }
  fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
  })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{

        if(value.status==false){
          
          alert("รูปภาพเนื้อหามีขนาดใหญ่เกินไป ไม่สามารถ upload ได้")
          ReuseNormal.add_class(sp1,'d-none');
          btn_sp1.disabled = false;
          return

        }

        if(value.status){
          Swal.fire(
              'เพิ่มข้อมูลข่าวสารสำเร็จ',
              '',
              'success'
            ).then((value)=>{
                gal_home()
            })
        }
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}

