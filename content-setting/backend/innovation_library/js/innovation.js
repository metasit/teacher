function into_create_category(){
  if(localStorage.getItem('production')=="1"){
    window.location.href =  window.url+"Thaisuprateacher/content-setting/backend/innovation_library/index_category.html"   
  }
  else {
    window.location.href =  window.url+"content-setting/backend/innovation_library/index_category.html"
  }
}


function into_create_category_data(){
  if(localStorage.getItem('production')=="1"){
    window.location.href =  window.url+"Thaisuprateacher/content-setting/backend/innovation_library/create_category.html"
  }
  else {
    window.location.href =  window.url+"content-setting/backend/innovation_library/create_category.html"
  }
}

function into_home_innovation(){
  if(localStorage.getItem('production')=="1"){
    window.location.href =  window.url+"Thaisuprateacher/content-setting/backend/innovation_library/"
  }
  else {
    window.location.href =  window.url+"content-setting/backend/innovation_library/"
  }
}

function into_update_category(id){
  if(localStorage.getItem('production')=="1"){
      window.location.href = window.url+"Thaisuprateacher/content-setting/backend/innovation_library/update_category.html?id="+id
  }
  else {
      window.location.href = window.url+"content-setting/backend/innovation_library/update_category.html?id="+id
  }
}

function into_create_innovation(){
  if(localStorage.getItem('production')=="1"){
    window.location.href = window.url+"Thaisuprateacher/content-setting/backend/innovation_library/create_innovation.html"
  }
  else {
    window.location.href = window.url+"content-setting/backend/innovation_library/create_innovation.html"
  }
  // window.location.href = window.url+"Thaisuprateacher/content-setting/backend/innovation_library/create_innovation.html"
}

function into_update(id){
  if(localStorage.getItem('production')=="1"){
    window.location.href = window.url+"Thaisuprateacher/content-setting/backend/innovation_library/update_innovation.html?id="+id
  }
  else {
    window.location.href = window.url+"content-setting/backend/innovation_library/update_innovation.html?id="+id
  }
  // window.location.href = window.url+"Thaisuprateacher/content-setting/backend/innovation_library/update_innovation.html?id="+id
}



function list_category_data(){

  let url_token 
  if(localStorage.getItem('production')=="1") {
    url_token = `${window.url}Thaisuprateacher/content-setting/API/category_innovation/list_category.php`
  }
  else {
    url_token = `${window.url}content-setting/API/category_innovation/list_category.php`
  }

    fetch(url_token, {
        method:"POST",
      //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      //   body:{key:value}
      })
      .then(function(response) {
        return response.json();
      })
      .then((value)=>{
            let str = ""
            data_category = value.data_list.map((content,index)=>{
                return  {
                    id:content.id_category_innovation,
                    name:content.name_category
                }
            }) 
            let category = document.getElementById('category');
            data_category.forEach(data => {
                str = `<option value='${data.id}' >${data.name}</option>`;
                category.innerHTML += str;
            });
      })
      .catch(function(error) {
        console.log("Request failed", error);
      });


}

function list_category(){


    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/category_innovation/list_category.php`
    }
    else {
      url_token = `${window.url}content-setting/API/category_innovation/list_category.php`
    }

    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
    //   body:{key:value}
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        let message_table = document.getElementById('category_name')
        message_table.innerHTML = ""
        let str = ""
        value.data_list.forEach((value,index) => {
            str = "<tr>";
            str +=`<td class="text-left">${index+1}</td>`;
            str +=`<td>${value.name_category}</td>`;
            str +=`<td    class="text-right">
            <button onclick="into_update_category(${value.id_category_innovation})" class="btn btn-primary mr-2">
              Update
            </button>
            <button  
            data-toggle="modal" data-target="#exampleModal"
            onclick="SetId_delete(${value.id_category_innovation })" class="btn btn-danger" >Delete</button>
            </td>`;    
            str +="</tr>";
            message_table.innerHTML += str;
        });
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}
function save_category(){
    if(!document.getElementById('name_category').value){
      ReuseNormal.add_class(document.getElementById('name_category'),'is-invalid');
      return
    }

    let sp1 = document.getElementById('sp1');
    let btn_sp1 = document.getElementById('btn_sp1');
    ReuseNormal.remove_class(sp1,'d-none');
    btn_sp1.disabled = true;

   let name = document.getElementById('name_category').value
   var formData = new FormData();
   formData.append('name', name);

    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/category_innovation/create.php`
    }
    else {
      url_token = `${window.url}content-setting/API/category_innovation/create.php`
    }
    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        if(value.status){
            if(value.status){
                Swal.fire(
                    'เพิ่มข้อมูลสำเร็จ',
                    '',
                    'success'
                ).then((value)=>{
                    into_create_category()
                })
            }
        }
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}

function SetId_delete(id){
    id_value = id;
}

function delete_category(){
    var formData = new FormData();
    formData.append('id',id_value);


    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/category_innovation/delete.php`
    }
    else {
      url_token = `${window.url}content-setting/API/category_innovation/delete.php`
    }
    fetch(url_token, {
      method:"POST",
      //headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{

        let message_table = document.getElementById('category_name')
        message_table.innerHTML = ""
        let str = ""
        if(ReuseNormal.ckvalue(value.data_list)){        
          value.data_list.forEach((value,index) => {
              str = "<tr>";
              str +=`<td class="text-left">${index+1}</td>`;
              str +=`<td>${value.name_category}</td>`;
              str +=`<td    class="text-right">
              <button onclick="into_update(${value.id_message})" class="btn btn-primary mr-2">
                Update
              </button>
              <button  
              data-toggle="modal" data-target="#exampleModal"
              onclick="SetId_delete(${value.id_category_innovation })" class="btn btn-danger" >Delete</button>
              </td>`;    
              str +="</tr>";
              message_table.innerHTML += str;
          });
        }
        document.getElementById('close-popup-category').click();
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });

}

function get_data_category(){
    let id =  new URLSearchParams(window.location.search).get('id')
    var formData = new FormData();
    formData.append('id',id);

    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/category_innovation/list_category.php`
    }
    else {
      url_token = `${window.url}content-setting/API/category_innovation/list_category.php`
    }
    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        console.log(value,'value');
        document.getElementById('name_category').value = value.data_list[0]['name_category'] 
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });    
}


function update_category(){

    if(!document.getElementById('name_category').value){
      ReuseNormal.add_class(document.getElementById('name_category'),'is-invalid');
      return
    }
    let sp1 = document.getElementById('sp1');
    let btn_sp1 = document.getElementById('btn_sp1');
    ReuseNormal.remove_class(sp1,'d-none');
    btn_sp1.disabled = true;
    let name = document.getElementById('name_category').value
    var formData = new FormData();
    formData.append('name', name);
    formData.append('id', new URLSearchParams(window.location.search).get('id'));

    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/category_innovation/update.php`
    }
    else {
      url_token = `${window.url}content-setting/API/category_innovation/update.php`
    }
    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        console.log(value);
        if(value.status){
            Swal.fire(
                'แก้ไขข้อมูลสำเร็จ',
                '',
                'success'
              ).then((value)=>{
                into_create_category()
              })
          }
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}


function click_file(event){
    event.currentTarget.previousSibling.previousSibling.click()
}

async function set_file_photo(event){
  if(ReuseNormal.verifyFile(event,{},2,"")==false){
    alert("File มีขนาด เกิน 2 MB")
    return 
  }

    base64 = await ReuseNormal.base64_input(event)
    document.getElementById('photo').src = base64;
    data_file = event.target.files[0];
    console.log(data_file);
    console.log(base64);
}

function  click_video(event){
    event.currentTarget.previousSibling.previousSibling.click()
}

 function set_file_video(event){

  if(ReuseNormal.verifyFile(event,{},100,"")==false){
    alert("File มีขนาด เกิน 100 MB")
    return 
  }

    data_video = event.currentTarget.files[0];
    // console.log(data_video);
    ReuseNormal.remove_class(document.getElementById('update_success_video'),'d-none')
}
 function setdoc1(event){
  if(ReuseNormal.verifyFile(event,{},10,"")==false){
    alert("File มีขนาด เกิน 10 MB")
    return 
  }
  data_doc = event.target.files[0];
  ReuseNormal.remove_class(document.getElementById('update_success'),'d-none')
}


function setdoc2(event){
  console.log('setdoc2');
  if(ReuseNormal.verifyFile(event,{},10,"")==false){
    alert("File มีขนาด เกิน 10 MB")
    return 
  }
  data_doc_pdf = event.target.files[0];
  console.log(data_doc_pdf,'data_doc_pdf');
  ReuseNormal.remove_class(document.getElementById('update_success_pdf'),'d-none')
}



function save_innovation(){
    
    let name = document.getElementById('name').value;
    let title_first = document.getElementById('title_first').value
    let school = document.getElementById('school').value
    let category = document.getElementById('category').value
    let color = document.getElementById('color').value
    let title_content = document.getElementById('title_content').value
    let summernote = $('#summernote').summernote('code');
    let link_youtube = document.getElementById('link_youtube').value

    // photo-img
    // sumer_detail
    //id="video-img"



      let ck = 0;

      if(!category){
        ReuseNormal.add_class(document.getElementById('category'),'is-invalid');
        ck = 1;        
      }
  
      if(!name){
        ReuseNormal.add_class(document.getElementById('name'),'is-invalid');
        ck = 1;
      }

      if(!title_first){
        ReuseNormal.add_class(document.getElementById('title_first'),'is-invalid');
        ck = 1;
      }

      if(!school){
        ReuseNormal.add_class(document.getElementById('school'),'is-invalid');
        ck = 1;
      }
      if(!color){
        ReuseNormal.add_class(document.getElementById('color'),'is-invalid');
        ck = 1;
      }
      if(!title_content){
        ReuseNormal.add_class(document.getElementById('title_content'),'is-invalid');
        ck = 1;
      }
      // if(!link_youtube){
      //   ReuseNormal.add_class(document.getElementById('link_youtube'),'is-invalid');
      //   ck = 1;
      // }
      if(!data_file){
        ReuseNormal.remove_class(document.getElementById('photo-img'),'d-none');
        ck = 1;
      }

      var htmlContent = $('#summernote').summernote('code');
      var plainText = $(htmlContent).text();;
      if(!plainText){
        ReuseNormal.remove_class(document.getElementById('sumer_detail'),'d-none');
        ck = 1;
      }

      // if(!data_video){
      //   ReuseNormal.remove_class(document.getElementById('video-img'),'d-none');
      //   ck = 1;
      // }
      try {
        if(!data_doc){
          console.log('doc_file_success');
          ReuseNormal.remove_class(document.getElementById('doc-file'),'d-none');
          ck = 1;
        }        
      } catch (e) {
        
      }

      if(ck==1){
        return
      }

      let sp1 = document.getElementById('sp1');
      let btn_sp1 = document.getElementById('btn_sp1');
      ReuseNormal.remove_class(sp1,'d-none');
      btn_sp1.disabled = true;

    // console.log(name,'name');
    // console.log(title_first,'title_first');
    // console.log(school,'school');
    // console.log(category,'category');
    // console.log(color,'color');
    // console.log(title_content,'title_content');
    // console.log(summernote,'summernote');
    // console.log(link_youtube,'link_youtube');
    // console.log(data_file,'data_file');
    // console.log(data_video,'data_video');


    var formData = new FormData();
    formData.append('name',name);
    formData.append('title_first',title_first);
    formData.append('school',school);
    formData.append('category',category);
    formData.append('color',color);
    formData.append('title_content',title_content);
    formData.append('summernote',summernote);

    
    if(data_doc_pdf){
      formData.append('data_doc_pdf',data_doc_pdf);  
    }

    if(ReuseNormal.ckvalue(link_youtube))
    formData.append('link_youtube',link_youtube);

    formData.append('data_file',data_file);
    console.log(data_video);
    console.log(ReuseNormal.ckvalue(data_video),'FALSE');
    if(data_video)
    formData.append('data_video',data_video);

    formData.append('data_doc', data_doc);
    // console.log(data_doc,'data_doc');
    
    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/innovation/create.php`
    }
    else {
      url_token = `${window.url}content-setting/API/innovation/create.php`
    }

  fetch(url_token, {
    method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
    body:formData
    })
    .then(function(response) {
    return response.json();
    })
    .then((value)=>{
        console.log(value,'value');

        if(value.status==false){
          alert("รูปภาพเนื้อหามีขนาดใหญ่เกินไป ไม่สามารถ upload ได้")
          let sp1 = document.getElementById('sp1');
          let btn_sp1 = document.getElementById('btn_sp1');
          ReuseNormal.add_class(sp1,'d-none');
          btn_sp1.disabled = false;
          return
        }
        

        if(value.status){
            Swal.fire(
                'เพิ่มข้อมูลสำเร็จ',
                '',
                'success'
            ).then((value)=>{
                into_home_innovation()
            })
        }
    })
    .catch(function(error) {
        console.log("Request failed", error);
    });
}

function list_innovation_data(){
    var formData = new FormData();
    formData.append('id',new URLSearchParams(window.location.search).get('id'));
    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/innovation/innovation_list.php`
    }
    else {
      url_token = `${window.url}content-setting/API/innovation/innovation_list.php`
    }
    fetch(url_token, {
        method:"POST",
      //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
        body:formData
      })
      .then(function(response) {
        return response.json();
      })
      .then((value)=>{
            console.log(value.data[0],'value');
            let token = value.data[0]
            document.getElementById('name').value = token.name;
            document.getElementById('title_first').value = token.title_first;
            document.getElementById('school').value = token.school;
            document.getElementById('category').value = token.id_category
            document.getElementById('photo').src = "image/"+token.photo
            document.getElementById('color').value = token.color;
            document.getElementById('title_content').value = token.title_content;
            $('#summernote').summernote('code',token.detail_content);
            document.getElementById('link_youtube').value = token.link_youtube;
            document.getElementById('open_doc') .setAttribute('href', "doc/"+token.file_doc);

      })
      .catch(function(error) {
        console.log("Request failed", error);
      }); 
}



function update_innovation(status){
    // document.getElementById('photo').src = "image/"+token.photo
    // $('#summernote').summernote('code',token.detail_content);

    let name = document.getElementById('name').value;
    let title_first = document.getElementById('title_first').value
    let school = document.getElementById('school').value
    let category = document.getElementById('category').value
    let color = document.getElementById('color').value
    let title_content = document.getElementById('title_content').value
    // let summernote = $('#summernote').summernote('code');
    // let link_youtube = document.getElementById('link_youtube').value

    let ck = 0;

    console.log(category,'update_innovation');
  // console.log('');
    if(!category){
      ReuseNormal.add_class(document.getElementById('category'),'is-invalid');
      ck = 1;        
    }

    if(!name){
      ReuseNormal.add_class(document.getElementById('name'),'is-invalid');
      ck = 1;
    }

    if(!title_first){
      ReuseNormal.add_class(document.getElementById('title_first'),'is-invalid');
      ck = 1;
    }

    if(!school){
      ReuseNormal.add_class(document.getElementById('school'),'is-invalid');
      ck = 1;
    }
    if(!color){
      ReuseNormal.add_class(document.getElementById('color'),'is-invalid');
      ck = 1;
    }
    if(!title_content){
      ReuseNormal.add_class(document.getElementById('title_content'),'is-invalid');
      ck = 1;
    }
    // if(!link_youtube){
    //   ReuseNormal.add_class(document.getElementById('link_youtube'),'is-invalid');
    //   ck = 1;
    // }
    if(!data_file && status!=true){
      ReuseNormal.remove_class(document.getElementById('photo-img'),'d-none');
      ck = 1;
    }

    var htmlContent = $('#summernote').summernote('code');
    var plainText = $(htmlContent).text();;
    if(!plainText){
      ReuseNormal.remove_class(document.getElementById('sumer_detail'),'d-none');
      ck = 1;
    }

    if(!data_video && status!= true){
      ReuseNormal.remove_class(document.getElementById('video-img'),'d-none');
      ck = 1;
    }
    if(ck==1){
      return
    }

    let sp1 = document.getElementById('sp1');
    let btn_sp1 = document.getElementById('btn_sp1');
    ReuseNormal.remove_class(sp1,'d-none');
    btn_sp1.disabled = true;



    var formData = new FormData();
    formData.append('id', new URLSearchParams(window.location.search).get('id'));
    formData.append('name', document.getElementById('name').value);
    formData.append('school', document.getElementById('school').value);
    formData.append('title_first', document.getElementById('title_first').value);
    formData.append('category', document.getElementById('category').value);
    formData.append('color', document.getElementById('color').value);
    formData.append('title_content', document.getElementById('title_content').value);
    formData.append('summernote', $('#summernote').summernote('code'));
    formData.append('link_youtube', document.getElementById('link_youtube').value);
    formData.append('data_file', data_file);
    formData.append('data_video', data_video);
    formData.append('data_doc', data_doc);
    console.log(data_doc_pdf,'data_doc_pdf');

    if(data_doc_pdf){
      console.log('success');
      formData.append('data_doc_pdf', data_doc_pdf);
    }
    

    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/innovation/update.php`
    }
    else {
      url_token = `${window.url}content-setting/API/innovation/update.php`
    }

    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        // console.log(value,'value');
        if(value.status==false){
          alert("รูปภาพเนื้อหามีขนาดใหญ่เกินไป ไม่สามารถ upload ได้")
          let sp1 = document.getElementById('sp1');
          let btn_sp1 = document.getElementById('btn_sp1');
          ReuseNormal.add_class(sp1,'d-none');
          btn_sp1.disabled = false;
        }
        
        if(value.status){
            Swal.fire(
                'Updateข้อมูลสำเร็จ',
                '',
                'success'
              ).then((value)=>{
                into_home_innovation()
              })
          }
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });

}
// var x = document.getElementsByTagName('H1')[0].getAttribute('style');
document.getElementById('modal-addshop').style.zIndex = '2000 !important';

function remove_link_youtube(id,status){

  Swal.fire({
    title: 'ยืนยันลบ',
    text: "",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/innovation/update_empty_link.php`
    }
    else {
      url_token = `${window.url}content-setting/API/innovation/update_empty_link.php`
    }
    var formData = new FormData();
    formData.append('id',id);
    formData.append('status', status);
      fetch(url_token, {
          method:"POST",
          body:formData
      })
      .then(function(response) {
        return response.json();
      })
      .then((value)=>{
        console.log(id,'id');
        if (result.isConfirmed) {
          Swal.fire(
            'ลบ Link You Tube สำเร็จ!',
          )
        }
      })
      .catch(function(error) {
        console.log("Request failed", error);
      });
  })
}


function list_innovation(){


    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/innovation/innovation_list.php`
    }
    else {
      url_token = `${window.url}content-setting/API/innovation/innovation_list.php`
    }

    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
    //   body:{key:value}
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        let message_table = document.getElementById('list-innovation')
        message_table.innerHTML = ""
        let str = ""
        value.data.forEach((value,index) => {
            str = "<tr>";
            str +=`<td class="text-left">${index+1}</td>`;
            str +=`<td>${value.name}</td>`;
            str +=`<td><img style="object-fit:cover; max-height:80px; max-width:100px;  " width="100"  src="image/${value.photo}" /></td>`;
            str +=`<td>${value.school}</td>`;
            str +=`<td>${(value.name_category == null ? 'ไม่อยู่ในหมวดหมู่ใด' : value.name_category)}</td>`;
            str +=`<td>${value.title_first}</td>`;
            str +=`<td>${value.title_content}</td>`;
            str +=`<td> <a target="_blank" href="${value.link_youtube}" > ${value.link_youtube}</a> </td>`;
            str +=`<td    class="text-right">
            <button onclick="into_update(${value.id_innovation})" class="btn btn-primary mr-2">
              Update
            </button>

            <button  
                data-toggle="modal" data-target="#exampleModal"
                onclick="SetId_delete(${value.id_innovation })" class="btn btn-danger" >
                Delete
            </button>
            <button onclick='remove_link_youtube(${value.id_innovation},${"1"})' class="btn btn-success mr-2">ลบ Link Youtube </button>
            <button onclick='remove_link_youtube(${value.id_innovation},${"2"})' class="btn btn-success mr-2">ลบ Clip บน Server </button>
            <button onclick='remove_link_youtube(${value.id_innovation},${"3"})'  class="btn btn-success mr-2">ลบ File PDF </button>

            </td>`;    
            str +="</tr>";
            message_table.innerHTML += str;
        });

    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}

function SetId_delete(id){
    id_value = id;
}

function delete_innovation(){
    var formData = new FormData();
    formData.append('id', id_value);
    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/innovation/delete.php`
    }
    else {
      url_token = `${window.url}content-setting/API/innovation/delete.php`
    }
    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        console.log(value,'value');
        document.getElementById('close-popup-innovation').click();
        let message_table = document.getElementById('list-innovation')
        message_table.innerHTML = ""
        let str = ""
        if(ReuseNormal.ckvalue(value.data))
        value.data.forEach((value,index) => {
          str = "<tr>";
          str +=`<td class="text-left">${index+1}</td>`;
          str +=`<td>${value.name}</td>`;
          str +=`<td><img style="object-fit:cover; max-height:80px; max-width:100px;  " width="100"  src="image/${value.photo}" /></td>`;
          str +=`<td>${value.school}</td>`;
          str +=`<td>${(value.name_category == null ? 'ไม่อยู่ในหมวดหมู่ใด' : value.name_category)}</td>`;
          str +=`<td>${value.title_first}</td>`;
          str +=`<td>${value.title_content}</td>`;
          str +=`<td> <a target="_blank" href="${value.link_youtube}" > ${value.link_youtube}</a> </td>`;
          str +=`<td    class="text-right">
          <button onclick="into_update(${value.id_innovation})" class="btn btn-primary mr-2">
            Update
          </button>

          <button  
              data-toggle="modal" data-target="#exampleModal"
              onclick="SetId_delete(${value.id_innovation })" class="btn btn-danger" >
              Delete
          </button>
          <button onclick='remove_link_youtube(${value.id_innovation},${"1"})' class="btn btn-success mr-2">ลบ Link Youtube </button>
          <button onclick='remove_link_youtube(${value.id_innovation},${"2"})' class="btn btn-success mr-2">ลบ Clip บน Server </button>
          <button onclick='remove_link_youtube(${value.id_innovation},${"3"})'  class="btn btn-success mr-2">ลบ File PDF </button>

          </td>`;    
          str +="</tr>";
          message_table.innerHTML += str;
      });
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}

