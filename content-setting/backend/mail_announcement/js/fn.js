function into_create(){

  if(localStorage.getItem('production')=="1"){
    window.location.href =  window.url+"Thaisuprateacher/content-setting/backend/mail_announcement/create.html"
  }
  else {
    window.location.href =  window.url+"content-setting/backend/mail_announcement/create.html"
  }
   
}


function into_home(){
    if(localStorage.getItem('production')=="1"){
      window.location.href =  window.url+"Thaisuprateacher/content-setting/backend/mail_announcement/"
    }
    else {
      window.location.href =  window.url+"content-setting/backend/mail_announcement/"
    }
}


function into_update(id){
  if(localStorage.getItem('production')=="1"){
    window.location.href =  window.url+"Thaisuprateacher/content-setting/backend/mail_announcement/update.html?id="+id;
  }
  else {
    window.location.href =  window.url+"content-setting/backend/mail_announcement/update.html?id="+id;
  }    
}


function click_file(event){
    event.currentTarget.previousSibling.previousSibling.click();
  
}

async function set_file(event){
    // base64 = await ReuseNormal.base64_input(event)
    // document.getElementById('photo_letter').src = base64;

    if(ReuseNormal.verifyFile(event,{},2,"")==false){
      alert("File มีขนาด เกิน 2 MB")
      return 
    }

    data_file = event.target.files[0];
    ReuseNormal.remove_class(document.getElementById('upload_success'),'d-none')
}

function set_mail(event){
  status_mail = event.target.value;
  console.log('set_mail');
  // console.log(status_mail,'status_mail');
}

function save_letter(){

    let ck = 0;
    if(!document.getElementById('title').value){
      ReuseNormal.add_class(document.getElementById('title'),'is-invalid');
      ck = 1;
    }
    if(!document.getElementById('datepicker').value){
      console.log(document.getElementById('datepicker').value,'picker');
      ReuseNormal.remove_class(document.getElementById('warning-picker'),'d-none')
      ck = 1;
    }


    if(!data_file){
       ReuseNormal.remove_class(document.getElementById('photo-img'),'d-none')
       ck = 1;
    }



    if(ck==1){
      return
    }
    let sp1 = document.getElementById('sp1');
    let btn_sp1 = document.getElementById('btn_sp1');
    ReuseNormal.remove_class(sp1,'d-none');
    btn_sp1.disabled = true;

    // console.log(status_mail,'status_mail');
    
    // return
    

    var formData = new FormData();
    formData.append('title',document.getElementById('title').value);
    formData.append('photo',data_file);
    formData.append('status_mail', (ReuseNormal.ckvalue(status_mail) ? status_mail : ""));
    formData.append('day_open', document.getElementById('datepicker').value);
    let id_value 
    if(new URLSearchParams(window.location.search).get('id')){
      id_value  = new URLSearchParams(window.location.search).get('id')
      formData.append('id', id_value);
    }
    

    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/mail_announcement/create.php`
    }
    else {
      url_token = `${window.url}content-setting/API/mail_announcement/create.php`
    }
    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        console.log(value);
        if(value.status){
            console.log('success');
            Swal.fire(
                'เพิ่มข้อมูลสำเร็จ',
                '',
                'success'
            ).then((value)=>{
                into_home()
            })
          }
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}


function list_letter(){
    console.log('list_letter');

    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/mail_announcement/letter_list.php`
    }
    else {
      url_token = `${window.url}content-setting/API/mail_announcement/letter_list.php`
    }
    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
    //   body:{key:value}
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        console.log(value);
        if(value.status){

            let message_table = document.getElementById('table_letter')
            message_table.innerHTML = ""
            let str = ""
            value.data.forEach((value,index) => {
              let st = value.status
                if(st==1){
                  st = "New";
                }
                else if(st==2)  {
                  st = "Hot";
                }
                else {
                  st = "";
                }
                str = "<tr>";
                str +=`<td>${index+1}</td>`;
                str +=`<td>${value.title}</td>`;
                str +=`<td>${value.nick_name_pdf}</td>`;
                str +=`<td >
                <a  download href="image/${value.pdf_name}" >
                    Download File
                </a>
                </td>`;


                if(st) {
                  str +=`<td><button class="btn btn-success">${st}</button></td>`;
                }
                else {
                  str +=`<td></td>`;
                }

                // str +=`<td><button class="btn btn-success">${st}</button></td>`;
                str +=`<td class="font-weight-bold" >${value.day_open}</td>`;
                str +=`<td    class="text-right">
                <button onclick="into_update(${value.id_letter})" class="btn btn-primary mr-2">
                  Update
                </button>
                <button  
                data-toggle="modal" data-target="#exampleModal"
                onclick="SetId_delete(${value.id_letter})" class="btn btn-danger" >Delete</button>
                </td>`;    
                str +="</tr>";
                message_table.innerHTML += str;
            });

        }
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}

function SetId_delete(id){
    id_value = id;
    console.log(id_value);
}

function delete_letter(){
    var formData = new FormData();
    formData.append('id', id_value);

    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/mail_announcement/delete.php`
    }
    else {
      url_token = `${window.url}content-setting/API/mail_announcement/delete.php`
    }

    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        console.log(value);
        if(value.status){
          
       
          let message_table = document.getElementById('table_letter')
          message_table.innerHTML = ""
          let str = ""
          if(ReuseNormal.ckvalue(value.data))
          value.data.forEach((value,index) => {
            let st = value.status
              if(st==1){
                st = "New";
              }
              else if(st==2)  {
                st = "Hot";
              }
              else {
                st = "";
              }
              str = "<tr>";
              str +=`<td>${index+1}</td>`;
              str +=`<td>${value.title}</td>`;
              str +=`<td>${value.nick_name_pdf}</td>`;
              str +=`<td >
              <a  download href="image/${value.pdf_name}" >
                Download File
              </a>
              </td>`;


              if(st) {
                str +=`<td><button class="btn btn-success">${st}</button></td>`;
              }
              else {
                str +=`<td></td>`;
              }

              // str +=`<td><button class="btn btn-success">${st}</button></td>`;
              str +=`<td class="font-weight-bold" >${value.day_open}</td>`;
              str +=`<td    class="text-right">
              <button onclick="into_update(${value.id_letter})" class="btn btn-primary mr-2">
                Update
              </button>
              <button  
              data-toggle="modal" data-target="#exampleModal"
              onclick="SetId_delete(${value.id_letter})" class="btn btn-danger" >Delete</button>
              </td>`;    
              str +="</tr>";
              message_table.innerHTML += str;
          });

            document.getElementById('close-cancel').click();
        }
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}

function set_letter_value(){
    var formData = new FormData();
    formData.append('id', new URLSearchParams(window.location.search).get('id'));

    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/mail_announcement/letter_list.php`
    }
    else {
      url_token = `${window.url}content-setting/API/mail_announcement/letter_list.php`
    }
    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        console.log(value);
        if(value.data[0]['status']=="1"){
          console.log('true');
          document.getElementById('inlineRadio1').click();
        }
        else if(value.data[0]['status']=="2") {
          console.log('false');
          document.getElementById('inlineRadio2').click();
        }
        document.getElementById('title').value = value.data[0]['title'];
        document.getElementById('datepicker').value = value.data[0]['day_open']
        file_pdf_current = value.data[0]['pdf_name'];
        document.getElementById('open_pdf')
        .setAttribute("href", window.url+"Thaisuprateacher/content-setting/backend/mail_announcement/image/"+file_pdf_current);
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}

function letter_update(){
  console.log('letter_update');
  let ck = 0;
  if(!document.getElementById('title').value){
    ReuseNormal.add_class(document.getElementById('title'),'is-invalid');
    ck = 1;
  }

  if(!document.getElementById('datepicker').value){
    console.log(document.getElementById('datepicker').value,'picker');
    ReuseNormal.remove_class(document.getElementById('warning-picker'),'d-none')
    ck = 1;
  }

  if(ck==1){
    return
  }

  let sp1 = document.getElementById('sp1');
  let btn_sp1 = document.getElementById('btn_sp1');
  ReuseNormal.remove_class(sp1,'d-none');
  btn_sp1.disabled = true;
    var formData = new FormData();
    formData.append('id', new URLSearchParams(window.location.search).get('id'));
    formData.append('title', document.getElementById('title').value);
    formData.append('photo', data_file);
    formData.append('status', status_mail);
    formData.append('day_open',document.getElementById('datepicker').value);
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/mail_announcement/update.php`
    }
    else {
      url_token = `${window.url}content-setting/API/mail_announcement/update.php`
    }
    
    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        if(value.status){
            Swal.fire(
                'Updateข้อมูลสำเร็จ',
                '',
                'success'
              ).then((value)=>{
                into_home()
              })
        }
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}

