<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Label - Premium Responsive Bootstrap 4 Admin & Dashboard Template</title>
    <!-- plugins:css -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Prompt:wght@200&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/vendors/iconfonts/mdi/css/materialdesignicons.css">
    <!-- endinject -->
    <!-- vendor css for this page -->
    <!-- End vendor css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="../../assets/css/shared/style.css">
    <!-- endinject -->
    <!-- Layout style -->
    <link rel="stylesheet" href="../../assets/css/demo_1/style.css">
    <!-- Layout style -->
    <link rel="shortcut icon" href="../../asssets/images/favicon.ico" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <style>
      .header-fixed .sidebar{
        padding-top: 0px!important;
      }
      .page-body .page-content-wrapper{
        margin-top: 0px!important;
      }
      .navigation-menu{
        margin-top: 0px!important;
      }
      .nav-category-divider{
        font-size:15px!important;
      }
      .page-body .page-content-wrapper .page-content-wrapper-inner{
          margin-left: 0px!important;
          margin-right: 0px!important;
          max-width: 100% !important;
      }
      .page-content-wrapper{
        background: #e1ecf7;
      }
      #sample-pages::after{
        content:""!important;
      }
      .a-click{
          cursor: pointer;
      }
    </style>


  </head>
  <body class="header-fixed">
    <?php 
        session_start();
        require_once('../../../condb.php');
        date_default_timezone_set("Asia/Bangkok");

        



        if(in_array("ac", array_keys($_GET))){
            $ac = $_GET['ac'];
            $idx = $_POST['id'];
            $sql_update = " UPDATE login set status_officer  = 'ACTIVE'  WHERE ID = '$idx' ";
            $result_update = mysqli_query($con,$sql_update);
            echo "<script>alert('Active Account สำเร็จ'); window.location.href = 'confirm.php' </script>";
        }


        
        if(in_array("pg", array_keys($_GET))  ){
          if( $_GET['pg'] > 1  ) {
            $pg = $_GET['pg'];
            $start = (($pg-1) * 50) + 1;
            $end = $pg * 50;
          }
          else {
            $pg = 1;
            $start = $pg - 1;
            $end = 50*$pg ;           
          }
        }
        else {
            $pg = 1;
            $start = $pg - 1 ;
            $end = 50*$pg;
        }
        if(in_array("search", array_keys($_GET)) ){
            $search = $_POST['search'];
            $str_search = " AND (email like '%$search%' OR firstname like '%$search%' OR lastname like '%$search%'     ) ";
            $sql_list_account = "SELECT * FROM login WHERE status_people = 'ACTIVE'  AND code_people != ''    AND ( status_officer ='' OR status_officer is null ) $str_search order by ID desc   LIMIT $start,50     ";
            // var_dump($sql_list_account);
        }
        else {
            $sql_list_account = "SELECT * FROM login WHERE status_people = 'ACTIVE'  AND code_people != ''    AND ( status_officer ='' OR status_officer is null )  order by ID desc limit $start,50   ";
        }

        // $sql_list_account = "SELECT * FROM login WHERE status_people = 'ACTIVE'  AND code_people != '' AND   status_officer !='ACTIVE' ";
        $result = mysqli_query($con,$sql_list_account);
        
        $result_count_token = mysqli_query($con,$sql_list_account);
        $rowcount_token = mysqli_num_rows($result_count_token);

        $sql_list_account_count = "SELECT * FROM login WHERE status_people = 'ACTIVE'  AND code_people != ''  AND ( status_officer ='' OR status_officer is null ) ";
        $result_count = mysqli_query($con,$sql_list_account_count);
        $rowcount = mysqli_num_rows($result_count);

        $number_pg = ceil($rowcount / 50);
        // var_dump($rowcount);
        // exit();

  



    ?>
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">ยืนยันลบข้อมูล  Banner</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-right ">
          <button onclick="DeleteBanner()"  type="button" class="btn btn-danger">Delete</button>
          <button  id="close-banner" type="button" class="btn btn-secondary mr-2" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
    <!-- partial:partials/_header.html -->

    <!-- partial -->
    <div class="page-body">
      <!-- partial:partials/_sidebar.html -->
      <div class="sidebar">
        <ul class="navigation-menu">
          <li class="nav-category-divider font-weight-bold text-dark ">MAIN</li>
          <li class="active">
            <a href="#sample-pages" id="sample-pages" data-toggle="collapse" aria-expanded="true">
              <!-- <span class="link-title">Content</span> -->
              <!-- <i class="mdi mdi-flask link-icon"></i> -->
            </a>
            <ul id="menu_bar" class="collapse navigation-submenu show" id="sample-pages">
              <!-- <li>
                <a style="font-size:14px" href="" >Banner</a>
              </li>
              <li>
                <a style="font-size:14px" href="/Thaisuprateacher/content-setting/backend/galangal/" >ข่าวสาร</a>
              </li>
              <li>
                <a style="font-size:14px" href="/Thaisuprateacher/content-setting/backend/Hall_of_fame/" >หอเกียรติยศ</a>
              </li>
              <li>
                <a style="font-size:14px" href="/Thaisuprateacher/content-setting/backend/mail_announcement/" >ข้อมูลจดหมาย/ประกาศ</a>
              </li>
              <li>
                <a style="font-size:14px" href="/Thaisuprateacher/content-setting/backend/innovation_library/" >คลังสื่อนวัตกรรม</a>
              </li>
              <li>
                <a style="font-size:14px" href="/Thaisuprateacher/content-setting/backend/join_project/" >ร่วมโครงการ</a>
              </li> -->
              <li>
                <a style="font-size:14px" href="/dev/content-setting/backend/confirm_account/confirm.php" >Confirm Account</a>
              </li>
            </ul>
          </li>
          
          <!-- if(localStorage.getItem('production')=="1") {
            window.url = "http://localhost/";
          }
          else if(localStorage.getItem('production')=="2"){
            window.url = "http://www.thaisuprateacher.org/dev/";
          } -->

          <!-- <li class="nav-category-divider">DOCS</li> -->
          <!-- <li>
            <a href="../docs/docs.html">
              <span class="link-title">Documentation</span>
              <i class="mdi mdi-asterisk link-icon"></i>
            </a>
          </li> -->
        </ul>
        <!-- <div class="sidebar-upgrade-banner">
          <p class="text-gray">Upgrade to <b class="text-primary">PRO</b> for more exciting features</p>
          <a class="btn upgrade-btn" target="_blank" href="http://www.uxcandy.co/product/label-pro-admin-template/">Upgrade to PRO</a>
        </div> -->
      </div>
      <!-- partial -->
      <div class="page-content-wrapper">
        <div class="page-content-wrapper-inner">
          <div class="content-viewport">

              <div class="row">
                <div class="col-md-6">
                    <h1>Confirm Account</h1>
                </div>
                <!-- <div class="col-md-6 text-right">
                  <a href="create.html" class="btn btn-primary font-weight-bold">Create</a>
                </div> -->
              </div>

              <div class=" mt-5">
                  <div class="card text-center">
                      <div class="card-body">
                        
                        <!-- Modal -->

                        <div class="d-flex justify-content-end ">
                            
                            <div>
                            <form action="confirm.php?search=1" method="post">
                                <div>
                                        <input  name="search"  placeholder="Search" type="text" style=" width:200px; " class="form-control">
                                    </div>
                                    <div>
                                        <button class=" p-1 btn btn-primary mt-2" style="height:50px!important; width:200px; ">Search</button>
                                </div>
                            </form>

                            </div>
                            

                        </div>

                        <div class="table-responsive">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th  class="font-weight-bold" scope="col">Number</th>
                                <th  class="font-weight-bold" scope="col">คำนำหน้า</th>
                                <th  class="font-weight-bold" scope="col">ชื่อ</th>
                                <th  class="font-weight-bold" scope="col">นามสกุล</th>
                                <th  class="font-weight-bold" scope="col">รูปบัตรประชาชน</th>
                                <th  class="font-weight-bold" scope="col">เลขบัตรประชาชน</th>
                                <th  class="font-weight-bold" scope="col" >ตำแหน่ง</th>
                                <th  class="font-weight-bold" scope="col" >ระดับ Account</th>
                                <!-- <th  class="font-weight-bold" scope="col">email</th> -->
                                <th  class="font-weight-bold" width="20%" >ที่อยู่ หน่วย สังกัด</th>
                                <th  class="font-weight-bold" scope="col">email</th>
                                <th  class="font-weight-bold" scope="col">เบอร์โทร</th>
                                <th  class="font-weight-bold" scope="col" class="text-right ">ดูรูป</th>
                                <th  class="font-weight-bold" scope="col" class="text-right ">ตอบกลับ</th>
                              </tr>
                            </thead>

                            <tbody id="banner-table">
                                <?php
                                    function prename($str,$remark){
                                        if($remark) {
                                            return $remark;
                                        }
                                        else {
                                            if($str=="A") { return "นาย"; }
                                            else if($str=="B") { return "นาง"; }
                                            else if($str=="C") { return "นางสาว"; }
                                            else if($str=="D") { return "ด.ช."; }
                                            else if($str=="E") { return "ด.ญ."; }
                                            else if($str=="O") { return "อื่นๆ"; }
                                        }
                                    }
                                if($pg == 1){
                                    $number_token = 1;
                                }
                                else {
                                    $number_token = (($pg - 1) * 50) + 1;
                                }
                                    while($row =  $result->fetch_assoc()){
                                        
                                    
                                ?>
                                    <tr>
                                        <td class="text-center font-weight-bold"><?php  echo $number_token; ?></td>
                                        <td class="text-center font-weight-bold"><?php  echo prename($row['prename'],$row['prename_remark']); ?></td>
                                        <td class="text-center font-weight-bold"><?php  echo $row['firstname']; ?></td>
                                        <td class="text-center font-weight-bold"><?php  echo $row['lastname']; ?></td>
                                        <td class="text-center font-weight-bold"> 
                                            <img id="<?php  echo "img_".$row['ID'] ?>" class="a-click"  style="object-fit:cover;" src="<?php  echo "../../../images/photo_code_people/".$row['code_people'];  ?>" width="80" height="80" alt=""> </td>
                                            <td id="<?php  echo "code_persanal_".$row['ID'] ?>" class="text-center font-weight-bold"><?php  echo $row['code_persanal']; ?></td>
                                        <td 

                                            class=" text-left   font-weight-bold">
                                            <div   class=" text-wrap w-100 d-inline-block ">
                                                <?php  echo $row['affil_name']; ?>
                                            </div>
                                        </td>

                                        <td class="text-center font-weight-bold"><?php  echo $row['level']; ?></td>

                                        <td class="text-center font-weight-bold">
                                            <?php  
                                                // echo $row['occup_name'];
                                            ?>
                                            <div class=" text-wrap w-100 d-inline-block ">
                                                <?php  echo $row['occup_name']; ?>
                                            </div>
                                        </td>
                                        <td id="<?php  echo "email_".$row['ID'] ?>" class="text-center font-weight-bold">
                                            <?php  echo $row['email'] ?>
                                        </td>
                                        <td class="text-center font-weight-bold">
                                            <?php  echo $row['phonenum'] ?>
                                        </td>
                                        <td class="text-center font-weight-bold">
                                            <button id="<?php  echo "btn_".$row['ID'] ?>" class=" btnx btn btn-primary">บัตรประชาชน</button>
                                        </td>
                                        <td class="text-center font-weight-bold">
                                            <button id="<?php  echo "btn_reply_".$row['ID'] ?>" class=" btnx btn btn-success">ตอบกลับ</button>
                                        </td>
                                    </tr>


                            <?php
                                $number_token++;
                            }
                                
                            ?>
                              <!-- <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                              </tr> -->
                            </tbody>
                          </table>
                          <?php  
                            if(!isset($search)) {
                          ?>
                                <nav aria-label="...">
                                <ul class="pagination">
                                    <li class="<?php echo " page-item ";  if($pg == "1") echo "disabled" ?>  ">
                                        <a class="<?php echo " page-link ";     ?>" 
                                        href="<?php echo "confirm.php?pg=".($pg - 1)   ?>" tabindex="-1">Previous</a>
                                    </li>
                                    <?php 
                                    
                                    if($pg > 3){
                                        $st =  $pg - 2  ;
                                        $end = 5 + ($st - 1);
                                        if($end > $number_pg){
                                            $end = $number_pg;
                                        }
                                    }
                                    else { 
                                        $st = 1;
                                        $end = 5;
                                        if($number_pg < 5) {
                                          $end = $number_pg;
                                        }
                                    }
                                        for ($x = $st; $x <= $end; $x++) { 
                                    
                                    ?>
                                    <li class="<?php echo  " page-item "; if($pg == $x) echo "active"; ?>">
                                        <a class="<?php echo " page-link ";  ?>" href="<?php  echo "confirm.php?pg=".$x; ?>">
                                            <?php  echo $x; ?>
                                        </a>
                                    </li>

                                    <?php 
                                         }
                                    ?>

                                    <li class="<?php echo " page-item "; if($pg+1 > $number_pg) echo "disabled" ?>">
                                        <a class="page-link" 
                                        href="<?php 
                                            echo "confirm.php?pg=".($pg+1) 
                                        ?>">Next</a>
                                    </li>
                                </ul>
                            </nav>
                                         <?php } ?>



                        </div>
                      </div>
                  </div>
              </div>
              
            </div>
          </div>
        </div>
        <form action="confirm.php?ac=1" method="post">
            <input type="hidden" id="id_hide"  name="id" value="TEST">
            <input type="submit" id="send_save_account" >
        </form>
        <!-- content viewport ends -->
        <!-- partial:partials/_footer.html -->
        <!-- <footer class="footer">
          <div class="row">
            <div class="col-sm-6 text-center text-sm-right order-sm-1">
              <ul class="text-gray">
                <li><a href="#">Terms of use</a></li>
                <li><a href="#">Privacy Policy</a></li>
              </ul>
            </div>
            <div class="col-sm-6 text-center text-sm-left mt-3 mt-sm-0">
              <small class="text-muted d-block">Copyright © 2019 <a href="http://www.uxcandy.co" target="_blank">UXCANDY</a>. All rights reserved</small>
              <small class="text-gray mt-2">Handcrafted With <i class="mdi mdi-heart text-danger"></i></small>
            </div>
          </div>
        </footer> -->
        <!-- partial -->
      </div>
      <!-- page content ends -->
    </div>
    <!--page body ends -->
    <!-- SCRIPT LOADING START FORM HERE /////////////-->
    <!-- plugins:js -->
<script>

  
    let btn  = document.getElementById('btn');

                            // document.querySelectorAll(`button[class*=${"btn_"}]`)

     let btnx = document.querySelectorAll(`button[id*=${"btn_"}]`) 
    //   document.querySelectorAll('.btnx');
            btnx.forEach(element => {
            
            
              let id_token =   element.getAttribute('id');
              id_token =  id_token.split("_")[1];

          

        element.addEventListener('click',e =>{
            let id_hide = document.getElementById('id_hide');
            id_hide.value = id_token;
            let img = document.getElementById('img_'+id_hide.value)
            let code_persanal = document.getElementById('code_persanal_'+id_hide.value).innerHTML;
            console.log(code_persanal,'code_persanal');

            Swal.fire({
                imageUrl: img.src,
                imageHeight: 400,
                imageWidth: 600,
                imageAlt: 'A tall image',
                title: 'Active Account \n  เลขบัตรประชาชน  '+code_persanal,
                // text: "You won't be able to revert this!",
                // icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Active'



            }).then((value)=>{
                console.log();
                if(!value.isDismissed){
                    let send_save_account = document.getElementById('send_save_account');
                send_save_account.click();                    
                }

                // Swal.fire('Active Account Success!', '', 'success')
            })

        //    Swal.fire(
        //     'เพิ่มข้อมูล Banner สำเร็จ',
        //     '',
        //     'success'
        //   ).then((value)=>{
        //       banner_home()
        //   })
    })

    });
    

    let btnx_reply = document.querySelectorAll(`button[id*=${"btn_reply_"}]`) 
    console.log(btnx_reply,'btnx_reply');
    btnx_reply.forEach(element => {

       element.addEventListener('click',e =>{
        
        let id_el = e.target.getAttribute('id');
        id_el = id_el.split("_")[2];
        let email = document.getElementById('email_'+id_el)
        email = email.innerHTML.trim();
        console.log(email,'email');

             Swal.fire({
              input: 'textarea',
              inputLabel: " ตอบกลับ "+email,
              inputPlaceholder: 'ตอบกลับ Email',
              inputAttributes: {
                'aria-label': 'Type your message here'
              },
              showCancelButton: true
            }).then((value)=>{
              console.log(value.value,'value');

              var formData = new FormData();
              formData.append('id_token', id_el);
              formData.append('email', email);
              formData.append('message',value.value);



      if(localStorage.getItem('production')=="1") {
        window.url = "https://localhost/";
      }
      else if(localStorage.getItem('production')=="2"){
        window.url = "https://www.thaisuprateacher.org/dev/";
      }

    var url_token
		if(localStorage.getItem('production')=="1") {
			url_token = `${window.url}Thaisuprateacher/signup_data.php`
		}
		else {
			url_token = `${window.url}reply-account.php`
		}

              fetch(url_token, {
                method:"POST",
              //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
                body:formData
              })
              .then(function(response) {
                return response.json();
              })
              .then((value)=>{
                    console.log(value,'value');
              })
              .catch(function(error) {
                console.log("Request failed", error);
              });
            })

            

      })

    })



if(localStorage.getItem('production')=="1") {
        window.url = "http://localhost/";
      }
      else if(localStorage.getItem('production')=="2"){
        window.url = "http://www.thaisuprateacher.org/dev/";
      }
      var id_banner = 0
</script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="../functions/Fnc.js"></script>
    <script src="../functions/module-default.js"></script>
    <script src="./js/fn_create.js"></script>
    <script>
ListBanner()
      let menu_bar =  document.getElementById('menu_bar');
      let txt = document.querySelectorAll('#menu_bar li a');
      console.log(txt);
      txt.forEach(value => {
        console.log(value);
        console.log(localStorage.getItem('production') == "1");
          if(localStorage.getItem('production') == "1"){
            // value.setAttribute('href', '/Thaisuprateacher/content-setting/backend/join_project/');
          }
          else {
      txt.forEach((value,index) => {
        console.log(localStorage.getItem('production') == "1");
        let str
            if(index+1 == "1") {
              str = "banner";
            }
            else if (index+1=="2"){
              str = "galangal"
            }
            else if (index+1=="3"){
              str = "Hall_of_fame"
            }
            else if (index+1=="4"){
              str = "mail_announcement"
            }
            else if (index+1=="5"){
              str = "innovation_library"
            }
            else if (index+1=="6"){
              str = "join_project"
            }
            else if (index+1=="7"){
              str = "confirm"
            }
            value.setAttribute('href', `/dev/content-setting/backend/${str}/`);
      });

txt.forEach((value,index) => {
  console.log(localStorage.getItem('production') == "1");
    if(localStorage.getItem('production') == "1"){
      // value.setAttribute('href', '/Thaisuprateacher/content-setting/backend/join_project/');
    }
    else {
      let str
      if(index+1 == "1") {
        str = "banner";
      }
      else if (index+1=="2"){
        str = "galangal"
      }
      else if (index+1=="3"){
        str = "Hall_of_fame"
      }
      else if (index+1=="4"){
        str = "mail_announcement"
      }
      else if (index+1=="5"){
        str = "innovation_library"
      }
      else if (index+1=="6"){
        str = "join_project"
      }
      value.setAttribute('href', `/dev/content-setting/backend/${str}/`);
    }
});;
          }
      });

      


      
    </script>



    <script src="./js/fn_create.js"></script>
    <script src="../../assets/vendors/js/core.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    <!-- endinject -->
    <!-- Vendor Js For This Page Ends-->
    <script src="../../assets/vendors/apexcharts/apexcharts.min.js"></script>
    <script src="../../assets/vendors/chartjs/Chart.min.js"></script>
    <script src="../../assets/js/charts/chartjs.addon.js"></script>
    <!-- Vendor Js For This Page Ends-->
    <!-- build:js -->
    <script src="../../assets/js/template.js"></script>
    <script src="../../assets/js/dashboard.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- endbuild -->

  </body>
</html>