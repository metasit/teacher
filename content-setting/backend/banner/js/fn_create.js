async function uploadfile(event){



    let element = event.target
    let input_file = element.previousSibling.previousSibling
    input_file.click()
}

async function file64(event){
    // console.log(event,'event');
    // console.log(await ReuseNormal.base64_input(event));
    // console.log(base_64);
    // console.log('file64');
    // console.log( ReuseNormal.verifyFile(event,{},2,""));
    if(ReuseNormal.verifyFile(event,{},2,"")==false){
      alert("File มีขนาด เกิน 2 MB")
      return 
    }


    base_64 = await ReuseNormal.base64_input(event)
    document.getElementsByClassName('img-banner')[0].src = base_64;
    data_file = event.target.files[0];
    console.log(data_file);
}

async function into_update(id){
    if(localStorage.getItem('production')=="1"){
      window.location.href=window.url+"Thaisuprateacher/content-setting/backend/banner/update.html?id="+id;   
    }
    else {
      window.location.href=window.url+"content-setting/backend/banner/update.html?id="+id;
    }
   
}

async function GetBannerData(){
  let id = new URLSearchParams(window.location.search).get('id');
  let url_token
  if(localStorage.getItem('production')=="1") {
     url_token = `${window.url}Thaisuprateacher/content-setting/API/banner_list.php?id=`+id
  }
  else {
     url_token = `${window.url}content-setting/API/banner_list.php?id=`+id
  }
  fetch(url_token, {
    method:"POST",
    headers: { Accept: "application/json" ,'Content-Type':'application/json' },
    // body:formData
  })
  .then(function(response) {
    return response.json();
  })
  .then((value)=>{
      console.log(value.statu);
    // if(value.status==true){
      let img = "image/"+value.data[0]['file_name'];
      let sel = value.data[0]['id_project'];
      document.getElementById('img-banner').src = img;
      setTimeout(() => {
        document.getElementById('project-info').value = sel;  
      }, 300);
      
      
      // let element_select = Array.from(document.getElementById('project-info').children); 
      // document.getElementById("project-info").childNodes.forEach(function(entry) {
      //   console.log(entry);
      // });
      
      // element_select.children.forEach(value => {
      //   console.log(value,'value');
      // });
      // console.log(str,'str');
      // }

  })
  .catch(function(error) {
    console.log("Request failed", error);
  });

}

async function ListBanner(){
    console.log('ListBanner');
    let url_token 
    if(localStorage.getItem('production')=="1") {
       url_token = `${window.url}Thaisuprateacher/content-setting/API/banner_list.php`
    }
    else {
      console.log(`${window.url}content-setting/API/banner_list.php`);
       url_token = `${window.url}content-setting/API/banner_list.php`
    }
    fetch(url_token,{
        method:"POST",
        headers: { Accept: "application/json" ,'Content-Type':'application/json' },
        // body:formData
      })
      .then(function(response) {
        return response.json();
      })
      .then((value)=>{
          console.log(value,'value');

        
        if(value.status==true){
            let banner_table = document.getElementById('banner-table')
            let str = ""
            value.data.forEach((value,index) => {
                str = "<tr>";
                str +=`<td>${index+1}</td>`;
                str +=`<td><img style="object-fit:cover;" src="image/${value.file_name}" width="50" height="50" /></td>`;
                str +=`<td>${value.file_name}</td>`;
                str +=`<td>${value.size}</td>`;
                str +=`<td>${value.format}</td>`;
                str +=`<td onclick="SetId(${value.id_banner})"   class="text-right">
                
                <button onclick="into_update(${value.id_banner})" class="btn btn-primary mr-2">
                  Update
                </button>

                  <button  data-toggle="modal" data-target="#exampleModal" class="btn btn-danger" >Delete</button>
                </td>`;    
                str +="</tr>";
                banner_table.innerHTML += str;
            });
            // console.log(str,'str');
        }

      })
      .catch(function(error) {
        console.log("Request failed", error);
      });
}
async function SetId(id){
    id_banner = id;
}

async function DeleteBanner(){
    // $('#exampleModal').modal('hide')
    document.getElementById('close-banner').click()
    var formData = new FormData();
    formData.set('id', id_banner);

    let url_token
    if(localStorage.getItem('production')=="1") {
       url_token = `${window.url}Thaisuprateacher/content-setting/API/delete_banner.php`
    }
    else {
       url_token = `${window.url}content-setting/API/delete_banner.php`
    }

    fetch(url_token,{
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        let banner_table = document.getElementById('banner-table')
        banner_table.innerHTML = ""
        let str = ""
        value.data.forEach((value,index) => {
            str = "<tr>";
            str +=`<td>${index+1}</td>`;
            str +=`<td><img style="object-fit:cover;" src="image/${value.file_name}" width="50" height="50" /></td>`;
            str +=`<td>${value.file_name}</td>`;
            str +=`<td>${value.size}</td>`;
            str +=`<td>${value.format}</td>`;
            str +=`<td onclick="SetId(${value.id_banner})"  data-toggle="modal" data-target="#exampleModal" class="text-right">
            <button onclick="into_update(${value.id_banner})" class="btn btn-primary mr-2">
              Update
            </button>
            <button class="btn btn-danger" >
              Delete
            </button></td>`;    
            str +="</tr>";
            banner_table.innerHTML += str;
        });

        // console.log(value.status);

    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}   


async function save_banner(){
    // console.log(data_file);
    let ck = 0;
    if(!data_file) {
      document.getElementById('w-img').classList.remove('d-none');
      ck = 1;
    } 

    if(!document.getElementById('project-info').value){
      ReuseNormal.add_class(document.getElementById('project-info'),'is-invalid')
      ck = 1;
    }
    if(ck == 1){
      return 
    }
    let sp1 = document.getElementById('sp1');
    let btn_sp1 = document.getElementById('btn_sp1');
    ReuseNormal.remove_class(sp1,'d-none');
    btn_sp1.disabled = true;

    

    var formData = new FormData();
    formData.set('json', data_file);
    formData.set('test', 100);
    formData.set('id_project', document.getElementById('project-info').value);

    let url_token
    if(localStorage.getItem('production')=="1") {
       url_token = `${window.url}Thaisuprateacher/content-setting/API/banner.php`
    }
    else {
       url_token = `${window.url}content-setting/API/banner.php`
    }
    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        Swal.fire(
            'เพิ่มข้อมูล Banner สำเร็จ',
            '',
            'success'
          ).then((value)=>{
              banner_home()
          })
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}

function TEST(){
  console.log('TEST');
}

function GetProject(){
  let url_token
  if(localStorage.getItem('production')=="1") {
     url_token = `${window.url}Thaisuprateacher/content-setting/API/join_project/list_project.php`
  }
  else {
     url_token = `${window.url}content-setting/API/join_project/list_project.php`
  }

  fetch(url_token, {
    method:"POST",
    // headers: { Accept: "application/json" ,'Content-Type':'application/json' },
  })
  .then(function(response) {
    return response.json();
  })
  .then((value)=>{ 
    let project_info = document.getElementById('project-info');
    value.data.forEach(value => {
      project_info.innerHTML += `<option value="${value.id_join_project}" >${value.title_menu}</option>`;  
    });
  })
  .catch(function(error) {
    console.log("Request failed", error);
  });

}


async function banner_home(){

  if(localStorage.getItem('production')=="1"){
    window.location.href=window.url+"Thaisuprateacher/content-setting/backend/banner/";   
  }
  else {
    window.location.href=window.url+"content-setting/backend/banner/";
  }
}

async function value_text(){
    console.log($('#summernote').summernote('code'));
}

async function update_banner(){
  let sp1 = document.getElementById('sp1');
  let btn_sp1 = document.getElementById('btn_sp1');
  ReuseNormal.remove_class(sp1,'d-none');
  btn_sp1.disabled = true;
  var formData = new FormData();
  if(data_file)
  formData.append('photo',data_file);
  formData.append('id', new URLSearchParams(window.location.search).get('id'));
  formData.append('id_project', document.getElementById('project-info').value);
 let url_token 
 if(localStorage.getItem('production')=="1") {
    url_token = `${window.url}Thaisuprateacher/content-setting/API/update.php`
 }
 else {
    url_token = `${window.url}content-setting/API/update.php`
 }
  fetch(url_token, {
    method:"POST",
    // headers: { Accept: "application/json" ,'Content-Type':'application/json' },
    body:formData
  })
  .then(function(response) {
    return response.text();
  })
  .then((value)=>{
    Swal.fire(
      'เพิ่มข้อมูล Banner สำเร็จ',
      '',
      'success'
    ).then((value)=>{
        banner_home()
    })
  })
  .catch(function(error) {
    console.log("Request failed", error);
  });
}