function into_hom_farm(){

  if(localStorage.getItem('production')=="1"){
    window.location.href=window.url+"Thaisuprateacher/content-setting/backend/Hall_of_fame/";   
  }
  else {
    window.location.href=window.url+"content-setting/backend/Hall_of_fame/"
  }
}

function into_create(){

  if(localStorage.getItem('production')=="1"){
    window.location.href = window.url+"Thaisuprateacher/content-setting/backend/Hall_of_fame/create.html"   
  }
  else {
    window.location.href = window.url+"content-setting/backend/Hall_of_fame/create.html"
  }
}

function click_file(event){
    event.currentTarget.previousSibling.previousSibling.click()
}

async function setfile(event){
  if(ReuseNormal.verifyFile(event,{},2,"")==false){
    alert("File มีขนาด เกิน 2 MB")
    return 
  }

    base_64 = await ReuseNormal.base64_input(event)
    document.getElementById('photo_hell').src = base_64;
    data_file = event.target.files[0];
}

async function setdoc(event){

  if(ReuseNormal.verifyFile(event,{},2,"")==false){
    alert("File มีขนาด เกิน 2 MB")
    return 
  }
  
  data_doc = event.target.files[0];
  ReuseNormal.remove_class(document.getElementById('doc_success'),'d-none')
  // console.log(data_doc,'data_doc');
}


async function get_home(){
  if(localStorage.getItem('production')=="1"){
    window.location.href = window.url+"Thaisuprateacher/content-setting/backend/Hall_of_fame/"   
  }
  else {
    window.location.href = window.url+"content-setting/backend/Hall_of_fame/"
  }
}


function save_hell(){

    let title = document.getElementById('title').value;
    let name = document.getElementById('name').value;
    let position = document.getElementById('position').value;
    let detail = $('#summernote').summernote('code');
    let title_page = document.getElementById('title_page').value;  
    let ck = 0;

    if(!title_page){
      ReuseNormal.add_class(document.getElementById('title_page'),'is-invalid')
      ck = 1;      
    }

    if(!title){
      ReuseNormal.add_class(document.getElementById('title'),'is-invalid')
      ck = 1;
    }
    if(!name){
      ReuseNormal.add_class(document.getElementById('name'),'is-invalid')
      ck = 1;
    }
    if(!position){
      ReuseNormal.add_class(document.getElementById('position'),'is-invalid')
      ck = 1
    }

    if(!data_file){
      ReuseNormal.remove_class(document.getElementById('img'),'d-none')
      ck = 1
    }

    // if(!data_doc){
    //   ReuseNormal.remove_class(document.getElementById('file_doc'),'d-none')
    //   ck = 1;
    // }

 

    var htmlContent = $('#summernote').summernote('code');
    var plainText = $(htmlContent).text();;
    if(!plainText){
      ReuseNormal.remove_class(document.getElementById('sumer_detail'),'d-none');
      ck = 1;
    }

    if(ck==1){
      return
    }
    
    document.getElementById('save').disabled = true
    ReuseNormal.remove_class(document.getElementById('spin'),'d-none')
    // sumer_detail
      

    // document.getElementById('detail').value
    var formData = new FormData();
    formData.append('photo',data_file);
    formData.append('file_doc',data_doc);
    formData.append('title', title);
    formData.append('name',name);
    formData.append('position', position);
    formData.append('detail', detail);
    formData.append('title_page',title_page);


    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/hall_of_fame/create.php`
    }
    else {
      url_token = `${window.url}content-setting/API/hall_of_fame/create.php`
    }

    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        if(value.status == false){
          alert("รูปภาพเนื้อหามีขนาดใหญ่เกินไป ไม่สามารถ upload ได้")
          document.getElementById('save').disabled = false
          ReuseNormal.add_class(document.getElementById('spin'),'d-none')
          return
        }

        if(value.status){
            Swal.fire(
                'เพิ่มข้อมูลสำเร็จ',
                '',
                'success'
              ).then((value)=>{
                  get_home()
              })
        }
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}

function list_hall(id){
    if(id){
        var formData = new FormData();
        formData.append('id', id);
      
          let url_token 
          if(localStorage.getItem('production')=="1") {
            url_token = `${window.url}Thaisuprateacher/content-setting/API/hall_of_fame/hall_list.php`
          }
          else {
            url_token = `${window.url}content-setting/API/hall_of_fame/hall_list.php`
          }

          fetch(url_token, {
            method:"POST",
          //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
            body:formData
          })
          .then(function(response) {
            return response.json();
          })
          .then((value)=>{
              console.log(value);
              document.getElementById('title').value = value.data[0]['title']
              document.getElementById('photo_hell').src = "image/"+value.data[0]['file_name']
              document.getElementById('name').value= value.data[0]['name'];
              document.getElementById('position').value = value.data[0]['position']
              document.getElementById('title-len').innerHTML = value.data[0]['title'].length+"/50"; 
              document.getElementById('name-len').innerHTML = value.data[0]['name'].length+"/50";
              document.getElementById('title_page').value = value.data[0]['title_page'];
              $('#summernote').summernote('code',value.data[0]['detail'])
            //   document.getElementById('detail').value = value.data[0]['detail']
          })
          .catch(function(error) {
            console.log("Request failed", error);
          });
    }
    else {
      console.log(window.url,'window.url');

        let url_token 
        if(localStorage.getItem('production')=="1") {
          url_token = `${window.url}Thaisuprateacher/content-setting/API/hall_of_fame/hall_list.php`
        }
        else {
          url_token = `${window.url}content-setting/API/hall_of_fame/hall_list.php`
        }
        console.log(url_token,'url_token');
        fetch(url_token, {
          method:"POST",
        //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
        //   body:{key:value}
        })
        .then(function(response) {
          return response.json();
        })
        .then((value)=>{
            console.log(value.data,'value.data');
            let message_table = document.getElementById('hall-list')
            message_table.innerHTML = ""
            let str = ""
            value.data.forEach((value,index) => {
                console.log(value);
                str = "<tr>";
                str +=`<td>${index+1}</td>`;
                str +=`<td>${value.title}</td>`;
                str +=`<td><img style="object-fit:cover;" width="80" height="80" src="../Hall_of_fame/image/${value.file_name}" /></td>`;
                str +=`<td>${value.name}</td>`;
                str +=`<td>${value.position}</td>`;
                str +=`<td> <div style="width:200px; max-height:100px; " class=" d-inline-block   text-truncate"> ${value.detail}</div>  </td>`;
                str +=`<td    class="text-right">
                <button onclick="into_update(${value.id_hall_of_fame})" class="btn btn-primary">Update</button>
                <button  
                data-toggle="modal" data-target="#exampleModal"
                onclick="SetId_delete(${value.id_hall_of_fame})" class="btn btn-danger" >Delete</button>
                </td>`;    
                str +="</tr>";
                message_table.innerHTML += str;
            });
            


        })
        .catch(function(error) {
          console.log("Request failed", error);
        });
    }
}
function into_update(id){

  if(localStorage.getItem('production')=="1"){
    window.location.href = window.url+"Thaisuprateacher/content-setting/backend/Hall_of_fame/update.html?id="+id;   
  }
  else {
    window.location.href = window.url+"content-setting/backend/Hall_of_fame/update.html?id="+id;
  }

  
    


  }

function SetId_delete(id){
    id_value = id
    console.log(id_value,'id_value');
}

function delete_hell(){

    var formData = new FormData();
    formData.append('id', id_value);

    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/hall_of_fame/delete.php`
    }
    else {
      url_token = `${window.url}content-setting/API/hall_of_fame/delete.php`
    }
    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        console.log(value);
        document.getElementById('close-popup-hall').click();
        
        let message_table = document.getElementById('hall-list')
        message_table.innerHTML = ""
        let str = ""
        if(ReuseNormal.ckvalue(value.data))
        value.data.forEach((value,index) => {
          str = "<tr>";
          str +=`<td>${index+1}</td>`;
          str +=`<td>${value.title}</td>`;
          str +=`<td><img style="object-fit:cover;" width="80" height="80" src="../Hall_of_fame/image/${value.file_name}" /></td>`;
          str +=`<td>${value.name}</td>`;
          str +=`<td>${value.position}</td>`;
          str +=`<td> <div style="width:200px; max-height:100px; " class=" d-inline-block   text-truncate"> ${value.detail}</div>  </td>`;
          str +=`<td    class="text-right">
          <button onclick="into_update(${value.id_hall_of_fame})" class="btn btn-primary">Update</button>
          <button  
          data-toggle="modal" data-target="#exampleModal"
          onclick="SetId_delete(${value.id_hall_of_fame})" class="btn btn-danger" >Delete</button>
          </td>`;    
          str +="</tr>";
          message_table.innerHTML += str;
        });
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}

function update_hall(status){
  let title = document.getElementById('title').value;
  let name = document.getElementById('name').value;
  let position = document.getElementById('position').value;
  let title_page = document.getElementById('title_page').value
  let detail = $('#summernote').summernote('code')
  
  let ck = 0;

  if(!title_page){
    ReuseNormal.add_class(document.getElementById('title_page'),'is-invalid')
    ck = 1;
  }

  if(!title){
    ReuseNormal.add_class(document.getElementById('title'),'is-invalid')
    ck = 1;
  }
  if(!name){
    ReuseNormal.add_class(document.getElementById('name'),'is-invalid')
    ck = 1;
    accep
  }
  if(!position){
    ReuseNormal.add_class(document.getElementById('position'),'is-invalid')
    ck = 1
  }

  if(!data_file && status!= true ){
    ReuseNormal.remove_class(document.getElementById('img'),'d-none')
    ck = 1
  }

  var htmlContent = $('#summernote').summernote('code');
  // var plainText = $(htmlContent).text();;
  // console.log(htmlContent);
  // console.log(plainText);
  if(!htmlContent){
    // console.log(plainText,'plainText');
    ReuseNormal.remove_class(document.getElementById('sumer_detail'),'d-none');
    ck = 1;
  }

  if(ck==1){
    return
  }

  document.getElementById('save').disabled = true
  ReuseNormal.remove_class(document.getElementById('spin'),'d-none')

  
    var formData = new FormData();
    formData.append('title',document.getElementById('title').value);
    formData.append('photo_hell', data_file || 1 );
    formData.append('name', document.getElementById('name').value);
    formData.append('position',document.getElementById('position').value);
    formData.append('detail',$('#summernote').summernote('code') );
    formData.append('id', new URLSearchParams(window.location.search).get('id'));
    formData.append('file_doc', data_doc);
    formData.append('title_page',title_page);



    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/hall_of_fame/update.php`
    }
    else {
      url_token = `${window.url}content-setting/API/hall_of_fame/update.php`
    }
    fetch(url_token, {
      method:"POST",
      //headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    }).then((value)=>{

        if(value.status==false){
          if(value.status == false){
            alert("รูปภาพเนื้อหามีขนาดใหญ่เกินไป ไม่สามารถ upload ได้")
            document.getElementById('save').disabled = false
            ReuseNormal.add_class(document.getElementById('spin'),'d-none')
            return
          }
        }

        if(value.status){
            Swal.fire(
                'Updateข้อมูลสำเร็จ',
                '',
                'success'
              ).then((value)=>{
                  get_home()
              })
        }
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}


function title_len(el1,el2){
  
  
  let title = el1;   
  let title_len =  el2;  
   if(title.value.length > 50){
    let  = title.value;
    title.value = title.value.substr(0,50)
    return

   }
   if(title.value.length <=50) {
    title.value = title.value;
    title_len.innerHTML = title.value.length+'/50'; 
  }


}


