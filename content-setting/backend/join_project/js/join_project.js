function into_create_project(){
  if(localStorage.getItem('production')=="1"){
    window.location.href =  window.url+"Thaisuprateacher/content-setting/backend/join_project/create.html"
  }
  else {
    window.location.href =  window.url+"content-setting/backend/join_project/create.html"
  }
  // window.location.href =  window.url+"Thaisuprateacher/content-setting/backend/join_project/create.html"
}

function into_update_project(id){
  if(localStorage.getItem('production')=="1"){
    window.location.href =  window.url+"Thaisuprateacher/content-setting/backend/join_project/update.html?id="+id
  }
  else {
    window.location.href =  window.url+"content-setting/backend/join_project/update.html?id="+id
  }
  // window.location.href =  window.url+"Thaisuprateacher/content-setting/backend/join_project/update.html?id="+id
}

function into_home_project(){

  if(localStorage.getItem('production')=="1"){
    window.location.href =  window.url+"Thaisuprateacher/content-setting/backend/join_project/"
  }
  else {
    window.location.href =  window.url+"content-setting/backend/join_project/"
  }
    // window.location.href =  window.url+"Thaisuprateacher/content-setting/backend/join_project/"
}

function click_photo_project(event){
    event.currentTarget.previousSibling.previousSibling.click()

}



async function set_photo_project(event){
  if(ReuseNormal.verifyFile(event,{},2,"")==false){
    alert("File มีขนาด เกิน 2 MB")
    return 
  }

    base64 = await ReuseNormal.base64_input(event)
    document.getElementById('photo_project').src = base64;
    data_file = event.target.files[0];
    //console.log(data_file);
    //console.log(base64);
}

async function set_background_project(event){
  if(ReuseNormal.verifyFile(event,{},4,"")==false){
    alert("File มีขนาด เกิน 4 MB")
    return 
  }
  base64 = await ReuseNormal.base64_input(event)
  document.getElementById('background_project').src = base64;
  file_background = event.target.files[0];
  console.log(file_background);
  // console.log(base64);
}

async function set_photo_project_multiple(event){
    // console.log(event.target.files,'event.target.files');
    if(ReuseNormal.verifyFile(event,{},2,"")==false){
      alert("File มีขนาด เกิน 2 MB")
      return 
    }
    let data_photo =  await ReuseNormal.base64_input_multiple(event)
    document.getElementById('file-gll').innerHTML = "";
    for (let x = 0; x < data_photo[0].length; x++) {
        let value = data_photo[0][x];
        document.getElementById('file-gll').innerHTML += `<img src=${value} style="object-fit:cover;" class="mr-2 mt-2" width="80" height="80" />`;    
    }
    base64_multiple = data_photo[0];
    data_file_multiple = data_photo[1];
}

async function set_photo_project_multiple_pdf(event){
  console.log(event,'event');
  console.log(event.files,'files');
  if(ReuseNormal.verifyFile(event,{},100,"")==false){
    alert("File มีขนาด เกิน 100 MB")
    return 
  }
    // console.log(event.target.files,'event.target.files');
    if(ReuseNormal.ckvalue(document.getElementById('upload_success')))   {
      ReuseNormal.remove_class(document.getElementById('upload_success'),'d-none')
    }
    let data_photo =  await ReuseNormal.base64_input_multiple(event)
    file_pdf = data_photo[1];
    // data_file_multiple = event.target.files;
    document.getElementById('btn-pdf').innerHTML = "";
    document.getElementById('btn-pdf').innerHTML += `  ${file_pdf.length} File ` ;
    // console.log(file_pdf,'file_pdf');
}


function save_project(){
    let title_menu = document.getElementById('title_menu').value;
    let title_project = document.getElementById('title_project').value;
    let title_gallery = document.getElementById('title_gallery').value;
    let title_pdf = document.getElementById('title_pdf').value;
    let title_video = document.getElementById('title_video').value;
    let link_video = document.getElementById('link_video').value;
    // let detail_summary = $('#summernote_total').summernote('code')
    //  document.getElementById('detail_summary').value;
    let ck = 0;
    // if(!detail_summary){
    //   ReuseNormal.remove_class(document.getElementById('detail_summary'),'d-none')
    //   ck = 1;
    // }
    var htmlContent = $('#summernote_total').summernote('code');
    var plainText = $(htmlContent).text();;
    if(!plainText){
      ReuseNormal.remove_class(document.getElementById('detail_summary'),'d-none');
      ck = 1;
    }
    if(!title_menu){
      ReuseNormal.add_class(document.getElementById('title_menu'),'is-invalid')
      ck = 1;
    }

    if(!title_project){
      ReuseNormal.add_class(document.getElementById('title_project'),'is-invalid')
      ck = 1;
    }


    if(!title_gallery){
      ReuseNormal.add_class(document.getElementById('title_gallery'),'is-invalid')
      ck = 1;
    }

    if(!title_pdf){
      ReuseNormal.add_class(document.getElementById('title_pdf'),'is-invalid')
      ck = 1;
    }

    if(!title_video){
      ReuseNormal.add_class(document.getElementById('title_video'),'is-invalid')
      ck = 1;
    }


    if(!link_video){
      ReuseNormal.add_class(document.getElementById('link_video'),'is-invalid')
      ck = 1;
    }

    if(!data_file){
        ReuseNormal.remove_class(document.getElementById('pho-project'),'d-none');
        ck = 1;
    }
    if(!file_background){
      ReuseNormal.remove_class(document.getElementById('bg_summary'),'d-none');
      ck = 1;
    }


    var htmlContent = $('#summernote').summernote('code');
    var plainText = $(htmlContent).text();;
    if(!plainText){
      ReuseNormal.remove_class(document.getElementById('summer_detail'),'d-none');
      ck = 1;
    }

    if(!data_file_multiple){
      ReuseNormal.remove_class(document.getElementById('photo_multiple'),'d-none');
      ck = 1;
    }

    if(!file_pdf){
      ReuseNormal.remove_class(document.getElementById('pdf_multiple'),'d-none');
      ck = 1
    }
    if(ck==1){
      return
    }
    let sp1 = document.getElementById('sp1');
    let btn_sp1 = document.getElementById('btn_sp1');
    ReuseNormal.remove_class(sp1,'d-none');
    btn_sp1.disabled = true;
    // var data_file;
    // var base64_multiple;
    // var data_file_multiple;
    var formData = new FormData();
    formData.append('title_menu', title_menu);
    formData.append('title_project', title_project);
    formData.append('title_gallery', title_gallery);
    formData.append('title_pdf',title_pdf);
    formData.append('detail', $('#summernote').summernote('code'));
    formData.append('data_file', data_file);
    formData.append('title_video', title_video);
    formData.append('link_video', link_video);
    formData.append('detail_summary', $('#summernote_total').summernote('code'));
    formData.append('file_background',file_background);





    if(data_file_multiple)
    for (let x = 0; x < data_file_multiple.length; x++) {
         formData.append('data_file_multiple[]', data_file_multiple[x]);  ;
    }
    if(file_pdf)
    for (let x = 0; x < file_pdf.length; x++) {
         formData.append('file_pdf[]', file_pdf[x]);
    }

    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/join_project/create.php`
    }
    else {
      url_token = `${window.url}content-setting/API/join_project/create.php`
    }

    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        console.log(value,'value');
        if(value.status){

            if(value.status){
                Swal.fire(
                    'เพิ่มสารสำเร็จ',
                    '',
                    'success'
                  ).then((value)=>{
                    into_home_project()
                  })
            }
            
        }
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}

function list_project_data(){
    console.log('list_project_data');
    var formData = new FormData();
    formData.append('id', new URLSearchParams(window.location.search).get('id'));


    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/join_project/list_project.php`
    }
    else {
      url_token = `${window.url}content-setting/API/join_project/list_project.php`
    }
    fetch(url_token, {
        method:"POST",
      //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
        body:formData
      })
      .then(function(response) {
        return response.json();
      })
      .then((value)=>{
          console.log('success');
          console.log(value);
            // console.log(value,'value');
            let token = value.data;
            document.getElementById('title_menu').value = token.title_menu;
            document.getElementById('title_project').value = token.title_project;
            document.getElementById('photo_project').src =  "image/"+token.img_top_project;
            document.getElementById('background_project').src = "bg-background/"+token.file_background_summary;
            document.getElementById('title_pdf').value = token.title_pdf;
            document.getElementById('title_video').value = token.title_video;
            document.getElementById('link_video').value = token.link_video;
            document.getElementById('pre_message_project').value = token.pre_message;
          
            

            $('#summernote').summernote('code',token.detail);
            $('#summernote_total').summernote('code',token.detail_summary);
            document.getElementById('title_gallery').value = token.title_gallery
            let pdf = []
            for (let x = 0; x < value.name_file.length; x++) {
                let valuex = "image/"+value.name_file[x];
                if(
                  valuex.split(".")[1] == 'png' ||
                  valuex.split(".")[1] == 'jpg' ||
                  valuex.split(".")[1] == 'jpeg' ||
                  valuex.split(".")[1] == 'gif'  ||
                  valuex.split(".")[1] == 'tif' ||
                  valuex.split(".")[1] == 'tiff' ||
                  valuex.split(".")[1] == 'bmp' 
                  // valuex.split(".")[1]!="pdf"   &&
                  // valuex.split(".")[1]!="xls" &&
                  // valuex.split(".")[1]!="xlsx" &&
                  // valuex.split(".")[1]!="ptt" &&
                  // valuex.split(".")[1]!="doc" &&
                  // valuex.split(".")[1]!="docs" &&
                  // valuex.split(".")[1]!="txt"
                  )
                document.getElementById('file-gll').innerHTML += `<img  src=${valuex} style="object-fit:cover;" class="mr-2 mt-2" width="80" height="80" />`;    
                else {
                    pdf.push(value.name_file[x])
                }
            }
            len_pdf = pdf.length;
            document.getElementById('btn-pdf').innerHTML = "";
            document.getElementById('btn-pdf').innerHTML += `  ${len_pdf} File ` ;
      })
      .catch(function(error) {
        console.log("Request failed", error);
      });
}

function update_project (status){

    let title_menu = document.getElementById('title_menu').value ;
    let title_project = document.getElementById('title_project').value ;
    let detail = $('#summernote').summernote('code');
    let detail_summary = $('#summernote_total').summernote('code');
    let title_gallery = document.getElementById('title_gallery').value;
    let title_pdf = document.getElementById('title_pdf').value;
    let title_video = document.getElementById('title_video').value;
    let link_video = document.getElementById('link_video').value;
    let pre_message_project = document.getElementById('pre_message_project').value

    



    let ck = 0;
    var htmlContent = $('#summernote_total').summernote('code');
    var plainText = $(htmlContent).text();;
    
    if(!pre_message_project){
      ReuseNormal.add_class(document.getElementById('pre_message_project'),'is-invalid');
      ck = 1;
    }


    if(!plainText){
      ReuseNormal.remove_class(document.getElementById('detail_summary'),'d-none');
      ck = 1;
    }

    if(!title_menu){
      ReuseNormal.add_class(document.getElementById('title_menu'),'is-invalid')
      ck = 1;
    }

    



    if(!title_video){
      ReuseNormal.add_class(document.getElementById('title_video'),'is-invalid')
      ck = 1;
    }
    if(!link_video){
      ReuseNormal.add_class(document.getElementById('link_video'),'is-invalid')
      ck = 1;
    }

    if(!title_project){
      ReuseNormal.add_class(document.getElementById('title_project'),'is-invalid')
      ck = 1;
    }


    if(!title_gallery){
      ReuseNormal.add_class(document.getElementById('title_gallery'),'is-invalid')
      ck = 1;
    }

    if(!title_pdf){
      ReuseNormal.add_class(document.getElementById('title_pdf'),'is-invalid')
      ck = 1;
    }

    if(!data_file && status != true){
        ReuseNormal.remove_class(document.getElementById('pho-project'),'d-none')
    }

    var htmlContent = $('#summernote').summernote('code');
    var plainText = $(htmlContent).text();;
    if(!plainText){
      ReuseNormal.remove_class(document.getElementById('summer_detail'),'d-none');
      ck = 1;
    }

    if(ck==1){
      return
    }

    let sp1 = document.getElementById('sp1');
    let btn_sp1 = document.getElementById('btn_sp1');
    ReuseNormal.remove_class(sp1,'d-none');
    btn_sp1.disabled = true;


    // if(!data_file_multiple){
    //   console.log(data_file_multiple,'data_file_multiple');
    //   ReuseNormal.remove_class(document.getElementById('pdf_multiple'),'d-none');
    //   ck = 1;
    //   console.log('444');
    // }



    // if(!data_file_multiple && status != true ){
    //   ReuseNormal.remove_class(document.getElementById('photo_multiple'),'d-none');
    //   ck = 1;
    //   console.log('2222');
    // }

    // if(!data_file_multiple && status!=true ){
    //   ReuseNormal.remove_class(document.getElementById('pdf_multiple'),'d-none');
    //   ck = 1
    //   console.log('111');
    // }
 


    var formData = new FormData();
    formData.append('title_menu',title_menu);
    formData.append('title_project', title_project);
    formData.append('title_pdf', title_pdf);
    formData.append('detail',detail);
    formData.append('detail_summary', detail_summary);
    formData.append('title_gallery', title_gallery);
    formData.append('title_video',title_video);
    formData.append('link_video', link_video);
    formData.append('pre_message_project', pre_message_project);

    if(detail_summary){
      formData.append('detail_background_summary',file_background);
    }
    
  
    formData.append('id', new URLSearchParams(window.location.search).get('id'));
    if(data_file)
    formData.append('data_file',data_file);
    console.log(data_file_multiple,'data_file_multiple');
  
    if(data_file_multiple)
    for (let x = 0; x < data_file_multiple.length; x++) {
         formData.append('data_file_multiple[]', data_file_multiple[x]);  ;
    }
    console.log(file_pdf,'file_pdf');
    if(file_pdf)
    for (let x = 0; x < file_pdf.length; x++) {
         formData.append('file_pdf[]', file_pdf[x]);
    }



    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/join_project/update.php`
    }
    else {
      url_token = `${window.url}content-setting/API/join_project/update.php`
    }
    fetch(url_token, {
      method:"POST",
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        
      if(value.status==false){
        alert("รูปภาพเนื้อหามีขนาดใหญ่เกินไป ไม่สามารถ upload ได้")
        let sp1 = document.getElementById('sp1');
        let btn_sp1 = document.getElementById('btn_sp1');
        ReuseNormal.add_class(sp1,'d-none');
        btn_sp1.disabled = false; 
      }


      if(value.status){
            Swal.fire(
                'เพิ่มข้อมูลสำเร็จ',
                '',
                'success'
            ).then((value)=>{
                into_home_project()
            })
        }
    })
    .catch(function(error) {
      console.log("Request failed ERROR", error);
    });

    
}


function list_project(){

    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/join_project/list_project.php`
    }
    else {
      url_token = `${window.url}content-setting/API/join_project/list_project.php`
    }
    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        let message_table = document.getElementById('table_project')
        message_table.innerHTML = ""
        let str = ""
        value.data.forEach((value,index) => {
            str = "<tr>";
            str +=`<td>${index+1}</td>`;
            str +=`<td>${value.title_menu}</td>`;
            str +=`<td>${value.title_project}</td>`
            str +=`<td>${value.title_gallery}</td>`;
            str +=`<td>${value.title_pdf}</td>`;
            str +=`<td><img  style="object-fit:cover;" width="80" height="80" src="../join_project/image/${value.img_top_project}" /></td>`;
            str +=`<td class="text-right">
            <button onclick="into_update_project(${value.id_join_project})"  class="btn btn-primary">Update</button>

            </td>`;    
            str +="</tr>";
            message_table.innerHTML += str;
        });
    })
    .catch(function(error) {
      console.log("Request failed", error);
    });
}

function SetId_delete(id){
    id_value = id;
}

function delete_project(){
    var formData = new FormData();
    formData.append('id', id_value);

    let url_token 
    if(localStorage.getItem('production')=="1") {
      url_token = `${window.url}Thaisuprateacher/content-setting/API/join_project/delete.php`
    }
    else {
      url_token = `${window.url}content-setting/API/join_project/delete.php`
    }


    fetch(url_token, {
      method:"POST",
    //   headers: { Accept: "application/json" ,'Content-Type':'application/json' },
      body:formData
    })
    .then(function(response) {
      return response.json();
    })
    .then((value)=>{
        document.getElementById('close-popup-project').click();

        let message_table = document.getElementById('table_project')
        message_table.innerHTML = ""
        let str = ""
        value.data.forEach((value,index) => {
            str = "<tr>";
            str +=`<td>${index+1}</td>`;
            str +=`<td>${value.title_menu}</td>`;
            str +=`<td>${value.title_project}</td>`
            str +=`<td>${value.title_gallery}</td>`;
            str +=`<td>${value.title_pdf}</td>`;
            str +=`<td><img  style="object-fit:cover;" width="80" height="80" src="../join_project/image/${value.img_top_project}" /></td>`;
            str +=`<td class="text-right">
            <button onclick="into_update_project(${value.id_join_project})"  class="btn btn-primary">Update</button>

            </td>`;    
            str +="</tr>";
            message_table.innerHTML += str;
        });

    })
    .catch(function(error) {
      console.log("Request failed", error);
    });



}


