

<?php
	session_start();
	require_once('condb.php');

	// Set $membership_upgrade
	$membership_upgrade = $_GET['membership_upgrade'];

	if($membership_upgrade == 'memberSilver' || $membership_upgrade == 'memberGold' || $membership_upgrade == 'memberDiamond') { // In case of user has been approved, upgrade level for user

		// Set $membership_status
		$membership_status = 'Approve แล้ว';
		// Set $membership_ID
		$membership_ID = $_GET['membership_ID'];
		// Set $membership_id
		$membership_id = $_GET['membership_id'];
		// Set $win_scroll
		$win_scroll = $_GET['win_scroll'];

		

		/* Update Status in login table */
		$sql = "UPDATE `login` SET level='$membership_upgrade' WHERE ID='$membership_ID' ";
		$re = $con->query($sql) or die($con->error);

		// Set bill_code
		$sqlsetbillvalue = "SELECT MAX(CAST(SUBSTRING(bill_code,-5) AS UNSIGNED)) AS max_bill_code FROM `membership` ";
		$resbv = $con->query($sqlsetbillvalue);
		$rowsbv = $resbv->fetch_assoc();

		$bill_type = 'BN';
		$slip_date = date("Y-m-d H:i:s");
		$slip_year = substr(date('Y',strtotime($slip_date))+543,-2);
		$bill_num = sprintf('%05d',$rowsbv['max_bill_code']+1);
		$bill_code = $bill_type.$slip_year.$bill_num;

		/* Update membership_status in membership table */
		$sql = "UPDATE `membership` SET membership_status='$membership_status', bill_code='$bill_code' WHERE membership_id='$membership_id' ";
		$re = $con->query($sql) or die($con->error);

		header('location: ระบบหลังบ้านบำรุงสมาชิก.php?win_scroll='.$win_scroll);

	}elseif($membership_upgrade == 'ปฏิเสธSlip') {

		// Set $membership_id
		$membership_id = $_GET['membership_id'];

		$sqlmembership = "SELECT membership_remark FROM `membership` WHERE membership_id='$membership_id' ";
		$reMS = mysqli_query($con, $sqlmembership);
		$rowMS = mysqli_fetch_array($reMS);

		if($rowMS['membership_remark'] != '') {

			// Set $membership_status
			$membership_status = 'ปฏิเสธSlip';
			// Set $win_scroll
			$win_scroll = $_GET['win_scroll'];



			/* Update membership_status in membership table */
			$sql = "UPDATE `membership` SET membership_status='$membership_status' WHERE membership_id='$membership_id' ";
			$re = $con->query($sql) or die($con->error);

			header('location: ระบบหลังบ้านบำรุงสมาชิก.php?win_scroll='.$win_scroll);

		}else{

			echo '<script>';
				echo "alert('กรุณาใส่เหตุผลที่ปฏิเสธที่ช่องหมายเหตุด้วยค่ะ');";
				echo "window.location.replace('ระบบหลังบ้านบำรุงสมาชิก.php?win_scroll=$win_scroll')";
			echo '</script>';


		}

	}

?>