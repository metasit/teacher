<?php 
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		/*
			$tea_proj = 1: User need to see โครงการครูดีของแผ่นดิน
			$stu_proj = 1: User need to see โครงการเด็กดีของแผ่นดิน
			$sup_proj = 1: User need to see โครงการศน.ดีของแผ่นดิน
		*/

		if(isset($_POST['dash_update_btn'])) {

			$affil_code = $_POST['affsub4_id'];
			
			if($_POST['affsub4_id'] == '') {
				$affil_code = $_POST['affsub3_id'];

				if($_POST['affsub3_id'] == '') {
					$affil_code = $_POST['affsub2_id'];

					if($_POST['affsub2_id'] == '') {
						$affil_code = $_POST['affsub_id'];

						if($_POST['affsub_id'] == '') {
							$affil_code = substr($_SESSION['affil_code'], 0, 3);
						}
					}
				}
			}
			include('includes/convert2afftext.php');
			$affil_code_filter = $affil_code;
			$full_affiliation_name_filter = $full_affiliation_name;

			$affil_code = $_SESSION['affil_code'];
			include('includes/convert2afftext.php');


			$tea_proj = $_POST['tea_proj'];
			$stu_proj = $_POST['stu_proj'];
			$sup_proj = $_POST['sup_proj'];
			
		}else{

			$affil_code = $_SESSION['affil_code'];
			include('includes/convert2afftext.php');
			$affil_code_filter = $affil_code;
			$full_affiliation_name_filter = $full_affiliation_name;

			$affil_code = $_SESSION['affil_code'];
			include('includes/convert2afftext.php');

		}

	}else{

		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';

	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">

	<style>
		p.affil-size {
			font-size: 15px;
			line-height: 20px;
		}
		.price-pr p:hover {
			transform: scale(1.3);
			cursor: none;
		}
		.price-pr p.affil-size:hover {
			transform: scale(1.1);
			cursor: none;
		}
		.price-pr button {
			font-family: KanitLight;
			font-size: 18px;
			background-color: transparent;
			color: black;
			border-color: transparent;
			display: block;
			text-align: center;
			width: 100%;
		}
		.price-pr button:hover {
			cursor: pointer;
			transform: scale(1.3);
			text-decoration: underline;
		}

		.table3 {
			margin-top: 50px;
			margin-bottom: 50px;
		}
		.table3 table th {
			background-color: rgb(72, 160, 0);
			color: white;
		}
		.table3 table th:nth-child(1) {
			background-color: rgb(0, 6, 27);
			color: rgba(238, 195, 38, 0.8);;
		}
		.table3 table tr:hover {
			background-color: transparent;
			color: none;
		}
		.table3 table td:hover {
			background-color: rgba(72, 160, 0, 0.5);
		}

		.form-check {
			text-align: left;
		}

		.affil-menu {
			background-color: rgba(72, 160, 0, 0.1);
			padding: 10px;
			border-radius: 5px;
		}
		.affil-menu-head {
			font-size: 20px;
			text-decoration: underline;
		}
		.affil-menu-head:hover {
			text-decoration: none;
		}
		.btn-submit {
			height: 100%;
			width: 100%;
			border: 2px solid rgba(0, 6, 27, 0.3);
			border-radius: 5px;
			color: white;
			background-color: rgb(180, 147, 31);
			cursor: pointer;
		}
		.btn-submit:hover {
			background-color: transparent;
			color: black;
			border: 1px solid black;
		}
	</style>
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['win_scroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="index-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
	<!-- ################################################################################################ -->
	<a name="STP"></a> <!-- Scroll to this position after dash_update_btn was clicked -->

  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">Dashboard</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<main class="hoc container clear"> 
    <article>
			<!-- ################################################################################################ -->
			<!-- Start affiliation filter -->
			<section class="affil-menu">
				<form id="Affform" action="dashboard.php#STP" method="POST" enctype="multipart/form-data">
					<!-- ################################################################################################ -->
					<!-- Start สังกัด Content -->
					<div class="row center">
						<!-- Start Header -->
						<div class="col-md-12">
							<label class="bold affil-menu-head">เลือกข้อมูลที่ต้องการดู</label>
						</div>
						<!-- End Header -->
						<!-- ################################################################################################ -->
						<!-- Start Left Content -->
						<div class="col-md-4">
							<label class="bold">สังกัดหลัก</label>
							<?php
								if($_SESSION['level'] == 'admin') { ?>

									<select name="affiliation_id" id="affiliation_id" class="form-control" style="height:40px;" required>
										<option value="" disabled="disabled" selected="selected">เลือกสังกัด</option>
										<option value="AfA">สำนักงานปลัดกระทรวงศึกษาธิการ</option>
										<option value="AfB">สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน</option>
										<option value="AfC">สำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน</option>
										<option value="AfD">สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย</option>
										<option value="AfE">สำนักงานคณะกรรมการการอาชีวศึกษา</option>
										<option value="AfF">สำนักงานคณะกรรมการการอุดมศึกษา</option>
										<option value="AfG">กระทรวงมหาดไทย กรมส่งเสริมการปกครองส่วนท้องถิ่น</option>
										<option value="AfH">กรุงเทพมหานคร</option>
										<option value="AfI">เมืองพัทยา</option>
										<option value="AfJ">สำนักงานตำรวจแห่งชาติ</option>
										<option value="AfO">อื่นๆ โปรดระบุ....</option>
									</select> <?php

								}else{ ?>

									<p><?php if($affiliation_id == 'AfA'){echo 'สำนักงานปลัดกระทรวงศึกษาธิการ';} ?></p>
									<p><?php if($affiliation_id == 'AfB'){echo 'สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน';} ?></p>
									<p><?php if($affiliation_id == 'AfC'){echo 'สำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน';} ?></p>
									<p><?php if($affiliation_id == 'AfD'){echo 'สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย';} ?></p>
									<p><?php if($affiliation_id == 'AfE'){echo 'สำนักงานคณะกรรมการการอาชีวศึกษา';} ?></p>
									<p><?php if($affiliation_id == 'AfF'){echo 'สำนักงานคณะกรรมการการอุดมศึกษา';} ?></p>
									<p><?php if($affiliation_id == 'AfG'){echo 'กระทรวงมหาดไทย กรมส่งเสริมการปกครองส่วนท้องถิ่น';} ?></p>
									<p><?php if($affiliation_id == 'AfH'){echo 'กรุงเทพมหานคร';} ?></p>
									<p><?php if($affiliation_id == 'AfI'){echo 'เมืองพัทยา';} ?></p>
									<p><?php if($affiliation_id == 'AfJ'){echo 'สำนักงานตำรวจแห่งชาติ';} ?></p> <?php
								
								}
							?>
						</div>
						<!-- End Left Content -->
						<!-- ################################################################################################ -->
						<!-- Start Center Content -->
						<div class="col-md-4">
							<!-- สังกัดหลัก affsub -->
							<label class="bold">สังกัดย่อย</label>
							<select name="affsub_id" id="affsub_id" class="form-control">
								<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
								<option value="<?php echo $affsub_id; ?>"><?php echo $affsub_name; ?></option>
							</select>
							<div id="affsub_text" style="display:none;">
								<div class="wrap-input100">
									<input class="input100" name="affsub_ans_id" id="affsub_ans_id" type="text" class="form-control" placeholder="โปรดระบุสังกัด" />
									<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
								</div>
							</div>
							<!-- สังกัดหลัก affsub2 -->
							<select name="affsub2_id" id="affsub2_id" class="form-control" style="display:none;">
								<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
							</select>
							<!-- สังกัดหลัก affsub3 -->
							<select name="affsub3_id" id="affsub3_id" class="form-control" style="display:none;">
								<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
							</select>
							<!-- สังกัดหลัก affsub4 -->
							<select name="affsub4_id" id="affsub4_id" class="form-control" style="display:none;">
								<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
							</select>
							<div id="affsub4_text" style="display:none">
								<div class="wrap-input100">
									<input class="input100" name="affsub4_ans_id" id="affsub4_ans_id" type="text" class="form-control" placeholder="โปรดระบุสังกัด" />
									<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
						<!-- End Center Content -->
						<!-- ################################################################################################ -->
						<!-- Start Right Content -->
						<div class="col-md-3">
							<label class="bold">โครงการฯ</label>
							<div class="form-check">
								<div class="col-md-12">
									<input class="form-check-input" type="checkbox" name="tea_proj" value="1" checked>
									<label>โครงการครูดีของแผ่นดิน</label>
								</div>
								<div class="col-md-12">
									<input class="form-check-input" type="checkbox" name="stu_proj" value="1" checked>
									<label>โครงการเด็กดีของแผ่นดิน</label>
								</div>
								<div class="col-md-12">
									<input class="form-check-input" type="checkbox" name="sup_proj" value="1" checked>
									<label>โครงการศึกษานิเทศดีของแผ่นดิน</label>
								</div>
							</div>
						</div>
						<!-- End Right Content -->
						<!-- ################################################################################################ -->
						<!-- Start Submit Button -->
						<div class="col-md-1">
							<button type="submit" class="btn-submit" name="dash_update_btn">ค้นหา</button>
						</div>
						<!-- End Submit Button -->
					</div>
					<!-- End สังกัด Content -->
					<!-- ################################################################################################ -->
				</form>
			</section>
			<!-- End affiliation filter -->
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
			<!-- Start affiliation table -->
			<div class="table-responsive-md">
				<section class="table3">
				<!-- ################################################################################################ -->
				<!-- Start ตารางโครงการครูดีของแผ่นดิน --> <?php
				if($tea_proj == 1) { ?>
					<table class="m-l-r-auto">
						<tr>
							<th rowspan="2">โครงการครูดีของแผ่นดิน</th>
							<th>สังกัด</th>
							<th>สมาชิกทั้งหมด</th>
							<th>ผ่านขั้นพื้นฐาน</th>
							<th>ดำเนินการชั้นที่ 5</th>
							<th>ผ่านชั้นที่ 5</th>
							<!--
							<th>ดำเนินการชั้นที่ 4</th>
							<th>ผ่านชั้นที่ 4</th>
							<th>ดำเนินการชั้นที่ 3</th>
							<th>ผ่านชั้นที่ 3</th>
							<th>ดำเนินการชั้นที่ 2</th>
							<th>ผ่านชั้นที่ 2</th>
							<th>ดำเนินการชั้นที่ 1</th>
							<th>ผ่านชั้นที่ 1</th>
							-->
						</tr>
						<tr>
							<!-- ชื่อสังกัด -->
							<td class="price-pr">
								<p class="affil-size"><?php echo $full_affiliation_name_filter; ?></p>
							</td>
							<!-- ครู, สมาชิกทั้งหมด -->
							<td class="price-pr">
								<?php
									$sqlCount_Total_Tea = "SELECT ID FROM `login` WHERE (occup_code LIKE 'OcAA%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin') 
									OR (occup_code LIKE 'OcAB%' AND affil_code LIKE '{$affil_code_filter}%')
									OR (occup_code LIKE 'OcAO%' AND affil_code LIKE '{$affil_code_filter}%') ";
									$reCount_Total_Tea = mysqli_query($con, $sqlCount_Total_Tea);
									$total_tea_amount = mysqli_num_rows($reCount_Total_Tea);

									if($total_tea_amount == 0) { ?>

										<p><?php echo number_format($total_tea_amount); ?></p> <?php

									}else{ ?>

										<form action="dashboardJoinProNameList.php" method="GET" target="_blank">
											<button type="submit" name="CFP" value="dashboard"><?php echo number_format($total_tea_amount); ?></button>

											<input type="hidden" name="program" value="tea">
											<input type="hidden" name="list" value="all">
											<input type="hidden" name="affil_code_filter" value="<?php echo $affil_code_filter; ?>">
										</form> <?php

									}
								?>
							</td>
							<!-- ครู, ผ่านขั้นพื้นฐาน -->
							<td class="price-pr">
								<?php
									$sqlCount_Total_Tea_Pass_basic_score = "SELECT ID FROM `login` WHERE (occup_code LIKE 'OcAA%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									OR (occup_code LIKE 'OcAB%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									OR (occup_code LIKE 'OcAO%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin') ";
									$reCount_Total_Tea_Pass_basic_score = mysqli_query($con, $sqlCount_Total_Tea_Pass_basic_score);
									$total_tea_amount_pass_basic_score = mysqli_num_rows($reCount_Total_Tea_Pass_basic_score);

									if($total_tea_amount_pass_basic_score == 0) { ?>

										<p><?php echo number_format($total_tea_amount_pass_basic_score); ?></p> <?php

									}else{ ?>

										<form action="dashboardJoinProNameList.php" method="GET" target="_blank">
											<button type="submit" name="CFP" value="dashboard"><?php echo number_format($total_tea_amount_pass_basic_score); ?></button>

											<input type="hidden" name="program" value="tea">
											<input type="hidden" name="list" value="basic_score_pass">
											<input type="hidden" name="affil_code_filter" value="<?php echo $affil_code_filter; ?>">
										</form> <?php

									}
								?>
							</td>
							<!-- ครู, ดำเนินการชั้นที่ 5 -->
							<td class="price-pr">
								<?php
									$sqlCount_Total_Tea_InProcess_fifth_score = "SELECT fifth_score4tea_code, affil_code FROM `fifth_score4tea` INNER JOIN `login` ON fifth_score4tea.ID=login.ID 
									WHERE fifth_score4tea.fifth_score4tea_code='A' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin' ";
									$reCount_Total_Tea_InProcess_fifth_score = mysqli_query($con, $sqlCount_Total_Tea_InProcess_fifth_score);
									$total_tea_amount_inprocess_fifth_score = mysqli_num_rows($reCount_Total_Tea_InProcess_fifth_score);

									if($total_tea_amount_inprocess_fifth_score == 0) { ?>

										<p><?php echo number_format($total_tea_amount_inprocess_fifth_score); ?></p> <?php

									}else{ ?>

										<form action="dashboardJoinProNameList.php" method="GET" target="_blank">
											<button type="submit" name="CFP" value="dashboard"><?php echo number_format($total_tea_amount_inprocess_fifth_score); ?></button>

											<input type="hidden" name="program" value="tea">
											<input type="hidden" name="list" value="fifth_score_proc">
											<input type="hidden" name="affil_code_filter" value="<?php echo $affil_code_filter; ?>">
										</form> <?php

									}
								?>
							</td>
							<!-- ครู, ผ่านชั้นที่ 5 -->
							<td class="price-pr">
								<?php
									$sqlCount_Total_Tea_Pass_fifth_score = "SELECT ID FROM `login` WHERE (occup_code LIKE 'OcAA%' AND fifth_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									OR (occup_code LIKE 'OcAB%' AND fifth_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin') ";
									$reCount_Total_Tea_Pass_fifth_score = mysqli_query($con, $sqlCount_Total_Tea_Pass_fifth_score);
									$total_tea_amount_pass_fifth_score = mysqli_num_rows($reCount_Total_Tea_Pass_fifth_score);

									if($total_tea_amount_pass_fifth_score == 0) { ?>

										<p><?php echo number_format($total_tea_amount_pass_fifth_score); ?></p> <?php

									}else{ ?>

										<form action="dashboardJoinProNameList.php" method="GET" target="_blank">
											<button type="submit" name="CFP" value="dashboard"><?php echo number_format($total_tea_amount_pass_fifth_score); ?></button>

											<input type="hidden" name="program" value="tea">
											<input type="hidden" name="list" value="fifth_score_pass">
											<input type="hidden" name="affil_code_filter" value="<?php echo $affil_code_filter; ?>">
										</form> <?php

									}
								?>
							</td>
						</tr>
					</table> <?php
				} ?>
				<!-- End ตารางโครงการครูดีของแผ่นดิน --> 
				<!-- ################################################################################################ -->
				<!-- Start ตารางโครงการเด็กดีของแผ่นดิน --> <?php
				if($stu_proj == 1) { ?>

					<table class="m-l-r-auto m-t-80">
						<tr>
							<th rowspan="2">โครงการเด็กดีของแผ่นดิน</th>
							<th>สังกัด</th>
							<th>สมาชิกทั้งหมด</th>
							<th>ผ่านขั้นพื้นฐาน</th>
							<!--
							<th>ดำเนินการชั้นที่ 5</th>
							<th>ผ่านชั้นที่ 5</th>
							<th>ดำเนินการชั้นที่ 4</th>
							<th>ผ่านชั้นที่ 4</th>
							<th>ดำเนินการชั้นที่ 3</th>
							<th>ผ่านชั้นที่ 3</th>
							<th>ดำเนินการชั้นที่ 2</th>
							<th>ผ่านชั้นที่ 2</th>
							<th>ดำเนินการชั้นที่ 1</th>
							<th>ผ่านชั้นที่ 1</th>
							-->
						</tr>
						<tr>
							<!-- ชื่อสังกัด -->
							<td class="price-pr">
								<p class="affil-size"><?php echo $full_affiliation_name_filter; ?></p>
							</td>
							<!-- เด็ก, สมาชิกทั้งหมด -->
							<td class="price-pr">
								<?php
									$sqlCount_Total_Stu = "SELECT ID FROM `login` WHERE occup_code LIKE 'OcF%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";
									$reCount_Total_Stu = mysqli_query($con, $sqlCount_Total_Stu);
									$total_stu_amount = mysqli_num_rows($reCount_Total_Stu);

									if($total_stu_amount == 0) { ?>

										<p><?php echo number_format($total_stu_amount); ?></p> <?php

									}else{ ?>

										<form action="dashboardJoinProNameList.php" method="GET" target="_blank">
											<button type="submit" name="CFP" value="dashboard"><?php echo number_format($total_stu_amount); ?></button>

											<input type="hidden" name="program" value="stu">
											<input type="hidden" name="list" value="all">
											<input type="hidden" name="affil_code_filter" value="<?php echo $affil_code_filter; ?>">
										</form> <?php

									}
								?>
							</td>
							<!-- เด็ก, ผ่านขั้นพื้นฐาน -->
							<td class="price-pr">
								<?php
									$sqlCount_Total_Stu_Pass_basic_score = "SELECT ID FROM `login` 
									WHERE occup_code LIKE 'OcF%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";
									$reCount_Total_Stu_Pass_basic_score = mysqli_query($con, $sqlCount_Total_Stu_Pass_basic_score);
									$total_stu_amount_pass_basic_score = mysqli_num_rows($reCount_Total_Stu_Pass_basic_score);

									if($total_stu_amount_pass_basic_score == 0) { ?>

										<p><?php echo number_format($total_stu_amount_pass_basic_score); ?></p> <?php

									}else{ ?>

										<form action="dashboardJoinProNameList.php" method="GET" target="_blank">
											<button type="submit" name="CFP" value="dashboard"><?php echo number_format($total_stu_amount_pass_basic_score); ?></button>

											<input type="hidden" name="program" value="stu">
											<input type="hidden" name="list" value="basic_score_pass">
											<input type="hidden" name="affil_code_filter" value="<?php echo $affil_code_filter; ?>">
										</form> <?php

									}
								?>
							</td>
						</tr>
					</table> <?php
				} ?>
				<!-- End ตารางโครงการเด็กดีของแผ่นดิน -->
				<!-- ################################################################################################ -->
				<!-- Start ตารางโครงการศน.ดีของแผ่นดิน --> <?php
				if($sup_proj == 1) { ?>

					<table class="m-l-r-auto m-t-80">
						<tr>
							<th rowspan="2">โครงการศึกษานิเทศก์ดีของแผ่นดิน</th>
							<th>สังกัด</th>
							<th>สมาชิกทั้งหมด</th>
							<th>ผ่านขั้นพื้นฐาน</th>
							<th>ดำเนินการขั้นเกียรติคุณ</th>
							<th>ผ่านขั้นเกียรติคุณ</th>
							<!--
							<th>ดำเนินการชั้นที่ 4</th>
							<th>ผ่านชั้นที่ 4</th>
							<th>ดำเนินการชั้นที่ 3</th>
							<th>ผ่านชั้นที่ 3</th>
							<th>ดำเนินการชั้นที่ 2</th>
							<th>ผ่านชั้นที่ 2</th>
							<th>ดำเนินการชั้นที่ 1</th>
							<th>ผ่านชั้นที่ 1</th>
							-->
						</tr>
						<tr>
							<!-- ชื่อสังกัด -->
							<td class="price-pr">
								<p class="affil-size"><?php echo $full_affiliation_name_filter; ?></p>
							</td>
							<!-- ศน., สมาชิกทั้งหมด -->
							<td class="price-pr">
								<?php
									$sqlCount_Total_Sup = "SELECT ID FROM `login` WHERE occup_code LIKE 'OcAD%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";
									$reCount_Total_Sup = mysqli_query($con, $sqlCount_Total_Sup);
									$total_sup_amount = mysqli_num_rows($reCount_Total_Sup);

									if($total_sup_amount == 0) { ?>

										<p><?php echo number_format($total_sup_amount); ?></p> <?php

									}else{ ?>

										<form action="dashboardJoinProNameList.php" method="GET" target="_blank">
											<button type="submit" name="CFP" value="dashboard"><?php echo number_format($total_sup_amount); ?></button>

											<input type="hidden" name="program" value="sup">
											<input type="hidden" name="list" value="all">
											<input type="hidden" name="affil_code_filter" value="<?php echo $affil_code_filter; ?>">
										</form> <?php

									}
								?>
							</td>
							<!-- ศน., ผ่านขั้นพื้นฐาน -->
							<td class="price-pr">
								<?php
									$sqlCount_Total_Sup_Pass_basic_score = "SELECT ID FROM `login` WHERE occup_code LIKE 'OcAD%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";
									$reCount_Total_Sup_Pass_basic_score = mysqli_query($con, $sqlCount_Total_Sup_Pass_basic_score);
									$total_sup_amount_pass_basic_score = mysqli_num_rows($reCount_Total_Sup_Pass_basic_score);

									if($total_sup_amount_pass_basic_score == 0) { ?>

										<p><?php echo number_format($total_sup_amount_pass_basic_score); ?></p> <?php

									}else{ ?>

										<form action="dashboardJoinProNameList.php" method="GET" target="_blank">
											<button type="submit" name="CFP" value="dashboard"><?php echo number_format($total_sup_amount_pass_basic_score); ?></button>

											<input type="hidden" name="program" value="sup">
											<input type="hidden" name="list" value="basic_score_pass">
											<input type="hidden" name="affil_code_filter" value="<?php echo $affil_code_filter; ?>">
										</form> <?php

									}
								?>
							</td>
							<!-- ศน., ดำเนินการขั้นเกียรติคุณ -->
							<td class="price-pr">
								<?php
									$sqlCount_Total_Sup_InProcess_honor_score = "SELECT honor_score4sup_code, affil_code FROM `honor_score4sup` 
									INNER JOIN `login` ON honor_score4sup.ID=login.ID
									WHERE honor_score4sup.honor_score4sup_code='A' 
									AND login.affil_code LIKE '{$affil_code_filter}%' 
									AND login.level!='admin' ";
									
									$reCount_Total_Sup_InProcess_honor_score = mysqli_query($con, $sqlCount_Total_Sup_InProcess_honor_score);
									$total_sup_amount_inprocess_honor_score = mysqli_num_rows($reCount_Total_Sup_InProcess_honor_score);

									if($total_sup_amount_inprocess_honor_score == 0) { ?>

										<p><?php echo number_format($total_sup_amount_inprocess_honor_score); ?></p> <?php

									}else{ ?>

										<form action="dashboardJoinProNameList.php" method="GET" target="_blank">
											<button type="submit" name="CFP" value="dashboard"><?php echo number_format($total_sup_amount_inprocess_honor_score); ?></button>

											<input type="hidden" name="program" value="sup">
											<input type="hidden" name="list" value="honor_score_proc">
											<input type="hidden" name="affil_code_filter" value="<?php echo $affil_code_filter; ?>">
										</form> <?php

									}
								?>
							</td>
							<!-- ศน., ผ่านขั้นเกียรติคุณ -->
							<td class="price-pr">
								<?php
									$sqlCount_Total_Sup_Pass_honor_score = "SELECT ID FROM `login` WHERE occup_code LIKE 'OcAD%' AND honor_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin' ";
									$reCount_Total_Sup_Pass_honor_score = mysqli_query($con, $sqlCount_Total_Sup_Pass_honor_score);
									$total_sup_amount_pass_honor_score = mysqli_num_rows($reCount_Total_Sup_Pass_honor_score);

									if($total_sup_amount_pass_honor_score == 0) { ?>

										<p><?php echo number_format($total_sup_amount_pass_honor_score); ?></p> <?php

									}else{ ?>

										<form action="dashboardJoinProNameList.php" method="GET" target="_blank">
											<button type="submit" name="CFP" value="dashboard"><?php echo number_format($total_sup_amount_pass_honor_score); ?></button>

											<input type="hidden" name="program" value="sup">
											<input type="hidden" name="list" value="honor_score_pass">
											<input type="hidden" name="affil_code_filter" value="<?php echo $affil_code_filter; ?>">
										</form> <?php

									}
								?>
							</td>
						</tr>
					</table> <?php
				} ?>
				<!-- End ตารางโครงการศน.ดีของแผ่นดิน -->
				<!-- ################################################################################################ -->
				</section>
			</div>
			<!-- End affiliation table -->
			<!-- ################################################################################################ -->
		</article>
  </main>
</div>
<!-- End Content 00 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>
	<!-- JS for drop-down affiliation -->
	<script src="js/filterAff4dashboard.js" type="text/javascript"></script>

</body>
</html>