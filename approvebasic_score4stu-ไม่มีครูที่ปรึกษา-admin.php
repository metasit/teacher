<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$now_date = date("d-m-Y H:i:s");

	$ID_stu = $_GET['ID_stu']; // student ID

	/* Set all value */
	$winScroll = $_GET['winScroll'];
	$basic_score4stu_id = $_GET['basic_score4stu_id'];

	$basic_score4stu_check_status = 'อนุมัติ,'.$now_date;

	/* Update status in basic_score4stu table */
	$sql = "UPDATE `basic_score4stu` SET `basic_score4stu_check_status`='$basic_score4stu_check_status' WHERE `basic_score4stu_id`='$basic_score4stu_id' ";
	$res= $con->query($sql) or die($con->error); //Check error

	/* Log Admin Action */
	$ID = $_SESSION['ID']; // admin ID

	$ID_ad = $ID;
	$adscorelog_task = 'ตรวจความดีเด็กพื้นฐาน,'.$_GET['basic_score4stu_system_id'].$_GET['basic_score4stu_file_no'];
	$ID_user = $ID_stu;
	$adscorelog_detail = $basic_score4stu_check_status;

	$sqllog="INSERT INTO `adscorelog` (ID_ad, adscorelog_task, ID_user, adscorelog_detail) 
	VALUES ('$ID_ad', '$adscorelog_task', '$ID_user', '$adscorelog_detail') ";
	$relog = $con->query($sqllog) or die($con->error); //Check error

	header('location: รายละเอียดนักเรียนทำความดี-พื้นฐาน-ไม่มีครูที่ปรึกษา-admin.php?winScroll='.$winScroll.'&ID_stu='.$ID_stu.'&system_id='.$_GET['basic_score4stu_system_id']);
?>