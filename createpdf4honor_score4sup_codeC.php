<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		if($_SESSION['level'] == 'memberSilver' || $_SESSION['level'] == 'memberGold' || $_SESSION['level'] == 'memberDiamond' || $_SESSION['level'] == 'admin') {

			// require_once to use mpdf library
			require_once __DIR__ . '/vendor/autoload.php';
			// Set Thai language for Mpdf
			$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
			$fontDirs = $defaultConfig['fontDir'];

			$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
			$fontData = $defaultFontConfig['fontdata'];

			$mpdf = new \Mpdf\Mpdf([
					'fontDir' => array_merge($fontDirs, [
							__DIR__ . '/tmp',
					]),
					'fontdata' => $fontData + [
							'sarabun' => [
									'R' => 'THSarabunNew.ttf',
									'I' => 'THSarabunNew Italic.ttf',
									'B' => 'THSarabunNew Bold.ttf',
									'BI' => 'THSarabunNew BoldItalic.ttf',
							]
					],
					'default_font' => 'sarabun'
			]);
			// Need to use supervidor's honor_score
			include('includes/calhonor_score-ศน.php');

		}else{
			echo '<script>';
				echo "alert('ต้องเป็นสมาชิกระดับเงิน, ทอง หรือเพชร ถึงจะสามารถใช้ระบบการรายงานผลนี้ได้ค่ะ');";
				echo "window.location.replace('รายงานประเมินศน.ดีขั้นเกียรติคุณ.php')";
			echo '</script>';
		}
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<?php
		ob_start(); // Start to buffer data to print on pdf file<?php
	?>
	<style>
		table, td, th {
			border: 1px solid black;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		th {
			height: 50px;
		}

		td {
			height: 40px;
		}

		p, dd, table, td, th {
			font-size: 20px;
		}
	</style>
</head>

	<!-- Start -->
		<div style="text-align: center">
			<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png">
		</div>
		<h2 style="text-align: center; font-size: 30px;">
			ผลการประเมินความเป็นศึกษานิเทศก์ดีของแผ่นดิน
			<br>
			ตามมาตรฐานและตัวบ่งชี้ ครองตน ครองคน ครองงาน
		</h2>
		<p>
			<?php
				// Set pename
				if($_SESSION['prename'] == 'A') {
					$prename = 'นาย';
				}elseif($_SESSION['prename'] == 'B') {
					$prename = 'นาง';
				}elseif($_SESSION['prename'] == 'C') {
					$prename = 'นางสาว';
				}elseif($_SESSION['prename'] == 'D') {
					$prename = 'ด.ช.';
				}elseif($_SESSION['prename'] == 'E') {
					$prename = 'ด.ญ.';
				}elseif($_SESSION['prename'] == 'O') {
					$prename = $_SESSION['prename_remark'];
				}
				$name = $_SESSION['firstname'].' '.$_SESSION['lastname']; // Set name
				$namewithprename = $prename.$name; // Set prename

				// Set position
				$occup_name = $_SESSION['occup_name'];
				$position = substr($occup_name, strrpos($occup_name, '*')+1);

				// Set School
				$affil_name = $_SESSION['affil_name'];
				$school = 'โรงเรียน'.substr($affil_name, strrpos($affil_name, '*')+1);

				echo $namewithprename.' ตำแหน่ง '.$position.' สังกัด '.$school;
			?>
			<br>
			ได้ประเมินตนเองตามมาตรฐานและตัวบ่งชี้ ครองตน ครองคน ครองงาน ของมูลนิธิครูดีของแผ่นดิน ผลเป็นดังนี้
		</p>
		<table style="border: 1px solid black">
			<!-- ################################################################################################ -->
			<!-- Start หัวข้อตาราง -->
			<tr>
				<th>หมวด</th>
				<th>ตัวบ่งชี้</th>
				<th>คะแนนเต็ม</th>
				<th>คะแนนที่ได้</th>
			</tr>
			<!-- End หัวข้อตาราง -->
      <!-- ################################################################################################ -->
			<!-- Start เนื้อหาตาราง -->
			<!-- ตัวบ่งชี้ที่ 1 -->
			<tr>
				<td rowspan="6" style="text-align: center; font-weight: bold;">ครองตน</td>
				<td style="padding-left: 20px">1. <?php echo $selfscore_sub1_topic; ?></td>
				<td style="text-align: center">5</td>
				<td style="text-align: center"><?php echo $selfscore_sub1_avg; ?></td>
			</tr>
			<!-- ตัวบ่งชี้ที่ 2 -->
			<tr>
				
				<td style="padding-left: 20px">2. <?php echo $selfscore_sub2_topic; ?></td>
				<td style="text-align: center">5</td>
				<td style="text-align: center"><?php echo $selfscore_sub2_avg; ?></td>
			</tr>
			<!-- ตัวบ่งชี้ที่ 3 -->
			<tr>
				
				<td style="padding-left: 20px">3. <?php echo $selfscore_sub3_topic; ?></td>
				<td style="text-align: center">5</td>
				<td style="text-align: center"><?php echo $selfscore_sub3_avg; ?></td>
			</tr>
			<!-- ตัวบ่งชี้ที่ 4 -->
			<tr>
				
				<td style="padding-left: 20px">4. <?php echo $selfscore_sub4_topic; ?></td>
				<td style="text-align: center">5</td>
				<td style="text-align: center"><?php echo $selfscore_sub4_avg; ?></td>
			</tr>
			<!-- ตัวบ่งชี้ที่ 5 -->
			<tr>
				
				<td style="padding-left: 20px">5. <?php echo $selfscore_sub5_topic; ?></td>
				<td style="text-align: center">5</td>
				<td style="text-align: center"><?php echo $selfscore_sub5_avg; ?></td>
			</tr>
			<!-- ตัวบ่งชี้ที่ 6 -->
			<tr>
				
				<td style="padding-left: 20px">6. <?php echo $selfscore_sub6_topic; ?></td>
				<td style="text-align: center">5</td>
				<td style="text-align: center"><?php echo $selfscore_sub6_avg; ?></td>
			</tr>
			<!-- สรุป หมวดครองตน -->
			<tr>
				<td colspan="2" rowspan="2" style="text-align: center; font-weight: bold;">สรุปหมวดครองตน</td>
				<td style="text-align: center; font-weight: bold;">คะแนนเฉลี่ย</td>
				<?php
					if($selfscore_avg >= 3) { ?>
						<td style="text-align: center; font-weight: bold;"><?php echo $selfscore_avg; ?></td> <?php
					}else{ ?>
						<td style="text-align: center; font-weight: bold;"><?php echo $selfscore_avg; ?></td> <?php
					}
				?>
			</tr>
			
			<tr>
				<td style="text-align: center; font-weight: bold;">สถานะ</td>
				<?php
					if($selfscore_avg >= 3) { ?>
						<td style="text-align: center; font-weight: bold;"><?php echo 'ผ่าน'; ?></td> <?php
					}else{ ?>
						<td style="text-align: center; font-weight: bold;"><?php echo 'ไม่ผ่าน'; ?></td> <?php
					}
				?>
			</tr>
			<!-- ################################################################################################ -->
			<!-- ตัวบ่งชี้ที่ 7 -->
			<tr>
				<td rowspan="3" style="text-align: center; font-weight: bold;">ครองคน</td>
				<td style="padding-left: 20px">7. <?php echo $peoplescore_sub7_topic; ?></td>
				<td style="text-align: center">5</td>
				<td style="text-align: center"><?php echo $peoplescore_sub7_avg; ?></td>
			</tr>
			<!-- ตัวบ่งชี้ที่ 8 -->
			<tr>
				
				<td style="padding-left: 20px">8. <?php echo $peoplescore_sub8_topic; ?></td>
				<td style="text-align: center">5</td>
				<td style="text-align: center"><?php echo $peoplescore_sub8_avg; ?></td>
			</tr>
			<!-- ตัวบ่งชี้ที่ 9 -->
			<tr>
				
				<td style="padding-left: 20px">9. <?php echo $peoplescore_sub9_topic; ?></td>
				<td style="text-align: center">5</td>
				<td style="text-align: center"><?php echo $peoplescore_sub9_avg; ?></td>
			</tr>
			<!-- สรุป หมวดครองคน -->
			<tr>
				<td colspan="2" rowspan="2" style="text-align: center; font-weight: bold;">สรุปหมวดครองตน</td>
				<td style="text-align: center; font-weight: bold;">คะแนนเฉลี่ย</td>
				<?php
					if($peoplescore_avg >= 3) { ?>
						<td style="text-align: center; font-weight: bold;"><?php echo $peoplescore_avg; ?></td> <?php
					}else{ ?>
						<td style="text-align: center; font-weight: bold;"><?php echo $peoplescore_avg; ?></td> <?php
					}
				?>
			</tr>
			<tr>
				<td style="text-align: center; font-weight: bold;">สถานะ</td>
				<?php
					if($peoplescore_avg >= 3) { ?>
						<td style="text-align: center; font-weight: bold;"><?php echo 'ผ่าน'; ?></td> <?php
					}else{ ?>
						<td style="text-align: center; font-weight: bold;"><?php echo 'ไม่ผ่าน'; ?></td> <?php
					}
				?>
			</tr>
      <!-- ################################################################################################ -->
			<!-- ตัวบ่งชี้ที่ 10 -->
			<tr>
				<td rowspan="3" style="text-align: center; font-weight: bold;">ครองงาน</td>
				<td style="padding-left: 20px">10. <?php echo $workscore_sub10_topic; ?></td>
				<td style="text-align: center">5</td>
				<td style="text-align: center"><?php echo $workscore_sub10_avg; ?></td>
			</tr>
			<!-- ตัวบ่งชี้ที่ 11 -->
			<tr>
				
				<td style="padding-left: 20px">11. <?php echo $workscore_sub11_topic; ?></td>
				<td style="text-align: center">5</td>
				<td style="text-align: center"><?php echo $workscore_sub11_avg; ?></td>
			</tr>
			<!-- ตัวบ่งชี้ที่ 12 -->
			<tr>
				
				<td style="padding-left: 20px">12. <?php echo $workscore_sub12_topic; ?></td>
				<td style="text-align: center">5</td>
				<td style="text-align: center"><?php echo $workscore_sub12_avg; ?></td>
			</tr>
			<!-- สรุป หมวดครองตน -->
			<tr>
				<td colspan="2" rowspan="2" style="text-align: center; font-weight: bold;">สรุปหมวดครองตน</td>
				<td style="text-align: center; font-weight: bold;">คะแนนเฉลี่ย</td>
				<?php
					if($workscore_avg >= 3) { ?>
						<td style="text-align: center; font-weight: bold;"><?php echo $workscore_avg; ?></td> <?php
					}else{ ?>
						<td style="text-align: center; font-weight: bold;"><?php echo $workscore_avg; ?></td> <?php
					}
				?>
			</tr>
			
			<tr>
				<td style="text-align: center; font-weight: bold;">สถานะ</td>
				<?php
					if($workscore_avg >= 3) { ?>
						<td style="text-align: center; font-weight: bold;"><?php echo 'ผ่าน'; ?></td> <?php
					}else{ ?>
						<td style="text-align: center; font-weight: bold;"><?php echo 'ไม่ผ่าน'; ?></td> <?php
					}
				?>
			</tr>

		</table>
		<!-- ################################################################################################ -->
	</section>
	<section style="margin-top: 30px;">
		<h2>ท่านผ่าน <?php echo $numberpass_sub; ?> หมวด จากทั้งหมด 3 หมวด</h2>
		<!-- แสดงคะแนนหมวดสูงสุด 3 อันดับ -->
		<p>ตัวบ่งชี้ที่ท่านได้คะแนนสูงสุด 3 อันดับ ได้แก่</p>
		<dd>
			<?php
				$i=1;
				foreach($max34sub as $key => $value) {
					echo '<dl>'.$i.'. '.$key.'</dl>';
					$i++;
				}
			?>
		</dd>
		<!-- แสดงคะแนนหมวดน้อยสุด 3 อันดับ -->
		<p>ตัวบ่งชี้ที่ท่านได้คะแนนน้อยสุด 3 อันดับ (ควรได้รับการพัฒนา) ได้แก่</p>
		<dd>
			<?php
				$i=1;
				foreach($min34sub as $key => $value) {
					echo '<dl>'.$i.'. '.$key.'</dl>';
					$i++;
				}
			?>
		</dd>
	</section>

	<h2>หมายเหตุ</h2>
	<p>
		๑. ผลจากการประเมินครั้งนี้ เป็นผลจากการประเมินตนเองเท่านั้น
		<br>
		<?php
			$sqllogin = "SELECT basic_score_date FROM `login` WHERE ID='$ID' ";
			$relogin = mysqli_query($con, $sqllogin);
			$rowlogin = mysqli_fetch_array($relogin);

			$date = date('Y-m-d', strtotime($rowlogin['basic_score_date']));
	
			/* Array for Thai Date */
			$arabicnum = array("1","2","3","4","5","6","7","8","9","0");
			$thainum = array("๑","๒","๓","๔","๕","๖","๗","๘","๙","๐");
			$test = str_replace($numthai,$numarabic,$message);
			/* Array for Thai Month */
			$month_arr=array(
				"1"=>"มกราคม",
				"2"=>"กุมภาพันธ์",
				"3"=>"มีนาคม",
				"4"=>"เมษายน",
				"5"=>"พฤษภาคม",
				"6"=>"มิถุนายน", 
				"7"=>"กรกฎาคม",
				"8"=>"สิงหาคม",
				"9"=>"กันยายน",
				"10"=>"ตุลาคม",
				"11"=>"พฤศจิกายน",
				"12"=>"ธันวาคม"                 
			);
			$basic_score_date = str_replace($arabicnum, $thainum, date('j', strtotime($date))).' '.$month_arr[date('n', strtotime($date))].' '.str_replace($arabicnum, $thainum, (date('Y', strtotime($date))+543)); // Set certicate date in Thai

		?>
		๒. ผลการประเมินตนเองครั้งนี้ ประเมินเมื่อวันที่ <?php echo $basic_score_date; ?>
	</p>
	<!-- End -->
	
<?php
	$html = ob_get_contents();
	$mpdf->WriteHTML($html);

	date_default_timezone_set("Asia/Bangkok");
	$date2day = date('Y-m-d');
	$target_dir = 'images/honor_score4sup/codeC/'.$date2day;
	$pdfname = '/'.$_SESSION['firstname'].' '.$_SESSION['lastname'].'.pdf';

	if(!is_dir($target_dir)) { // if there's not folder in target_directory
		mkdir($target_dir); // Create folder name is today_date
	}

	$mpdf->Output($target_dir.$pdfname);

	// Seve location of pdf file to honor_score4sup table
	$sqlhonor_score4sup = "SELECT * FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code='C' ";
	$reHS4S = mysqli_query($con, $sqlhonor_score4sup);

	$honor_score4sup_code = 'C';
	$honor_score4sup_data1 = $target_dir.$pdfname;

	if(mysqli_num_rows($reHS4S) == 0) {
		$sql = "INSERT INTO `honor_score4sup` (`ID`, `honor_score4sup_code`, `honor_score4sup_data1`)	VALUES ('$ID', '$honor_score4sup_code', '$honor_score4sup_data1') ";
		$re = $con->query($sql) or die($con->error); //Check error

		// Log User Action
		$sql = "INSERT INTO `scorelog` (`ID`,`scorelog_task`,`scorelog_detail`)	VALUES ('$ID','สร้างไฟล์PDF','ศน.,ขั้นเกียรติคุณ,รายงานคะแนน,แบบตาราง') ";
		$re = $con->query($sql) or die($con->error); //Check error

	}else{
		$sql ="UPDATE `honor_score4sup` SET `honor_score4sup_data1`='$honor_score4sup_data1' WHERE ID='$ID' AND honor_score4sup_code='C' ";
		$re = $con->query($sql) or die($con->error); //Check error
	}

	$rowHS4S = mysqli_fetch_array($reHS4S);

	header('location: '.$target_dir.$pdfname);

	ob_end_flush(); // End or Stop to buffer data to print on pdf file
?>

</body>
</html>