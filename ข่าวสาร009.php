<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
  $sql_list_project  ="SELECT * FROM join_project";
  $list_project = $con->query($sql_list_project);
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start NoAccount Session -->
<?php
      if (empty($_SESSION['email']))
      {
?>
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
          <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
          <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
          <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
            <ul>
              <li><a href="ข่าวสาร009-en.php"> ข่าวสาร</a></li>
              <!--<li><a href="#">Chinese</a></li>-->
            </ul>
          </li>
          <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
        </ul>
      </nav>
      <!-- End NoAccount Session -->
      <!-- ################################################################################################ -->
      <!-- Start Admin Session -->
<?php
      }
      elseif ($_SESSION['email'])
      {
        if ($_SESSION["level"]=="admin")
        {
?>
        <nav id="mainav2" class="PreMenu_fl_right">
          <ul class="PreMenufaico2">
            <li style="background-color:rgb(228,0,0);"><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="ข่าวสาร009-en.php"> ข่าวสาร</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Admin Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member Session -->
<?php
        if ($_SESSION["level"]=="memberGeneral")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> สมาชิกทั่วไป</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="ข่าวสาร009-en.php"> ข่าวสาร</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member SILVER Session -->
<?php
        if ($_SESSION["level"]=="memberSilver")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="ข่าวสาร009-en.php"> ข่าวสาร</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member SILVER Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member GOLD Session -->
<?php
        if ($_SESSION["level"]=="memberGold")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="ข่าวสาร009-en.php"> ข่าวสาร</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member GOLD Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member DIAMOND Session -->
<?php
        if ($_SESSION["level"]=="memberDiamond")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="ข่าวสาร009-en.php"> ข่าวสาร</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
      }
?>
      <!-- End Member DIAMOND Session -->
      <!-- ################################################################################################ -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px; ">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <?php 
                while($row = $list_project->fetch_assoc()){
                  $id_join_project = $row['id_join_project'];
                  $title_menu = $row['title_menu']; 
                
              ?>
              <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
              <?php } ?>
              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          </li>
          
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="ข่าวสารรวมlatest.php"> ข่าวสาร</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="ข่าวสาร009.php"> มูลนิธิครูดีของแผ่นดิน จับมือกับ Google จัดโรดโชว์ “กิจกรรมสร้างเสริมการใช้เทคโนโลยีดิจิทัลอย่างสร้างสรรค์ ปลอดภัย ผ่านหลักสูตร Be Internet Awesome”</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - ข่าวสาร009 -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <article style="text-align:center; border-bottom:3px solid #59A209; padding-bottom:50px;">
        <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px"><strong>มูลนิธิครูดีของแผ่นดิน จับมือกับ Google จัดโรดโชว์ 
          “กิจกรรมสร้างเสริมการใช้เทคโนโลยีดิจิทัลอย่างสร้างสรรค์ ปลอดภัย ผ่านหลักสูตร Be Internet Awesome”</strong></p>
          <img src="images/รูปข่าวBIA_11.jpg" alt="รูปข่าวBIA_11">
        <p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
          วันที่ 14 กุมภาพันธ์ พ.ศ.2563 ณ โรงเรียนพญาไท กรุงเทพ มูลนิธิครูดีของแผ่นดิน ร่วมกับ Google Thailand ได้จัดงานสร้างเสริมการใช้เทคโนโลยีดิจิทัลอย่างสร้างสรรค์ ปลอดภัย ผ่านหลักสูตร 
          Be Internet Awesome งานในครั้งนี้ได้รับเกียรติจากท่านรัฐมนตรีว่าการกระทรวงศึกษาธิการ นายณัฏฐพล ทีปสุวรรณ เป็นประธานในพิธี และ คุณแจ๊คกี้ หวาง Country Manager Google Thailand
        </p>
        <p class="font-x2" style="font-family:ChulaCharasNew; line-height:30pt; text-align:center;">
          <strong>ท่านรัฐมนตรีว่าการกระทรวงศึกษาธิการ นายณัฏฐพล ทีปสุวรรณ</strong>
          <img src="images/รูปข่าวBIA_8.jpg" alt="รูปข่าวBIA_8">
        </p>
        <p class="font-x2" style="font-family:ChulaCharasNew; line-height:30pt; text-align:center;">
          <strong>คุณแจ๊คกี้ หวาง Country Manager Google Thailand</strong>
          <img src="images/รูปข่าวBIA_7.jpg" alt="รูปข่าวBIA_7">
        </p>
        <p class="font-x2" style="font-family:ChulaCharasNew; line-height:30pt; text-align:left;">
          กิจกรรมวันนี้ ประกอบด้วย ๒ ส่วน ได้แก่ 
          <br>
          ๑. การอบรมสัมมนาแก่ผู้บริหาร และครู จำนวน ๑๘๐ คน จากสถานศึกษา ๙๐ แห่งทั่วประเทศ ทั้งจากสำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน (สพฐ.) สำนักการศึกษากรุงเทพมหานคร 
          สำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน (สช.) และองค์กรปกครองส่วนท้องถิ่น
            <img src="images/รูปข่าวBIA_18.jpg" alt="รูปข่าวBIA_18">
          <br><br>
          ๒. การเข้าฐานกิจกรรมการเรียนรู้ สำหรับนักเรียน จำนวน ๕ ฐาน โดยนักเรียนจะผลัดเปลี่ยนหมุนเวียนเข้าร่วมกิจกรรมดังกล่าวตลอดทั้งวัน
          <img src="images/รูปข่าวBIA_12.jpg" alt="รูปข่าวBIA_12">
          <br><br>
          งานในครั้งนี้เป็นหนึ่งในกิจกรรมการจัดโรดโชว์ของ Google  เพื่อสร้างเสริมการใช้เทคโนโลยีดิจิทัลอย่างสร้างสรรค์ ปลอดภัย ผ่านหลักสูตร Be Internet Awesome ซึ่งเป็นหลักสูตรที่พัฒนาขึ้นมาโดยเฉพาะเพื่อ
          ให้ความรู้กับเด็กๆ วัย 7-12 ปี เกี่ยวกับการสำรวจโลกออนไลน์ เพื่อให้พวกเขาสามารถใช้อินเทอร์เน็ตให้เกิดประโยชน์อย่างชาญฉลาด ปลอดภัย และไม่ก่อให้เกิดปัญหาสังคมอื่นๆ ตามมา รวมทั้งสอนให้เด็กๆ 
          เกี่ยวกับพื้นฐานการเป็นพลเมืองดิจิทัลที่ดี เพื่อการท่องโลกออนไลน์อย่างมั่นใจ นอกจากนี้ การจัดโรดโชว์ดังกล่าวยังเป็นการตอกย้ำความสำคัญของ “สัปดาห์อินเทอร์เน็ตปลอดภัย หรือ Safer Internet Week 
          2020” ที่มีการเฉลิมฉลองทั่วโลกอีกด้วย
          <br><br>
          การจัดโรดโชว์นำร่องในครั้งนี้ Google ได้รับเกียรติจากโรงเรียนพญาไท กรุงเทพ ให้เป็นสถานที่จัดการฝึกอบรมเป็นครั้งแรก โดยภายในงานโรดโชว์เปิดโอกาสให้เด็กๆ ได้เล่นเกม Interland บนแท็บเล็ตขนาดใหญ่
          ซึ่งทั้งสนุกสนานและสอดแทรกด้วยความรู้เกี่ยวกับการเป็นพลเมืองดิจิทัล ทั้งนี้ Google มีความมุ่งมั่นที่จะเดินหน้าจัดโรดโชว์ Be Internet Awesome ต่อไปภายในปีนี้
          <img src="images/รูปข่าวBIA_20.jpg" alt="รูปข่าวBIA_20">
        </p>
    </article>
    <p class="font-x3"><span style="color:rgb(180,147,31); line-height:30px;"><strong>คลังภาพ</strong></span></p>
    <div id="gallery" style="margin-top:50px">
      <ul class="nospace clear">
        <li class="one_third first zoom11"><a href="images/รูปข่าวBIA_1.jpg"><img src="images/รูปข่าวBIA_1.jpg" alt="รูปข่าวBIA_1"></a></li>
        <li class="one_third zoom11"><a href="images/รูปข่าวBIA_2.jpg"><img src="images/รูปข่าวBIA_2.jpg" alt="รูปข่าวBIA_2"></a></li>
        <li class="one_third zoom11"><a href="images/รูปข่าวBIA_3.jpg"><img src="images/รูปข่าวBIA_3.jpg" alt="รูปข่าวBIA_3"></a></li>
        <li class="one_third first zoom11"><a href="images/รูปข่าวBIA_4.jpg"><img src="images/รูปข่าวBIA_4.jpg" alt="รูปข่าวBIA_4"></a></li>
        <li class="one_third zoom11"><a href="images/รูปข่าวBIA_5.jpg"><img src="images/รูปข่าวBIA_5.jpg" alt="รูปข่าวBIA_5"></a></li>
        <li class="one_third zoom11"><a href="images/รูปข่าวBIA_6.jpg"><img src="images/รูปข่าวBIA_6.jpg" alt="รูปข่าวBIA_6"></a></li>
        <li class="one_third first zoom11"><a href="images/รูปข่าวBIA_9.jpg"><img src="images/รูปข่าวBIA_9.jpg" alt="รูปข่าวBIA_9"></a></li>
        <li class="one_third zoom11"><a href="images/รูปข่าวBIA_10.jpg"><img src="images/รูปข่าวBIA_10.jpg" alt="รูปข่าวBIA_10"></a></li>
        <li class="one_third zoom11"><a href="images/รูปข่าวBIA_19.jpg"><figure><img src="images/รูปข่าวBIA_19.jpg" alt="รูปข่าวBIA_19"></figure></a></li>
        <li class="one_third first zoom11"><a href="images/รูปข่าวBIA_13.jpg"><img src="images/รูปข่าวBIA_13.jpg" alt="รูปข่าวBIA_13"></a></li>
        <li class="one_third zoom11"><a href="images/รูปข่าวBIA_15.jpg"><img src="images/รูปข่าวBIA_15.jpg" alt="รูปข่าวBIA_15"></a></li>
        <li class="one_third zoom11"><a href="images/รูปข่าวBIA_16.jpg"><img src="images/รูปข่าวBIA_16.jpg" alt="รูปข่าวBIA_16"></a></li>
        <li class="one_third first zoom11"><a href="images/รูปข่าวBIA_17.jpg"><img src="images/รูปข่าวBIA_17.jpg" alt="รูปข่าวBIA_17"></a></li>
        <li class="one_third zoom11"><a href="images/รูปข่าวBIA_14.jpg"><img src="images/รูปข่าวBIA_14.jpg" alt="รูปข่าวBIA_14"></a></li>
      </ul>
    </div>
  </main>
</div>
<!-- End Content 04 - ข่าวสาร009 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>