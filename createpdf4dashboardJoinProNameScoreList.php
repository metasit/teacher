<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		if($_SESSION['vip_pass_status'] == 1) {

			$program = $_GET['program'];
			$program_name = $_GET['program_name'];
			$list = $_GET['list'];
			$list_name = $_GET['list_name'];

			// require_once to use mpdf library
			require_once __DIR__ . '/vendor/autoload.php';
			// Set Thai language for Mpdf
			$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
			$fontDirs = $defaultConfig['fontDir'];

			$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
			$fontData = $defaultFontConfig['fontdata'];

			$mpdf = new \Mpdf\Mpdf([
					'fontDir' => array_merge($fontDirs, [
							__DIR__ . '/tmp',
					]),
					'fontdata' => $fontData + [
							'sarabun' => [
									'R' => 'THSarabunNew.ttf',
									'I' => 'THSarabunNew Italic.ttf',
									'B' => 'THSarabunNew Bold.ttf',
									'BI' => 'THSarabunNew BoldItalic.ttf',
							]
					],
					'default_font' => 'sarabun'
			]);

		}else{
			echo '<script>';
				echo "alert('คุณต้องได้รับสิทธิ์ในการเข้าหน้านี้ค่ะ');";
				echo "window.history.go(-1)";
			echo '</script>';
		}
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<?php
		ob_start(); // Start to buffer data to print on pdf file<?php
	?>
	<style>table, td, th {
			border: 1px solid black;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		th {
			height: 50px;
		}

		td {
			height: 40px;
			text-align: center;
		}

		p, dd, table, td, th {
			font-size: 20px;
		}
	</style>
</head>

<!-- Start -->
<section class="center">
	<!-- ################################################################################################ -->
	<h2 style="text-align: center; font-size: 30px;">
		<?php
			echo $_GET['program_name'].' - '.$_GET['list_name'];
		?>
	</h2>

	
	<table class="table" style="background-color: rgb(240, 240, 240);">
		<thead>
			<tr>
				<th>ลำดับ</th>
				<th>ชื่อ-นามสกุล</th>
				<th>สังกัด</th>
				<?php
					if($program == 'tea' && $list == 'all') {
						
					}elseif($program == 'tea' && $list == 'basic_score_pass') { ?>

						<th>คะแนนประเมิน<?php echo '<br>(คะแนนเต็ม 30 คะแนน)'; ?></th> <?php

					}elseif($program == 'tea' && $list == 'fifth_score_proc') { ?>

						<th>คะแนนประเมิน<?php echo '<br>(แต่ละหมวดคะแนนเต็ม 5 คะแนน)<br>ประกอบด้วยหมวดครองตน, ครองคน และครองงาน'; ?></th> <?php

					}elseif($program == 'tea' && $list == 'fifth_score_pass') { ?>

						<th>คะแนนประเมิน<?php echo '<br>(แต่ละหมวดคะแนนเต็ม 5 คะแนน)<br>ประกอบด้วยหมวดครองตน, ครองคน และครองงาน'; ?></th> <?php

					}elseif($program == 'stu' && $list == 'all') {

					}elseif($program == 'stu' && $list == 'basic_score_pass') {

					}elseif($program == 'sup' && $list == 'all') {

					}elseif($program == 'sup' && $list == 'basic_score_pass') { ?>

						<th>คะแนนประเมิน<?php echo '<br>(คะแนนเต็ม 36 คะแนน)'; ?></th> <?php

					}elseif($program == 'sup' && $list == 'honor_score_proc') { ?>

						<th>คะแนนประเมิน<?php echo '<br>(คะแนนเต็ม 36 คะแนน)'; ?></th> <?php

					}elseif($program == 'sup' && $list == 'honor_score_pass') { ?>

						<th>คะแนนประเมิน<?php echo '<br>(คะแนนเต็ม 36 คะแนน)'; ?></th> <?php
									
					}
				?>
			</tr>
		</thead>
		<tbody class="disable-margin">
			<?php
				/* Set number per page */
				$results_per_page = 500;
				if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
				$start_from = ($page-1) * $results_per_page;

				/* Call data from mySQL */
				if($program == 'tea' && $list == 'all') {
					$sql = "SELECT firstname, lastname, affil_code, basic_score_total FROM `login` WHERE (occup_code LIKE 'OcAA%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin') 
					OR (occup_code LIKE 'OcAB%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
					ORDER BY firstname, lastname LIMIT $start_from, $results_per_page" ;

					
				}elseif($program == 'tea' && $list == 'basic_score_pass') {
					$sql = "SELECT firstname, lastname, affil_code, basic_score_total FROM `login` WHERE (occup_code LIKE 'OcAA%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
					OR (occup_code LIKE 'OcAB%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
					ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

				}elseif($program == 'tea' && $list == 'fifth_score_proc') {
					$sql = "SELECT fifth_score4tea.fifth_score4tea_code, login.affil_code, login.firstname, login.lastname 
					FROM `fifth_score4tea` 
					INNER JOIN `login` ON fifth_score4tea.ID=login.ID 
					WHERE fifth_score4tea.fifth_score4tea_code='A' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin'
					ORDER BY login.firstname, login.lastname LIMIT $start_from, $results_per_page ";

				}elseif($program == 'tea' && $list == 'fifth_score_pass') {
					$sql = "SELECT firstname, lastname, affil_code FROM `login` WHERE (occup_code LIKE 'OcAA%' AND fifth_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
					OR (occup_code LIKE 'OcAB%' AND fifth_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
					ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

				}elseif($program == 'stu' && $list == 'all') {
					$sql = "SELECT firstname, lastname, affil_code FROM `login` WHERE occup_code LIKE 'OcF%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' 
					ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

				}elseif($program == 'stu' && $list == 'basic_score_pass') {
					$sql = "SELECT firstname, lastname, affil_code FROM `login` 
					WHERE occup_code LIKE 'OcF%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' 
					ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

				}elseif($program == 'sup' && $list == 'all') {
					$sql = "SELECT firstname, lastname, affil_code FROM `login` WHERE occup_code LIKE 'OcAD%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin'
					ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

				}elseif($program == 'sup' && $list == 'basic_score_pass') {
					$sql = "SELECT firstname, lastname, affil_code, basic_score_total FROM `login` WHERE occup_code LIKE 'OcAD%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin'
					ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

				}elseif($program == 'sup' && $list == 'honor_score_proc') {
					$sql = "SELECT honor_score4sup.honor_score4sup_code, login.firstname, login.lastname, login.affil_code 
					FROM `honor_score4sup` 
					INNER JOIN `login` ON honor_score4sup.ID=login.ID
					WHERE honor_score4sup.honor_score4sup_code='A' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin'
					ORDER BY login.firstname, login.lastname LIMIT $start_from, $results_per_page ";

				}elseif($program == 'sup' && $list == 'honor_score_pass') {
					$sql = "SELECT firstname, lastname, affil_code FROM `login` WHERE occup_code LIKE 'OcAD%' AND honor_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' 
					ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";
								
				}

				$re = mysqli_query($con, $sql);

				$n = $start_from+1; // Get number of row
				while($row = $re->fetch_assoc()) {
					
					$name = trim($row['firstname']).' '.trim($row['lastname']);
					
					$affil_code = $row['affil_code'];
					include('includes/convert2afftext.php'); ?>

					<tr>
						<!-- No. -->
						<td class="price-pr">
							<p><?php echo $n ?></p>
						</td>
						<!-- Name -->
						<td class="name-pr lh-1-0">
							<p><?php echo $name; ?></p>
						</td>
						<!-- สังกัด-->
						<td class="name-pr lh-1-0">
							<p><?php echo $full_affiliation_name; ?></p>
						</td>
						<!-- คะแนน-->
						<?php
							if($program == 'tea' && $list == 'all') {
								
							}elseif($program == 'tea' && $list == 'basic_score_pass') { ?>

								<td class="price-pr">
									<p><?php echo $row['basic_score_total']; ?></p>
								</td> <?php

							}elseif($program == 'tea' && $list == 'fifth_score_proc') { ?>

								<td class="price-pr">
									<p><?php if($row['fifth_score4tea_detail'] == ''){echo 'กำลังดำเนินการ';}else{echo $row['fifth_score4tea_detail'];} ?></p>
								</td> <?php

							}elseif($program == 'tea' && $list == 'fifth_score_pass') { ?>

								<td class="price-pr">
									<p><?php echo $row['fifth_score4tea_detail']; ?></p>
								</td> <?php

							}elseif($program == 'stu' && $list == 'all') {

							}elseif($program == 'stu' && $list == 'basic_score_pass') {

							}elseif($program == 'sup' && $list == 'all') {

							}elseif($program == 'sup' && $list == 'basic_score_pass') { ?>

								<td class="price-pr">
									<p><?php echo $row['basic_score_total']; ?></p>
								</td> <?php

							}elseif($program == 'sup' && $list == 'honor_score_proc') { ?>

								<td class="price-pr">
									<p><?php if($row['honor_score4sup_data2'] == ''){echo 'กำลังดำเนินการ';}else{echo $row['honor_score4sup_data2'];} ?></p>
								</td> <?php

							}elseif($program == 'sup' && $list == 'honor_score_pass') { ?>

								<td class="price-pr">
									<p><?php echo str_replace(',', ' ', substr($row['honor_score4sup_data2'], 1)); ?></p>
								</td> <?php
											
							}
						?>
					</tr> <?php
					$n++;
					$selfscore = 0;
					$peoplescore = 0;
					$workscore = 0;
					$k = 0;
				}
			?>
		</tbody>
	</table>
	<!-- ################################################################################################ -->
</section>
<!-- End -->
	
<?php
	$html = ob_get_contents();
	$mpdf->WriteHTML($html);

	$target_dir = 'images/dashboard/รายการผู้เข้าร่วมโครงการ';
	$pdfname = '/'.$_SESSION['firstname'].' '.$_SESSION['lastname'].'.pdf';

	if(!is_dir($target_dir)) { // if there's not folder in target_directory
		mkdir($target_dir); // Create folder name is today_date
	}

	$mpdf->Output($target_dir.$pdfname);

	header('location: '.$target_dir.$pdfname);

	ob_end_flush(); // End or Stop to buffer data to print on pdf file
?>

</body>
</html>