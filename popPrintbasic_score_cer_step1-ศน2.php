<?php
	session_start();
	require_once('condb.php');

	$ID = $_SESSION['ID'];
	$sqllogin = "SELECT * FROM `login` WHERE ID='$ID' ";
	$relogin = mysqli_query($con,$sqllogin);
	$rowlogin = mysqli_fetch_array($relogin);

	if(!$_SESSION['ID']) {
		header('location: login.php');
	}else{
		}if($rowlogin['basic_score_status'] == 'Approve แล้ว') {
				
		}elseif($rowlogin['basic_score_status'] == 'Approve แล้ว/ยืนยันแล้ว') {
			header('location: popPrintbasic_score_cer_step2-ศน.php');
		}else{
			echo "<script>window.history.go(-1)</script>";
		}
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<span class="login100-form-title fs-30 lh-1-1">
					ยินดีด้วยค่ะ
					<br>
					ท่านได้รับรางวัลศึกษานิเทศก์ดีของแผ่นดิน ขั้นพื้นฐาน
					<br><br>
					ตรวจสอบและยืนยันการใช้ชื่อ-นามสกุล และแจ้งชื่อโรงเรียนของท่านที่จะให้พิมพ์บนเกียรติบัตร
					<br>
					(หลังจากเปลี่ยนหรือยืนยันการใช้ชื่อ-นามสกุลนี้ ท่านจะไม่สามารถเปลี่ยนแปลงได้อีกต่อไป โปรดตรวจสอบชื่อ-นามสกุลอย่างละเอียด)
					<br><br>
					ชื่อ-นามสกุลที่ท่านให้ไว้ตอนสมัครสมาชิก
					<br>
					<form action="popPrintbasic_score_cer_step2-ศน.php" method="POST">
						<div class="wrap-input100 validate-input">
							<input class="input100" type="text" name="firstname" class="form-control" placeholder="<?php echo $_SESSION['firstname'];?>" max-lenght="100" />
							<span class="symbol-input100">
								<i class="fas fa-file-signature" aria-hidden="true"></i>
							</span>
						</div>
						<div class="wrap-input100 validate-input">
							<input class="input100" type="text" name="lastname" class="form-control" placeholder="<?php echo $_SESSION['lastname'];?>" max-lenght="100" />
							<span class="symbol-input100">
								<i class="fas fa-file-signature" aria-hidden="true"></i>
							</span>
						</div>
						โปรดใส่ชื่อโรงเรียน
						<div class="wrap-input100 validate-input">
							<input class="input100" type="text" name="school" class="form-control" placeholder="ชื่อโรงเรียน" max-lenght="300" required />
							<span class="symbol-input100">
								<i class="fas fa-school" aria-hidden="true"></i>
							</span>
						</div>
						<br>
						<input type="hidden" name="CFP" value="20">
						<button type="submit" class="ml-auto btn hvr-hover fs-30" >กดเพื่อยืนยัน</button>
					</form>
				</span>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>