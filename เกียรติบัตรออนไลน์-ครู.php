<?php
	session_start();
	require_once('condb.php');

	/* Save เกียนติบัตรออนไลน์ with user name */
	$jpg_image = imagecreatefromjpeg('images/เกียรติบัตร/พื้นฐาน/ครู/เกียรติบัตรขั้นพื้นฐาน-ครู.jpg'); // Create Image From Existing File
	$font_color = imagecolorallocate($jpg_image, 0, 0, 0); // Set font color
	$font_path = 'layout/styles/fonts/RSU_Text_Bold.ttf'; // Set font file path
	// Set Text that need to be Printed On Image
	$firstname = $_SESSION['firstname'].' '; // Set firstname
	$lastname = ' '.$_SESSION['lastname']; // Set lastname
	//Set date
	$sqladscorelog = "SELECT * FROM `adscorelog` WHERE ID=1 ORDER BY adscorelog_id DESC LIMIT 1 ";
	$readscorelog = mysqli_query($con,$sqladscorelog);
	$rowadscorelog = mysqli_fetch_array($readscorelog);
	$date = new DateTime($rowadscorelog['adscorelog_date']);
	$date = $date->format("Y-m-d"); // Set date format

	/* Array for Month to Thai */
	$month_arr=array(
		"1"=>"มกราคม",
		"2"=>"กุมภาพันธ์",
		"3"=>"มีนาคม",
		"4"=>"เมษายน",
		"5"=>"พฤษภาคม",
		"6"=>"มิถุนายน", 
		"7"=>"กรกฎาคม",
		"8"=>"สิงหาคม",
		"9"=>"กันยายน",
		"10"=>"ตุลาคม",
		"11"=>"พฤศจิกายน",
		"12"=>"ธันวาคม"                 
	);
	$date_cer = date('j', strtotime($date)).' '.$month_arr[date('n', strtotime($date))].' '.(date('Y', strtotime($date))+543); // Set certicate date in Thai

	$font_size = 110; //Set font size
	$angle = 0; //Set angle
	/* Set x-position on certificate for firstname */
	$dimensions = imagettfbbox($font_size, $angle, $font_path, $firstname);
	$textWidth = abs($dimensions[4] - $dimensions[0]);
	$x = 1789.5 - $textWidth;
	$y = 1050; // Set y-position on certificate for firstname and lastname
	/* Print firstname, lastname and date on certificate */
	imagettftext($jpg_image, $font_size, $angle, $x, $y, $font_color, $font_path, $firstname);
	imagettftext($jpg_image, $font_size, $angle, 1789.5, $y, $font_color, $font_path, $lastname);
	imagettftext($jpg_image, $font_size, $angle, 1710, 1718, $font_color, $font_path, $date_cer);

	$target_dir = 'images/เกียรติบัตร/พื้นฐาน/ครู/'.$date.'/'; // Set target_directory

	if(!is_dir($target_dir)) { // if there's not folder in target_directory
		mkdir($target_dir); // Create folder name is today_date
	}

	imagejpeg($jpg_image,'images/เกียรติบัตร/พื้นฐาน/ครู/'.$date.'/'.$_SESSION['firstname'].' '.$_SESSION['lastname'].'.jpg');// Send Image to Browser or save in directory on client
	imagedestroy($jpg_image); // Clear Memory	
?>