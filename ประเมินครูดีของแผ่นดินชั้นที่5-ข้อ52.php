<?php 
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	$question_num = 52; // Set question no. for this page
	require('includes/header4fifth_evaluation.php');
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="m-t-90"></div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ประเมินครูดีของแผ่นดินชั้นที่5 -->
<div class="wrapper row3">
	<div class="hoc container clear">
		<!-- ################################################################################################ -->
		<div class="center">
			<h8 class="m-b-50">หมวด: ครองงาน</h8>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-6 col-md-6 col-lg-6 fs-20">
				<p>ข้อที่ / ทั้งหมด</p>
				<p class="bold"><?php echo $question_num; ?> / 79</p>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 right fs-20">
				<p>เหลือเวลา</p>
				<div>
					<p class="bold m-t-1" id="timer" onload="localStorage.getItem('currenttime')">.</p> วินาที
				</div>
			</div>
		</div>

		<div class="row p-t-20 p-l-10 m-t-20" style="border-top:2px solid rgb(16,192,0); border-radius:50px;">
			<div class="col-md-12 col-lg-12">
				<form action="addfifth_score_A-ครู.php" method="GET">
					<div class="fs-20 inline" style="text-align:left;">
						<p>
							<strong>
								<?php echo $question_num; ?>. ท่านมีลักษณะเด่นตรงตามข้อใดบ้าง
								<br>
								ก. จัดการเรียนการสอนด้วยวิธีการใหม่ ๆ หลากหลาย
								<br>
								ข. สร้างผลงานที่มีประสิทธิภาพดีขึ้นอย่างต่อเนื่อง
								<br>
								ค. ทำงานที่คนอื่นว่ายาก ให้สำเร็จได้ด้วยดี
								<br>
								ง. ปฏิบัติงานภายใต้ข้อจำกัดด้านเวลาและทรัพยากรให้สำเร็จได้
							</strong>
						</p>

						<br>
						<input type="radio" id="choice1" name="fifth_score_A" value="1" required>
						<label for="choice1">ไม่มีลักษณะในข้อใดตรงกับท่านเลย</label>
						<br>
						<input type="radio" id="choice2" name="fifth_score_A" value="2" required>
						<label for="choice2">มีลักษณะตามข้อข้างต้น 1 ข้อ</label>
						<br>
						<input type="radio" id="choice3" name="fifth_score_A" value="3" required>
						<label for="choice3">มีลักษณะตามข้อข้างต้น 2 ข้อ</label>
						<br>
						<input type="radio" id="choice4" name="fifth_score_A" value="4" required>
						<label for="choice4">มีลักษณะตามข้อข้างต้น 3 ข้อ</label>
						<br>
						<input type="radio" id="choice5" name="fifth_score_A" value="5" required>
						<label for="choice5">มีลักษณะตามข้อข้างต้นครบทุกข้อ</label>
						<br>
					</div>

					<button type="submit" id="nextBtn" class="btnJoin" style="color:white; cursor:pointer; width:80%; margin-top:50px;"><h7>ข้อถัดไป</h7></button>
					<input type="hidden" id="CFP" name="CFP" value="<?php echo $question_num; ?>">
				</form>
			</div>
		</div>
	</div>
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ประเมินครูดีของแผ่นดินชั้นที่5 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>
<!-- Countdown Timer -->
<script src="js/countdowntimer.js"></script>
<!-- Disable Back btn -->
<script src="js/disablebackbtn4fifth_evaluation4tea.js"></script>

</body>
</html>