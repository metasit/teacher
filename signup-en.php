<?php
	session_start();
	if(isset($_SESSION['ID'])) {
		echo "<script>window.history.go(-1)</script>";
	}else{
		require_once('condb.php');
	}
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>Foundation of Thai Suprateacher</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="index-en.php"><i class="fas fa-arrow-left"></i> Back to Home</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_right" style="padding:2px;">
					<a href="login-en.php">Log in</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" name="formsignup" action="savesignup-en.php" method="POST">
					<!-- ################################################################################################ -->
					<span class="login100-form-title">
						Member System
					</span>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="email" name="email" class="form-control" placeholder="Email" 
						pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$" title="Please enter a valid email address" required />
						<span class="symbol-input100">
							<i class="fas fa-user" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="password" name="password" class="form-control" placeholder="Password" 
						pattern=".{8,}" title="Please enter at least 8 characters." required />
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="password" name="conpassword" class="form-control" placeholder="Confirm Password" 
						pattern=".{8,}" title="Please enter at least 8 characters." required />
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
				<!-- ################################################################################################ -->
				<select name="pre_name" class="form-control" style="margin-bottom:10px; height:40px;" onchange="PickPrename(this.value);" required>
					<option value="" disabled="disabled" selected="selected">Choose your prefix name</option>
					<option value="A">Mr.</option>
					<option value="B">Ms.</option>
					<option value="C">Mrs.</option>
					<option value="D">Master/Mstr.</option>
					<option value="E">Miss</option>
					<option value="O">Others</option>
				</select>
				<div id="pre_name_text" class="wrap-input100 validate-input" style="display:none">
					<input class="input100" type="text" id="pre_name_text_required" name="pre_name_text" class="form-control" placeholder="โปรดระบุคำหน้านาม" max-lenght="30" />
					<span class="symbol-input100">
						<i class="fas fa-file-signature" aria-hidden="true"></i>
					</span>
				</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="firstname" class="form-control" placeholder="Firstname" max-lenght="100" required />
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="lastname" class="form-control" placeholder="Lastname" max-lenght="100" required />
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="tel" name="phonenum" class="form-control" placeholder="Mobile Number" pattern="[0][0-9]{9}" maxlength="10"
						title="Please enter 10 numbers, including the first 0 (Zero) number." required />
						<span class="symbol-input100">
							<i class="fas fa-mobile-alt" aria-hidden="true"></i>
						</span>
						<em>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="docaddress" class="form-control" placeholder="Documentary Address" required />
						<span class="symbol-input100">
							<i class="fas fa-address-book" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="date" name="birthdate" class="form-control" placeholder="Birthdate"
						min='1950-01-01' max="<?php echo date('Y-m-d'); ?>" required />
						<span class="symbol-input100">
							<i class="fas fa-birthday-cake" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit" id="register">
							Sign up
						</button>
					</div>
					<!-- ################################################################################################ -->
					<input type="hidden" name="CFP" value="20">
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	 
<script>
	function PickPrename(val) {
		var elePrename = document.getElementById('pre_name');
		var elePrenameText = document.getElementById('pre_name_text');
		var elePrenameTextReq = document.getElementById('pre_name_text_required');
		if(val == 'O') {
			elePrenameText.style.display = 'block';
			elePrenameTextReq.required = true;
		}else{
			elePrenameText.style.display = 'none';
			elePrenameText.value = '';
			elePrenameTextReq.required = false;
		}
	}
</script>

</body>
</html>