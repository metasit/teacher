</div>


		<div style="width:100%; margin-top:20px; font-size:23px; line-height:30px; margin-top:58px;">
			<div style="text-align:center;">
				ใบเสร็จรับเงิน
				<br>
				RECEIPT
			</div>
			<div style="width:80%; display:inline-block; text-align:right;">
					วันที่
			</div>
			<div style="width:19.4%; display:inline-block; text-align:left; padding-left:20px">
				<?php
					$month_arr=array(
						"1"=>"มกราคม",
						"2"=>"กุมภาพันธ์",
						"3"=>"มีนาคม",
						"4"=>"เมษายน",
						"5"=>"พฤษภาคม",
						"6"=>"มิถุนายน", 
						"7"=>"กรกฎาคม",
						"8"=>"สิงหาคม",
						"9"=>"กันยายน",
						"10"=>"ตุลาคม",
						"11"=>"พฤศจิกายน",
						"12"=>"ธันวาคม"                 
				);
				
				echo date("d", strtotime($rowDN['donate_date']))." ".$month_arr[date("n", strtotime($rowDN['donate_date']))]." ".(date("Y", strtotime($rowDN['donate_date']))+543);
			?>
			</div>
			<div style="width:80%; display:inline-block; text-align:right;">
				เลขที่
			</div>
			<div style="width:19.4%; display:inline-block; text-align:left; padding-left:20px">
				<?php echo $rowDN['bill_code']; ?>
			</div>
				
			<div style="width:25%; display:inline-block; text-align:left;">
				ได้รับเงินจาก
			</div>
			<div style="width:74.4%; display:inline-block; text-align:left;">
				<?php echo $donate_name; ?>
			</div>
			<div style="width:25%; display:inline-block; text-align:left;">
				ที่อยู่
			</div>
			<div style="width:74.4%; display:inline-block; text-align:left;">
				<?php echo $donate_address; ?>
			</div>
			<div style="width:30%; display:inline-block; text-align:left;">
				<?php
					if($rowDN['bill_tax_kind'] == 'Personal' || $rowDN['bill_tax_kind'] == 'Corporation') { ?>

						เลขประจำตัวผู้เสียภาษีอากร <?php

					}elseif($rowDN['bill_tax_kind'] == 'Government') { ?>

						หน่วยงานราชการ <?php
						
					}else{ ?>

						เลขประจำตัวผู้เสียภาษีอากร <?php

					}
				?>				
			</div>

			<div style="width:25%; display:inline-block; text-align:left;">
				<?php echo $rowDN['bill_tax_id']; ?>
			</div>
			<div style="width:21%; display:inline-block; text-align:left;">
				<i class="fa-square fs-40"></i> สำนักงานใหญ่ &nbsp;&nbsp;&nbsp;&nbsp; 
			</div>
			<div style="width:20%; display:inline-block; text-align:left;">
				<i class="fa-square fs-40"></i> สาขา.....
			</div>

			

			<div style=" text-align:left; margin-top:40px; ">
					บริจาคเพื่อสนับสนุนการดำเนินการของมูลนิธีครูดีของแผ่นดิน
			<div>

			<table style="width:100%; margin-top:40px;">
				<thead>
					<tr>
						<th>
							<!-- ลำดับที่ -->
						</th>
						<th>
							<!-- รายการ -->
						</th>
						<th>
							<!-- ราคาต่อหน่วย -->
						</th>
						<th>
							<!-- จำนวน -->
						</th>
						<th>
							<!-- ราคา -->
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="text-align:left; padding-left:0px; ">
							<!-- 1 -->
							เป็นจำนวนเงิน
						</td>
						<td style="text-align:left;">
							<!-- บริจาคสนับสนุนโครงการต่างๆ -->
							<?php
							 echo number_format($donate_amount, 2);
							 ?>
						</td>
						<td style="padding-left:50px">
						--<?php 	echo Convert($donate_amount) ?>--
						</td>
						<td>
							<!-- 1 -->
						</td>
						<td style="text-align:right; padding-right:0;">
						<!-- <?php echo number_format($donate_amount, 2); ?> -->
						</td>
					</tr>


					<tr>
						<td style="text-align:left; padding-left:0px; "></td>
						<td style="text-align:left;"></td>
						<td style="padding-left:50px">
								<div style="margin-top:20px">
									....................................
								</div>
								<div>ผู้รับเงิน</div>
								<div>1 มกราคม 2564</div>
						</td>
						<td></td>
						<td style="text-align:right; padding-right:0;">
						<!-- <?php echo number_format($donate_amount, 2); ?> -->
						</td>
					</tr>

				</tbody>
			</table>
			<div style="width:49.5%; display:inline-block; text-align:right; font-weight:700;">
				<!-- (<?php echo Convert($donate_amount); ?>) -->
			</div>
			<div style="width:24.4%; display:inline-block; text-align:right; font-weight:700;">
				<!-- ยอดรวม -->
			</div>
			<div style="width:25%; display:inline-block; text-align:right; font-weight:700;">
				<!-- <?php echo number_format($donate_amount, 2); ?> -->
			</div>
			
			<br><br>

			<!-- <div style="width:50%; display:inline-block; text-align:left; font-weight:700;">
				<?php
				//  echo number_format($donate_amount, 2);
				?>
				<span style="margin-right:20px;">หมายเหตุ</span>
				<span>ยังไม่สามารถนำไปใช้หย่อนภาษีได้</span>
			</div>	 -->
			
			<!-- <div style="width:49.2%; display:inline-block; text-align:center; margin-top:20px;">
				ตามแบบตัวอย่าง
			</div>
			<div style="width:49.2%; display:inline-block; text-align:center; margin-top:20px;">
				
			</div>

			<div style="width:49.2%; display:inline-block; text-align:center; margin-top:20px;">
				..............................................
			</div>
			<div style="width:49.2%; display:inline-block; text-align:center;">
				..............................................
			</div>
			<div style="width:49.2%; display:inline-block; text-align:center;">
				ผู้รับเงิน
			</div>
			<div style="width:49.2%; display:inline-block; text-align:center;">
				ผู้รับสินค้า
			</div>
			<div style="width:49.2%; display:inline-block; text-align:center;">
				วันที่...........................................
			</div> -->
			<div style="width:49.2%; display:inline-block; text-align:center;">
				<!-- วันที่........................................... -->
			</div>
		</div>
	</div>