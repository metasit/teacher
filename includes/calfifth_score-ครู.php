<?php

	if(!$ID){
		$ID = $_SESSION['ID'];
	}
	$sqlfifth_score4tea = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='A'  ";
	$reFS4T = mysqli_query($con, $sqlfifth_score4tea);
	$rowFS4T = mysqli_fetch_array($reFS4T);


	// if(in_array("fifth_score4tea_data",array_keys($rowFS4T)))
	// {
	// 	$fifth_score4tea_data = $rowFS4T['fifth_score4tea_data'];}
	// else {
	// 	$fifth_score4tea_data = 0;	
	// }
	
	$fifth_score4tea_data = $rowFS4T['fifth_score4tea_data'];
	// หมวดครองตน NOTE: มี9ตัวบ่งชี้
	for($x=1; $x<=38; $x++) {
		$i = strpos($fifth_score4tea_data, '.'); // Find position of each score
		$j = substr($fifth_score4tea_data, $i+1, 1); // Return value of each score

		$score[$x] = $j; // Set valuable for each score
		$selfscore += (float) $score[$x]; // Accumulate all score
		
		if( !isset($selfscore_sub1) ) $selfscore_sub1 = 0;
		if( !isset($selfscore_sub2) ) $selfscore_sub2 = 0;
		if( !isset($selfscore_sub3) ) $selfscore_sub3 = 0;
		if( !isset($selfscore_sub4) ) $selfscore_sub4 = 0;
		if( !isset($selfscore_sub5) ) $selfscore_sub5 = 0;
		if( !isset($selfscore_sub6) ) $selfscore_sub6 = 0;
		if( !isset($selfscore_sub7) ) $selfscore_sub7 = 0;
		if( !isset($selfscore_sub8) ) $selfscore_sub8 = 0;
		if( !isset($selfscore_sub9) ) $selfscore_sub9 = 0;

		
		
		if($x>=1 && $x<=5) { // ตัวบ่งชี้ที่1: รัก เข้าใจ ห่วงใยลูกศิษย์
			$selfscore_sub1 += (float) $score[$x];
		}elseif($x>=6 && $x<=10) { //ตัวบ่งชี้ที่2: หน้าที่ครูต่อศิษย์ตามหลักทิศ 6
			$selfscore_sub2 += (float) $score[$x];
		}elseif($x>=11 && $x<=14) { //ตัวบ่งชี้ที่3: ไม่สร้างหนี้เพิ่ม
			$selfscore_sub3 += (float) $score[$x];
		}elseif($x>=15 && $x<=18) { //ตัวบ่งชี้ที่4: จิตสาธารณะ
			$selfscore_sub4 += (float) $score[$x];
		}elseif($x>=19 && $x<=26) { //ตัวบ่งชี้ที่5: ปฏิบัติตนอยู่ในศีล 5 และความดีสากล 5
			$selfscore_sub5 += (float) $score[$x];
		}elseif($x>=27 && $x<=30) { //ตัวบ่งชี้ที่6: มีความรับผิดชอบ พูดอย่างไรทำอย่างนั้น ทำอย่างไรพูดอย่างนั้น
			$selfscore_sub6 += (float) $score[$x];
		}elseif($x>=31 && $x<=33) { //ตัวบ่งชี้ที่7: เรียนรู้ตลอดชีวิต
			$selfscore_sub7 += (float) $score[$x];
		}elseif($x>=34 && $x<=36) { //ตัวบ่งชี้ที่8: รู้เท่าทันตนเอง
			$selfscore_sub8 += (float) $score[$x];
		}elseif($x>=37 && $x<=38) { //ตัวบ่งชี้ที่9: ละอายและเกรงกลัวต่อบาป
			$selfscore_sub9 += (float) $score[$x];
		}
		// var_dump($score[$x]);
		// exit();
		$fifth_score4tea_data = substr($fifth_score4tea_data, $i+1);
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// หมวดครองคน NOTE: มี3ตัวบ่งชี้
	for($x=39; $x<=49; $x++) {
		$i = strpos($fifth_score4tea_data, '.'); // Find position of each score
		$j = substr($fifth_score4tea_data, $i+1, 1); // Return value of each score

		if( !isset($peoplescore) ) $peoplescore = 0;
		$score[$x] = $j; // Set valuable for each score
		 $peoplescore += (float) $score[$x]; // Accumulate all score

		if( !isset($peoplescore_sub10) ) $peoplescore_sub10= 0;
		if( !isset($peoplescore_sub11) ) $peoplescore_sub11= 0;
		if( !isset($peoplescore_sub12) ) $peoplescore_sub12= 0;

		if($x>=39 && $x<=41) { //ตัวบ่งชี้ที่10: เป็นผู้หวังดีให้ด้วยจิตเมตตา
			$peoplescore_sub10 += (float) $score[$x];
		}elseif($x>=42 && $x<=44) { //ตัวบ่งชี้ที่11: ปฏิบัติตนตามแบบแผน และปรับแนวคิดให้ถูกต้องตรงกัน
			$peoplescore_sub11 += (float) $score[$x];
		}elseif($x>=45 && $x<=49) { //ตัวบ่งชี้ที่12: ปฏิบัติตนกับบุคคลรอบข้างอย่างเหมาะสม
			$peoplescore_sub12 += (float) $score[$x];
		}

		$fifth_score4tea_data = substr($fifth_score4tea_data, $i+1);
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// หมวดครองงาน NOTE: มี7ตัวบ่งชี้
	for($x=50; $x<=79; $x++) {
		$i = strpos($fifth_score4tea_data, '.'); // Find position of each score
		$j = substr($fifth_score4tea_data, $i+1, 1); // Return value of each score
		if( !isset($workscore) ) $workscore = 0;
		$score[$x] = $j; // Set valuable for each score
		$workscore += (float) $score[$x]; // Accumulate all score
		
		if( !isset($workscore_sub13) ) $workscore_sub13 = 0;
		if( !isset($workscore_sub14) ) $workscore_sub14 = 0;
		if( !isset($workscore_sub15) ) $workscore_sub15 = 0;
		if( !isset($workscore_sub16) ) $workscore_sub16 = 0;
		if( !isset($workscore_sub17) ) $workscore_sub17 = 0;
		if( !isset($workscore_sub18) ) $workscore_sub18 = 0; 
		if( !isset($workscore_sub19) ) $workscore_sub19 = 0;


		if($x>=50 && $x<=52) { //ตัวบ่งชี้ที่13: มีความคิดสร้างสรรค์ (Creativity)
			$workscore_sub13 += (float) $score[$x];
		}elseif($x>=53 && $x<=58) { //ตัวบ่งชี้ที่14: มีการคิดวิเคราะห์ (Critical Thinking)
			$workscore_sub14 += (float) $score[$x];
		}elseif($x>=59 && $x<=62) { //ตัวบ่งชี้ที่15: มีทักษะด้านความร่วมมือและการทำงานเป็นทีม (Collaboration and Teamwork)
			$workscore_sub15 += (float) $score[$x];
		}elseif($x>=63 && $x<=65) { //ตัวบ่งชี้ที่16: มีทักษะสื่อสาร (Communication)
			$workscore_sub16 += (float) $score[$x];
		}elseif($x>=66 && $x<=68) { //ตัวบ่งชี้ที่17: รู้คอมพิวเตอร์และไอซีที (Computing and ICT Literacy)
			$workscore_sub17 += (float) $score[$x];
		}elseif($x>=69 && $x<=76) { //ตัวบ่งชี้ที่18: มีทักษะอาชีพและทักษะการเรียนรู้ (Career and Learning Skill)
			$workscore_sub18 += (float) $score[$x];
		}elseif($x>=77 && $x<=79) { //ตัวบ่งชี้ที่19: ผสมผสานทางวัฒนธรรม (Cross-cultural)
			$workscore_sub19 += (float) $score[$x];
		}
		$fifth_score4tea_data = substr($fifth_score4tea_data, $i+1);
	}

	

	// สรุปคะแนนรวม แยกเป็นรายหมวด
	if(!isset($selfscore_avg)) $selfscore_avg = 0;
	if(!isset($selfscore)) $selfscore = 0;
	$selfscore_avg = number_format($selfscore/38, 2);
	$peoplescore_avg = number_format($peoplescore/11, 2);
	$workscore_avg = number_format($workscore/30, 2);
	// หาจำนวนหมวดที่ผ่าน
	if($selfscore_avg >= 3) {
		$numberpass_sub = 1;
	}else{
		$numberpass_sub = 0;
	}

	if($peoplescore_avg >= 3) {
		$numberpass_sub += 1;
	}else{
		$numberpass_sub += 0;
	}

	if($workscore_avg >= 3) {
		$numberpass_sub += 1;
	}else{
		$numberpass_sub += 0;
	}
	// สรุปคะแนน เป็นจุดทศนิยม2ตำแหน่ง ของแต่ละ ตัวบ่งชี้
	$selfscore_sub1_avg = number_format($selfscore_sub1/5, 2); $selfscore_sub1 = 0;
	$selfscore_sub2_avg = number_format($selfscore_sub2/5, 2); $selfscore_sub2 = 0;
	$selfscore_sub3_avg = number_format($selfscore_sub3/4, 2); $selfscore_sub3 = 0;
	$selfscore_sub4_avg = number_format($selfscore_sub4/4, 2); $selfscore_sub4 = 0;
	$selfscore_sub5_avg = number_format($selfscore_sub5/8, 2); $selfscore_sub5 = 0;
	$selfscore_sub6_avg = number_format($selfscore_sub6/4, 2); $selfscore_sub6 = 0;
	$selfscore_sub7_avg = number_format($selfscore_sub7/3, 2); $selfscore_sub7 = 0;
	$selfscore_sub8_avg = number_format($selfscore_sub8/3, 2); $selfscore_sub8 = 0;
	$selfscore_sub9_avg = number_format($selfscore_sub9/2, 2); $selfscore_sub9 = 0;

	$peoplescore_sub10_avg = number_format($peoplescore_sub10/3, 2); $peoplescore_sub10 = 0;
	$peoplescore_sub11_avg = number_format($peoplescore_sub11/3, 2); $peoplescore_sub11 = 0;
	$peoplescore_sub12_avg = number_format($peoplescore_sub12/5, 2); $peoplescore_sub12 = 0;
	


	$workscore_sub13_avg = number_format($workscore_sub13/3, 2); $workscore_sub13 = 0; 
	$workscore_sub14_avg = number_format($workscore_sub14/6, 2); $workscore_sub14 = 0;
	$workscore_sub15_avg = number_format($workscore_sub15/4, 2); $workscore_sub15 = 0;
	$workscore_sub16_avg = number_format($workscore_sub16/3, 2); $workscore_sub16 = 0;
	$workscore_sub17_avg = number_format($workscore_sub17/3, 2); $workscore_sub17 = 0;
	$workscore_sub18_avg = number_format($workscore_sub18/8, 2); $workscore_sub18 = 0;
	$workscore_sub19_avg = number_format($workscore_sub19/3, 2); $workscore_sub19 = 0;
	
	// Set ค่า Valueble สำหรับชื่อหมวดทั้งหมด
	
	$selfscore_sub1_topic = 'รัก เข้าใจ ห่วงใยลูกศิษย์';
	$selfscore_sub2_topic = 'หน้าที่ครูต่อศิษย์ตามหลักทิศ 6';
	$selfscore_sub3_topic = 'ไม่สร้างหนี้เพิ่ม';
	$selfscore_sub4_topic = 'จิตสาธารณะ';
	$selfscore_sub5_topic = 'ปฏิบัติตนอยู่ในศีล 5 และความดีสากล 5';
	$selfscore_sub6_topic = 'มีความรับผิดชอบ พูดอย่างไรทำอย่างนั้น ทำอย่างไรพูดอย่างนั้น';
	$selfscore_sub7_topic = 'เรียนรู้ตลอดชีวิต';
	$selfscore_sub8_topic = 'รู้เท่าทันตนเอง';
	$selfscore_sub9_topic = 'ละอายและเกรงกลัวต่อบาป';

	$peoplescore_sub10_topic = 'เป็นผู้หวังดีให้ด้วยจิตเมตตา';
	$peoplescore_sub11_topic = 'ปฏิบัติตนตามแบบแผน และปรับแนวคิดให้ถูกต้องตรงกัน';
	$peoplescore_sub12_topic = 'ปฏิบัติตนกับบุคคลรอบข้างอย่างเหมาะสม';

	$workscore_sub13_topic = 'มีความคิดสร้างสรรค์ (Creativity)';
	$workscore_sub14_topic = 'มีการคิดวิเคราะห์ (Critical Thinking)';
	$workscore_sub15_topic = 'มีทักษะด้านความร่วมมือและการทำงานเป็นทีม (Collaboration and Teamwork)';
	$workscore_sub16_topic = 'มีทักษะสื่อสาร (Communication)';
	$workscore_sub17_topic = 'รู้คอมพิวเตอร์และไอซีที (Computing and ICT Literacy)';
	$workscore_sub18_topic = 'มีทักษะอาชีพและทักษะการเรียนรู้ (Career and Learning Skill)';
	$workscore_sub19_topic = 'ผสมผสานทางวัฒนธรรม (Cross-cultural)';
	// หาคะแนนหมวดสูงสุด 3 อันดับ
	
	$i = array(
		$selfscore_sub1_topic=>$selfscore_sub1_avg, 
		$selfscore_sub2_topic=>$selfscore_sub2_avg, 
		$selfscore_sub3_topic=>$selfscore_sub3_avg, 
		$selfscore_sub4_topic=>$selfscore_sub4_avg, 
		$selfscore_sub5_topic=>$selfscore_sub5_avg, 
		$selfscore_sub6_topic=>$selfscore_sub6_avg, 
		$selfscore_sub7_topic=>$selfscore_sub7_avg, 
		$selfscore_sub8_topic=>$selfscore_sub8_avg, 
		$selfscore_sub9_topic=>$selfscore_sub9_avg, 

		$peoplescore_sub10_topic=>$peoplescore_sub10_avg, 
		$peoplescore_sub11_topic=>$peoplescore_sub11_avg, 
		$peoplescore_sub12_topic=>$peoplescore_sub12_avg, 

		$workscore_sub13_topic=>$workscore_sub13_avg, 
		$workscore_sub14_topic=>$workscore_sub14_avg, 
		$workscore_sub15_topic=>$workscore_sub15_avg, 
		$workscore_sub16_topic=>$workscore_sub16_avg
	);

	arsort($i); // เรียงจากมากไปน้อย เรียงทั้ง Key ทั้ง Value
	$max34sub = array_slice($i, 0, 3); // เลือกแค่3อันดับแรกมากที่สุด
	// หาคะแนนหมวดน้อยสุด 3 อันดับ
	asort($i); // เรียงจากน้อยไปมาก เรียงทั้ง Key ทั้ง Value
	$min34sub = array_slice($i, 0, 3); // เลือกแค่3อันดับแรกน้อยที่สุด
	

?>