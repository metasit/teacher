<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
					<?php
						if(isset($_SESSION['ID'])) {
							//Admin Session
							if($_SESSION["level"]=="admin") { ?>
								<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false">
								 ผู้ดูแลระบบ</a> <?php
							//General Member Session
							}elseif($_SESSION['level']=='memberGeneral') { ?>
								<li class="faicon-backsystem" style="color:black; background-color:white;"><a href="#" onclick="return false">
								<i class="fas fa-user-check"></i> สมาชิกทั่วไป</a> <?php
							//Member Silver Session
							}elseif($_SESSION['level']=='memberSilver') { ?>
								<li class="faicon-backsystem" style="color:white; background-color:rgb(169,169,169);"><a href="#" onclick="return false">
								<i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</a> <?php
							//Member Gold Session
							}elseif($_SESSION['level']=='memberGold') { ?>
								<li class="faicon-backsystem" style="color:white; background-color:rgb(180,147,31);"><a href="#" onclick="return false">
								<i class="fas fa-user-check"></i> สมาชิก ระดับทอง</a> <?php
							//Member Diamond Session
							}elseif($_SESSION['level']=='memberDiamond') { ?>
								<li class="faicon-backsystem" style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><a href="#" onclick="return false"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</a> <?php
							// No Account Session or others
							} ?>

									<!-- ################################################################################################ -->
									<ul>
										<li><a href="editprofile.php">แก้ไขข้อมูลส่วนตัว</a></li>
										<!-- ################################################################################################ -->
										<?php
											if(strpos($_SESSION['permis_system'], '*all*') !== false) { ?>
												
												<li><a href="#">ระบบหลังบ้านครู<i class="fas fa-caret-right m-l-10"></i></a>
													<ul>
														<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">พื้นฐาน</a></li>
														<li><a href="ระบบหลังบ้านครูชั้นที่5.php">ชั้นที่ 5</a></li>
													</ul>
												</li>
												<?php
													if((in_array("level", array_keys($_SESSION)))) {
														if($_SESSION['level']=="admin"){
												?>
												<li><a href="#">ระบบหลังบ้านศน.<i class="fas fa-caret-right m-l-10"></i></a>
													<ul>
														<li><a href="ระบบหลังบ้านศน.ขั้นพื้นฐาน.php">พื้นฐาน</a></li>
														<li><a href="ระบบหลังบ้านศน.ขั้นเกียรติคุณ.php">เกียรติคุณ</a></li>
													</ul>
												</li>
												<?php
														}
													}
												?>
												<li><a href="#">ระบบหลังบ้านเด็ก<i class="fas fa-caret-right m-l-10"></i></a>
													<ul>
														<li><a href="ระบบหลังบ้านเด็กพื้นฐาน-มีครูที่ปรึกษา.php">พื้นฐาน (มีครูที่ปรึกษา)</a></li>
														<li><a href="ระบบหลังบ้านเด็กพื้นฐาน-ไม่มีครูที่ปรึกษา.php">พื้นฐาน (ไม่มีครูที่ปรึกษา)</a></li>
													</ul>
												</li>
												<li><a href="#">ระบบหลังบ้านจัดการทั่วไป<i class="fas fa-caret-right m-l-10"></i></a>
													<ul>
														<li><a href="ระบบหลังบ้านบำรุงสมาชิก.php">บำรุงสมาชิก</a></li>
														<li><a href="ระบบหลังบ้านบริจาค.php">บริจาค</a></li>
														<li><a href="ระบบหลังบ้านร้านค้า.php">ร้านค้า</a></li>
													</ul>
												</li>
												<li><a href="content-setting/backend/">ระบบหลังบ้านจัดการเนื้อหา</a></li>
												<li><a href="content-setting/backend/confirm_account/confirm.php">ยืนยันตัวตน account</a></li>
												<?php
											}else{
												if(strpos($_SESSION['permis_system'], 'A') !== false) { ?>

													<li><a href="#">ระบบหลังบ้านครู<i class="fas fa-caret-right m-l-10"></i></a>
														<ul> <?php
															if(strpos($_SESSION['permis_system'], '*A00*') !== false) { ?>
																<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">พื้นฐาน</a></li>
																<li><a href="ระบบหลังบ้านครูชั้นที่5.php">ชั้นที่ 5</a></li> <?php

															}elseif(strpos($_SESSION['permis_system'], '*A01*') !== false) { ?>
																<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">พื้นฐาน</a></li> <?php

															}elseif(strpos($_SESSION['permis_system'], '*A02*') !== false) { ?>
																<li><a href="ระบบหลังบ้านครูชั้นที่5.php">ชั้นที่ 5</a></li> <?php

															} ?>
														</ul>
													</li> <?php
												}
												if(strpos($_SESSION['permis_system'], 'B') !== false && (in_array("level", array_keys($_SESSION)))   ) {
													if($_SESSION['level'] == "admin") {
													?>
																
													<li><a href="#">ระบบหลังบ้านศน.<i class="fas fa-caret-right m-l-10"></i></a>
														<ul> <?php
															if(strpos($_SESSION['permis_system'], '*B00*') !== false) { ?>
																<li><a href="ระบบหลังบ้านศน.ขั้นพื้นฐาน.php">พื้นฐาน</a></li>
																<!-- <li><a href="ระบบหลังบ้านศน.ขั้นเกียรติคุณ.php">เกียรติคุณ</a></li>  -->
																<?php

															}elseif(strpos($_SESSION['permis_system'], '*B01*') !== false) { ?>
																<li><a href="ระบบหลังบ้านศน.ขั้นพื้นฐาน.php">พื้นฐาน</a></li> <?php

															}elseif(strpos($_SESSION['permis_system'], '*B02*') !== false) { ?>
																<!-- <li><a href="ระบบหลังบ้านศน.ขั้นเกียรติคุณ.php">เกียรติคุณ</a></li> -->
																 <?php

															} ?>
														</ul>
													</li> <?php
													}
												}
												if(strpos($_SESSION['permis_system'], 'C') !== false) { ?>

													<li><a href="#">ระบบหลังบ้านเด็ก<i class="fas fa-caret-right m-l-10"></i></a>
														<ul> <?php
															if(strpos($_SESSION['permis_system'], '*C00*') !== false) { ?>
																<li><a href="ระบบหลังบ้านเด็กพื้นฐาน-มีครูที่ปรึกษา.php">พื้นฐาน (มีครูที่ปรึกษา)</a></li>
																<li><a href="ระบบหลังบ้านเด็กพื้นฐาน-ไม่มีครูที่ปรึกษา.php">พื้นฐาน (ไม่มีครูที่ปรึกษา)</a></li> <?php

															}elseif(strpos($_SESSION['permis_system'], '*C01*') !== false) { ?>
																<li><a href="ระบบหลังบ้านเด็กพื้นฐาน-มีครูที่ปรึกษา.php">พื้นฐาน (มีครูที่ปรึกษา)</a></li> <?php

															}elseif(strpos($_SESSION['permis_system'], '*C02*') !== false) { ?>
																<li><a href="ระบบหลังบ้านเด็กพื้นฐาน-ไม่มีครูที่ปรึกษา.php">พื้นฐาน (ไม่มีครูที่ปรึกษา)</a></li> <?php

															} ?>
														</ul>
													</li> <?php
												}
												if(strpos($_SESSION['permis_system'], 'D') !== false) { ?>

													<li><a href="#">ระบบหลังบ้านจัดการทั่วไป<i class="fas fa-caret-right m-l-10"></i></a>
														<ul> <?php
															if(strpos($_SESSION['permis_system'], '*D00*') !== false) { ?>
																<li><a href="ระบบหลังบ้านบำรุงสมาชิก.php">บำรุงสมาชิก</a></li>
																<li><a href="ระบบหลังบ้านบริจาค.php">บริจาค</a></li>
																<li><a href="ระบบหลังบ้านร้านค้า.php">ร้านค้า</a></li> <?php

															}elseif(strpos($_SESSION['permis_system'], '*D01*') !== false) { ?>
																<li><a href="ระบบหลังบ้านบำรุงสมาชิก.php">บำรุงสมาชิก</a></li> <?php

															}elseif(strpos($_SESSION['permis_system'], '*D02*') !== false) { ?>
																<li><a href="ระบบหลังบ้านบริจาค.php">บริจาค</a></li> <?php

															}elseif(strpos($_SESSION['permis_system'], '*D02*') !== false) { ?>
																<li><a href="ระบบหลังบ้านร้านค้า.php">ร้านค้า</a></li> <?php

															} ?>
														</ul>
													</li>
													<li><a href="content-setting/backend/">ระบบหลังบ้านจัดการเนื้อหา</a></li>
													<li><a href="content-setting/backend/confirm_account/confirm.php">ยืนยันตัวตน account</a></li>
													<?php
												}
											}
										?>
									</ul>
									<!-- ################################################################################################ -->
								</li>
								<li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li> <?php
						}else{ ?>
							<li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
							<li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li> <?php
						}
					?>
							<li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>