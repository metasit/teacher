<?php

	$sqlfifth_score4tea = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='A' ";
	$reFS4T = mysqli_query($con, $sqlfifth_score4tea);
	$rowFS4T = mysqli_fetch_array($reFS4T);

	$fifth_score4tea_data = $rowFS4T['fifth_score4tea_data'];

	// หมวดครองตน NOTE: มี7ตัวบ่งชี้
	for($x=1; $x<=9; $x++) {
		$i = strpos($fifth_score4tea_data, '.'); // Find position of each score
		$j = substr($fifth_score4tea_data, $i+1, 1); // Return value of each score

		$score[$x] = $j; // Set valuable for each score
		$selfscore += $score[$x]; // Accumulate all score
		
		if($x==1) { // ตัวบ่งชี้ที่1: รับผิดชอบต่อหน้าที่
			$selfscore_sub1 += $score[$x];
		}elseif($x==2) { //ตัวบ่งชี้ที่2: เรียนรู้ตลอดชีวิต
			$selfscore_sub2 += $score[$x];
		}elseif($x==3) { //ตัวบ่งชี้ที่3: รู้เท่าทันตนเอง
			$selfscore_sub3 += $score[$x];
		}elseif($x>=4 && $x<=5) { //ตัวบ่งชี้ที่4: ไม่มีหนี้สินล้นพ้นตัว
			$selfscore_sub4 += $score[$x];
		}elseif($x==6) { //ตัวบ่งชี้ที่5: มีจิตอาสา/จิตสาธารณะ
			$selfscore_sub5 += $score[$x];
		}elseif($x>=7 && $x<=8) { //ตัวบ่งชี้ที่6: ปฏิบัติตนอยู่ในศีล 5 และความดีสากล 5
			$selfscore_sub6 += $score[$x];
		}elseif($x==9) { //ตัวบ่งชี้ที่7: มีหิริโอตตัปปะ
			$selfscore_sub7 += $score[$x];
		}
		
		$fifth_score4tea_data = substr($fifth_score4tea_data, $i+1);
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// หมวดครองคน NOTE: มี5ตัวบ่งชี้
	for($x=10; $x<=20; $x++) {
		$i = strpos($fifth_score4tea_data, '.'); // Find position of each score
		$j = substr($fifth_score4tea_data, $i+1, 1); // Return value of each score

		$score[$x] = $j; // Set valuable for each score
		$peoplescore += $score[$x]; // Accumulate all score

		if($x>=10 && $x<=12) { //ตัวบ่งชี้ที่8: รัก เข้าใจ ห่วงใยบุคลากรของโรงเรียน
			$peoplescore_sub8 += $score[$x];
		}elseif($x==13) { //ตัวบ่งชี้ที่9: ปฏิบัติตนเป็นกัลยาณมิตรต่อทิศ 6 อย่างเหมาะสม
			$peoplescore_sub9 += $score[$x];
		}elseif($x>=14 && $x<=15) { //ตัวบ่งชี้ที่10: ปฏิบัติตนเป็นผู้ให้ด้วยจิตเมตตา
			$peoplescore_sub10 += $score[$x];
		}elseif($x>=16 && $x<=18) { //ตัวบ่งชี้ที่11: ปฏิบัติตนตามแบบแผนและปรับแนวคิดให้ถูกต้องตรงกัน
			$peoplescore_sub11 += $score[$x];
		}elseif($x>=19 && $x<=20) { //ตัวบ่งชี้ที่12: ปฏิบัติตนกับบุคคลรอบข้างอย่างเหมาะสม
			$peoplescore_sub12 += $score[$x];
		}

		$fifth_score4tea_data = substr($fifth_score4tea_data, $i+1);
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// หมวดครองงาน NOTE: มี7ตัวบ่งชี้
	for($x=21; $x<=32; $x++) {
		$i = strpos($fifth_score4tea_data, '.'); // Find position of each score
		$j = substr($fifth_score4tea_data, $i+1, 1); // Return value of each score

		$score[$x] = $j; // Set valuable for each score
		$workscore += $score[$x]; // Accumulate all score
		
		if($x==21) { //ตัวบ่งชี้ที่13: การคิดอย่างมีวิจารณญาณและการแก้ปัญหา (Critical Thinking and Problem Solving)
			$workscore_sub13 += $score[$x];
		}elseif($x>=22 && $x<=23) { //ตัวบ่งชี้ที่14: การสื่อสารสารสนเทศและรู้เท่าทันสื่อ(Communications, Information and Media Literacy)
			$workscore_sub14 += $score[$x];
		}elseif($x>=24 && $x<=25) { //ตัวบ่งชี้ที่15: ความร่วมมือ การทำงานเป็นทีม และภาวะผู้นำ (collaboration, teamwork and leadership)
			$workscore_sub15 += $score[$x];
		}elseif($x==26) { //ตัวบ่งชี้ที่16: การสร้างสรรค์และนวัตกรรม (creativity and innovation)
			$workscore_sub16 += $score[$x];
		}elseif($x>=27 && $x<=28) { //ตัวบ่งชี้ที่17: คอมพิวเตอร์ และเทคโนโลยีสารสนเทศและการสื่อสาร (Computing and ICT literlacy)
			$workscore_sub17 += $score[$x];
		}elseif($x==29) { //ตัวบ่งชี้ที่18: การทำงาน การเรียนรู้ และการพึ่งตนเอง (Career and learning self-reliance)			
			$workscore_sub18 += $score[$x];
		}elseif($x>=30 && $x<=32) { //ตัวบ่งชี้ที่19: ความเข้าใจต่างวัฒนธรรม ต่างกระบวนทัศน์ (Cross-cultural understanding)
			$workscore_sub19 += $score[$x];
		}
		$fifth_score4tea_data = substr($fifth_score4tea_data, $i+1);
	}

	// สรุปคะแนนรวม แยกเป็นรายหมวด
	$selfscore_avg = number_format($selfscore/9, 2);
	$peoplescore_avg = number_format($peoplescore/14, 2);
	$workscore_avg = number_format($workscore/12, 2);
	// หาจำนวนหมวดที่ผ่าน
	if($selfscore_avg >= 3) {
		$numberpass_sub = 1;
	}else{
		$numberpass_sub = 0;
	}

	if($peoplescore_avg >= 3) {
		$numberpass_sub += 1;
	}else{
		$numberpass_sub += 0;
	}

	if($workscore_avg >= 3) {
		$numberpass_sub += 1;
	}else{
		$numberpass_sub += 0;
	}
	// สรุปคะแนน เป็นจุดทศนิยม2ตำแหน่ง ของแต่ละ ตัวบ่งชี้
	$selfscore_sub1_avg = number_format($selfscore_sub1, 2);
	$selfscore_sub2_avg = number_format($selfscore_sub2, 2);
	$selfscore_sub3_avg = number_format($selfscore_sub3, 2);
	$selfscore_sub4_avg = number_format($selfscore_sub4/2, 2);
	$selfscore_sub5_avg = number_format($selfscore_sub5, 2);
	$selfscore_sub6_avg = number_format($selfscore_sub6/2, 2);
	$selfscore_sub7_avg = number_format($selfscore_sub7, 2);

	$peoplescore_sub8_avg = number_format($peoplescore_sub8/2, 2);
	$peoplescore_sub9_avg = number_format($peoplescore_sub9, 2);
	$peoplescore_sub10_avg = number_format($peoplescore_sub10/2, 2);
	$peoplescore_sub11_avg = number_format($peoplescore_sub11/3, 2);
	$peoplescore_sub12_avg = number_format($peoplescore_sub12/2, 2);
	
	$workscore_sub13_avg = number_format($workscore_sub13, 2);
	$workscore_sub14_avg = number_format($workscore_sub14/2, 2);
	$workscore_sub15_avg = number_format($workscore_sub15/2, 2);
	$workscore_sub16_avg = number_format($workscore_sub16, 2);
	$workscore_sub17_avg = number_format($workscore_sub17/2, 2);
	$workscore_sub18_avg = number_format($workscore_sub18/1, 2);
	$workscore_sub19_avg = number_format($workscore_sub19/3, 2);
	// Set ค่า Valueble สำหรับชื่อหมวดทั้งหมด
	$selfscore_sub1_topic = 'รับผิดชอบต่อหน้าที่';
	$selfscore_sub2_topic = 'เรียนรู้ตลอดชีวิต';
	$selfscore_sub3_topic = 'รู้เท่าทันตนเอง';
	$selfscore_sub4_topic = 'ไม่มีหนี้สินล้นพ้นตัว';
	$selfscore_sub5_topic = 'มีจิตอาสา/จิตสาธารณะ';
	$selfscore_sub6_topic = 'ปฏิบัติตนอยู่ในศีล 5 และความดีสากล 5';
	$selfscore_sub7_topic = 'มีหิริโอตตัปปะ';

	$peoplescore_sub8_topic = 'รัก เข้าใจ ห่วงใยบุคลากรของโรงเรียน';
	$peoplescore_sub9_topic = 'ปฏิบัติตนเป็นกัลยาณมิตรต่อทิศ 6 อย่างเหมาะสม';
	$peoplescore_sub10_topic = 'ปฏิบัติตนเป็นผู้ให้ด้วยจิตเมตตา';
	$peoplescore_sub11_topic = 'ปฏิบัติตนตามแบบแผนและปรับแนวคิดให้ถูกต้องตรงกัน';
	$peoplescore_sub12_topic = 'ปฏิบัติตนกับบุคคลรอบข้างอย่างเหมาะสม';

	$workscore_sub13_topic = 'การคิดอย่างมีวิจารณญาณและการแก้ปัญหา (Critical Thinking and Problem Solving)';
	$workscore_sub14_topic = 'การสื่อสารสารสนเทศและรู้เท่าทันสื่อ(Communications, Information and Media Literacy)';
	$workscore_sub15_topic = 'ความร่วมมือ การทำงานเป็นทีม และภาวะผู้นำ (collaboration, teamwork and leadership)';
	$workscore_sub16_topic = 'คอมพิวเตอร์ และเทคโนโลยีสารสนเทศและการสื่อสาร (Computing and ICT literlacy)';
	$workscore_sub17_topic = 'รู้คอมพิวเตอร์และไอซีที (Computing and ICT Literacy)';
	$workscore_sub18_topic = 'การทำงาน การเรียนรู้ และการพึ่งตนเอง (Career and learning self-reliance)';
	$workscore_sub19_topic = 'ความเข้าใจต่างวัฒนธรรม ต่างกระบวนทัศน์ (Cross-cultural understanding)';

	// หาคะแนนหมวดสูงสุด 3 อันดับ
	
	$i = array(
		$selfscore_sub1_topic=>$selfscore_sub1_avg, 
		$selfscore_sub2_topic=>$selfscore_sub2_avg, 
		$selfscore_sub3_topic=>$selfscore_sub3_avg, 
		$selfscore_sub4_topic=>$selfscore_sub4_avg, 
		$selfscore_sub5_topic=>$selfscore_sub5_avg, 
		$selfscore_sub6_topic=>$selfscore_sub6_avg, 
		$selfscore_sub7_topic=>$selfscore_sub7_avg, 

		$peoplescore_sub8_topic=>$peoplescore_sub8_avg, 
		$peoplescore_sub9_topic=>$peoplescore_sub9_avg, 
		$peoplescore_sub10_topic=>$peoplescore_sub10_avg, 
		$peoplescore_sub11_topic=>$peoplescore_sub11_avg, 
		$peoplescore_sub12_topic=>$peoplescore_sub12_avg, 

		$workscore_sub13_topic=>$workscore_sub13_avg, 
		$workscore_sub14_topic=>$workscore_sub14_avg, 
		$workscore_sub15_topic=>$workscore_sub15_avg, 
		$workscore_sub16_topic=>$workscore_sub16_avg, 
		$workscore_sub17_topic=>$workscore_sub17_avg, 
		$workscore_sub18_topic=>$workscore_sub18_avg, 
		$workscore_sub19_topic=>$workscore_sub19_avg
	);

	arsort($i); // เรียงจากมากไปน้อย เรียงทั้ง Key ทั้ง Value
	$max34sub = array_slice($i, 0, 3); // เลือกแค่3อันดับแรกมากที่สุด
	// หาคะแนนหมวดน้อยสุด 3 อันดับ
	asort($i); // เรียงจากน้อยไปมาก เรียงทั้ง Key ทั้ง Value
	$min34sub = array_slice($i, 0, 3); // เลือกแค่3อันดับแรกน้อยที่สุด
	

?>