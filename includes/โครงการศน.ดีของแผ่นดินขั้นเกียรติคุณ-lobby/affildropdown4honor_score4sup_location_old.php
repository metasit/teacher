<?php
	/* 
		input need: $affiliation_id_location_old && $affil_code_location_old
	*/
	if($affiliation_id_location_old == 'AfA') {
		$affiliation_name_location_old = 'สำนักงานปลัดกระทรวงศึกษาธิการ';
		$affsub_id_location_old = substr($affil_code_location_old,0,4);
		$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id_location_old' ";
		$reaffiliation = mysqli_query($con, $sqlaffiliation);
		$rowaffiliation = mysqli_fetch_array($reaffiliation);
		$affsub_name_location_old = $rowaffiliation['affsub_name'];

		$sqlaffsub24AfA = "SELECT `affsub2_name` FROM `affsub24AfA` WHERE affsub2_id='$affil_code_location_old' ";
		$reaffsub24AfA = mysqli_query($con, $sqlaffsub24AfA);
		$rowaffsub24AfA = mysqli_fetch_array($reaffsub24AfA);
		$affsub2_name_location_old = $rowaffsub24AfA['affsub2_name'];
	}elseif($affiliation_id_location_old == 'AfB') {
		$affiliation_name_location_old = 'สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน';
		$affsub_id_location_old = substr($affil_code_location_old,0,4);
		$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id_location_old' ";
		$reaffiliation = mysqli_query($con, $sqlaffiliation);
		$rowaffiliation = mysqli_fetch_array($reaffiliation);
		$affsub_name_location_old = $rowaffiliation['affsub_name'];

		$affsub2_id_location_old = substr($affil_code_location_old,0,5);
		$sqlaffsub24AfB = "SELECT `affsub2_name` FROM `affsub24AfB` WHERE affsub2_id='$affsub2_id_location_old' ";
		$reaffsub24AfB = mysqli_query($con, $sqlaffsub24AfB);
		$rowaffsub24AfB = mysqli_fetch_array($reaffsub24AfB);
		$affsub2_name_location_old = $rowaffsub24AfB['affsub2_name'];

		if($affsub_id_location_old == 'AfBA') {
			$affsub3_id_location_old = substr($affil_code_location_old,0,8);
			$sqlaffsub34AfBA = "SELECT `affsub3_name` FROM `affsub34AfBA` WHERE affsub3_id='$affsub3_id_location_old' ";
			$reaffsub34AfBA = mysqli_query($con, $sqlaffsub34AfBA);
			$rowaffsub34AfBA = mysqli_fetch_array($reaffsub34AfBA);
			$affsub3_name_location_old = $rowaffsub34AfBA['affsub3_name'];
			
			if($affsub2_id_location_old == 'AfBAA') {
				$affsub4_id_location_old = substr($affil_code_location_old,0,13);
				$sqlaffsub44AfBAA = "SELECT `affsub4_name` FROM `affsub44AfBAA` WHERE affsub4_id='$affsub4_id_location_old' ";
				$reaffsub44AfBAA = mysqli_query($con, $sqlaffsub44AfBAA);
				$rowaffsub44AfBAA = mysqli_fetch_array($reaffsub44AfBAA);
				$affsub4_name_location_old = $rowaffsub44AfBAA['affsub4_name'];
			}elseif($affsub2_id_location_old == 'AfBAB') {
				$affsub4_id_location_old = substr($affil_code_location_old,0,13);
				$sqlaffsub44AfBAB = "SELECT `affsub4_name` FROM `affsub44AfBAB` WHERE affsub4_id='$affsub4_id_location_old' ";
				$reaffsub44AfBAB = mysqli_query($con, $sqlaffsub44AfBAB);
				$rowaffsub44AfBAB = mysqli_fetch_array($reaffsub44AfBAB);
				$affsub4_name_location_old = $rowaffsub44AfBAB['affsub4_name'];
			}
			
		}elseif($affsub_id_location_old == 'AfBB') {
			$affsub3_id_location_old = substr($affil_code_location_old,0,10);
			$sqlaffsub34AfBB = "SELECT `affsub3_name` FROM `affsub34AfBB` WHERE affsub3_id='$affsub3_id_location_old' ";
			$reaffsub34AfBB = mysqli_query($con, $sqlaffsub34AfBB);
			$rowaffsub34AfBB = mysqli_fetch_array($reaffsub34AfBB);
			$affsub3_name_location_old = $rowaffsub34AfBB['affsub3_name'];
		}
	}elseif($affiliation_id_location_old == 'AfC') {
		$affiliation_name_location_old = 'สำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน';
		$affsub_id_location_old = substr($affil_code_location_old,0,5);
		$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id_location_old' ";
		$reaffiliation = mysqli_query($con, $sqlaffiliation);
		$rowaffiliation = mysqli_fetch_array($reaffiliation);
		$affsub_name_location_old = $rowaffiliation['affsub_name'];

		$affsub2_id_location_old = substr($affil_code_location_old,0,8);
		$sqlaffsub24AfC = "SELECT `affsub2_name` FROM `affsub24AfC` WHERE affsub2_id='$affsub2_id_location_old' ";
		$reaffsub24AfC = mysqli_query($con, $sqlaffsub24AfC);
		$rowaffsub24AfC = mysqli_fetch_array($reaffsub24AfC);
		$affsub2_name_location_old = $rowaffsub24AfC['affsub2_name'];

		$affsub3_id_location_old = substr($affil_code_location_old,0,13);
		$sqlaffsub34AfC = "SELECT `affsub3_name` FROM `affsub34AfC` WHERE affsub3_id='$affsub3_id_location_old' ";
		$reaffsub34AfC = mysqli_query($con, $sqlaffsub34AfC);
		$rowaffsub34AfC = mysqli_fetch_array($reaffsub34AfC);
		$affsub3_name_location_old = $rowaffsub34AfC['affsub3_name'];
	}elseif($affiliation_id_location_old == 'AfD') {
		$affiliation_name_location_old = 'สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย';
		$affsub_id_location_old = substr($affil_code_location_old,0,5);
		$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id_location_old' ";
		$reaffiliation = mysqli_query($con, $sqlaffiliation);
		$rowaffiliation = mysqli_fetch_array($reaffiliation);
		$affsub_name_location_old = $rowaffiliation['affsub_name'];

		$affsub2_id_location_old = substr($affil_code_location_old,0,9);
		$sqlaffsub24AfD = "SELECT `affsub2_name` FROM `affsub24AfD` WHERE affsub2_id='$affsub2_id_location_old' ";
		$reaffsub24AfD = mysqli_query($con, $sqlaffsub24AfD);
		$rowaffsub24AfD = mysqli_fetch_array($reaffsub24AfD);
		$affsub2_name_location_old = $rowaffsub24AfD['affsub2_name'];

		$affsub3_id_location_old = substr($affil_code_location_old,0,13);
		$sqlaffsub34AfD = "SELECT `affsub3_name` FROM `affsub34AfD` WHERE affsub3_id='$affsub3_id_location_old' ";
		$reaffsub34AfD = mysqli_query($con, $sqlaffsub34AfD);
		$rowaffsub34AfD = mysqli_fetch_array($reaffsub34AfD);
		$affsub3_name_location_old = $rowaffsub34AfD['affsub3_name'];

		$affsub4_ans_id_location_old = $affil_remark_location_old;
	}elseif($affiliation_id_location_old == 'AfE') {
		$affiliation_name_location_old = 'สำนักงานคณะกรรมการการอาชีวศึกษา';
		$affsub_id_location_old = substr($affil_code_location_old,0,5);
		$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id_location_old' ";
		$reaffiliation = mysqli_query($con, $sqlaffiliation);
		$rowaffiliation = mysqli_fetch_array($reaffiliation);
		$affsub_name_location_old = $rowaffiliation['affsub_name'];

		$affsub2_id_location_old = substr($affil_code_location_old,0,6);
		$sqlaffsub24AfE = "SELECT `affsub2_name` FROM `affsub24AfE` WHERE affsub2_id='$affsub2_id_location_old' ";
		$reaffsub24AfE = mysqli_query($con, $sqlaffsub24AfE);
		$rowaffsub24AfE = mysqli_fetch_array($reaffsub24AfE);
		$affsub2_name_location_old = $rowaffsub24AfE['affsub2_name'];

		$affsub3_id_location_old = substr($affil_code_location_old,0,10);
		$sqlaffsub34AfE = "SELECT `affsub3_name` FROM `affsub34AfE` WHERE affsub3_id='$affsub3_id_location_old' ";
		$reaffsub34AfE = mysqli_query($con, $sqlaffsub34AfE);
		$rowaffsub34AfE = mysqli_fetch_array($reaffsub34AfE);
		$affsub3_name_location_old = $rowaffsub34AfE['affsub3_name'];
	}elseif($affiliation_id_location_old == 'AfF') {
		$affiliation_name_location_old = 'สำนักงานคณะกรรมการการอุดมศึกษา';
		$affsub_id_location_old = substr($affil_code_location_old,0,4);
		$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id_location_old' ";
		$reaffiliation = mysqli_query($con, $sqlaffiliation);
		$rowaffiliation = mysqli_fetch_array($reaffiliation);
		$affsub_name_location_old = $rowaffiliation['affsub_name'];

		$affsub2_id_location_old = substr($affil_code_location_old,0,7);
		$sqlaffsub24AfF = "SELECT `affsub2_name` FROM `affsub24AfF` WHERE affsub2_id='$affsub2_id_location_old' ";
		$reaffsub24AfF = mysqli_query($con, $sqlaffsub24AfF);
		$rowaffsub24AfF = mysqli_fetch_array($reaffsub24AfF);
		$affsub2_name_location_old = $rowaffsub24AfF['affsub2_name'];
	}elseif($affiliation_id_location_old == 'AfG') {
		$affiliation_name_location_old = 'กระทรวงมหาดไทย กรมส่งเสริมการปกครองส่วนท้องถิ่น';
		$affsub_id_location_old = substr($affil_code_location_old,0,5);
		$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id_location_old' ";
		$reaffiliation = mysqli_query($con, $sqlaffiliation);
		$rowaffiliation = mysqli_fetch_array($reaffiliation);
		$affsub_name_location_old = $rowaffiliation['affsub_name'];

		$affsub2_id_location_old = substr($affil_code_location_old,0,8);
		$sqlaffsub24AfG = "SELECT `affsub2_name` FROM `affsub24AfG` WHERE affsub2_id='$affsub2_id_location_old' ";
		$reaffsub24AfG = mysqli_query($con, $sqlaffsub24AfG);
		$rowaffsub24AfG = mysqli_fetch_array($reaffsub24AfG);
		$affsub2_name_location_old = $rowaffsub24AfG['affsub2_name'];

		$affsub3_id_location_old = substr($affil_code_location_old,0,11);
		$sqlaffsub34AfG = "SELECT `affsub3_name` FROM `affsub34AfG` WHERE affsub3_id='$affsub3_id_location_old' ";
		$reaffsub34AfG = mysqli_query($con, $sqlaffsub34AfG);
		$rowaffsub34AfG = mysqli_fetch_array($reaffsub34AfG);
		$affsub3_name_location_old = $rowaffsub34AfG['affsub3_name'];

		$affsub4_id_location_old = substr($affil_code_location_old,0,15);
		$sqlaffsub44AfG = "SELECT `affsub4_name` FROM `affsub44AfG` WHERE affsub4_id='$affsub4_id_location_old' ";
		$reaffsub44AfG = mysqli_query($con, $sqlaffsub44AfG);
		$rowaffsub44AfG = mysqli_fetch_array($reaffsub44AfG);
		$affsub4_name_location_old = $rowaffsub44AfG['affsub4_name'];
	}elseif($affiliation_id_location_old == 'AfH') {
		$affiliation_name_location_old = 'กรุงเทพมหานคร';
		$affsub_id_location_old = substr($affil_code_location_old,0,4);
		$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id_location_old' ";
		$reaffiliation = mysqli_query($con, $sqlaffiliation);
		$rowaffiliation = mysqli_fetch_array($reaffiliation);
		$affsub_name_location_old = $rowaffiliation['affsub_name'];

		if($affsub_id_location_old == 'AfHA') {
			$affsub2_id_location_old = 'AfHAA';
			$affsub2_name_location_old = 'สำนักงานเขต';

			$affsub3_id_location_old = substr($affil_code_location_old,0,8);
			$sqlaffsub34AfHAA = "SELECT `affsub3_name` FROM `affsub34AfHAA` WHERE affsub3_id='$affsub3_id_location_old' ";
			$reaffsub34AfHAA = mysqli_query($con, $sqlaffsub34AfHAA);
			$rowaffsub34AfHAA = mysqli_fetch_array($reaffsub34AfHAA);
			$affsub3_name_location_old = $rowaffsub34AfHAA['affsub3_name'];

			$affsub4_id_location_old = substr($affil_code_location_old,0,11);
			$sqlaffsub44AfHAA = "SELECT `affsub4_name` FROM `affsub44AfHAA` WHERE affsub4_id='$affsub4_id_location_old' ";
			$reaffsub44AfHAA = mysqli_query($con, $sqlaffsub44AfHAA);
			$rowaffsub44AfHAA = mysqli_fetch_array($reaffsub44AfHAA);
			$affsub4_name_location_old = $rowaffsub44AfHAA['affsub4_name'];
		}
	}elseif($affiliation_id_location_old == 'AfI') {
		$affiliation_name_location_old = 'เมืองพัทยา';
		$affsub_id_location_old = substr($affil_code_location_old,0,4);
		$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id_location_old' ";
		$reaffiliation = mysqli_query($con, $sqlaffiliation);
		$rowaffiliation = mysqli_fetch_array($reaffiliation);
		$affsub_name_location_old = $rowaffiliation['affsub_name'];
	}elseif($affiliation_id_location_old == 'AfJ') {
		$affiliation_name_location_old = 'สำนักงานตำรวจแห่งชาติ';
		$affsub4_ans_id_location_old = $affil_remark_location_old;
	}else{
		$affiliation_name_location_old = 'อื่นๆ';
		$affsub4_ans_id_location_old = $affil_remark_location_old;
	}

?>
<!-- Start สังกัด Content -->
					<!-- เลือกสังกัด -->
					<label for="affiliation_id" class="center text-black">สถานที่ที่นิเทศ</label>
					<select name="affiliation_id" id="affiliation_id" class="form-control" style="height:40px;" required>
						
						<option value="AfA" <?php if($affiliation_id_location_old=='AfA'){echo 'selected="selected" ';}else{echo 'style="display:none" ';} ?> >สำนักงานปลัดกระทรวงศึกษาธิการ</option>
						<option value="AfB" <?php if($affiliation_id_location_old=='AfB'){echo 'selected="selected" ';}else{echo 'style="display:none" ';} ?> >สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน</option>
						<option value="AfC" <?php if($affiliation_id_location_old=='AfC'){echo 'selected="selected" ';}else{echo 'style="display:none" ';} ?> >สำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน</option>
						<option value="AfD" <?php if($affiliation_id_location_old=='AfD'){echo 'selected="selected" ';}else{echo 'style="display:none" ';} ?> >สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย</option>
						<option value="AfE" <?php if($affiliation_id_location_old=='AfE'){echo 'selected="selected" ';}else{echo 'style="display:none" ';} ?> >สำนักงานคณะกรรมการการอาชีวศึกษา</option>
						<option value="AfF" <?php if($affiliation_id_location_old=='AfF'){echo 'selected="selected" ';}else{echo 'style="display:none" ';} ?> >สำนักงานคณะกรรมการการอุดมศึกษา</option>
						<option value="AfG" <?php if($affiliation_id_location_old=='AfG'){echo 'selected="selected" ';}else{echo 'style="display:none" ';} ?> >กระทรวงมหาดไทย กรมส่งเสริมการปกครองส่วนท้องถิ่น</option>
						<option value="AfH" <?php if($affiliation_id_location_old=='AfH'){echo 'selected="selected" ';}else{echo 'style="display:none" ';} ?> >กรุงเทพมหานคร</option>
						<option value="AfI" <?php if($affiliation_id_location_old=='AfI'){echo 'selected="selected" ';}else{echo 'style="display:none" ';} ?> >เมืองพัทยา</option>
						<option value="AfJ" <?php if($affiliation_id_location_old=='AfJ'){echo 'selected="selected" ';}else{echo 'style="display:none" ';} ?> >สำนักงานตำรวจแห่งชาติ</option>
						<option value="AfO" <?php if($affiliation_id_location_old=='AfO'){echo 'selected="selected" ';}else{echo 'style="display:none" ';} ?> >อื่นๆ โปรดระบุ....</option>
						<!--
						<option value="" disabled="disabled" selected="selected">เลือกสังกัด</option>
						<option value="AfA" <?php if($affiliation_id_location_old=='AfA'){echo 'selected="selected" ';} ?> >สำนักงานปลัดกระทรวงศึกษาธิการ</option>
						<option value="AfB" <?php if($affiliation_id_location_old=='AfB'){echo 'selected="selected" ';} ?> >สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน</option>
						<option value="AfC" <?php if($affiliation_id_location_old=='AfC'){echo 'selected="selected" ';} ?> >สำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน</option>
						<option value="AfD" <?php if($affiliation_id_location_old=='AfD'){echo 'selected="selected" ';} ?> >สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย</option>
						<option value="AfE" <?php if($affiliation_id_location_old=='AfE'){echo 'selected="selected" ';} ?> >สำนักงานคณะกรรมการการอาชีวศึกษา</option>
						<option value="AfF" <?php if($affiliation_id_location_old=='AfF'){echo 'selected="selected" ';} ?> >สำนักงานคณะกรรมการการอุดมศึกษา</option>
						<option value="AfG" <?php if($affiliation_id_location_old=='AfG'){echo 'selected="selected" ';} ?> >กระทรวงมหาดไทย กรมส่งเสริมการปกครองส่วนท้องถิ่น</option>
						<option value="AfH" <?php if($affiliation_id_location_old=='AfH'){echo 'selected="selected" ';} ?> >กรุงเทพมหานคร</option>
						<option value="AfI" <?php if($affiliation_id_location_old=='AfI'){echo 'selected="selected" ';} ?> >เมืองพัทยา</option>
						<option value="AfJ" <?php if($affiliation_id_location_old=='AfJ'){echo 'selected="selected" ';} ?> >สำนักงานตำรวจแห่งชาติ</option>
						<option value="AfO" <?php if($affiliation_id_location_old=='AfO'){echo 'selected="selected" ';} ?> >อื่นๆ โปรดระบุ....</option>
					</select>
					-->
					<select name="affsub_id" id="affsub_id" class="form-control" style="height:40px; <?php if($affsub_name_location_old!=''){echo 'display:block;';}else{echo 'display:none;';} ?>">
						<option value="" disabled="disabled" selected="selected">ถ้าต้องการแก้ไข ให้เริ่มเลือกใหม่ตั้งแต่สังกัดหลักด้านบน</option>
						<option value="<?php echo $affsub_id_location_old; ?>" selected="selected"><?php echo $affsub_name_location_old; ?></option>
					</select>
					<div id="affsub_text" <?php if($affsub_ans_id_location_old!=''){echo 'style="display:block" ';}else{echo 'style="display:none" ';} ?>>
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="affsub_ans_id" id="affsub_ans_id" type="text" class="form-control" 
							placeholder="<?php if($affsub_ans_id_location_old!=''){echo $affsub_ans_id_location_old;}else{echo 'โปรดระบุสังกัด';}?>" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<select name="affsub2_id" id="affsub2_id" class="form-control" style="height:40px; <?php if($affsub2_name_location_old!=''){echo 'display:block;';}else{echo 'display:none;';} ?>">
						<option value="" disabled="disabled" selected="selected">ถ้าต้องการแก้ไข ให้เริ่มเลือกใหม่ตั้งแต่สังกัดหลักด้านบน</option>
						<option value="<?php echo $affsub2_id_location_old; ?>" selected="selected"><?php echo $affsub2_name_location_old; ?></option>
					</select>
					<select name="affsub3_id" id="affsub3_id" class="form-control" style="height:40px; <?php if($affsub3_name_location_old!=''){echo 'display:block;';}else{echo 'display:none;';} ?>">
						<option value="" disabled="disabled" selected="selected">ถ้าต้องการแก้ไข ให้เริ่มเลือกใหม่ตั้งแต่สังกัดหลักด้านบน</option>
						<option value="<?php echo $affsub3_id_location_old; ?>" selected="selected"><?php echo $affsub3_name_location_old; ?></option>
					</select>
					<select name="affsub4_id" id="affsub4_id" class="form-control" style="height:40px; <?php if($affsub4_name_location_old!=''){echo 'display:block;';}else{echo 'display:none;';} ?>">
						<option value="" disabled="disabled" selected="selected">ถ้าต้องการแก้ไข ให้เริ่มเลือกใหม่ตั้งแต่สังกัดหลักด้านบน</option>
						<option value="<?php echo $affsub4_id_location_old; ?>" selected="selected"><?php echo $affsub4_name_location_old; ?></option>
					</select>
					<div id="affsub4_text" <?php if($affsub4_ans_id_location_old!=''){echo 'style="display:block" ';}else{echo 'style="display:none" ';} ?>>
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="affsub4_ans_id" id="affsub4_ans_id" type="text" class="form-control" 
							placeholder="<?php if($affsub4_ans_id_location_old!=''){echo $affsub4_ans_id_location_old;}else{echo 'โปรดระบุสังกัด';}?>" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
<!-- End สังกัด Content -->