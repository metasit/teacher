<!-- Start อาชีพ, ตำแหน่ง Content -->
					<!-- เลือกอาชีพ -->
					<?php 
					if(!isset($occupation_id)) $occupation_id = "";
					?>
					<label for="Oc" class="center m-t-10 text-black">ตำแหน่ง</label>
					<select id="Oc" name="occupation_id_pupil_new" class="form-control" style="height:40px" onchange="PickOccupation_pupil_new(this.value);" required>
						<option value="" disabled="disabled" selected="selected">เลือกอาชีพ</option>
						<option value="OcA" <?php if($occupation_id=='OcA'){echo 'selected="selected" ';} ?> >ข้าราชการ พนักงานราชการ ลูกจ้างประจำ อัตราจ้าง ประเภทครูและบุคลากรทางการศึกษา</option>
						<option value="OcB" <?php if($occupation_id=='OcB'){echo 'selected="selected" ';} ?> >ข้าราชการ พนักงานราชการ ลูกจ้างประจำ อัตราจ้าง ที่ไม่ใช่ประเภทครูและบุคลากรทางการศึกษา</option>
						<option value="OcC" <?php if($occupation_id=='OcC'){echo 'selected="selected" ';} ?> >ข้าราชการบำนาญ/เกษียณ</option>
						<option value="OcD" <?php if($occupation_id=='OcD'){echo 'selected="selected" ';} ?> >เจ้าของธุรกิจ</option>
						<option value="OcE" <?php if($occupation_id=='OcE'){echo 'selected="selected" ';} ?> >พนักงานบริษัท/รับจ้าง</option>
						<option value="OcF" <?php if($occupation_id=='OcF'){echo 'selected="selected" ';} ?> >นักเรียน/นักศึกษา</option>
						<option value="OcG" <?php if($occupation_id=='OcG'){echo 'selected="selected" ';} ?> >อาชีพอิสระ</option>
						<option value="OcO" <?php if($occupation_id=='OcO'){echo 'selected="selected" ';} ?> >อื่นๆ โปรดระบุ....</option>
					</select>


					<!-- OcA List -->
					<select id="pomain4OcA_pupil_new" name="pomain_id_pupil_new" class="form-control" style="display:none; height:40px;" onchange="PickPoMain4OcA_pupil_new(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ผู้ปฏิบัติหน้าที่สอน</option>
						<option value="B">ผู้บริหารสถานศึกษา</option>
						<option value="C">ผู้บริหารการศึกษา</option>
						<option value="D">บุคลากรทางการศึกษาอื่น</option>
						<option value="O">อื่นๆ ระบุ</option>
					</select>
					<!-- OcB List -->
					<div id="pomain4OcB_pupil_new" <?php  if(!isset($occupation_id)) $occupation_id = ""; if($occupation_id=='OcB'){echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcB1_pupil_new" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
						</div>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcB2_pupil_new" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
						</div>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcB3_pupil_new" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่)" />
						</div>
					</div>
					<!-- OcC List -->
					<div id="pomain4OcC_pupil_new" <?php if($occupation_id=='OcC'){echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcC1_pupil_new" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
						</div>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcC2_pupil_new" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
						</div>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcC3_pupil_new" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่)" />
						</div>
					</div>
					<!-- OcD List -->
					<div id="pomain4OcD_pupil_new" <?php if($occupation_id=='OcD'){echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcD1_pupil_new" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
						</div>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcD2_pupil_new" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
						</div>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcD3_pupil_new" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่)" />
						</div>
					</div>
					<!-- OcE List -->
					<div id="pomain4OcE_pupil_new" <?php if($occupation_id=='OcE'){echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcE1_pupil_new" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
						</div>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcE2_pupil_new" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
						</div>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcE3_pupil_new" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่)" />
						</div>
					</div>
					<!-- OcF List -->
					<div id="pomain4OcF_pupil_new" <?php if($occupation_id=='OcF'){echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcF1_pupil_new" class="form-control" placeholder="ตำแหน่ง/หน้าที่ในโรงเรียน" />
						</div>
					</div>
					<!-- OcG List -->
					<div id="pomain4OcG_pupil_new" <?php if($occupation_id=='OcG'){echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcG1_pupil_new" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
						</div>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcG2_pupil_new" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
						</div>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcG3_pupil_new" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่)" />
						</div>
					</div>
					<!-- OcO List -->
					<div id="pomain4OcO_pupil_new" <?php if($occupation_id=='OcO'){echo ' style="display:block" ';}else{ echo ' style="display:none" ';} ?>>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcO1_pupil_new" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
						</div>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcO2_pupil_new" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
						</div>
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
							<input class="input100" type="text" name="pomain_id_ansOcO3_pupil_new" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่)" />
						</div>
					</div>
					<!-- เลือกตำแหน่งย่อยขั้น1 -->
					<!-- OcAA List -->
					<select id="posub4OcAA_pupil_new" name="posub1_id_pupil_new" class="form-control" 
					style="display:none; height:40px;" 
					onchange="PickPoSub4OcAA_pupil_new(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ครู</option>
						<option value="B">ครู กศน.</option>
						<option value="C">ครู ตชด.</option>
						<option value="D">อาจารย์</option>
					</select>
					<!-- OcAB List -->
					<select id="posub4OcAB_pupil_new" name="posub1_id_pupil_new" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAB_pupil_new(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองผู้อำนวยการสถานศึกษา</option>
						<option value="B">ผู้อำนวยการสถานศึกษา</option>
						<option value="C">เจ้าของสถานศึกษา/ผู้รับใบอนุญาต</option>
						<option value="D">ผู้อำนวยการกศน.</option>
						<option value="E">ครูใหญ่ โรงเรียน ตชด.</option>
					</select>
					<!-- OcAC List -->
					<select id="posub4OcAC_pupil_new" name="posub1_id_pupil_new" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAC_pupil_new(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">สำนักงานเขตพื้นที่การศึกษา</option>
						<option value="B">ศึกษาธิการ</option>
						<option value="C">ผู้บริหารส่วนกลางกระทรวง</option>
						<option value="D">สถาบันอุดมศึกษา</option>
					</select>
					<!-- OcAD List -->
					<select id="posub4OcAD_pupil_new" name="posub1_id_pupil_new" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAD_pupil_new(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ศึกษานิเทศก์</option>
					</select>
					<!-- OcAO List -->
					<div id="posub4OcAO_pupil_new" style="display:none">
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
							<input class="input100" name="posub1_id_ans_pupil_new" type="text" class="form-control" placeholder="ตำแหน่ง" />
						</div>
					</div>
					<!-- เลือกตำแหน่งย่อยขั้น2 -->
					<!-- OcAAA List -->
					<select id="posub4OcAAA_pupil_new" name="posub2_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ครูอัตราจ้าง</option>
						<option value="B">ครูผู้ช่วย</option>
						<option value="C">ครู</option>
						<option value="D">ครูชำนาญการ</option>
						<option value="E">ครูชำนาญการพิเศษ</option>
						<option value="F">ครูเชี่ยวชาญ</option>
						<option value="G">ครูเชี่ยวชาญพิเศษ</option>
					</select>
					<!-- OcAAB List -->
					<select id="posub4OcAAB_pupil_new" name="posub2_id_pupil_new" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAAB_pupil_new(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ข้าราชการครู</option>
						<option value="B">ครู กศน.ตำบล</option>
						<option value="C">ครู ศูนย์การเรียนรู้ชุมชน</option>
						<option value="D">ครู อาสาสมัคร</option>
						<option value="E">ครู สอนคนพิการ</option>
						<option value="F">ครู ประกาศนียบัตรวิชาชีพ</option>
						<option value="O">อื่น ๆ ระบุ....</option>
					</select>
					<!-- OcAABO List -->
					<div id="posub4OcAABO_pupil_new" style="display:none">
						<div class="wrap-input100 m-t-10">
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
							<input class="input100" name="posub3_id_ans_pupil_new" type="text" class="form-control" placeholder="ตำแหน่ง" />
						</div>
					</div>
					<!-- OcAAC List -->
					<!-- No Content in this list -->
					<!-- OcAAD List -->
					<select id="posub4OcAAD_pupil_new" name="posub2_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">อาจารย์</option>
						<option value="B">ผู้ช่วยศาสตราจารย์</option>
						<option value="C">รองศาสตราจารย์</option>
						<option value="D">ศาสตราจารย์</option>
					</select>
					<!-- OcABA List -->
					<select id="posub4OcABA_pupil_new" name="posub2_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองผู้อำนวยการโรงเรียนเอกชน</option>
						<option value="B">รองผู้อำนวยการชำนาญการ</option>
						<option value="C">รองผู้อำนวยการชำนาญการพิเศษ</option>
						<option value="D">รองผู้อำนวยการเชี่ยวชาญ</option>
					</select>
					<!-- OcABB List -->
					<select id="posub4OcABB_pupil_new" name="posub2_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ผู้อำนวยการโรงเรียนเอกชน</option>
						<option value="B">ผู้อำนวยการชำนาญการ</option>
						<option value="C">ผู้อำนวยการชำนาญการพิเศษ</option>
						<option value="D">ผู้อำนวยการเชี่ยวชาญ</option>
						<option value="F">ผู้อำนวยการเชี่ยวชาญพิเศษ</option>
					</select>
					<!-- OcABC List -->
					<!-- No Content in this list -->
					<!-- OcABD List -->
					<select id="posub4OcABD_pupil_new" name="posub2_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ผู้อำนวยการ กศน.ตำบล</option>
						<option value="B">ผู้อำนวนการ กศน.จังหวัด</option>
					</select>
					<!-- OcABE List -->
					<!-- No Content in this list -->
					<!-- OcACA List -->
					<select id="posub4OcACA_pupil_new" name="posub2_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาชำนาญการพิเศษ</option>
						<option value="B">รองผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญ</option>
						<option value="C">ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญ</option>
						<option value="D">ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญพิเศษ</option>
					</select>
					<!-- OcACB List -->
					<select id="posub4OcACB_pupil_new" name="posub2_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองศึกษาธิการจังหวัด</option>
						<option value="B">ศึกษาธิการจังหวัด</option>
						<option value="C">รองศึกษาธิการภาค</option>
						<option value="D">ศึกษาธิการภาค</option>
					</select>
					<!-- OcACC List -->
					<select id="posub4OcACC_pupil_new" name="posub2_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองผู้อำนวยการสำนัก</option>
						<option value="B">ผู้อำนวยการสำนัก</option>
						<option value="C">ผู้เชี่ยวชาญ</option>
						<option value="D">ผู้ตรวจราชการ/ที่ปรึกษาระดับ 10</option>
						<option value="F">ปลัด/รองปลัด/อธิบดี/รองอธิบดี</option>
					</select>
					<!-- OcACD List -->
					<select id="posub4OcACD_pupil_new" name="posub2_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองคณบดี</option>
						<option value="B">คณบดี</option>
						<option value="C">รองอธิการบดี</option>
						<option value="D">อธิการบดี</option>
					</select>
					<!-- OcADA List -->
					<select id="posub4OcADA_pupil_new" name="posub2_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ศึกษานิเทศก์ชำนาญการ</option>
						<option value="B">ศึกษานิเทศก์ชำนาญการพิเศษ</option>
						<option value="C">ศึกษานิเทศก์เชี่ยวชาญ</option>
						<option value="D">ศึกษานิเทศก์เชี่ยวชาญพิเศษ</option>
					</select>
					<!-- End อาชีพ, ตำแหน่ง Content -->