<!-- Start สังกัด Content -->
					<!-- เลือกสังกัด -->
					<label for="affiliation_id_pupil_new" class="center m-t-10 text-black">โรงเรียน</label>
					<select name="affiliation_id_pupil_new" id="affiliation_id_pupil_new" class="form-control" style="height:40px;" required>
						<option value="" disabled="disabled" selected="selected">เลือกสังกัด</option>
						<option value="AfA">สำนักงานปลัดกระทรวงศึกษาธิการ</option>
						<option value="AfB">สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน</option>
						<option value="AfC">สำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน</option>
						<option value="AfD">สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย</option>
						<option value="AfE">สำนักงานคณะกรรมการการอาชีวศึกษา</option>
						<option value="AfF">สำนักงานคณะกรรมการการอุดมศึกษา</option>
						<option value="AfG">กระทรวงมหาดไทย กรมส่งเสริมการปกครองส่วนท้องถิ่น</option>
						<option value="AfH">กรุงเทพมหานคร</option>
						<option value="AfI">เมืองพัทยา</option>
						<option value="AfJ">สำนักงานตำรวจแห่งชาติ</option>
						<option value="AfO">อื่นๆ โปรดระบุ....</option>
					</select>

					<select name="affsub_id_pupil_new" id="affsub_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
					</select>
					<div id="affsub_text_pupil_new" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="affsub_ans_id_pupil_new" id="affsub_ans_id_pupil_new" type="text" class="form-control" placeholder="โปรดระบุสังกัด" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<select name="affsub2_id_pupil_new" id="affsub2_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
					</select>
					<select name="affsub3_id_pupil_new" id="affsub3_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
					</select>
					<select name="affsub4_id_pupil_new" id="affsub4_id_pupil_new" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
					</select>
					<div id="affsub4_text_pupil_new" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="affsub4_ans_id_pupil_new" id="affsub4_ans_id_pupil_new" type="text" class="form-control" placeholder="โปรดระบุสังกัด" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- End สังกัด Content -->