<?php
		/* Print & Save เกียรติบัตร */
		$sql = "UPDATE `login` SET `basic_score_status`='Approve แล้ว/ยืนยันแล้ว' WHERE ID='$ID' ";
		$res = $con->query($sql) or die($con->error); //Check error
	
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		/* Save เกียนติบัตรออนไลน์ with user name */
		$jpg_image = imagecreatefromjpeg('images/เกียรติบัตร/พื้นฐาน/ครู/เกียรติบัตรขั้นพื้นฐาน-ครู.jpg'); // Create Image From Existing File
		$font_color = imagecolorallocate($jpg_image, 0, 0, 0); // Set font color
		$font_path = 'layout/styles/fonts/THSarabunIT๙ Bold.ttf'; // Set font file path

		$sqllogin = "SELECT * FROM `login` WHERE ID='$ID' ";
		$relogin = mysqli_query($con,$sqllogin);
		$rowlogin = mysqli_fetch_array($relogin);
		/* Set Text that need to be Printed On Image */
		$name = $_SESSION['firstname'].' '.$_SESSION['lastname']; // Set name
		// Set School
		include('includes/separatestarAff.php');
		if($affsub4_name != '') {
			$school = $affsub4_name;
		}else{
			if($affsub3_name != '') {
				$school = $affsub3_name;
			}else{
				if($affsub2_name != '') {
					$school = $affsub2_name;
				}else{
					if($affsub_name != '') {
						$school = $affsub_name;
					}else{
						$school = $affiliation_name;
					}
				}
			}
		}
		// Set Running Number for basic certificate
		$sqlbasic_score_date = "SELECT `basic_score_date` FROM `login` WHERE ID='$ID' ";
		$rebasic_score_date = mysqli_query($con, $sqlbasic_score_date);
		$rowBSD = mysqli_fetch_array($rebasic_score_date);
	
		$sqlbasic_score_cer_code = "SELECT MAX(CAST(SUBSTRING(basic_score_cer_code,-5) AS UNSIGNED)) AS max_basic_score_cer_code FROM `basic_score_cer` ";
		$rebasic_score_cer_code = $con->query($sqlbasic_score_cer_code);
		$rowBSCC = $rebasic_score_cer_code->fetch_assoc();
	
		$basic_score_cer_type = 'คดผ.';
		$year = substr(date('Y',strtotime($rowBSD['basic_score_date']))+543,-2);
		$basic_score_cer_num = sprintf('%05d',$rowBSCC['max_basic_score_cer_code']+1);
		$basic_score_cer_code = $basic_score_cer_type.$year.'00'.$basic_score_cer_num;
	
		$sqlUPbasic_score_num = "INSERT INTO `basic_score_cer` (`ID`,`basic_score_cer_type`,`basic_score_cer_code`)	VALUES ('$ID','$basic_score_cer_type','$basic_score_cer_code') ";
		$reUPbasic_score_num = $con->query($sqlUPbasic_score_num) or die($con->error); //Check error
	
		//Set date
		$sqladscorelog = "SELECT * FROM `adscorelog` WHERE ID_user='$ID' AND adscorelog_task='Approve แล้ว' AND adscorelog_detail='ขั้นพื้นฐาน' ORDER BY adscorelog_id DESC LIMIT 1 ";
		$readscorelog = mysqli_query($con,$sqladscorelog);
		$rowadscorelog = mysqli_fetch_array($readscorelog);
		$date = new DateTime($rowadscorelog['adscorelog_date']);
		$date = $date->format("Y-m-d"); // Set date format
	
		/* Array for Thai Date */
		$arabicnum = array("1","2","3","4","5","6","7","8","9","0");
		$thainum = array("๑","๒","๓","๔","๕","๖","๗","๘","๙","๐");
		$test = str_replace($numthai,$numarabic,$message);
		/* Array for Thai Month */
		$month_arr=array(
			"1"=>"มกราคม",
			"2"=>"กุมภาพันธ์",
			"3"=>"มีนาคม",
			"4"=>"เมษายน",
			"5"=>"พฤษภาคม",
			"6"=>"มิถุนายน", 
			"7"=>"กรกฎาคม",
			"8"=>"สิงหาคม",
			"9"=>"กันยายน",
			"10"=>"ตุลาคม",
			"11"=>"พฤศจิกายน",
			"12"=>"ธันวาคม"                 
		);
		$date_cer = str_replace($arabicnum, $thainum, date('j', strtotime($date))).' '.$month_arr[date('n', strtotime($date))].' '.str_replace($arabicnum, $thainum, (date('Y', strtotime($date))+543)); // Set certicate date in Thai
	
		$font_size = 40; //Set font size
		$angle = 0; //Set angle
		/* Set x-position on certificate for name */
		$dimen4name = imagettfbbox($font_size, $angle, $font_path, $name);
		$text4name = (abs($dimen4name[4] - $dimen4name[0]))/2;
		$x4name = 758 - $text4name;
		/* Set x-position on certificate for name */
		$dimen4school = imagettfbbox($font_size, $angle, $font_path, $school);
		$text4school = (abs($dimen4school[4] - $dimen4school[0]))/2;
		$x4school = 758 - $text4school;
		/* Print firstname, lastname, school and date on certificate */
		imagettftext($jpg_image, $font_size, $angle, $x4name, 402, $font_color, $font_path, $name);
		imagettftext($jpg_image, $font_size, $angle, $x4school, 465, $font_color, $font_path, $school);
		imagettftext($jpg_image, $font_size, $angle, 690, 730, $font_color, $font_path, $date_cer);
		imagettftext($jpg_image, 30, $angle, 1150, 150, $font_color, $font_path, $basic_score_cer_code);
	
		$target_dir = 'images/เกียรติบัตร/พื้นฐาน/ครู/'.$date.'/'; // Set target_directory
	
		if(!is_dir($target_dir)) { // if there's not folder in target_directory
			mkdir($target_dir); // Create folder name is today_date
		}
	
		imagejpeg($jpg_image,'images/เกียรติบัตร/พื้นฐาน/ครู/'.$date.'/'.$name.'.jpg');// Send Image to Browser or save in directory on client
		imagedestroy($jpg_image); // Clear Memory

?>