<?php

	$sqlhonor_score4sup = "SELECT * FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code='A' ";
	$reHS4S = mysqli_query($con, $sqlhonor_score4sup);
	$rowHS4S = mysqli_fetch_array($reHS4S);

	$honor_score4sup_data1 = $rowHS4S['honor_score4sup_data1'];
	
	// หมวดครองตน NOTE: มี6ตัวบ่งชี้
	for($x=1; $x<=18; $x++) {
		$i = strpos($honor_score4sup_data1, '.'); // Find position of each score
		$j = substr($honor_score4sup_data1, $i+1, 1); // Return value of each score
		// var_dump($honor_score4sup_data);
		// var_dump($j);
		

		$score[$x] = $j; // Set valuable for each score
		if(!isset($selfscore)) $selfscore = 0;
		$selfscore +=  (float) (float) $score[$x]; // Accumulate all score
		
		if($x>=1 && $x<=5) { // ตัวบ่งชี้ที่1: รักและศรัทธาในวิชาชีพ
			if(!isset($selfscore_sub1)) {  $selfscore_sub1 = 0; }
			$selfscore_sub1 += (float) $score[$x];
		}elseif($x>=6 && $x<=10) { //ตัวบ่งชี้ที่2: ปฏิบัติตนเป็นแบบอย่างที่ดี
			if(!isset($selfscore_sub2)) $selfscore_sub2 = 0;
			$selfscore_sub2 += (float) $score[$x];
		}elseif($x>=11 && $x<=13) { //ตัวบ่งชี้ที่3: ดำรงชีวิตเหมาะสมตามหลักปรัชญาของเศรษฐกิจพอเพียง
			if(!isset($selfscore_sub3)) $selfscore_sub3 = 0;
			$selfscore_sub3 += (float) $score[$x];
		}elseif($x>=14 && $x<=16) { //ตัวบ่งชี้ที่4: มีจิตสำนึกที่ดี
			if(!isset($selfscore_sub4)) $selfscore_sub4 = 0;
			$selfscore_sub4 += (float) $score[$x];
		}elseif($x==17) { //ตัวบ่งชี้ที่5: มีจิตสาธารณะ/จิตอาสา
			if(!isset($selfscore_sub5)) $selfscore_sub5 = 0;
			$selfscore_sub5 += (float) $score[$x];
		}elseif($x==18) { //ตัวบ่งชี้ที่6: เรียนรู้ตลอดชีวิต
			if(!isset($selfscore_sub6)) $selfscore_sub6 = 0;
			$selfscore_sub6 += (float) $score[$x]; 
		}	
		$honor_score4sup_data1 = substr($honor_score4sup_data1, $i+1);
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// หมวดครองคน NOTE: มี3ตัวบ่งชี้
	for($x=19; $x<=22; $x++) {
		$i = strpos($honor_score4sup_data1, '.'); // Find position of each score
		$j = substr($honor_score4sup_data1, $i+1, 1); // Return value of each score

		$score[$x] = $j; // Set valuable for each score
		if(!isset($peoplescore)) $peoplescore = 0;
		$peoplescore += (float) $score[$x]; // Accumulate all score

		if($x==19) { //ตัวบ่งชี้ที่7: ให้บริการที่ดี
			if(!isset($peoplescore_sub7)) $peoplescore_sub7 = 0;
			$peoplescore_sub7 += (float) $score[$x];
		}elseif($x==20) { //ตัวบ่งชี้ที่8: ทำงานเป็นทีม
			if(!isset($peoplescore_sub8)) $peoplescore_sub8 = 0;
			$peoplescore_sub8 += (float) $score[$x];
		}elseif($x>=21 && $x<=22) { //ตัวบ่งชี้ที่9: ปฏิบัติตนกับบุคคลรอบข้างอย่างเหมาะสม
			if(!isset($peoplescore_sub9)) $peoplescore_sub9 = 0;
			$peoplescore_sub9 += (float) $score[$x];
		}

		$honor_score4sup_data1 = substr($honor_score4sup_data1, $i+1);
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// หมวดครองงาน NOTE: มี7ตัวบ่งชี้
	
	$workscore_sub10 = 0;
	$k = 0;
	for($x=23; $x<=37; $x++) {
		$i = strpos($honor_score4sup_data1, '.'); // Find position of each score
		if($x == 23) {
			for($y=1; $y<=8; $y++) {
				if(!isset($k)) $k = 0;
				$k += intval(substr($honor_score4sup_data1, $i+$y, 1));
			}
			$j = $k/8;
		}else{
			$j = intval(substr($honor_score4sup_data1, $i+1, 1)); // Return value of each score
		}
		$score[$x] = $j; // Set valuable for each score
		if(!isset($workscore)) $workscore = 0;
		$workscore += (float) $score[$x]; // Accumulate all score
		
		if($x>=23 && $x<=25) { //ตัวบ่งชี้ที่10: มีทักษะการนิเทศทางการศึกษา	
			if(!isset($workscore_sub10)) $workscore_sub10 = 0;
			// echo $workscore_sub10."<br/>"; 
			$workscore_sub10 += (float) $score[$x];
			// echo $score[$x]."<br>";
			// echo $workscore_sub10."<br/>";
		}elseif($x>=26 && $x<=30) { //ตัวบ่งชี้ที่11: มีทักษะการคิดและสร้างสรรค์นวัตกรรมทางการนิเทศ
			
			if(!isset($workscore_sub11)) $workscore_sub11 = 0;
			$workscore_sub11 += (float) $score[$x];

		}elseif($x>=31 && $x<=37) { //ตัวบ่งชี้ที่12: มีทักษะการสานพลังเครือข่ายความร่วมมือ
			
			if(!isset($workscore_sub12)) $workscore_sub12 = 0;
			$workscore_sub12 += (float) $score[$x];

		}

		if($x == 23) {
			$honor_score4sup_data1 = substr($honor_score4sup_data1, $i+8);
		}else{
			$honor_score4sup_data1 = substr($honor_score4sup_data1, $i+1);
		}
	}

	// echo "================================<br/>";

	// สรุปคะแนนรวม แยกเป็นรายหมวด
	$selfscore_avg = number_format($selfscore/18, 2);
	$peoplescore_avg = number_format($peoplescore/4, 2);
	$workscore_avg = number_format($workscore/15, 2);
	// หาจำนวนหมวดที่ผ่าน
	if($selfscore_avg >= 3) {
		$numberpass_sub = 1;
	}else{
		$numberpass_sub = 0;
	}

	if($peoplescore_avg >= 3) {
		$numberpass_sub += 1;
	}else{
		$numberpass_sub += 0;
	}

	if($workscore_avg >= 3) {
		$numberpass_sub += 1;
	}else{
		$numberpass_sub += 0;
	}
	// สรุปคะแนน เป็นจุดทศนิยม2ตำแหน่ง ของแต่ละ ตัวบ่งชี้
	$selfscore_sub1_avg = number_format($selfscore_sub1/5, 2); $selfscore_sub1 = 0;
	$selfscore_sub2_avg = number_format($selfscore_sub2/5, 2); $selfscore_sub2 = 0; 
	$selfscore_sub3_avg = number_format($selfscore_sub3/3, 2); $selfscore_sub3 = 0;
	$selfscore_sub4_avg = number_format($selfscore_sub4/3, 2); $selfscore_sub4 = 0;
	$selfscore_sub5_avg = number_format($selfscore_sub5/1, 2); $selfscore_sub5 = 0;
	$selfscore_sub6_avg = number_format($selfscore_sub6/1, 2); $selfscore_sub6 = 0;

	$peoplescore_sub7_avg = number_format($peoplescore_sub7/1, 2); $peoplescore_sub7 = 0;
	$peoplescore_sub8_avg = number_format($peoplescore_sub8/1, 2); $peoplescore_sub8 = 0;
	$peoplescore_sub9_avg = number_format($peoplescore_sub9/2, 2); $peoplescore_sub9 = 0;
	


	$workscore_sub10_avg = number_format($workscore_sub10/3, 2); 
	$workscore_sub10 = 0;

	$workscore_sub11_avg = number_format($workscore_sub11/5, 2); 
	$workscore_sub11 = 0;
	
	$workscore_sub12_avg = number_format($workscore_sub12/7, 2); 
	$workscore_sub12 = 0;
	
	// Set ค่า Valueble สำหรับชื่อหมวดทั้งหมด
	
	$selfscore_sub1_topic = 'รักและศรัทธาในวิชาชีพ';
	$selfscore_sub2_topic = 'ปฏิบัติตนเป็นแบบอย่างที่ดี';
	$selfscore_sub3_topic = 'ดำรงชีวิตเหมาะสมตามหลักปรัชญาของเศรษฐกิจพอเพียง';
	$selfscore_sub4_topic = 'มีจิตสำนึกที่ดี';
	$selfscore_sub5_topic = 'มีจิตสาธารณะ/จิตอาสา';
	$selfscore_sub6_topic = 'เรียนรู้ตลอดชีวิต';

	$peoplescore_sub7_topic = 'ให้บริการที่ดี';
	$peoplescore_sub8_topic = 'ทำงานเป็นทีม';
	$peoplescore_sub9_topic = 'ปฏิบัติตนกับบุคคลรอบข้างอย่างเหมาะสม';

	$workscore_sub10_topic = 'มีทักษะการนิเทศทางการศึกษา';
	$workscore_sub11_topic = 'มีทักษะการคิดและสร้างสรรค์นวัตกรรมทางการนิเทศ';
	$workscore_sub12_topic = 'มีทักษะการสานพลังเครือข่ายความร่วมมือ';
	// หาคะแนนหมวดสูงสุด 3 อันดับ
	
	$i = array(
		$selfscore_sub1_topic=>$selfscore_sub1_avg, 
		$selfscore_sub2_topic=>$selfscore_sub2_avg, 
		$selfscore_sub3_topic=>$selfscore_sub3_avg, 
		$selfscore_sub4_topic=>$selfscore_sub4_avg, 
		$selfscore_sub5_topic=>$selfscore_sub5_avg, 
		$selfscore_sub6_topic=>$selfscore_sub6_avg, 

		$peoplescore_sub7_topic=>$peoplescore_sub7_avg, 
		$peoplescore_sub8_topic=>$peoplescore_sub8_avg, 
		$peoplescore_sub9_topic=>$peoplescore_sub9_avg, 

		$workscore_sub10_topic=>$workscore_sub10_avg, 
		$workscore_sub11_topic=>$workscore_sub11_avg, 
		$workscore_sub12_topic=>$workscore_sub12_avg, 
	);

	// $workscore_sub10_avg = 0;

	arsort($i); // เรียงจากมากไปน้อย เรียงทั้ง Key ทั้ง Value
	$max34sub = array_slice($i, 0, 3); // เลือกแค่3อันดับแรกมากที่สุด
	// หาคะแนนหมวดน้อยสุด 3 อันดับ
	asort($i); // เรียงจากน้อยไปมาก เรียงทั้ง Key ทั้ง Value
	$min34sub = array_slice($i, 0, 3); // เลือกแค่3อันดับแรกน้อยที่สุด
	

?>