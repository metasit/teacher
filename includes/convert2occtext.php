<?php

if($occupation_id == 'OcA') {
	$occupation_name = 'ข้าราชการ พนักงานราชการ ลูกจ้างประจำ อัตราจ้าง ประเภทครูและบุคลากรทางการศึกษา';

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	if($pomain_id == 'A') {
		$pomain_name = 'ผู้ปฏิบัติหน้าที่สอน';
		if($posub1_id == 'A') {
			$posub1_name = 'ครู';
			if($posub2_id == 'A') {
				$posub2_name = 'ครูอัตราจ้าง';
			}elseif($posub2_id == 'B') {
				$posub2_name = 'ครูผู้ช่วย';
			}elseif($posub2_id == 'C') {
				$posub2_name = 'ครู';
			}elseif($posub2_id == 'D') {
				$posub2_name = 'ครูชำนาญการ';
			}elseif($posub2_id == 'E') {
				$posub2_name = 'ครูชำนาญการพิเศษ';
			}elseif($posub2_id == 'F') {
				$posub2_name = 'ครูเชี่ยวชาญ';
			}elseif($posub2_id == 'G') {
				$posub2_name = 'ครูเชี่ยวชาญพิเศษ';
			}
		}elseif($posub1_id == 'B') {
			$posub1_name = 'ครู กศน.';
			if($posub2_id == 'A') {
				$posub2_name = 'ข้าราชการครู';
			}elseif($posub2_id == 'B') {
				$posub2_name = 'ครู กศน.ตำบล';
			}elseif($posub2_id == 'C') {
				$posub2_name = 'ครู ศูนย์การเรียนรู้ชุมชน';
			}elseif($posub2_id == 'D') {
				$posub2_name = 'ครู อาสาสมัคร';
			}elseif($posub2_id == 'E') {
				$posub2_name = 'ครู สอนคนพิการ';
			}elseif($posub2_id == 'F') {
				$posub2_name = 'ครู ประกาศนียบัตรวิชาชีพ';
			}else{
				$posub2_name = 'อื่นๆ ระบุ';
			}
		}elseif($posub1_id == 'C') {
			$posub1_name = 'ครู ตชด.';
		}elseif($posub1_id == 'D') {
			$posub1_name = 'อาจารย์';
			if($posub2_id == 'A') {
				$posub2_name = 'อาจารย์';
			}elseif($posub2_id == 'B') {
				$posub2_name = 'ผู้ช่วยศาสตราจารย์';
			}elseif($posub2_id == 'C') {
				$posub2_name = 'รองศาสตราจารย์';
			}elseif($posub2_id == 'D') {
				$posub2_name = 'ศาสตราจารย์';
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
	}elseif($pomain_id == 'B') {
		$pomain_name = 'ผู้บริหารสถานศึกษา';
		if($posub1_id == 'A') {
			$posub1_name = 'รองผู้อำนวยการสถานศึกษา';
			if($posub2_id == 'A') {
				$posub2_name = 'รองผู้อำนวยการโรงเรียนเอกชน';
			}elseif($posub2_id == 'B') {
				$posub2_name = 'รองผู้อำนวยการชำนาญการ';
			}elseif($posub2_id == 'C') {
				$posub2_name = 'รองผู้อำนวยการชำนาญการพิเศษ';
			}elseif($posub2_id == 'D') {
				$posub2_name = 'รองผู้อำนวยการเชี่ยวชาญ';
			}
		}elseif($posub1_id == 'B') {
			$posub1_name = 'ผู้อำนวยการสถานศึกษา';
			if($posub2_id == 'A') {
				$posub2_name = 'ผู้อำนวยการโรงเรียนเอกชน';
			}elseif($posub2_id == 'B') {
				$posub2_name = 'ผู้อำนวยการชำนาญการ';
			}elseif($posub2_id == 'C') {
				$posub2_name = 'ผู้อำนวยการชำนาญการพิเศษ';
			}elseif($posub2_id == 'D') {
				$posub2_name = 'ผู้อำนวยการเชี่ยวชาญ';
			}elseif($posub2_id == 'E') {
				$posub2_name = 'ผู้อำนวยการเชี่ยวชาญพิเศษ';
			}
		}elseif($posub1_id == 'C') {
			$posub1_name = 'เจ้าของสถานศึกษา/ผู้รับใบอนุญาต';
		}elseif($posub1_id == 'D') {
			$posub1_name = 'ผู้อำนวยการกศน.';
			if($posub2_id == 'A') {
				$posub2_name = 'ผู้อำนวยการ กศน.ตำบล';
			}elseif($posub2_id == 'B') {
				$posub2_name = 'ผู้อำนวนการ กศน.จังหวัด';
			}
		}elseif($posub1_id == 'E') {
			$posub1_name = 'ครูใหญ่ โรงเรียน ตชด.';
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
	}elseif($pomain_id == 'C') {
		$pomain_name = 'ผู้บริหารการศึกษา';
		if($posub1_id == 'A') {
			$posub1_name = 'สำนักงานเขตพื้นที่การศึกษา';
			if($posub2_id == 'A') {
				$posub2_name = 'รองผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาชำนาญการพิเศษ';
			}elseif($posub2_id == 'B') {
				$posub2_name = 'รองผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญ';
			}elseif($posub2_id == 'C') {
				$posub2_name = 'ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญ';
			}elseif($posub2_id == 'D') {
				$posub2_name = 'ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญพิเศษ';
			}
		}elseif($posub1_id == 'B') {
			$posub1_name = 'ศึกษาธิการ';
			if($posub2_id == 'A') {
				$posub2_name = 'รองศึกษาธิการจังหวัด';
			}elseif($posub2_id == 'B') {
				$posub2_name = 'ศึกษาธิการจังหวัด';
			}elseif($posub2_id == 'C') {
				$posub2_name = 'รองศึกษาธิการภาค';
			}elseif($posub2_id == 'D') {
				$posub2_name = 'ศึกษาธิการภาค';
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
	}elseif($pomain_id == 'D') {
		$pomain_name = 'บุคลากรทางการศึกษาอื่น';
		if($posub1_id == 'A') {
			$posub1_name = 'ศึกษานิเทศก์';
			if($posub2_id == 'A') {
				$posub2_name = 'ศึกษานิเทศก์ชำนาญการ';
			}elseif($posub2_id == 'B') {
				$posub2_name = 'ศึกษานิเทศก์ชำนาญการพิเศษ';
			}elseif($posub2_id == 'C') {
				$posub2_name = 'ศึกษานิเทศก์เชี่ยวชาญ';
			}elseif($posub2_id == 'D') {
				$posub2_name = 'ศึกษานิเทศก์เชี่ยวชาญพิเศษ';
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
	}else{
		$pomain_name = 'อื่นๆ ระบุ';
	}


}elseif($occupation_id == 'OcB') {
	$occupation_name = 'ข้าราชการ พนักงานราชการ ลูกจ้างประจำ อัตราจ้าง ที่ไม่ใช่ประเภทครูและบุคลากรทางการศึกษา';


}elseif($occupation_id == 'OcC') {
	$occupation_name = 'ข้าราชการบำนาญ/เกษียณ';


}elseif($occupation_id == 'OcD') {
	$occupation_name = 'เจ้าของธุรกิจ';


}elseif($occupation_id == 'OcE') {
	$occupation_name = 'พนักงานบริษัท/รับจ้าง';


}elseif($occupation_id == 'OcF') {
	$occupation_name = 'นักเรียน/นักศึกษา';
	if($pomain_id == 'A') {
		$pomain_name = 'ประธาน/รองประธาน/กรรมการ นักเรียน-นักศึกษา';
	}elseif($pomain_id == 'B') {
		$pomain_name = 'ประธาน/รองประธาน/กรรมการ ชมรม-ชุมนุม-สโมสร-กิจกรรม';
	}elseif($pomain_id == 'C') {
		$pomain_name = 'หัวหน้า/รองหัวหน้า ห้อง-กลุ่ม-ชั้นปี';
	}elseif($pomain_id == 'D') {
		$pomain_name = 'นักเรียน/นักศึกษาทั่วไป';
	}


}elseif($occupation_id == 'OcG') {
	$occupation_name = 'อาชีพอิสระ';


}elseif($occupation_id == 'OcG') {
	$occupation_name = 'อาชีพอิสระ';


}else{
	$occupation_name = 'อื่นๆ ระบุ....';
}




if($pomain_name != '') {
	$full_occupation_name = '<strong>'.$pomain_name.'</strong>';

	if($posub1_name != '') {
		$full_occupation_name = $pomain_name.'<br><strong>'.$posub1_name.'</strong>';

		if($posub2_name != '') {
			$full_occupation_name = $pomain_name.'<br>'.$posub1_name.'<br><strong>'.$posub2_name.'</strong>';
		
			
		}
	}
}




?>