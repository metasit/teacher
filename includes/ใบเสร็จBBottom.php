</div>


		<div style="width:100%; margin-top:20px; font-size:23px; line-height:30px; margin-top:58px; ">
			<div style="text-align:center;">
				ใบเสร็จรับเงิน/ใบกำกับภาษี
				<br>
				RECEIPT/TAX INVOICE
			</div>
			<div style="width:80%; display:inline-block; text-align:right;">
					วันที่
			</div>
			<div style="width:19.4%; display:inline-block; text-align:center;">
				<?php
					$month_arr=array(
						"1"=>"มกราคม",
						"2"=>"กุมภาพันธ์",
						"3"=>"มีนาคม",
						"4"=>"เมษายน",
						"5"=>"พฤษภาคม",
						"6"=>"มิถุนายน", 
						"7"=>"กรกฎาคม",
						"8"=>"สิงหาคม",
						"9"=>"กันยายน",
						"10"=>"ตุลาคม",
						"11"=>"พฤศจิกายน",
						"12"=>"ธันวาคม"                 
				);
				
				echo date("d", strtotime($rowMS['membership_date']))." ".$month_arr[date("n", strtotime($rowMS['membership_date']))]." ".(date("Y", strtotime($rowMS['membership_date']))+543);
			?>
			</div>
			<div style="width:80%; display:inline-block; text-align:right;">
				เลขที่
			</div>
			<div style="width:19.4%; display:inline-block; text-align:center;">
				<?php echo $rowMS['bill_code']; ?>
			</div>
				
			<div style="width:59%; display:inline-block; text-align:left;">
				ได้รับเงินจาก 	<?php echo $rowlogin['firstname'].' '.$rowlogin['lastname']; ?>
			</div>
			<div style="width:20%; display:inline-block; text-align:left;">

			</div>
			<div style="width:59%; display:inline-block; text-align:left;">
				ที่อยู่  <?php echo $rowlogin['docaddress']; ?>
			</div>
			<div style="width:30.4%; display:inline-block; text-align:left;">
				
			</div>
			<div style="width:25%; display:inline-block; text-align:left;">
				<?php if($rowMS['bill_tax_kind'] == 'Personal' || $rowMS['bill_tax_kind'] == 'Corporation') { ?>
								เลขประจำตัวผู้เสียภาษีอากร
				<?php }else{ ?>
								หน่วยงานราชการ
				<?php } ?>				
			</div>


			<div style="width:33.5%; display:inline-block; text-align:left;">
				<?php echo $rowMS['bill_tax_id']; ?>
			</div>
			<div style="width:33.4%; display:inline-block; text-align:left;">
				<i class="fa-square fs-40"></i> สำนักงานใหญ่ &nbsp;&nbsp;&nbsp;&nbsp; <i class="fa-square fs-40"></i> สาขา.....
			</div>
			<div style="width:80.4%; display:inline-block; text-align:left; margin-top:30px; " >
				ระดับ	&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp;&nbsp;
				 <i class="fa-square fs-40"></i> เงิน  &nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp;&nbsp;
				 <i class="fa-square fs-40"></i> ทอง  &nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp;&nbsp;
				 <i class="fa-square fs-40"></i> เพชร  &nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp;&nbsp;
				 <i class="fa-square fs-40"></i> ปรับระดับ  &nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp;&nbsp;
			</div>


					<table  style="width:100%;">
					   <thead>
						   <tr>
							   <th style="border: 1px solid;">ลำดับ</th>
							   <th style="border: 1px solid;">รายการ</th>
							   <th style="border: 1px solid;">ราคาต่อหน่วย</th>
							   <th style="border: 1px solid;">จำนวน</th>
							   <th style="border: 1px solid;">ราคา</th>
						   </tr>
					   </thead>
					   <tbody>
						   <tr style="">
							   	<td style="border-left: 1px solid; border-right: 1px solid;  
								 		  vertical-align: top;  
								   ">1</td>
							   	<td style=" ; text-align:left; ">
									<div>ค่าบำรุงสมาชิกประจำปี</div>
									<div>ระหว่างวันที่ 1 มกราคม 2564-วันที่ 1 มกราคม 2565</div>
								</td>
							   	<td style="border-left: 1px solid; text-align:right; vertical-align: top; padding:5px; padding-top:0;">
							   		<div style="display:flex; justify-content: flex-end; align-items: flex-end;">
									   <?php
									 		echo number_format($membership_price,2);  
									   ?>
									</div>
								</td>
							   	<td style="  border-left: 1px solid; border-right: 1px solid;  text-align:center; vertical-align: top; padding:5px; padding-top:0; ">
									<div style="width:100%; height:100%;" >1</div>
								</td>
							   <td style=" border-right: 1px solid; text-align:right; vertical-align: top; padding:5px; padding-top:0;">
							   	<?php echo number_format($membership_price,2); ?>
							   </td>
						   </tr>
						   <tr>
							   <td style="border-left: 1px solid; border-right: 1px solid; ">2</td>
							   <td style=" text-align:left;" >ฟรีสมาชิก......เดือน............ถึง</td>
							   <td style="border-left: 1px solid; text-align:right; vertical-align: top; padding:5px; padding-top:0;">0</td>
							   <td style="border-left: 1px solid; text-align:right; vertical-align: top; padding:5px; padding-top:0;">0</td>
							   <td style="border-left: 1px solid; border-right: 1px solid;  text-align:right; vertical-align: top; padding:5px; padding-top:0;">0</td>
						   </tr>

						   <tr>
							   <td style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;  ">1</td>
							   <td style="border-bottom: 1px solid; text-align:left; ">ปรับระดับสมาชิก</td>
							   <td style=" border-bottom: 1px solid; border-left: 1px solid; text-align:right; vertical-align: top; padding:5px; padding-top:0;">.......</td>
							   <td style="border-left: 1px solid; border-bottom: 1px solid; "></td>
							   <td style="  border-right: 1px solid; border-bottom: 1px solid; border-left: 1px solid; text-align:right; vertical-align: top; padding:5px; padding-top:0;">.......</td>
						   </tr>

						   <tr style="border:none;">
							   <td></td>
							   <td></td>
							   <td style="text-align:right;">ราคาสุทธิ</td>
							   <td></td>
							   <td  style=" padding:0; padding-right:5px; text-align:right; border-right: 1px solid; border-bottom: 1px solid; border-left: 1px solid; " >
							   <?php 
							 		echo  number_format($membership_price,2)  
							 	?>
							   </td>
						   </tr>

						   <tr style="border:none;">
							   <td></td>
							   <td></td>
							   <td style="text-align:right;">ภาษีมูลค่าเพิ่ม 7%</td>
							   <td></td>
							   <td style=" text-align:right; padding:0; padding-right:5px; border-right: 1px solid; border-bottom: 1px solid; border-left: 1px solid;">
							   	<?php
									echo    number_format($membership_price * 0.07 ,2)   
								 ?>
							   </td>
						   </tr>

						   <tr style="border:none;">
							   <td></td>
							   <td>(ห้าร้อยบาทถ้วน)</td>
							   <td style="text-align:right;">ราคารวม</td>
							   <td></td>
							   <td style="text-align:right; padding:0; padding-right:5px; border-right: 1px solid; border-bottom: 1px solid; border-left: 1px solid;">
							   	<?php 
									echo number_format( $membership_price * 0.07 + $membership_price ,2)    
								?>
							   </td>
						   </tr>
					   </tbody>
					</table>




				<div style="margin-top: 50px; margin-left: 109px;width: 350px; ">
					<div style=" background:black; width:100%; height:1px "></div>
					<div style="text-align:center; margin-top:20px">ผู้รับเงิน</div>
					<div style="text-align:center; margin-top:30px;"> 1 มกราคม 2564 </div>
				</div>



			<!-- <table style="width:100%; margin-top:40px;">
				<thead>
					<tr>
						<th>ลำดับที่</th>
						<th>รายการ</th>
						<th>ราคาต่อหน่วย</th>
						<th>จำนวน</th>
						<th>ราคา</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td style="text-align:left;">บำรุงสมาชิกรายปี</td>
						<td><?php echo number_format($membership_price,2); ?></td>
						<td>1</td>
						<td style="text-align:right; padding-right:0;"><?php echo number_format($membership_price,2); ?></td>
					</tr>
					<tr>
						<td class="" style="text-align:left;" ></td>
						<td colspan="2" class= "" style="text-align:left;" >ระหว่างวันที่ 5 มกราคม 2564 - วันที่ 5 มกราคม 2565</td>
						<td class="" style="text-align:left;" >CCCC</td>
						<td class="" style="text-align:left;" >ราคาสุทธิ</td>
						<td class="" style="text-align:end; padding-right:0px;" class="pr-0"  >280.32</td>
					</tr>

				</tbody>
			</table> -->


			<?php
				$price_tax = $membership_price;
				$price_notax = $price_tax*100/107;
				$tax = $price_tax - $price_notax; 
			?>
		
			<!--
			<div style="width:74.4%; display:inline-block; margin-top:20px; text-align:right; font-weight:700;">
				ค่าขนส่ง
			</div>
			<div style="width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php 
				// echo number_format($shipping_cost,2);
				 ?>
			</div>
			<div style="width:74.4%; display:inline-block; margin-top:10px; text-align:right; font-weight:700;">
				ส่วนลด
			</div>
			<div style="width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php
				//  echo number_format($rowsale['sale_total'],2);
				  ?>
			</div>
			<div style="width:74.4%; display:inline-block; text-align:right; font-weight:700;">
				ราคาสุทธิหลังหักส่วนลด
			</div>
			<div style="width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php
				//  echo number_format($price_notax,2); 
				 ?>
			</div>
			-->
			<div style="width:74.4%; display:inline-block; margin-top:20px; text-align:right; font-weight:700;">
				<!-- ภาษีมูลค่าเพิ่ม  -->
			</div>
			<div class="d-flex justify-content-between" style="width:25%; display:inline-block;  font-weight:700;">
				 	<span style="margin-left: 10px;margin-right: 117px;" class="mr-auto" >
					 <!-- 7% -->
					</span> 
					<span>
						<?php 
						// echo number_format($tax,2);
						 ?>
					</span>
			</div>
			<div style="width:49.5%; display:inline-block; text-align:right; font-weight:700;">
				<?php
				//  echo Convert($membership_price);
				  ?>
			</div>
			<div style="width:24.4%; display:inline-block; text-align:right; font-weight:700;">
				<!-- ราคารวม -->
			</div>
			<div style="width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php 
				// echo number_format($membership_price,2);
				 ?>
			</div>
			
			<br><br>

			<div style="width:49.2%; display:inline-block; text-align:center; margin-top:20px;">
				<!-- .............................................. -->
			</div>
			<div style="width:49.2%; display:inline-block; text-align:center;">
				<!-- .............................................. -->
			</div>
			<div style="width:49.2%; display:inline-block; text-align:center;">
				<!-- ผู้รับเงิน -->
			</div>
			<div style="width:49.2%; display:inline-block; text-align:center;">
				<!-- ผู้รับสินค้า -->
			</div>
			
			<div style="width:49.2%; display:inline-block; text-align:center;">
				<!-- วันที่........................................... -->
			</div>

		</div>
	</div>

<!-- echo number_format($membership_price,2); -->
<?php 
	// echo number_format($tax,2);
?>
<!-- echo number_format($membership_price,2); -->