</div>


		<div style="width:100%; margin-top:20px; font-size:23px; line-height:30px; margin-top:58px;">
			<div style="text-align:center;">
				ใบเสร็จรับเงิน/ใบกำกับภาษี
				<br>
				RECEIPT/TAX INVOICE
			</div>
			<div style="width:80%; display:inline-block; text-align:right;">
					วันที่
			</div>
			<div style="width:19.4%; display:inline-block; text-align:left; padding-left:20px;">
				<?php
					$month_arr=array(
						"1"=>"มกราคม",
						"2"=>"กุมภาพันธ์",
						"3"=>"มีนาคม",
						"4"=>"เมษายน",
						"5"=>"พฤษภาคม",
						"6"=>"มิถุนายน", 
						"7"=>"กรกฎาคม",
						"8"=>"สิงหาคม",
						"9"=>"กันยายน",
						"10"=>"ตุลาคม",
						"11"=>"พฤศจิกายน",
						"12"=>"ธันวาคม"                 
				);
				
				echo date("d", strtotime($roworders1['slip_date']))." ".$month_arr[date("n", strtotime($roworders1['slip_date']))]." ".(date("Y", strtotime($roworders1['slip_date']))+543);
			?>
			</div>
			<div style="width:80%; display:inline-block; text-align:right;">
				เลขที่
			</div>
			<div style="width:19.4%; display:inline-block; text-align:left; padding-left:20px; ">
				<?php echo $rowbill['bill_code']; ?>
			</div>
				
			<div style="width:25%; display:inline-block; text-align:left;">
				ได้รับเงินจาก
			</div>
			<div style="width:74.4%; display:inline-block; text-align:left;">
				<?php echo str_replace('<br>',' ',$roworders1['contact_name']); ?>
			</div>
			<div style="width:25%; display:inline-block; text-align:left;">
				ที่อยู่
			</div>
			<div style="width:74.4%; display:inline-block; text-align:left;">
				<?php echo str_replace('<br>',' ',$roworders1['shipping_address']); ?>
			</div>
			<div style="width:32%; display:inline-block; text-align:left;">
				<?php if($rowbill['bill_tax_kind'] == 'Personal' || $rowbill['bill_tax_kind'] == 'Corporation') { ?>
								เลขประจำตัวผู้เสียภาษีอากร
				<?php }else{ ?>
								หน่วยงานราชการ
				<?php } ?>				
			</div>
			<div style="width:25%; display:inline-block; text-align:left;">
				<?php echo $rowbill['bill_tax_id']; ?>
			</div>
			<div style="width:28.4%; display:inline-block; text-align:left;">
				<i class="fa-square fs-40"></i> สำนักงานใหญ่ &nbsp;&nbsp;&nbsp;&nbsp; <i class="fa-square fs-40"></i> สาขา.....
			</div>
			
			

			<?php
				$price_tax = $rowtotal['price_total']+$shipping_cost; //680
				$price_notax = $price_tax*7/100; //635.51
				$tax = $price_tax - $price_notax;
			?>

			<table style="width:100%; margin-top:40px;">
				<thead>
					<tr style="border: 1px solid">
						<th style="border: 1px solid" >ลำดับที่
					<?php 
						// echo $rowtotal['price_total']."<br/>";
						// echo $shipping_cost."<br/>";
						// echo $price_notax."<br/>";
						// echo $tax."<br/>";
					?>
					</th>
						<th style="border: 1px solid" width="40%"  >รายการ</th>
						<th style="border: 1px solid" >ราคาต่อหน่วย</th>
						<th style="border: 1px solid" >จำนวน</th>
						<th style="border: 1px solid" >ราคา</th>
					</tr>
				</thead>
				<tbody>
						<?php $i=1;
							$sqlorders="SELECT * FROM orders WHERE order_group='$order_group' ";
							$resultorders=mysqli_query($con,$sqlorders);

							while($roworders = $resultorders->fetch_assoc()) {
								$product_id = $roworders['product_id'];
								$sqlshelf="SELECT * FROM shelf WHERE product_id='$product_id' ";
								$resultshelf=mysqli_query($con,$sqlshelf);
								$rowshelf=mysqli_fetch_array($resultshelf);
								?>
								<tr>
									<td style="border: 1px solid" ><?php echo $i; ?></td>
									<td style="border: 1px solid; text-align:left;"  ><?php echo $rowshelf['product_name']; ?></td>
									<td style="border-left: 1px solid; border-bottom: 1px solid;  text-align:right; vertical-align: top; padding:5px; padding-top:0;" ><?php echo number_format($roworders['product_price'],2); ?></td>
									<td style="border-left:  1px solid; border-bottom:  1px solid;  text-align:center; vertical-align: top; padding:5px; padding-top:0;" ><?php echo $roworders['product_amount']; ?></td>
									<td style="border-left: 1px solid;  border-right: 1px solid; text-align:right; vertical-align: top; padding:5px; padding-top:0"  style="text-align:right; padding-right:0;">
									<?php echo number_format($roworders['product_amount']*$roworders['product_price'],2); ?>
								</td>
								</tr>
						<?php	
						$i++; 
						} ?>

							<tr>
								<td></td>
								<td colspan="2" style="text-align:right;">
									ค่าขนส่ง
								</td>
								
								<td></td>
								<td style="border: 1px solid; text-align:right; padding-right:5px;">
									<?php echo number_format($shipping_cost,2); ?>
								</td>
							</tr>
							<tr>
								<td></td>
								<td colspan="2" style="text-align:right;">ส่วนลด</td>
								<td></td>
								<td style="border: 1px solid; text-align:right; padding-right:5px;">
									<?php echo number_format($rowsale['sale_total'],2); ?>
								</td>
							</tr>
							<tr>
								<td></td>
								<td colspan="2" style="text-align:right;">ราคาสุทธิหลังหักส่วนลด</td>
								<td></td>
								<td style="border: 1px solid; text-align:right; padding-right:5px;">
								<?php 
									// echo number_format($tax,2);

								// echo number_format($roworders['product_amount']*$roworders['product_price'],2)
								//  $price_token = number_format($roworders['product_amount']*$roworders['product_price'],2);
								//  $shipping_token = number_format($shipping_cost,2);
								//  $sale_token = number_format($rowsale['sale_total'],2);

								//  $total_token = $price_token + $shipping_token - $sale_token;
								//  $total_token = $total_token + ($total_token*0.07);
								//  echo $total_token;
								 // echo number_format($price_notax,2);
								
								?>
									
									<?php echo number_format($tax,2); ?>
								</td>
							</tr>
							<tr>
								<td ></td>
								<td colspan="2" style="text-align:right;">ภาษีมูลค่าเพิ่ม 7%</td>
								<td></td>
								<td style="border: 1px solid; text-align:right; padding-right:5px; ">
									<?php 
										// echo  $rowtotal['price_total']+$shipping_cost -  $tax;
									?>
									<?php echo number_format($price_notax,2); ?>

								</td>
							</tr>
							<tr>
								<td></td>
								<td>(หนึ่งพันหกร้อยเจ็ดสิบเก้า)</td>
								<td style="text-align:right;">ราคารวม</td>
								<td></td>
								<td style="border: 1px solid; text-align:right; padding-right:5px;">
									<?php echo number_format($rowtotal['price_total']+$shipping_cost,2); ?>
								</td>
							</tr>
				</tbody>
			</table>

			<!-- <div style="width:74.4%; display:inline-block;  text-align:right; font-weight:700;">
				ค่าขนส่ง
			</div>
			<div  style=" border: 1px solid; width:25%; display:inline-block; text-align:right; font-weight:700; ">
				<?php echo number_format($shipping_cost,2); ?>
			</div>
			<div style="width:74.4%; display:inline-block;  text-align:right; font-weight:700;">
				ส่วนลด
			</div>
			<div style=" border: 1px solid; width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php echo number_format($rowsale['sale_total'],2); ?>
			</div>
			<div style="width:74.4%; display:inline-block; text-align:right; font-weight:700;">
				ราคาสุทธิหลังหักส่วนลด
			</div>
			<div style=" border: 1px solid; width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php echo number_format($price_notax,2); ?>
			</div>
			<div style="width:74.4%; display:inline-block; text-align:right; font-weight:700;">
				ภาษีมูลค่าเพิ่ม 7
			</div>
			
			<div style=" border: 1px solid; width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php echo number_format($tax,2); ?>
			</div>
			<div style="width:49.5%; display:inline-block; text-align:right; font-weight:700;">
				(<?php echo Convert($rowtotal['price_total']+$shipping_cost); ?>)
			</div>
			<div style="width:24.4%; display:inline-block; text-align:right; font-weight:700;">
				ราคารวม
			</div>
			<div style="  border: 1px solid; width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php echo number_format($rowtotal['price_total']+$shipping_cost,2); ?>
			</div> -->
			
			<br><br>

			<div style="width:49.2%; display:inline-block; text-align:left; margin-top:20px;">
				..............................................
			</div>
			<div style="width:49.2%; display:inline-block; text-align:left;">
				..............................................
			</div>
			<div style="width:49.2%; display:inline-block; text-align:left;">
			<div style="padding-left:60px;">
				ผู้รับเงิน
			</div>
				
			</div>
			<div style="width:49.2%; display:inline-block; text-align:left;">
				<div style="padding-left:60px;">
					ผู้รับสินค้า
				</div>
			</div>
			<div style="width:49.2%; display:inline-block; text-align:left;">
				 <div style="padding-left:40px;">
				 	1 มกราคม 2564
				 </div>
			</div>
			<div style="width:49.2%; display:inline-block; text-align:left;">
				วันที่
			</div>
		</div>
	</div>