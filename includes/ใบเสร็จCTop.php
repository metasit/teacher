<div class="hoc clear p-t-60 p-b-80">
	<div style="width:20%; display:inline-block; text-align:center; margin-bottom:50px;">
		<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Footer.png" alt="มูลนิธิครูดีของแผ่นดิน Logo Footer.png" style="width:100px;">
	</div>
	<div style="width:50%; display:inline-block; text-align:left; margin-left:20px; font-size:23px; font-weight:700; line-height:40px; vertical-align:-108px">
		มูลนิธิครูดีของแผ่นดิน(สำนักงานใหญ่)
		<br>
		103 ถนนสุวิสุทธิ์กษัตริย์ แขวงบางขุนพรหม เขตพระนคร กรุงเทพฯ 10200
		<br>
		เลขประจำตัวผู้เสียภาษี 099-3-00038742-2
		<br>
		<span style="margin-right:20px">โทร 02-001-1515</span>
		<span>แฟกซ์ 02-001-1368</span>
	</div>
	<div style="width:19.4%; display:inline-block; text-align:center; font-size:50px; font-weight:700;">