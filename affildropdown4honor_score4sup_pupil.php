<?php
	include('includes/convert2afftext.php');
?>

<!-- Start สังกัด Content -->
<!-- เลือกสังกัด -->
<label for="affiliation_id" class="center m-t-10 text-black">โรงเรียน</label>
<select name="affiliation_id" id="affiliation_id" class="form-control" style="height:40px;" required>
	<option value="" disabled="disabled" selected="selected">เลือกสถานที่</option>
	<option value="AfA" <?php if($affiliation_id=='AfA'){echo 'selected="selected" ';} ?> >สำนักงานปลัดกระทรวงศึกษาธิการ</option>
	<option value="AfB" <?php if($affiliation_id=='AfB'){echo 'selected="selected" ';} ?> >สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน</option>
	<option value="AfC" <?php if($affiliation_id=='AfC'){echo 'selected="selected" ';} ?> >สำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน</option>
	<option value="AfD" <?php if($affiliation_id=='AfD'){echo 'selected="selected" ';} ?> >สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย</option>
	<option value="AfE" <?php if($affiliation_id=='AfE'){echo 'selected="selected" ';} ?> >สำนักงานคณะกรรมการการอาชีวศึกษา</option>
	<option value="AfF" <?php if($affiliation_id=='AfF'){echo 'selected="selected" ';} ?> >สำนักงานคณะกรรมการการอุดมศึกษา</option>
	<option value="AfG" <?php if($affiliation_id=='AfG'){echo 'selected="selected" ';} ?> >กระทรวงมหาดไทย กรมส่งเสริมการปกครองส่วนท้องถิ่น</option>
	<option value="AfH" <?php if($affiliation_id=='AfH'){echo 'selected="selected" ';} ?> >กรุงเทพมหานคร</option>
	<option value="AfI" <?php if($affiliation_id=='AfI'){echo 'selected="selected" ';} ?> >เมืองพัทยา</option>
	<option value="AfJ" <?php if($affiliation_id=='AfJ'){echo 'selected="selected" ';} ?> >สำนักงานตำรวจแห่งชาติ</option>
	<option value="AfO" <?php if($affiliation_id=='AfO'){echo 'selected="selected" ';} ?> >อื่นๆ โปรดระบุ....</option>
</select>
<select name="affsub_id" id="affsub_id" class="form-control" style="height:40px; <?php if($affsub_name!=''){echo 'display:block;';}else{echo 'display:none;';} ?>">
	<option value="" disabled="disabled" selected="selected">ถ้าต้องการแก้ไข ให้เริ่มเลือกใหม่ตั้งแต่ช่องด้านบนสุด</option>
	<option value="<?php echo $affsub_id; ?>" selected="selected"><?php echo $affsub_name; ?></option>
</select>
<div id="affsub_text" <?php if($affsub_ans_id!=''){echo 'style="display:block" ';}else{echo 'style="display:none" ';} ?>>
	<div class="wrap-input100 m-t-10">
		<input class="input100" name="affsub_ans_id" id="affsub_ans_id" type="text" class="form-control" 
		placeholder="<?php if($affsub_ans_id!=''){echo $affsub_ans_id;}else{echo 'โปรดระบุ';}?>" />
		<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
	</div>
</div>
<select name="affsub2_id" id="affsub2_id" class="form-control" style="height:40px; <?php if($affsub2_name!=''){echo 'display:block;';}else{echo 'display:none;';} ?>">
	<option value="" disabled="disabled" selected="selected">ถ้าต้องการแก้ไข ให้เริ่มเลือกใหม่ตั้งแต่ช่องด้านบนสุด</option>
	<option value="<?php echo $affsub2_id; ?>" selected="selected"><?php echo $affsub2_name; ?></option>
</select>
<select name="affsub3_id" id="affsub3_id" class="form-control" style="height:40px; <?php if($affsub3_name!=''){echo 'display:block;';}else{echo 'display:none;';} ?>">
	<option value="" disabled="disabled" selected="selected">ถ้าต้องการแก้ไข ให้เริ่มเลือกใหม่ตั้งแต่ช่องด้านบนสุด</option>
	<option value="<?php echo $affsub3_id; ?>" selected="selected"><?php echo $affsub3_name; ?></option>
</select>
<select name="affsub4_id" id="affsub4_id" class="form-control" style="height:40px; <?php if($affsub4_name!=''){echo 'display:block;';}else{echo 'display:none;';} ?>">
	<option value="" disabled="disabled" selected="selected">ถ้าต้องการแก้ไข ให้เริ่มเลือกใหม่ตั้งแต่ช่องด้านบนสุด</option>
	<option value="<?php echo $affsub4_id; ?>" selected="selected"><?php echo $affsub4_name; ?></option>
</select>
<div id="affsub4_text" <?php if($affsub4_ans_id!=''){echo 'style="display:block" ';}else{echo 'style="display:none" ';} ?>>
	<div class="wrap-input100 m-t-10">
		<input class="input100" name="affsub4_ans_id" id="affsub4_ans_id" type="text" class="form-control" 
		placeholder="<?php if($affsub4_ans_id!=''){echo $affsub4_ans_id;}else{echo 'โปรดระบุ';}?>" />
		<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
	</div>
</div>
<!-- End สังกัด Content -->