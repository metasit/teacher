<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($_SESSION['ID'])) {
		// Set Occupation_id
		$occupation_id = $_POST['occupation_id'];
		// Set pomain_id
		$pomain_id = $_POST['pomain_id'];
		// Set pomain_idans
		$pomain_id_ans1 = $_POST['pomain_id_ansOcB1'].$_POST['pomain_id_ansOcC1'].$_POST['pomain_id_ansOcD1'].$_POST['pomain_id_ansOcE1'].$_POST['pomain_id_ansOcF1'].$_POST['pomain_id_ansOcG1'].$_POST['pomain_id_ansOcO1'].'*';
		$pomain_id_ans2 = $_POST['pomain_id_ansOcB2'].$_POST['pomain_id_ansOcC2'].$_POST['pomain_id_ansOcD2'].$_POST['pomain_id_ansOcE2'].$_POST['pomain_id_ansOcG2'].$_POST['pomain_id_ansOcO2'].'*';
		$pomain_id_ans3 = $_POST['pomain_id_ansOcB3'].$_POST['pomain_id_ansOcC3'].$_POST['pomain_id_ansOcD3'].$_POST['pomain_id_ansOcE3'].$_POST['pomain_id_ansOcG3'].$_POST['pomain_id_ansOcO3'].'*';
		$pomain_id_ans = $pomain_id_ans1.$pomain_id_ans2.$pomain_id_ans3;
		// Set posub1_id
		$posub1_id = $_POST['posub1_id'];
		// Set posub2_id
		$posub2_id = $_POST['posub2_id'];

		// Set Affiliation_id
		if(isset($_POST['affiliation_id'])) {
			$affiliation_id = $_POST['affiliation_id'];
		}
		// Set affsub_id
		if(isset($_POST['affsub_id'])) {
			$affsub_id = $_POST['affsub_id'];
		}
		// Set affsub2_id
		if(isset($_POST['affsub2_id'])) {
			$affsub2_id = $_POST['affsub2_id'];
		}
		// Set affsub3_id
		if(isset($_POST['affsub3_id'])) {
			$affsub3_id = $_POST['affsub3_id'];
		}
		// Set affsub4_id
		if(isset($_POST['affsub4_id'])) {
			$affsub4_id = $_POST['affsub4_id'];
		}
		// Set affsub_ans_id
		if(isset($_POST['affsub_ans_id'])) {
			$affsub_ans_id = $_POST['affsub_ans_id'];
		}
		// Set affsub4_ans_id
		if(isset($_POST['affsub4_ans_id'])) {
			$affsub4_ans_id = $_POST['affsub4_ans_id'];
		}
		
		if($_POST['CFP'] == 30) {
			/* Prepare Occupation value to save to login table */
			if($occupation_id == 'OcA') {
				if($pomain_id == 'O') {
					$occup_code = 'OcAO';
					$occup_remark = $_POST['posub1_id_ans'];
				}elseif($pomain_id == 'A' && $posub1_id == 'B' && $posub2_id == 'O') {
					$occup_code = 'OcAABO';
					$occup_remark = $_POST['posub3_id_ans'].'*';
				}else{
					$occup_code = $occupation_id.$pomain_id.$posub1_id.$posub2_id;
				}
			}elseif($occupation_id == 'OcF') {
				$occup_code = 'OcF';
				$occup_remark =	$_POST['pomain_id_ansOcF1'].'*';
			}else{
				$occup_code = $occupation_id;
				$occup_remark =	$pomain_id_ans;
			}
			$occup_remark = $_SESSION['occup_remark'].$occup_remark;

			/* Prepare Affiliation value to save to login table */
			if($affiliation_id == 'AfA' || $affiliation_id == 'AfF') {
				$affil_code = $affsub2_id;
			}elseif($affiliation_id == 'AfB') {
				if($affsub_id == 'AfBA') {
					$affil_code = $affsub4_id;
				}elseif($affsub_id == 'AfBB') {
					$affil_code = $affsub3_id;
				}
			}elseif($affiliation_id == 'AfC' || $affiliation_id == 'AfE' || $affiliation_id == 'AfG') {
				$affil_code = $affsub3_id;
			}elseif($affiliation_id == 'AfD') {
				$affil_code = $affsub3_id;
				$affil_remark = $affsub4_ans_id;
			}elseif($affiliation_id == 'AfH') {
				if($affsub_id == 'AfHA') {
					$affil_code = $affsub4_id;
				}elseif($affsub_id == 'AfHB') {
					$affil_code = $affsub_id;
				}
			}elseif($affiliation_id == 'AfI') {
				$affil_code = $affsub_id;
			}elseif($affiliation_id == 'AfJ' || $affiliation_id == 'AfO') {
				$affil_code = $affiliation_id;
				$affil_remark = $affsub_ans_id;
			}

			$sqllogin = "UPDATE `login` SET occup_code='$occup_code', occup_remark='$occup_remark', affil_code='$affil_code', affil_remark='$affil_remark' WHERE ID='$ID' ";
			$relogin = $con->query($sqllogin) or die($con->error); //Check error

			$_SESSION['occup_code'] = $occup_code;
			$_SESSION['occup_remark'] = $occup_remark;
			$_SESSION['affil_code'] = $affil_code;
			$_SESSION['affil_remark'] = $affil_remark;
			echo "alert('ยินดีด้วยค่ะ คุณสามารถร่วมโครงการกับทางมูลนิธิครูดีของแผ่นดินได้แล้วค่ะ');";
			header('location: all_project.php');
		}else{
			echo "window.location.replace('index.php')";
			//echo '<script>window.history.go(-1)</script>';
		}
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}

?>