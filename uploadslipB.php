<?php	
	session_start();
	require_once("condb.php");
	date_default_timezone_set("Asia/Bangkok");

	if($_POST['CFP'] == 10) { // Protection Policy: come from page ส่งหลักฐานการโอนB.php


			$nomem_code = $_POST['nomem_code'];
			if(substr($nomem_code,0,1) == '0') {
				$email = 'GUEST '.substr($nomem_code,-3);
			}else{
				$email = 'GUEST '.$nomem_code;
			}

			/* Chack nomem_code is correct with order_number in order table or not */
			$sqlcheck = "SELECT * FROM `orders` WHERE order_number='$nomem_code' ";
			$recheck = mysqli_query($con,$sqlcheck);
			if(mysqli_num_rows($recheck) >= 1) {

				/* Set values for orders */
				$order_group = $_POST['order_group'];
				$date_pay = $_POST['date_pay'];
				$time_pay = $_POST['time_pay'];
				$pay_date = $date_pay.' '.$time_pay;
				$slip_date = date("Y-m-d H:i:s");
				/* Set Values for bill */
				$sqlsetbillvalue = "SELECT MAX(CAST(SUBSTRING(bill_code,-5) AS UNSIGNED)) AS max_bill_code FROM `bill` WHERE bill_type='IN' ";
				$resbv = $con->query($sqlsetbillvalue);
				$rowsbv = $resbv->fetch_assoc();

				$bill_type = 'IN';
				$slip_year = substr(date('Y',strtotime($slip_date))+543,-2);
				$bill_num = sprintf('%05d',$rowsbv['max_bill_code']+1);
				$bill_code = $bill_type.$slip_year.$bill_num;
				$bill_date = date("Y-m-d H:i:s");
				$bill_tax_kind = $_POST['bill_tax_kind'];
				if($_POST['bill_tax_id_Personal'] != '') {
					$bill_tax_id = $_POST['bill_tax_id_Personal'];
				}elseif($_POST['bill_tax_id_Corporation'] != '') {
					$bill_tax_id = $_POST['bill_tax_id_Corporation'];
				}else{
					$bill_tax_id = $_POST['bill_tax_id_Government'];
				}

				$target_dir = 'images/slips/'.$date_pay.'/'; // Set target_directory

				if(!is_dir($target_dir)) { // if there's not folder in target_directory
					mkdir($target_dir); // Create folder name is today_date
				}

				$target_file = $target_dir.basename($_FILES["slip_image"]["name"]); // Save image in the target folder

				if (move_uploaded_file($_FILES["slip_image"]["tmp_name"], $target_file)) {
					/* Save slip location in orders database */
					$sql = "UPDATE `orders` SET `order_status`='กำลังตรวจสอบ',`slip_image`='$target_file',`pay_date`='$pay_date',`slip_date`='$slip_date' WHERE email='$email' ";
					$res= $con->query($sql) or die($con->error);
					/* Insert bill in bill database */
					$sqlorders = "SELECT `order_group` AS order_group FROM `orders` WHERE order_number='$nomem_code' ";
					$reorders = mysqli_query($con,$sqlorders);
					$roworders = mysqli_fetch_array($reorders);
					$order_group = $roworders['order_group'];

					$sqlbill = "INSERT INTO `bill` VALUES ('$email','$order_group',NULL,'$bill_type','$bill_date','$bill_code','$bill_tax_kind','$bill_tax_id') ";
					$rebill= $con->query($sqlbill) or die($con->error);

					echo '<script>';
						echo "alert(\"ทำรายการแจ้งการโอนเรียบร้อยแล้วค่ะ\");";
						echo "alert(\"ติดตามสถานะการสั่งซื้อได้ทางอีเมลนะคะ\");";
						echo "window.location.replace('สนับสนุนของที่ระลึก.php')";
					echo '</script>';
				}else{
					echo "<script>";
						echo "alert(\"โปรดติดต่อผู้พัฒนาระบบของมูลนิธิครูดีของแผ่นดิน<br>เพื่อดำเนินการแก้ไข<br>ขอบคุณครับ\");";
						echo "window.location.replace('สนับสนุนของที่ระลึก.php')";
					echo "</script>";
				}
			}else{
				echo "<script>";
					echo "alert(\" เลขใบสั่งซื้อไม่ถูกต้อง\");"; 
					echo "window.history.back()";
				echo "</script>";
			}


	}else{
		echo "<script>window.history.go(-1)</script>";
	}
?>