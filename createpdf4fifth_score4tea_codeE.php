<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		if($_SESSION['level'] == 'memberSilver' || $_SESSION['lever'] == 'memberGold' || $_SESSION['level'] == 'memberDiamond' || $_SESSION['level'] == 'admin') {

			// require_once to use mpdf library
			require_once __DIR__ . '/vendor/autoload.php';
			// Set Thai language for Mpdf
			$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
			$fontDirs = $defaultConfig['fontDir'];

			$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
			$fontData = $defaultFontConfig['fontdata'];

			$mpdf = new \Mpdf\Mpdf([
					'fontDir' => array_merge($fontDirs, [
							__DIR__ . '/tmp',
					]),
					'fontdata' => $fontData + [
							'sarabun' => [
									'R' => 'Sarabun-Regular.ttf',
									'I' => 'Sarabun-Italic.ttf',
									'B' => 'Sarabun-Bold.ttf',
									'BI' => 'Sarabun-BoldItalic.ttf',
							]
					],
					'default_font' => 'sarabun'
			]);
			// Need to use teacher's fifth_score
			include('includes/calfifth_score-ครู.php');

		}else{/*
			echo '<script>';
				echo "alert('ประเภทสมาชิกของท่านไม่สามารถเข้าใช้ระบบการรายงานผลนี้ได้ค่ะ');";
				echo "window.location.replace('รายงานประเมินครูดีชั้นที่5.php')";
			echo '</script>';*/
		}
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<?php
		ob_start(); // Start to buffer data to print on pdf file<?php
	?>
<!-- Start Style for Radar Chart -->
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
<link rel="stylesheet" href="layout/styles/radar-chart.css">

<style>
	.radar-chart .area {
		fill-opacity: 0.7;
	}
	.radar-chart.focus .area {
		fill-opacity: 0.3;
	}
	.radar-chart.focus .area.focused {
		fill-opacity: 0.9;
	}
	.area.ปี2563, .ปี2563 .circle {
		fill: rgb(255,247,176);
		stroke: none;
	}
	/*
	.area.argentina, .argentina .circle {
		fill: #ADD8E6;
		stroke: none;
	}
	*/
</style>
<!-- End Style for Radar Chart -->
<!-- Start Script for Radar Chart -->
<script type="text/javascript" src="js/d3.v3.js"></script>
<script type="text/javascript" src="js/radar-chart.js"></script>

<script type="text/javascript">
	RadarChart.defaultConfig.color = function() {};
	RadarChart.defaultConfig.radius = 1;
</script>

<script type="text/javascript">
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var allscore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "ครองตน", value: <?php echo $selfscore_avg; ?>}, 
				{axis: "ครองคน", value: <?php echo $peoplescore_avg; ?>}, 
				{axis: "ครองงาน", value: <?php echo $workscore_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var selfscore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "<?php echo $selfscore_sub1_topic; ?>", value: <?php echo $selfscore_sub1_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub2_topic; ?>", value: <?php echo $selfscore_sub2_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub3_topic; ?>", value: <?php echo $selfscore_sub3_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub4_topic; ?>", value: <?php echo $selfscore_sub4_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub5_topic; ?>", value: <?php echo $selfscore_sub5_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub6_topic; ?>", value: <?php echo $selfscore_sub6_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub7_topic; ?>", value: <?php echo $selfscore_sub7_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub8_topic; ?>", value: <?php echo $selfscore_sub8_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub9_topic; ?>", value: <?php echo $selfscore_sub9_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var peoplescore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "<?php echo $peoplescore_sub10_topic; ?>", value: <?php echo $peoplescore_sub10_avg; ?>}, 
				{axis: "<?php echo $peoplescore_sub11_topic; ?>", value: <?php echo $peoplescore_sub11_avg; ?>}, 
				{axis: "<?php echo $peoplescore_sub12_topic; ?>", value: <?php echo $peoplescore_sub12_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var workscore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "<?php echo $workscore_sub13_topic; ?>", value: <?php echo $workscore_sub13_avg; ?>}, 
				{axis: "<?php echo $workscore_sub14_topic; ?>", value: <?php echo $workscore_sub14_avg; ?>}, 
				{axis: "<?php echo $workscore_sub15_topic; ?>", value: <?php echo $workscore_sub15_avg; ?>}, 
				{axis: "<?php echo $workscore_sub16_topic; ?>", value: <?php echo $workscore_sub16_avg; ?>}, 
				{axis: "<?php echo $workscore_sub17_topic; ?>", value: <?php echo $workscore_sub17_avg; ?>}, 
				{axis: "<?php echo $workscore_sub18_topic; ?>", value: <?php echo $workscore_sub18_avg; ?>}, 
				{axis: "<?php echo $workscore_sub19_topic; ?>", value: <?php echo $workscore_sub19_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
</script>
</head>

	<!-- Start การรายงานผลแบบ Radar Chart -->
	<section class="m-t-250">
		<div class="row center">

			<p class="fs-30 bold">แสดงคะแนนเป็นรายหมวด มีทั้งหมด 3 หมวด</p>
			<div class="col-md-3 chart_container_allscore"></div>
			<script type="text/javascript">
				RadarChart.draw(".chart_container_allscore", allscore);
			</script>

			<p class="fs-30 bold">หมวดครองตน</p>
			<div class="col-md-2 chart_container_selfscore" style="display: inline-block; padding: 10px;"></div>
			<script type="text/javascript">
				RadarChart.draw(".chart_container_selfscore", selfscore);
			</script>

			<p class="fs-30 bold">หมวดครองคน</p>
			<div class="col-md-2 chart_container_peoplescore" style="display: inline-block; padding: 0 10px;"></div>
			<script type="text/javascript">
				RadarChart.draw(".chart_container_peoplescore", peoplescore);
			</script>

			<p class="fs-30 bold">หมวดครองงาน</p>
			<div class="col-md-2 chart_container_workscore" style="display: inline-block; padding: 0 10px;"></div>
			<script type="text/javascript">
				RadarChart.draw(".chart_container_workscore", workscore);
			</script>

		</div>
	</section>
	<!-- End การรายงานผลแบบ Radar Chart -->
	
<?php
	$html = ob_get_contents();
	$mpdf->WriteHTML($html);

	date_default_timezone_set("Asia/Bangkok");
	$date2day = date('Y-m-d');
	$target_dir = 'images/fifth_score4tea/codeE/'.$date2day;
	$pdfname = '/'.$_SESSION['firstname'].' '.$_SESSION['lastname'].'.pdf';

	if(!is_dir($target_dir)) { // if there's not folder in target_directory
		mkdir($target_dir); // Create folder name is today_date
	}

	$mpdf->Output($target_dir.$pdfname);

	// Seve location of pdf file to fifth_score4tea table
	$sqlfifth_score4tea = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='E' ";
	$reFS4T = mysqli_query($con, $sqlfifth_score4tea);

	$fifth_score4tea_code = 'E';
	$fifth_score4tea_data = $target_dir.$pdfname;

	if(mysqli_num_rows($reFS4T) == 0) {
		$sql = "INSERT INTO `fifth_score4tea` (`ID`,`fifth_score4tea_code`,`fifth_score4tea_data`)	VALUES ('$ID','$fifth_score4tea_code','$fifth_score4tea_data') ";
		$re = $con->query($sql) or die($con->error); //Check error

		// Log User Action
		$sql = "INSERT INTO `scorelog` (`ID`,`scorelog_task`,`scorelog_detail`)	VALUES ('$ID','สร้างไฟล์PDF','ครู,ชั้น5,รายงานคะแนน,RadarChart') ";
		$re = $con->query($sql) or die($con->error); //Check error

	}else{
		$sql ="UPDATE `fifth_score4tea` SET `fifth_score4tea_data`='$fifth_score4tea_data' WHERE ID='$ID' AND fifth_score4tea_code='E' ";
		$re = $con->query($sql) or die($con->error); //Check error
	}

	$rowFS4T = mysqli_fetch_array($reFS4T);

	//header('location: '.$target_dir.$pdfname);

	ob_end_flush(); // End or Stop to buffer data to print on pdf file
?>

</body>
</html>