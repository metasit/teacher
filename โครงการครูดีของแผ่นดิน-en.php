<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start NoAccount Session -->
<?php
      if (empty($_SESSION['email']))
      {
?>
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
          <li class="faicon-login"><a href="login-en.php"><i class="fas fa-user-lock"></i> <text1>Log in</text1></a></li>
          <li class="faicon-login"><a href="signup-en.php"><i class="fas fa-file-signature"></i> <text1>Sign up</text1></a></li>
          <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
            <ul>
              <li><a href="โครงการครูดีของแผ่นดิน.php">ภาษาไทย</a></li>
              <!--<li><a href="#">Chinese</a></li>-->
            </ul>
          </li>
          <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
              <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
        </ul>
      </nav>
      <!-- End NoAccount Session -->
      <!-- ################################################################################################ -->
      <!-- Start Admin Session -->
<?php
      }
      elseif ($_SESSION['email'])
      {
        if ($_SESSION["level"]=="admin")
        {
?>
        <nav id="mainav2" class="PreMenu_fl_right">
          <ul class="PreMenufaico2">
            <li style="background-color:rgb(228,0,0);"><i class="fas fa-user-edit"></i> Admin</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการครูดีของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Admin Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member General Session -->
<?php
        if ($_SESSION["level"]=="memberGeneral")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการครูดีของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member General Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member SILVER Session -->
<?php
        if ($_SESSION["level"]=="memberSilver")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> Silver Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการครูดีของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member SILVER Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member GOLD Session -->
<?php
        if ($_SESSION["level"]=="memberGold")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> Gold Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการครูดีของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member GOLD Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member DIAMOND Session -->
<?php
        if ($_SESSION["level"]=="memberDiamond")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> Diamond Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการครูดีของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
      }
?>
      <!-- End Member DIAMOND Session -->
      <!-- ################################################################################################ -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index-en.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index-en.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index-en.php">Home</a></li>
          <li><a href="เกี่ยวกับเรา-en.php">About us</a></li>
          <li class="active"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>Join us</a>
            <ul>
              <li><a href="ร่วมโครงการ-en.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <li><a href="โครงการครูดีของแผ่นดิน-en.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา-en.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA-en.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน-en.php">โครงการอาสาของแผ่นดิน</a></li>
            </ul>
          </li>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">Innovation Library</a></li>
          <li><a href="ติดต่อเรา-en.php">Contact us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="ร่วมโครงการ-en.php"> Join Us</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="โครงการครูดีของแผ่นดิน-en.php"> โครงการครูดีของแผ่นดิน</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - โครงการครูดีของแผ่นดิน -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <article style="text-align:center; padding-bottom:50px;">
        <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px"><strong>ครูดีของแผ่นดิน เจริญตามรอยเบื้องพระยุคลบาท</strong></p>
          <img src="images/มูลนิธิครูดีของแผ่นดิน ร่วมงาน_ครูดี.jpg" alt="มูลนิธิครูดีของแผ่นดิน ร่วมงาน_ครูดี">
        <p class="fs-32" style="font-family:RSUText; line-height:30pt; text-align:left;">
          ลึกลงไปในมิติของความมนุษย์ที่มีความปรารถนาดีซึ่งกันและกันเป็นพื้นฐาน ทำให้สังคมของการเรียนรู้ไม่เคยจบสิ้น บรรดาแม่พิมพ์แห่งปัญญาไม่เคยสูญหาย โลกนี้ยังคงศักดิ์และศรีแห่งดวงใจอันมุ่งมั่นของคุณครูผู้ทุ่มเท 
          เสียสละ ในการอบรมสั่งสอนศิษย์ให้เป็นผู้มีความรู้คู่คุณธรรม ด้วยมุ่งหวังประโยชน์สุขแก่สังคมและประเทศชาติเป็นสำคัญ
          <br><br>
          <strong>โครงการเครือข่ายครูดีของแผ่นดิน เจริญตามรอยเบื้องพระยุคลบาท</strong> ขอเป็นฟันเฟืองหนึ่งที่จะขับเคลื่อนคุณภาพวงการศึกษาไทยและครูไทยให้ก้าวไกลรุดหน้า เพื่อสร้างสังคมแห่งคุณค่าและมีคุณภาพในอนาคต 
          ภายใต้วิสัยทัศน์ <strong>“สร้างคนดีให้แผ่นดิน”</strong> ได้จัดพิธีมอบเกียรติบัตรแก่กลุ่มครูดีของแผ่นดิน 73 กลุ่ม เพื่อเป็นขวัญกำลังใจที่ได้ประพฤติปฏิบัติตนเป็นครูดีของแผ่นดินเจริญรอยตามเบื้องพระยุคลบาท 
          ในพระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดชมหาราช บรมนาถบพิตร ซึ่งกลุ่มครูดีของแผ่นดินที่ได้รับรางวัลเบื้องต้นต้องพัฒนาตนเองอย่างสม่ำเสมอ และผ่านเกณฑ์มาตรฐาน 3 ด้านคือ การครองตน ครองคน 
          และครองงาน ตลอดจนเจริญรอยตามบาทแห่งพระราชา เพื่อรับรางวัลเครือข่ายครูดีของแผ่นดินระดับชาติ
          <br><br>
          โครงการเครือข่ายครูดีของแผ่นดิน เจริญตามรอยพระยุคลบาท มีวัตถุประสงค์เพื่อพัฒนาครูและบุคลากรทางการศึกษาให้ประพฤติตนเป็นครูดีของแผ่นดิน มีศีลธรรม อุดมด้วยจิตวิญญาณความเป็นครู 
          เจริญตามรอยพระยุคลบาท ตามหลักปรัชญาเศรษฐกิจพอเพียง มีทักษะการจัดการเรียนการสอน สำหรับผู้เรียนในศตวรรษที่ 21 สอดคล้องกับพระราชดำรัสของพระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช ว่า 
          <strong>“ให้ครูรักนักเรียน นักเรียนรักครูให้ครูสอนเด็กมีน้ำใจต่อเพื่อน ไม่ให้แข่งขันกัน ให้แข่งกับตนเอง ให้เด็กเก่งกว่าช่วยสอนเพื่อนที่ช้ากว่า ให้ครูจัดกิจกรรมให้เด็กทำ ร่วมกันเพื่อให้เกิดความสามัคคี”</strong>
          โดยกระบวนการดังกล่าวสร้างแรงบันดาลใจเพื่อพัฒนาครู และกระบวนการกลุ่ม Professional Learning Community หรือ PLC ในการขับเคลื่อนโครงการ
          <br><br>
          ดังนั้น รางวัล <strong>“ครูดีของแผ่นดิน เจริญตามรอยเบื้องพระยุคลบาท”</strong> จึงเกิดขึ้น พร้อมพลังและความตั้งใจอันดีงาม มอบให้กับคุณครูและผู้บริหารสถานศึกษาทั่วประเทศ  ด้วยการยกย่อง เชิดชูเกียรติ 
          คุณครูและผู้บริหารสถานศึกษาให้เป็นที่ประจักษ์แก่สาธารณชน นำมาซึ่งการยกระดับมาตรฐานความเป็นครูมืออาชีพในวงกว้าง อีกทั้งยังเป็นหลักฐานประกอบการพิจารณาวิทยฐานะ สำหรับครูอาจารย์และผู้บริหาร
          สถานศึกษาผู้ตั้งใจปฏิบัติหน้าที่อย่างน่าชื่นชมอีกด้วย โดยมีจุดประสงค์
          <br>
          1.	อยากให้คุณครูที่สร้างคนดีให้แผ่นดิน มีที่ยืน มีความภาคภูมิใจในตนเอง เพราะรางวัลทั่วไปมีแต่รางวัลทางวิชาการเป็นส่วนใหญ่ ไม่ค่อยมีรางวัลเกี่ยวกับการสร้างคนดีมากนัก
          <br>
          2.	อยากให้ครูช่วยครู เกิดการช่วยเหลือซึ่งกันและกันระหว่างครูด้วยกัน เพราะที่ผ่านมาหลายรางวัล เมื่อครูได้ไปแล้วรางวัลก็ต้องแก่งแย่งแข่งขันกัน มักจะไม่แบ่งปันความรู้และความดีๆ กับผู้อื่น 
          แต่โครงการนี้ออกแบบให้ครูช่วยครูเป็นสำคัญ จนเกิดเป็นกลุ่มเครือข่าย ซึ่ง 1 กลุ่มมี 3-8 คน และดูแลไม่เกิน 3 กลุ่ม คือ 34 คน ซึ่งครูแต่ละกลุ่มจะต้องปฏิบัติตามเกณฑ์ประมาณ 1 ปี 
          จนกว่าจะผ่านเกณฑ์ที่กำหนด
          <br>
          3.	รางวัลนี้ไม่มีแพ้คัดออก ไม่ต้องมาแข่งขันกันว่าใครดีกว่าใคร หากได้ตามเกณฑ์ที่กำหนดทุกคนมีสิทธิ์รับรางวัลทุกคน แต่หากไม่ได้ตามเกณฑ์ที่กำหนดให้คุณครูพัฒนาตนเองต่อไปเรื่อยๆ จนกว่าจะได้รางวัลสูงสุด 
          คือ รางวัลครูของแผ่นดินชั้นที่ 1
          <br>
          4.	เรามีนวัตกรรมสร้างคนดีให้แผ่นดิน และโครงการเครือข่ายการสร้างคนดีให้แผ่นดิน เป็นเหมือนเข็มทิศชี้แนะแนวทางในการสร้างคนดีให้กับคุณครู โดยมุ่งเน้นให้ครูพัฒนาตนเองเป็นจุดเริ่มต้นพื้นฐาน 
          แล้วขยายต่อไปยังคนรอบข้าง โดยครูที่เข้าร่วมโครงการต้องพัฒนาตนเอง 3 หมวด 19 ตัวบ่งชี้ ครองตน ครองคน ครองงาน ซึ่งมีรายละเอียดอยู่ในเอกสารโครงการ
          <br><br>
          <section>
            <p class="font-x3 p-t-50 line2-t-green1"><span style="color:rgb(180,147,31); line-height:80px"><strong>รายการพุธเช้าข่าวสพฐ.<br>ตอน โครงการเครือข่ายครูดีของแผ่นดิน</strong></p>
            <iframe width="640px" height="360px" src="https://www.youtube.com/embed/m1sted8bcKc"></iframe>
          </section>
          <ul class="fs-32 textlink m-t-50 p-t-50 line2-t-green1" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
						<li><a href="docs/ร่วมโครงการ/ครูดี/โครงการเครือข่ายครูดีของแผ่นดิน-ผสาน.pdf">ดาวน์โหลดรายละเอียดโครงการ</a></li>
						<!--
            <li><a href="docs/ร่วมโครงการ/ครูดี/เล่ม 1 คู่มือใช้งานสำหรับผู้ที่ต้องการเข้าร่วม.pdf">ดาวน์โหลด คู่มือการใช้ระบบวัดประเมินสมรรถนะครูดีของแผ่นดิน สำหรับผู้ที่สนใจสมัครเข้าร่วมโครงการ</a></li>
            <li><a href="docs/ร่วมโครงการ/ครูดี/เล่ม 2 คู่มือสำหรับสมาชิกเก่า.pdf">ดาวน์โหลด เล่ม 2 คู่มือสำหรับสมาชิกเก่า</a></li>
						-->
						<li><a class="blink1">New</a><a href="docs/ร่วมโครงการ/ครูดี/คำอธิบายตัวชี้วัด51ปรับตามผู้เชี่ยวชาญ.pdf">ดาวน์โหลดพฤติกรรมตามตัวบ่งชี้ 19 ตัวบ่งชี้</a></li>
						<a name="secA"></a> <!-- back btn from pop basic_score -->
            <li><a href="docs/ร่วมโครงการ/ครูดี/สไลด์แนะนำโครงการ.pdf">ดาวน์โหลดสไลด์แนะนำโครงการ</a></li>
            <li><a href="docs/ร่วมโครงการ/ครูดี/สมุดภาพโครงการ.pdf">ดาวน์โหลดสมุดภาพโครงการ</a></li>
            <li><a href="docs/ร่วมโครงการ/ศึกษานิเทศก์ดี/ตัวอย่างหนังสือรับรองเพื่อประกอบการพิจารณารางวัล.pdf">ตัวอย่างหนังสือรับรองเพื่อประกอบการพิจารณารางวัลครูดีของแผ่นดิน</a></li>
            <li><a href="docs/จดหมาย/จดหมาย17/จม.63ผู้เกี่ยวข้อง.pdf">หนังสือแจ้งปฏิทินการดำเนินโครงการฯ ปี 2563</a></li>
            <li><a href="docs/จดหมาย/จดหมาย18/จม.63หน่วยMOU.pdf">หนังสือโครงการเครือข่ายครูดีของแผ่นดิน ถึงหน่วยงานที่ MOU ร่วมกับมูลนิธิครูดีของแผ่นดิน</a></li>
            <li><a href="https://drive.google.com/drive/folders/1wrx-ff1M4_JeIwAEhsyiLSp3L1jI-YJj?usp=sharing" target="_blank">ประกาศรางวัลครูดีของแผ่นดินขั้นพื้นฐาน</a></li>
            <li><a href="https://drive.google.com/drive/folders/1ahw1_TXbsV-jUzhaEKGRvtuko4pECJl7?usp=sharing" target="_blank">ประกาศรางวัลครูดีของแผ่นดินชั้นที่ 5</a></li>
          </ul>
        </p>
    </article>
    <div class="one_first m-t-50">
      <div class="btnJoin">
        <a href="สนับสนุนมูลนิธิ-en.php"><h6>Donate</h6></a>
      </div>
    </div>
  </main>
</div>
<!-- End Content 04 - โครงการครูดีของแผ่นดิน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear">
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Related Department</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Foundation of Thai Suprateacher</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>