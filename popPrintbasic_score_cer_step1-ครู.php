<?php
	session_start();
	require_once('condb.php');

	$ID = $_SESSION['ID'];
	$sqllogin = "SELECT * FROM `login` WHERE ID='$ID' ";
	$relogin = mysqli_query($con,$sqllogin);
	$rowlogin = mysqli_fetch_array($relogin);

	if(!$_SESSION['ID']) {
		header('location: login.php');
	}else{
		}if($rowlogin['basic_score_status'] == 'Approve แล้ว') {
			if($rowlogin['occup_code'] != '' && $rowlogin['affil_code'] != '') {
				$go2page = 'popPrintbasic_score_cer_step2OccAffReady-ครู.php';
			}else{
				$go2page = 'popPrintbasic_score_cer_step2-ครู.php';
			}
		}elseif($rowlogin['basic_score_status'] == 'Approve แล้ว/ยืนยันแล้ว') {
			header('location: popPrintbasic_score_cer-ครู.php');
		}else{
			echo "<script>window.history.go(-1)</script>";
		}
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-form-title fs-30 lh-1-1">
					ยินดีด้วยค่ะ
					<br>
					ท่านได้รับรางวัลครูดีของแผ่นดิน ขั้นพื้นฐาน
					<br><br>
					ตรวจสอบและยืนยันการใช้ชื่อ-นามสกุล, อาชีพ, ตำแหน่ง และสังกัด
					<br>
					(หลังจากเปลี่ยนหรือยืนยันการใช้ข้อมูลทั้งหมดนี้ ท่านจะไม่สามารถเปลี่ยนแปลงได้อีกต่อไป โปรดตรวจสอบข้อมูลอย่างละเอียด)
					<br><br>
					<!-- Start show step -->
					<div class="center">
						<span class="dot BG-green1"></span>
						<span class="line" style="width:25%"></span>
						<span class="dot"></span>
						<span class="line" style="width:25%"></span>
						<span class="dot"></span>
						<span class="line" style="width:25%"></span>
						<span class="dot"></span>
					</div>
					<div style="text-align:left;">
						<p style="color:rgb(94,177,26);">ขั้นตอนที่ 1: ยืนยันชื่อและนามสกุล</p>
					</div>
					<!-- End show step -->
					<br>
					ข้อมูลที่ท่านให้ไว้ตอนสมัครสมาชิก หรือระบบอื่นๆของมูลนิธิครูดีของแผ่นดิน
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form" action="<?php echo $go2page; ?>" method="POST">
					<select name="prename" class="form-control" style="margin-bottom:10px; height:40px;" onchange="PickPrename(this.value);" required>
						<option value="" disabled="disabled" selected="selected">เลือกคำหน้านาม</option>
						<option value="A" <?php if($rowlogin['prename']=='A'){echo 'selected="selected" ';} ?> >นาย</option>
						<option value="B" <?php if($rowlogin['prename']=='B'){echo 'selected="selected" ';} ?> >นาง</option>
						<option value="C" <?php if($rowlogin['prename']=='C'){echo 'selected="selected" ';} ?> >นางสาว</option>
						<option value="D" <?php if($rowlogin['prename']=='D'){echo 'selected="selected" ';} ?> >ด.ช.</option>
						<option value="E" <?php if($rowlogin['prename']=='E'){echo 'selected="selected" ';} ?> >ด.ญ.</option>
						<option value="O" <?php if($rowlogin['prename']=='O'){echo 'selected="selected" ';} ?> >อื่นๆ</option>
					</select>
					<div id="pre_name_text" class="wrap-input100 validate-input" <?php if($rowlogin['prename']=='O'){echo 'style="display:block"';}else{echo 'style="display:none"';}?>>
						<input class="input100" type="text" id="pre_name_text_required" name="prename_text" class="form-control" 
						placeholder="<?php if($rowlogin['prename_remark'] != ''){echo $rowlogin['prename_remark'];}else{echo 'โปรดระบุคำหน้านาม"';}?>" max-lenght="30" />
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="firstname" class="form-control" placeholder="<?php echo $_SESSION['firstname'];?>" max-lenght="100" />
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="lastname" class="form-control" placeholder="<?php echo $_SESSION['lastname'];?>" max-lenght="100" />
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>					
					<!-- ################################################################################################ -->
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit" id="register">
							กดเพื่อยืนยัน
						</button>
					</div>
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>




<script>

	function PickPrename(val) {
		var elePrename = document.getElementById('pre_name');
		var elePrenameText = document.getElementById('pre_name_text');
		var elePrenameTextReq = document.getElementById('pre_name_text_required');
		if(val == 'O') {
			elePrenameText.style.display = 'block';
			elePrenameTextReq.required = true;
		}else{
			elePrenameText.style.display = 'none';
			elePrenameText.value = '';
			elePrenameTextReq.required = false;
		}
	}

</script>

</body>
</html>