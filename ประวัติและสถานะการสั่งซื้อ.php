<?php 
	session_start();
	require_once('condb.php');
	
  $sql_list_project  ="SELECT * FROM join_project";
  $list_project = $con->query($sql_list_project);


	$email = $_SESSION['email'];
	
	/* SUM all products price */
	$sqlcount="SELECT SUM(product_total) AS price_total FROM cart WHERE email='$email' ";
	$ressultcount=mysqli_query($con,$sqlcount);
	$rowcount=mysqli_fetch_array($ressultcount);
	$price_total=$rowcount['price_total'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>


<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
        <?php if (strpos($_SESSION['email'], '@') !== false) { //Check Account is signed in
                if ($_SESSION["level"]=="admin") { //Admin Session ?>
								<li style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
									<ul>
										<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION["level"]=="memberGeneral") { //General Member Session ?>
                <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> สมาชิกทั่วไป</li>
          <?php }elseif ($_SESSION["level"]=="memberSilver") { //Member Silver Session ?>
                <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</li>
          <?php }elseif ($_SESSION["level"]=="memberGold") { //Member Gold Session ?>
                <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</li>
          <?php }elseif ($_SESSION["level"]=="memberDiamond") { //Member Diamond Session ?>
                <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</li>
          <?php } ?>
                <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
          <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
                <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
        <?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
                  <ul>
                    <li><a href="ประวัติและสถานะการสั่งซื้อ-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
                    <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึก</a></li>
                    
                  </ul>
                </li>
                <li class="faicon-basket"><i class="fas fa-caret-down"></i><i class="fa fa-shopping-basket"></i> <?php echo number_format($price_total,2); ?>
									<ul>
										<li><a href="สนับสนุนของที่ระลึก.php" >ร้านค้า&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
										<li><a href="ตะกร้า.php">ดูของในตะกร้าและยืนยันคำสั่งซื้อ</a></li>
										
										<li><a href="ประวัติและสถานะการสั่งซื้อ.php">ประวัติและสถานะการสั่งซื้อ</a></li>
									</ul>
								</li>
        </ul>
      </nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <?php 
                while($row = $list_project->fetch_assoc()){
                  $id_join_project = $row['id_join_project'];
                  $title_menu = $row['title_menu']; 
              ?>
              <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
              <?php } ?>
              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">  
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="สนับสนุนของที่ระลึก.php">ร้านค้า</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ประวัติและสถานะการสั่งซื้อ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ประวัติและสถานะการสั่งซื้อ -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<div class="container">
			<!-- Start table of my product detail -->
			<div class="row">
				<div class="col-lg-12">
					<div class="table-main table-responsive">
						<!-- ################################################################################################ -->
						<table class="table">
							<thead>
								<tr>
									<th>รายการที่</th>
									<th>วัน</th>
									<th>เวลา</th>
									<th>ราคาสุทธิ</th>
									<th>สถานะ</th>
									<th>รายละเอียดคำสั่งซื้อ</th>
									<th>แจ้งชำระเงิน</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sqlmax = "SELECT DISTINCT MAX(order_group) AS max_order_group FROM orders WHERE email='$email' ";
									$resultmax = mysqli_query($con,$sqlmax);
									$rowmax = mysqli_fetch_array($resultmax);
									$max = $rowmax['max_order_group'];

									if($max == NULL) { ?>
										</tbody>
									</table>
										<div class="no-order">
											<p>ท่านไม่มีประวัติการสั่งซื้อ โปรดไปหน้าร้านค้ามูลนิธิ เพื่อดำเนินการสั่งซื้อ<br>ขอบคุณค่ะ</p>
											<a href="สนับสนุนของที่ระลึก.php">ไปหน้าร้านค้ามูลนิธิ</a>
										</div> <?php
									}else{
										$sql1 = "SELECT DISTINCT order_group FROM orders WHERE email='$email' ";
										$result1 = $con->query($sql1);
										
										$i=1;
										while($row1 = $result1->fetch_assoc()) {
											$order_group = $row1['order_group'];
											$sql2 = "SELECT * FROM orders WHERE email='$email' AND order_group='$order_group' ";
											$result2 = mysqli_query($con,$sql2);
											$row2 = mysqli_fetch_array($result2);
											/* Set shipping-cost value */
											if($row2['shipping_kind'] == 'normal') {
												$shipping_cost = '50';
											}elseif($row2['shipping_kind'] == 'ems') {
												$shipping_cost = '80';
											}elseif($row2['shipping_kind'] == 'Free') {
												$shipping_cost = '0';
											}else{
												echo 'error: others kind of shipping_kind, not normal or ems//'.$email.'//'.$order_group;
											}
											/* Set price-total value */
											$sql3 = "SELECT SUM(product_price_sale*product_amount) AS pricetotal FROM orders WHERE email='$email' AND order_group='$order_group' ";
											$result3 = mysqli_query($con,$sql3);
											$row3 = mysqli_fetch_array($result3);
											$price_total = $row3['pricetotal'];
											/* Set grand_total value */
											$grand_total = number_format($price_total+$shipping_cost,2); ?>
											<tr>
												<!-- No. รายการที่ -->
												<td class="price-pr">
													<p><?php echo $i; ?></p>
												</td>
												<!-- Date -->
												<td class="price-pr">
													<p><?php echo date("d-m-Y", strtotime($row2['order_date'])); ?></p>
												</td>
												<!-- Time -->
												<td class="price-pr">
													<p><?php echo date("H:i", strtotime($row2['order_date'])); ?></p>
												</td>
												<!-- Grand Total -->
												<td class="price-pr">
													<p><?php echo $grand_total; ?></p>
												</td>
												<!-- Status -->
												<td class="name-pr">
												<?php if($row2['order_status'] == 'สินค้าถึงปลายทางแล้ว') { ?>
																<p><i class="fas fa-check-circle"></i> <?php echo $row2['order_status']; ?></p>
												<?php }else{ ?>
																<p><?php echo $row2['order_status']; ?></p>
												<?php } ?>
												</td>
												<!-- Order Detail -->
												<td class="order-detail-btn">
													<a href="รายละเอียดคำสั่งซื้อ.php?order_group=<?php echo $row2['order_group'];?>">ดูรายละเอียด</a>
												</td>
												<!-- Slip Upload แจ้งการชำระเงิน -->
												<td class="slip-upload-btn">
													<?php if($row2['order_status']=='รอการชำระเงิน') { ?>
														<a href="ส่งหลักฐานการโอนA.php?order_group=<?php echo $row2['order_group'];?>&grand_total=<?php echo $grand_total;?>">ส่งหลักฐาน</a>
													<?php }else{ ?>
														<a href="#" onclick="return false" class="disable-hover">ส่งหลักฐานแล้ว</a>
													<?php } ?>
												</td>
												<!-- Cancel Order Button -->
												<td class="remove-pr">
													<?php if($row2['order_status']=='รอการชำระเงิน') { ?>
													<a href="delorders.php?order_group=<?php echo $row2['order_group'] ?>">
														<i class="fas fa-times"></i>
													</a>
													<?php }else{ ?>
														<i class="fas fa-check-circle" style="color:rgb(72,160,0);"></i>
													<?php } ?>
												</td>
											</tr> <?php
											$i++;
										} ?>
										</tbody>
										</table> <?php
									}
								?>
						<!-- ################################################################################################ -->
					</div>
				</div>				
			</div>
			<!-- End table of my product detail -->
			<!-- ################################################################################################ -->
		</div>
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ประวัติและสถานะการสั่งซื้อ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->


<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>






</body>
</html>