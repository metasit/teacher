<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$now_date = date("d-m-Y H:i:s");

	$ID = $_SESSION['ID']; // admin ID
	$ID_user = $_POST['ID_sup']; // supervisor ID

	/* Set all value */
	$winScroll = $_POST['winScroll'];
	$honor_score4sup_id = $_POST['honor_score4sup_id'];
	$reject_reason = $_POST['reject_reason'];

	$honor_score4sup_check_status = 'ปฏิเสธ,'.$reject_reason.','.$now_date;

	$sql = "UPDATE `honor_score4sup` SET `honor_score4sup_check_status`='$honor_score4sup_check_status' WHERE `honor_score4sup_id`='$honor_score4sup_id' ";
	$re = $con->query($sql) or die($con->error); //Check error


	/* Log Admin Action */
	$adscorelog_task = 'ตรวจรายงานการนิเทศ,'.$_POST['honor_score4sup_code'];
	
	$adscorelog_detail = 'ศน.,เกียรติคุณ,'.$honor_score4sup_check_status;
	$adscorelog_date = date("Y-m-d H:i:s");

	$sqllog = "INSERT INTO `adscorelog` (ID_ad, adscorelog_task, ID_user, adscorelog_detail, adscorelog_date) 
	VALUES ('$ID', '$adscorelog_task', '$ID_user', '$adscorelog_detail', '$adscorelog_date') ";
	$relog = $con->query($sqllog) or die($con->error); //Check error


	header('location: รายงานการนิเทศ-admin.php?winScroll='.$winScroll.'&ID_sup='.$_POST['ID_sup'].'&sup_name='.$_POST['sup_name'].'&sup_email='.$_POST['sup_email']);
?>