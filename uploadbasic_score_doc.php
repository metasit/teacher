<?php	
	session_start();
	include("condb.php");
	date_default_timezone_set("Asia/Bangkok");

	$email = $_SESSION['email'];
	$ID = $_SESSION['ID'];
	$basic_score_doc_date = date("Y-m-d");
	$scorelog_date = date("Y-m-d H:i:s");

	$target_dir = 'images/basic_score_doc/'.$basic_score_doc_date.'/'; // Set target_directory

	if(!is_dir($target_dir)) { // if there's not folder in target_directory
		mkdir($target_dir); // Create folder name is today_date
	}

	$target_file = $target_dir.basename($_FILES["basic_score_doc"]["name"]); // Save image in the target folder

	if(move_uploaded_file($_FILES["basic_score_doc"]["tmp_name"], $target_file)) {
		$sql = "UPDATE `login` SET `basic_score_status`='กำลังตรวจสอบ',`basic_score_doc`='$target_file',`basic_score_doc_date`='$basic_score_doc_date' WHERE email='$email' ";
		$res= $con->query($sql) or die($con->error);

		/* Log User Action */
		$sqllog="INSERT INTO `scorelog` VALUES (NULL,'$ID','แนบเอกสาร','ขั้นพื้นฐาน,หนังสือรับรอง',NULL,NULL,NULL,'$scorelog_date') ";
		$relog = $con->query($sqllog) or die($con->error); //Check error

		echo '<script>';
			echo "alert(\"แนบเอกสารเรียบร้อยแล้วค่ะ\");";
			echo "window.location.replace('โครงการครูดีของแผ่นดิน.php#secA')";
		echo '</script>';
	}/*else{
		echo '<script>';
			echo "alert(\"เกิดขอผิดพลาด<br>โปรดติดต่อผู้พัฒนาระบบ(สรณ์ปกรณ์ ศิรการบัณฑิตย์)ของมูลนิธิครูดีของแผ่นดิน<br>เพื่อดำเนินการแก้ไข<br>ขอบคุณครับ\");";
			echo "window.location.replace('โครงการครูดีของแผ่นดิน.php')";
		echo '</script>';
	}*/
?>