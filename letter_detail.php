<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
    $id = $_GET['id'];
    $sql_list  ="SELECT * FROM 	letter WHERE id_letter = $id ";
    $list = $con->query($sql_list);


    while($row = $list->fetch_assoc()){
        $name = $row['title'];
        $pdf = $row['pdf_name'];
        $nick_name_pdf = $row['nick_name_pdf'];
        $pdf_name = $row['pdf_name'];
                // var_dump($row);
    }
    // exit();
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="จดหมาย019-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->

  <?php 
    if(explode(".",$nick_name_pdf)[1] != "pdf"){
  
  ?>

<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="จดหมายรวมlatest.php">จดหมาย/ประกาศ</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> <?php  echo $name; ?></a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>

  <?php
  
    }
    else {

  ?>



  <?php 
    }
  ?>


<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - จดหมาย019 -->
 <?php 
 
 
 ?>
<div class="wrapper row3">
  <!-- AAAAA -->
  <main class="hoc container clear"> 
    <article style="text-align:center" >
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px;">
        <strong>
        <?php

       if(explode(".",$nick_name_pdf)[1] == "pdf"){  
          echo $name;
       }else{
        //  echo $pdf_name;
         ?>
          <a 
            id="dw"
            href="<?php  echo "/dev/content-setting/backend/mail_announcement/image/".$pdf_name; ?>"
            style="color:#b4931f"
            class="font-20 fotn-weight-bold"
          >
            Download File  
          </a>

    <?php
    }
      
      
      ?></strong>
      


      </p>
      <ul class="font-x2plus textlink" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
        <li>
          <?php  
          if(explode(".",$nick_name_pdf)[1] == "pdf") {
            ?>
            <iframe src="<?php echo "content-setting/backend/mail_announcement/image/".$pdf ?>" width="100%" height="1200px">
              <?php
              //  echo $name; 
              ?>        
            </iframe>
            <?php  } ?>
        </li>
      </ul>
    </article>
  </main>
</div>
<!-- End Content 01 - จดหมาย019 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->

  <script>
    
    let dw = document.getElementById('dw')
    dw.addEventListener('click',e =>{
      let  path = e.target.getAttribute('href')
      let url = "https://www.thaisuprateacher.org/"+path;
      console.log(url,'url');
    })
  </script>

<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>