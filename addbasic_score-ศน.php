<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");

	$email = $_SESSION['email'];
	$ID = $_SESSION['ID'];
	$basic_score_ans = $_POST['basic_score1'].','.$_POST['basic_score2'].','.$_POST['basic_score3'].','.$_POST['basic_score4'].','.$_POST['basic_score5'].','.$_POST['basic_score6'].','.$_POST['basic_score7'].','.$_POST['basic_score8'].','.$_POST['basic_score9'].','.$_POST['basic_score10'].','.$_POST['basic_score11'].','.$_POST['basic_score12'];
	$basic_score_text = $_POST['basic_score_text'];
	$basic_score_total = $_POST['basic_score1']+$_POST['basic_score2']+$_POST['basic_score3']+$_POST['basic_score4']+$_POST['basic_score5']+$_POST['basic_score6']+$_POST['basic_score7']+$_POST['basic_score8']+$_POST['basic_score9']+$_POST['basic_score10']+$_POST['basic_score11']+$_POST['basic_score12'];
	$basic_score_date = date("Y-m-d H:i:s");

	if($_POST['CFP'] == 10) {

		if($basic_score_total >= 23) {
			/* Add data to login Table */
			$sql="UPDATE `login` SET `basic_score_status`='รอแนบเอกสาร', `basic_score_total`='$basic_score_total', `basic_score_text`='$basic_score_text', 
			`basic_score_ans`='$basic_score_ans', `basic_score_date`='$basic_score_date' WHERE email='$email' ";
			$res= $con->query($sql) or die($con->error); //Check error

			/* Log User Action */
			$sqllog="INSERT INTO `scorelog` VALUES (NULL,'$ID','ทำประเมิน','ขั้นพื้นฐาน,ผ่าน','$basic_score_total','$basic_score_ans','$basic_score_text',NULL,NULL,'$basic_score_date') ";
			$relog = $con->query($sqllog) or die($con->error); //Check error

			header('location: popCongratbasic_score-ศน.php');

		}else{
			/* Add data to login Table */
			$sql="UPDATE `login` SET `basic_score_status`='คะแนนต่ำกว่าเกณฑ์',`basic_score_total`='$basic_score_total', `basic_score_text`='$basic_score_text', 
			`basic_score_ans`='$basic_score_ans', `basic_score_date`='$basic_score_date' WHERE email='$email' ";
			$res= $con->query($sql) or die($con->error); //Check error

			/* Log User Action */
			$sqllog="INSERT INTO `scorelog` VALUES (NULL,'$ID','ทำประเมิน','ขั้นพื้นฐาน,ต่ำกว่าเกณฑ์','$basic_score_total','$basic_score_ans','$basic_score_text',NULL,NULL,'$basic_score_date') ";
			$relog = $con->query($sqllog) or die($con->error); //Check error

			header('location: popFailbasic_score-ศน.php?basic_score_total='.$basic_score_total);
		}

	}else{
		echo "<script>window.history.go(-1)</script>"; //Protect policy: In the case of this page, not access by addbasic_score_btn
	}

?>