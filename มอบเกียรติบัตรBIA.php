<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		$affil_code = $_SESSION['affil_code'];
		$occup_code = $_SESSION['occup_code'];

		$sqlbia_cer = "SELECT * FROM `bia_cer` WHERE bia_cer_affil_code='$affil_code' && bia_cer_code='A' ";
		$rebia_cer = mysqli_query($con, $sqlbia_cer);

		if(mysqli_num_rows($rebia_cer) != 0) {
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			$rowbia_cer = mysqli_fetch_array($rebia_cer);
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* Set school name */
			$sqlaffil_name = "SELECT `affil_name` FROM `login` WHERE `ID`='$ID' ";
			$reaffil_name = mysqli_query($con, $sqlaffil_name);
			$rowaffil_name = mysqli_fetch_array($reaffil_name);

			$affil_name = $rowaffil_name['affil_name'];
			$school_name = 'โรงเรียน'.substr($affil_name, strrpos($affil_name, '*')+1);
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* ตอนนี้โรงเรียนนี้พิมพ์เกียรติบัตรประเภทครูไปแล้วกี่ใบ */
			$sqlbia_cerF = "SELECT * FROM `bia_cer` WHERE `bia_cer_affil_code`='$affil_code' && `bia_cer_code` LIKE 'F%' ";
			$reBCF = mysqli_query($con, $sqlbia_cerF);

			$count_tea_cer = mysqli_num_rows($reBCF); // ตอนนี้มีครู Print ไปแล้วกี่ใบ
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* หาจำนวนที่ครูโรงเรียนนี้สามารถ Print ได้มากที่สุดกี่ใบ */
			$sqlbia_cerB = "SELECT * FROM `bia_cer` WHERE `bia_cer_affil_code`='$affil_code' && `bia_cer_code`='B' ";
			$reBCB = mysqli_query($con, $sqlbia_cerB);
			$rowBCB = mysqli_fetch_array($reBCB);

			$max_tea_cer = ceil($rowBCB['bia_cer_data1']/20); // จำนวนครูสามารถพิมพ์เกียรติบัตรได้
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		}else{
			echo "<script>window.history.go(-1)</script>";
		}
		
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
/*
	$name = $_SESSION['firstname'].' '.$_SESSION['lastname'];
	//Set date
	$sqladscorelog = "SELECT * FROM `adscorelog` WHERE ID_user='$ID' AND adscorelog_task='Approve แล้ว' ORDER BY adscorelog_id DESC LIMIT 1 ";
	$readscorelog = mysqli_query($con,$sqladscorelog);
	$rowadscorelog = mysqli_fetch_array($readscorelog);
	$date = new DateTime($rowadscorelog['adscorelog_date']);
	$date = $date->format("Y-m-d"); // Set date format
*/
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="โครงการBIA.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าโครงการ BIA</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt inline m-t-20">
					<div class="col-md-6">
						<img src="images/เด็กดีของแผ่นดิน Logo02.jpg" alt="มูลนิธิครูดีของแผ่นดิน Logo02">
					</div>
					<div class="col-md-5">
						<img src="images/BIAlogo01.png" style="width:70%;" alt="มูลนิธิครูดีของแผ่นดิน BIAlogo01">
					</div>
				</div>
				<!-- ################################################################################################ -->
				<br>
				<span class="login100-form-title fs-30 lh-1-1">
					<?php
						if($rowbia_cer['bia_cer_data1'] == 'ผ่าน') { ?>
						
							<!-- ปุ่ม Print เกียรติบัตรสำหรับโรงเรียน -->
							<a href="printBIA_cer4sch.php?CFP=มอบเกียรติบัตรBIA" target="_blank" class="btn2">พิมพ์เกียรติบัตร <?php echo $school_name; ?></a>
							<br><br>

							<!-- ปุ่ม Print เกียรติบัตรสำหรับผู้บริหาร --> <?php
							if(substr($occup_code, 0, 4) == 'OcAB') { ?>

								<a href="printBIA_cer4exe.php?CFP=มอบเกียรติบัตรBIA" target="_blank" class="btn2">พิมพ์เกียรติบัตรผู้บริหาร <?php echo $school_name; ?></a> <?php

							}else{ ?>

								<a href="กรอกข้อมูลผู้บริหารเกียรติบัตรBIA.php?CFP=มอบเกียรติบัตรBIA" target="_blank" class="btn2">พิมพ์เกียรติบัตรผู้บริหาร <?php echo $school_name; ?></a> <?php

							} ?>
							<br><br>

							<!-- ปุ่ม Print เกียรติบัตรสำหรับครู --> <?php
							if(substr($occup_code, 0, 4) == 'OcAB') { ?>

								<div style="font-family: KanitLight; font-size:18px; display:inline;">
									ตอนนี้เกียรติบัตรประเภทครู <strong><?php echo $school_name; ?></strong> พิมพ์ไปแล้ว <?php echo $count_tea_cer.'/'.$max_tea_cer; ?> ใบ
								</div> <?php

							}elseif(substr($occup_code, 0, 4) == 'OcAA') { ?>

								<a href="printBIA_cer4tea.php?CFP=มอบเกียรติบัตรBIA" target="_blank" class="btn2">พิมพ์เกียรติบัตรครู</a>
								<div style="font-family: KanitLight; font-size:15px; display:inline;"> ตอนนี้พิมพ์ไปแล้ว <?php echo $count_tea_cer.'/'.$max_tea_cer; ?> ใบ</div> <?php

							}
						///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						}elseif($rowbia_cer['bia_cer_data1'] == 'ไม่ผ่าน') { ?>

							<!-- ปุ่ม Print เกียรติบัตรสำหรับครู --> <?php
							if(substr($occup_code, 0, 4) == 'OcAB') { ?>

								เนื่องจาก <?php echo $school_name; ?> ไม่ผ่านเกณฑ์ จะสามารถพิมพ์เกียรติบัตรประเภทครูได้เท่านั้น
								<br>
								(ปุ่มพิมพ์เกียรติบัตรประเภทครู จะแสดงต่อเมื่อตำแหน่งครูเข้ามาเท่านั้น)
								
								<div style="font-family: KanitLight; font-size:18px; display:inline;">
									ตอนนี้เกียรติบัตรประเภทครู <strong><?php echo $school_name; ?></strong> พิมพ์ไปแล้ว <?php echo $count_tea_cer.'/'.$max_tea_cer; ?> ใบ
								</div> <?php

							}elseif(substr($occup_code, 0, 4) == 'OcAA') { ?>

								<a href="printBIA_cer4tea.php?CFP=มอบเกียรติบัตรBIA" target="_blank" class="btn2">พิมพ์เกียรติบัตรครู</a>
								<div style="font-family: KanitLight; font-size:15px; display:inline;"> ตอนนี้พิมพ์ไปแล้ว <?php echo $count_tea_cer.'/'.$max_tea_cer; ?> ใบ</div> <?php

							}

						}
					?>
				</span>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>