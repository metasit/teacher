<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
  $sql_list_project  ="SELECT * FROM join_project";
  $list_project = $con->query($sql_list_project);

?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start NoAccount Session -->
<?php
      if (empty($_SESSION['email']))
      {
?>
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
          <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
          <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
          <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
            <ul>
              <li><a href="หอเกียรติยศรวมlatest-en.php">English</a></li>
              <!--<li><a href="#">Chinese</a></li>-->
            </ul>
          </li>
          <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
        </ul>
      </nav>
      <!-- End NoAccount Session -->
      <!-- ################################################################################################ -->
      <!-- Start Admin Session -->
<?php
      }
      elseif ($_SESSION['email'])
      {
        if ($_SESSION["level"]=="admin")
        {
?>
        <nav id="mainav2" class="PreMenu_fl_right">
          <ul class="PreMenufaico2">
            <li style="background-color:rgb(228,0,0);"><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="หอเกียรติยศรวมlatest-en.php">English</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Admin Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member Session -->
<?php
        if ($_SESSION["level"]=="memberGeneral")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> สมาชิกทั่วไป</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="หอเกียรติยศรวมlatest-en.php">English</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member SILVER Session -->
<?php
        if ($_SESSION["level"]=="memberSilver")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="หอเกียรติยศรวมlatest-en.php">English</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member SILVER Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member GOLD Session -->
<?php
        if ($_SESSION["level"]=="memberGold")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="หอเกียรติยศรวมlatest-en.php">English</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member GOLD Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member DIAMOND Session -->
<?php
        if ($_SESSION["level"]=="memberDiamond")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="หอเกียรติยศรวมlatest-en.php">English</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
      }
?>
      <!-- End Member DIAMOND Session -->
      <!-- ################################################################################################ -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px; ">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <?php 
                    while($row = $list_project->fetch_assoc()){
                      $id_join_project = $row['id_join_project'];
                      $title_menu = $row['title_menu']; 
                  ?>
                  <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
                  <?php } ?>
              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          </li>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="window.location.reload(true);">หอเกียรติยศ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - หอเกียรติยศรวม -->
<div class="wrapper bgded overlay coloured" style="background-color:rgba(189,255,159,0.829)">
  <div class="hoc container testimonials clear">
    <!-- ################################################################################################ -->
    <div style="background-color:rgb(0,6,27); padding:20px 20px 0; border-radius:10px;">
      <div class="group">
        <article class="one_third first">
          <a href="หอเกียรติยศ001.php">
            <img class="zoom108" src="images/Posterประธาน.jpg" alt="Posterประธาน">
            <h9 class="heading" style="color:white;">พลเอกเอกชัย ศรีวิลาศ</h9>
            <em style="color:white;">ประธานมูลนิธิครูดีของแผ่นดิน</em>
            <blockquote>“As the Future will catches you! เมื่ออนาคตไล่ล่าคุณ” 
              แนวโน้มการปฏิรูปการศึกษาไทยในศตวรรษที่ 21 ที่ต้องก้าวตามให้ทันพัฒนาการของเทคโนโลยี 3 อย่างที่กำลังครอบงำโลกใบนี้ และสิ่งที่น่าตกใจยิ่งไปกว่านั้น คือ เทคโนโลยีเหล่านี้สามารถส่งพลังมหาศาล
              ต่อการเปลี่ยนแปลงวิถีชีวิตของคนเราและลูกหลานเราอย่างคาดไม่ถึง หากวงการศึกษาไทยยังไม่ตื่นตัว ยากจะคาดเดาถึงผลกระทบที่จะตามมา</blockquote>
          </a>
        </article>
        <article class="one_third">
          <a href="หอเกียรติยศ007.php">
            <img class="zoom108" src="images/Posterอุดมการณ์ที่ดี.png" alt="Posterอุดมการณ์ที่ดี">
            <h9 class="heading" style="color:white;">ดร.วิระ แข็งกสิการ</h9>
            <em style="color:white;">ผู้ตรวจราชการกระทรวงศึกษาธิการ</em>
            <blockquote>ครูดีมันเป็นคีย์เวิร์ดของการที่จะส่งเสริมคนดีของแผ่นดินอยู่แล้ว วันนี้ท่านปลัดกระทรวงศึกษาธิการ ท่านประเสริฐ บุญเรือง มีความตั้งใจที่จะมาพบกับพวกเรา ถ้าท่านเสร็จภารกิจจะรีบกลับมาเยี่ยมชม
              กับพวกเรา ท่านก็เลยมอบหมายให้ผมมาทำหน้าที่แทน</blockquote>
          </a>
        </article>
        <article class="one_third">
          <a href="หอเกียรติยศ006.php">
            <img class="zoom108" src="images/Posterระบบการใช้.png" alt="Posterระบบการใช้">
            <h9 class="heading" style="color:white;">ดร.อโณทัย ไทยวรรณศรี</h9>
            <em style="color:white;">ผู้อำนวยการสำนักพัฒนานวัตกรรมการจัดการศึกษา สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน</em>
            <blockquote>สมาชิกครูดีของแผ่นดิน คือ เรื่องของการใช้อำนาจในโรงเรียน สมัยก่อนเรายังมองไม่เห็นระบบชัดเจน จึงเป็นการจัดการเรียนการสอนทั่วไป แต่เรื่องการใช้อำนาจในโรงเรียน 
              สถานการณ์นี้มีความจำเป็นอย่างยิ่งในปัจจุบันครับ </blockquote>
          </a>
        </article>
      </div>
      <div class="group">
        <article class="one_third first">
          <a href="หอเกียรติยศ005.php">
            <img class="zoom108" src="images/Posterปัจจุบันสังคม.png" alt="Posterปัจจุบันสังคม">
            <h9 class="heading" style="color:white;">ท่าน ช โชคชัยคำแหง</h9>
            <em style="color:white;">ผู้อำนวยการสถาบันพัฒนาบุคลากรท้องถิ่น กรมส่งเสริมการปกครองส่วนท้องถิ่น</em>
            <blockquote>ครูดีเป็นครูที่ไม่ทำเหมือนคนอื่นๆ ในระบบการศึกษา เท่าที่สังเกต คือ คนที่ทำไม่เหมือนชาวบ้านจะได้รับการคัดเลือกเป็นคนดี โรงเรียนที่ไม่บริหารแบบคนอื่นจะได้รับการคัดเลือกเป็นโรงเรียนดี</blockquote>
          </a>
        </article>
        <article class="one_third">
          <a href="หอเกียรติยศ004.php">
            <img class="zoom108" src="images/Posterหัวใจของการสร้าง.png" alt="Posterหัวใจของการสร้าง">
            <h9 class="heading" style="color:white;">ท่านตติยา ใจบุญ</h9>
            <em style="color:white;">ผู้อำนวยการศูนย์วิทยาศาสตร์เพื่อการศึกษา กศน.</em>
            <blockquote>กศน. คือ สำนักงานการศึกษานอกระบบ การศึกษานอกระบบที่มาเด็กมาลงทะเบียนขั้นพื้นฐานประมาณ 9 แสนคน การศึกษาตามอัธยาศัยเป็นการจัดการเรียนโดยหลักศูนย์วิทยาศาสตร์ทั่ว
              ประเทศ 20 แห่ง และห้องสมุดประชาชนทั่วประเทศ 900 แห่ง</blockquote>
          </a>
        </article>
        <article class="one_third">
          <a href="หอเกียรติยศ003.php">
            <img class="zoom108" src="images/Posterการที่เราจะสร้าง.png" alt="Posterการที่เราจะสร้าง">
            <h9 class="heading" style="color:white;">ดร.บูรพาทิศ พลอยสุวรรณ์</h9>
            <em style="color:white;">รองเลขาธิการคุรุสภา</em>
            <blockquote>ปัจจุบันปฏิบัติหน้าที่เป็นเลขาธิการคุรุสภา ท่านมีภารกิจสำคัญที่บอกให้ผมมา และได้บอกหัวข้อ วันนี้ทางครูดีของแผ่นดิน อยากให้คุยเสวนาเกี่ยวกับทิศทางการสร้างคนดี</blockquote>
          </a>
        </article>
      </div>
      <div class="group">
        <article class="one_third first">
          <a href="หอเกียรติยศ002.php">
            <img class="zoom108" src="images/Posterเด็กต้องสร้าง.png" alt="Posterเด็กต้องสร้าง">
            <h9 class="heading" style="color:white;">ดร.กวินทร์เกียรติ นนธ์พละ</h9>
            <em style="color:white;">รองเลขาธิการคณะกรรมการส่งเสริมการศึกษาเอกชน (กช.)</em>
            <blockquote>มุมมองของผมมองว่าการสร้างคนดีให้กับเยาวชน เราทำผิดวิธี เราไปมุ่งเน้นในเรื่องของท่องจำบทเรียน การทำแบบฝึกหัดที่ไม่ใช่ของจริง ในมุมมองของผม ผมคิดว่าเด็กต้องสร้างความดีด้วยการกระทำ</blockquote>
          </a>
        </article><!--
        <article class="one_third">
          <a href="หอเกียรติยศ013.php">
            <img class="zoom108" src="images/Posterหอนิสัยทิ้งขยะ.png" alt="Posterหอนิสัยทิ้งขยะ">
            <h9 class="heading" style="color:white;">นางกฤตยา จุลละมณฑล</h9>
            <em style="color:white;">โรงเรียนวัดหนามแดง สพป.ฉะเชิงเทรา เขต 1</em>
            <blockquote>นิสัยการทิ้งขยะไม่ถูกที่สามารถเปลี่ยนได้ โดยวิธีการค้นหา ข้อที่ตนเองควรแก้ไขและแก้ไขตนเองด้วยการอาสา เป็นผู้เก็บขยะในโรงเรียน อย่างต่อเนื่องเป็นเวลา 21 วัน</blockquote>
          </a>
        </article>
        <article class="one_third">
          <a href="หอเกียรติยศ012.php">
            <img class="zoom108" src="images/Posterหอกีฬา.png" alt="Posterหอกีฬา">
            <h9 class="heading" style="color:white;">นายพัทธกานต์ อู่ทองมาก</h9>
            <em style="color:white;">โรงเรียนบ้านแก่งกุลาสามัคคี สพป.พิษณุโลก เขต 2</em>
            <blockquote>ปัญหาเด็กติดเกม เป็นปัญหาของครูมากขึ้นในปัจจุบัน ผลกระทบที่เกิดกับเด็กมีเกิดขึ้นทั้งร่างกายและทางจิตใจ การเลือกหาวิธีเพื่อการแก้ไขปัญหา ก็ต้องเลือกให้ถูกต้องและตรงความต้องการของเด็ก</blockquote>
          </a>
        </article>-->
      </div>
    </div>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End Content 01 - หอเกียรติยศรวม -->
<!-- ################################################################################################ -->
<!-- Start Pagination --><!--
<div class="wrapper row3 overlay coloured" style="background-color:rgba(189,255,159,0.829)">
  <main class="hoc container clear">
    
    <div class="content">
      <nav class="pagination">
        <ul>
          <li class="current"><strong>3</strong></li>
          <li><a href="หอเกียรติยศรวม002.php">2</a></li>
          <li><a href="หอเกียรติยศรวม001.php">1</a></li>
          <li><a href="หอเกียรติยศรวม002.php">ถัดไป &raquo;</a></li>
        </ul>
      </nav>
    </div>
    
  </main>
</div>
<!-- End Pagination -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End Copyright Tab -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Back2Top Button -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Back2Top Button -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>