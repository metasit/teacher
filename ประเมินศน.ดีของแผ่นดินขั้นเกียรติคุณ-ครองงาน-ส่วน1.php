<?php 
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$ID = $_SESSION['ID'];

	$CFP = $_GET['CFP'];

	if(isset($ID) && $CFP == 'ส่วน0') {
		/* Check that this question did? */
		$sqlhonor_score4sup = "SELECT * FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code='A' ";
		$reHS4S = mysqli_query($con, $sqlhonor_score4sup);
		$rowHS4S = mysqli_fetch_array($reHS4S);

		/* Program calculate the last question was answer */
		$a = strrpos($rowHS4S['honor_score4sup_data1'], ',')+1; // Find the last position of , and add more 1 position to ignore this ,
		$b = strrpos($rowHS4S['honor_score4sup_data1'], '.'); // Find the last position of .
		$c = $b-$a; // Find lenght between them
		$lastquestion = substr($rowHS4S['honor_score4sup_data1'], $a, $c); // Get the last question that finish last time

		if($rowHS4S['honor_score4sup_data1'] != '') {
			if($lastquestion == 22) {

			}elseif($lastquestion == 25) {
				header('location: ประเมินศน.ดีของแผ่นดินขั้นเกียรติคุณ-ครองงาน-ส่วน2.php?CFP=ส่วน1');
			}elseif($lastquestion == 30) {
				header('location: ประเมินศน.ดีของแผ่นดินขั้นเกียรติคุณ-ครองงาน-ส่วน3.php?CFP=ส่วน2');
			}else{
				echo "<script>window.history.go(-1)</script>";
			}
		}else{
			echo "<script>window.history.go(-1)</script>";
		}
		
	}else{
		echo "<script>window.history.go(-1)</script>";
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="m-t-90"></div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ประเมินศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row">
			<div class="col-lg-12">
				<div class="table-main table-responsive hoc container">
					<!-- ################################################################################################ -->
					<div class="center">
						<h8 class="m-b-20">หมวด: ครองงาน</h8>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-6 col-md-6 col-lg-6 fs-20">
							<p>ข้อที่ / ทั้งหมด</p>
							<p class="bold">23-25 / 37</p>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6 right fs-20">
							<p>เหลือเวลา</p>
							<div>
								<p class="bold m-t-1" id="timer" onload="localStorage.getItem('currenttime')">.</p> วินาที
							</div>
						</div>
					</div>

					<form action="addhonor_score_A4work-ศน.php" method="GET">
						<table class="table m-t-20" style="background-color:rgb(240,240,240);">
							<thead>
								<tr>
									<th rowspan="2" style="width:5%;">ข้อ</th>
									<th rowspan="2" style="width:60%;">รายการประเมิน</th>
									<th colspan="5" style="width:35%;">ระดับคุณภาพ</th>
								</tr>
								<tr>
									<th>1</th>
									<th>2</th>
									<th>3</th>
									<th>4</th>
									<th>5</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<!-- No. 23 -->
									<td class="price-pr bold">
										<p>23</p>
									</td>
									<!-- รายการประเมิน -->
									<td colspan="7" class="name-pr bold" style="text-align: left;">
										มีความรู้ ทักษะเกี่ยวกับการพัฒนาวิชาชีพ การนิเทศการศึกษา
									</td>
								</tr>
								<tr>
									<!-- No. 23.1 -->
									<td class="price-pr">
										<p>23.1</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr" style="text-align: left;">
										มีความรู้ ทักษะ และความสามารถในการกำหนดกลยุทธ์ แผน กิจกรรมการนิเทศการพัฒนาหลักสูตรและการจัดการเรียนรู้
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore231" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore231" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore231" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore231" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore231" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 23.2 -->
									<td class="price-pr">
										<p>23.2</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr" style="text-align: left;">
										มีความรู้ ทักษะ และความสามารถในการกำหนดกลยุทธ์ แผน กิจกรรมการนิเทศการวิจัยทางการศึกษา
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore232" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore232" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore232" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore232" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore232" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 23.3 -->
									<td class="price-pr">
										<p>23.3</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr" style="text-align: left;">
										มีความรู้ ทักษะ และความสามารถในการกำหนดกลยุทธ์ แผน กิจกรรมการนิเทศนวัตกรรมและเทคโนโลยีสารสนเทศทางการศึกษา
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore233" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore233" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore233" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore233" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore233" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 23.4 -->
									<td class="price-pr">
										<p>23.4</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr" style="text-align: left;">
										มีความรู้ ทักษะ และความสามารถในการกำหนดกลยุทธ์ แผน กิจกรรมการนิเทศการประกันคุณภาพการศึกษา
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore234" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore234" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore234" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore234" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore234" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 23.5 -->
									<td class="price-pr">
										<p>23.5</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr" style="text-align: left;">
										มีความรู้ ทักษะ และความสามารถในการกำหนดกลยุทธ์ แผน กิจกรรมการนิเทศคุณธรรม จริยธรรมและจรรยาบรรณในวิชาชีพครู
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore235" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore235" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore235" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore235" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore235" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 23.6 -->
									<td class="price-pr">
										<p>23.6</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr" style="text-align: left;">
										มีความรู้ ทักษะเฉพาะทางในสาขาหรือวิชาที่จะนิเทศ เพื่อส่งเสริม สนับสนุนให้ผู้รับการนิเทศ เกิดการพัฒนาอย่างมีคุณภาพ เต็มศักยภาพ
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore236" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore236" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore236" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore236" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore236" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 23.7 -->
									<td class="price-pr">
										<p>23.7</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr" style="text-align: left;">
										มีความสามารถใช้การนิเทศการศึกษาในการพัฒนาผู้บริหารสถานศึกษาและครูบนพื้นฐานของข้อมูล ประสบการณ์ของผู้บริหาร ครู และบริบทของสถานศึกษา
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore237" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore237" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore237" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore237" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore237" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 23.8 -->
									<td class="price-pr">
										<p>23.8</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr" style="text-align: left;">
										มีความสามารถใช้การนิเทศการศึกษาในการสร้างผู้นำทางวิชาการในสถานศึกษา เพื่อช่วยพัฒนาครู ผู้เรียน และคุณภาพการศึกษา
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore238" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore238" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore238" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore238" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore238" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 24 -->
									<td class="price-pr bold">
										<p>24</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										มีความรู้ ทักษะเฉพาะทางในสาขา หรือวิชาชีพที่จะนิเทศ
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore24" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore24" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore24" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore24" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore24" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 25 -->
									<td class="price-pr bold">
										<p>25</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										มีความรู้ และศาสตร์อื่นๆ ที่เกี่ยวข้องเพื่อการนิเทศ
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore25" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore25" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore25" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore25" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore25" value="5" required></td>
								</tr>
							</tbody>
						</table>

						<input type="hidden" name="CFP" value="ส่วน1">
						<button type="submit" id="nextBtn" class="btnJoin" style="color:white; cursor:pointer; width:80%;"><h1>ส่วนถัดไป</h1></button>
					</form>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ประเมินศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script>
	// Set the timeout
	if(typeof localStorage.getItem('currenttime') !== 'undefined' && localStorage.getItem('currenttime') > 0) {
		if(localStorage.getItem('currenttime') < 450) {
			var currenttime = localStorage.getItem('currenttime');
		}
	}else{
		var currenttime = 451;
	}

	// Clear currenttime when ปุ่มข้อถัดไป has been selected
	var nextBtn = document.getElementById('nextBtn');
	nextBtn.onclick = function() {
		localStorage.clear('currenttime');
	}
	
	// Update the count down every 1 second
	setInterval(function() {
		if(currenttime > 0) {
			currenttime--;
		}

		localStorage.setItem('currenttime', currenttime);
		
		if(currenttime >= 0) {
			document.getElementById("timer").innerHTML = currenttime;
		}
			
		if(currenttime === 0) {
			window.location.replace('addhonor_score_A4work-ศน.php?CFP=ส่วน1&timeout=yes');
		}

	}, 1000);
</script>

<script>
	history.replaceState(null, null, 'ประเมินศน.ดีของแผ่นดินขั้นเกียรติคุณ-ครองงาน.php');
</script>

</body>
</html>