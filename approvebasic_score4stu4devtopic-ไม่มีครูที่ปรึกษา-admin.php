<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$now_date = date("d-m-Y H:i:s");

	$ID_stu = $_GET['ID_stu']; // student ID

	/* Set all value */
	$winScroll = $_GET['winScroll'];
	$basic_score4stu_system_id = 'C';
	$new_devsub = $_GET['new_devsub'];
	$basic_score_ans = substr($_GET['basic_score_ans'], 0, 4).$new_devsub;

	$basic_score4stu_file_date = date("Y-m-d H:i:s");
	$basic_score4stu_text = $new_devsub;
	$basic_score4stu_check_status = 'อนุมัติ,'.$now_date;

	/* Check ว่ามี basic_score4stu_id แล้วหรือไม่ ถ้ามีแล้ว ให้ Update แต่ถ้ายังให้ INSERT */
	$sqlbasic_score4stu_check_C = "SELECT * FROM `basic_score4stu` WHERE ID='$ID_stu' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
	$reBS4SCC = $con->query($sqlbasic_score4stu_check_C) or die($con->error); //Check error

	if(mysqli_num_rows($reBS4SCC) == 0) { // first time click to update devtopic
		$sql = "INSERT INTO `basic_score4stu` (ID, basic_score4stu_system_id, basic_score4stu_file_date, basic_score4stu_text, basic_score4stu_check_status) VALUES ('$ID_stu', '$basic_score4stu_system_id', '$basic_score4stu_file_date', '$basic_score4stu_text', '$basic_score4stu_check_status') ";
		$res = $con->query($sql) or die($con->error); //Check error
	}else{
		$sql = "UPDATE `basic_score4stu` SET `basic_score4stu_file_date`='$basic_score4stu_file_date' , `basic_score4stu_text`='$basic_score4stu_text', `basic_score4stu_check_status`='$basic_score4stu_check_status' WHERE ID='$ID_stu' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
		$res= $con->query($sql) or die($con->error); //Check error
	}

	$sql = "UPDATE `login` SET basic_score_ans='$basic_score_ans' WHERE ID='$ID_stu' ";
	$res = $con->query($sql) or die($con->error); //Check error

	/* Log Admin Action */
	$ID_ad = $_SESSION['ID']; // admin ID
	$adscorelog_task = 'ตรวจความดีเด็กพื้นฐาน,'.$basic_score4stu_system_id;
	$ID_user = $ID_stu; // ID of student who do Good
	$adscorelog_detail = $basic_score4stu_check_status;

	$sqllog="INSERT INTO `adscorelog` (ID_ad, adscorelog_task, ID_user, adscorelog_detail) 
	VALUES ('$ID_ad', '$adscorelog_task', '$ID_user', '$adscorelog_detail') ";
	$relog = $con->query($sqllog) or die($con->error); //Check error

	
	$system_id = $_GET['basic_score4stu_system_id']; // Set system_id to pass valueble to another page
	header('location: รายละเอียดนักเรียนทำความดี-พื้นฐาน-ไม่มีครูที่ปรึกษา-admin.php?winScroll='.$winScroll.'&ID_stu='.$ID_stu.'&system_id='.$system_id);
?>