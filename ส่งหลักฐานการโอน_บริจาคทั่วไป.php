<?php
	session_start();
	require_once('condb.php');
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>


<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="ส่งหลักฐานการโอน_บริจาคทั่วไป-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom4Aboutus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ส่งหลักฐานการบริจาค</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ส่งหลักฐานการโอน -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
	<!-- ################################################################################################ -->
	<!-- Start Slip Upload  -->
	<div class="cart-box-main">
    <div class="container">
      <div class="row">
				<!-- ################################################################################################ -->
				<!-- Start Left Content -->
				<div class="col-sm-6 col-lg-6 mb-3">
					<div class="slip-page">
						<div class="title-left">
							<h3>ช่องทางการบริจาค</h3>
						</div>
						<div class="center m-t-30 m-b-30">
							<p>
								<strong>ธนาคารกรุงไทย</strong>
								<br>
								<strong>ชื่อบัญชี:</strong> มูลนิธิครูดีของแผ่นดิน สาขาถนนวิสุทธิกษัตริย์
								<br>
								<strong>เลขที่บัญชี:</strong> 006 – 0 – 21576 – 3
							</p>
							<img src="images/QRCodeบริจาคทั่วไป+บำรุงสมาชิก.jpg" alt="QRCodeบริจาคทั่วไป+บำรุงสมาชิก">
						</div>
					</div>
				</div>
				<!-- End Left Content -->
				<!-- ################################################################################################ -->
				<!-- Start Right Content -->
				<div class="col-sm-6 col-lg-6 mb-3">
					<div class="col-md-12 col-lg-12">
						<div class="input-slip-upload">
							<div class="title-left">
								<h3>แจ้งการบริจาค</h3>
							</div>
							<!-- Start form input -->
							<form action="uploadslip_donate.php" method="POST" enctype="multipart/form-data">
								<div class="row m-t-20">
									<div class="col-md-6 mb-3">
										<label for="date_donate">วันที่ผู้บริจาค *</label>
										<input type="date" name="date_donate" class="form-control" placeholder="" min='2020-04-14' max='<?php echo date("Y-m-d") ?>' required/>
									</div>
									<div class="col-md-6 mb-3">
										<label for="time_donate">เวลา (โดยประมาณ) *</label>
										<input type="time" name="time_donate" class="form-control" placeholder="" required/>
									</div>
								</div>
								<div class="row m-t-20">
									<div class="col-md-6 mb-3">
										<label for="donate_topic">หัวข้อในการบริจาค</label>
										<select id="donate_topic" name="donate_topic" class="form-control" required>
											<option disabled="disabled" selected="selected" value="">โปรดเลือก</option>
											<option value="General">ทั่วไป</option>
											<option value="Teacher">โครงการครูดีของแผ่นดิน</option>
											<option value="Supervisor">โครงการศึกษานิเทศก์ดีของแผ่นดิน</option>
											<option value="Student">โครงการเด็กดีของแผ่นดิน</option>
											<option value="Train">โครงการอบรมศึกษาพัฒนา</option>
										</select>
									</div>
									<div class="col-md-6 mb-3">
										<label for="donate_slip_image">หลักฐานการโอน *</label>
										<input type="file" id="slip_image" name="slip_image" class="form-control" accept="image/*" required/>
									</div>
								</div>
								<div class="row m-t-20">
									<div class="col-md-6 mb-3">
										<label for="bill_tax_kind">นามในการออกใบเสร็จ *</label>
										<select id="bill_tax_kind" name="bill_tax_kind" class="form-control" onchange="PickBillIN(this.value);" required>
											<option disabled="disabled" selected="selected" value="">โปรดเลือก</option>
											<option value="Anonymous">ไม่ประสงค์ออกนาม</option>
											<option value="Personal">บุคคล</option>
											<option value="Corporation">นิติบุคคล บริษัท/ห้าง/ร้าน</option>
											<option value="Government">หน่วยงานราชการ</option>
										</select>
									</div>
									<div class="col-md-6 mb-3" style="display:none" id="ID-Personal">
										<label for="ID-Personal">เลขบัตรประชาชน *</label>
										<input type="text" pattern="[0-9]{13,13}" maxlength="13" title="โปรดใส่ตัวเลข13ตัว" name="bill_tax_id_Personal" class="form-control" id="ID-Personal-required" />
									</div>
									<div class="col-md-6 mb-3" style="display:none" id="ID-Corporation">
										<label for="ID-Corporation">เลขประจำตัวผู้เสียภาษี *</label>
										<input type="text" pattern="[0-9]{13,13}" maxlength="13" title="โปรดใส่ตัวเลข13ตัว" name="bill_tax_id_Corporation" class="form-control" id="ID-Corporation-required" />
									</div>
									<div class="col-md-6 mb-3" style="display:none" id="ID-Government">
										<label for="ID-Government">ชื่อหน่วยงานของท่าน *</label>
										<input type="text" title="โปรดใส่ชื่อหน่วยงานของท่าน" name="bill_tax_id_Government" class="form-control" id="ID-Government-required" />
									</div>
								</div>
								<?php
									if(!$_SESSION['ID']) { ?>

										<div class="row m-t-20">
											<div class="col-md-6 mb-3">
												<label for="donate_firstname">ชื่อผู้บริจาค *</label>
												<input type="text" title="โปรดใส่ชื่อของท่าน" name="donate_firstname" class="form-control" required />
											</div>
											<div class="col-md-6 mb-3">
												<label for="donate_lastname">นามสกุลผู้บริจาค *</label>
												<input type="text" title="โปรดใส่นามสกุลของท่าน" name="donate_lastname" class="form-control" required />
											</div>
										</div>
										<div class="row m-t-20">
											<div class="col-md-12 mb-3">
												<label for="donate_firstname">ที่อยู่ผู้บริจาค *</label>
												<input type="text" title="โปรดใส่ที่อยู่ของท่าน" name="donate_address" class="form-control" required />
											</div>
										</div> <?php

									}
								?>
								<div class="center m-t-30 m-b-30 fs-16">
									*กรุณาตรวจทานรายละเอียดให้ถูกต้องอีกครั้ง ก่อนยืนยันการบริจาค
								</div>
								<div class="shopping-box">
									<button type="submit" name="submit" class="ml-auto btn hvr-hover">แจ้งการบริจาค</button>
								</div>
							</form>
							<!-- End form input -->
						</div>
					</div>
      	</div>
				<!-- End Right Content -->
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
  <!-- End Slip Upload -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ส่งหลักฐานการโอน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->


<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<script src="js/pickBillType.js"></script>

</body>
</html>