<?php 
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	$sqllogin = "SELECT * FROM `login` WHERE ID='$ID' ";
	$relogin = mysqli_query($con,$sqllogin);
	$rowlogin = mysqli_fetch_array($relogin);

	if($rowlogin['basic_score_status'] == 'ยืนยันแล้ว') {
		date_default_timezone_set("Asia/Bangkok");
	}else{
		echo "<script>window.history.go(-1)</script>"; //Protect policy
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>


<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li><a href="all_project.php"> ร่วมโครงการ</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการเด็กดีของแผ่นดิน.php"> โครงการเด็กดีของแผ่นดิน</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">แนบไฟล์</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ส่งการบ้านเด็กขั้นพื้นฐาน -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
	<!-- ################################################################################################ -->
	<!-- Start Slip Upload  -->
	<div class="cart-box-main">
    <div class="container">
      <div class="row">
				<!-- ################################################################################################ -->
				<!-- Start Left Content -->
				<?php
					$system_id = substr($rowlogin['basic_score_ans'],0,1);
					$devmain_id = substr($rowlogin['basic_score_ans'],2,1);
					$devsub = substr($rowlogin['basic_score_ans'],4);

					if($system_id == 'A') {
						$system_name = 'ระบบต้นกล้าแห่งความดี';
					}elseif($system_id == 'B') {
						$system_name = 'ระบบทฤษฎี 21 วัน (แต่ในปีนี้เนื่องจากโควิด 19 จะลดเหลือ 14 ครั้ง)';
					}

					if($devmain_id == 'A') {
						$devmain_name = 'การรู้จัก/จัดการตนเอง';
					}elseif($devmain_id == 'B') {
						$devmain_name = 'การเข้าใจผู้อื่น';
					}elseif($devmain_id == 'C') {
						$devmain_name = 'การเข้าสังคม';
					}elseif($devmain_id == 'D') {
						$devmain_name = 'การสื่อสารและการสร้างแรงบันดาลใจ';
					}

					/* Let set DateTime format before check more than 30 days can do basic_score again */
					$last_basic_score_date = new DateTime($rowlogin['basic_score_date']);
					$expire_basic_score_date = new DateTime($rowlogin['basic_score_date']);
					$datetoday = new DateTime(date('Y-m-d'));
					$last_basic_score_date->settime(0,0); // No need time to check
					$expire_basic_score_date->settime(0,0); // No need time to check
					date_add($expire_basic_score_date, date_interval_create_from_date_string('30 days'));
					$diff = date_diff($datetoday, $expire_basic_score_date); // Find interval date between last did basic_score and today basic_score
					$expire_basic_score_date = date_format($expire_basic_score_date, 'd-m-Y');
					$check_basic_score_date = $diff->format('%a');

					$sqlcountrow = "SELECT count(*) AS countrow FROM `basic_score4stu` WHERE ID='$ID' ORDER BY basic_score4stu_file_date ASC ";
					$recountrow = mysqli_query($con,$sqlcountrow);
					$rowcountrow = mysqli_fetch_array($recountrow);
				?>
				<div class="col-sm-5 col-lg-5 mb-3">
					<div class="col-md-12 col-lg-12">
						<div class="title-left">
							<h3>รายละเอียดสิ่งที่ต้องทำเพื่อได้รับรางวัล</h3>
						</div>
						<!-- ################################################################################################ -->
						<p class="m-t-10 text-black"><strong>วันที่เริ่มสมัครโครงการ:</strong> <?php echo date("d-m-Y", strtotime($rowlogin['basic_score_date'])); ?></p>
						<p class="m-t-10 text-black"><strong>วันสุดท้ายที่สามารถส่งผลการพัฒนาตนเอง:</strong> <?php echo $expire_basic_score_date.' (เหลือ '.$check_basic_score_date; ?> วัน)</p>
						<p class="m-t-10 text-black bold">สิ่งที่ต้องการพัฒนาตนเอง</p>
						<p class="m-t-10 text-black">หัวข้อหลัก: <?php echo $devmain_name; ?></p>
						<p class="m-t-10 text-black">หัวข้อย่อย: <?php echo $devsub; ?></p>
						<p class="m-t-10 text-black"><strong>ระบบที่ใช้ส่งผลงาน:</strong> <?php echo $system_name; ?></p>
						<p class="m-t-10 text-black"><strong>จำนวนผลการพัฒนาตนเองที่ส่งแล้ว:</strong> <?php echo $rowcountrow['countrow']; ?>/<?php if($system_id=='A'){echo '1';}else{echo '14';} ?> ครั้ง</label>
						<?php
							if($system_id == 'B') { ?>
								<p>* กรุณาอ่านและทำความเข้าใจอย่างละเอียด ก่อนกดอัพโหลดไฟล์</p>
								<p>
									กฎเกณฑ์ในการส่งผลงานหรือการอัพโหลดไฟล์
									<br>
									1. อนุญาตให้เข้าหน้านี้ ได้เพียงวันละ 1 ครั้งเท่านั้น
									<br>
									2. สามารถแนบภาพ/ข้อความ/วีดีโอสั้นๆได้
									<br>
									3. การอัพโหลดไฟล์ต้องระบุทั้งวันและเวลาที่ได้ทำความดี
									<br>
									4. ความดีที่ทำนั้นทำในวันใด วันที่ทำต้องไม่ซ้ำกัน
								</p> <?php
							}
						?>
					</div>
				</div>
				<!-- End Left Content -->
				<!-- ################################################################################################ -->
				<!-- Start Right Content -->
				<div class="col-sm-7 col-lg-7 mb-3">
					<div class="col-md-12 col-lg-12">
						<div class="input-slip-upload">
							<div class="title-left">
								<h3>แนบไฟล์ เพื่อประกอบการพิจารณารางวัล</h3>
							</div>
							<!-- Start form input -->
							<?php
							if($system_id == 'A') {
								$basic_score4stu_file_no = $rowcountrow['countrow']+1;
								
								$sqlbasic_score4stu = "SELECT * FROM `basic_score4stu` WHERE ID='$ID' AND basic_score4stu_system_id='A' ";
								$reBS4S = mysqli_query($con,$sqlbasic_score4stu);
								$rowBS4S = mysqli_fetch_array($reBS4S); 

								$basic_score4stu_file_date = $rowBS4S['basic_score4stu_file_date'];
								$createDate = new DateTime($basic_score4stu_file_date);
								$date = $createDate->format('Y-m-d');
								$time = $createDate->format('h:i:s'); ?>
								<form action="uploadHW-เด็ก.php" method="POST" enctype="multipart/form-data">
									<div class="row m-t-20">
										<div class="col-md-6 mb-3">
											<label for="basic_score4stu_file_date">วันและเวลาที่ทำความดี </label>
											<input type="datetime-local" name="basic_score4stu_file_date" class="form-control" value="<?php echo $date.'T',$time; ?>" min="2020-07-01T00:00:00" max="<?php echo date('Y-m-d').'T'.date('h:i:s'); ?>" required/>
										</div>
										<div class="col-md-6 mb-3">
											<label for="basic_score4stu_file">แนบหลักฐานการทำความดี </label>
											<input type="file" id="basic_score4stu_file" name="basic_score4stu_file" class="form-control" required/>
										</div>
										<div class="col-md-6 mb-3">
											<label for="basic_score4stu_text">รายงานการทำความดี </label>
											<textarea class="col-md-12" rows="1" name="basic_score4stu_text"><?php echo $rowBS4S['basic_score4stu_text']; ?></textarea>
										</div>
										<div class="col-md-6 mb-3">
											<label for="basic_score4stu_text" style="color:transparent">.</label>
											<?php
												if($rowBS4S['basic_score4stu_file'] != '') { ?>
													<button class="col-md-12 stu-file-btn" onclick="window.location.href='<?php echo $rowBS4S['basic_score4stu_file']; ?>'">ดูไฟล์แนบ</button> <?php
												}else{ ?>
													<button class="col-md-12 BG-silver" style="border:1px solid rgb(100,100,100); border-radius:50px;" onclick="return false">ดูไฟล์แนบ</button> <?php
												}
											?>
										</div>
									</div>
									<div class="row m-t-12">
										<div class="col-md-12">
											<button type="submit" class="ml-auto btn hvr-hover col-md-12" style="border-color:transparent; border-radius:50px; padding:1px 0;"
												onmousemove="this.style.cursor='pointer'">
												แนบไฟล์
											</button>
										</div>
									</div>
									<input type="hidden" name="basic_score4stu_file_no" value="<?php echo $basic_score4stu_file_no; ?>">
									<input type="hidden" name="basic_score4stu_system_id" value="<?php echo $system_id; ?>">
								</form> <?php



							}elseif($system_id == 'B') {
								$sqlbasic_score4stu = "SELECT * FROM `basic_score4stu` WHERE ID='$ID' ORDER BY basic_score4stu_file_date ASC ";
								$reBS4S = mysqli_query($con,$sqlbasic_score4stu);

								$i=1;
								while($rowBS4S = mysqli_fetch_array($reBS4S)) {
									$basic_score4stu_file_date = $rowBS4S['basic_score4stu_file_date'];
									$createDate = new DateTime($basic_score4stu_file_date);
									$date = $createDate->format('Y-m-d');
									$time = $createDate->format('h:i:s'); ?>
									<form action="uploadHW-เด็ก.php" method="POST" enctype="multipart/form-data">
										<div class="row m-t-20">
											<div class="col-md-6 mb-3">
												<label for="basic_score4stu_file_date">วันและเวลาที่ทำความดี <?php echo $i; ?></label>
												<input type="datetime-local" name="basic_score4stu_file_date" class="form-control" value="<?php echo $date.'T',$time; ?>" min="2020-07-01T00:00:00" max="<?php echo date('Y-m-d').'T'.date('h:i:s'); ?>" required/>
											</div>
											<div class="col-md-6 mb-3">
												<label for="basic_score4stu_file">แนบหลักฐานการทำความดี <?php echo $i; ?></label>
												<input type="file" id="basic_score4stu_file" name="basic_score4stu_file" class="form-control" required/>
											</div>
											<div class="col-md-6 mb-3">
												<label for="basic_score4stu_text">รายงานการทำความดี <?php echo $i; ?></label>
												<textarea class="col-md-12" rows="1" name="basic_score4stu_text"><?php echo $rowBS4S['basic_score4stu_text']; ?></textarea>
											</div>
											<div class="col-md-6 mb-3">
												<label for="basic_score4stu_text" style="color:transparent">.</label>
												<button class="col-md-12 stu-file-btn" 
													onclick="window.location.href='<?php echo $rowBS4S['basic_score4stu_file']; ?>'">
													ดูไฟล์แนบ <?php echo $i; ?>
												</button>
											</div>
										</div>
										<div class="row m-t-12">
											<div class="col-md-12">
												<button type="submit" class="col-md-12 stu-up-btn"
													onmousemove="this.style.cursor='pointer'">
													แนบไฟล์ <?php echo $i; ?> (ถ้าทำการแนบไฟล์ซ่ำ จะทำให้ไฟล์เก่าหาย)
												</button>
											</div>
										</div>
										<input type="hidden" name="basic_score4stu_file_no" value="<?php echo $rowBS4S['basic_score4stu_file_no']; ?>">
										<input type="hidden" name="basic_score4stu_system_id" value="<?php echo $system_id; ?>">
									</form> <?php
									$i++;
								}
									


									if($rowcountrow['countrow'] < 14) {
										$basic_score4stu_file_no = $rowcountrow['countrow']+1; ?>
									
										<form action="uploadHW-เด็ก.php" method="POST" enctype="multipart/form-data">
											<div class="row m-t-20">
												<div class="col-md-6 mb-3">
													<label for="basic_score4stu_file_date">วันและเวลาที่ทำความดี *</label>
													<input type="datetime-local" name="basic_score4stu_file_date" class="form-control" min="2020-07-01T00:00:00" max="<?php echo date('Y-m-d').'T'.date('h:i:s'); ?>" required/>
												</div>
												<div class="col-md-6 mb-3">
													<label for="basic_score4stu_file">แนบหลักฐานการทำความดี *</label>
													<input type="file" id="basic_score4stu_file" name="basic_score4stu_file" class="form-control" required/>
												</div>
												<div class="col-md-6 mb-3">
													<label for="basic_score4stu_text">รายงานการทำความดี *</label>
													<textarea class="col-md-12" rows="1" name="basic_score4stu_text"></textarea>
												</div>
												<div class="col-md-6 mb-3">
													<label for="basic_score4stu_text" style="color:transparent">.</label>
													<button class="col-md-12 BG-silver" style="border:1px solid rgb(100,100,100); border-radius:50px;" onclick="return false">ดูไฟล์แนบ</button>
												</div>
											</div>
											<div class="row m-t-12">
												<div class="col-md-12">
													<button type="submit" class="ml-auto btn hvr-hover col-md-12" style="border-color:transparent; border-radius:50px; padding:1px 0;"
														onmousemove="this.style.cursor='pointer'">
														แนบไฟล์ใหม่
													</button>
												</div>
											</div>
											<input type="hidden" name="basic_score4stu_file_no" value="<?php echo $basic_score4stu_file_no; ?>">
											<input type="hidden" name="basic_score4stu_system_id" value="<?php echo $system_id; ?>">
										</form> <?php
									}
								 } ?>
								<!-- ################################################################################################ -->
							<!-- End form input -->
						</div>
					</div>
      	</div>
				<!-- End Right Content -->
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
  <!-- End Slip Upload -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ส่งการบ้านเด็กขั้นพื้นฐาน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyrigth -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyrigth -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

</body>
</html>