<?php 
	session_start();
	if($_SESSION['level'] == 'admin') {
		require_once('condb.php');

		$sql_list_project  ="SELECT * FROM join_project";
		$list_project = $con->query($sql_list_project);
  
		$email = $_SESSION['email'];
		$ID = $_SESSION['ID'];
	}else{
		header("Location: javascript:history.go(-1);");
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['win_scroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
        <?php if (strpos($_SESSION['email'], '@') !== false) { //Check Account is signed in
                if ($_SESSION["level"]=="admin") { //Admin Session ?>
								<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
									<ul>
										<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
										<li><a href="ระบบหลังบ้านตรวจหนังสือรับรอง.php">ตรวจหนังสือรับรอง</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION["level"]=="memberGeneral") { //General Member Session ?>
                <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> สมาชิกทั่วไป</li>
          <?php }elseif ($_SESSION["level"]=="memberSilver") { //Member Silver Session ?>
                <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</li>
          <?php }elseif ($_SESSION["level"]=="memberGold") { //Member Gold Session ?>
                <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</li>
          <?php }elseif ($_SESSION["level"]=="memberDiamond") { //Member Diamond Session ?>
                <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</li>
          <?php } ?>
                <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
          <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
                <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
        <?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
                  <ul>
                    <li><a href="index-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค.php">เกี่ยวกับการบริจาค</a></li>
                    <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึก</a></li>
                  </ul>
                </li>
        </ul>
      </nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
			  	<?php 
				  	if($_SESSION['level'] == 'admin')
                    while($row = $list_project->fetch_assoc()){
                      $id_join_project = $row['id_join_project'];
                      $title_menu = $row['title_menu']; 
                  ?>
                  <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
                <?php } ?>
              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear" style="padding: 0 0 15px;">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ระบบหลังบ้านตรวจหนังสือรับรอง</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ระบบหลังบ้านตรวจหนังสือรับรอง -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row" style="margin:0 20px; overflow-x:auto;">
			<div class="col-lg-12">
				<div class="table-main table-responsive" style="margin-bottom:200px">
					<!-- ################################################################################################ -->
					<table class="table" style="background-color:rgb(240,240,240);">
						<thead>
							<tr>
								<th>ลำดับ</th>
								<th>ชื่อ</th>
								<th>วันที่ทำแบบประเมิน</th>
								<th>เวลา</th>
								<th>สถานะ</th>
								<th>หนังสือรับรอง</th>
								<th>อัพเดท</th>
								<th>Print</th>
								<th>หมายเหตุ</th>
							</tr>
						</thead>
						<tbody class="disable-margin">
							<?php
							/* Call login table data from mySQL */
							$sql = "SELECT * FROM `login` WHERE basic_score_total IS NOT NULL AND ID!='$ID' ORDER BY basic_score_date DESC ";
							$re = mysqli_query($con,$sql);

							$maxrow = mysqli_num_rows($re); // Get number of row
							while($row = $re->fetch_assoc()) {
							?>
								<tr>
									<!-- No. -->
									<td class="price-pr">
										<p><?php echo $maxrow; ?></p>
									</td>
									<!-- Name -->
									<?php if($row['level'] == 'admin') { ?>
													<td class="name-pr lh-1-0" style="color:white; background-color:rgb(228,0,0);">
									<?php }elseif($row['level'] == 'memberGeneral') { ?>
													<td class="name-pr lh-1-0" style="color:black; background-color:white;">
									<?php }elseif($row['level'] == 'memberSilver') { ?>
													<td class="name-pr lh-1-0" style="color:white; background-color:rgb(169,169,169);">
									<?php }elseif($row['level'] == 'memberGold') { ?>
													<td class="name-pr lh-1-0" style="color:white; background-color:rgb(180,147,31);">
									<?php }elseif($row['level'] == 'memberDiamond') { ?>
													<td class="name-pr lh-1-0" style="color:white; background-color:rgb(52,52,53);">
									<?php } ?>
										<p><?php echo $row['firstname'].'<br>'.$row["lastname"]; ?></p>
									</td>
									<!-- basic_score Date -->
									<td class="price-pr">
										<p><?php echo date("d-m-Y", strtotime($row['basic_score_date'])); ?></p>
									</td>
									<!-- basic_score Time -->
									<td class="price-pr">
										<p><?php echo date("H:i", strtotime($row['basic_score_date'])); ?></p>
									</td>
									<!-- Status -->
									<td class="name-pr">
										<?php if($row['level'] == 'admin' || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') { // For high level member
														if($row['basic_score_status'] == 'ถึงปลายทางแล้ว') { ?>
															<p><i class="fas fa-check-circle"></i><br>ถึงปลายทางแล้ว</p>
											<?php }else{ ?>
														<p><?php echo $row['basic_score_status']; ?></p>
										<?php 	}
													}else{ // For low level member
														if($row['basic_score_status'] == 'Approve แล้ว') { ?>
															<p><i class="fas fa-check-circle"></i><br>Approve แล้ว</p>
											<?php }else{ ?>
														<p><?php echo $row['basic_score_status']; ?></p>
										<?php 	}
													} ?>
									</td>
									<!-- basic_score_doc Upload -->
									<td class="slip-upload-btn">
										<?php if($row['basic_score_status'] == 'รอแนบเอกสาร' || $row['basic_score_status'] == 'ปฏิเสธหนังสือรับรอง') { ?>
														<a href="#" style="background-color:transparent; color:black; font-weight:700; border: none;" onclick="return false">รอแนบเอกสาร</a>
										<?php }else{ ?>
														<a href="<?php echo $row['basic_score_doc'];?>" target="_blank">ดูหนังสือรับรอง</a>
										<?php	} ?>
									</td>
									<!-- Update Status Button -->
									<?php
										/* Let set DateTime format before check more than 30 days can do basic_score again */
										$last_basic_score_date = new DateTime($row['basic_score_date']);
										$today_basic_score_date = new DateTime('tomorrow');
										$last_basic_score_date = $last_basic_score_date->settime(0,0); // No need time to check
										$today_basic_score_date  = $today_basic_score_date->settime(0,0); // No need time to check
										$diff = date_diff($last_basic_score_date,$today_basic_score_date); // Find interval date between last did basic_score and today basic_score
										$check_basic_score_date = $diff->format("%a");
									?>
									<td>
										<div class="up-stat-sty">
											<button class="up-stat-btn"><i class="fas fa-caret-down"></i></button>
											<div class="dropdown">
												<?php if($row['basic_score_status'] == 'กำลังตรวจสอบ') { ?>
																<a onclick="getWinScroll_changeStatusBtn_updatebasic_score_status('ปฏิเสธหนังสือรับรอง','<?php echo $row['ID'];?>','<?php echo $row['basic_score_doc'];?>')">ปฏิเสธหนังสือรับรอง</a>
																<a onclick="getWinScroll_changeStatusBtn_updatebasic_score_status('Approve แล้ว','<?php echo $row['ID'];?>','<?php echo $row['basic_score_doc'];?>')">Approve</a>
													<?php if($row['level'] == 'admin' || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') { // For high level member ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ส่งเกียรติบัตรพิเศษแล้ว</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ถึงปลายทางแล้ว</a>
													<?php	}

															}elseif($row['basic_score_status'] == 'ปฏิเสธหนังสือรับรอง'){ ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ปฏิเสธหนังสือรับรอง</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">Approve</a>
													<?php if($row['level'] == 'admin' || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') { // For high level member ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ส่งเกียรติบัตรพิเศษแล้ว</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ถึงปลายทางแล้ว</a>
													<?php	}

															}elseif($row['basic_score_status'] == 'Approve แล้ว'){ ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ปฏิเสธหนังสือรับรอง</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">Approve</a>
													<?php if($row['level'] == 'admin' || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') { // For high level member ?>
																<a onclick="getWinScroll_changeStatusBtn_updatebasic_score_status('ส่งเกียรติบัตรพิเศษแล้ว','<?php echo $row['ID'];?>','<?php echo $row['basic_score_doc'];?>')">ส่งเกียรติบัตรพิเศษแล้ว</a>
																<a onclick="getWinScroll_changeStatusBtn_updatebasic_score_status('ถึงปลายทางแล้ว','<?php echo $row['ID'];?>','<?php echo $row['basic_score_doc'];?>')">ถึงปลายทางแล้ว</a>
													<?php	}

															}elseif($row['basic_score_status'] == 'คะแนนต่ำกว่าเกณฑ์') { ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">รอคุณครูทำแบบประเมิน เหลือ <?php echo $check_basic_score_date; ?> วัน</a>

												<?php	}elseif($row['basic_score_status'] == 'ส่งเกียรติบัตรพิเศษแล้ว'){ ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ปฏิเสธหนังสือรับรอง</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">Approve</a>
													<?php if($row['level'] == 'admin' || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') { // For high level member ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ส่งเกียรติบัตรพิเศษแล้ว</a>
																<a onclick="getWinScroll_changeStatusBtn_updatebasic_score_status('ถึงปลายทางแล้ว','<?php echo $row['ID'];?>','<?php echo $row['basic_score_doc'];?>')">ถึงปลายทางแล้ว</a>
													<?php	}
												
															}elseif($row['basic_score_status'] == 'ถึงปลายทางแล้ว'){ ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ปฏิเสธหนังสือรับรอง</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">Approve</a>
													<?php if($row['level'] == 'admin' || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') { // For high level member ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ส่งเกียรติบัตรพิเศษแล้ว</a>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">ถึงปลายทางแล้ว</a>
													<?php	}

															}else{ ?>
																<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">รอแนบเอกสาร</a>
												<?php	} ?>
											</div>
										</div>
									</td>
									<!-- Printing -->
									<td class="print-btn">
										<?php if($row['level'] == 'admin' || $row['level'] == 'memberSilver' || $row['level'] == 'memberGold' || $row['level'] == 'memberDiamond') {
														if($row['basic_score_status'] == 'Approve แล้ว' || $row['basic_score_status'] == 'ส่งเกียรติบัตรพิเศษแล้ว' || $row['basic_score_status'] == 'ถึงปลายทางแล้ว') { ?>
															<a href="spe cer-ครู.php?name=<?php echo $_SESSION['firstname'].' '.$_SESSION['lastname'];?>&basic_score_code"
															target="_blank" class="tooltip4"><i class="fas fa-print"><span class="tooltiptext4">พิมพ์เกียรติบัตรพิเศษ</span></i></a>
											<?php	}else{ ?>
															<a href="#" onclick="return false" style="background-color:rgb(206,206,206); pointer-events:none;"><i class="fas fa-print"></i></a>
											<?php }
													}else{ ?>
														<a href="#" onclick="return false" style="background-color:rgb(206,206,206); pointer-events:none;"><i class="fas fa-print"></i></a>
											<?php	} ?>
									</td>
									<!-- Remark -->
									<form action="addbasic_score_remark.php" method="POST">
										<td class="order_remark-sty">
											<textarea rows="1" cols="15" name="basic_score_remark"><?php echo $row['basic_score_remark']; ?></textarea>
											<div class="order-detail-btn">
												<input type="hidden" name="ID" value=<?php echo $row['ID']; ?>>
												<input class="order_remark-submit" type="submit" value="บันทึก">
											</div>
										</td>
									</form>
								</tr>
								<?php $maxrow--; }  ?>
								</tbody>
								</table>

					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ระบบหลังบ้านตรวจหนังสือรับรอง -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_half first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        <a class="font-x1 footercontact" href="http://www.thaisuprateacherdonate.org/">
          <img src="images/มูลนิธิครูดีของแผ่นดิน inwshop Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>ของที่ระลึกมูลนิธิ</p>
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<!-- JS for getWinScroll -->
<script src="js/getWinScroll_changeStatusBtn.js"></script>

</body>
</html>