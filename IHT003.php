<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
  $sql_list_project  ="SELECT * FROM join_project";
  $list_project = $con->query($sql_list_project);
?>

<!DOCTYPE html>

<html lang="thai">
<!-- To declare your language - read more here: https://www.w3.org/International/questions/qa-html-language-declarations -->

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
        <?php if (strpos($_SESSION['email'], '@') !== false) { //Check Account is signed in
                if ($_SESSION["level"]=="admin") { //Admin Session ?>
								<li style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
									<ul>
										<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION["level"]=="memberGeneral") { //General Member Session ?>
                <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> สมาชิกทั่วไป</li>
          <?php }elseif ($_SESSION["level"]=="memberSilver") { //Member Silver Session ?>
                <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</li>
          <?php }elseif ($_SESSION["level"]=="memberGold") { //Member Gold Session ?>
                <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</li>
          <?php }elseif ($_SESSION["level"]=="memberDiamond") { //Member Diamond Session ?>
                <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</li>
          <?php } ?>
                <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
          <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
                <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
        <?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
                  <ul>
                    <li><a href="index-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
                    <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึก</a></li>
                    
                  </ul>
                </li>
        </ul>
      </nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li class="active"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
							<li class="active"><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา<i class="fas fa-caret-right" style="margin-left:90px"></i></a>
								<ul style="position:absolute; left:305px;">
									<li><a href="PublicTraining.php">Public Training</a></li>
									<li class="active"><a href="InhouseTraining.php">Inhouse Training</a></li>
								</ul>
							</li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->

              <?php 
                while($row = $list_project->fetch_assoc()){
                  $id_join_project = $row['id_join_project'];
                  $title_menu = $row['title_menu']; 
                
              ?>
              <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
              <?php } ?>

            </ul>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="all_project.php">ร่วมโครงการ</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li class="linkst"><a href="โครงการอบรมสัมมนา.php">โครงการอบรมสัมมนา</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li class="linkst"><a href="InhouseTraining.php">Inhouse Training</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">การสร้างเครือข่ายครูดีของแผ่นดิน</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - PBT003 -->
<div class="wrapper row3">
  <main class="hoc container clear">
		<!-- Start Content -->
    <article style="text-align:center; border-bottom:3px solid #59A209; padding-bottom:50px;">
			<!-- Start Heading -->
			<p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px;"><strong>การสร้างเครือข่ายครูดีของแผ่นดิน</strong></p>
			<!-- End Heading -->
			<!-- ################################################################################################ -->
			<!-- Start Poster -->
			<img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1/IHT003img11.jpg" alt="อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1 IHT003img11">
			<br><br>
			<img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1/IHT003img07.jpg" alt="อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1 IHT003img07">
			<br><br>
			<!-- End Poster -->
			<!-- ################################################################################################ -->
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>กำหนดการประชุมเชิงปฏิบัติการการสร้างเครือข่ายครูดีของแผ่นดิน</strong>
				<div class="table-main table-responsive">
					<table class="table6" style="background-color:rgb(240,240,240);">
						<thead>
							<tr>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>07.00 - 08.30 น.</td>
								<td class="detail">รายงานตัว</td>
							</tr>
							<tr>
								<td>08.30 - 09.00 น.</td>
								<td class="detail">พิธีเปิดและบรรยายพิเศษ</td>
							</tr>
							<tr>
								<td>09.00 - 10.20 น.</td>
								<td class="detail"><strong>ระเบียบการโครงการเครือข่ายครูดีของแผ่นดิน เจริญรอยตามเบื้องพระยุคลบาท</strong></td>
							</tr>
							<tr>
								<td>10.20 - 10.30 น.</td>
								<td class="detail">พักรับประทานอาหารว่างและเครื่องดื่ม</td>
							</tr>
							<tr>
								<td>10.30 - 12.00 น.</td>
								<td class="detail"><strong>การพัฒนาตนเอง ๓ หมวด ๑๙ ตัวบ่งชี้ ครองตน ครองคน ครองงาน</strong></td>
							</tr>
							<tr>
								<td>12.00 - 13.00 น.</td>
								<td class="detail">พักรับประทานอาหารกลางวัน</td>
							</tr>
							<tr>
								<td>13.00 - 14.30 น.</td>
								<td class="detail"><strong>หลักการสร้างเครือข่ายแบบปลาดาว </strong></td>
							</tr>
							<tr>
								<td>14.30 - 14.45 น.</td>
								<td class="detail">พักรับประทานอาหารว่างและเครื่องดื่ม</td>
							</tr>
							<tr>
								<td>14.45 - 16.30 น.</td>
								<td class="detail"><strong>ขั้นตอนการสมัครเข้าร่วมโครงการด้วยระบบคอมพิวเตอร์การประเมินรางวัลครูดีของแผ่นดิน ขั้นพื้นฐานและชั้นที่ ๕</strong></td>
							</tr>
						</tbody>
					</table>
					<p>
						<strong>หมายเหตุ</strong>กำหนดการอาจเปลี่ยนแปลงได้ตามความเหมาะสม
					</p>
				</div>
			</p>
			<ul class="fs-32 textlink" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
				<li><a href="docs/ร่วมโครงการ/อบรมสัมมนา/Inhouse Training/หลักสูตร3 การสร้างเครือข่ายครูดีของแผ่นดิน/กำหนดการ การสร้างเครือข่ายครูดีของแผ่นดิน.pdf">ดาวน์โหลดรายละเอียดหลักสูตร</a></li>
			</ul>
		</article>
		<!-- End Content -->
		<!-- ################################################################################################ -->
		<!-- Start Gallery -->
    <p class="font-x3"><span style="color:rgb(180,147,31); line-height:30px;"><strong>คลังภาพ</strong></span></p>
    <div id="gallery" style="margin-top:50px">
      <ul class="nospace clear">
        <li class="one_third first zoom11"><a href="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1/IHT003img01.jpg"><img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1/IHT003img01.jpg" alt="อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1 IHT003img01"></a></li>
        <li class="one_third zoom11"><a href="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1/IHT003img02.jpg"><img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1/IHT003img02.jpg" alt="อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1 IHT003img02"></a></li>
        <li class="one_third zoom11"><a href="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1/IHT003img06.jpg"><img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1/IHT003img06.jpg" alt="อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1 IHT003img06"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1/IHT003img10.jpg"><img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1/IHT003img10.jpg" alt="อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1 IHT003img10"></a></li>
        <li class="one_third zoom11"><a href="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1/IHT003img12.jpg"><img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1/IHT003img12.jpg" alt="อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1 IHT003img12"></a></li>
				<li class="one_third zoom11"><a href="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1/IHT003img13.jpg"><img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1/IHT003img13.jpg" alt="อบรม ผอ.โรงเรียน ณ สพป.น่าน เขต 1 IHT003img13"></a></li>
			</ul>
			<ul class="nospace clear">
        <li class="one_third first zoom11"><a href="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1/IHT003img01.jpg"><img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1/IHT003img01.jpg" alt="อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1 IHT003img01"></a></li>
        <li class="one_third zoom11"><a href="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1/IHT003img02.jpg"><img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1/IHT003img02.jpg" alt="อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1 IHT003img02"></a></li>
        <li class="one_third zoom11"><a href="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1/IHT003img03.jpg"><img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1/IHT003img03.jpg" alt="อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1 IHT003img03"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1/IHT003img04.jpg"><img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1/IHT003img04.jpg" alt="อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1 IHT003img04"></a></li>
        <li class="one_third zoom11"><a href="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1/IHT003img05.jpg"><img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1/IHT003img05.jpg" alt="อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1 IHT003img05"></a></li>
				<li class="one_third zoom11"><a href="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1/IHT003img06.jpg"><img src="images/Inhouse Training/IHT003/อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1/IHT003img06.jpg" alt="อบรม ผอ.โรงเรียน สพป.ฉะเชิงเทรา เขต 1 IHT003img06"></a></li>
			</ul>
		</div>
		<!-- End Gallery -->
  </main>
</div>
<!-- End Content 00 - PBT003 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- JS for slider -->
<script src="js/slider.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>