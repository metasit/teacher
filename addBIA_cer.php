<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$bia_cer_affil_code = $_SESSION['affil_code'];
		// เช็คว่า ผ่านเงื่อนไขการได้รับเกียรติบัตร หรือไม่
		/* เงื่อนไขการรับเกียรติบัตร
				สถานศึกษาที่มีนักเรียนผ่านหลักสูตรไม่น้อยกว่า 120 คน หรือ
				มากกว่าร้อยละ 70 ของนักเรียนชั้นประถมศึกษาปีที่ 3 ขึ้นไป
				-เกียรติบัตรสำหรับสถานศึกษา ๑
				-เกียรติบัตรสำหรับผู้บริหาร ๑ คน
				-เกียรติบัตรสำหรับครู อัตราส่วนครู ๑ คน ต่อนักเรียน ๒๐ คน (เศษน้อยกว่า ๒๐ ปัดขึ้น)
		*/
		$stu_num_all = $_POST['stu_num_all'];
		$stu_num_p3plus = $_POST['stu_num_p3+'];
		$stu_num_BIA = $_POST['stu_num_BIA'];
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Set bia_cer_code & bia_cer_data1
		/*
			Code A: status ผ่านหรือไม่ผ่าน
			Code B: จำนวนนักเรียนทั้งโรงเรียน
			Code C: จำนวนนักเรียนตั้งแต่ ป.3 ขึ้นไป
			Code D: จำนวนนักเรียนที่เข้าร่วม BIA
			Code E: คำตอบ Questionnaire 3 ข้อ
			Code F: Directory เกียรติบัตรครูทั้งหมด (ไม่ได้ Add ลง Database ในหน้านี้ ไป Add ที่หน้า มอบเกียรติบัตรBIA.php & printBIA_cer4tea)
			Code G: Directory เกียรติบัตรโรงเรียน (ไม่ได้ Add ลง Database ในหน้านี้ ไป Add ที่หน้า มอบเกียรติบัตรBIA.php & printBIA_cer4sch)
			Code H: Directory เกียรติบัตรผู้บริหารโรงเรียน (ไม่ได้ Add ลง Database ในหน้านี้ ไป Add ที่หน้า มอบเกียรติบัตรBIA.php & printBIA_cer4exe)
			Code I: Running Number (ดสป.630000001=โรงเรียน, ดสป.630100001=ผู้บริหาร, ดสป.6302000001=ครู)
		*/

		if($stu_num_BIA > 120) {

			$bia_cer_code_A = 'A';
			$bia_cer_data1_A = 'ผ่าน';

		}else{

			$percent70ofstu_num_p3plus = $stu_num_p3plus*0.7;

			if($stu_num_BIA > $percent70ofstu_num_p3plus) {

				$bia_cer_code_A = 'A';
				$bia_cer_data1_A = 'ผ่าน';

			}else{

				$bia_cer_code_A = 'A';
				$bia_cer_data1_A = 'ไม่ผ่าน';

			}

		}


		$bia_cer_code_B = 'B';
		$bia_cer_data1_B = $stu_num_all;

		$bia_cer_code_C = 'C';
		$bia_cer_data1_C = $stu_num_p3plus;

		$bia_cer_code_D = 'D';
		$bia_cer_data1_D = $stu_num_BIA;

		$bia_cer_code_E = 'E';
		$bia_cer_data1_E = '1.'.$_POST['BIA_question1'].',2.'.$_POST['BIA_question2'].',3.'.$_POST['BIA_question3'].' ,CT.'.$_POST['BIA_confirm_true'];

		$bia_cer_remark = 'IDคนทำ:'.$ID;
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/* Save data to table `bia_cer` in database */
		$sql = "INSERT INTO `bia_cer` (`bia_cer_affil_code`, `bia_cer_code`, `bia_cer_data1`, `bia_cer_remark`) 
		VALUES ('$bia_cer_affil_code', '$bia_cer_code_A', '$bia_cer_data1_A', '$bia_cer_remark'), 
		('$bia_cer_affil_code', '$bia_cer_code_B', '$bia_cer_data1_B', '$bia_cer_remark'), 
		('$bia_cer_affil_code', '$bia_cer_code_C', '$bia_cer_data1_C', '$bia_cer_remark'), 
		('$bia_cer_affil_code', '$bia_cer_code_D', '$bia_cer_data1_D', '$bia_cer_remark'), 
		('$bia_cer_affil_code', '$bia_cer_code_E', '$bia_cer_data1_E', '$bia_cer_remark') ";
		$re = $con->query($sql) or die($con->error); //Check error

		header('location: มอบเกียรติบัตรBIA.php');
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>