<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		date_default_timezone_set("Asia/Bangkok");

		$ID_sup = $_GET['ID_sup'];
		$sup_name = $_GET['sup_name'];
		$sup_email = $_GET['sup_email'];

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['winScroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการครูดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="ระบบหลังบ้านศน.ขั้นเกียรติคุณ.php">ระบบหลังบ้านศึกษานิเทศก์ดีขั้นเกียรติคุณ</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">การเปลี่ยนแปลงเชิงประจักษ์</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Parent Content -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Child Content -->
	<div class="cart-box-main">
		<div style="max-width: 80%; margin: 30px auto 130px;">
			<!-- Start Child2 Content -->
			<div class="row">
				<div class="col-lg-12">
					<div class="table-main table-responsive">
						<!-- ################################################################################################ -->
						<?php
							$sqlhonor_score4sup_codeD = "SELECT * FROM `honor_score4sup` WHERE ID='$ID_sup' AND honor_score4sup_code='D' ";
							$reHS4SCD = mysqli_query($con, $sqlhonor_score4sup_codeD);
							$rowHS4SCD = mysqli_fetch_array($reHS4SCD);

							$honor_score4sup_data1 = $rowHS4SCD['honor_score4sup_data1'];
							if($honor_score4sup_data1 == 'most') {
								$honor_score4sup_part4_1_answer = 'มากที่สุด';
							}elseif($honor_score4sup_data1 == 'much') {
								$honor_score4sup_part4_1_answer = 'มาก';
							}elseif($honor_score4sup_data1 == 'neutral') {
								$honor_score4sup_part4_1_answer = 'ปานกลาง';
							}elseif($honor_score4sup_data1 == 'less') {
								$honor_score4sup_part4_1_answer = 'น้อย';
							}elseif($honor_score4sup_data1 == 'least') {
								$honor_score4sup_part4_1_answer = 'น้อยที่สุด';
							}

							$honor_score4sup_part4_2_answer = $rowHS4SCD['honor_score4sup_data2'];
							$honor_score4sup_part4_3_answer = $rowHS4SCD['honor_score4sup_data3'];
							$honor_score4sup_part4_4_answer = $rowHS4SCD['honor_score4sup_data4'];
						?>
						<!-- ################################################################################################ -->
						<h2 class="center m-b-50">แบบประเมินคุณสมบัติพื้นฐานของครูดีของแผ่นดิน</h2>
						<form action="addbasic_score-ครู.php" method="POST">
							<table class="table" style="background-color:rgb(240,240,240);">
								<thead>
									<tr>
										<th rowspan="2">ที่</th>
										<th rowspan="2">ประเด็น</th>
										<th colspan="5">คะแนนที่ได้</th>
									</tr>
									<tr>
										<th style="width:10%;">5</th>
										<th style="width:10%;">4</th>
										<th style="width:10%;">3</th>
										<th style="width:10%;">2</th>
										<th style="width:10%;">1</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<!-- No. 1 -->
										<td class="price-pr">
											<p>1</p>
										</td>
										<!-- ประเด็นพิจารณา -->
										<td class="name-pr">
											กริยามารยาท สุภาพ อ่อนน้อม
										</td>
										<!-- ระดับการปฏิบัติ -->
										<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
											<input type="radio" id="always" class="basic_score_sty" name="basic_score1" value="3" style="width:25%;" required>
											<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score1" value="2" style="width:25%" required>
											<input type="radio" id="seldom" class="basic_score_sty" name="basic_score1" value="1" style="width:25%" required>
											<input type="radio" id="never" class="basic_score_sty" name="basic_score1" value="0" style="width:25%" required>
										</td>
									</tr>
									<tr>
										<!-- No. 2 -->
										<td class="price-pr">
											<p>2</p>
										</td>
										<!-- ประเด็นพิจารณา -->
										<td class="name-pr">
											แต่งกายทันสมัยตามแฟชั่น
										</td>
										<!-- ระดับการปฏิบัติ -->
										<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
											<input type="radio" id="always" class="basic_score_sty" name="basic_score2" value="0" style="width:25%;" required>
											<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score2" value="1" style="width:25%" required>
											<input type="radio" id="seldom" class="basic_score_sty" name="basic_score2" value="2" style="width:25%" required>
											<input type="radio" id="never" class="basic_score_sty" name="basic_score2" value="3" style="width:25%" required>
										</td>
									</tr>
									<tr>
										<!-- No. 3 -->
										<td class="price-pr">
											<p>3</p>
										</td>
										<!-- ประเด็นพิจารณา -->
										<td class="name-pr">
											ไม่พูดจาหยาบคาย แม้เวลาโกรธ
										</td>
										<!-- ระดับการปฏิบัติ -->
										<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
											<input type="radio" id="always" class="basic_score_sty" name="basic_score3" value="3" style="width:25%;" required>
											<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score3" value="2" style="width:25%" required>
											<input type="radio" id="seldom" class="basic_score_sty" name="basic_score3" value="1" style="width:25%" required>
											<input type="radio" id="never" class="basic_score_sty" name="basic_score3" value="0" style="width:25%" required>
										</td>
									</tr>
									<tr>
										<!-- No. 4 -->
										<td class="price-pr">
											<p>4</p>
										</td>
										<!-- ประเด็นพิจารณา -->
										<td class="name-pr">
											ใช้ความรุนแรงในการแก้ปัญหา
										</td>
										<!-- ระดับการปฏิบัติ -->
										<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
											<input type="radio" id="always" class="basic_score_sty" name="basic_score4" value="0" style="width:25%;" required>
											<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score4" value="1" style="width:25%" required>
											<input type="radio" id="seldom" class="basic_score_sty" name="basic_score4" value="2" style="width:25%" required>
											<input type="radio" id="never" class="basic_score_sty" name="basic_score4" value="3" style="width:25%" required>
										</td>
									</tr>
									<tr>
										<!-- No. 5 -->
										<td class="price-pr">
											<p>5</p>
										</td>
										<!-- ประเด็นพิจารณา -->
										<td class="name-pr">
											เล่นหวยใต้ดิน-การพนัน
										</td>
										<!-- ระดับการปฏิบัติ -->
										<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
											<input type="radio" id="always" class="basic_score_sty" name="basic_score5" value="0" style="width:25%;" required>
											<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score5" value="1" style="width:25%" required>
											<input type="radio" id="seldom" class="basic_score_sty" name="basic_score5" value="2" style="width:25%" required>
											<input type="radio" id="never" class="basic_score_sty" name="basic_score5" value="3" style="width:25%" required>
										</td>
									</tr>
								</tbody>
							</table>
							<p class="fs-20 lh-2-0">
								<strong>ความดีที่ภาคภูมิใจที่สุดในชีวิต</strong> * มีตัวอักษรได้ไม่เกิน1000ตัว
								<input type="text" name="basic_score_text" style="width:100%" maxlength="1000" title="สามารถมีตัวอักษรไม่เกิน1000ตัว" required>
								<br>
								<strong>เกณฑ์การได้รับรางวัลครูดีของแผ่นดิน ขั้นพื้นฐาน</strong>
								<br>
								ต้องได้คะแนนรวมไม่น้อยกว่า ร้อยละ 75 คือ ได้ 23 คะแนนจาก 30 คะแนน
								<br>
								และมีเอกสารแนบจากผู้บังคับบัญชาว่า ไม่อยู่ในระหว่างการลงโทษทางวินัยหรือตั้งกรรมการสอบสวนวินัย
							</p>
							<input type="hidden" name="CFP" value="10">
							<button type="submit" class="btnJoin" style="color:white; cursor:pointer; width:80%;"><h1>ส่งคะแนนประเมิน</h1></button>
						</form>
						<!-- ################################################################################################ -->
					</div>
				</div>
			</div>
			<!-- End Child2 Content -->
		</div>
	</div>
	<!-- End Child Content -->
	<!-- ################################################################################################ -->
</div>
<!-- End Parent Content -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

</body>
</html>