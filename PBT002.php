<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
	$sql_list_project  ="SELECT * FROM join_project";
    $list_project = $con->query($sql_list_project);
?>

<!DOCTYPE html>

<html lang="thai">
<!-- To declare your language - read more here: https://www.w3.org/International/questions/qa-html-language-declarations -->

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
        <?php if (strpos($_SESSION['email'], '@') !== false) { //Check Account is signed in
                if ($_SESSION["level"]=="admin") { //Admin Session ?>
								<li style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
									<ul>
										<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION["level"]=="memberGeneral") { //General Member Session ?>
                <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> สมาชิกทั่วไป</li>
          <?php }elseif ($_SESSION["level"]=="memberSilver") { //Member Silver Session ?>
                <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</li>
          <?php }elseif ($_SESSION["level"]=="memberGold") { //Member Gold Session ?>
                <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</li>
          <?php }elseif ($_SESSION["level"]=="memberDiamond") { //Member Diamond Session ?>
                <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</li>
          <?php } ?>
                <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
          <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
                <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
        <?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
                  <ul>
                    <li><a href="index-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
                    <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึก</a></li>
                    
                  </ul>
                </li>
        </ul>
      </nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li class="active"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
			  <?php 
                while($row = $list_project->fetch_assoc()){
                  $id_join_project = $row['id_join_project'];
                  $title_menu = $row['title_menu']; 
                
              ?>
              <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
              <?php } ?> 
			 
			  <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
							<li class="active"><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา<i class="fas fa-caret-right" style="margin-left:90px"></i></a>
								<ul style="position:absolute; left:305px;">
									<li class="active"><a href="PublicTraining.php">Public Training</a></li>
									<li><a href="InhouseTraining.php">Inhouse Training</a></li>
								</ul>
							</li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="all_project.php">ร่วมโครงการ</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li class="linkst"><a href="โครงการอบรมสัมมนา.php">โครงการอบรมสัมมนา</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li class="linkst"><a href="PublicTraining.php">Public Training</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ศาสตร์พระราชา สำหรับครูดีภาษาไทยในศตวรรษที่ 21</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - PBT003 -->
<div class="wrapper row3">
  <main class="hoc container clear">
		<!-- Start Content -->
    <article style="text-align:center; border-bottom:3px solid #59A209; padding-bottom:50px;">
			<p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px;"><strong>ศาสตร์พระราชา สำหรับครูดีภาษาไทยในศตวรรษที่ 21</strong></p>
			<!-- Start Poster -->
			<div class="slider-container">
				<a href="#" onclick="return false"><img class="imgSlides" src="images/Public Training/PBT002/610805 อบรม ณ จังหวัดอุดรธานี/PBT002img22.jpg" alt="610805 อบรม ณ จังหวัดอุดรธานี PBT002img22" style="width:100%"></a>
				<a href="#" onclick="return false"><img class="imgSlides" src="images/Public Training/PBT002/610812 อบรม ณ จังหวัดลพบุรี/PBT002img04.jpg" alt="610812 อบรม ณ จังหวัดลพบุรี PBT002img04" style="width:100%"></a>
				<a href="#" onclick="return false"><img class="imgSlides" src="images/Public Training/PBT002/610812 อบรม ณ จังหวัดลพบุรี/PBT002img08.jpg" alt="610812 อบรม ณ จังหวัดลพบุรี PBT002img08" style="width:100%"></a>
				<a href="#" onclick="return false"><img class="imgSlides" src="images/Public Training/PBT002/610701 อบรม ณ จังหวัดนครนายก/PBT002img03.jpg" alt="610701 อบรม ณ จังหวัดนครนายก PBT002img03" style="width:100%"></a>
				<a href="#" onclick="return false"><img class="imgSlides" src="images/Public Training/PBT002/610701 อบรม ณ จังหวัดนครนายก/PBT002img02.jpg" alt="610812 อบรม ณ จังหวัดลพบุรี PBT002img02" style="width:100%"></a>
				<a href="#" onclick="return false"><img class="imgSlides" src="images/Public Training/PBT002/610701 อบรม ณ จังหวัดนครนายก/PBT002img04.jpg" alt="610812 อบรม ณ จังหวัดลพบุรี PBT002img04" style="width:100%"></a>
				<a href="#" onclick="return false"><img class="imgSlides" src="images/Public Training/PBT002/610722 อบรม ณ จังหวัด บุรีรัมย์/PBT002img13.jpg" alt="610722 อบรม ณ จังหวัด บุรีรัมย์ PBT002img13" style="width:100%"></a>
				<div class="navi-container">
					<div class="prev fl_left" onclick="plusDivs(-1)">&#10094;</div>
					<div class="next fl_right" onclick="plusDivs(1)">&#10095;</div>
				</div>
			</div>
			<!-- End Poster -->
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>สาระเนื้อหา (Content) :</strong><br>
				กลุ่มสาระการเรียนรู้ภาษาไทย : การอ่านและการเขียน
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>สาระที่เป็นศาสตร์ที่เกี่ยวข้องการจัดการเรียนรู้ (Pedagogy) :</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. การสอนในศตวรรษที่ 21 ได้แก่ การเรียนรู้แบบมีส่วนร่วมและฝึกปฏิบัติ (Active learning) ศาสตร์พระราชา (The King's Philosophy)
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. การวิจัยพัฒนาผลการเรียนการสอน/ชุมชนแห่งการเรียนรู้ทางวิชาชีพ ได้แก่ ชุมชนแห่งการเรียนรู้มืออาชีพ (PLC : Professional Learning Community)
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. การใช้สื่อและเทคโนโลยีในการจัดการเรียนรู้ ได้แก่ การใช้สื่อสังคมออนไลน์ เช่น เฟซบุ๊ก ในการนำเสนอผลงานและกิจกรรม
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>พื้นฐานและเงื่อนไขของครูที่จะรับการพัฒนาตามหลักสูตร :</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. สำเร็จการศึกษาระดับปริญญาตรีขึ้นไป หรือเทียบเท่า ในสาขาวิชาเอกภาษาไทย ประถมศึกษา หรือ
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. สำเร็จการศึกษาระดับปริญญาตรีขึ้นไป หรือเทียบเท่า และมีความถนัดในวิชาภาษาไทย
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. นับถือศาสนาพุทธ คริสต์ อิสลาม หรือศาสนาและลัทธิความเชื่ออื่นที่ไม่ขัดต่อความสงบเรียบร้อยและศีลธรรมอันดีงามของประชาชน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4. มีความรู้และทักษะการใช้งานคอมพิวเตอร์และอินเทอร์เน็ต หรือสมาร์ทโฟน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;5. สามารถเข้ารับการพัฒนาได้ตลอดหลักสูตร
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>คำสำคัญ (Keyword) :</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. Active Learning ภาษาไทย ประถมศึกษา
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. ศาสตร์พระราชา ภาษาไทย ประถมศึกษา
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. PLC ภาษาไทย ประถมศึกษา
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4. การเรียนรู้ในศตวรรษที่ 21 ภาษาไทย ประถมศึกษา
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>หลักการและที่มาของหลักสูตร :</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้จัดทำหลักสูตรมีความมุ่งมั่นในการยกระดับมาตรฐานวิชาชีพครูกลุ่มสาระการเรียนรู้ภาษาไทยระดับชั้นประถมศึกษาตามแนวทางศาสตร์พระราชาด้วย
				หลักปรัชญาของเศรษฐกิจพอเพียง และการเรียนรู้ในศตวรรษที่ 21 อย่างเป็นรูปธรรม โดยการน้อมนำพระบรมราโชวาทที่ทรงให้ไว้แก่วงการการศึกษาไทยมาปฏิบัติ ทั้งการครองตน ครองคน และครองงาน 
				ผ่านเทคโนโลยีสมัยใหม่ และเครือข่ายการเรียนรู้ร่วมกัน หรือชุมชนแห่งการเรียนรู้มืออาชีพ (PLC : Professional Learning Community) เพื่อให้ครูมีศีลธรรม มีจิตวิญญาณความเป็นครู ปฏิบัติตนตามจรรยาบรรณ
				วิชาชีพ เป็นแบบอย่างที่ดีให้ผู้เรียน ผู้ปกครอง และชุมชน มีทักษะการจัดการเรียนการสอนสำหรับผู้เรียนในศตวรรษที่ 21
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>วัตถุประสงค์ของหลักสูตร :</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;มีการกำหนดเป้าหมายการเรียนรู้ที่ชัดเจน ครอบคลุมทั้ง 3 ด้าน ได้แก่ ด้านความรู้ (Knowledge) ด้านทักษะปฏิบัติ (Skill) และด้านคุณลักษณะความเป็นครู (Attribute)
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. เพื่อพัฒนาและยกระดับมาตรฐานความเป็นครูมืออาชีพให้ครูผู้สอนกลุ่มสาระการเรียนรู้ภาษาไทยระดับชั้นประถมศึกษาให้มีความรู้ความเข้าใจและมีเทคนิคการจัดการเรียนรู้เพื่อพัฒนา
				การอ่านและการเขียนของผู้เรียนตามแนวทางศาสตร์พระราชาด้วยหลักปรัชญาของเศรษฐกิจพอเพียง และการเรียนรู้ในศตวรรษที่ 21 ด้วยกระบวนการ Active Learning
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. เพื่อพัฒนาครูและบุคลากรทางการศึกษาให้ประพฤติตนเป็นครูดีของแผ่นดิน มีศีลธรรม มีจิตวิญญาณความเป็นครู เจริญรอยตามเบื้องพระยุคลบาท มีทักษะการจัดการเรียนการสอนสำหรับผู้เรียนในศตวรรษที่ 21
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. เพื่อพัฒนาทักษะด้านเทคโนโลยี การใช้คอมพิวเตอร์ อินเทอร์เน็ต หรือสมาร์ทโฟน และสามารถบูรณาการกับการจัดการเรียนรู้ได้อย่างเหมาะสม
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>ตัวชี้วัดความสำเร็จของการเรียนรู้ของครู :</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. ผู้เข้ารับการพัฒนาอย่างน้อยร้อยละ 80 เกิดแรงบันดาลใจและตั้งใจปรับเปลี่ยนพฤติกรรมตนเองในทางที่ดีขึ้น
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. ผู้เข้ารับการพัฒนาร้อยละ 100 มีกลุ่มการเรียนรู้ร่วมกันทั้งทางสังคมจริงและสังคมเสมือนออนไลน์
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. ผู้เข้ารับการพัฒนาร้อยละ 80 มีทักษะการจัดการเรียนการสอนภาษาไทยสำหรับผู้เรียนชั้นประถมศึกษา ในศตวรรษที่ 21 เพิ่มขึ้น
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4. ผู้เข้ารับการพัฒนามีเวลาเข้าร่วมกิจกรรมตามหลักสูตรไม่น้อยกว่าร้อยละ 80
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>กรอบแนวคิดของหลักสูตร :</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;กรอบแนวคิดของหลักสูตร (Curriculum Conceptual Framework) เป็นการเชื่อมโยงแนวคิดของหลักสูตรกับหลักการทางวิชาการสะท้อนเป็น
				การหลอมรวมเทคโนโลยีสารสนเทศเข้ากับการสอนเนื้อหาสาระเฉพาะ (Technological Pedagogical Content Knowledge : TPCK) ที่จะเชื่อมโยงกลับไปสู่การจัดการเรียนรู้กลุ่มสาระการเรียนรู้ภาษาไทย
				ระดับชั้นประถมศึกษา
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;จากแนวคิดที่ต้องการพัฒนาครูผู้สอนกลุ่มสาระการเรียนรู้ภาษาไทยระดับชั้นประถมศึกษา เพื่อยกระดับมาตรฐานวิชาชีพครูตามแนวทางศาสตร์พระราชาด้วย
				หลักปรัชญาของเศรษฐกิจพอเพียง และการเรียนรู้ในศตวรรษที่ 21 อย่างเป็นรูปธรรม โดยการน้อมนำพระบรมราโชวาทที่ทรงให้ไว้แก่วงการการศึกษาไทยมาปฏิบัติ ทั้งการครองตน ครองคน และครองงาน ผ่านเทคโนโลยี
				สมัยใหม่ และเครือข่ายการเรียนรู้ร่วมกัน หรือชุมชนแห่งการเรียนรู้มืออาชีพ (PLC) ด้วยการนำเสนอหลักการ แนวคิด และประสบการณ์ ที่เน้นการปลูกฝังคุณธรรม จริยธรรม และจรรยาบรรณวิชาชีพครู มีการจัดการ
				ความรู้ด้วยกระบวนการแลกเปลี่ยนเรียนรู้ (KM) การเรียนรู้ร่วมกัน (PLC) การเรียนรู้แบบมีส่วนร่วมและฝึกปฏิบัติ (Active Learning) ผสมผสานกับเทคโนโลยีและชุมชนแห่งการเรียนรู้ออนไลน์ เพื่อให้เกิดความเข้าใจ
				ที่ชัดเจนและสามารถนำไปบูรณาการในการจัดการเรียนการสอน โดยเฉพาะการอ่านและการเขียนภาษาไทย การดำรงตนในสังคม และการเป็นแบบอย่างที่ดีให้กับผู้เรียน ผู้ปกครอง และชุมชน ซึ่งจะส่งผลให้ครูประพฤติตน
				เป็นครูดีของแผ่นดิน มีศีลธรรม มีจิตวิญญาณความเป็นครู เจริญรอยตามเบื้องพระยุคลบาท มีทักษะการจัดการเรียนการสอนสำหรับผู้เรียนในศตวรรษที่ 21 มีความรู้ความเข้าใจและมีเทคนิคการจัดการเรียนรู้เพื่อพัฒนา
				การอ่านและการเขียนของผู้เรียนตามแนวทางศาสตร์พระราชาด้วยหลักปรัชญาของเศรษฐกิจพอเพียง มีทักษะด้านเทคโนโลยี การใช้คอมพิวเตอร์ อินเทอร์เน็ต หรือสมาร์ทโฟนสูงขึ้น
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>หัวข้อสาระการอบรม :</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;หัวข้อเนื้อหาสาระการอบรมทั้งหมด 7 หัวข้อ ประกอบด้วย 3 กลุ่มหลัก ได้แก่
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. กลุ่มเนื้อหาที่เป็นสาระวิชาด้าน (Content)
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1 เครือข่ายครูดีของแผ่นดิน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.2 อ่าน เขียน เรียนสนุก
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. กลุ่มเนื้อหาสาระที่เป็นศาสตร์ทางวิชาชีพครู (Pedagogy)
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.1 กลุ่มสัมพันธ์ ทักษะพื้นฐานเพื่อการเป็นครูดีของแผ่นดิน และการสะท้อนคิด
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.2 Active Learning เพื่อเรียนรู้การครองตน ครองคน ครองงาน และกระบวนการในโครงการ
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3 “ถามมาตอบไป” และสรุปบทเรียนประจำวัน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.4 เวทีเสวนา ตัวอย่างการปฏิบัติที่ดี “ครูดีของแผ่นดิน”
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. กลุ่มบูรณาการเนื้อหาสาระกับศาสตร์วิชาชีพครูและเทคโนโลยีสารสนเทศ (Technological Pedagogical Content Knowledge : TPCK)
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.1 ระบบการติดตามและประเมินผลผ่านระบบออนไลน์
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>หัวข้อที่ 1 เครือข่ายครูดีของแผ่นดิน</strong>
				<br>
				<i>ความคิดรวบยอดหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เพื่อให้ผู้เข้ารับการอบรมมีความเข้าใจแนวคิดและหลักคิดการสร้างคนดี การสร้างเครือข่าย วัตถุประสงค์ เป้าหมายของเครือข่ายครูดีของแผ่นดิน 
				มีความรู้และความเข้าใจหลักการครองตน ครองคน ครองงาน และมีความเข้าใจแนวทางการปฏิบัติ การติดตามและประเมินผลให้เป็นไปตามเป้าหมาย
				<br>
				<i>วิธีการอบรมหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. การบรรยายเรื่อง “ความเป็นมาของเครือข่ายครูดีของแผ่นดิน”
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. การบรรยายเรื่อง “วัตถุประสงค์และเป้าหมายของเครือข่ายครูดีของแผ่นดิน”
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. การบรรยายเรื่อง “หลักการครองตน ครองคน ครองงาน”
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4. การบรรยายเรื่อง “การทำกิจกรรมพัฒนา การติดตามผล และการประเมินผล”
				<br>
				<strong>หัวข้อที่ 2 กลุ่มสัมพันธ์ ทักษะพื้นฐานเพื่อการเป็นครูดีของแผ่นดิน และการสะท้อนคิด</strong>
				<br>
				<i>ความคิดรวบยอดหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เพื่อให้ผู้เข้ารับการอบรมมีความกล้าแสดงออก เปิดใจยอมรับกันและกัน ยอมรับในความแตกต่างของผู้อื่น และสามารถปรับตัวเข้าหากันได้อย่างรวดเร็ว 
				เสริมสร้างความสามัคคี และความรู้สึกที่ดีต่อกันด้วยบรรยากาศสนุกสนาน และผ่อนคลาย และมีความรู้ความเข้าใจเกี่ยวกับองค์ประกอบหลัก 3 องค์ประกอบ และตัวบ่งชี้ย่อย 19 ตัวบ่งชี้ ได้แก่ 1) การครองตน 9 
				ตัวบ่งชี้ 2) การครองคน 3 ตัวบ่งชี้ 3) การครองงาน 7 ตัวบ่งชี้ และมีทักษะพื้นฐานเพื่อการเป็นครูดีของแผ่นดิน
				<br>
				<i>วิธีการอบรมหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. กิจกรรมรวมกลุ่ม
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. กิจกรรมเพื่อละลายพฤติกรรม
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. กิจกรรมแลกเปลี่ยนความคิดและประสบการณ์ วิเคราะห์และสรุปร่วมกัน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4. กิจกรรมนันทนาการ
				<br>
				<strong>หัวข้อที่ 3 Active Learning เพื่อเรียนรู้การครองตน ครองคน ครองงาน และกระบวนการในโครงการ</strong>
				<br>
				<i>ความคิดรวบยอดหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เพื่อให้ผู้เข้ารับการอบรมมีความรู้และความเข้าใจกระบวนการในโครงการเครือข่ายครูดีของแผ่นดินฯ หลักการครองตน ครองคน ครองงาน ซึ่งประกอบ
				ด้วยองค์ประกอบหลัก 3 องค์ประกอบ และตัวบ่งชี้ย่อย 19 ตัวบ่งชี้ ได้แก่ 1) การครองตน 9 ตัวบ่งชี้ 2) การครองคน 3 ตัวบ่งชี้ 3) การครองงาน 7 ตัวบ่งชี้ ผ่านกระบวนการเรียนรู้ Active Learning 
				และสามารถนำไปปรับใช้ได้
				<br>
				<i>วิธีการอบรมหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. การบรรยายเรื่อง “กระบวนการในโครงการเครือข่ายครูดีของแผ่นดินฯ”
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. กิจกรรมแลกเปลี่ยนความคิดและประสบการณ์ วิเคราะห์และสรุปร่วมกัน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. กิจกรรมการเรียนรู้แบบมีส่วนร่วม
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4. กิจกรรมกลุ่ม
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;5. กิจกรรมนำเสนอและอภิปราย
				<br>
				<strong>หัวข้อที่ 4 “ถามมาตอบไป” และสรุปบทเรียนประจำวัน</strong>
				<br>
				<i>ความคิดรวบยอดหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เพื่อให้ผู้เข้ารับการอบรมทบทวนความรู้ความเข้าใจเนื้อหาสาระจากการเรียนรู้ เกี่ยวกับหลักการครองตน ครองคน ครองงาน และเพื่อให้เกิดทักษะการคิด
				วิเคราะห์ คิดสังเคราะห์ และคิดรวบยอด สามารถเชื่อมโยงความรู้และประสบการณ์ นำไปสู่การปฏิบัติได้ และมีทักษะพื้นฐานเพื่อการเป็นครูดีของแผ่นดิน
				<br>
				<i>วิธีการอบรมหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. กิจกรรมการแลกเปลี่ยนเรียนรู้ (KM)
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. กิจกรรมการเรียนรู้ร่วมกัน (PLC)
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. กิจกรรมนันทนาการ
				<br>
				<strong>หัวข้อที่ 5 เวทีเสวนา ตัวอย่างการปฏิบัติที่ดี “ครูดีของแผ่นดิน”</strong>
				<br>
				<i>ความคิดรวบยอดหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เพื่อให้ผู้เข้ารับการอบรมสามารถเชื่อมโยงความรู้และประสบการณ์ เกี่ยวกับหลักการครองตน ครองคน ครองงานและนำไปสู่การปฏิบัติ และเพื่อเสริม
				แนวคิดและวิธีปฏิบัติจากผู้มีประสบการณ์ ที่สามารถนำไปปรับใช้ให้เหมาะสมและประสบความสำเร็จ
				<br>
				<i>วิธีการอบรมหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;กิจกรรมเสวนา
				<br>
				<strong>หัวข้อที่ 6 ระบบการติดตามและประเมินผลผ่านระบบออนไลน์</strong>
				<br>
				<i>ความคิดรวบยอดหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เพื่อให้ผู้เข้ารับการอบรมมีความรู้และความเข้าใจระบบการติดตามและประเมินผลผ่านระบบออนไลน์ หลักการและวิธีการนำเสนอผลงานหรือกิจกรรมผ่าน
				เฟซบุ๊ก ผ่านการเรียนรู้ด้วยการปฏิบัติจริง สามารถสมัครสมาชิกเครือข่ายครูดีได้ สามารถนำเสนอการทำกิจกรรมผ่านเฟซบุ๊กได้ สามารถใช้งานระบบประเมินผลออนไลน์ได้
				<br>
				<i>วิธีการอบรมหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. การบรรยายพร้อมการสาธิตเรื่อง “ระบบติดตามและประเมินผลผ่านระบบออนไลน์”
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. การบรรยายพร้อมการสาธิตเรื่อง “การสมัครสมาชิกเครือข่ายครูดีของแผ่นดินและการรวมกลุ่ม”
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. การบรรยายพร้อมการสาธิตเรื่อง “แนวทางการโพสกิจกรรมต่างๆ และการติด Hashtag”
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4. การฝึกปฏิบัติ
				<br>
				<strong>หัวข้อที่ 7 อ่าน เขียน เรียนสนุก</strong>
				<br>
				<i>ความคิดรวบยอดหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เพื่อให้ผู้เข้ารับการอบรมมีเทคนิคการสอนและทักษะการสอนด้านการอ่านและการเขียนที่ถูกต้อง
				<br>
				<i>วิธีการอบรมหัวข้อสาระการอบรม :</i>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. กิจกรรมแลกเปลี่ยนความคิดและประสบการณ์ วิเคราะห์และสรุปร่วมกัน เรื่อง “ปัญหาการอ่านและการเขียนที่พบในห้องเรียน”
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. กิจกรรมกลุ่ม เลือกอ่านนิทานตามความสนใจ 1 เรื่อง และนำเสนอโดยแสดงบทบาทและน้ำเสียงให้สมจริง
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. กิจกรรมกลุ่ม เขียนนิทานตามความสนใจกลุ่มละ 1 เรื่อง และนำเสนอโดยแสดงบทบาทและน้ำเสียงให้สมจริงและอภิปราย
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4. การบรรยายเรื่อง “แนวคิดการสอนอ่านและเทคนิคการสอนอ่าน”
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;5. การบรรยายเรื่อง “แนวคิดการสอนเขียนและเทคนิคการสอนเขียน”
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;6. กิจกรรมกลุ่ม การนำแนวทางจากการอบรมสู่ห้องเรียนโดยเขียนแผนการจัดกิจกรรมการเรียนรู้ 1 แผน พร้อมนำเสนอ และอภิปราย
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>ตารางกิจกรรม</strong>
				<br>
				<strong>วันแรก</strong>
				<div class="table-main table-responsive">
					<table class="table6" style="background-color:rgb(240,240,240);">
						<thead>
							<tr>
								<th>เวลา</th>
								<th>กิจกรรม</th>
								<th>เนื้อหา</th>
								<th>วิทยากร</th>
								<th>รูปแบบกิจกรรม</th>
								<th>ระยะเวลา</th>
								<th>สื่อ/อุปกรณ์</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>8:30</td>
								<td>ลงทะเบียน</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>9:00</td>
								<td>เครือข่ายครูดีของแผ่นดิน</td>
								<td>แนวคิดและหลักคิดการสร้างคนดี การสร้างเครือข่าย วัตถุประสงค์ เป้าหมายของเครือข่ายครูดีของแผ่นดิน</td>
								<td>รศ.ดร.ปรีชา คัมภีรปกรณ์ หรือ นางปาลิตา กุลรุ่งโรจน์ หรือ นายสมยศ ศิริบรรณ</td>
								<td>บรรยาย</td>
								<td>1 ชม. 15 นาที</td>
								<td>คอมพิวเตอร์ โปรแกรมนำเสนอ (Power Point) และสื่อมัลติมีเดีย</td>
							</tr>
							<tr>
								<td>10:30</td>
								<td>กลุ่มสัมพันธ์ ทักษะพื้นฐานเพื่อการเป็นครูดีของแผ่นดิน และการสะท้อนความคิด</td>
								<td>องค์ประกอบหลัก 3 องค์ประกอบ และตัวบ่งชี้ย่อย 19 ตัวบ่งชี้ และมีทักษะพื้นฐานเพื่อการเป็นครูดีของแผ่นดิน</td>
								<td>นายอำนาจ จารุมณีโรจน์ หรือ นางละเอียด นิ่มมะโน</td>
								<td>บรรยายและลงมือปฏิบัติ</td>
								<td>1 ชม. 30 นาที</td>
								<td>คอมพิวเตอร์ โปรแกรมนำเสนอ (Power Point) และสื่อมัลติมีเดีย Flip chart กระดาษปากกาเคมี Post-it และผ้าพันคอสีกลุ่ม</td>
							</tr>
							<tr>
								<td>12:00</td>
								<td>รับประทานอาหารกลางวัน</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>13:30</td>
								<td>Active Learning เพื่อเรียนรู้การครองตน ครองคน ครองงาน และกระบวนการในโครงการ</td>
								<td>การเรียนรู้กระบวนการในโครงการเครือข่ายครูดีของแผ่นดินฯ ผ่านกระบวนการเรียนรู้ Active Learning และการนำไปปรับใช้</td>
								<td>นายอำนาจ จารุมณีโรจน์ หรือ นางละเอียด นิ่มมะโน</td>
								<td>ลงมือปฏิบัติ</td>
								<td>2 ชม. 45 นาที</td>
								<td>Flip chart กระดาษ และปากกาเคมี</td>
							</tr>
							<tr>
								<td>16:30</td>
								<td>รับประทานอาหารเย็น</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>18:00</td>
								<td>"ถามมาตอบไป" และสรุปบทเรียนประจำวัน</td>
								<td>ทบทวนความรู้ความเข้าใจเกี่ยวกับหลักการครองตน ครองคน ครองงาน และมีทักษะพื้นฐานเพื่อการเป็นครูดีของแผ่นดิน</td>
								<td>นายอำนาจ จารุมณีโรจน์ หรือ นางละเอียด นิ่มมะโน</td>
								<td>ลงมือปฏิบัติ</td>
								<td>2 ชม. 0 นาที</td>
								<td>Flip chart กระดาษ และปากกาเคมี</td>
							</tr>
							<tr>
								<td>20:00</td>
								<td>สวดมนต์ ทำวัตรเย็น นั่งสมาธิ</td>
								<td></td>
								<td>นายอำนาจ จารุมณีโรจน์ หรือ นางละเอียด นิ่มมะโน</td>
								<td>ลงมือปฏิบัติ</td>
								<td>1 ชม. 0 นาที</td>
								<td>ไม่มี</td>
							</tr>
							<tr>
								<td>21:00</td>
								<td>พักผ่อนตามอัธยาศัย</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<strong>หมายเหตุ:</strong> รับประทานอาหารว่าง เวลา 10:15 น. และ 15:00 น.
				</div>
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>วันที่สอง</strong>
				<div class="table-main table-responsive">
					<table class="table6" style="background-color:rgb(240,240,240);">
						<thead>
							<tr>
								<th>เวลา</th>
								<th>กิจกรรม</th>
								<th>เนื้อหา</th>
								<th>วิทยากร</th>
								<th>รูปแบบกิจกรรม</th>
								<th>ระยะเวลา</th>
								<th>สื่อ/อุปกรณ์</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>6:30</td>
								<td>"งานบุญใส่บาตรเพื่อการเรียนรู้ร่วมกัน</td>
								<td></td>
								<td>นายอำนาจ จารุมณีโรจน์ หรือ นางละเอียด นิ่มมะโน</td>
								<td>ลงมือปฏิบัติ</td>
								<td>30 นาที</td>
								<td>ของใส่บาตรผู้เข้ารับการอบรมเตรียมเอง</td>
							</tr>
							<tr>
								<td>7:00</td>
								<td>รับประทานอาหารเช้า</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>8:30</td>
								<td>ลงทะเบียน</td>
								<td></td>
								<td></td>
								<td></td>
								<td>30 นาที</td>
								<td></td>
							</tr>
							<tr>
								<td>9:00</td>
								<td>เวทีเสวนา ตัวอย่างการปฏิบัติที่ดี "ครูดีของแผ่นดิน"</td>
								<td>เชื่อมโยงความรู้และประสบการณ์เกี่ยวกับหลักการครองตน ครองคน ครองงานและนำไปสู่การปฏิบัติให้เหมาะสมและประสบความสำเร็จ</td>
								<td>นางละเอียด นิ่มมะโน หรือ นายนิกร เสาร์ฝัน</td>
								<td>การเสวนา</td>
								<td>1 ชม. 15 นาที</td>
								<td>ไม่มี</td>
							</tr>
							<tr>
								<td>10:30</td>
								<td>ระบบการติดตามและประเมินผลผ่านระบบออนไลน์</td>
								<td>ระบบการติดตามและประเมินผลผ่านระบบออนไลน์ หลักการและวิธีการนำเสนอผลงานหรือกิจกรรมผ่านเฟซบุ๊ก</td>
								<td>นายโชคชัย ตันเจริญ หรือ นายสุวิทย์ วิลัยแก้ว หรือ นายโสฬส บุญโทแสง</td>
								<td>บรรยายและลงมือปฏิบัติ</td>
								<td>1 ชม. 30 นาที</td>
								<td>คอมพิวเตอร์ โปรแกรมนำเสนอ (Power Point) และสมาร์ทโฟนหรือคอมพิวเตอร์โน๊ตบุ๊กของผู้เข้ารับการอบรม</td>
							</tr>
							<tr>
								<td>12:00</td>
								<td>รับประทานอาหารกลางวัน</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>13:15</td>
								<td>อ่าน เขียน เรียนสนุก</td>
								<td>เทคนิคการสอนและทักษะการสอนด้านการอ่านและการเขียนที่ถูกต้อง</td>
								<td>นางแพรวพรรณ บรรจงศิริทัศน์<br>นายเตชิต นันทประพิณ</td>
								<td>บรรยายและลงมือปฏิบัติ</td>
								<td>2 ชม. 15 นาที</td>
								<td>คอมพิวเตอร์ โปรแกรมนำเสนอ (Power Point) หนังสือนิทานอุปกรณ์ที่เป็นหน้ากากชนิดต่างๆ</td>
							</tr>
							<tr>
								<td>15:45</td>
								<td>สรุปบทเรียน</td>
								<td></td>
								<td>นายอำนาจ จารุมณีโรจน์ หรือ นางละเอียด นิ่มมะโน</td>
								<td>ลงมือปฏิบัติ</td>
								<td>45 นาที</td>
								<td>Flip chart กระดาษ และปากกาเคมี</td>
							</tr>
							<tr>
								<td>16:30</td>
								<td>เดินทางกลับ</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<strong>หมายเหตุ:</strong> รับประทานอาหารว่าง เวลา 10:15 น. และ 15:00 น.
				</div>
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>กิจกรรมการติดตามหรือทำงานร่วมกับครูหลังการพัฒนา :</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. ผู้เข้าอบรมประเมินตนเองผ่านระบบออนไลน์ ก่อนเริ่มกิจกรรมพัฒนาในโครงการและหลังจากร่วมกิจกรรมพัฒนาต่างๆ
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. ผู้เข้ารับการอบรมรายงานผลการพัฒนาตนเองผ่านชุมชนแห่งการเรียนรู้ ไลน์ และ เฟซบุ๊กของเครือข่ายครูดีของแผ่นดิน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. ผู้ประสานงานจังหวัดของมูลนิธิฯ นิเทศติดตาม ให้คำแนะนำ ปรึกษา ให้ขวัญกำลังใจกลุ่มเครือข่ายครูดีในการดำเนินกิจกรรมต่างๆ
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4. ผู้รับผิดชอบโครงการเครือข่ายครูดีของแผ่นดินจากสำนักงานเขตพื้นที่การศึกษาต่างๆ ที่ได้รับการแต่งตั้งจากมูลนิธิฯ นิเทศติดตาม ให้คำแนะนำ ปรึกษา ให้ขวัญกำลังใจกลุ่มเครือข่ายครูดีในการดำเนินกิจกรรมต่างๆ
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;5. คณะกรรมการประเมินเครือข่ายครูดีที่ได้รับการแต่งตั้งจากมูลนิธิฯ ตรวจเยี่ยมและให้กำลังใจกลุ่มเครือข่ายครูดี
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>วิทยากรหลัก :</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;คณะวิทยากร ได้แก่
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. รศ.ดร.ปรีชา คัมภีรปกรณ์
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• อดีตคณบดีคณะศึกษาศาสตร์ มหาวิทยาลัยขอนแก่น
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• ผู้ทรงคุณวุฒิกระทรวงศึกษาธิการ
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• ประธานหลักสูตรปริญญาเอก การบริหารการศึกษา มหาวิทยาลัยราชภัฎสกลนคร
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• ประธานอนุกรรมการวิชาการเครือข่ายครูดีของแผ่นดิน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• รองประธานกรรมการมูลนิธิครูดีของแผ่นดิน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. นางปาลิดา กุลรุ่งโรจน์
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• ปริญญาโท ศึกษาศาสตรมหาบัณฑิต (การบริหารการศึกษา) มหาวิทยาลัยนเรศวร
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• การบริหารงานภาครัฐและกฎหมายมหาชน สถาบันพระปกเกล้า
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• อดีตผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาประถมศึกษาพระนครศรีอยุธยา เขต 2
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• ประธานอนุกรรมการประสานงานเครือข่ายครูดีของแผ่นดิน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• กรรมการมูลนิธิครูดีของแผ่นดิน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. นายอำนาจ จารุมณีโรจน์
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• ปริญญาโท ศึกษาศาสตร์มหาบัณฑิต (การบริหารการศึกษา) มหาวิทยาลัยศิลปากร
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• กำลังศึกษาระดับปริญญาเอก ภาควิชาการบริหารการศึกษา มหาวิทยาลัยศิลปากร
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• อดีตกรรมการผู้ทรงคุณวุฒิ สำนักงานเขตพื้นที่การศึกษาประถมศึกษากรุงเทพมหานคร
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• อดีตกรรมการผู้ทรงคุณวุฒิ สำนักงานเขตพื้นที่การศึกษามัธยมศึกษา เขต 1 (กรุงเทพ)
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• รางวัลคุรุคุณธรรม คุรุสภา ปี พ.ศ. 2553-2556
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• เลขานุการมูลนิธิครูดีของแผ่นดิน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4. นางละเอียด นิ่มมะโน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• อดีตศึกษานิเทศก์เชี่ยวชาญ
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• วิทยากรหลักสูตรการศึกษาขั้นพื้นฐาน พุทธศักราช 2551
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• วิทยากรหลักปรัชญาของเศรษฐกิจพอเพียง
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;5. นางแพรวพรรณ บรรจงศิริทัศน์
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• ปริญญาโท การศึกษามหาบัณฑิต (หลักสูตรและการสอน) มหาวิทยาลัยมหาสารคาม
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• อดีตข้าราชารครูวิทยฐานะครูเชี่ยวชาญ
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• วิทยากรภาษาไทย สมาคมครูภาษาไทย
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;6. นายโชคชัย ตันเจริญ
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• เจ้าหน้าที่สารสนเทศของโครงการ
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>กำหนดจำนวนครูที่จะเข้าร่วมพัฒนาต่อกลุ่ม :</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;ผู้เข้าอบรมต่อรุ่น 60 – 150 คน
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>วิธีการวัดและประเมินผลการอบรมของหลักสูตร :</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;<u>วิธีการ</u>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. เวลาเข้าร่วมกิจกรรมตามหลักสูตร
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. การมีส่วนร่วมในกิจกรรม
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. ประเมินผลงานของผู้เข้ารับการอบรม
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4. ผู้เข้ารับการอบรมประเมินตนเอง
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;5. ผู้เข้ารับการอบรมประเมินความพึงพอใจหลักสูตร
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;<u>เครื่องมือ</u>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. แบบลงทะเบียนผู้เข้ารับการอบรม
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. แบบประเมินการนำเสนอแผนการจัดกิจกรรมการเรียนรู้
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. ระบบประเมินตนเองออนไลน์
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4. ระบบประเมินความพึงพอใจของผู้เข้ารับการอบรม
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;<u>เกณฑ์การรับรองผ่านหลักสูตร</u>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1. มีเวลาเข้าร่วมกิจกรรมตามหลักสูตรไม่น้อยกว่าร้อยละ 80
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2. มีผลงานจากการทำกิจกรรม โดยสังเกตการมีส่วนร่วมในกิจกรรมต่างๆ จากการสัมภาษณ์พูดคุยขณะทำกิจกรรม และศึกษาเอกสารจากผลงาน แบบประเมินการนำเสนอแผนการจัดกิจกรรมการเรียนรู้ มีคะแนนร้อยละ 80 จึงผ่านเกณฑ์
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3. มีผลการประเมินตนเองด้านการครองตน ครองคน ครองงาน ผ่านระบบประเมินออนไลน์
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4. มีผลการประเมินความพึงพอใจของผู้เข้ารับการอบรมผ่านระบบออนไลน์
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>วันที่ :</strong>&nbsp;&nbsp;ช่วงเวลา 30 มิถุนายน – 16 กันยายน 2561
				<br>
				<strong>สถานที่ :</strong>&nbsp;&nbsp;ยังไม่ได้กำหนด
				<br>
				<strong>ค่าใช้จ่าย :</strong>&nbsp;&nbsp;2,895 บาท รวมที่พัก อาหาร 4 มื้อ อาหารว่าง 4 มื้อ
			</p>
			<ul class="fs-32 textlink" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
				<li><a href="docs/ร่วมโครงการ/อบรมสัมมนา/Public Training/หลักสูตร  ศาสตร์พระราชา สำหรับครูดีภาษาไทยในศตวรรษที่ 21 (ประถมศึกษา).pdf">ดาวน์โหลดรายละเอียดหลักสูตร</a></li>
			</ul>
		</article>
		<!-- End Content -->
		<!-- ################################################################################################ -->
		<!-- Start Gallery -->
    <p class="font-x3"><span style="color:rgb(180,147,31); line-height:30px;"><strong>คลังภาพ</strong></span></p>
    <div id="gallery" style="margin-top:50px">
      <ul class="nospace clear">
        <li class="one_third first zoom11"><a href="images/Public Training/PBT002/610701 อบรม ณ จังหวัดนครนายก/PBT002img05.jpg"><img src="images/Public Training/PBT002/610701 อบรม ณ จังหวัดนครนายก/PBT002img05.jpg" alt="610701 อบรม ณ จังหวัดนครนายก PBT002img05"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610701 อบรม ณ จังหวัดนครนายก/PBT002img07.jpg"><img src="images/Public Training/PBT002/610701 อบรม ณ จังหวัดนครนายก/PBT002img07.jpg" alt="610701 อบรม ณ จังหวัดนครนายก PBT002img07"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610701 อบรม ณ จังหวัดนครนายก/PBT002img08.jpg"><img src="images/Public Training/PBT002/610701 อบรม ณ จังหวัดนครนายก/PBT002img08.jpg" alt="610701 อบรม ณ จังหวัดนครนายก PBT002img08"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Public Training/PBT002/610701 อบรม ณ จังหวัดนครนายก/PBT002img10.jpg"><img src="images/Public Training/PBT002/610701 อบรม ณ จังหวัดนครนายก/PBT002img10.jpg" alt="610701 อบรม ณ จังหวัดนครนายก PBT002img10"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610708 อบรม ณ จังหวัดสงขลา/PBT002img03.jpg"><img src="images/Public Training/PBT002/610708 อบรม ณ จังหวัดสงขลา/PBT002img03.jpg" alt="610708 อบรม ณ จังหวัดสงขลา PBT002img03"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610708 อบรม ณ จังหวัดสงขลา/PBT002img05.jpg"><img src="images/Public Training/PBT002/610708 อบรม ณ จังหวัดสงขลา/PBT002img05.jpg" alt="610708 อบรม ณ จังหวัดสงขลา PBT002img05"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Public Training/PBT002/610708 อบรม ณ จังหวัดสงขลา/PBT002img06.jpg"><img src="images/Public Training/PBT002/610708 อบรม ณ จังหวัดสงขลา/PBT002img06.jpg" alt="610708 อบรม ณ จังหวัดสงขลา PBT002img06"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610708 อบรม ณ จังหวัดสงขลา/PBT002img08.jpg"><img src="images/Public Training/PBT002/610708 อบรม ณ จังหวัดสงขลา/PBT002img08.jpg" alt="610708 อบรม ณ จังหวัดสงขลา PBT002img08"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610708 อบรม ณ จังหวัดสงขลา/PBT002img09.jpg"><img src="images/Public Training/PBT002/610708 อบรม ณ จังหวัดสงขลา/PBT002img09.jpg" alt="610708 อบรม ณ จังหวัดสงขลา PBT002img09"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Public Training/PBT002/610708 อบรม ณ จังหวัดสงขลา/PBT002img10.jpg"><img src="images/Public Training/PBT002/610708 อบรม ณ จังหวัดสงขลา/PBT002img10.jpg" alt="610708 อบรม ณ จังหวัดสงขลา PBT002img10"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610715 อบรม ณ จังหวัดเชียงใหม่/PBT002img01.jpg"><img src="images/Public Training/PBT002/610715 อบรม ณ จังหวัดเชียงใหม่/PBT002img01.jpg" alt="610715 อบรม ณ จังหวัดเชียงใหม่ PBT002img01"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610715 อบรม ณ จังหวัดเชียงใหม่/PBT002img02.jpg"><img src="images/Public Training/PBT002/610715 อบรม ณ จังหวัดเชียงใหม่/PBT002img02.jpg" alt="610715 อบรม ณ จังหวัดเชียงใหม่ PBT002img02"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Public Training/PBT002/610715 อบรม ณ จังหวัดเชียงใหม่/PBT002img03.jpg"><img src="images/Public Training/PBT002/610715 อบรม ณ จังหวัดเชียงใหม่/PBT002img03.jpg" alt="610715 อบรม ณ จังหวัดเชียงใหม่ PBT002img03"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610715 อบรม ณ จังหวัดเชียงใหม่/PBT002img05.jpg"><img src="images/Public Training/PBT002/610715 อบรม ณ จังหวัดเชียงใหม่/PBT002img05.jpg" alt="610715 อบรม ณ จังหวัดเชียงใหม่ PBT002img05"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610722 อบรม ณ จังหวัด นครศรีธรรมราช/PBT002img07.jpg"><img src="images/Public Training/PBT002/610722 อบรม ณ จังหวัด นครศรีธรรมราช/PBT002img07.jpg" alt="610722 อบรม ณ จังหวัด นครศรีธรรมราช PBT002img07"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Public Training/PBT002/610722 อบรม ณ จังหวัด นครศรีธรรมราช/PBT002img03.jpg"><img src="images/Public Training/PBT002/610722 อบรม ณ จังหวัด นครศรีธรรมราช/PBT002img03.jpg" alt="610722 อบรม ณ จังหวัด นครศรีธรรมราช PBT002img03"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610722 อบรม ณ จังหวัด นครศรีธรรมราช/PBT002img08.jpg"><img src="images/Public Training/PBT002/610722 อบรม ณ จังหวัด นครศรีธรรมราช/PBT002img08.jpg" alt="610722 อบรม ณ จังหวัด นครศรีธรรมราช PBT002img08"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610722 อบรม ณ จังหวัด นครศรีธรรมราช/PBT002img09.jpg"><img src="images/Public Training/PBT002/610722 อบรม ณ จังหวัด นครศรีธรรมราช/PBT002img09.jpg" alt="610722 อบรม ณ จังหวัด นครศรีธรรมราช PBT002img09"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Public Training/PBT002/610722 อบรม ณ จังหวัด นครศรีธรรมราช/PBT002img11.jpg"><img src="images/Public Training/PBT002/610722 อบรม ณ จังหวัด นครศรีธรรมราช/PBT002img11.jpg" alt="610722 อบรม ณ จังหวัด นครศรีธรรมราช PBT002img11"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610722 อบรม ณ จังหวัด นครศรีธรรมราช/PBT002img14.jpg"><img src="images/Public Training/PBT002/610722 อบรม ณ จังหวัด นครศรีธรรมราช/PBT002img14.jpg" alt="610722 อบรม ณ จังหวัด นครศรีธรรมราช PBT002img14"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610722 อบรม ณ จังหวัด บุรีรัมย์/PBT002img09.jpg"><img src="images/Public Training/PBT002/610722 อบรม ณ จังหวัด บุรีรัมย์/PBT002img09.jpg" alt="610722 อบรม ณ จังหวัด บุรีรัมย์ PBT002img09"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Public Training/PBT002/610722 อบรม ณ จังหวัด บุรีรัมย์/PBT002img10.jpg"><img src="images/Public Training/PBT002/610722 อบรม ณ จังหวัด บุรีรัมย์/PBT002img10.jpg" alt="610722 อบรม ณ จังหวัด บุรีรัมย์ PBT002img10"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610722 อบรม ณ จังหวัด บุรีรัมย์/PBT002img12.jpg"><img src="images/Public Training/PBT002/610722 อบรม ณ จังหวัด บุรีรัมย์/PBT002img12.jpg" alt="610722 อบรม ณ จังหวัด บุรีรัมย์ PBT002img12"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610805 อบรม ณ จังหวัดสระบุรี/PBT002img11.jpg"><img src="images/Public Training/PBT002/610805 อบรม ณ จังหวัดสระบุรี/PBT002img11.jpg" alt="610805 อบรม ณ จังหวัดสระบุรี PBT002img11"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Public Training/PBT002/610805 อบรม ณ จังหวัดสระบุรี/PBT002img12.jpg"><img src="images/Public Training/PBT002/610805 อบรม ณ จังหวัดสระบุรี/PBT002img12.jpg" alt="610805 อบรม ณ จังหวัดสระบุรี PBT002img12"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610805 อบรม ณ จังหวัดสระบุรี/PBT002img13.jpg"><img src="images/Public Training/PBT002/610805 อบรม ณ จังหวัดสระบุรี/PBT002img13.jpg" alt="610805 อบรม ณ จังหวัดสระบุรี PBT002img13"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610805 อบรม ณ จังหวัดสระบุรี/PBT002img16.jpg"><img src="images/Public Training/PBT002/610805 อบรม ณ จังหวัดสระบุรี/PBT002img16.jpg" alt="610805 อบรม ณ จังหวัดสระบุรี PBT002img16"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Public Training/PBT002/610805 อบรม ณ จังหวัดอุดรธานี/PBT002img01.jpg"><img src="images/Public Training/PBT002/610805 อบรม ณ จังหวัดอุดรธานี/PBT002img01.jpg" alt="610805 อบรม ณ จังหวัดอุดรธานี PBT002img01"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610805 อบรม ณ จังหวัดอุดรธานี/PBT002img06.jpg"><img src="images/Public Training/PBT002/610805 อบรม ณ จังหวัดอุดรธานี/PBT002img06.jpg" alt="610805 อบรม ณ จังหวัดอุดรธานี PBT002img06"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610805 อบรม ณ จังหวัดอุดรธานี/PBT002img10.jpg"><img src="images/Public Training/PBT002/610805 อบรม ณ จังหวัดอุดรธานี/PBT002img10.jpg" alt="610805 อบรม ณ จังหวัดอุดรธานี PBT002img10"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Public Training/PBT002/610812 อบรม ณ จังหวัดลพบุรี/PBT002img01.jpg"><img src="images/Public Training/PBT002/610812 อบรม ณ จังหวัดลพบุรี/PBT002img01.jpg" alt="610812 อบรม ณ จังหวัดลพบุรี PBT002img01"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610812 อบรม ณ จังหวัดลพบุรี/PBT002img02.jpg"><img src="images/Public Training/PBT002/610812 อบรม ณ จังหวัดลพบุรี/PBT002img02.jpg" alt="610812 อบรม ณ จังหวัดลพบุรี PBT002img02"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610812 อบรม ณ จังหวัดลพบุรี/PBT002img17.jpg"><img src="images/Public Training/PBT002/610812 อบรม ณ จังหวัดลพบุรี/PBT002img17.jpg" alt="610812 อบรม ณ จังหวัดลพบุรี PBT002img17"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Public Training/PBT002/610819 อบรม ณ จังหวัดสกลนคร/PBT002img01.jpg"><img src="images/Public Training/PBT002/610819 อบรม ณ จังหวัดสกลนคร/PBT002img01.jpg" alt="610819 อบรม ณ จังหวัดสกลนคร PBT002img01"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610819 อบรม ณ จังหวัดสกลนคร/PBT002img14.jpg"><img src="images/Public Training/PBT002/610819 อบรม ณ จังหวัดสกลนคร/PBT002img14.jpg" alt="610819 อบรม ณ จังหวัดสกลนคร PBT002img14"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610819 อบรม ณ จังหวัดสกลนคร/PBT002img15.jpg"><img src="images/Public Training/PBT002/610819 อบรม ณ จังหวัดสกลนคร/PBT002img15.jpg" alt="610819 อบรม ณ จังหวัดสกลนคร PBT002img15"></a></li>
			</ul>
			<ul class="nospace clear">
				<li class="one_third first zoom11"><a href="images/Public Training/PBT002/610826 อบรม ณ จังหวัดพิษณุโลก/PBT002img04.jpg"><img src="images/Public Training/PBT002/610826 อบรม ณ จังหวัดพิษณุโลก/PBT002img04.jpg" alt="610826 อบรม ณ จังหวัดพิษณุโลก PBT002img04"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610826 อบรม ณ จังหวัดพิษณุโลก/PBT002img06.jpg"><img src="images/Public Training/PBT002/610826 อบรม ณ จังหวัดพิษณุโลก/PBT002img06.jpg" alt="610826 อบรม ณ จังหวัดพิษณุโลก PBT002img06"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT002/610826 อบรม ณ จังหวัดพิษณุโลก/PBT002img10.jpg"><img src="images/Public Training/PBT002/610826 อบรม ณ จังหวัดพิษณุโลก/PBT002img10.jpg" alt="610826 อบรม ณ จังหวัดพิษณุโลก PBT002img10"></a></li>
			</ul>
		</div>
		<!-- End Gallery -->
  </main>
</div>
<!-- End Content 00 - PBT003 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- JS for slider -->
<script src="js/slider.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>