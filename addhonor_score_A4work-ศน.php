<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		$sqlhonor_score4sup = "SELECT * FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code='A' ";
		$reHS4S = mysqli_query($con, $sqlhonor_score4sup);
		$rowHS4S = mysqli_fetch_array($reHS4S);
		$honor_score4sup_data1 = $rowHS4S['honor_score4sup_data1'];

		if($_GET['CFP'] == 'ส่วน1') {

			if($_GET['timeout'] == 'yes') {
				$honor_score4sup_data1 = $honor_score4sup_data1.',23.00000000,24.0,25.0';
			}else{
				$honor_score4sup_data1 = $honor_score4sup_data1.',23.'.$_GET['workscore231'].$_GET['workscore232'].$_GET['workscore233'].$_GET['workscore234'].$_GET['workscore235'].$_GET['workscore236'].$_GET['workscore237'].$_GET['workscore238'].',24.'.$_GET['workscore24'].',25.'.$_GET['workscore25'];
			}

			/* Update questions' answers to `honor_score4sup` Table */
			$sql = "UPDATE `honor_score4sup` SET honor_score4sup_data1='$honor_score4sup_data1' WHERE ID='$ID' AND honor_score4sup_code='A' ";
			$re = $con->query($sql) or die($con->error); //Check error

			header('location: ประเมินศน.ดีของแผ่นดินขั้นเกียรติคุณ-ครองงาน-ส่วน2.php?CFP=ส่วน1');

		}elseif($_GET['CFP'] == 'ส่วน2') {

			if($_GET['timeout'] == 'yes') {
				$honor_score4sup_data1 = $honor_score4sup_data1.',26.0,27.0,28.0,29.0,30.0';
			}else{
				$honor_score4sup_data1 = $honor_score4sup_data1.',26.'.$_GET['workscore26'].',27.'.$_GET['workscore27'].',28.'.$_GET['workscore28'].',29.'.$_GET['workscore29'].',30.'.$_GET['workscore30'];
			}

			/* Update questions' answers to `honor_score4sup` Table */
			$sql = "UPDATE `honor_score4sup` SET honor_score4sup_data1='$honor_score4sup_data1' WHERE ID='$ID' AND honor_score4sup_code='A' ";
			$re = $con->query($sql) or die($con->error); //Check error

			header('location: ประเมินศน.ดีของแผ่นดินขั้นเกียรติคุณ-ครองงาน-ส่วน3.php?CFP=ส่วน2');

		}elseif($_GET['CFP'] == 'ส่วน3') {

			if($_GET['timeout'] == 'yes') {
				$honor_score4sup_data1 = $honor_score4sup_data1.',31.0,32.0,33.0,34.0,35.0,36.0,37.0';
			}else{
				$honor_score4sup_data1 = $honor_score4sup_data1.',31.'.$_GET['workscore31'].',32.'.$_GET['workscore32'].',33.'.$_GET['workscore33'].',34.'.$_GET['workscore34'].',35.'.$_GET['workscore35'].',36.'.$_GET['workscore36'].',37.'.$_GET['workscore37'];
			}
			
			$honor_score4sup_date = date("Y-m-d H:i:s");

			/* Update questions' answers to `honor_score4sup` Table */
			$sql = "UPDATE `honor_score4sup` SET honor_score4sup_data1='$honor_score4sup_data1', honor_score4sup_date='$honor_score4sup_date' WHERE ID='$ID' AND honor_score4sup_code='A' ";
			$re = $con->query($sql) or die($con->error); //Check error

			include('includes/calhonor_score-ศน.php');
			$honor_score4sup_data2 = ','.$selfscore_avg.','.$peoplescore_avg.','.$workscore_avg;

			$sql = "UPDATE `honor_score4sup` SET honor_score4sup_data2='$honor_score4sup_data2' WHERE ID='$ID' AND honor_score4sup_code='A' ";
			$re = $con->query($sql) or die($con->error); //Check error

			// Log User Action
			$scorelog_task = 'ทำประเมิน';
			$scorelog_detail = 'ศน.,เกียรติคุณ';
			$scorelog_ans = $honor_score4sup_data1;

			$sqllog = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail, scorelog_ans) VALUES ('$ID', '$scorelog_task', '$scorelog_detail', '$scorelog_ans') ";
			$relog = $con->query($sqllog) or die($con->error); //Check error
			
			echo '<script>';
				echo "alert('คุณทำประเมินศึกษานิเทศก์ดีขั้นเกียรติคุณ เสร็จแล้วค่ะ');";
				echo "window.location.replace('โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php')";
			echo '</script>';

		}else{
			echo "<script>window.history.go(-1)</script>";
		}

	}else{
		echo "<script>window.history.go(-1)</script>"; //Protect policy: In the case of this page, not access by addbasic_score_btn
	}

?>