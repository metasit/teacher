<?php
/* Save&Print เกียรติบัตร for ครู */

	/***********************************************************************************************************************************/
	/**************************************************** Start Print & Save เกียรติบัตร **************************************************/
	/***********************************************************************************************************************************/

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/* Save เกียรติบัตรออนไลน์ with user name */
	$jpg_image = imagecreatefromjpeg('images/เกียรติบัตร/พื้นฐาน/ครู/เกียรติบัตรขั้นพื้นฐาน-ครู.jpg'); // Create Image From Existing File
	$font_color = imagecolorallocate($jpg_image, 0, 0, 0); // Set font color
	$font_path = 'layout/styles/fonts/THSarabunIT๙ Bold.ttf'; // Set font file path

	$name = 'พิมพ์ชนก วงศ์นคร';
	$namewithprename = 'นางสาวพิมพ์ชนก วงศ์นคร';
	$school = 'โรงเรียนเสาไห้ วิมลวิทยานุกูล';
	$date_cer = '๒ กรกฎาคม ๒๕๖๓'; // Set certicate date in Thai
	$basic_score_cer_code = 'คดผ.630000002';

	$font_size = 40; //Set font size
	$angle = 0; //Set angle
	/* Set x-position on certificate for name */
	$dimen4name = imagettfbbox($font_size, $angle, $font_path, $namewithprename);
	$text4name = (abs($dimen4name[4] - $dimen4name[0]))/2;
	$x4name = 758 - $text4name;
	/* Set x-position on certificate for name */
	$dimen4school = imagettfbbox($font_size, $angle, $font_path, $school);
	$text4school = (abs($dimen4school[4] - $dimen4school[0]))/2;
	$x4school = 758 - $text4school;
	/* Print firstname, lastname, school and date on certificate */
	imagettftext($jpg_image, $font_size, $angle, $x4name, 402, $font_color, $font_path, $namewithprename);
	imagettftext($jpg_image, $font_size, $angle, $x4school, 465, $font_color, $font_path, $school);
	imagettftext($jpg_image, $font_size, $angle, 690, 730, $font_color, $font_path, $date_cer);
	imagettftext($jpg_image, 30, $angle, 1150, 150, $font_color, $font_path, $basic_score_cer_code);

	$target_dir = 'images/เกียรติบัตร/พื้นฐาน/ครู/'; // Set target_directory

	if(!is_dir($target_dir)) { // if there's not folder in target_directory
		mkdir($target_dir); // Create folder name is today_date
	}

	imagejpeg($jpg_image,'images/เกียรติบัตร/พื้นฐาน/ครู/'.$name.'.jpg');// Send Image to Browser or save in directory on client
	imagedestroy($jpg_image); // Clear Memory
	/***********************************************************************************************************************************/
	/****************************************************** End Print & Save เกียรติบัตร **************************************************/
	/***********************************************************************************************************************************/

?>