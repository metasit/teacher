<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start NoAccount Session -->
<?php
      if (empty($_SESSION['email']))
      {
?>
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
          <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
          <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
          <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
            <ul>
              <li><a href="ข่าวสาร008-en.php"> ข่าวสาร</a></li>
              <!--<li><a href="#">Chinese</a></li>-->
            </ul>
          </li>
          <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
        </ul>
      </nav>
      <!-- End NoAccount Session -->
      <!-- ################################################################################################ -->
      <!-- Start Admin Session -->
<?php
      }
      elseif ($_SESSION['email'])
      {
        if ($_SESSION["level"]=="admin")
        {
?>
        <nav id="mainav2" class="PreMenu_fl_right">
          <ul class="PreMenufaico2">
            <li style="background-color:rgb(228,0,0);"><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="ข่าวสาร008-en.php"> ข่าวสาร</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Admin Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member Session -->
<?php
        if ($_SESSION["level"]=="memberGeneral")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> สมาชิกทั่วไป</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="ข่าวสาร008-en.php"> ข่าวสาร</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member SILVER Session -->
<?php
        if ($_SESSION["level"]=="memberSilver")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="ข่าวสาร008-en.php"> ข่าวสาร</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member SILVER Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member GOLD Session -->
<?php
        if ($_SESSION["level"]=="memberGold")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="ข่าวสาร008-en.php"> ข่าวสาร</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member GOLD Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member DIAMOND Session -->
<?php
        if ($_SESSION["level"]=="memberDiamond")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="ข่าวสาร008-en.php"> ข่าวสาร</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
      }
?>
      <!-- End Member DIAMOND Session -->
      <!-- ################################################################################################ -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px; ">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li>
            </ul>
          </li>
          
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="ข่าวสารรวมlatest.php"> ข่าวสาร</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="ข่าวสาร008.php"> อบรม “อาสาของแผ่นดิน” สาขาสารสนเทศ รุ่นที่ 1</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - ข่าวสาร008 -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <article style="text-align:center; border-bottom:3px solid #59A209; padding-bottom:50px;" >
        <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px"><strong>อบรม “อาสาของแผ่นดิน” สาขาสารสนเทศ รุ่นที่ 1</strong></p>
        <p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
          <strong>เมื่อเวลาและโอกาสของคุณมาถึง คุณจะได้เป็นส่วนหนึ่งในการแสดงศักยภาพ เพื่อพัฒนาวงการศึกษาไทยให้ยั่งยืน ด้วยสองมือของเหล่าจิตอาสาของแผ่นดิน</strong>
          <img src="images/รูปข่าว007_01.jpg" alt="รูปข่าว007_01">
          <br><br>
          วันที่ 13 – 14 กุมภาพันธ์ 2563 มูลนิธิครูดีของแผ่นดิน ได้จัดอบรมอาสาของแผ่นดิน สาขาสารสนเทศ รุ่นที่ 1 ครั้งที่  1 จำนวน 10 ท่าน ณ มูลนิธิครูดีของแผ่นดิน และโรงเรียนพญาไท กรุงเทพ 
          โดยมีครูเก๋ จารุณี สินชัยโรจน์กุล Google Certified Innovator / Trainer  และพี่เต้ ศุภชัย แซ่อือ ผู้ดูแลระบบ Skillmeo จากบริษัททรินนิเทค จำกัด เป็นวิทยากร
          <br><br>
          บรรยากาศการอบรมเต็มเปี่ยมไปด้วยรอยยิ้ม มิตรภาพ และน้ำใจของผู้อบรมทั้ง 10 ท่าน ทุกท่านต่างมาด้วยหัวใจแห่งจิตอาสาอย่างแท้จริง หลังจากอบรมในวันแรกเป็นที่เรียบร้อย อาสาของแผ่นดิน สาขาสารสนเทศ 
          ก็ทำหน้าที่ทันที่ในวันที่ 14 กุมภาพันธ์ 2563 ณ โรงเรียนพญาไท โดยการเป็นผู้ช่วยวิทยากรในงานสร้างเสริมการใช้เทคโนโลยีดิจิทัลอย่างสร้างสรรค์ปลอดภัย ผ่านหลักสูตร Be Internet Awesome by Google
          <img src="images/รูปข่าว007_02.jpg" alt="รูปข่าว007_02">
          <br><br>
          หลังจากการอบรมและการทำหน้าที่ในวันที่ 13 และ 14 กุมภาพันธ์ที่ผ่านมาแล้ว อาสาของแผ่นดิน สาขาสารสนเทศจะได้ทำหน้าที่คัดกรองและจัดทำข้อมูลด้านสารสนเทศของผู้บริหารและคุณครูที่เข้าร่วมโครงการ
          เครือข่ายครูดีของแผ่นดิน และโครงการที่เกี่ยวข้อง ให้เป็นไปอย่างมีระบบและได้มาตรฐานต่อไป
        </p>
    </article>
    <p class="font-x3"><span style="color:rgb(180,147,31); line-height:30px;"><strong>คลังภาพ</strong></span></p>
    <div id="gallery" style="margin-top:50px">
      <ul class="nospace clear">
        <li class="one_third first zoom11"><a href="images/รูปข่าวอบรมสารสนเทศ-03.jpg"><img src="images/รูปข่าวอบรมสารสนเทศ-03.jpg" alt="รูปข่าวอบรมสารสนเทศ-03"></a></li>
        <li class="one_third zoom11"><a href="images/รูปข่าวอบรมสารสนเทศ-04.jpg"><img src="images/รูปข่าวอบรมสารสนเทศ-04.jpg" alt="รูปข่าวอบรมสารสนเทศ-04"></a></li>
        <li class="one_third zoom11"><a href="images/รูปข่าวอบรมสารสนเทศ-05.jpg"><img src="images/รูปข่าวอบรมสารสนเทศ-05.jpg" alt="รูปข่าวอบรมสารสนเทศ-05"></a></li>
        <li class="one_third first zoom11"><a href="images/รูปข่าวอบรมสารสนเทศ-06.jpg"><img src="images/รูปข่าวอบรมสารสนเทศ-06.jpg" alt="รูปข่าวอบรมสารสนเทศ-06"></a></li>
        <li class="one_third zoom11"><a href="images/รูปข่าวอบรมสารสนเทศ-07.jpg"><img src="images/รูปข่าวอบรมสารสนเทศ-07.jpg" alt="รูปข่าวอบรมสารสนเทศ-07"></a></li>
        <li class="one_third zoom11"><a href="images/รูปข่าวอบรมสารสนเทศ-08.jpg"><img src="images/รูปข่าวอบรมสารสนเทศ-08.jpg" alt="รูปข่าวอบรมสารสนเทศ-08"></a></li>
      </ul>
    </div>
  </main>
</div>
<!-- End Content 04 - ข่าวสาร008 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>