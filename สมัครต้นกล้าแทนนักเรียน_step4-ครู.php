<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if($_POST['CFP'] == 'สมัครต้นกล้าแทนนักเรียน_step3-ครู') {
			/* Set prename */
			$prename_stu = $_POST['prename_stu'];
			/* Set firstname */
			$firstname_stu = $_POST['firstname_stu'];
			/* Set lastname */
			$lastname_stu = $_POST['lastname_stu'];
			/* Set birthdate */
			$birthdate_stu = $_POST['birthdate_stu'];
			/* Set special child btn */
			$spe_child_btn = $_POST['spe_child_btn'];
			/* Set pomain_id */
			$pomain_id = $_POST['pomain_id'];
			/* Set ID_stu */
			$ID_stu = $_POST['ID_stu'];
			/* Set affil_code */
			$affil_code = $_SESSION['affil_code'];
			/* Set affil_name */
			$affil_name = $_SESSION['affil_name'];

			$sql = "UPDATE `login` SET affil_code='$affil_code', affil_name='$affil_name' WHERE ID='$ID_stu' ";
			$re = $con->query($sql) or die($con->error);
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* Log User Action */
			$scorelog_task = 'สมัครต้นกล้าแทนนักเรียน_step3';
			$scorelog_detail = 'ครู,ชั้น5,เด็ก,ขั้นพื้นฐาน,'.$affil_code;
			$scorelog_total = $ID_stu;
			$sqllog = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail, scorelog_total) VALUES ('$ID', '$scorelog_task', '$scorelog_detail', '$scorelog_total') ";
			$relog = $con->query($sqllog) or die($con->error);

		}else{

			echo "<script>window.history.go(-1)</script>";

		}

	}else{

		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';

	}
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/เด็กดีของแผ่นดิน Logo.jpg" style="width:70%" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<span class="login100-form-title">
					ขั้นตอนสุดท้าย: หัวข้อที่จะพัฒนาตัวเอง
				</span>
				<!-- ################################################################################################ -->
				<!-- Start show step -->
				<div class="login100-form validate-form">
					<div class="center">
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
					</div>
					<div class="right">
						<p style="color:rgb(94,177,26);">เลือกหัวข้อ</p>
					</div>
				</div>
				<!-- End show step -->
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" id="Affform" action="addZidol_A4tea.php" method="POST" enctype="multipart/form-data">
					<!-- ################################################################################################ -->
					<label for="teacher_id" id="teacherhead" class="center m-t-10 text-gray bold">ชื่อคุณครูที่ปรึกษา</label>
					<select name="teacher_ID" id="teacher_id" class="form-control" style="height:40px;">
						<option value="" disabled="disabled" selected="selected">ไม่สามารถแก้ไขได้</option>
						<?php
							$name = $_SESSION['firstname'].' '.$_SESSION['lastname']; ?>
							<option selected="selected"><?php echo $name; ?></option> <?php
						?>
					</select>
					<!-- ################################################################################################ -->
					<label class="center m-t-15 p-t-15 text-black bold" style="border-top:rgb(72,160,0) solid 2px; border-radius:20px;">สิ่งที่ต้องการจะแก้ไข/พัฒนาในโครงการเด็กดีขั้นพื้นฐาน</label>
					<label class="center m-t-10 text-black bold">หัวข้อหลัก</label>
					<select name="devmain" id="devmain_id" class="form-control" style="height:40px;" required>
						<option value="" disabled="disabled" selected="selected">เลือกหัวข้อหลัก</option>
						<option value="A"><strong>การรู้จัก/จัดการตนเอง</strong> เช่น การรู้จักเป้าหมายชีวิต, การบริหารเวลา, การจัดการอารมณ์, การฝึกสมาธิ, การบริหารการเงิน, การจัดของให้สะอาดเป็นระเบียบ</option>
						<option value="B"><strong>การเข้าใจผู้อื่น</strong> เช่น การทำงานร่วมกับผู้อื่นอย่างมีความสุข, การคบเพื่อน, การดูแลพ่อแม่ และผู้ใหญ่</option>
						<option value="C"><strong>การเข้าสังคม</strong> เช่น มารยาทและการวางตัว, การช่วยเหลือผู้อื่น, การมีจิตอาสา</option>
						<option value="D"><strong>การสื่อสารและการสร้างแรงบันดาลใจ</strong> เช่น การพูด, ความเป็นผู้นำ, การกล้าแสดงออก</option>
					</select>
					<label class="center m-t-10 text-black bold">หัวข้อย่อย</label>
					<div class="wrap-input100 m-t-10">
						<input class="input100" name="devsub" id="devsub_id" type="text" class="form-control" placeholder="หัวข้อย่อยที่ต้องการจะแก้ไข/พัฒนาจะตามตัวอย่างหรือกำหนดเองก็ได้" required/>
						<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
					</div>
					<label class="center m-t-15 p-t-15 text-black bold" style="border-top:rgb(72,160,0) solid 2px; border-radius:20px;">ระบบการส่งงาน</label>
					<select name="system" id="system_id" class="form-control" style="height:40px;" required>
						<option value="" disabled="disabled" selected="selected">ไม่สามารถแก้ไขได้</option>
						<option value="A" selected="selected">ระบบต้นกล้าแห่งความดี (สำหรับอายุไม่เกิน 9 ปี)</option>
					</select>
					<!-- ################################################################################################ -->
					<div class="container-login100-form-btn m-t-20">
						<a href="สมัครต้นกล้าแทนนักเรียน_step3-ครู.php?
							CFP=สมัครต้นกล้าแทนนักเรียน_step4-ครู
							&pomain_id=<?php echo $pomain_id; ?>
							&ID_stu=<?php echo $ID_stu; ?>
							&prename_stu=<?php echo $prename_stu; ?>
							&firstname_stu=<?php echo $firstname_stu; ?>
							&lastname_stu=<?php echo $lastname_stu; ?>
							&birthdate_stu=<?php echo $birthdate_stu; ?>
							&spe_child_btn=<?php echo $spe_child_btn; ?>" 
							class="login100-form-btn" style="width:30%; margin:0 10px;">ย้อนกลับ
						</a>
						<button class="login100-form-btn" style="width:40%; margin:0 10px;" type="submit" id="register">สมัครโครงการ</button>
					</div>

					<input type="hidden" name="ID_stu" value="<?php echo $ID_stu; ?>">
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>