<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		date_default_timezone_set("Asia/Bangkok");

		$ID_sup = $_GET['ID_sup'];
		$sup_name = $_GET['sup_name'];
		$sup_email = $_GET['sup_email'];

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
	<style>
		.price-pr p{
			font-family: RSUText;
			font-size: 30px;
		}

		/* The Modal (background) */
		.modal {
			display: none; /* Hidden by default */
			position: fixed; /* Stay in place */
			z-index: 5; /* Sit on top */
			padding-top: 250px; /* Location of the box */
			left: 0;
			top: 0;
			width: 100%; /* Full width */
			height: 100%; /* Full height */
			overflow: auto; /* Enable scroll if needed */
			background-color: rgb(0,0,0); /* Fallback color */
			background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
			
		}
		/* Modal Content */
		.modal-content {
			background-color: white;
			margin: auto;
			padding: 20px;
			border: 10px solid rgb(94,177,26);
			border-radius: 10px;
			width: 50%;
		}
		/* The Close Button */
		.close, #close4devtopic {
			color: #aaaaaa;
			float: right;
			font-size: 28px;
			font-weight: bold;
		}
		.close:hover, .close:focus, #close4devtopic:hover, #close4devtopic:focus {
			color: #000;
			text-decoration: none;
			cursor: pointer;
		}
		.center_text_input > input {
			margin: 0 auto;
			margin-left: auto;
			margin-right: auto;
		}
		/***********************************************************************************************************/
		/* Update Status Button Style */
		.cart-box-main .up-stat-sty .up-stat-btn {
			width: 100%;
			text-align: center;
			padding-right: 0;
			border: none;
			cursor: pointer;
		}
		.cart-box-main .up-stat-sty .up-stat-btn .fa-caret-down {
			margin-left: auto;
		}
		.cart-box-main .up-stat-sty .dropdown_thispage {
			display: none;
			position: absolute;
			z-index: 4;
		}
		.cart-box-main .up-stat-sty:hover .dropdown_thispage {
			display: block;
		}
		.cart-box-main .up-stat-sty:hover .dropdown_thispage a {
			background-color: rgba(72,160,0,0.9);
			color: white;
			display: block;
			text-align: initial;
			font-size: 18px;
			padding: 15px 45px;
			margin-left: -45px;
			cursor: pointer;
			border-bottom: 2px solid rgb(2,83,165);
		}
		.cart-box-main .up-stat-sty:hover .dropdown_thispage .reject-btn-sty:hover {
			background-color: rgb(185, 0, 0);
		}
		.cart-box-main .up-stat-sty:hover .dropdown_thispage .approve-btn-sty:hover {
			background-color: rgb(2,83,165);
		}
	</style>
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['winScroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="ระบบหลังบ้านศน.ขั้นเกียรติคุณ.php">ระบบหลังบ้านศึกษานิเทศก์ดีขั้นเกียรติคุณ</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li class="linkst"><a href="#" onclick="return false">รายงานการนิเทศ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row" style="margin: 0 20px; overflow-x:auto;">
			<div class="col-lg-12 m-t-40">
				<div class="table-main table-responsive" style="margin-bottom:200px">
					<!-- ################################################################################################ -->
					<p class="fs-20 m-l-30"><strong>ชื่อศึกษานิเทศก์:</strong> <?php echo $sup_name.' ('.$sup_email; ?>)</p>
					<table class="table" style="background-color: rgb(240,240,240);">
						<colgroup>
							<col>
							<col width="120">
							<col span="2">
							<col width="250">
							<col span="4">
						</colgroup>
						<thead>
							<tr>
								<th rowspan="2">ลำดับ</th>
								<th colspan="4">ข้อมูลผู้นิเทศ</th>
								<th class="BG-blue1" colspan="3">ข้อมูลผู้ถูกนิเทศ</th>
								<th rowspan="2">สถานะการตรวจ</th>
								<th class="BG-gold1" rowspan="2">อัพเดทสถานะ</th>
							</tr>
							<tr>
								<th>วันที่-เวลา</th>
								<th>สถานที่</th>
								<th>หลักฐาน</th>
								<th>รายงาน</th>
								<th class="BG-blue1">ชื่อ-นามสกุล</th>
								<th class="BG-blue1">ตำแหน่ง</th>
								<th class="BG-blue1">สังกัด/โรงเรียน</th>
							</tr>
						</thead>
						<tbody class="disable-margin">
							<?php
								$sqlhonor_score4sup_codeB = "SELECT * FROM `honor_score4sup` WHERE ID='$ID_sup' AND honor_score4sup_code LIKE 'B%' ";
								$reHS4SCB = mysqli_query($con, $sqlhonor_score4sup_codeB);
								
								$n=1;
								while($rowHS4SCB = $reHS4SCB->fetch_assoc()) { ?>
									<tr>
										<!-- ลำดับ -->
										<td class="price-pr">
											<p><?php echo $n; ?></p>
										</td>
										<!-- วันที่-เวลา ที่นิเทศ -->
										<td class="price-pr">
											<p class="font"><?php echo date("d-m-Y", strtotime($rowHS4SCB['honor_score4sup_date'])).'<br>'.date("H:i", strtotime($rowHS4SCB['honor_score4sup_date'])); ?></p>
										</td>
										<!-- สถานที่ ที่นิเทศก์ -->
										<td class="price-pr">
											<?php
												$affil_code = substr($rowHS4SCB['honor_score4sup_data1'], 0, -1);
												include('includes/convert2afftext.php');
											?>
											<p class="fs-10"><?php if($full_affiliation_name != '') {echo $full_affiliation_name;}else{echo '<strong>รหัสสังกัด:</strong> '.$affil_code;} ?></p>
										</td>
										<!-- หลักฐาน ที่นิเทศก์ -->
										<td class="price-btn">
											<a class="btn" href="<?php echo $rowHS4SCB['honor_score4sup_data2']; ?>" target="_blank">ดู</a>
										</td>
										<!-- รายงาน การนิเทศ -->
										<td class="price-pr">
											<p><?php echo $rowHS4SCB['honor_score4sup_data6']; ?></p>
										</td>
										<!-- ชื่อ-นามสกุล ผู้ถูกนิเทศก์ -->
										<td class="price-pr">
											<p class="bold"><?php echo trim($rowHS4SCB['honor_score4sup_data3']); ?></p>
										</td>
										<!-- ตำแหน่ง ผู้ถูกนิเทศก์ -->
										<td class="price-pr">
											<?php
												$occup_code = substr($rowHS4SCB['honor_score4sup_data4'], 0, -1);
												$occupation_id = substr($occup_code, 0, 3);
												$pomain_id = substr($occup_code, 3, 1);
												$posub1_id = substr($occup_code, 4, 1);
												$posub2_id = substr($occup_code, 5, 1);
												include('includes/convert2occtext.php');
											?>
											<p class="fs-10"><?php echo $full_occupation_name; ?></p>
										</td>
										<!-- สังกัด,โรงเรียน ผู้ถูกนิเทศก์ -->
										<td class="price-pr">
											<?php
												$affil_code = substr($rowHS4SCB['honor_score4sup_data5'], 0, -1);
												include('includes/convert2afftext.php');
											?>
											<p class="fs-10"><?php if($full_affiliation_name != '') {echo $full_affiliation_name;}else{echo '<strong>รหัสสังกัด:</strong> '.$affil_code;} ?></p>
										</td>
										<!-- สถานะการตรวจ -->
										<td class="price-pr bold">
											<?php
												if(is_null($rowHS4SCB['honor_score4sup_check_status']) || empty($rowHS4SCB['honor_score4sup_check_status'])) { ?>
													<p>รอตรวจ</p> <?php
												}elseif(strpos($rowHS4SCB['honor_score4sup_check_status'], 'ปฏิเสธ,') !== false) { ?>
													<p class="text-red1"><?php echo str_replace(',', '<br>', $rowHS4SCB['honor_score4sup_check_status']); ?></p> <?php
												}elseif(strpos($rowHS4SCB['honor_score4sup_check_status'], 'อนุมัติ,') !== false) { ?>
													<p class="text-green1"><?php echo str_replace(',', '<br>', $rowHS4SCB['honor_score4sup_check_status']); ?> <i class="fas fa-check-circle"></i></p> <?php
												}elseif(strpos($rowHS4SCB['honor_score4sup_check_status'], 'แก้ไข') !== false) { ?>
													<p class="text-yellow2"><?php echo str_replace(',', '<br>', $rowHS4SCB['honor_score4sup_check_status']); ?></p> <?php
												}
											?>
										</td>
										<!-- ช่องตรวจรายงานการนิเทศก์ -->
										<td>
											<div class="up-stat-sty">
												<button class="up-stat-btn"><i class="fas fa-caret-down"></i></button>
													<div class="dropdown_thispage">
														<!-- ################################################################################################ -->
														<!-- Trigger/Open The Modal -->
														<?php
															if(strpos($rowHS4SCB['honor_score4sup_check_status'], 'อนุมัติ,') !== false) { ?>
																<a class="reject-btn-sty" onclick="return false" style="background-color: rgb(2,83,165); cursor: no-drop;">ปฏิเสธ</a>
																<a class="approve-btn-sty" style="background-color: rgb(2,83,165); cursor: no-drop;">อนุมัติ</a> <?php

															}else{ ?>
																<a id="rejectBtn" class="reject-btn-sty">ปฏิเสธ</a>
																<a onclick="approveBtn('<?php echo $ID_sup; ?>', '<?php echo $sup_name; ?>', '<?php echo $sup_email; ?>', '<?php echo $rowHS4SCB['honor_score4sup_code']; ?>', '<?php echo $rowHS4SCB['honor_score4sup_id'] ?>') "
																	class="approve-btn-sty">อนุมัติ
																</a> <?php

															}
														?>
														<!-- ################################################################################################ -->
													</div>
													<!-- The Reject Modal for  (Need to move out from dropdown_thispage class's child because if inside, the modal will get hover when move out but we want reject modal window stay in place) -->
													<?php
													if(strpos($rowHS4SCB['honor_score4sup_check_status'], 'อนุมัติ,') === false) {

														if($m == ''){$m = 0;} ?>
															<div id="rejectModal" class="modal">
																<div class="modal-content">
																	<span class="close">&times;</span>
																	<p class="fs-18"><strong>เหตุผลในการปฏิเสธ</strong> (เหตุผลตรงนี้จะนำไปแสดงให้ศึกษานิเทศก์แก้ไขได้ถูกต้อง)</p>
																	<!-- ################################################################################################ -->
																	<form id="rejectMoalForm" action="rejecthonor_score4sup-admin.php" method="POST">
																		<div class="center_text_input fs-18 m-t-20 m-b-20">
																			<input type="text" name="reject_reason" placeholder="เหตุผลในการปฏิเสธ" style="width: 70%; height: 40px; padding: 0 8px" required>
																		</div>

																		<button class="submitBtn btn BG-green1 fs-15" style="cursor: pointer; border: transparent;">ยืนยัน</button>
																		<button class="cancelBtn btn BG-red1 fs-15" type="reset" style="cursor: pointer; border: transparent;">ยกเลิก</button>

																		<input type="hidden" name="ID_sup" value="<?php echo $ID_sup; ?>">
																		<input type="hidden" name="sup_name" value="<?php echo $sup_name; ?>">
																		<input type="hidden" name="sup_email" value="<?php echo $sup_email; ?>">
																		<input type="hidden" name="honor_score4sup_code" value="<?php echo $rowHS4SCB['honor_score4sup_code']; ?>">
																		<input id="winScroll_input" type="hidden" name="winScroll">
																		<input type="hidden" name="honor_score4sup_id" value="<?php echo $rowHS4SCB['honor_score4sup_id'] ?>">
																	</form>
																	<!-- ################################################################################################ -->
																</div>
															</div> <?php
														
													} ?>
											</div>
										</td>

										<!-- JS for rejectModal box -->
											<?php
												if(strpos($rowHS4SCB['honor_score4sup_check_status'], 'อนุมัติ,') === false) { ?>

													<script>
														// Get the rejectModal
														var rejectModal = document.querySelectorAll("#rejectModal");
														// Get the button that opens the rejectModal
														var btn = document.querySelectorAll("#rejectBtn");
														// Get the <span> element that closes the rejectModal
														var closeBtn = document.getElementsByClassName("close")[<?php echo $m; ?>];
														// Get the button that submit reason for rejectModal
														var cancelBtn = document.getElementsByClassName("cancelBtn")[<?php echo $m; ?>];
														// Get the button that submit reason for rejectModal
														var submitBtn = document.getElementsByClassName("submitBtn")[<?php echo $m; ?>];
														// Get the input winScroll
														var winScroll_input = document.querySelectorAll("#winScroll_input");
														// Get the dropdown_thispage
														// var dropdown_thispage = document.getElementsByClassName("dropdown_thispage")[<?php echo $m; ?>];
														// Get rejectModal form
														var rejectMoalForm = document.querySelectorAll("#rejectMoalForm");
														///////////////////////////////////////////////////////////////////////////////////////////////////////////
														// When the user clicks the button, open the rejectModal
														btn[<?php echo $m; ?>].onclick = function() {
															//dropdown_thispage[<?php echo $m; ?>].style.backgroundColor = "yellow";
															rejectModal[<?php echo $m; ?>].style.display = "block";
														}
														// When the user clicks on <closeBtn> (x), close the rejectModal
														closeBtn.onclick = function() {
															rejectModal[<?php echo $m; ?>].style.display = "none";
														}
														// When the user clicks on <cancelBtn>, close the rejectModal
														cancelBtn.onclick = function() {
															rejectModal[<?php echo $m; ?>].style.display = "none";
														}
														// When the user clicks on ยืนยัน button on rejectModal form
														submitBtn.onmouseover = function() {
															var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
															winScroll_input[<?php echo $m; ?>].value = winScroll;
														}
														// When the user clicks on อนุมัติ button
														function approveBtn(a, b, c, d, e) {
															var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
															window.location.href = "approvehonor_score4sup-admin.php?winScroll="+winScroll + "&ID_sup="+a + "&sup_name="+b + "&sup_email="+c +
															"&honor_score4sup_code="+d + "&honor_score4sup_id="+e;
														}
														
													</script> <?php
													
													$m++;
												}
											?>
									</tr> <?php
									$n++;
								}
							?>		
						</tbody>
					</table>
					<!-- ################################################################################################ -->
				</div>
			</div>
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<!-- JS for getWinScroll -->
<script src="js/getWinScroll_changeStatusBtn.js"></script>

</body>
</html>