<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	$name = $_SESSION['firstname'].' '.$_SESSION['lastname'];
	//Set date
	$sqladscorelog = "SELECT * FROM `adscorelog` WHERE ID_user='$ID' AND adscorelog_task='Approve แล้ว' AND adscorelog_detail='ขั้นพื้นฐาน' ORDER BY adscorelog_id DESC LIMIT 1 ";
	$readscorelog = mysqli_query($con,$sqladscorelog);
	$rowadscorelog = mysqli_fetch_array($readscorelog);
	$date = new DateTime($rowadscorelog['adscorelog_date']);
	$date = $date->format("Y-m-d"); // Set date format
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าโครงการศึกษานิเทศก์ดีของแผ่นดิน</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<span class="login100-form-title">
					ยินดีด้วยค่ะ คุณผ่านการประเมินขั้นพื้นฐาน
				</span>
				<!-- ################################################################################################ -->
				<!-- Start show step -->
				<div class="login100-form validate-form">
					<div class="center">
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
					</div>
					<div class="right">
						<p style="color:rgb(94,177,26);">การยืนยันเสร็จสมบูรณ์</p>
					</div>
				</div>
				<!-- End show step -->
				<!-- ################################################################################################ -->
				<span class="login100-form-title fs-30 lh-1-1">
					<?php if($_SESSION['level']=="memberGeneral") { ?>
									เนื่องจากคุณเป็นสมาชิกระดับทั่วไป
									<br>
									คุณสามารถพิมพ์เกียรติบัตรแบบธรรมดาได้ค่ะ
									<br>
									<a href="<?php echo 'images/เกียรติบัตร/พื้นฐาน/ศน/'.$date.'/'.$name.'.jpg'; ?>" target="_blank" class="btn2">แบบออนไลน์</a>
									<br><br>
									คุณสามารถได้รับเกียรติบัตรแบบพิเศษ โดยการเป็นสมาชิกระดับเงิน, ทอง หรือ เพชร <a href="บำรุงค่าสมาชิก.php" target="_blank" class="readall fs-18">ได้ที่นี้</a>
									<br>
									<!--
									<a href="images/เกียรติบัตร/พื้นฐาน/ศน/เกียรติบัตร ปี62.jpg" target="_blank" class="btn2">ตัวอย่างแบบพิเศษ ปี62</a>
									-->
					<?php }elseif($_SESSION['level']=="admin" || $_SESSION['level']=="memberSilver" || $_SESSION['level']=="memberGold" || $_SESSION['level']=="memberDiamond") { ?>
									เนื่องจากคุณเป็นสมาชิกระดับพิเศษ
									<br>
									ยินดีด้วยค่ะ นอกจากคุณจะสามารถพิมพ์เกียรติบัตรออนไลน์ได้เองแล้ว
									<br>
									คุณได้รับเกียรติบัตรแบบพิเศษ ทางมูลนิธิจะส่งไปให้ตามที่อยู่ที่ให้ไว้ตอนสมัครสมาชิกค่ะ
									<br>
									<a href="<?php echo 'images/เกียรติบัตร/พื้นฐาน/ศน/'.$date.'/'.$name.'.jpg'; ?>" target="_blank" class="btn2">แบบออนไลน์</a>
									<!--
									<a href="images/เกียรติบัตร/พื้นฐาน/ศน/เกียรติบัตร ปี62.jpg" target="_blank" class="btn2">ตัวอย่างแบบพิเศษ ปี62</a>

					-->
					<?php }else{
									header("Location: javascript:history.go(-1);");
								} ?>
				</span>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>