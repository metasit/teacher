<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$now_date = date("d-m-Y H:i:s");

	$ID_user = $_POST['ID_stu']; // student ID

	/* Set all value */
	$winScroll = $_POST['winScroll'];
	$reject_reason = $_POST['reject_reason'];
	$devsub_name = $_POST['devsub_name'];

	$basic_score4stu_system_id = 'C'; // Set basic_score4stu_system_id to save to database
	$basic_score4stu_text = $devsub_name;
	$basic_score4stu_file_date = date("Y-m-d H:i:s");
	$basic_score4stu_check_status = 'ปฏิเสธ,'.$reject_reason.','.$now_date;

	/* Check ว่ามี basic_score4stu_id แล้วหรือไม่ ถ้ามีแล้ว ให้ Update แต่ถ้ายังให้ INSERT */
	$sqlbasic_score4stu_check_C = "SELECT * FROM `basic_score4stu` WHERE ID='$ID_user' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
	$reBS4SCC = $con->query($sqlbasic_score4stu_check_C) or die($con->error); //Check error

	if(mysqli_num_rows($reBS4SCC) == 0) { // first time click to update devtopic
		$sql = "INSERT INTO `basic_score4stu` (ID, basic_score4stu_system_id, basic_score4stu_check_status) VALUES ('$ID_user', '$basic_score4stu_system_id', '$basic_score4stu_check_status') ";
		$res= $con->query($sql) or die($con->error); //Check error
	}else{
		$sql = "UPDATE `basic_score4stu` SET `basic_score4stu_check_status`='$basic_score4stu_check_status' WHERE ID='$ID_user' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
		$res= $con->query($sql) or die($con->error); //Check error
	}

	/* Log Admin Action */
	$ID_ad = $_SESSION['ID']; // admin ID
	$adscorelog_task = 'ตรวจความดีเด็กพื้นฐาน,C';
	$ID_user = $_POST['ID_stu'];
	$adscorelog_detail = 'เด็ก,ขั้นพื้นฐาน,'.$basic_score4stu_check_status;

	$sqllog ="INSERT INTO `adscorelog` (ID_ad, adscorelog_task, ID_user, adscorelog_detail) VALUES ('$ID_ad','$adscorelog_task',$ID_user,'$adscorelog_detail') ";
	$relog = $con->query($sqllog) or die($con->error); //Check error




	$system_id = $_POST['basic_score4stu_system_id']; // Set system_id to pass valuable to another page
	header('location: รายละเอียดนักเรียนทำความดี-พื้นฐาน-ไม่มีครูที่ปรึกษา-admin.php?winScroll='.$winScroll.'&ID_stu='.$ID_user.'&system_id='.$system_id);
?>