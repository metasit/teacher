<?php
//คำสั่ง connect db เขียนเพิ่มเองนะ
$strExcelFileName="การประเมินศนดี.xls";
header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");
session_start();
require_once('condb.php');
$sql_list  ="
    SELECT * From login as lg 
    LEFT JOIN honor_score4sup as hs  ON  lg.ID = hs.ID    
    WHERE  hs.honor_score4sup_code  = 'A' AND  lg.occup_code like 'OcAD%'
";
$list = $con->query($sql_list) ;
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<script type="text/javascript">
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var allscore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "ครองตน", value: <?php echo $selfscore_avg; ?>}, 
				{axis: "ครองคน", value: <?php echo $peoplescore_avg; ?>}, 
				{axis: "ครองงาน", value: <?php echo $workscore_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var selfscore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "<?php echo $selfscore_sub1_topic; ?>", value: <?php echo $selfscore_sub1_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub2_topic; ?>", value: <?php echo $selfscore_sub2_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub3_topic; ?>", value: <?php echo $selfscore_sub3_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub4_topic; ?>", value: <?php echo $selfscore_sub4_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub5_topic; ?>", value: <?php echo $selfscore_sub5_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub6_topic; ?>", value: <?php echo $selfscore_sub6_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub7_topic; ?>", value: <?php echo $selfscore_sub7_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub8_topic; ?>", value: <?php echo $selfscore_sub8_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub9_topic; ?>", value: <?php echo $selfscore_sub9_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var peoplescore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "<?php echo $peoplescore_sub10_topic; ?>", value: <?php echo $peoplescore_sub10_avg; ?>}, 
				{axis: "<?php echo $peoplescore_sub11_topic; ?>", value: <?php echo $peoplescore_sub11_avg; ?>}, 
				{axis: "<?php echo $peoplescore_sub12_topic; ?>", value: <?php echo $peoplescore_sub12_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var workscore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "<?php echo $workscore_sub13_topic; ?>", value: <?php echo $workscore_sub13_avg; ?>}, 
				{axis: "<?php echo $workscore_sub14_topic; ?>", value: <?php echo $workscore_sub14_avg; ?>}, 
				{axis: "<?php echo $workscore_sub15_topic; ?>", value: <?php echo $workscore_sub15_avg; ?>}, 
				{axis: "<?php echo $workscore_sub16_topic; ?>", value: <?php echo $workscore_sub16_avg; ?>}, 
				{axis: "<?php echo $workscore_sub17_topic; ?>", value: <?php echo $workscore_sub17_avg; ?>}, 
				{axis: "<?php echo $workscore_sub18_topic; ?>", value: <?php echo $workscore_sub18_avg; ?>}, 
				{axis: "<?php echo $workscore_sub19_topic; ?>", value: <?php echo $workscore_sub19_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
</script>


<br>
<div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
<table x:str border=1 cellpadding=0 cellspacing=1 width=100% style="border-collapse:collapse">
    <tr>
        <td style="text-align:center;">คำนำหน้านาม</td>
        <td style="text-align:center;">ชื่อ</td>
        <td style="text-align:center;">นามสกุล</td>
        <td style="text-align:center;">อายุ</td>
        <td style="text-align:center;">สังกัดหลัก</td>
        <td style="text-align:center;">สังกัดย่อย</td>
        <!-- <td>จังหวัด</td> -->
        <td style="text-align:center;" colspan="2">ขั้นพื้นฐาน	</td>
        <td style="text-align:center;" colspan="1">ผ่านระดับเกียรติคุณหรือไม่	</td>
        <td style="text-align:center;" colspan="12">ระดับเกียรติคุณ</td>
        <td style="text-align:center;" >คะแนนครองตน</td>
        <td style="text-align:center;" >คะแนนครองคน</td>
        <td style="text-align:center;" >คะแนนครองงาน</td>
        <!-- <td style="text-align:center;" colspan="12">ระดับเกียรติคุณ</td> -->
        <td style="text-align:center;" >คะแนนรวมทั้ง 12 ตัวบ่งชี้</td>
    </tr>

    <?php 
        while($row = $list->fetch_assoc()) {
            $ID = $row['ID'];
            $selfscore_sub1_avg = 0;
            $selfscore_sub2_avg = 0;
            $selfscore_sub3_avg = 0;
            $selfscore_sub4_avg = 0;
            $selfscore_sub5_avg = 0;
            $selfscore_sub6_avg = 0;
            $selfscore_sub7_avg = 0;
            $selfscore_sub8_avg = 0;
            $selfscore_sub9_avg = 0;
            $workscore_sub10_avg = 0;
            $selfscore_sub11_avg = 0;
            $selfscore_sub12_avg = 0;
            include('includes/calhonor_score-ศน.php'); 
            //include('includes/calfifth_score-ครู.php');
       
    ?>
    <tr>
        <td width="94" height="30" align="center" valign="middle" >
            <?php 
            // echo $row['prename_remark']
             ?>
        </td>
        <td width="200" align="center" valign="middle" >
            <strong>
                <?php
                //  echo $row['firstname'];
                echo $workscore_sub10_avg;
                  ?>

            </strong>
        </td>
        <td width="181" align="center" valign="middle" >
            <strong><?php
            //  echo  $row['lastname'];
              ?></strong>
        </td>
        <td width="181" align="center" valign="middle" >
            <strong>
                <?php
                    //    $age =  2021 - explode("-",$row['birthdate'])[0];
                    //    echo $age;
                ?>
            </strong>
        </td>
        <td width="181" align="center" valign="middle" >
            <strong>
                <?php 
                //  echo $row['affil_name'];
                 ?>
            </strong>
        </td>
        <td width="185" align="center" valign="middle" >
            <strong>
                <?php 
                //  echo $row['affil_name']
                  ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php 
                //  echo $row['basic_score_status']
                  ?>
            </strong>
        </td>
        <td width="185" align="center" valign="middle" >
            <?php  
            // echo $row['basic_score_total']
             ?>  
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php 
                //  echo $row['honor_score_status']
                  ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                //  echo $selfscore_sub1_avg
                  ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php 
                //  echo $selfscore_sub2_avg
                  ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php 
                //  echo $selfscore_sub3_avg
                  ?>
            </strong>
        </td>


        <td width="185" align="center" valign="middle" >
            <strong>
                <?php 
                //  echo $selfscore_sub4_avg
                  ?>
            </strong>
        </td>


        <td width="185" align="center" valign="middle" >
            <strong>
                <?php 
                //  echo $selfscore_sub5_avg 
                 ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                // echo $selfscore_sub6_avg;
                 ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                // echo $peoplescore_sub7_avg;
                 ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php
                //   echo $peoplescore_sub8_avg;
                   ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php
                //   echo $peoplescore_sub9_avg;
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php
                //   echo $workscore_sub10_avg; 
                ?>
                <!-- AAAA -->
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php 
                //  echo $workscore_sub11_avg;
                 ?>
            </strong>
        </td>
        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                    // echo $workscore_sub12_avg;
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                // echo $row['honor_score4sup_data2'];
                // if($row['honor_score4sup_data2']){
                //      $cou =  count(explode(",",$row['honor_score4sup_data2']));
                //      $arr_token = explode(",",$row['honor_score4sup_data2']) ;
                //      if($cou==3) {
                //         //echo $cou;
                //         echo $arr_token[0];
                //      }
                // }
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                // if($row['honor_score4sup_data2']){
                //      $cou =  count(explode(",",$row['honor_score4sup_data2']));
                //      $arr_token = explode(",",$row['honor_score4sup_data2']) ;
                //      if($cou==3) {
                //         //  echo $cou;
                //         echo $arr_token[1];
                //      }
                }
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php  
                // if($row['honor_score4sup_data2']){
                //      $cou =  count(explode(",",$row['honor_score4sup_data2']));
                //      $arr_token = explode(",",$row['honor_score4sup_data2']) ;
                //     if($cou==3) {
                //         // echo $cou;
                //         echo $arr_token[2];
                //     }
                // }
                ?>
            </strong>
        </td>

        <td width="185" align="center" valign="middle" >
            <strong>
                <?php
                    // echo 
                    // (float)  $selfscore_sub1_avg+
                    // (float)  $selfscore_sub2_avg+
                    // (float)  $selfscore_sub3_avg+
                    // (float)  $selfscore_sub4_avg+
                    // (float)  $selfscore_sub5_avg+
                    // (float)  $selfscore_sub6_avg+
                    // (float)  $peoplescore_sub7_avg+
                    // (float)  $peoplescore_sub8_avg+
                    // (float)  $peoplescore_sub9_avg+
                    // (float)  $workscore_sub10_avg+
                    // (float)  $workscore_sub11_avg+
                    // (float)  $workscore_sub12_avg
                ?>
            </strong>
        </td>
        <!-- honor_score4sup_data2 -->
    </tr>
      <?php 

    //   $selfscore_sub1_avg = 0;
    //   $selfscore_sub2_avg = 0;
    //   $selfscore_sub3_avg = 0;
    //   $selfscore_sub4_avg = 0;
    //   $selfscore_sub5_avg = 0;
    //   $selfscore_sub6_avg = 0;
    //   $selfscore_sub7_avg = 0;
    //   $selfscore_sub8_avg = 0;
    //   $selfscore_sub9_avg = 0;
    //   $selfscore_sub10_avg = 0;
    //   $selfscore_sub11_avg = 0;
    //   $selfscore_sub12_avg = 0;
    // };
      ?>
</table>
</div>
<script>
    window.onbeforeunload = function(){return false;};
    setTimeout(function(){window.close();}, 10000);
</script>
</body>
</html>