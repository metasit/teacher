<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>
<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="จดหมายรวม001-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="จดหมายรวม001.php">จดหมาย/ประกาศ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 03 - จดหมาย -->
<div style="background-color:rgb(226,255,224);">
  <div class="hoc container clear">
    <table class="table1 table2" style="padding-bottom:20px;">
      <thead>
        <tr>
          <th</th>
          <th></th>
        </tr>
      </thead>
      <!-- ################################################################################################ -->
      <tbody>
        <!-- จดหมาย 003 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย003.php">หนังสือเชิญนำเสนอ "นวัตกรรมสร้างคนดีให้แผ่นดิน" ในเวทีระดับชาติ</td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 002 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย002.php">หนังสือเชิญสำนักงานเขตพื้นที่การศึกษานำร่องเข้าร่วมงานสายสัมพันธ์ 4 ปี เครือข่ายครูดีของแผ่นดิน และ 2 ปีมูลนิธิครูดีของแผ่นดิน</td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 001 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย001.php">หนังสือเชิญศึกษาธิการจังหวัดและสำนักงานเขตพื้นที่การศึกษาเข้าร่วมโครงการเครือข่ายครูดีของแผ่นดิน ปีที่ 2</td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย B -->
        <tr>
          <td><a class="linkfortable" href="จดหมายB.php">ประกาศรางวัลครูดีของแผ่นดิน ชั้นที่ 5 ปี 2562</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย A -->
        <tr>
          <td><a class="linkfortable" href="จดหมายA.php">ประกาศรางวัลครูดีของแผ่นดิน ชั้นที่ 4 ปี 2562</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>        
        <!-- จดหมาย 000 -->
        <tr>
          <td><a class="linkfortable" href="https://drive.google.com/drive/folders/1-bJ2_I2Uti_MCl95kYOOq-iIJQL9twDF">ประกาศรางวัลครูดีของแผ่นดินขั้นพื้นฐาน</td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<!-- End Content 03 - จดหมาย -->
<!-- ################################################################################################ -->
<!-- Start Pagination -->
<div class="wrapper row3 coloured" style="background-color:rgb(226,255,224);">
  <main class="hoc container clear">
    <div class="content">
      <nav class="pagination">
        <ul>
          <li><a href="จดหมายรวมlatest.php">3</a></li>
          <li><a href="จดหมายรวม002.php">2</a></li>
          <li class="current"><strong>1</strong></li>
        </ul>
      </nav>
    </div>
  </main>
</div>
<!-- End Pagination -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>