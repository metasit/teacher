<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
				<?php if (strpos($_SESSION['email'], '@') !== false) {
								if ($_SESSION["level"]=="admin") { //Admin Session ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
											<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*A*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*B*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
										</ul>
									</li>
          <?php }elseif ($_SESSION['level']=='memberGeneral') { //General Member Session ?>
								<li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> General Member</li>
          <?php }elseif ($_SESSION['level']=='memberSilver') { //Member Silver Session ?>
								<li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> Silver Member</li>
          <?php }elseif ($_SESSION['level']=='memberGold') { //Member Gold Session ?>
								<li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> Gold Member</li>
          <?php }elseif ($_SESSION['level']=='memberDiamond') { //Member Diamond Session ?>
								<li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> Diamond Member</li>
          <?php } ?>
								<li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
        <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login-en.php"><i class="fas fa-user-lock"></i> <text1>Log in</text1></a></li>
                <li class="faicon-login"><a href="signup-en.php"><i class="fas fa-file-signature"></i> <text1>Sign up</text1></a></li>
				<?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
                  <ul>
                    <li><a href="ขั้นตอนการสนับสนุนของที่ระลึก.php">ภาษาไทย</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                    <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li class="active"><a href="สนับสนุนของที่ระลึก-en.php">สนับสนุนของที่ระลึก</a></li>
                  </ul>
                </li>
								<li class="faicon-basket"><i class="fas fa-caret-down"></i><i class="fa fa-shopping-basket"></i> <?php echo number_format($price_total,2); ?>
									<ul>
										<li><a href="สนับสนุนของที่ระลึก-en.php" >Shopping Online</a></li>
										<li><a href="ตะกร้า-en.php">View Cart</a></li>
										<li><a href="ตรวจสอบตะกร้า-en.php">Checkout</a></li>
										<li><a href="ประวัติและสถานะการสั่งซื้อ-en.php">Order History and Status</a></li>
									</ul>
								</li>
        </ul>
			</nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index-en.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index-en.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index-en.php">Home</a></li>
          <li><a href="เกี่ยวกับเรา-en.php">About us</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>Join us</a>
            <ul>
              <li><a href="ร่วมโครงการ-en.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <li><a href="โครงการครูดีของแผ่นดิน-en.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา-en.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA-en.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน-en.php">โครงการอาสาของแผ่นดิน</a></li>
            </ul>
          </li>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">Innovation Library</a></li>
          <li><a href="ติดต่อเรา-en.php">Contact us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="สนับสนุนของที่ระลึก.php"> สนับสนุนของที่ระลึก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> ขั้นตอนการสนับสนุนของที่ระลึกมูลนิธิครูดีของแผ่นดิน</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - วิธีการสนับสนุนมูลนิธิ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <article class="font-x2plus left">
			<!-- ################################################################################################ -->
			<section class="text-gold center" style="line-height:80px;">
				<h7>ขั้นตอนการสนับสนุนของที่ระลึกมูลนิธิครูดีของแผ่นดิน</h7><br>
      </section>
			<!-- ################################################################################################ -->
			<dl class="fs-32 font-RSUText lh-1-3">
				<dt>ขั้นที่ 1 เข้าระบบสมาชิก</dt>
        	<dd>1.2 สำหรับท่าน ที่ยังไม่ได้เป็นสมาชิก ท่านสามารถสมัครได้โดยไม่เสียค่าจ่ายใดๆ เมื่อท่านสมัครแล้ว ท่านจะได้รับสิทธิประโยชน์เพิ่มเติมจากการเป็นสมาชิก </strong></dd>
				<dt class="m-t-15">ขั้นที่ 2 กดที่เมนูสนับสนุนของที่ระลึก </dt>
				<dt class="m-t-15">ขั้นที่ 3 ท่านเลือก ของที่ระลึก และ จำนวนที่ท่านต้องการ</dt>
				<dt class="m-t-15">ขั้นที่ 4 กดยืนยันการสั่งซื้อ (ในขั้นตอนที่ 4 ท่านจะได้รับ E- Mail ยืนยันการสั่งซื้อสินค้า)</dt>
				<dt class="m-t-15">ขั้นที่ 5  ท่านสามารถชำระเงินการสั่งชื้อ ได้ 3 ช่องทาง ดังนี้</dt>
					<dd>5.1 เงินสด นำฝากเข้า ชื่อบัญชี มูลนิธิครูดีของแผ่นดิน ธนาคารกรุงไทย สาขาถนนวิสุทธิกษัตริย์ เลขที่บัญชี 006 – 0 – 20390 – 0</dd>
					<dd>5.2 โอนเงินผ่านระบบออนไลน์ Mobile Banking เลือกโอนเงินมาที่ บัญชี มูลนิธิครูดีของแผ่นดินธนาคารกรุงไทย เลขที่บัญชี 006 – 0 – 20390 – 0</dd>
					<dd>5.3 สแกน QR Code นี้ </dd>
				<img src="images/QRCodeสนับสนุนของที่ระลึก.jpg" alt="QRCodeสนับสนุนของที่ระลึก">
				<dt class="m-t-15">ขั้นที่ 6  แจ้งชำระเงิน และข้อมูลผู้สั่งซื้อสินค้า</dt>
					<dd>6.1 กรอกวัน เดือน ปี และ เวลา ที่ท่านชำระเงิน </dd>
					<dd>6.2 แนบหลักฐานการชำระเงิน</dd>
					<dd>6.3 ข้อมูลผู้สั่งซื้อสินค้า (สำหรับออกใบเสร็จ พร้อมกรอกข้อมูลเพิ่มเติมที่ปรากฏ เช่น เลขที่บัตรประจำตัวประชาชน)</dd>
					<dd>6.4 กด แจ้งชำระเงิน</dd>
				<dt class="m-t-15">ขั้นที่ 7  เจ้าหน้าที่ดำเนินการหลังจากที่ท่านได้แจ้งข้อมูลเพื่อการสั่งซื้อสินค้า โดยที่</dt>
					<dd>7.1 เจ้าหน้าที่ทำการตรวจสอบข้อมูลการสั่งซื้อสินค้าของท่าน</dd>
					<dd>7.2 เจ้าหน้าที่ทำดำเนินการออกใบสำคัญรับเงินพร้อมแพคสินค้าตามที่ท่านได้สั่งซื้อไว้ภายใน 24  ชั่วโมงหลังจากที่ท่านแจ้งข้อมูล</dd>
					<dd>7.3 เจ้าหน้าที่ดำเนินการจัดส่งสินค้าภายใน 24 ชั่วโมง หลังจากแพคสินค้าท่านเรียบร้อย (ในขั้นตอน7.3 ผู้สั่งซื้อสินค้าสามารถตรวจสอบเลขที่ไปรษณีย์ได้ E-Mail ผู้สั่งซื้อสินค้า หลังจาก 48 ชั่วโมง ที่ท่านได้แจ้งข้อมูลสั่งซื้อสินค้า)</dd>
					<dd>7.4 ท่านจะได้รับสินค้าภายใน</dd>
					<dd class="m-l-70">7.4.1 2-4 วัน หลังจากที่ท่านได้รับ E-Mail แจ้งเลขที่ไปรษณีย์ สำหรับ ผู้สั่งซื้อสินค้า ให้จัดส่งแบบ EMS</dd>
					<dd class="m-l-70">7.4.2 5-7 วัน หลังจากที่ท่านได้รับ E-Mail แจ้งเลขที่ไปรษณีย์ สำหรับ ผู้สั่งซื้อสินค้าให้จัดส่งแบบ ลงทะเบียน</dd>
					<dd class="m-l-70">7.4.5 เมื่อผู้สั่งซื้อสินค้าได้รับสินค้า (หากเกิดกรณี)</dd>
					<dd class="m-l-100">7.4.5.1 กรณีผู้สั่งซื้อสินค้า เลือกสินค้าผิด ประสงค์จะเปลี่ยนแปลงสินค้า ผู้สั่งซื้อสินค้าจะต้องเป็นผู้เสียค่าใช้จ่ายที่เกิดขึ้นเพิ่มเติมทั้งหมด</dd>
					<dd class="m-l-100">7.4.5.2 กรณีเจ้าหน้าที่มูลนิธิ จัดส่งสินค้าผิดไม่เป็นไปตามข้อมูลที่ท่านสั่งซื้อ ทางมูลนิธิจะเป็นผู้รับผิดชอบค่าใช้จ่ายที่เกิดขึ้นเพิ่มเติมทั้งหมด</dd>
					<dd class="m-l-100">7.4.5.3 หากเกิด 7.4.5.1 และ 7.4.5.2 ผู้สั่งซื้อสินค้าจะต้องแจ้งมูลนิธิภายใน 7 วันหลังจากที่ได้รับสินค้า โดยผู้สั่งซื้อสินค้าสามารถแจ้งผ่านทาง Inbox เพจเครือข่ายครูดีของแผ่นดิน</dd>
					<dd class="m-l-100 m-t-15"><strong>*หากไม่มีการแจ้งภายใน 7 วัน มูลนิธิจะถือว่าผู้สั่งซื้อสินค้าได้รับสินค้าครบถ้วนถูกต้องสมบรูณ์ จะไม่สามารถเปลี่ยนแปลงสินค้าใดๆได้</strong></dd>
			</dl>
      <!-- ################################################################################################ -->
    </article>
  </main>
</div>
<!-- End Content 01 - วิธีการสนับสนุนมูลนิธิ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>