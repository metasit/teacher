<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		
		if($_GET['CFP'] == 'popPrintfifth_score4tea_cer') {

			/*
				Code E: Directory เกียรติบัตรครูชั้นที่ 5
				Code F: Running Number (คดผ.6305XXXXX1)
			*/

			$sqlfifth_score4tea_code_E = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND `fifth_score4tea_code`='E' ";
			$reFS4TE = mysqli_query($con, $sqlfifth_score4tea_code_E);
			
			if(mysqli_num_rows($reFS4TE) == 0) { // first time click print fifth_score4tea
				
					/***********************************************************************************************************************************/
					/**************************************************** Start Print & Save เกียรติบัตร **************************************************/
					/***********************************************************************************************************************************/
				
					/* Save เกียรติบัตรออนไลน์ with user name */
					$font_color = imagecolorallocate($jpg_image, 0, 0, 0); // Set font color
					$font_path = 'layout/styles/fonts/THSarabunIT๙ Bold.ttf'; // Set font file path
					
					/* Set Text that need to be Printed On Image */
					$sqllogin = "SELECT `prename` FROM `login` WHERE `ID`='$ID' ";
					$relogin = mysqli_query($con, $sqllogin);
					$rowlogin = mysqli_fetch_array($relogin);
					// Set pename
					if($rowlogin['prename'] == 'A') {
						$prename = 'นาย';
					}elseif($rowlogin['prename'] == 'B') {
						$prename = 'นาง';
					}elseif($rowlogin['prename'] == 'C') {
						$prename = 'นางสาว';
					}elseif($rowlogin['prename'] == 'D') {
						$prename = 'ด.ช.';
					}elseif($rowlogin['prename'] == 'E') {
						$prename = 'ด.ญ.';
					}elseif($rowlogin['prename'] == 'O') {
						$prename = $rowlogin['prename_remark'];
					}
					$name = trim($_SESSION['firstname']).' '.trim($_SESSION['lastname']); // Set name
					$namewithprename = $prename.$name; // Set prename
	
					// Set School
					$sqlaffil_name = "SELECT `affil_name` FROM `login` WHERE `ID`='$ID' ";
					$reaffil_name = mysqli_query($con, $sqlaffil_name);
					$rowaffil_name = mysqli_fetch_array($reaffil_name);
	
					$affil_name = $rowaffil_name['affil_name'];
	
					$school = 'โรงเรียน'.substr($affil_name, strrpos($affil_name, '*')+1);

					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					// Set Running Number for basic certificate

					/* NOTE: ต้องมีการตรวจสอบว่า มี code F มาแล้วหรือไม่ เนื่องจากเกิดเหตุการณ์ช่วงรอยต่อระหว่างปรับ Organize ของกระทรวงระหว่างปี ตอน ต.ค.63 จึงทำให้โลโก้+ลายเซ็นบนเกียรติบัตรมีข้อ Concern
					และรวมกับมูลนิธิได้ทำการออกเกียรติบัตรสำหรับสมาชิกที่บำรุงสมาชิกไปแล้ว ทำให้ต้องใช้ Running Number เดิม ห้ามเปลี่ยน
					
					จึงทำการลบ Code E ออกเพื่อให้ระบบพิมพ์เกียรติใหม่ แต่เก็บ Code F ไว้ เพื่อจะได้ทราบว่า Running Number เดิมของ User เป็นเลขอะไร */
					$sqlfifth_score4tea_code_F = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND `fifth_score4tea_code`='F' ";
					$reFS4TF = mysqli_query($con, $sqlfifth_score4tea_code_F);

					if(mysqli_num_rows($reFS4TF) == 0) {

						$year = substr(date('Y')+543,-2);
						$find_run_num = 'คดผ.'.$year.'05%';

						$sqlfifth_score4tea_code_E = "SELECT * FROM `fifth_score4tea` WHERE `fifth_score4tea_code` LIKE 'F' AND `fifth_score4tea_data` LIKE '$find_run_num' ORDER BY `fifth_score4tea_id` DESC LIMIT 1 ";
						$reFS4TCE = mysqli_query($con, $sqlfifth_score4tea_code_E);
						$rowFS4TCE = mysqli_fetch_array($reFS4TCE);
					
						$fifth_score4tea_cer_type = 'คดผ.';
						$fifth_score4tea_cer_num = sprintf('%06d', intval(substr($rowFS4TCE['fifth_score4tea_data'], -5))+1);
						$fifth_score4tea_cer_run_num = $fifth_score4tea_cer_type.$year.'05'.$fifth_score4tea_cer_num;

					}else{

						$rowFS4TF = mysqli_fetch_array($reFS4TF);
						$fifth_score4tea_cer_run_num = $rowFS4TF['fifth_score4tea_data'];

					}
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

					// Set date
					$sqlfifth_score_date = "SELECT `fifth_score_date` FROM `login` WHERE `ID`='$ID' ";
					$refifth_score_date = mysqli_query($con, $sqlfifth_score_date);
					$rowfifth_score_date = mysqli_fetch_array($refifth_score_date);

					// Check date is before or after than 1 Oct? to know which certificate will be printed
					$fifth_score_date = $rowfifth_score_date['fifth_score_date'];
					$date = date('Y-m-d', strtotime($fifth_score_date));
					
					if($date <= '2020-10-31') {

						$jpg_image = imagecreatefromjpeg('images/เกียรติบัตร/ชั้นที่5/ครู/เกียรติบัตรชั้นที่5-ครู.jpg'); // Create Image From Existing File (4 Logos)

					}else{

						$jpg_image = imagecreatefromjpeg('images/เกียรติบัตร/ชั้นที่5/ครู/เกียรติบัตรชั้นที่5_2-ครู.jpg'); // Create Image From Existing File (3 Logos)

					}

					/* Set date (Special Case) */
					if($date > '2020-09-30' && $date < '2020-11-01') {

						$date = '2020-09-30';

					}
					
					/* Array for Thai Date */
					$arabicnum = array("1","2","3","4","5","6","7","8","9","0");
					$thainum = array("๑","๒","๓","๔","๕","๖","๗","๘","๙","๐");
					$test = str_replace($numthai,$numarabic,$message);
					/* Array for Thai Month */
					$month_arr=array(
						"1"=>"มกราคม",
						"2"=>"กุมภาพันธ์",
						"3"=>"มีนาคม",
						"4"=>"เมษายน",
						"5"=>"พฤษภาคม",
						"6"=>"มิถุนายน", 
						"7"=>"กรกฎาคม",
						"8"=>"สิงหาคม",
						"9"=>"กันยายน",
						"10"=>"ตุลาคม",
						"11"=>"พฤศจิกายน",
						"12"=>"ธันวาคม"                 
					);
					$date_cer = str_replace($arabicnum, $thainum, date('j', strtotime($date))).' '.$month_arr[date('n', strtotime($date))].' '.str_replace($arabicnum, $thainum, (date('Y', strtotime($date))+543)); // Set certicate date in Thai
	
					$font_size = 40; //Set font size
					$angle = 0; //Set angle
					/* Set x-position on certificate for name */
					$dimen4name = imagettfbbox($font_size, $angle, $font_path, $namewithprename);
					$text4name = (abs($dimen4name[4] - $dimen4name[0]))/2;
					$x4name = 758 - $text4name+15; // Template เกียรติบัตรไม่กึ่งกลาง
					/* Set x-position on certificate for school */
					$dimen4school = imagettfbbox($font_size, $angle, $font_path, $school);
					$text4school = (abs($dimen4school[4] - $dimen4school[0]))/2;
					$x4school = 758 - $text4school+15;
					/* Print school name and date on certificate */
					if($date < '2020-11-01') {
						imagettftext($jpg_image, $font_size, $angle, $x4name, 445, $font_color, $font_path, $namewithprename);
						imagettftext($jpg_image, $font_size, $angle, $x4school, 515, $font_color, $font_path, $school);
						imagettftext($jpg_image, $font_size, $angle, 670, 675, $font_color, $font_path, $date_cer);
						imagettftext($jpg_image, 27, $angle, 1200, 135, $font_color, $font_path, $fifth_score4tea_cer_run_num);
					}else{
						imagettftext($jpg_image, $font_size, $angle, $x4name, 445, $font_color, $font_path, $namewithprename);
						imagettftext($jpg_image, $font_size, $angle, $x4school, 515, $font_color, $font_path, $school);
						imagettftext($jpg_image, $font_size, $angle, 640, 665, $font_color, $font_path, $date_cer);
						imagettftext($jpg_image, 27, $angle, 1200, 135, $font_color, $font_path, $fifth_score4tea_cer_run_num);
					}
					
	
					$target_dir = 'images/เกียรติบัตร/ชั้นที่5/ครู/'.date('Y-m-d', strtotime($fifth_score_date)).'/'; // Set target_directory
	
					if(!is_dir($target_dir)) { // if there's not folder in target_directory
						mkdir($target_dir); // Create folder name is today_date
					}
	
					$target_file = $target_dir.'เกียรติบัตรครูชั้นที่5 '.$name.'.jpg';
	
					imagejpeg($jpg_image, $target_file);// Send Image to Browser or save in directory on client
					imagedestroy($jpg_image); // Clear Memory

					/* INSERT data to `honor_score4sup_cer` table */
					if(mysqli_num_rows($reFS4TF) == 0) {

						$fifth_score4tea_code_E = 'E';
						$fifth_score4tea_data_E = $target_file;

						$fifth_score4tea_code_F = 'F';
						$fifth_score4tea_data_F = $fifth_score4tea_cer_run_num;

						$sql = "INSERT INTO `fifth_score4tea` (`ID`, `fifth_score4tea_code`, `fifth_score4tea_data`) 
						VALUES ('$ID', '$fifth_score4tea_code_E', '$fifth_score4tea_data_E'),
						('$ID', '$fifth_score4tea_code_F', '$fifth_score4tea_data_F') ";

						$res = $con->query($sql) or die($con->error); //Check error

					}else{

						$fifth_score4tea_code_E = 'E';
						$fifth_score4tea_data_E = $target_file;

						$sql = "INSERT INTO `fifth_score4tea` (`ID`, `fifth_score4tea_code`, `fifth_score4tea_data`) 
						VALUES ('$ID', '$fifth_score4tea_code_E', '$fifth_score4tea_data_E') ";
						
						$res = $con->query($sql) or die($con->error); //Check error
						
					}
	
					header('location: '.$target_file);
	
					/***********************************************************************************************************************************/
					/****************************************************** End Print & Save เกียรติบัตร **************************************************/
					/***********************************************************************************************************************************/


			}else{

				$rowFS4TE = mysqli_fetch_array($reFS4TE);
				header('location: '.$rowFS4TE['fifth_score4tea_data']);

			}


		}else{
			echo "<script>window.history.go(-1)</script>";
		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>