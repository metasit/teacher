<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if($_GET['CFP'] == 'มอบเกียรติบัตรBIA') {

			$affil_code = $_SESSION['affil_code'];

			$sqlbia_cer = "SELECT * FROM `bia_cer` WHERE `bia_cer_affil_code`='$affil_code' && `bia_cer_code`='A' "; // เช็คว่า table `bia_cer` มีการกรอกข้อมูลหน้า มอบเกียรติบัตรBIA_step1.php มาหรือยัง
			$rebia_cer = mysqli_query($con, $sqlbia_cer);

			if(mysqli_num_rows($rebia_cer) != 0) {

				$sqlbia_cer = "SELECT * FROM `bia_cer` WHERE `bia_cer_affil_code`='$affil_code' && `bia_cer_code`='H' "; // เช็คว่า table `bia_cer` มีการการปริ้นเกียรติบัตรผู้บริหาร ไปหรือยัง
				$rebia_cer = mysqli_query($con, $sqlbia_cer);

				if(mysqli_num_rows($rebia_cer) != 0) {

					$rowbia_cer = mysqli_fetch_array($rebia_cer);
					header('location: '.$rowbia_cer['bia_cer_data1']);

				}else{

					$sqlaffil_name = "SELECT `affil_name` FROM `login` WHERE `ID`='$ID' ";
					$reaffil_name = mysqli_query($con, $sqlaffil_name);
					$rowaffil_name = mysqli_fetch_array($reaffil_name);

					$affil_name = $rowaffil_name['affil_name'];
					$school_name = 'โรงเรียน'.substr($affil_name, strrpos($affil_name, '*')+1);

				}

			}else{
				echo "<script>window.history.go(-1)</script>";
			}

		}else{
			echo "<script>window.history.go(-1)</script>";
		}
		
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}

?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt inline">
					<div class="col-md-6">
						<img src="images/เด็กดีของแผ่นดิน Logo02.jpg" alt="มูลนิธิครูดีของแผ่นดิน Logo02">
					</div>
					<div class="col-md-5">
						<img src="images/BIAlogo01.png" style="width:70%;" alt="มูลนิธิครูดีของแผ่นดิน BIAlogo01">
					</div>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-form-title fs-30 lh-1-1">
					เนื่องจากคุณไม่ใช่ผู้บริหารสถานศึกษา <strong><?php echo $school_name; ?></strong>
					<br>
					กรุณาอย่าแอบอ้างการพิมพ์เกียรติบัตรของผู้บริหารสถานศึกษา
					<br>
					<div class="text-red1 m-t-10 m-b-10" style="font-family: RSUText">
						(คุณจะสามารถกรอกชื่อ-นามสกุล ให้กับท่าน กรณีที่คุณได้รับการมอบหมายให้พิมพ์เกียรติบัตรประเภทผู้บริการสถานศึกษามาแล้วเท่านั้น)
					</div>	
					กรอกคำนำหน้านาม, ชื่อ และนามสกุล ด้านล่าง
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form" action="printBIA_cer4exe.php?CFP=กรอกข้อมูลผู้บริหารเกียรติบัตรBIA" method="POST">
					<select name="prename_exe" class="form-control" style="margin-bottom:10px; height:40px;" onchange="PickPrename(this.value);" required>
						<option value="" disabled="disabled" selected="selected">เลือกคำหน้านาม</option>
						<option value="A">นาย</option>
						<option value="B">นาง</option>
						<option value="C">นางสาว</option>
						<option value="O">อื่นๆ</option>
					</select>
					<div id="pre_name_text" class="wrap-input100 validate-input" style="display: none;">
						<input class="input100" type="text" id="pre_name_text_required" name="prename_text_exe" class="form-control" placeholder="โปรดระบุคำหน้านาม" max-lenght="30" />
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="firstname_exe" class="form-control" placeholder="ชื่อ" max-lenght="100" />
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="lastname_exe" class="form-control" placeholder="นามสกุล" max-lenght="100" />
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="login100-form-title fs-30 lh-1-1 text-red1 m-t-10 m-b-10 bold">
						(กรุณาตรวจสอบข้อมูลที่ท่านให้มาอย่างละเอียดอีกครั้ง เพื่อความถูกต้อง)
					</div>				
					<!-- ################################################################################################ -->
					<div class="container-login100-form-btn">
						<a href="มอบเกียรติบัตรBIA.php" class="login100-form-btn" style="width:30%; margin:0 10px; font-family: KanitLight;">ย้อนกลับ</a>
						<button class="login100-form-btn" style="width:30%" type="submit" id="register">พิมพ์เกียรติบัตร</button>
					</div>
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

<script>
	function PickPrename(val) {
		var elePrename = document.getElementById('pre_name');
		var elePrenameText = document.getElementById('pre_name_text');
		var elePrenameTextReq = document.getElementById('pre_name_text_required');
		if(val == 'O') {
			elePrenameText.style.display = 'block';
			elePrenameTextReq.required = true;
		}else{
			elePrenameText.style.display = 'none';
			elePrenameText.value = '';
			elePrenameTextReq.required = false;
		}
	}
</script>

</body>
</html>