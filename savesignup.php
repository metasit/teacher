<?php
	session_start();
	if(isset($_SESSION['ID'])) {
		echo "<script>window.history.go(-1)</script>";
	}else{
		if($_POST['CFP'] != 20) { // not CFP signup.php || signup-en.php
			echo "<script>window.history.go(-1)</script>";
		}else{
			require_once("condb.php");
		}
	}
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>

<body>
	<?php
		$newEmail = strtolower(trim($_POST["email"]));

		$strSQL = "SELECT * FROM `login` WHERE email='$newEmail' "; //Let $strSQL to be a command that find email with user request in database
		$objQuery = mysqli_query($con,$strSQL); // Connect to database $con and do command as $strSQL
		$objResult = mysqli_fetch_array($objQuery);

		$newPassword = $_POST["password"];
		$newConpassword = $_POST["conpassword"];
		$newPrename = $_POST['pre_name'];
		if($newPrename == 'O') {
			$newPrenameRemark = $_POST['pre_name_text'];
		}
		$newFirstname = $_POST["firstname"];
		$newFirstname = str_replace("นาย","",$newFirstname);
		$newFirstname = str_replace("นางสาว","",$newFirstname);
		$newFirstname = str_replace("นาง","",$newFirstname);
		$newFirstname = str_replace("ด.ช.","",$newFirstname);
		$newFirstname = str_replace("ด.ญ.","",$newFirstname);
		// $newFirstname = str_replace("อื่นๆ","",$newFirstname);
		
		// var_dump($newFirstname);
		// exit();
		// firstname

		$newLastname = $_POST["lastname"];
		$newPhonenum = $_POST["phonenum"];
		$Birthdate = $_POST["birthdate"];

		// 299 หมู่3 ตำบล อำเภอเมืองสุพรรณบุรี อำเภอ รั้วใหญ่ จังหวัด สุพรรณบุรี 

		$newBirthdate = str_replace("-",'',$Birthdate);
		$newDocaddress =   $_POST['docaddress']."  ".explode("/",$_POST['district_sub'])[0]."   ".$_POST['district']." จังหวัด ".$_POST['province']." ".$_POST['zipcode'];
		// var_dump(explode("/",$_POST['district'])[0]);
		// var_dump($newDocaddress);
		// exit();
		// $_POST["docaddress"];

		if($objResult) { ?>
			<!-- Start Email exists -->
			<div class="limiter">
				<div class="container-login100">
					<div class="wrap-login100">
						<!-- ################################################################################################ -->
						<div class="textlink3 PreMenu_fl_left">
							<a href="signup.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าสมัครสมาชิก</a>
						</div>
						<!-- ################################################################################################ -->
						<div class="login100-pic js-tilt">
							<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
						</div>
						<!-- ################################################################################################ -->
						<div class="signup-stat m-t-20">
							<h8>อีเมลนี้เคยถูกใช้ในการสมัครแล้ว</h8>
							<p>โปรดใช้อีเมลอื่นในการสมัคร</p>
						</div>
						<!-- ################################################################################################ -->
					</div>
				</div>
			</div>
			<!-- End Email exists --> <?php
		}elseif($newPassword != $newConpassword) { ?>
			<!-- Start Password non-match -->
			<div class="limiter">
				<div class="container-login100">
					<div class="wrap-login100">
						<!-- ################################################################################################ -->
						<div class="textlink3 PreMenu_fl_left">
							<a href="signup.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าสมัครสมาชิก</a>
						</div>
						<!-- ################################################################################################ -->
						<div class="login100-pic js-tilt">
							<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
						</div>
						<!-- ################################################################################################ -->
						<div class="signup-stat m-t-20">
							<h8>รหัสผ่านไม่ตรงกัน</h8>
							<p>โปรดกลับไปหน้าสมัครสมาชิก และกรอกรหัสผ่านให้ตรงกัน</p>
						</div>
						<!-- ################################################################################################ -->
					</div>
				</div>
			</div>
			<!-- End Password non-match --> <?php
		}else{

			/* ตรวจว่า เป็นเด็กที่เคยเข้าร่วมโครงการต้นกล้ามาหรือไม่ */
			$login_stu_check = "SELECT * FROM `login` WHERE firstname='$newFirstname' AND lastname='$newLastname' AND basic_score_remark2 LIKE '%B,yes%' ";
			$relogin_stu_check = mysqli_query($con, $login_stu_check);
			$rowlogin_stu_check = mysqli_fetch_array($relogin_stu_check);

			if(mysqli_num_rows($relogin_stu_check) == 1 && $_POST['migrate_stu'] == '') { ?>
				<div class="limiter">
					<div class="container-login100">
						<div class="wrap-login100">
							<!-- ################################################################################################ -->
							<div class="textlink3 PreMenu_fl_left">
								<a href="index.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าหลัก</a>
							</div>
							<!-- ################################################################################################ -->
							<div class="login100-pic js-tilt">
								<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
							</div>
							<!-- ################################################################################################ -->
							<div class="signup-stat m-t-20 inline">
								<h8>คุณเคยมีข้อมูลอยู่ในระบบของมูลนิธิ</h8>
								<br><br>
								<p>คุณเคยเข้าร่วมโครงการเด็กดีของแผ่นดิน (ต้นกล้าแห่งความดี)</p>
								<p>ชื่อผู้สมัครโครงการนี้ให้กับคุณ: <?php echo $rowlogin_stu_check['basic_score_text']; ?></p>
								<br>
								<p>คุณต้องการดึงข้อมูลที่คุณเคยทำมาด้วยหรือไม่</p>
								<br>
								<form action="savesignup.php" method="POST">
									<button class="btn BG-green1" type="submit">ใช่</button>

									<input type="hidden" name="email" value="<?php echo $newEmail; ?>">
									<input type="hidden" name="password" value="<?php echo $newPassword; ?>">
									<input type="hidden" name="conpassword" value="<?php echo $newConpassword; ?>">
									<input type="hidden" name="phonenum" value="<?php echo $newPhonenum; ?>">
									<input type="hidden" name="birthdate" value="<?php echo $Birthdate; ?>">
									<input type="hidden" name="docaddress" value="<?php echo $newDocaddress; ?>">

									<input type="hidden" name="CFP" value="20">
									<input type="hidden" name="ID_stu" value="<?php echo $rowlogin_stu_check['ID']; ?>">
									<input type="hidden" name="migrate_stu" value="yes">
								</form>
								<form action="savesignup.php" method="POST">
									<button class="btn BG-red1" type="submit">ไม่</button>

									<input type="hidden" name="email" value="<?php echo $newEmail; ?>">
									<input type="hidden" name="password" value="<?php echo $newPassword; ?>">
									<input type="hidden" name="conpassword" value="<?php echo $newConpassword; ?>">
									<input type="hidden" name="pre_name" value="<?php echo $newPrename; ?>"> <?php
									if($newPrename == 'O') { ?>
										<input type="hidden" name="pre_name_text" value="<?php echo $newPrenameRemark; ?>"> <?php
									} ?>
									<input type="hidden" name="firstname" value="<?php echo $newFirstname; ?>">
									<input type="hidden" name="lastname" value="<?php echo $newLastname; ?>">
									<input type="hidden" name="phonenum" value="<?php echo $newPhonenum; ?>">
									<input type="hidden" name="birthdate" value="<?php echo $Birthdate; ?>">
									<input type="hidden" name="docaddress" value="<?php echo $newDocaddress; ?>">

									<input type="hidden" name="CFP" value="20">
									<input type="hidden" name="migrate_stu" value="no">
								</form>
							</div>
							<!-- ################################################################################################ -->
						</div>
					</div>
				</div> <?php
				
			}else{

				if($_POST['migrate_stu'] == 'yes') {
					echo "success";
					$ID_stu = $_POST['ID_stu'];
					$strSQL = "UPDATE `login` SET `email`='$newEmail', `password`='$newPassword', `phonenum`='$newPhonenum', `birthdate`='$newBirthdate', `docaddress`='$newDocaddress' WHERE ID='$ID_stu' ";
				}else{
					$strSQL = "INSERT INTO `login` (`email`,`password`,`prename`,`prename_remark`,`firstname`,`lastname`,`phonenum`,`birthdate`,`docaddress`) 
					VALUES ('$newEmail','$newPassword','$newPrename','$newPrenameRemark','$newFirstname','$newLastname','$newPhonenum','$newBirthdate','$newDocaddress') ";
				}
				// echo "MAIL";
				// var_dump($newEmail);
				// exit();

				// echo $strSQL."<br/>";
				// exit();
				$objQuery = mysqli_query($con,$strSQL);



				$str_select_sql = "SELECT * FROM login order by   ID desc LIMIT 1";
				$sql_query_list =  $con->query($str_select_sql);
				while($row = $sql_query_list->fetch_assoc()) {
				  $id_token = $row['ID'];
				}
				// var_dump($id_token);
				// exit();
				// var_dump($newEmail);
				$to = $newEmail;
				$subject = 'Activate Account';
				// $message = 'hello';
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				 
				// Create email headers
				$headers .= 'From: '."webmaster@example.com"."\r\n".
				'Reply-To: '."webmaster@example.com"."\r\n" .
				'X-Mailer: PHP/' . phpversion();
			
				$message = '<html><body>';
				$message .= '<h1 style="color:#f40;"> <a href = "http://www.thaisuprateacher.org/dev/confirm-account.php?id='.$id_token.'"> Activate Account </a> </h1>';
				$message .= '</body></html>';
				
				// $headers = 'From: webmaster@example.com' . "\r\n" .
				//     'Reply-To: webmaster@example.com' . "\r\n" .
				//     'X-Mailer: PHP/' . phpversion();
				
				mail($to, $subject, $message, $headers);

				
				?>
				<!-- Start Password match and save to database -->
				<div class="limiter">
					<div class="container-login100">
						<div class="wrap-login100">
							<!-- ################################################################################################ -->
							<div class="textlink3 PreMenu_fl_left">
								<a href="index.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าหลัก</a>
							</div>
							<!-- ################################################################################################ -->
							<div class="login100-pic js-tilt">
								<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
							</div>
							<!-- ################################################################################################ -->
							<div class="signup-stat m-t-20">
								<h8>การสมัครสมาชิกสมบูรณ์</h8>
								<div>
									<h9>กรุณาตรวจสอบ Email <?php echo $newEmail; ?>  </h9>
								</div>
								<p>ยินดีด้วยค่ะ คุณได้เป็นสมาชิกทั่วไปของมูลนิธิ</p>
								<p>คุณสามารถเลื่อนระดับสมาชิก <a href="บำรุงค่าสมาชิก.php">ได้ที่นี่</a></p>
								<p>สมัครเข้าร่วมโครงการ <a href="login.php?CFP=30">ได้ที่นี่</a></p>
							</div>
							<!-- ################################################################################################ -->
						</div>
					</div>
				</div>
				<!-- End Password match and save to database --> <?php
			}
		}
	?>
</body>
</html>