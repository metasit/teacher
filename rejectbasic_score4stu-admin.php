<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$now_date = date("d-m-Y H:i:s");

	/* Set all value */
	$winScroll = $_POST['winScroll'];
	$basic_score4stu_id = $_POST['basic_score4stu_id'];
	$reject_reason = $_POST['reject_reason'];

	$basic_score4stu_check_status = 'ปฏิเสธ,'.$reject_reason.','.$now_date;

	$sql = "UPDATE `basic_score4stu` SET `basic_score4stu_check_status`='$basic_score4stu_check_status' WHERE `basic_score4stu_id`='$basic_score4stu_id' ";
	$res= $con->query($sql) or die($con->error); //Check error



	/* Log Admin Action */
	$ID_ad = $_SESSION['ID']; // admin ID
	$adscorelog_task = 'ตรวจความดีเด็กพื้นฐาน,'.$_POST['basic_score4stu_system_id'].$_POST['basic_score4stu_file_no'];
	$ID_user = $_POST['ID_stu'];
	$adscorelog_detail = 'เด็ก,ขั้นพื้นฐาน,'.$basic_score4stu_check_status;

	$sqllog="INSERT INTO `adscorelog` (ID_ad, adscorelog_task, ID_user, adscorelog_detail) VALUES ('$ID_ad','$adscorelog_task',$ID_user,'$adscorelog_detail') ";
	$relog = $con->query($sqllog) or die($con->error); //Check error


	header('location: รายละเอียดนักเรียนทำความดี-พื้นฐาน-ไม่มีครูที่ปรึกษา-admin.php?winScroll='.$winScroll.'&ID_stu='.$_POST['ID_stu'].'&system_id='.$_POST['basic_score4stu_system_id']);
?>