<?php session_start(); ?>

<!DOCTYPE html>

<html lang="thai">
<!-- To declare your language - read more here: https://www.w3.org/International/questions/qa-html-language-declarations -->

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start NoAccount Session -->
<?php
      if (empty($_SESSION['email']))
      {
?>
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
          <li class="faicon-login"><a href="login-en.php"><i class="fas fa-user-lock"></i> <text1>Log in</text1></a></li>
          <li class="faicon-login"><a href="signup-en.php"><i class="fas fa-file-signature"></i> <text1>Sign up</text1></a></li>
          <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i><i class="fas fa-language"></i> <text1>English</text1></a>
            <ul>
              <li><a href="โครงการนักศึกษานิเทศก์ดีของแผ่นดิน.php">ภาษาไทย</a></li>
              <!--<li><a href="#">Chinese</a></li>-->
            </ul>
          </li>
          <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
              <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="http://www.thaisuprateacherdonate.org">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              <li><a href="วิธีการบริจาค-en.php">วิธีการบริจาค</a></li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- End NoAccount Session -->
      <!-- ################################################################################################ -->
      <!-- Start Admin Session -->
<?php
      }
      elseif ($_SESSION['email'])
      {
        if ($_SESSION["level"]=="admin")
        {
?>
        <nav id="mainav2" class="PreMenu_fl_right">
          <ul class="PreMenufaico2">
            <li style="background-color:rgb(228,0,0); padding:5px 17px;"><i class="fas fa-user-edit"></i> Admin</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการนักศึกษานิเทศก์ดีของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="http://www.thaisuprateacherdonate.org">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                <li><a href="วิธีการบริจาค-en.php">วิธีการบริจาค</a></li>
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Admin Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member General Session -->
<?php
        if ($_SESSION["level"]=="memberGeneral")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:black; background-color:white; padding:5px 17px;"><i class="fas fa-user-check"></i> Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการนักศึกษานิเทศก์ดีของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="http://www.thaisuprateacherdonate.org">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                <li><a href="วิธีการบริจาค-en.php">วิธีการบริจาค</a></li>
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member General Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member SILVER Session -->
<?php
        if ($_SESSION["level"]=="memberSilver")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(169,169,169); padding:5px 17px;"><i class="fas fa-user-check"></i> Silver Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการนักศึกษานิเทศก์ดีของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="http://www.thaisuprateacherdonate.org">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                <li><a href="วิธีการบริจาค-en.php">วิธีการบริจาค</a></li>
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member SILVER Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member GOLD Session -->
<?php
        if ($_SESSION["level"]=="memberGold")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(180,147,31); padding:5px 17px;"><i class="fas fa-user-check"></i> Gold Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการนักศึกษานิเทศก์ดีของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="http://www.thaisuprateacherdonate.org">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                <li><a href="วิธีการบริจาค-en.php">วิธีการบริจาค</a></li>
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member GOLD Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member DIAMOND Session -->
<?php
        if ($_SESSION["level"]=="memberDiamond")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white; padding:5px 17px;"><i class="fas fa-user-check"></i> Diamond Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการนักศึกษานิเทศก์ดีของแผ่นดิน.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="http://www.thaisuprateacherdonate.org">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                <li><a href="วิธีการบริจาค-en.php">วิธีการบริจาค</a></li>
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
      }
?>
      <!-- End Member DIAMOND Session -->
      <!-- ################################################################################################ -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index-en.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index-en.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index-en.php">Home</a></li>
          <li><a href="เกี่ยวกับเรา-en.php">About us</a></li>
          <li class="active"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i>Join us</a>
            <ul>
              <li><a href="ร่วมโครงการ-en.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <li><a href="โครงการครูดีของแผ่นดิน-en.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการนักศึกษานิเทศก์ดีของแผ่นดิน-en.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมศึกษาพัฒนา-en.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA-en.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน-en.php">โครงการอาสาของแผ่นดิน</a></li>
            </ul>
          </li>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">Innovation Library</a></li>
          <li><a href="ติดต่อเรา-en.php">Contact us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="ร่วมโครงการ-en.php"> Join Us</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="โครงการนักศึกษานิเทศก์ดีของแผ่นดิน-en.php"> โครงการนักศึกษานิเทศก์ดีของแผ่นดิน</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - โครงการเด็กดีของแผ่นดิน -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <article style="text-align:center; padding-bottom:50px;">
        <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px"><strong>ศึกษานิเทศก์ดีของแผ่นดิน<br>
          “ เป็นครูว่ายากแล้ว เป็นครูของครูยากยิ่งกว่า ”</strong></p>
          <img src="images/มูลนิธิครูดีของแผ่นดิน ร่วมงาน_ศึกษานิเทศน์ดี FullScale.jpg" alt="เด็กดีของแผ่นดิน Logo">
        <p class="fs-32" style="font-family:RSUText; line-height:30pt; text-align:left;">
          <strong>ศึกษานิเทศก์คือใคร?</strong>
          <br><br>
          ศึกษานิเทศก์ (Supervisor)  คือ ผู้ทำหน้าที่แนะนำ ชี้แนะแนวทางให้ครูและผู้บริหารสถานศึกษาเกิดความตระหนักรู้ มีทักษะในการบริหารจัดการ และการจัดการเรียนการสอนอย่างเป็นระบบ 
          รวมทั้งสามารถเป็นที่ปรึกษาทางวิชาการสำหรับนักบริหารระดับสูงในองค์กรที่สังกัด เช่น ศึกษาธิการจังหวัด ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษา ผู้อำนวยการกลุ่มงานต่าง ๆ เป็นต้น
          <br><br>
          หากจะพูดเป็นภาษาที่เข้าใจง่าย ต้องกล่าวว่าศึกษานิเทศก์เป็นยิ่งกว่าครู นั่นคือ เป็นครูของครู เป็นที่ปรึกษาของครูในการพัฒนาการจัดการเรียนการสอน และเป็นกัลยาณมิตรของครูในทุกๆ เรื่องของการศึกษา 
          ทำให้เกิดความเชื่อร่วมกันว่า คนที่จะทำหน้าที่ศึกษานิเทศก์ได้ "ต้องมีความรู้แตกฉานลึกซึ้ง รู้จริงเก่งวิจัยเพื่อสร้างองค์ความรู้ เก่งประเมินเพื่อชี้แนะทิศทางการพัฒนา มีมนุษย์สัมพันธ์ดี มีทักษะไอซีทีได้มาตรฐาน 
          มีความสามารถในการสร้างเครือข่ายและเครื่องมือการนิเทศ "
          <br><br>
          <strong>ศึกษานิเทศก์...บุคคลที่หลายคนมองข้ามไป</strong>
          <br><br>
          แวดวงการการศึกษาขั้นพื้นฐาน <strong>ศึกษานิเทศก์</strong> นับเป็นหนึ่งในกลุ่มบุคลากรทางการศึกษาที่เป็นกลไกในการขับเคลื่อนคุณภาพการศึกษาไทยมายาวนานกว่า 60 ปี มีบทบาทและคุณูปการอย่างมากต่อการพัฒนา
          คุณภาพการศึกษาไทยมาทุกยุคทุกสมัย ผ่านระบบคัดเลือกมาจากครูดี ครูเก่ง จากหนึ่งในร้อย หนึ่งในพัน จนถึงหนึ่งในหมื่น นับเป็นเพชรน้ำงามประดับวงการศึกษาที่หาได้ยากยิ่ง แต่เป็นที่น่าสะท้อนใจว่าในปัจจุบัน 
          เมื่อเอ่ยถึงคำว่า “ศึกษานิเทศก์” คนรุ่นใหม่แทบไม่รู้จัก และมักมีคำถามตามมาว่า ศึกษานิเทศก์ คือ ใคร? มีหน้าที่อะไร? อยู่เสมอ
          <br><br>
          คงไม่มีใครอยากให้คุณค่าของศึกษานิเทศก์ไทยในวันนี้ ถูกออกแบบมาเพียงเพื่อรองรับงานโครงการ งานข้อมูล งานประเมินจากส่วนกลางเท่านั้น คงไม่มีใครอยากให้ความสง่างามในการนิเทศนับแต่อดีต 
          กลายเป็นเพียงเรื่องเล่าในตำนานให้คนรุ่นหลังได้จดจำ  ซึ่งนับวันจะเป็นดังเปลวเทียนอ่อนแสงลงไปทุกขณะ ท่ามกลางภาวะความเปลี่ยนแปลงเทคโนโลยีที่ยากจะก้าวตามให้ทันในทุกบริบท โครงการศึกษานิเทศก์ดี
          ของแผ่นดิน จึงบังเกิดขึ้น เพื่อบุคลากรทางการศึกษา..ผู้มีหัวใจทุ่มเทเสียสละเพื่อการพัฒนาวิชาชีพครู
          <br><br>
          <strong>ศึกษานิเทศก์ดีของแผ่นดิน</strong>
          <br><br>
          <strong>มูลนิธิครูดีของแผ่นดิน ร่วมกับสมาคมศึกษานิเทศก์แห่งประเทศไทย จึงได้จัดทำ โครงการ “ศึกษานิเทศก์ดีของแผ่นดิน”</strong> ขึ้น เพื่อส่งเสริม สนับสนุน พัฒนา ยกย่อง เชิดชูเกียรติ และ
          สร้างเครือข่ายศึกษานิเทศก์ที่สร้างคนดีให้แผ่นดิน ประพฤติปฏิบัติตนเจริญรอยตามเบื้องพระยุคลบาท ด้วยสำนึกในพระมหากรุณาธิคุณล้นเกล้าล้นกระหม่อมแห่งพระบาทสมเด็จพระวชิรเกล้าเจ้าอยู่หัว  และ
          พระบาทสมเด็จพระบรมชนกาธิเบศร มหาภูมิพลอดุลยเดชมหาราช บรมนาถบพิตร พระผู้ทรงเป็นครูแห่งแผ่นดิน ที่ทรงใส่พระราชหฤทัยในการส่งเสริมและพัฒนาการศึกษาของชาติ อันเป็นรากฐานสำคัญในการพัฒนา
          ทรัพยากรมนุษย์ทุกระดับชั้น 
          <strong>มูลนิธิครูดีของแผ่นดิน</strong> ขอเป็นกำลังใจให้กับเหล่าศึกษานิเทศก์ทั่วไทยได้จุดประกายความฝัน ส่งต่อแรงบันดาลใจให้กับครูทุกรุ่น ได้ตระหนักถึงจิตวิญญาณของความเป็นครู ด้วยหัวใจแห่งศึกษา
          นิเทศก์ที่ไม่มีวันมอดดับ จุดประกายไฟแห่งความมุ่งมั่นที่จะพัฒนาวงการศึกษาไทย ส่งต่อให้ถึงปลายทางคุณภาพคือเด็กนักเรียนที่เฝ้ารอคอยความหวัง รอคอย ของศึกษานิเทศก์ดีแห่งแผ่นดินที่จะไม่มีวันเลือนหายไป
          จากสังคมไทย ตราบนานเท่านาน.
          <ul class="fs-32 textlink" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
            <li><a href="docs/ร่วมโครงการ/ศึกษานิเทศก์ดี/รายละเอียดโครงการศึกษานิเทศก์ของแผ่นดิน.pdf">ดาวน์โหลดรายละเอียดโครงการ</a></li>
            <li><a href="docs/ร่วมโครงการ/ศึกษานิเทศก์ดี/เล่ม 1 คู่มือใช้งานสำหรับผู้ที่ต้องการเข้าร่วม.pdf">ดาวน์โหลด คู่มือการใช้ระบบวัดประเมินสมรรถนะครูดีของแผ่นดิน สำหรับผู้ที่สนใจสมัครเข้าร่วมโครงการ</a></li>
            <li><a href="docs/ร่วมโครงการ/ศึกษานิเทศก์ดี/ตัวอย่างหนังสือรับรองเพื่อประกอบการพิจารณารางวัล.pdf">ตัวอย่างหนังสือรับรองเพื่อประกอบการพิจารณารางวัลศึกษานิเทศก์ดีของแผ่นดิน</a></li>
          </ul>
        </p>
    </article>
    <div class="one_first">
      <div class="btnJoin">
        <a href="https://www.skillmeo.com/login"><h6>Apply this program</h6></a>
      </div>
    </div>
    <div class="one_first m-t-50">
      <div class="btnJoin">
        <a href="สนับสนุนมูลนิธิ-en.php"><h6>Donate</h6></a>
      </div>
    </div>
  </main>
</div>
<!-- End Content 01 - โครงการเด็กดีของแผ่นดิน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear">
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Related Department</strong></p>
      <div class="one_half first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        <a class="font-x1 footercontact" href="http://www.thaisuprateacherdonate.org/">
          <img src="images/มูลนิธิครูดีของแผ่นดิน inwshop Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Souvenir</p>
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Foundation of Thai Suprateacher</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>