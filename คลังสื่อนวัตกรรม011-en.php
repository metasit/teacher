<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start NoAccount Session -->
<?php
      if (empty($_SESSION['email']))
      {
?>
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
          <li class="faicon-login"><a href="login-en.php"><i class="fas fa-user-lock"></i> <text1>Log in</text1></a></li>
          <li class="faicon-login"><a href="signup-en.php"><i class="fas fa-file-signature"></i> <text1>Sign up</text1></a></li>
          <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
            <ul>
              <li><a href="คลังสื่อนวัตกรรม011.php">ภาษาไทย</a></li>
              <!--<li><a href="#">Chinese</a></li>-->
            </ul>
          </li>
          <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
              <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
        </ul>
      </nav>
      <!-- End NoAccount Session -->
      <!-- ################################################################################################ -->
      <!-- Start Admin Session -->
<?php
      }
      elseif ($_SESSION['email'])
      {
        if ($_SESSION["level"]=="admin")
        {
?>
        <nav id="mainav2" class="PreMenu_fl_right">
          <ul class="PreMenufaico2">
            <li style="background-color:rgb(228,0,0);"><i class="fas fa-user-edit"></i> Admin</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="คลังสื่อนวัตกรรม011.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Admin Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member General Session -->
<?php
        if ($_SESSION["level"]=="memberGeneral")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="คลังสื่อนวัตกรรม011.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member General Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member SILVER Session -->
<?php
        if ($_SESSION["level"]=="memberSilver")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> Silver Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="คลังสื่อนวัตกรรม011.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member SILVER Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member GOLD Session -->
<?php
        if ($_SESSION["level"]=="memberGold")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> Gold Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="คลังสื่อนวัตกรรม011.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member GOLD Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member DIAMOND Session -->
<?php
        if ($_SESSION["level"]=="memberDiamond")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> Diamond Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="คลังสื่อนวัตกรรม011.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
      }
?>
      <!-- End Member DIAMOND Session -->
      <!-- ################################################################################################ -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index-en.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index-en.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index-en.php">Home</a></li>
          <li><a href="เกี่ยวกับเรา-en.php">About us</a></li>
          <li class="joinus-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>Join us</a>
            <ul>
              <li><a href="ร่วมโครงการ-en.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <li><a href="โครงการครูดีของแผ่นดิน-en.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา-en.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA-en.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน-en.php">โครงการอาสาของแผ่นดิน</a></li>
            </ul>
          </li>
          
          <li class="active"><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">Innovation Library</a></li>
          <li><a href="ติดต่อเรา-en.php">Contact us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">Innovation Library</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> ค้นหาปัญหาของตนเองให้พบ ร่วมกลุ่มกันแก้ปัญหา...</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - คลังสื่อนวัตกรรม011 -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <article style="text-align:center; margin-bottom:100px;">
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px;">
        <strong>"ค้นหาปัญหาของตนเองให้พบ แล้วร่วมกลุ่มกันระดมความคิดแก้ปัญหา เพื่อสร้างพลังที่จะนำพาซึ่งการเปลี่ยนแปลง"</strong>
      </p>
      <div class="bgvdo">
        <video width="100%" controls>
          <source src="images/วีดีโอหอปัญหาตนเอง.mp4" type="video/mp4">
        </video>
      </div>
    </article>
    <p class="font-x3"><span style="color:rgb(180,147,31); line-height:30px; text-align:left;"><strong>บทความ</strong></span></p>
    <p class="font-x2" style="font-family:ChulaCharasNew; line-height:30pt; text-align:left;">
      เรื่อง Learning Together by PBL
      <br><br>
      1."ค้นหาปัญหาของตนเองให้พบ รวมกลุ่มกันแก้ปัญหา นำพาซึ่งการเปลี่ยนแปลง"
      <br>
      ครูใช้การจัดการเรียนรู้โดยใช้ปัญหาเป็นฐาน (Problem-Based Learning) ร่วมกับรูปแบบการจัดการเรียนรู้แบบร่วมมือ (Learning Together) โดยให้นักเรียนค้นหาปัญหาที่เกิดขึ้นกับตนเองและต้องการแก้ไขปัญหานั้น จับกลุ่มร่วมกันแก้ไขปัญหาที่มีความคล้ายถึงกัน ใช้กระบวนการกลุ่มเพื่อนช่วยเพื่อนและครูติดตามดูแลเอาใจใส่ ให้กำลังใจนักเรียน นำมาสู่การเปลี่ยนแปลงของนักเรียนในทางที่ดีขึ้น...
      <br><br>
      2.
      ✍️ ความคิดในการสร้างเด็กดีของแผ่นดินเกิดจากการที่ครูได้รับรางวัลครูดีของแผ่นดินขั้นพื้นฐานและครูคิดว่าจะต้องทำอะไรสักอย่างเพื่อให้สมกับคำว่าครูดีของแผ่นดิน และนี่ก็เป็นแรงบันดาลใจในการทำครูดีของแผ่นดิน
      ในขั้นต่อๆไป เมื่อมีโครงการเด็กดีของแผ่นดิน Z-IDOL เข้ามาครูมีความมุ่งมั่นตั้งใจจะเข้าร่วมโครงการนี้ เพียงหวังว่า ต้องการให้นักเรียนของครูเกิดการเปลี่ยนแปลงไปในทางที่ดีขึ้น มีความรับผิดชอบต่อตนเอง 
      สังคมและเป็นคนดีโดยครูจะเป็นต้นแบบที่ดี ใช้จิตวิญญาณความเป็นครู ใช้ทักษะในการจัดกิจกรรมการเรียนรู้ที่ทันสมัย หาความรู้และพัฒนาตนเองอยู่เสมอ
      <br><br>
      ✍️✍️ นวัตกรรมและกระบวนการที่ใช้ในการสร้างเด็กดีของแผ่นดิน ครูใช้การจัดการเรียนรู้โดยใช้ปัญหาเป็นฐาน (Problem-Based Learning) ร่วมกับการเรียนรู้แบบร่วมมือ (Learning Together) โดยเริ่มต้นจากการ
      ที่ครูให้นักเรียนค้นหาปัญหาที่เกิดขึ้นกับตนเองและต้องการแก้ไขคนละ 1 ปัญหา สามารถสรุปปัญหาของนักเรียนเป็น 3 ด้าน ได้แก่ 1) ด้านความต้องการพัฒนาความสามารถทางด้านกีฬาและความสามารถพิเศษ 2) 
      ด้านความต้องการพัฒนาผลสัมฤทธิ์ทางด้านการเรียนให้ดีขึ้น 3) ด้านความต้องการให้โรงเรียนมีสภาพแวดล้อมที่ดีขึ้น โดยครูได้แบ่งกลุ่มนักเรียนที่มีความต้องการในการแก้ปัญหาที่คล้ายๆกันให้อยู่กลุ่มเดียวกัน 
      หลังจากนั้นครูก็ติดตามดูแลนักเรียนพบปะพูดคุยให้คำปรึกษานักเรียนในทุกเช้าและเย็นโดยเราจะร่วมกันแก้ไขปัญหาที่เกิดขึ้นก่อนกลับบ้าน ครูใช้ความรัก ความเข้าใจ เอาใจใส่ติดตามและให้กำลังใจนักเรียนในทุกๆวัน
      <br><br>
      ✍️✍️✍️ ผลจากการทำโครงการเด็กดีของแผ่นดิน เด็กดีของครูที่มีปัญหาทั้ง 3 ด้าน มีการเปลี่ยนแปลงไปในทางที่ดีขึ้น คือ นักเรียนมีความอดทนในการออกกำลังกายทุกวัน นักเรียนอ่านหนังสือและทำการบ้านทุกวัน 
      นักเรียนช่วยกันเก็บขยะ รดน้ำต้นไม้ ดูแลความสะอาดของโรงเรียน ส่งผลให้นักเรียนทั้ง 19 คน มีการเปลี่ยนแปลงไปในทางที่ดีขึ้น ความสำเร็จที่เกิดขึ้นในครั้งนี้ เกิดจากการติดตาม ดูแล เอาใจใส่ 
      และที่สำคัญนักเรียนเข้าใจความเป็นตัวตนของนักเรียน ต้องการปรับปรุง เปลี่ยนแปลงแก้ไขปัญหาของตนเอง จากการจัดการเรียนรู้โดยใช้ปัญหาเป็นฐานและได้รับความร่วมมือจากเพื่อนๆในกลุ่มเดียวกัน 
      ในการให้คำปรึกษาเบื้องต้นการถ่ายภาพและการถ่ายคลิปต่างๆ ซึ่งการรับส่งข้อมูลของนักเรียน ครูได้ใช้แชทกลุ่มจาก Facebook และสร้างเพจเด็กดีของแผ่นดิน by  Kru wan เพื่อติดตามพัฒนาการของเด็กๆโดยครูให้
      กำลังใจ แนะนำ พูดคุยให้เด็กมีกำลังใจในการทำความดีต่อไปในทุกวันอย่างต่อเนื่อง โดยมีครูเป็นแบบอย่างของการทำความดี
      <br><br>
      ✍️✍️✍️✍️ สรุปผลจากการทำโครงการโครงการเด็กดีของแผ่นดิน ทำให้ครูมีความรักความห่วงใยลูกศิษย์มากขึ้น มีความภาคภูมิใจที่ได้เห็นลูกศิษย์ตั้งใจทำความดีต่อไปในทุกๆวันนักเรียนมีความรับผิดชอบต่อตนเอง
      และสังคมเพิ่มมากขึ้น ปฏิบัติตนในสังคมได้อย่างมีความสุข มีความคิดริเริ่มสร้างสรรค์ สามารถทำงานร่วมกันเป็นทีมได้ อีกทั้งนักเรียนกล้าคิด กล้าทำกล้าแสดงออก ในทางที่ถูกต้องและไม่อายที่จะทำความดีต่อไป
      <br><br>
      ✍️✍️ และท้ายที่สุดนี้ครูขอขอบพระคุณมูลนิธิครูดีของแผ่นดินที่ร่วมกันสร้างครูดีและเด็กดีไปพร้อมกัน ครูหวังว่าเราจะเป็นครูที่ดีและพร้อมที่จะสรรค์สร้างเด็กดีของแผ่นดินเพื่อพัฒนาประเทศชาติของเราต่อไป
      มาร่วมสร้างเด็กดีไปด้วยกันนะคะ
    </p>
  </main>
</div>
<!-- End Content 01 - คลังสื่อนวัตกรรม011 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear">
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Related Department</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Foundation of Thai Suprateacher</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End Copyright Tab -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Back2Top Button -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Back2Top Button -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>