

<?php
	session_start();
	require_once('condb.php');

	$ID = $_GET['ID'];
	$basic_score_status = $_GET['basic_score_status'];
	$basic_score_doc = $_GET['basic_score_doc'];
	$win_scroll = $_GET['win_scroll'];

	$ID_ad = $_SESSION['ID'];

	$sqlcheck = "SELECT * FROM `login` WHERE ID='$ID' ";
	$recheck = mysqli_query($con,$sqlcheck);
	$rowcheck = mysqli_fetch_array($recheck);

	if($basic_score_status == 'ปฏิเสธหนังสือรับรอง') {
		if(is_null($rowcheck['basic_score_remark']) || empty($rowcheck['basic_score_remark'])) {
			echo '<script>';
				echo "alert('กรุณาใส่เหตุผลที่ปฏิเสธที่ช่องหมายเหตุด้วยค่ะ ข้อความตรงนี้จะไปแสดงให้คุณครูรับทราบเพื่อดำเนินการแก้ไข และแนบเอกสารเข้ามาใหม่ค่ะ');";
				echo "window.location.replace('ระบบหลังบ้านครูขั้นพื้นฐาน.php?win_scroll=$win_scroll')";
			echo '</script>';
		}else{
			/* Update Status */
			$sql = "UPDATE `login` SET basic_score_status='ปฏิเสธหนังสือรับรอง' WHERE ID='$ID' ";
			$res= $con->query($sql) or die($con->error);
			/* Delete rejected basic_score_doc Status */
			$sqlde_doc = "UPDATE `login` SET basic_score_doc=NULL WHERE ID='$ID' ";
			$rede_doc = $con->query($sqlde_doc) or die($con->error);
			/* Log Admin Action */
			$sqllog = "INSERT INTO `adscorelog` (`ID_ad`,`adscorelog_task`,`ID_user`,`adscorelog_detail`) VALUES ('$ID_ad','ปฏิเสธหนังสือรับรอง','$ID','ขั้นพื้นฐาน') ";
			$relog = $con->query($sqllog) or die($con->error);

			header('location: ระบบหลังบ้านครูขั้นพื้นฐาน.php?win_scroll='.$win_scroll);
		}
	}else{		
		/* Update Status */
		$sql = "UPDATE `login` SET basic_score_status='$basic_score_status' WHERE ID='$ID' ";
		$res= $con->query($sql) or die($con->error);
		/* Log Admin Action */
		$sqllog = "INSERT INTO `adscorelog` (`ID_ad`,`adscorelog_task`,`ID_user`,`adscorelog_detail`) VALUES ('$ID_ad','Approve แล้ว','$ID','ขั้นพื้นฐาน') ";
		$relog = $con->query($sqllog) or die($con->error);
		
		header('location: ระบบหลังบ้านครูขั้นพื้นฐาน.php?win_scroll='.$win_scroll);
	}

?>