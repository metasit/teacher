<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		$basic_score4stu_system_id = 'B';
		$basic_score4stu_text = $_POST['basic_score4stu_text'];
		$basic_score4stu_file_no = $_POST['basic_score4stu_file_no'];

		$sqlbasic_score4stu_check_status = "SELECT basic_score4stu_check_status FROM `basic_score4stu` WHERE ID='$ID' AND basic_score4stu_system_id='$basic_score4stu_system_id' AND basic_score4stu_file_no='$basic_score4stu_file_no' ";
		$reBS4SCS = mysqli_query($con, $sqlbasic_score4stu_check_status);
		$rowBS4SCS = mysqli_fetch_array($reBS4SCS);

		$basic_score4stu_check_status = $rowBS4SCS['basic_score4stu_check_status'];
		
		if($basic_score4stu_check_status == NULL || is_null($basic_score4stu_check_status) || $basic_score4stu_check_status == '' || empty($basic_score4stu_check_status)) {
			
			$sql = "UPDATE `basic_score4stu` SET basic_score4stu_text='$basic_score4stu_text' WHERE ID='$ID' AND basic_score4stu_file_no='$basic_score4stu_file_no' ";
			$re = $con->query($sql) or die($con->error); //Check error
			
			/* Log User Action */
			$basic_score4stu_system_id = $_POST['basic_score4stu_system_id'];
			$scorelog_detail = 'เด็ก,ขั้นพื้นฐาน'.','.$basic_score4stu_system_id.','.$basic_score4stu_text;
			$scorelog_task = 'อัพเดทรายงานความดี'.$basic_score4stu_file_no;
			$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', '$scorelog_task', '$scorelog_detail') ";
			$re = $con->query($sql) or die($con->error); //Check error
			
			echo '<script>';
				echo "alert('อัพเดทรายงานความดีแล้วค่ะ');";
				echo "window.location.replace('แนบไฟล์ทำความดี-เด็ก.php')";
			echo '</script>';

		}else{

			/* Set $basic_score4stu_check_status */
			if(strpos($basic_score4stu_check_status, 'ปฏิเสธ,') !== false) {
				$basic_score4stu_check_status = 'แก้ไขรายงาน,'.date('d-m-Y H:i:s');
			}elseif(strpos($basic_score4stu_check_status, 'แก้ไขไฟล์,') !== false || strpos($basic_score4stu_check_status, 'แก้ไขรายงาน,') !== false) {

				if(strpos($basic_score4stu_check_status, 'แก้ไขไฟล์,') !== false) {
					$file_index = substr($basic_score4stu_check_status, strpos($basic_score4stu_check_status, 'แก้ไขไฟล์,'), 47).',';
				}else{
					$file_index = '';
				}

				$text_index = 'แก้ไขรายงาน,'.date('d-m-Y H:i:s');
				
				$basic_score4stu_check_status = $file_index.$text_index;
			}
			
			$sql = "UPDATE `basic_score4stu` SET basic_score4stu_text='$basic_score4stu_text', basic_score4stu_check_status='$basic_score4stu_check_status' WHERE ID='$ID' AND basic_score4stu_file_no='$basic_score4stu_file_no' ";
			$re = $con->query($sql) or die($con->error); //Check error
			
			/* Log User Action */
			$basic_score4stu_system_id = $_POST['basic_score4stu_system_id'];
			$scorelog_detail = 'เด็ก,ขั้นพื้นฐาน'.','.$basic_score4stu_system_id.','.$basic_score4stu_text;
			$scorelog_task = 'แก้ไขรายงานความดี'.$basic_score4stu_file_no;
			$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', '$scorelog_task', '$scorelog_detail') ";
			$re = $con->query($sql) or die($con->error); //Check error
			
			echo '<script>';
				echo "alert('แก้ไขรายงานความดีแล้วค่ะ');";
				echo "window.location.replace('แนบไฟล์ทำความดี-เด็ก.php')";
			echo '</script>';

		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
	
?>