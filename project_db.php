<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];
  $id = $_GET['id'];
  $sql_list  ="SELECT * FROM join_project as jp 
  LEFT JOIN join_project_pdf as jpp on jp.id_join_project = jpp.id_join_project
   WHERE jp.id_join_project = $id   ";
  $list = $con->query($sql_list);
  
  $num_count = 1;
  while($row = $list->fetch_assoc()){
    if($num_count == 1){
      $id_join_project = $row['id_join_project'];
      $title_menu_project = $row['title_menu']; 
      $title_project = $row['title_project'];
      $img_top_project = $row['img_top_project'];
      $detail = $row['detail'];
      $title_gallery = $row['title_gallery']; 
      $title_video = $row['title_video'];
      $link_video = trim($row['link_video']);
      $title_pdf = $row['title_pdf'];
      $link_program = $row['link_program']; 


    }
    if(explode(".",$row['name_file'])[1]=="png" || 
    explode(".",$row['name_file'])[1]=="jpg"  ||
     explode(".",$row['name_file'])[1]=="jpeg" || 
     explode(".",$row['name_file'])[1]=="gif" || 
     explode(".",$row['name_file'])[1]=="tif" ||
     explode(".",$row['name_file'])[1]=="tiff" ||
     explode(".",$row['name_file'])[1]=="bmp" 
     ){
      $name_file[] = $row['name_file'];
      
    }
    else{
      $name_show_link[] = $row['name_file'];
      $name_show[] = $row['name_show'];
    }
    $num_count++;
  }
  // var_dump($name_file);
  // exit();

?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
@font-face {
  font-family: 'korbauMed'; /*a name to be used later*/
  src:  url('content-setting/backend/font/korbau.ttf')  format('truetype') 
}
@font-face {
  font-family: 'Niramit'; /*a name to be used later*/
  src:  url('content-setting/backend/font/TH Niramit AS.ttf')  format('truetype') 
}
@font-face {
  font-family: 'CSChatThai'; /*a name to be used later*/
  src:  url('content-setting/backend/font/CSChatThai.ttf')  format('truetype') 
}
@font-face {
  font-family: 'Emmal'; /*a name to be used later*/
  src:  url('content-setting/backend/font/EmmaliDEMO-Thin.otf')  format('truetype') 
}
p.big {
line-height: 30px;
}
.one_first, .one_half, .one_third, .two_third, .one_quarter, .two_quarter, .three_quarte{
  margin:0 0 0 0!important;
  margin-bottom:30px!important;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php 
include('includes/headerTop.php'); 
?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="all_project.php"> ร่วมโครงการ</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="โครงการเด็กดีของแผ่นดิน.php"> <?php  echo $title_menu_project; ?></a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - โครงการเด็กดีของแผ่นดิน -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <!-- ################################################################################################ -->
    <article style="text-align:center; padding-bottom:20px; border-bottom:3px solid #59A209;">
      <!-- ################################################################################################ -->
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px">
      <strong>
        <?php  echo $title_project; ?>
        <!-- เด็กดีของแผ่นดิน (Z-iDol) -->
      </strong></p>
      <img src="<?php  echo "content-setting/backend/join_project/image/".$img_top_project; ?>" alt="เด็กดีของแผ่นดิน Logo">
      <p class="fs-32" style="font-family:RSUText; line-height:30pt; text-align:left;">
        <?php  echo $detail; ?>
      <!-- เด็กรุ่นใหม่ ที่เรียกกันว่า Generation Z หรือ Gen Z คือ นิยามของเด็กที่เกิดหลังปี ค.ศ. 1995 หรือ ปี พ.ศ. 2538 ลงมา เด็กกลุ่มนี้เติบโตมาพร้อมกับสิ่งอำนวยความสะดวกมากมาย แวดล้อมด้วยการเรียนรู้
        และการดำเนินชีวิตในสังคมดิจิทัล การติดต่อสื่อสารไร้สาย และสื่อสมัยใหม่ ซึ่งสิ่งต่างๆ เหล่านี้มีคุณอนันต์ แต่ก็มีโทษมหันต์หากใช้ไม่ถูกทิศทาง ซึ่งการดูแลเด็กยุคนี้ ต้องใช้ “ความรัก” “ความเข้าใจ” 
        บนพื้นฐานของความ “หลากหลาย” และ “แตกต่าง” จะใช้การบังคับอย่างที่ผ่านๆมามิได้อีกต่อไป
        <br><br>
        โครงการ <strong>“เด็กดีของแผ่นดิน”</strong> จึงเกิดขึ้น โดยให้ครูดีของแผ่นดิน ได้สร้างเด็กดีของแผ่นดิน หรือชื่อภาษาอังกฤษว่า Z-iDol คือ เด็กและเยาวชนรุ่นใหม่ ในยุค Generation Z ที่สามารถค้นพบความต้องการ
        ของตนเอง สามารถพัฒนาตนเองตามความต้องการนั้น จนประสบความสำเร็จในโลกศตวรรษที่ 21 และเป็นแบบอย่างที่ดีให้กับเพื่อนๆ ได้อย่างภาคภูมิใจ 
        <br><br>
        ในมุมมองของการพัฒนาและปรัชญา เด็กดีของแผ่นดิน หรือ Z-iDol คำว่า Z (ซี) พ้องเสียงกับคำว่า See ที่แปลว่าเห็น ซึ่งสอดคล้องกับปรัชญาของการศึกษาที่ การจะพัฒนาตนเองได้นั้น 
        ต้องเริ่มจากการมองตนเอง เห็นตนเอง เข้าใจตนเอง จึงจะพัฒนาตนเองได้ และสอดคล้องกับความหมายของคำว่า ศึกษา ที่มาจากภาษาบาลีว่า สิกขา ซึ่งมาจาก ส กับ อิกข และอาปัจจัย ที่แปลว่า 
        การเห็นตนเอง นั่นเอง โดยจะต้องมองตนเองก่อน ทั้งในเรื่องพฤติกรรม (ศีล) ความคิด ความตั้งใจ (สมาธิ) และสติปัญญา (ปัญญา) ซึ่งเป็นหัวใจสำคัญในการพัฒนาทรัพยากรมนุษย์ให้มีคุณค่าในทุกระดับชั้น
        <br><br>
        ซึ่งการจะพัฒนาตนเองได้อย่างยั่งยืนนั้น โครงการมุ่งเน้นไปที่การเปลี่ยนแปลงนิสัย ไม่ว่าจะเป็นการละเลิกนิสัยที่ไม่ดี หรือสร้างนิสัยดีๆ ให้เกิดขึ้น หรือการทำจิตใจให้เข้มแข็ง (ละชั่ว ทำดี ทำจิตใจให้ผ่องใส) 
        โดยใช้ทฤษฎีเปลี่ยนนิสัยใน 21 วัน จากนั้นขยายผลสู่การมีอาชีพ มีงานทำ ตามพระบรมราโชบายด้านการศึกษาของพระบาทสมเด็จพระวชิรเกล้าเจ้าอยู่หัว รวมถึงทักษะชีวิตในศตวรรษที่ 21 เช่น ความคิดสร้างสรรค์ 
        การทำงานเป็นทีม เป็นต้น ไม่ใช่เรียนดี กิจกรรมเด่น คุณธรรมเลิศ แต่ไม่สามารถเลี้ยงชีพได้ ช่วยเหลือตัวเองไม่ได้ ช่วยเหลือผู้อื่นไม่ได้ หากแต่ต้องดี เก่ง และประกอบสัมมาอาชีพได้ นำมาซึ่งประสิทธิภาพและ
        ประสิทธิผลสูงสุด นั่นคือ ความสุขและความสำเร็จในชีวิตที่แท้จริงนั่นเอง -->
      </p>
    </article>
    <!-- ################################################################################################ -->
    <?php  
      if($title_video  && $link_video && $link_video !='title="YouTube' ){
    ?>
    <article style="text-align:center; padding-bottom:20px; border-bottom:3px solid #59A209;">
  
    <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px;">
        <div>
          <span style="color:rgb(180,147,31); line-height:80px; font-size:3rem;">
            <strong><?php echo $title_video; ?></strong>
          </span>
          
        
        </div>
        
        <iframe width="560" height="317" 
        src=<?php echo $link_video;  ?> 
        title="YouTube video player" 
        frameborder="0" 
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
        allowfullscreen>
      </iframe>



        <!-- <iframe width="640px" height="360px" src="<?php echo $link_video;  ?>"></iframe> -->
    </p>
    </article>

    <?php } ?>
    

    <div style="border-bottom:3px solid #59A209;">
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px;">
        <strong><?php echo $title_gallery; ?></strong></span>
      </p>
      <div id="gallery" style="margin-top:50px; height:auto; ">
        <ul class="nospace clear">
          <?php for ($x=1; $x <count($name_file)+1; $x++) { 
            
          ?>
          <!-- one_third first zoom11 -->
          <li  style="margin-right:20px;!important" class="<?php if($x%3 == 1 || $x==1) echo "one_third first zoom11";  else echo "one_third  zoom11";    ?>">
            <a href="<?php  echo "content-setting/backend/join_project/image/".$name_file[$x-1]; ?>">
            <!-- <img src="images/ขั้นตอนร่วม ZiDol01.jpg" alt="ขั้นตอนร่วม ZiDol01"> -->
            <img  style="min-width:350px; min-height:200px; max-width:450px; max-height:300px;  margin-right:20px;  " src="<?php  echo "content-setting/backend/join_project/image/".$name_file[$x-1]; ?>" >
            
          </a>
          </li>
        <?php  } ?>
        </ul>
      </div>
    </div>
    <!-- ################################################################################################ -->
    <ul class="fs-32 textlink" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
      <!-- <li><a href="images/เด็กดีของแผ่นดิน Logo.jpg">สัญลักษณ์เด็กดีของแผ่นดิน</a></li> -->
      <!-- <li><a href="docs/ร่วมโครงการ/เด็กดี/รายละเอียดโครงการเด็กดี.pdf">ดาวน์โหลดรายละเอียดโครงการ</a></li> -->
      <li><?php echo $title_pdf;
      ?>
        <!-- ต้นกล้าแห่งแผ่นดิน 6 แบบ -->
        <ul style="list-style-type:disc; text-decoration:none;">
        <?php 
          for ($x=0; $x <count($name_show) ; $x++) { 
            # code...
            
        ?>
          <li><a download href="<?php  echo "content-setting/backend/join_project/pdf/".$name_show_link[$x];  ?>">
          <?php echo 
          explode(".",$name_show[$x])[0];  ?></a></li>
        <?php  } ?>
     
          <!-- <li><a href="images/ต้นกล้าของแผ่นดิน/แบบที่ 2.jpg">แบบที่ 2</a></li>
          <li><a href="images/ต้นกล้าของแผ่นดิน/แบบที่ 3.jpg">แบบที่ 3</a></li>
          <li><a href="images/ต้นกล้าของแผ่นดิน/แบบที่ 4.jpg">แบบที่ 4</a></li>
          <li><a href="images/ต้นกล้าของแผ่นดิน/แบบที่ 5.jpg">แบบที่ 5</a></li>
          <li><a href="images/ต้นกล้าของแผ่นดิน/แบบที่ 6.jpg">แบบที่ 6</a></li> -->
        </ul>
			</li>
			<!-- <li><a href="docs/จดหมาย/จดหมาย17/จม.63ผู้เกี่ยวข้อง.pdf">หนังสือแจ้งปฏิทินการดำเนินโครงการฯ ปี 2563</a></li> -->
			<!-- <li style="text-align: center;"><a href="https://www.instagram.com/zidol_2563/?hl=th"><img src="images/ZiDol_IG.jpg" alt="ZiDol_IG"><br>กดติดตามได้ที่นี้</a></li> -->
    </ul>

      <!-- <div class="one_first m-t-50">
          <div class="btnJoin">
            <a href="เกี่ยวกับการบริจาค.php"><h6>สนับสนุนโครงการ</h6></a>
          </div>
      </div> -->
		<!-- ################################################################################################ -->



		<!-- <div class="one_first">
			<div class="btnJoin" style="background-color: gray">
				<a href="images/ประกาศปิดโครงการ.jpg" target="_blank"><h7>โครงการเด็กดีของแผ่นดิน</h7></a>
			</div>
		</div> -->





		<?php
			$sqllogin = "SELECT * FROM `login` 
			WHERE (ID='$ID' AND occup_code LIKE 'OcF%') 
			OR (ID='$ID' AND occup_code IS NULL)
			OR (ID='$ID' AND occup_code='') ";
			$relogin = mysqli_query($con,$sqllogin);
			$rowlogin = mysqli_fetch_array($relogin);

			if(mysqli_num_rows($relogin) != 0 || !isset($ID)) { ?>
				<div class="one_first">
					<div class=""> <?php
						// if(strpos($rowlogin['basic_score_status'], 'Approve แล้ว') !== false) { // กรณีที่ได้รับการ Approve ให้เข้าหน้า Print เกียรติบัตร
             ?>
							<!-- <a href="popPrintbasic_score4stu_cer.php"><h7 class="blink3">ผ่านโครงการเด็กดีของแผ่นดินขั้นพื้นฐานแล้วค่ะ</h7></a> -->
               <?php
						// }elseif(strpos($rowlogin['basic_score_status'], 'ยืนยันแล้ว') !== false) { // กรณีที่มีการยืนยันตัวตนแล้ว ให้เข้าหน้า registZidol.php
             ?>
							<!-- <a href="registZidol.php"><h6 class="blink3">โครงการเด็กดีของแผ่นดินขั้นพื้นฐาน</h6></a> -->
               <?php
						}else{ // กรณีที่เข้ามาครั้งแรก ให้เข้าหน้ายืนยันตัวตน ที่หน้า registZidol_step1.php ?>
							<!-- <a href="registZidol_step1.php"><h6>เข้าร่วมโครง <?php echo $title_menu;  ?> ขั้นพื้นฐาน</h6></a> -->
               <?php
						}
            
						// NOTE: ที่ใช้ strpos ในการกรอง เพราะอยากจะให้เคสที่ไม่มีคำว่า "Approve แล้ว" ทั้งหมดไปเข้า Loop ที่2 แต่ถ้ามีคำว่า "Approve แล้ว" ใน basic_score_status ก็ให้เข้าหน้า print เกียรติบัตร ?>
			<!-- <a href="registZidol_step1.php"><h6>เข้าร่วมโครง <?php echo $title_menu;  ?> ขั้นพื้นฐาน</h6></a>	 -->
  
    
    <?php
      $id = $_GET['id'];



            if($id=="21"){
    ?>

<?php


			$sqllogin = "SELECT * FROM `login` 
			WHERE (ID='$ID' AND occup_code LIKE 'OcAA%' AND basic_score_status='Approve แล้ว/ยืนยันแล้ว') 
			OR (ID='$ID' AND occup_code LIKE 'OcAB%' AND basic_score_status='Approve แล้ว/ยืนยันแล้ว') ";
			$relogin = mysqli_query($con, $sqllogin);
			$rowlogin = mysqli_fetch_array($relogin);
				
			if(mysqli_num_rows($relogin) != 0) {
				$fifth_score_status = $rowlogin['fifth_score_status'];

				if($fifth_score_status == 'Approve แล้ว') { // กรณีที่ครูผ่านครูดีชั้นที่ 5 แล้ว ?>
					<div class="one_first">
						<div class="btnJoin">
							<a href="popPrintfifth_score_cer-ครู.php"><h7 class="blink3">ยินดีด้วยค่ะ ท่านพิมพ์เกียรติบัตรชั้นที่ 5 ได้แล้วค่ะ</h7></a>
						</div>
					</div> <?php
				}else{ // กรณีที่ยังไม่ผ่านครูดีชั้นที่ 5 ?>
					<div class="one_first">
						<div class="btnJoin">
            <a href="director-lobby.php"><h6>เข้ารับการพิจารณาครูดีของแผ่นดินชั้นที่ 5</h6></a>
							<!-- <a href="โครงการครูดีของแผ่นดินชั้นที่5-lobby.php"><h6>เข้ารับการพิจารณาครูดีของแผ่นดินชั้นที่ 5</h6></a> -->
						</div>
					</div> 
					<?php
				}
			}
		?>





      <!-- <div class="one_first m-t-50 " style="margin-bottom:50px;">
        <div class="btnJoin">
          <a href="director-lobby.php"><h6><?php  echo $title_menu_project; ?></h6></a>
        </div>
      </div> -->
    <?php 
  } ?>


    <?php 
      if($id=="23"){
    ?>
    <div class="one_first">
      <div class="btnJoin">
        <a href="popWaitTraining.php"><h6>สมัครเข้าร่วมโครงการ</h6></a>
      </div>
    </div>


    <?php }  ?>

    <?php
			 if($id=="29"){
      $sqllogin = "SELECT * FROM `login` 
			WHERE (ID='$ID'AND occup_code LIKE 'OcAA%' AND basic_score_status='Approve แล้ว/ยืนยันแล้ว') 
			OR (ID='$ID'AND occup_code LIKE 'OcAA%' AND affil_code!='')
			OR (ID='$ID'AND occup_code LIKE 'OcAA%' AND affil_code IS NOT NULL)
			OR (ID='$ID' AND occup_code LIKE 'OcAB%' AND basic_score_status='Approve แล้ว/ยืนยันแล้ว')
			OR (ID='$ID'AND occup_code LIKE 'OcAB%' AND affil_code!='')
			OR (ID='$ID'AND occup_code LIKE 'OcAB%' AND affil_code IS NOT NULL) ";
			$relogin = mysqli_query($con, $sqllogin);

        ?>

    <div class="one_first m-t-50">
      <div class="btnJoin">
        <a href="https://beinternetawesome.withgoogle.com/th_th"><h6>เข้าสู่เว็ปไซต์ Be Internet Awesome by Google</h6></a>
      </div>
		</div>

  <?php
			if(mysqli_num_rows($relogin) != 0 || !isset($ID)) { ?>
				<div class="one_first">
					<div class="btnJoin">
						<a href="มอบเกียรติบัตรBIA_step1.php"><h6>รับเกียรติบัตร Be Internet Awesome</h6></a>
					</div>
				</div> <?php
			}
        ?>

  <?php 
    }
		?>

<?php
      $id = $_GET['id'];
      if($id=="21"){

			$sqllogin = "SELECT * FROM `login` 
			WHERE (ID='$ID' AND occup_code LIKE 'OcAA%') 
			OR (ID='$ID' AND occup_code LIKE 'OcAB%') 
			OR (ID='$ID' AND occup_code IS NULL)
			OR (ID='$ID' AND occup_code='') ";
			$relogin = mysqli_query($con, $sqllogin);
			$rowlogin = mysqli_fetch_array($relogin);

			if(mysqli_num_rows($relogin) != 0 || !isset($ID)) { ?>
				<div class="one_first m-t-50">
					<div class="btnJoin">
						<?php
							if($rowlogin['basic_score_status'] == 'รอแนบเอกสาร') {
								?> <a href="popCongratbasic_score-ครู.php"><h6 class="blink3">กดเพื่อแนบหนังสือรับรองขั้นพื้นฐาน</h6></a> <?php
							}elseif($rowlogin['basic_score_status'] == 'กำลังตรวจสอบ') {
								?> <a href="#" onclick="return false" style="cursor:context-menu"><h7 class="blink3">เจ้าหน้าที่กำลังตรวจสอบหนังสือรับรองขั้นพื้นฐาน</h7></a> <?php
							}elseif($rowlogin['basic_score_status'] == 'ปฏิเสธหนังสือรับรอง') {
								?> <a href="popRejectbasic_score_doc-ครู.php"><h7 class="blink3">ท่านถูกปฏิเสธหนังสือรับรองขั้นพื้นฐาน กดเพื่อดูรายละเอียด</h7></a> <?php
							}elseif($rowlogin['basic_score_status'] == 'Approve แล้ว' || $rowlogin['basic_score_status'] == 'Approve แล้ว/ยืนยันแล้ว') {
								if($rowlogin['basic_score_remark'] == 'Fast-Track') { ?>
									<a href="popPrintbasic_score_cer_step1-ครู.php"><h7 class="blink3">กรุณาตรวจสอบข้อมูลของท่านให้ถูกต้อง</h7></a> <?php
								}else{ ?>
									<a href="popPrintbasic_score_cer_step1-ครู.php"><h7 class="blink3">ยินดีด้วยค่ะ ท่านพิมพ์เกียรติบัตรขั้นพื้นฐานได้แล้วค่ะ</h7></a> <?php
								}
							}else{
								?> <a href="ประเมินครูดีของแผ่นดินขั้นพื้นฐาน.php"><h6>ประเมินครูดีของแผ่นดิน ขั้นพื้นฐาน</h6></a> <?php
							}
						?>
					</div>
				</div> <?php

			}else{ 
        ?>
				<div class="one_first m-t-50">
					<div class="btnJoin">
						<a href="#" onclick="return false" style="color:gray"><h7>ผู้เข้าร่วมโครงการนี้ต้องเป็นผู้บริหารสถานศึกษาและครูเท่านั้น </h7></a>
					</div>
				</div> 
    <?php
			}
    }
		?>



<?php 
          if(in_array("ID",array_keys($_SESSION)))
          $ID = $_SESSION['ID'];
    			$sqllogin = "SELECT * FROM `login` WHERE (ID='$ID' AND occup_code LIKE 'OcAD%' AND basic_score_status='Approve แล้ว/ยืนยันแล้ว') ";
          $relogin = mysqli_query($con, $sqllogin);
          $rowlogin = mysqli_fetch_array($relogin);
    			// var_dump(mysqli_num_rows($relogin));
          // echo ;
          // var_dump($rowlogin);



          
          
          if($id=="22"){
            if(mysqli_num_rows($relogin) != 0) {
              if($rowlogin['honor_score_status'] == 'Approve แล้ว') { ?>
                <div class="one_first">
                  <div class="btnJoin">
                    <a href="popPrinthonor_score_cer-ศน.php">
                      <h7 class="blink3">ยินดีด้วยค่ะ ท่านพิมพ์เกียรติบัตรขั้นเกียรติคุณได้แล้วค่ะ</h7>
                    </a>
                  </div>
                </div> <?php
              }else{ ?>
                <div class="one_first">
                  <div class="btnJoin">
                    <a href="โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php"><h6>ศึกษานิเทศก์ดีของแผ่นดิน ระดับเกียรติคุณ</h6></a>
                  </div>
                </div> <?php
              }
            }
          }
    ?>
    <?php 
      if($id=="47"){
    ?>
      <!-- <div class="one_first">
        <div class="btnJoin">
          <a href="โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php"><h6>ผู้อำนวยการสถานศึกษาดี</h6></a>
        </div>
      </div> -->
    <?php } ?>

    <?php
    
        
    ?>


          

		<?php
      $id = $_GET['id'];
        if($id=="19"){
          $ID = $_SESSION['ID'];
          $sqllogin = "SELECT * FROM `login` 
          WHERE (ID='$ID' AND occup_code LIKE 'OcF%') 
          OR (ID='$ID' AND occup_code IS NULL)
          OR (ID='$ID' AND occup_code='') ";
          $relogin = mysqli_query($con,$sqllogin);
          $rowlogin = mysqli_fetch_array($relogin);
    
          // if( !isset($id)) {
             ?>
            <div class="one_first m-t-50">
              <div class="btnJoin"> <?php
                if(strpos($rowlogin['basic_score_status'], 'Approve แล้ว') !== false) { // กรณีที่ได้รับการ Approve ให้เข้าหน้า Print เกียรติบัตร ?>
                  <a href="popPrintbasic_score4stu_cer.php"><h7 class="blink3">ผ่านโครงการเด็กดีของแผ่นดินขั้นพื้นฐานแล้วค่ะ</h7></a> <?php
                }elseif(strpos($rowlogin['basic_score_status'], 'ยืนยันแล้ว') !== false) { // กรณีที่มีการยืนยันตัวตนแล้ว ให้เข้าหน้า registZidol.php ?>
                  <a href="registZidol.php"><h6 class="blink3">โครงการเด็กดีของแผ่นดินขั้นพื้นฐาน</h6></a> <?php
                }else{ // กรณีที่เข้ามาครั้งแรก ให้เข้าหน้ายืนยันตัวตน ที่หน้า registZidol_step1.php ?>
                  <a href="registZidol_step1.php"><h7>ประเมินเด็กดีของแผ่นดีของแผ่นดินขั้นพื้นฐาน ขั้นพื้นฐาน</h7></a> 
                  <?php
                }
                // NOTE: ที่ใช้ strpos ในการกรอง เพราะอยากจะให้เคสที่ไม่มีคำว่า "Approve แล้ว" ทั้งหมดไปเข้า Loop ที่2 แต่ถ้ามีคำว่า "Approve แล้ว" ใน basic_score_status ก็ให้เข้าหน้า print เกียรติบัตร ?>
              </div>
            </div> <?php
          // }

          
        }  
		?>
    <!-- <div class="one_first">
      <div class="btnJoin">
        <a href="โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php"><h6>ศึกษานิเทศก์ดีของแผ่นดิน ระดับเกียรติคุณศึกษานิเทศก์ดีของแผ่นดิน ระดับเกียรติคุณ</h6></a>
      </div>
		</div>  -->


    </div>
   
				</div> <?php
			
		?>
    <div class="one_first m-t-50 mb-4">
      <div class="btnJoin">
        <a href="เกี่ยวกับการบริจาค.php"><h6>สนับสนุนมูลนิธิ</h6></a>
      </div>
    </div>

    <!-- ################################################################################################ -->
  </main>
</div>


<!-- End Content 01 - โครงการเด็กดีของแผ่นดิน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>