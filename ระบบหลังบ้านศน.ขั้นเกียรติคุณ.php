<?php
	session_start();
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		
		if(strpos($_SESSION['permis_system'], '*all*') !== false || strpos($_SESSION['permis_system'], '*B00*') !== false || strpos($_SESSION['permis_system'], '*B02*') !== false) {
			require_once('condb.php');
			$email = $_SESSION['email'];
		}else{
			header("Location: javascript:history.go(-1);");
		}
		
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
	
	<style>
		.score-input-box {
			width: 50px;
			height: 25px;
			border-color: rgb(200,200,200);
			border-radius: 5px;
			text-align: center;
			font-size: 18px;
		}
	</style>
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['winScroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ระบบหลังบ้านศึกษานิเทศก์ดีขั้นเกียรติคุณ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ระบบหลังบ้านศึกษานิเทศก์ดีขั้นเกียรติคุณ -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row" style="margin:0 20px; overflow-x:auto;">
			<div class="col-lg-12">
				<div class="table-main table-responsive" style="margin-bottom:200px">
					<!-- ################################################################################################ -->
					<table class="table" style="background-color:rgb(240,240,240);">
						<thead>
							<tr>
								<th>ลำดับ</th>
								<th>ชื่อ-นามสกุล</th>
								<th>วันที่ศน.ส่งเรื่อง</th>
								<th>สถานะ</th>
								<th class="BG-blue1">ศน.พื้นฐาน</th>
								<th class="BG-blue1">รายงานนิเทศ</th>
								<th class="BG-blue1">ประเมิน<br>ขั้นเกียรติคุณ</th>
								<th class="BG-blue1">เปลี่ยนแปลง<br>เชิงประจักษ์</th>
								<?php
									if(strpos($_SESSION['permis_system'], '*B021*') !== false) { ?>
										<th class="BG-gold1">คะแนน</th> <?php
									}else{ ?>
										<th class="BG-gold1">คะแนน1</th>
										<th class="BG-gold1">คะแนน2</th>
										<th class="BG-gold1">คะแนน3</th> <?php
									}
								?>
								<th class="BG-gold1">หมายเหตุ</th>
							</tr>
						</thead>
						<tbody class="disable-margin">
							<?php
								/* Set number product per page */
								$results_per_page = 50;
								if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
								$start_from = ($page-1) * $results_per_page;

								/* Call login table data from mySQL */
								$sql = "SELECT ID, email, firstname, lastname, basic_score_remark, honor_score_status, honor_score_remark FROM `login` 
								WHERE (occup_code LIKE 'OcAD%' AND honor_score_status='ส่งให้มูลนิธิตรวจสอบ') 
								OR (occup_code LIKE 'OcAD%' AND honor_score_status='Approve แล้ว') 
								OR (occup_code LIKE 'OcAD%' AND honor_score_status='ปฏิเสธ') 
								ORDER BY FIELD(honor_score_status, 'Approve แล้ว') ASC, honor_score_date DESC LIMIT $start_from, $results_per_page ";
								$re = mysqli_query($con, $sql);

								$n = $start_from+1; // Get number of row // ใช้ $n เพราะหน้านี้ calhonor_score-ศน.php มีใช้ตัวแปล $i ไปแล้ว
								while($row = $re->fetch_assoc()) {
									
									$ID_sup = $row['ID'];
									$sup_name = trim($row['firstname']).' '.trim($row['lastname']);
									$sup_email = $row['email'];

									/* เช็คว่า ได้ขั้นพื้นฐานมาเมื่อไหร่ honor_score4sup_part1 */
									// เรียก basic_score_remark เพื่อดูว่า เป็นกลุ่ม Fast-Track หรือไม่
									$basic_score_remark = $row['basic_score_remark'];

									// เรียก adscorelog เพื่อดูว่า ได้รับศน.ดีขั้นพื้นฐานมาเมื่อไหร่
									$sqladscorelog = "SELECT adscorelog_date FROM `adscorelog` WHERE ID_user='$ID_sup' AND adscorelog_task='Approve แล้ว' AND adscorelog_detail='ขั้นพื้นฐาน' ORDER BY adscorelog_id DESC LIMIT 1 ";
									$readscorelog = mysqli_query($con,$sqladscorelog);
									$rowadscorelog = mysqli_fetch_array($readscorelog);

									$check_basic_score = mysqli_num_rows($readscorelog);
									if($check_basic_score == 0) {
										if($basic_score_remark == 'Fast-Track') { // ถ้าไม่มี ให้เช็คอีกว่า เป็นกลุ่ม Fast-Track หรือไม่
											$basic_score_date = 'Fast-Track';
											$basic_score_status = 'Fast-Track';
											$expire_basic_score_date = 'Fast-Track';
										}else{ // ถ้าไม่มี แสดงว่า ไม่เคยได้รับการ Approve ครูขั้นพื้นฐานมาเลย
											$basic_score_date = 'ไม่พบข้อมูลในระบบ';
											$basic_score_status = 'ไม่พบข้อมูลในระบบ';
											$expire_basic_score_date = 'ไม่พบข้อมูลในระบบ';
										}
									}else{
										/* Find and Set the latest basic_score_date */
										$date = new DateTime($rowadscorelog['adscorelog_date']);
										$basic_score_date = $date->format("d-m-Y"); // Set date format
										/* Set the expire_basic_score_date */
										$expire_basic_score_date = $date->add(new DateInterval('P1Y1D')); // Find expire date for basic_score		
										/* Check that this user basic_score expire? and Set $basic_score_status */
										$date_today = new DateTime('today');
										if($expire_basic_score_date >= $date_today) {
											$basic_score_status = 'ได้รับการอนุมัติ'; // Set basic_score_status to Approved
										}else{
											$basic_score_status = 'หมดอายุ';
										}
										$expire_basic_score_date = $date->format("d-m-Y"); // Set date format
									}
									////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
									/* เช็คว่า มีการแนบการนิเทศมาครบ 12 หรือยัง honor_score4sup_part2 */
									$sqlhonor_score4sup_codeB = "SELECT * FROM `honor_score4sup` WHERE ID='$ID_sup' AND honor_score4sup_code LIKE 'B%' ";
									$reHS4S = mysqli_query($con, $sqlhonor_score4sup_codeB);
									
									$countrow_B = mysqli_num_rows($reHS4S);
									////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
									/* เช็คว่า ประเมินขั้นเกียรติคุณ ผ่านรึยัง honor_score4sup_part3 */
									/*
									$sqlhonor_score4sup_codeA = "SELECT * FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code='A' ";
									$reA = mysqli_query($con, $sqlhonor_score4sup_codeA);
									$rowA = mysqli_fetch_array($reA);
									$check_37 = $rowA['honor_score4sup_data1']; // Check that user was done all 37 question in evaluation?

									$date1 = new DateTime($rowA['honor_score4sup_date']);
									$honor_score4sup_date = $date1->format('d-m-Y');

									$date2 = new DateTime($rowA['honor_score4sup_date']);
									$date2 = $date2->add(new DateInterval('P15D'));
									$honor_score4sup_date_again = $date2->format('d-m-Y');
									*/
									////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
									/* เช็คว่า มีการส่งการเปลี่ยนแแปลงเชิงประจักษ์ หรือยัง honor_score4sup_part4 */
									$sqlhonor_score4sup_codeD = "SELECT honor_score4sup_data1, honor_score4sup_data2, honor_score4sup_data3, honor_score4sup_data4 FROM `honor_score4sup` WHERE ID='$ID_sup' AND honor_score4sup_code='D' ";
									$reD = mysqli_query($con, $sqlhonor_score4sup_codeD);
									$rowD = mysqli_fetch_array($reD);

									$honor_score4sup_part4_1 = $rowD['honor_score4sup_data1'];
									$honor_score4sup_part4_2 = $rowD['honor_score4sup_data2'];
									$honor_score4sup_part4_3 = $rowD['honor_score4sup_data3'];
									$honor_score4sup_part4_4 = $rowD['honor_score4sup_data4'];
									////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
									/* เช็คว่า ทั้ง4part ผ่านรึยัง */
									// Part 1
									if($basic_score_status == 'ได้รับการอนุมัติ' || $basic_score_status == 'Fast-Track') {
										$honor_score4sup_part1 = 'Pass';
									}
									// Part 2
									if($countrow_B == 12) {
										$honor_score4sup_part2 = 'Ready'; // ที่ใช้คำว่า Ready ไม่เหมือนครูชั้น 5 ที่ใช้คำว่า Pass เพราะของศน.เป็นแค่สถานะการแนบไฟล์ครบ12ไฟล์เท่านั้น จึงสามารถส่งให้มูลนิธิตรวจสอบได้ แต่ของครูชั้น5 การที่ส่วนนี้จะผ่าน จำนวนเด็กที่ทำพื้นฐานต้องผ่านตามกำหนดจริง ซึ่งเป็นส่วนที่มูลนิธิอนุมัติมาแล้ว
									}
									// Part 3
									$ID = $ID_sup;
									include('includes/calhonor_score-ศน.php');
									$ID = $_SESSION['ID'];
									if($numberpass_sub >= 1) {
										$honor_score4sup_part3 = 'Pass';
									}
									// Part 4
									if(mysqli_num_rows($reD) == 1) {
										$honor_score4sup_part4 = 'Ready';
									} ?>



									<tr>
										<!-- No. -->
										<td class="price-pr">
											<p><?php echo $n ?></p>
										</td>
										<!-- Supervisor Name -->
										<td class="name-pr lh-1-0">
											<p><?php echo $row['firstname'].'<br>'.$row["lastname"]; ?></p>
										</td>
										<!-- วันที่ศน.ส่งมาให้ตรวจ -->
										<td class="price-pr">
											<?php
												$sqlscorelog = "SELECT scorelog_date FROM `scorelog` WHERE ID='$ID_sup' AND scorelog_task='ส่งให้มูลนิธิตรวจสอบ' AND scorelog_detail='ศน.,เกียรติคุณ,ทำ3ส่วนครบแล้ว' ORDER BY scorelog_date DESC LIMIT 1";
												$rescorelog = mysqli_query($con, $sqlscorelog);
												$rowscorelog = mysqli_fetch_array($rescorelog); ?>
												<p><?php echo date("d-m-Y", strtotime($rowscorelog['scorelog_date'])); ?></p>
										</td>
										<!-- Status -->
										<td class="name-pr">
											<?php
												if($row['honor_score_status']  == 'ส่งให้มูลนิธิตรวจสอบ') { ?>
													<p>รอมูลนิธิตรวจสอบ</p> <?php
												}elseif($row['honor_score_status'] == 'ปฏิเสธ') { ?>
													<p>ถูกปฏิเสธ</p> <?php
												}elseif($row['honor_score_status'] == 'Approve แล้ว') { ?>
													<p style="color:rgb(72,160,0);"><i class="fas fa-check-circle"></i><br>ผ่านแล้ว</p> <?php
												}
											?>
										</td>
										<!-- ศน.พื้นฐาน -->
										<td class="name-pr">
											<?php
												if($honor_score4sup_part1 == 'Pass') { ?>
													<p>ผ่านแล้ว <?php if($basic_score_status == 'Fast-Track') {echo '(FT)';} ?></p> <?php
												}else{ ?>
													<p>-</p> <?php
												}
											?>
										</td>
										<!-- รายงานการนิเทศ -->
										<?php
											if($honor_score4sup_part2 == 'Ready') { ?>
												<td class="slip-upload-btn">
													<a href="รายงานการนิเทศ-admin.php?ID_sup=<?php echo $ID_sup; ?>&sup_name=<?php echo $sup_name; ?>&sup_email=<?php echo $sup_email; ?>" target="_blank">ดู</a>
												</td> <?php
											}else{ ?>
												<td class="name-pr">
													<p>รอส่งข้อมูล</p>
												</td> <?php
											}
										?>
										<!-- ประเมินขั้นเกียรติคุณ -->
										<td class="name-pr">
											<?php
												if($honor_score4sup_part3 == 'Pass') { ?>
													<p>ผ่านแล้ว</p> <?php
												}else{ ?>
													<p>-</p> <?php
												}
											?>
										</td>
										<!-- การเปลี่ยนแปลงเชิงประจักษ์ 1 พฤติกรรม/โครงการ/กิจกรรม -->
										<?php
											if($honor_score4sup_part4 == 'Ready') { ?>
												<td class="slip-upload-btn">
													<a href="การเปลี่ยนแปลงเชิงประจักษ์-admin.php?ID_sup=<?php echo $ID_sup; ?>&sup_name=<?php echo $sup_name; ?>&sup_email=<?php echo $sup_email; ?>" target="_blank">ดู</a>
												</td> <?php
											}else{ ?>
												<td class="name-pr">
													<p>รอส่งข้อมูล</p>
												</td> <?php
											}
										?>
										<!-- คะแนน -->
										<?php
											if(strpos($_SESSION['permis_system'], '*B021*') !== false) {
												/* Count number of honor_score4sup_code LIKE 'G%' rows */
												$sqlhonor_score4sup_codeG = "SELECT * FROM `honor_score4sup` WHERE ID='$ID_sup' AND honor_score4sup_code LIKE 'G%' ";
												$reHS4SCG = mysqli_query($con, $sqlhonor_score4sup_codeG);
												/* ตรวจว่า user นี้ ได้เป็นคนให้คะแนนไปแล้วหรือไม่ */
												$sqlhonor_score4sup_codeG_check = "SELECT * FROM `honor_score4sup` WHERE ID='$ID_sup' AND honor_score4sup_code LIKE 'G%' AND honor_score4sup_data1='$ID' ";
												$reHS4SCG_C = mysqli_query($con, $sqlhonor_score4sup_codeG_check);
	
												if(mysqli_num_rows($reHS4SCG) < 3) {

													if(mysqli_num_rows($reHS4SCG_C) == 0) { ?>

														<form action="addhonor_score4sup_codeG-admin.php" method="POST">
															<td>
																<div class="center inline block">
																	<input type="number" name="score" class="score-input-box" min="1" max="5">
																</div>

																<div class="order-detail-btn">
																	<input type="hidden" name="ID_sup" value=<?php echo $ID_sup; ?>>
																	<input type="hidden" name="count_rows_codeG" value="<?php echo mysqli_num_rows($reHS4SCG); ?>">
																	<input type="hidden" id="winScroll_input" name="winScroll">
																	<input type="hidden" id="page" name="page" value="<?php echo $page; ?>">
																	<input type="submit" id="honor_score4sup_codeG_submitbtn" class="order_remark-submit" value="ให้คะแนน">
																	
																	<script>
																		var winScroll_input = document.querySelectorAll("#winScroll_input");
																		var honor_score4sup_codeG_submitbtn = document.querySelectorAll("#honor_score4sup_codeG_submitbtn");

																		honor_score4sup_codeG_submitbtn[<?php echo $n-1; ?>].onmouseover = function() {
																			var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
																			winScroll_input[<?php echo $n-1; ?>].value = winScroll;
																		}
																	</script>
																</div>
															</td>
														</form> <?php

													}elseif(mysqli_num_rows($reHS4SCG_C) == 1) {

														$rowHS4SCG_C = mysqli_fetch_array($reHS4SCG_C); ?>

														<td class="name-pr">
															<p>
																<?php echo $rowHS4SCG_C['honor_score4sup_data3']; ?>
																<br>
																<?php echo date('d-m-Y H:i', strtotime($rowHS4SCG_C['honor_score4sup_date'])); ?>
															</p>
														</td> <?php

													}

												}elseif(mysqli_num_rows($reHS4SCG) == 3) {

													if(mysqli_num_rows($reHS4SCG_C) == 1) {
														
														$rowHS4SCG_C = mysqli_fetch_array($reHS4SCG_C); ?>

														<td class="name-pr">
															<p>
																<?php echo $rowHS4SCG_C['honor_score4sup_data3']; ?>
																<br>
																<?php echo date('d-m-Y H:i', strtotime($rowHS4SCG_C['honor_score4sup_date'])); ?>
																<br>
																(ตรวจครบ3ท่านแล้ว)
															</p>
														</td> <?php

													}else{ ?>

														<td class="name-pr">
															<p>(ตรวจครบ3ท่านแล้ว)</p>
														</td> <?php

													}

												}else{ ?>

													<td class="name-pr">
														<p>-</p>
													</td> <?php

												}

											}else{

												/* Start honor_score4sup_code='G01' */
												$sqlhonor_score4sup_codeG1 = "SELECT * FROM `honor_score4sup` WHERE ID='$ID_sup' AND honor_score4sup_code='G01' ";
												$reHS4SCG1 = mysqli_query($con, $sqlhonor_score4sup_codeG1);

												if(mysqli_num_rows($reHS4SCG1) == 1) {

													$rowHS4SCG1 = mysqli_fetch_array($reHS4SCG1); ?>

													<td class="name-pr">
														<p>
															<?php echo $rowHS4SCG1['honor_score4sup_data3']; ?>
															<br>
															<?php echo $rowHS4SCG1['honor_score4sup_data2']; ?>
															<br>
															<?php echo date('d-m-Y H:i', strtotime($rowHS4SCG1['honor_score4sup_date'])); ?>
														</p>
													</td> <?php

												}else{ ?>

													<td class="name-pr">
														<p>-</p>
													</td> <?php

												}

												/* Start honor_score4sup_code='G02' */
												$sqlhonor_score4sup_codeG2 = "SELECT * FROM `honor_score4sup` WHERE ID='$ID_sup' AND honor_score4sup_code='G02' ";
												$reHS4SCG2 = mysqli_query($con, $sqlhonor_score4sup_codeG2);

												if(mysqli_num_rows($reHS4SCG2) == 1) {

													$rowHS4SCG2 = mysqli_fetch_array($reHS4SCG2); ?>

													<td class="name-pr">
														<p>
															<?php echo $rowHS4SCG2['honor_score4sup_data3']; ?>
															<br>
															<?php echo $rowHS4SCG2['honor_score4sup_data2']; ?>
															<br>
															<?php echo date('d-m-Y H:i', strtotime($rowHS4SCG2['honor_score4sup_date'])); ?>
														</p>
													</td> <?php

												}else{ ?>

													<td class="name-pr">
														<p>-</p>
													</td> <?php

												}

												/* Start honor_score4sup_code='G03' */
												$sqlhonor_score4sup_codeG3 = "SELECT * FROM `honor_score4sup` WHERE ID='$ID_sup' AND honor_score4sup_code='G03' ";
												$reHS4SCG3 = mysqli_query($con, $sqlhonor_score4sup_codeG3);

												if(mysqli_num_rows($reHS4SCG3) == 1) {

													$rowHS4SCG3 = mysqli_fetch_array($reHS4SCG3); ?>

													<td class="name-pr">
														<p>
															<?php echo $rowHS4SCG3['honor_score4sup_data3']; ?>
															<br>
															<?php echo $rowHS4SCG3['honor_score4sup_data2']; ?>
															<br>
															<?php echo date('d-m-Y H:i', strtotime($rowHS4SCG3['honor_score4sup_date'])); ?>
														</p>
													</td> <?php

												}else{ ?>

													<td class="name-pr">
														<p>-</p>
													</td> <?php

												}
											}
										?>
										<!-- Remark -->
										<form action="addhonor_score4sup_remark-admin.php" method="POST">
											<td class="order_remark-sty">
												<textarea rows="1" cols="15" name="honor_score_remark"><?php echo $row['honor_score_remark']; ?></textarea>
												<div class="order-detail-btn">
													<input type="hidden" name="ID" value=<?php echo $ID_sup; ?>>

													<input type="hidden" id="winScroll_input" name="winScroll">
													<input type="hidden" id="page" name="page" value="<?php echo $page; ?>">
													<input type="submit" id="honor_score_remark_submitbtn" class="order_remark-submit" value="บันทึก">

													<script>
														var winScroll_input = document.querySelectorAll("#winScroll_input");
														var honor_score_remark_submitbtn = document.querySelectorAll("#honor_score_remark_submitbtn");

														honor_score_remark_submitbtn[<?php echo $n-1; ?>].onmouseover = function() {
															var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
															winScroll_input[<?php echo $n-1; ?>].value = winScroll;
														}
													</script>
												</div>
											</td>
										</form>
										


									</tr> <?php
									$n++;
									$selfscore = 0;
									$peoplescore = 0;
									$workscore = 0;
									$k = 0;
								}
							?>
						</tbody>
					</table>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ระบบหลังบ้านศึกษานิเทศก์ดีขั้นเกียรติคุณ -->
<?php
					$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE honor_score_status='ส่งให้มูลนิธิตรวจสอบ' OR honor_score_status='Approve แล้ว' OR honor_score_status='ปฏิเสธ' ";
					$result = $con->query($sql);
					$row = $result->fetch_assoc();
					$total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results
?>
            <div class="wrapper row3 coloured" style="background-color:rgb(226,255,224);">
              <main class="hoc container clear">
                <div class="content">
                  <nav class="pagination">
                    <ul>
<?php
                        for ($n=1; $n<=$total_pages; $n++) {  // print links for all pages
                          if ($n==$page)
                          {
                            echo "<li class='current'>";
                          }else{
                            echo "<li>";
                          }
                          echo "<a href='ระบบหลังบ้านศน.ขั้นเกียรติคุณ.php?page=".$n."'";
                          if ($n==$page)  echo " class='curPage'";
                          echo ">".$n."</a> ";
                        }; 
?>
                      </li>
                    </ul>
                  </nav>
                </div>
              </main>
            </div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

</body>
</html>