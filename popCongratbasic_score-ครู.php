<?php
	session_start();
	require_once('condb.php');

	$ID = $_SESSION['ID'];
	$sqllogin = "SELECT * FROM `login` WHERE ID='$ID' ";
	$relogin = mysqli_query($con,$sqllogin);
	$rowlogin = mysqli_fetch_array($relogin);
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<span class="login100-form-title fs-30 lh-1-1">
					ยินดีด้วยค่ะ
					<br>
					ท่านได้คะแนน <?php echo $rowlogin['basic_score_total']; ?>/30 คะแนน
					<br><br>
					ท่านมีสิทธิ์ได้รับรางวัลครูดีของแผ่นดิน ขั้นพื้นฐาน กรุณาส่งใบรับรองพฤติกรรมจากผู้บริหารที่สูงขึ้นไป 1 ระดับชั้น
					<br><br>
					กรุณาแนบเอกสารจากผู้บังคับบัญชาว่า<br>"ไม่อยู่ในระหว่างการลงโทษทางวินัยหรือตั้งกรรมการสอบสวนวินัย"
					<br>
					<a href="docs/ร่วมโครงการ/ศึกษานิเทศก์ดี/ตัวอย่างหนังสือรับรองเพื่อประกอบการพิจารณารางวัล.pdf" class="btn2">ตัวอย่างหนังสือรับรอง</a>
					<br><br>
					<a href="โครงการครูดีของแผ่นดิน.php#secA" class="btn2">แนบเอกสารภายหลัง</a>
					<a href="แนบหนังสือรับรองขั้นพื้นฐาน-ครู.php" class="btn2">แนบเอกสารตอนนี้</a>
				</span>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>