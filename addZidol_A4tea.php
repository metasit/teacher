<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

	date_default_timezone_set("Asia/Bangkok");

	$sqlaffil_name = "SELECT occup_name, affil_name FROM `login` WHERE ID='$ID' ";
	$reaffil_name = mysqli_query($con, $sqlaffil_name);
	$rowaffil_name = mysqli_fetch_array($reaffil_name);

	$affil_name = $rowaffil_name['affil_name'];

	$basic_score_status = 'ยืนยันแล้ว';
	$basic_score_total = $ID; // Teacher ID
	$basic_score_text = $_SESSION['firstname'].' '.$_SESSION['lastname']; // Teacher name
	$basic_score_ans = $_POST['system'].'*'.$_POST['devmain'].'*'.$_POST['devsub'];
	$datetime = date("Y-m-d H:i:s");

	$ID_stu = $_POST['ID_stu'];

	$sql = "UPDATE `login` SET affil_name='$affil_name', basic_score_status='$basic_score_status', basic_score_total='$basic_score_total', basic_score_text='$basic_score_text', basic_score_ans='$basic_score_ans', basic_score_date='$datetime' WHERE ID='$ID_stu' ";
	$re = $con->query($sql) or die($con->error); //Check error
	/* Log User Action */
	$scorelog_task = 'สมัครต้นกล้าแทนนักเรียน_step4';
	$scorelog_detail = 'ครู,ชั้น5,เด็ก,ขั้นพื้นฐาน,'.$basic_score_ans;
	$scorelog_total = $ID_stu;
	$sqllog = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail, scorelog_total) VALUES ('$ID', '$scorelog_task', '$scorelog_detail', '$scorelog_total') ";
	$relog = $con->query($sqllog) or die($con->error);

	echo '<script>';
		echo "alert('สมัครระบบต้นกล้าแห่งความดีแทนนักเรียนเรียบร้อยค่ะ');";
		echo "window.location.replace('ระบบติดตามสถานะนักเรียนขั้นพื้นฐาน-ครู.php')";
	echo '</script>';
	
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>