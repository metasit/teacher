<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
				<?php if (strpos($_SESSION['email'], '@') !== false) {
								if ($_SESSION["level"]=="admin") { //Admin Session ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า-en.php">ระบบหลังบ้านร้านค้า</a></li>
											<li><a href="ระบบหลังบ้านประเมินครู-en.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*A*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านประเมินครู-en.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*B*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า-en.php">ระบบหลังบ้านร้านค้า</a></li>
										</ul>
									</li>
          <?php }elseif ($_SESSION['level']=='memberGeneral') { //General Member Session ?>
								<li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> General Member</li>
          <?php }elseif ($_SESSION['level']=='memberSilver') { //Member Silver Session ?>
								<li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> Silver Member</li>
          <?php }elseif ($_SESSION['level']=='memberGold') { //Member Gold Session ?>
								<li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> Gold Member</li>
          <?php }elseif ($_SESSION['level']=='memberDiamond') { //Member Diamond Session ?>
								<li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> Diamond Member</li>
          <?php } ?>
								<li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
        <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login-en.php"><i class="fas fa-user-lock"></i> <text1>Log in</text1></a></li>
                <li class="faicon-login"><a href="signup-en.php"><i class="fas fa-file-signature"></i> <text1>Sign up</text1></a></li>
				<?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
                  <ul>
                    <li><a href="บำรุงค่าสมาชิก.php">ภาษาไทย</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                    <li class="active"><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก-en.php">สนับสนุนของที่ระลึก</a></li>
                  </ul>
                </li>
        </ul>
			</nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index-en.php">Home</a></li>
          <li><a href="เกี่ยวกับเรา-en.php">About us</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>Join us</a>
            <ul>
              <li><a href="ร่วมโครงการ-en.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <li><a href="โครงการครูดีของแผ่นดิน-en.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา-en.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA-en.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน-en.php">โครงการอาสาของแผ่นดิน</a></li>
            </ul>
          </li>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">Innovation Library</a></li>
          <li><a href="ติดต่อเรา-en.php">Contact us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> Donate</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> บำรุงสมาชิก</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - บำรุงสมาชิก -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <article>
      <!-- ################################################################################################ -->
      <section class="text-gold center" style="line-height:80px;">
        <h7>บำรุงค่าสมาชิกรายปี</h7><br>
      </section>
      <!-- ################################################################################################ -->
      <section class="table3" style="overflow-x:auto;">
        <table class="m-l-r-auto">
          <tr>
            <th rowspan="2">ลำดับ</th>
            <th rowspan="2">ประเภทสมาชิก</th>
            <th rowspan="2">ค่าบำรุงสมาชิก<br>(ต่อปี)</th>
            <th colspan="4">สิทธิพิเศษ</th>
          </tr>
          <tr>
            <th>โครงการ</th>
            <th>เกียรติบัตร</th>
            <th>ดาวน์โหลด</th>
            <th>คูปองอบรม</th>
          </tr>
          <tr class="text-black BG-white">
            <td>1</td>
            <td>สมาชิก ทั่วไป</td>
            <td>ฟรี</td>
            <td><i class="fas fa-check"></i></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="text-black BG-silver">
            <td>2</td>
            <td>สมาชิก ระดับเงิน</td>
            <td>300 บาท</td>
            <td><i class="fas fa-check"></i></td>
            <td><i class="fas fa-check"></i></td>
            <td></td>
            <td></td>
          </tr>
          <tr class="text-white BG-gold1">
            <td>3</td>
            <td>สมาชิก ระดับทอง</td>
            <td>500 บาท</td>
            <td><i class="fas fa-check"></i></td>
            <td><i class="fas fa-check"></i></td>
            <td><i class="fas fa-check"></i></td>
            <td>1 บัตร มูลค่า 500 บาท</td>
          </tr>
          <tr class="text-white BG-diamond">
            <td>4</td>
            <td>สมาชิก ระดับเพชร</td>
            <td>1,000 บาท</td>
            <td><i class="fas fa-check"></i></td>
            <td><i class="fas fa-check"></i></td>
            <td><i class="fas fa-check"></i></td>
            <td>2 บัตร มูลค่า 1,000 บาท</td>
          </tr>
        </table>
      </section>
      <dl class="fs-32 font-RSUText lh-1-3">
        <dt>หมายเหตุ</dt>
        <dd>- <strong>โครงการ</strong> หมายถึง สามารถเข้าร่วมโครงการต่างๆ ฟรี ตามความสมัครใจ</dd>
        <dd>- <strong>เกียรติบัตร</strong> หมายถึง มูลนิธิ จะส่งเกียรติบัตร (ตัวจริง สวยงามมาก ไม่ใช่จากระบบออนไลน์) ตามรางวัลที่ท่านได้รับในโครงการ ไปยังที่อยู่ของท่าน ปีละ 1 ครั้ง (ตามประเภทสมาชิก)</dd>
        <dd>- <strong>ผลิตภัณฑ์</strong> หมายถึง สนับสนุนผลิตภัณฑ์มูลนิธิในราคาพิเศษ</dd>
        <dd>- <strong>ดาวน์โหลด</strong> หมายถึง ท่านสามารถดาวน์โหลดข้อมูลต่างๆ ในเวปไซต์ของมูลนิธิ และ ข้อมูลประเมินตนเองในระบบสมรรถนะฟรี</dd>
        <dd>- <strong>คูปองอบรม</strong> หมายถึง จะได้รับ Cash Voucher เพื่อใช้เป็นส่วนลด เมื่อเข้าอบรมสัมมนาที่มูลนิธิจัดขึ้น (1 บัตรมีมูลค่า 500 บาท ไม่สามารถแลกเป็นเงินสดได้) โดยมูลนิธิจะจัดส่งให้ท่านทางไปรษณีย์</dd>
			</dl>
			<!-- ################################################################################################ -->
			<section class="text-gold center" style="line-height:80px;">
        <h7>ขั้นตอนการบำรุงสมาชิก</h7><br>
      </section>
			<!-- ################################################################################################ -->
			<dl class="fs-32 font-RSUText lh-1-3">
				<dt>ขั้นที่ 1 เลือกระดับสมาชิกที่ท่านต้องการ เลื่อนระดับ</dt>
				<dt class="m-t-15">ขั้นที่ 2 ท่านสามารถบำรุงสมาชิกได้ 3 ช่องทาง ดังนี้</dt>
        	<dd>2.1 เงินสด นำฝากเข้า <strong>ชื่อบัญชี มูลนิธิครูดีของแผ่นดิน(บำรุงสมาชิก-บริจาค) ธนาคารกรุงไทย สาขาถนนวิสุทธิกษัตริย์ เลขที่บัญชี 006 – 0 – 21576 – 3</strong></dd>
        	<dd>2.2 โอนเงินผ่านระบบออนไลน์ Mobile Banking เลือกโอนเงินมาที่ <strong>บัญชี มูลนิธิครูดีของแผ่นดิน (บำรุงสมาชิก-บริจาค) ธนาคารกรุงไทย เลขที่บัญชี 006 – 0 – 21576 – 3</strong></dd>
					<dd>2.3 สแกน QR Code นี้ แล้วบำรุงสมาชิกตามระดับที่ท่านต้องการ</dd>
					<img src="images/QRCodeบริจาคทั่วไป+บำรุงสมาชิก.jpg" alt="QRCodeบริจาคทั่วไป+บำรุงสมาชิก">
				<dt class="m-t-15">ขั้นที่ 3 แจ้งการบำรุงสมาชิก และข้อมูลผู้บำรุงสมาชิก</dt>
					<dd>3.1  กรอกวัน เดือน ปี และ เวลา ที่ท่านชำระ</dd>
					<dd>3.2  แนบหลักฐานการบำรุงสมาชิก</dd>
					<dd>3.3 ข้อมูลผู้บำรุงสมาชิก (สำหรับออกใบสำคัญรับเงิน พร้อมกรอกข้อมูลเพิ่มเติมที่ปรากฏ เช่น เลขที่บัตรประจำตัวประชาชน)</dd>
					<dd>3.4 กด แจ้งชำระเงิน  </dd>
				<dt class="m-t-15">ขั้นที่ 4 เจ้าหน้าที่ดำเนินการหลังจากที่ท่านได้แจ้งข้อมูลเพื่อการเลื่อนระดับสมาชิก โดยที่</dt>
					<dd>4.1 เจ้าหน้าที่ทำการตรวจสอบข้อมูลการบำรุงสมาชิกของท่าน </dd>
					<dd>4.2 เจ้าหน้าที่ทำการUpdate สถานะสมาชิก ตามที่ท่านได้แจ้งขอมูลภายใน 7 วัน หลังจากที่ท่านแจ้ง</dd>
					<dd>4.3 เจ้าหน้าที่ดำเนินการออกใบสำคัญรับเงิน และ ส่ง CASH Voucherไปยังท่าน ภายใน 7 วันทำการ (ในขั้นตอน 4.3 ผู้บำรุงสมาชิก สามารถตรวจสอบเลขที่ไปรษณีย์ได้ที่ E-Mail ผู้บำรุงสมาชิก)</dd>
					<dd>4.4 เจ้าหน้าที่ทำการประกาศรายชื่อผู้บำรุงสมาชิกผ่านทางเพจเครือข่ายครูดีของแผ่นดิน ทุกวันศุกร์ ท่านสามารถตรวจสอบรายชื่อท่านได้ หากไม่มีรายชื่อให้ท่าน แจ้งชื่อ พร้อมข้อมูล ทาง In box เพจเครือข่ายครูดีของแผ่นดิน </dd>
			</dl>
      <!-- ################################################################################################ -->
    </article>
  </main>
</div>
<!-- End Content 01 - บำรุงสมาชิก -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear">
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Related Department</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Foundation of Thai Suprateacher</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>