<?php 
	session_start();
	require_once('condb.php');
  $sql_list_project  ="SELECT * FROM join_project";
  $list_project = $con->query($sql_list_project);
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
				<?php if (strpos($_SESSION['email'], '@') !== false) {
								if ($_SESSION["level"]=="admin") { //Admin Session ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
											<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*A*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*B*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
										</ul>
									</li>
          <?php }elseif ($_SESSION['level']=='memberGeneral') { //General Member Session ?>
								<li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> สมาชิกทั่วไป</li>
          <?php }elseif ($_SESSION['level']=='memberSilver') { //Member Silver Session ?>
								<li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</li>
          <?php }elseif ($_SESSION['level']=='memberGold') { //Member Gold Session ?>
								<li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</li>
          <?php }elseif ($_SESSION['level']=='memberDiamond') { //Member Diamond Session ?>
								<li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</li>
          <?php } ?>
								<li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
        <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
                <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
				<?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
                  <ul>
                    <li><a href="คลังสื่อนวัตกรรมรวม001-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
                    <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึก</a></li>
                  </ul>
                </li>
        </ul>
			</nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <?php 
                while($row = $list_project->fetch_assoc()){
                  $id_join_project = $row['id_join_project'];
                  $title_menu = $row['title_menu']; 
              ?>
              <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
              <?php } ?>
              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          <li class="active"><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="window.location.reload(true);">คลังสื่อนวัตกรรม</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - คลังสื่อนวัตกรรม -->
<div class="wrapper bgded coloured" style="background-color:rgb(226,255,224);">
  <div class="hoc container testimonials clear">
    <!-- ################################################################################################ -->
    <div style="padding:20px 20px 0;">
      <div class="group">
        <article class="one_third first center">
          <a href="คลังสื่อนวัตกรรม012.php">
            <img class="zoom108" src="images/Posterครูจ้างสร้างเด็ก.png" alt="Posterครูจ้างสร้างเด็ก">
            <div class="text-black">
              <h9 class="heading">นางเดือนเพ็ญ บัวศรีล</h9>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <i class="fab fa-youtube"></i>
            </div>
            <em>โรงเรียนวัดโบสถ์สมพรชัย สพป.พระนครศรีอยุธยา เขต 2</em>
            <div class="tag">
              <ul class="nospace meta">
                <!--<li><i class="fa fa-user"></i> <a href="#">Admin</a></li>-->
                <li><a class="psygui-sty" href="#"><i class="fa fa-tag"></i> จิตวิทยาและการแนะแนว</a></li>
              </ul>
            </div><!--
            <blockquote>ตำแหน่งจะสูงต่ำแค่ไหน ถ้าครูมีใจรักศิษย์และศรัทธาในอาชีพ “ครู” ก็สามารถสร้างเด็กดีให้แผ่นดินได้ “เด็กที่มีความตั้งใจที่เปลี่ยนแปลง ตนเอง ในการมาโรงเรียนเช้าขึ้นเพื่อมาช่วยเพื่อๆทำเวร 
              เมื่อเขาเปลี่ยนพฤตกรรมได้ เขามีความภูมิใจและอยากสร้างเด็กดีต่อๆไป</blockquote>-->
          </a>
        </article>
        <article class="one_third center">
          <a href="คลังสื่อนวัตกรรม011.php">
            <img class="zoom108" src="images/Posterหอปัญหาตนเอง.png" alt="Posterหอปัญหาตนเอง">
            <div class="text-black">
              <h9 class="heading">นางสาวกมลวรรณ ปานเมือง</h9>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <i class="fab fa-youtube"></i>
            </div>
            <em>โรงเรียนทุ่งคาพิทยาคาร สพม. เขต 11</em>
            <div class="tag">
              <ul class="nospace meta">
                <!--<li><i class="fa fa-user"></i> <a href="#">Admin</a></li>-->
                <li><a class="coutea-sty" href="#"><i class="fa fa-tag"></i> หลักสูตรและวิธีการสอน</a></li>
              </ul>
            </div><!--
            <blockquote>ค้นหาปัญหาของตนเองให้พบ ร่วมกลุ่มกันแก้ปัญหา นำพาซึ่งการเปลี่ยนแปลง ครูใช้การจัดการเรียนรู้โดยใช้ปัญหาเป็นฐาน (Problem-Based Learning) ร่วมกับรูปแบบการจัดการเรียนรู้แบบร่วมมือ 
              (Learning Together) โดยให้นักเรียนค้นหาปัญหาที่เกิดขึ้นกับตนเองและการแก้ปัญหานั้นจับกลุ่มร่วมกันแก้ไขปัญหาที่มีความคล้ายถึงกัน ใช้กระบวนการกลุ่มเพื่อน ช่วยเพื่อนและครูติดตามดูแลเอาใจใส่ 
              ให้กำลังใจนักเรียน นำมาสู่การเปลี่ยนแปลงของนักเรียนในทางที่ดีขึ้น</blockquote>-->
          </a>
        </article>
        <article class="one_third center">
          <a href="คลังสื่อนวัตกรรม010.php">
            <img class="zoom108" src="images/Posterหอสร้างเด็กนิทาน.png" alt="Posterหอสร้างเด็กนิทาน">
            <div class="text-black">
              <h9 class="heading">นางสาวโสรญา คงรักษา</h9>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <i class="fab fa-youtube"></i>
            </div>
            <em>โรงเรียนประชานิคม 4 สพป.ชุมพร เขต 1</em>
            <div class="tag">
              <ul class="nospace meta">
                <!--<li><i class="fa fa-user"></i> <a href="#">Admin</a></li>-->
                <li><a class="coutea-sty" href="#"><i class="fa fa-tag"></i> หลักสูตรและวิธีการสอน</a></li>
              </ul>
            </div><!--
            <blockquote>กุศโลบายง่ายๆ ในการสร้างพฤติกรรมใหม่ แก้นิสัยเก่าเพียงทำผ่านการเล่านิทาน นอกจากช่วยกระตุ้นการสร้างจินตนาการแล้ว 
              ยังช่วยบ่มเพาะคุณธรรมพื้นฐานให้ประทับใจจิตวิญญาณของเด็ก</blockquote>-->
          </a>
        </article>
      </div>
      <div class="group m-t-20">
        <article class="one_third first center">
          <a href="คลังสื่อนวัตกรรม009.php">
            <img class="zoom108" src="images/Posterหอของหายแก้ได้.png" alt="Posterหอของหายแก้ได้">
            <div class="text-black">
              <h9 class="heading">นางสาววิไลลักษณ์ อยู่ดี</h9>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <i class="fab fa-youtube"></i>
            </div>
            <em>โรงเรียนวัดหนามแดง สพป.ฉะเชิงเทรา เขต 1</em>
            <div class="tag">
              <ul class="nospace meta">
                <!--<li><i class="fa fa-user"></i> <a href="#">Admin</a></li>-->
                <li><a class="devatt-sty" href="#"><i class="fa fa-tag"></i> การพัฒนาคุณลักษณะอันพึงประสงค์</a></li>
              </ul>
            </div><!--
            <blockquote>การเปลี่ยนแปลงพฤติกรรมจากเด็กที่ไม่เคยเก็บรักษาของเลยกลายเป็นเด็กที่รู้จักเก็บรักษาของได้ดีขึ้นด้วยสมุดบันทึก</blockquote>-->
          </a>
        </article>
        <article class="one_third center">
          <a href="คลังสื่อนวัตกรรม008.php">
            <img class="zoom108" src="images/PosterหอBully.png" alt="PosterหอBully">
            <div class="text-black">
              <h9 class="heading">นางสาวดรุณี บุญวงค์</h9>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <i class="fab fa-youtube"></i>
            </div>
            <em>โรงเรียนวัดป่าประดู่ สพม. เขต 18</em>
            <div class="tag">
              <ul class="nospace meta">
                <!--<li><i class="fa fa-user"></i> <a href="#">Admin</a></li>-->
                <li><a class="tecedu-sty" href="#"><i class="fa fa-tag"></i> เทคโนโลยีการศึกษา</a></li>
              </ul>
            </div><!--
            <blockquote>การจัดกิจกรรมการเรียนรู้แบบร่วมมือ เพื่อแก้ปัญหา Cyber bullying ของนักเรียนที่ปรึกษา</blockquote>-->
          </a>
        </article>
        <article class="one_third center">
          <a href="คลังสื่อนวัตกรรม007.php">
            <img class="zoom108" src="images/Posterหอครูปรับเด็กเปลี่ยน.png" alt="Posterหอครูปรับเด็กเปลี่ยน">
            <div class="text-black">
              <h9 class="heading">นางณัฐณิชา กล้าหาญ</h9>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <i class="fab fa-youtube"></i>
            </div>
            <em>โรงเรียนชากังราววิทยา (อินทร์-ชุ่ม ดีสารอุปถัมภ์)</em>
            <div class="tag">
              <ul class="nospace meta">
                <!--<li><i class="fa fa-user"></i> <a href="#">Admin</a></li>-->
                <li><a class="psygui-sty" href="#"><i class="fa fa-tag"></i> จิตวิทยาและการแนะแนว</a></li>
              </ul>
            </div><!--
            <blockquote>เมื่อบรรยากาศห้องเรียนเต็มไปด้วยพลังลบ เพียงครูเปิดใจ ปรับแนวคิด หาจ้อบกพร่องของตัวครูเอง พัฒนาตนเอง นำมาปรับกระบวนการเรียนรู้สร้างสังคม กัลยาณมิตร ในห้องเรียน ให้เด็กมีพลังบวก 
              เป็นกันเอง ก็ทำให้ เด็กสนใจเรียนมากขึ้น กล้าคิด กล้าตอบ เป็นก้องเรียนคณิตที่มีชีวิตได้</blockquote>-->
          </a>
        </article>
      </div>
      <div class="group m-t-20">
        <article class="one_third first center">
          <a href="คลังสื่อนวัตกรรม006.php">
            <img class="zoom108" src="images/Posterหอพลังเสียง.png" alt="Posterหอพลังเสียง">
            <div class="text-black">
              <h9 class="heading">นายนิติกรณ์ ตั้งหลัก</h9>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <i class="fab fa-youtube"></i>
            </div>
            <em>โรงเรียนดอนเมืองทหารอากาศบำรุง สพม. เขต 2</em>
            <div class="tag">
              <ul class="nospace meta">
                <!--<li><i class="fa fa-user"></i> <a href="#">Admin</a></li>-->
                <li><a class="psygui-sty" href="#"><i class="fa fa-tag"></i> จิตวิทยาและการแนะแนว</a></li>
              </ul>
            </div><!--
            <blockquote>หากครูเปลี่ยนท่าทีและน้ำเสียงที่ขึงขังน่ากลัวปรับตัวใช้น้ำเสียงที่นุ่มนวล อ่อนหวาน ในการจัดการเรียนรู้ได้ฉันใด น้ำเสียงแห่งการสวดโอ้เอ้วิหารรายก็เป็นสิ่งที่สามารถเปลี่ยนเด็กติดเม 
              เด็กที่อ่านไม่คล่อง และเดสมาธิสั้นได้ฉันนั้น</blockquote>-->
          </a>
        </article>
        <article class="one_third center">
          <a href="คลังสื่อนวัตกรรม005.php">
            <img class="zoom108" src="images/Posterหอกระดาษนิสัย.png" alt="Posterหอกระดาษนิสัย">
            <div class="text-black">
              <h9 class="heading">นางสาวกนกรัตน์ พรหมเดช</h9>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <i class="fab fa-youtube"></i>
            </div>
            <em>โรงเรียนวัดหัวกรูด สพป. ชุมพร เขต 1</em>
            <div class="tag">
              <ul class="nospace meta">
                <!--<li><i class="fa fa-user"></i> <a href="#">Admin</a></li>-->
                <li><a class="coutea-sty" href="#"><i class="fa fa-tag"></i> หลักสูตรและวิธีการสอน</a></li>
              </ul>
            </div><!--
            <blockquote>กระดาษกับสีที่นำมาบวกกับความตั้งใจจริงที่จะเปลี่ยนพฤติกรรม จะสร้างผลลัพธ์ที่น่าเหลือเชือให้กับทุกคนโดยใช้เทคนิค การตั้งคำถามแบบ Wh-guestion</blockquote>-->
          </a>
        </article><!--
        <article class="one_third center">
          <a href="หอเกียรติยศ003.php">
            <img class="zoom108" src="images/Posterหอครูปรับเด็กเปลี่ยน.png" alt="Posterหอครูปรับเด็กเปลี่ยน">
            <h9 class="heading" style="color:white;">นางณัฐณิชา กล้าหาญ</h9>
            <em>โรงเรียนชากังราววิทยา (อินทร์-ชุ่ม ดีสารอุปถัมภ์)</em>
            <blockquote>เมื่อบรรยากาศห้องเรียนเต็มไปด้วยพลังลบ เพียงครูเปิดใจ ปรับแนวคิด หาจ้อบกพร่องของตัวครูเอง พัฒนาตนเอง นำมาปรับกระบวนการเรียนรู้สร้างสังคม กัลยาณมิตร ในห้องเรียน ให้เด็กมีพลังบวก 
              เป็นกันเอง ก็ทำให้ เด็กสนใจเรียนมากขึ้น กล้าคิด กล้าตอบ เป็นก้องเรียนคณิตที่มีชีวิตได้</blockquote>
          </a>
        </article>-->
      </div>
    </div>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End Content 01 - คลังสื่อนวัตกรรม -->
<!-- ################################################################################################ -->
<!-- Start Pagination -->
<div class="wrapper row3 coloured" style="background-color:rgb(226,255,224);">
  <main class="hoc container clear">
    <div class="content">
      <nav class="pagination">
        <ul>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">&laquo; ก่อนหน้า</a></li>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">2</a></li>
          <li class="current"><strong>1</strong></li>
        </ul>
      </nav>
    </div>
  </main>
</div>
<!-- End Pagination -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          <p>ของที่ระลึกมูลนิธิฯ</p>
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพฯ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End Copyright Tab -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Back2Top Button -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Back2Top Button -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>
<script src="js/custom.js"></script>


<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>