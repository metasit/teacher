<?php session_start();
			require_once('condb.php');

date_default_timezone_set("Asia/Bangkok");
error_reporting(~E_NOTICE);

$email = $_SESSION['email'];

/* Set order_group */
$sqlmax = "SELECT DISTINCT MAX(order_group) AS max_order_group FROM `orders` ";
$resultmax = mysqli_query($con,$sqlmax);
$rowmax = mysqli_fetch_array($resultmax);

/* Prepare valiable for INSERT data */
$order_group = $rowmax['max_order_group']+1;
$doc_address = $_SESSION['docaddress'];
$new_name = $_POST['new_name'];
$contact_email = $_POST['contact_email'];
$new_address = $_POST['new_address'];
$shipping_kind = $_POST['shipping_kind'];
$order_date = date("Y-m-d H:i:s");

/* Set shipping_address */
if(empty($new_address)) {
	$shipping_address = $doc_address;
}else{
	$shipping_address = $new_address;
}
/* Set shipping_kind */
if(empty($shipping_kind)) {
	$shipping_kind = 'ems';
}else{
	$shipping_kind = $_POST['shipping_kind'];
}
/* Set contact_name */
$new_name = $_POST['new_name'];
if(empty($new_name)) {
	$contact_name = $_SESSION['fisrtname'].' '.$_SESSION['lastname'];
}else{
	$contact_name = $new_name;
}
/* Set contact_number */
$new_phonenum = $_POST['new_phonenum'];
if(empty($new_phonenum)) {
	$contact_number = $_SESSION['phonenum'];
}else{
	$contact_number = $new_phonenum;
}


$sqlcart = "SELECT * FROM `cart` WHERE email='$email' ";
$resultcart = $con->query($sqlcart);

while($rowcart = $resultcart->fetch_assoc()) {
	$product_id = $rowcart['product_id'];
	$product_price = $rowcart['product_price'];
	$product_amount = $rowcart['product_amount'];

	if(strpos($_SESSION['email'], 'GUEST') !== false) {
		$email = 'GUEST '.$_POST['cart_id'];
	}else{
		$email = $_SESSION['email'];
	}

	$sql="INSERT INTO `orders` VALUES (NULL,'$email','$order_group','$product_id','$product_price','$product_amount',
	'$shipping_kind','$shipping_address','$contact_name','$contact_email','$contact_number','รอการชำระเงิน','$order_date',NULL,NULL,'0000-00-00 00:00:00',NULL)";

	$res= $con->query($sql) or die($con->error);
}

/* Send order summary by email */
if(strpos($_SESSION['email'], '@') !== false) {
	$strTo = $email;
}elseif(strpos($_SESSION['email'], 'GUEST') !== false) {
	$strTo = $contact_email;
	$email = $_SESSION['email'];
	unset($_SESSION['email']);
}

/* Write email */
$strSubject = "Your Account information username and password.";
$strHeader = "Content-type: text/html; charset=UTF-8\n"; // or windows-874 //
$strHeader .= "From: thaisuprateacher@gmail.com\n";
$strMessage = "";
$strMessage .= 'มูลนิธิครูดีของแผ่นดิน ';
$strMessage .= "=================================<br>";
$strMessage .= "Thaisuprateacher.com<br>";
$flgSend = mail($strTo,$strSubject,$strMessage,$strHeader);


/* Clear all data in cart */
$sqldelete_cart = "DELETE FROM `cart` WHERE email='$email' ";
$resdelete_cart= $con->query($sqldelete_cart) or die($con->error);

if(strpos($_SESSION['email'], 'GUEST') !== false) {
	$_SESSION['email'] = 'GUEST '.$_POST['cart_id'];
}

unset($_SESSION['pricetotal']); // Clear pricetotal value

header('location: ประวัติและสถานะการสั่งซื้อ.php');

?>