<?php
	session_start();
	require_once('condb.php');
	if(!isset($_SESSION['email'])) {
		header("Location: javascript:history.go(-1);");
	}

	$email = $_SESSION['email'];
	$sqllogin = "SELECT * FROM `login` WHERE email='$email' ";
	$relogin = mysqli_query($con,$sqllogin);
	$rowlogin = mysqli_fetch_array($relogin);
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>Foundation of Thai Suprateacher</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="index.php"><i class="fas fa-arrow-left"></i> Back to Home</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" name="formeditprofile" action="saveeditprofile.php" method="POST">
					<!-- ################################################################################################ -->
					<span class="login100-form-title">
						<strong>Edit Profile</strong><br>Managing your profile to stay safe online<br>Put your new profile instead of old profile in each box and click to update your profile
					</span>
					<!-- Email -->
					<div class="wrap-input100">
						<div class="input100 text-black BG-white" style="line-height:45px"><?php echo $rowlogin['email']; ?></div>
						<span class="symbol-input100">
							<i class="fas fa-user" aria-hidden="true"></i>
						</span>
					</div>
					<!-- Password -->
					<div class="wrap-input100 validate-input">
						<input class="input100 text-black" type="password" name="password" value="<?php echo $rowlogin['password']; ?>" pattern=".{8,}" title="โปรดใส่จำนวนรหัสผ่านเท่ากับหรือมากกว่า 8 ตัว" />
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					<!-- Firstname -->
					<div class="wrap-input100">
						<!--
						<input class="input100 text-black" type="text" name="firstname" value="<?php echo $rowlogin['firstname']; ?>" max-lenght="100"/>
						-->
						<div class="input100 text-black BG-white" style="line-height:45px"><?php echo $rowlogin['firstname']; ?></div>
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- Lastname -->
					<div class="wrap-input100">
						<!--
						<input class="input100 text-black" type="text" name="lastname" value=" max-lenght="100"/>
						-->
						<div class="input100 text-black BG-white" style="line-height:45px"><?php echo $rowlogin['lastname']; ?></div>
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- Phone number -->
					<div class="wrap-input100 validate-input">
						<input class="input100 text-black" type="tel" name="phonenum" value="<?php echo $rowlogin['phonenum']; ?>" maxlength="10" pattern="[0][0-9]{9}"
						title="โปรดใส่จำนวนตัวเลขให้ครบ 10 ตัว (รวมเลข 0 ตัวแรก)" />
						<span class="symbol-input100">
							<i class="fas fa-mobile-alt" aria-hidden="true"></i>
						</span>
						<em>
					</div>
					<!-- Documentary Address -->
					<div class="wrap-input100 validate-input">
						<input class="input100 text-black" type="text" name="docaddress" value="<?php echo $rowlogin['docaddress']; ?>"/>
						<span class="symbol-input100">
							<i class="fas fa-address-book" aria-hidden="true"></i>
						</span>
					</div>
					<!-- Birthdate -->
					<div class="wrap-input100 validate-input">
						<input class="input100 text-black" type="date" name="birthdate" value="<?php echo $rowlogin["birthdate"]; ?>"
						min='1950-01-01' max='2002-01-01'/>
						<span class="symbol-input100">
							<i class="fas fa-birthday-cake" aria-hidden="true"></i>
						</span>
					</div>
					<!-- Submit button -->
					<div class="container-login100-form-btn">
						<input type="hidden" name="check" value="1">
						<button class="login100-form-btn" type="submit" id="register">
							Update
						</button>
					</div>
					<!-- ################################################################################################ -->
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	 
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>