<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<!--
				<div class="textlink3 PreMenu_fl_left">
					<a href="โครงการครูดีของแผ่นดิน.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าโครงการครูดีของแผ่นดิน</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/เด็กดีของแผ่นดิน Logo.jpg" style="width:70%" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<span class="login100-form-title">
					ขั้นตอนสุดท้าย: หัวข้อที่จะพัฒนาตัวเอง 
				</span>
				<!-- ################################################################################################ -->
				<!-- Start show step -->
				<div class="login100-form validate-form">
					<div class="center">
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
					</div>
					<div class="right">
						<p style="color:rgb(94,177,26);">เลือกหัวข้อ</p>
					</div>
				</div>
				<!-- End show step -->
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" id="Affform" action="addZidol.php?CFP=registZidol_step4" method="POST" enctype="multipart/form-data">
					<!-- ################################################################################################ -->
					<?php
						$affil_code = $_SESSION['affil_code'];

						$sql = "SELECT * FROM `login` WHERE (occup_code LIKE 'OcAA%' AND affil_code='$affil_code')
						OR (occup_code LIKE 'OcAB%' AND affil_code='$affil_code') ";
						$re = $con->query($sql);
					?>
					<label for="teacher_id" id="teacherhead" class="center m-t-10 text-gray bold">ชื่อคุณครูที่ปรึกษา (ถ้ามี)</label>
					<select name="teacher_ID" id="teacher_id" class="form-control" style="height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกครูที่ปรึกษา</option>
						<?php
							while($row = $re->fetch_assoc()) {
								$name = $row['firstname'].' '.$row['lastname']; ?>
								<option value="<?php echo $row['ID']; ?>"><?php echo $name; ?></option> <?php
							}
						?>
					</select>
					<!-- ################################################################################################ -->
					<label class="center m-t-15 p-t-15 text-black bold" style="border-top:rgb(72,160,0) solid 2px; border-radius:20px;">
						หัวข้อการพัฒนา "ปรับปรุงนิสัยที่ไม่ดี"
					</label>
					<input type="text" value="ปรับปรุงนิสัยที่ไม่ดี" disabled class=" bg-white form-control mt-5 ">

					<div class="text-center font-weight-bold my-4 text-dark">
						นิสัยที่ต้องปรับปรุง
					</div>


				<div class="wrap-input100 validate-input">
					<input 
                    value="<?php echo "การรู้จักเป้ายหมายชีวิต"; ?>"
                    class="input100" type="text" name="firstname" class="form-control"   max-lenght="100" required />
					<span class="symbol-input100">
						<i class="fas fa-file-signature" aria-hidden="true"></i>
					</span>
				</div>

				<label class="center m-t-15 p-t-15 text-black bold mt-3 " style="border-top:rgb(72,160,0) solid 2px; border-radius:20px;">
					อยู่ในหมวดใด
				</label>

				<select name="" id="" class="form-control">	
					<option value="" disabled selected >เลือกหัวข้อหลัก</option>
					<option value="">การเข้าสังคม เช่น มารยาทและการวางตัว,การช่วยเหลือผู้อื่น,การมีจิตอาสา</option>
					<option value="">การรู้จัก/การจัดการตนเอง เช่น การรู้จักเป้าหมายชีวิต,การบริหารเวลา,การจัดการอารมณ์,การฝึกสมาธิ,การบริหารการเงิน, การจัดของให้เป็นระเบียบ</option>
					<option value="">การเข้าใจผู้อื่น เช่น การทำงานร่วมกับผู้อื่นอย่างมีความสุข, การคบเพื่อน , การดูแลพ่อแม่ และผู้ใหญ่</option>
					<option value="">การสื่อสารและสร้างแรงบันดาลใจ เช่นการพูด, ความเป็นผู้นำ, การกล้าแสดงออก</option>
				</select>


				<div class="mt-4 text-center fotn-weight-bold text-dark " style="font-size:18px;">
					ให้ความหมาย หรือ นิยามในนิสัยนั้น ว่าคืออะไร (what)
				</div>

				<div class="mt-2 font-weight0bold text-center ">
					<input  value="เติมรายละเอียด" type="text" disabled class="form-control bg-white">
				</div>


				<div class="mt-4 text-center fotn-weight-bold text-dark " style="font-size:18px;">
					เราทำสิ่งนั้นทำไม เพื่ออะไร มีผลดี ผลเสีย อย่างไร (why)
				</div>

				<div class="mt-2 font-weight0bold text-center ">
					<input  value="เติมรายละเอียด" type="text" disabled class="form-control bg-white">
				</div>


				<div class="mt-4 text-center fotn-weight-bold text-dark " style="font-size:18px;">
					สิ่งนั้นมีขั้นตอน วิธีการทำอย่างไร (How)
				</div>
				<div class="mt-2 font-weight0bold text-center ">
					<input  value="เติมรายละเอียด" type="text" disabled class="form-control bg-white">
				</div>
				



				<div class="mt-4 text-center fotn-weight-bold text-dark " style="font-size:18px;">
					ใครเป็นต้นแบบ ในการทำเรื่องนั้นๆ พร้อมกับศึกษาเรื่องราวของคนนั้น (who)
				</div>

				<div class="mt-2 font-weight0bold text-center ">
					<input  value="เติมรายละเอียด" type="text" disabled class="form-control bg-white">
				</div>

					<!-- <label class="center m-t-10 text-black bold">หัวข้อหลัก</label> -->
					<!-- <select name="devmain" id="devmain_id" class="form-control" style="height:40px;" required>
						<option value="" disabled="disabled" selected="selected">เลือกหัวข้อหลัก</option>
						<option value="A"><strong>การรู้จัก/จัดการตนเอง</strong> เช่น การรู้จักเป้าหมายชีวิต, การบริหารเวลา, การจัดการอารมณ์, การฝึกสมาธิ, การบริหารการเงิน, การจัดของให้สะอาดเป็นระเบียบ</option>
						<option value="B"><strong>การเข้าใจผู้อื่น</strong> เช่น การทำงานร่วมกับผู้อื่นอย่างมีความสุข, การคบเพื่อน, การดูแลพ่อแม่ และผู้ใหญ่</option>
						<option value="C"><strong>การเข้าสังคม</strong> เช่น มารยาทและการวางตัว, การช่วยเหลือผู้อื่น, การมีจิตอาสา</option>
						<option value="D"><strong>การสื่อสารและการสร้างแรงบันดาลใจ</strong> เช่น การพูด, ความเป็นผู้นำ, การกล้าแสดงออก</option>
					</select> -->

					<!-- ################################################################################################ -->
					<div class="container-login100-form-btn m-t-20">
						<a href="registZidol_step3.php" class="login100-form-btn" style="width:30%; margin:0 10px;">ย้อนกลับ</a>
						<button class="login100-form-btn" style="width:40%; margin:0 10px;" type="submit" id="register">สมัครโครงการ</button>
					</div>
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>