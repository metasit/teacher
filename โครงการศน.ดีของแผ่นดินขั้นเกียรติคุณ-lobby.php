<?php 
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];
	var_dump($ID);
	if(isset($ID)) {
		$sqllogin = "SELECT honor_score_status, basic_score_remark FROM `login` WHERE ID='$ID' AND occup_code LIKE 'OcAD%' AND basic_score_status LIKE 'Approve แล้ว/ยืนยันแล้ว' ";
		$relogin = mysqli_query($con,$sqllogin);

		if(mysqli_num_rows($relogin) != 0) {
			date_default_timezone_set("Asia/Bangkok");
			$rowlogin = mysqli_fetch_array($relogin);

			$honor_score_status = $rowlogin['honor_score_status'];
			/* เช็คว่า ได้ขั้นพื้นฐานมาเมื่อไหร่ honor_score4sup_part1 */
			// เรียก basic_score_remark เพื่อดูว่า เป็นกลุ่ม Fast-Track หรือไม่
			$basic_score_remark = $rowlogin['basic_score_remark'];

			// เรียก adscorelog เพื่อดูว่า ได้รับครูดีขั้นพื้นฐานมาเมื่อไหร่
			$sqladscorelog = "SELECT adscorelog_date FROM `adscorelog` WHERE ID_user='$ID' AND adscorelog_task='Approve แล้ว' AND adscorelog_detail='ขั้นพื้นฐาน' ORDER BY adscorelog_id DESC LIMIT 1 ";
			$readscorelog = mysqli_query($con,$sqladscorelog);
			$rowadscorelog = mysqli_fetch_array($readscorelog);

			$check_basic_score = mysqli_num_rows($readscorelog);
			if($check_basic_score == 0) {
				if($basic_score_remark == 'Fast-Track') { // ถ้าไม่มี ให้เช็คอีกว่า เป็นกลุ่ม Fast-Track หรือไม่
					$basic_score_date = 'Fast-Track';
					$basic_score_status = 'Fast-Track';
					$expire_basic_score_date = 'Fast-Track';
				}else{ // ถ้าไม่มี แสดงว่า ไม่เคยได้รับการ Approve ครูขั้นพื้นฐานมาเลย
					$basic_score_date = 'ไม่พบข้อมูลในระบบ';
					$basic_score_status = 'ไม่พบข้อมูลในระบบ';
					$expire_basic_score_date = 'ไม่พบข้อมูลในระบบ';
				}
			}else{
				/* Find and Set the latest basic_score_date */
				$date = new DateTime($rowadscorelog['adscorelog_date']);
				$basic_score_date = $date->format("d-m-Y"); // Set date format
				/* Set the expire_basic_score_date */
				$expire_basic_score_date = $date->add(new DateInterval('P1Y1D')); // Find expire date for basic_score		
				/* Check that this user basic_score expire? and Set $basic_score_status */
				$date_today = new DateTime('today');
				if($expire_basic_score_date >= $date_today) {
					$basic_score_status = 'ได้รับการอนุมัติ'; // Set basic_score_status to Approved
				}else{
					$basic_score_status = 'หมดอายุ';
				}
				$expire_basic_score_date = $date->format("d-m-Y"); // Set date format
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* เช็คว่า มีการแนบการนิเทศมาครบ 12 หรือยัง honor_score4sup_part2 */
			$sqlhonor_score4sup_codeB = "SELECT ID FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code LIKE 'B%' ";
			$reB = mysqli_query($con, $sqlhonor_score4sup_codeB);
			
			$countrow_B = mysqli_num_rows($reB);
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* เช็คว่า ประเมินขั้นเกียรติคุณ ผ่านรึยัง honor_score4sup_part3 */
			$sqlhonor_score4sup_codeA = "SELECT honor_score4sup_data1, honor_score4sup_date FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code='A' ";
			$reA = mysqli_query($con, $sqlhonor_score4sup_codeA);
			$rowA = mysqli_fetch_array($reA);
			$check_37 = $rowA['honor_score4sup_data1']; // Check that user was done all 37 question in evaluation?

			$date1 = new DateTime($rowA['honor_score4sup_date']);
			$honor_score4sup_date = $date1->format('d-m-Y');

			$date2 = new DateTime($rowA['honor_score4sup_date']);
			$date2 = $date2->add(new DateInterval('P15D'));
			$honor_score4sup_date_again = $date2->format('d-m-Y');
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* เช็คว่า มีการส่งการเปลี่ยนแแปลงเชิงประจักษ์ หรือยัง honor_score4sup_part4 */
			$sqlhonor_score4sup_codeD = "SELECT honor_score4sup_data1, honor_score4sup_data2, honor_score4sup_data3, honor_score4sup_data4, honor_score4sup_data5 FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code='D' ";
			$reD = mysqli_query($con, $sqlhonor_score4sup_codeD);
			$rowD = mysqli_fetch_array($reD);

			$honor_score4sup_part4_1 = $rowD['honor_score4sup_data1'];
			$honor_score4sup_part4_2 = $rowD['honor_score4sup_data2'];
			$honor_score4sup_part4_3 = $rowD['honor_score4sup_data3'];
			$honor_score4sup_part4_4 = $rowD['honor_score4sup_data4'];
			$honor_score4sup_part4_5 = $rowD['honor_score4sup_data5'];
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* เช็คว่า ทั้ง3part ผ่านรึยัง */
			// Part 1
			if($basic_score_status == 'ได้รับการอนุมัติ' || $basic_score_status == 'Fast-Track') {
				$honor_score4sup_part1 = 'Pass';
			}
			// Part 2
			if($countrow_B == 12) {
				$honor_score4sup_part2 = 'Ready'; // ที่ใช้คำว่า Ready ไม่เหมือนครูชั้น 5 ที่ใช้คำว่า Pass เพราะของศน.เป็นแค่สถานะการแนบไฟล์ครบ12ไฟล์เท่านั้น จึงสามารถส่งให้มูลนิธิตรวจสอบได้ แต่ของครูชั้น5 การที่ส่วนนี้จะผ่าน จำนวนเด็กที่ทำพื้นฐานต้องผ่านตามกำหนดจริง ซึ่งเป็นส่วนที่มูลนิธิอนุมัติมาแล้ว
			}
			// Part 3
			include('includes/calhonor_score-ศน.php');
			if($numberpass_sub >= 1) {
				$honor_score4sup_part3 = 'Pass';
			}
			// Part 4
			if(mysqli_num_rows($reD) == 1) {
				$honor_score4sup_part4 = 'Ready';
			}
					
		}else{
			echo "<script>window.history.go(-1)</script>"; //Protect policy
		}
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">

	<style>
		.input100 {
			width: 100%;
		}

		.stu-up-btn-nohover{ /* Student Upload Button */
			color: white;
			background-color:rgb(206, 206, 206);
			border-color:transparent;
			border-radius:50px;
			padding:1px 0;
		}
	</style>

	<!-- JS for multi-sub-dropdown -->
	<script src="js/jquery.min.js"></script>

</head>


<body id="top" onLoad="window.scroll(0, <?php echo $_GET['winScroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li><a href="ร่วมโครงการ.php"> ร่วมโครงการ</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php"> โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="#" onclick="return false"> ศึกษานิเทศก์ดีของแผ่นดินขั้นเกียรติคุณ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
	<!-- ################################################################################################ -->
	<div style="max-width:80%; margin-right:auto; margin-left:auto; padding:130px 0;">
		<div class="row">
			<!-- ################################################################################################ -->
			<!-- Start เมื่อทั้ง 4 ส่วนผ่าน ให้ user กดปุ่มส่งให้เจ้าหน้าที่ตรวจสอบต่อไป -->
			<?php
				$sql_check_reject = "SELECT ID FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code LIKE 'B%' AND honor_score4sup_check_status LIKE 'ปฏิเสธ,%' ";
				$reCR = mysqli_query($con, $sql_check_reject);

				if($honor_score4sup_part1 == 'Pass' && $honor_score4sup_part2 == 'Ready' && $honor_score4sup_part3 == 'Pass' && $honor_score4sup_part4 == 'Ready') {
					if($honor_score_status == NULL || $honor_score_status == '') { ?>
						<div class="col-sm-12 col-lg-12 mb-3 fs-35 center" style="font-family: RSUText">
							<p class="bold" style="margin: 0;">
								เนื่องจากระบบพบว่า คุณมีข้อมูลทั้ง 4 ส่วนครบถ้วน คุณสามารถกดปุ่มนี้เพื่อให้เจ้าหน้าที่มูลนิธิตรวจสอบ
							</p>
							<br>
							<a href="sendcheckhonor_score4sup.php?ID=<?php echo $ID; ?>" class="btn m-b-20" style="padding: 10px;">ส่งให้มูลนิธิตรวจสอบ</a>
							<br>
						</div> <?php

					}elseif($honor_score_status == 'ส่งให้มูลนิธิตรวจสอบ') {

						if(mysqli_num_rows($reCR) != 0) { ?>

							<div class="col-sm-12 col-lg-12 mb-3 m-b-30 fs-20 center inline" style="font-family: RSUText">
								<div class="col-md-12 bold">
									<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="มูลนิธิครูดีของแผ่นดิน PureLogo">
									<p>สถานะเจ้าหน้าที่มูลนิธิ: <div class="text-yellow2 m-l-10"> รอการแก้ไข</div></p>
								</div>
							</div> <?php

						}else{ ?>

							<div class="col-sm-12 col-lg-12 mb-3 m-b-30 fs-20 center inline" style="font-family: RSUText">
								<div class="col-md-12">
									<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="มูลนิธิครูดีของแผ่นดิน PureLogo">
									<p><strong>สถานะเจ้าหน้าที่มูลนิธิ:</strong> รอมูลนิธิตรวจสอบ</p>
								</div>
							</div> <?php

						}

					}elseif($honor_score_status == 'Approve แล้ว') { ?>
						<div class="col-sm-12 col-lg-12 mb-3 m-b-30 fs-20 center inline" style="font-family: RSUText">
							<div class="col-md-12">
								<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="มูลนิธิครูดีของแผ่นดิน PureLogo">
								<p class="text-green1"><strong>สถานะเจ้าหน้าที่มูลนิธิ:</strong> ผ่านแล้ว <i class="fas fa-check-circle"></i></p>
							</div>
						</div> <?php

					}
				}
			?>
			<!-- End เมื่อทั้ง 4 ส่วนผ่าน ให้ครูกดปุ่มส่งให้เจ้าหน้าที่ตรวจสอบต่อไป -->
			<!-- ################################################################################################ -->
			<!-- Start Left Content -->
			<div class="col-sm-4 col-lg-4 mb-3">
				<div class="input-slip-upload">
					<div class="title-left">
						<h3>เคยได้รับรางวัลศึกษานิเทศก์ดีของแผ่นดิน ขั้นพื้นฐาน <?php if($basic_score_status=='ได้รับการอนุมัติ'){echo '<i class="fas fa-check-circle"></i>';}?></h3>
					</div>
					<!-- ################################################################################################ -->
					<p class="m-t-10 text-black"><strong>สถานะรางวัลศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน:</strong> <?php echo $basic_score_status; ?></p>
					<p class="m-t-10 text-black"><strong>วันที่ได้รับครั้งล่าสุด:</strong> <?php echo $basic_score_date; ?></p>
					<p class="m-t-10 text-black"><strong>วันหมดอายุ:</strong> <?php echo $expire_basic_score_date; ?></p>
				</div>
			</div>
			<!-- End Left Content -->
			<!-- ################################################################################################ -->
			<!-- Start Center Content -->

			<div class="col-sm-4 col-lg-4 mb-3">
				<div class="input-slip-upload">
					<div class="title-left">
						<h3 class="inline">2.การนิเทศที่สอดแทรกการสร้างคนดีให้แผ่นดิน 12 ครั้ง
							<?php
								if($honor_score4sup_part2 == 'Ready') {

									if(mysqli_num_rows($reCR) != 0) { ?>
										<div class="text-yellow2">
											<i class="fas fa-minus-circle"></i>
										</div> <?php
									}else{ ?>
										<div>
											<i class="fas fa-check-circle"></i>
										</div> <?php
									}

								}
							?>
						</h3>
						<p>จำนวนการนิเทศโดยสอดแทรกคุณธรรมที่ส่งแล้ว <strong><?php echo $countrow_B; ?></strong>/12 ครั้ง</p>
						<?php
							if(mysqli_num_rows($reCR) != 0) { ?>
								<p class="text-yellow2 bold center">
									คุณมีรายงานที่ต้องแก้ไขจำนวน: <strong><?php echo mysqli_num_rows($reCR); ?></strong>
									<br>
									(ต้องแก้ไขภายใน 7 วัน หลังจากโดนปฏิเสธ)
								</p> <?php
							}
						?>
					</div>
					<!-- ################################################################################################ -->
					<!-- Start Sub-Center Content -->
					<?php
						/* Start Box for new file upload */
						if($countrow_B < 12) { // NOTE: New upload file. If there're 12 files completely in honor_score4sup (code:B__) table
							$file_num = $countrow_B+1;
							$honor_score4sup_code = sprintf("B%02d", $file_num); ?>
							
							<form action="uploadhonor_score4sup.php?check=new" method="POST" enctype="multipart/form-data" class="m-b-20 p-b-20" style="border-bottom:2px solid white;">

								<div class="row m-t-20">
									<div class="col-md-6 mb-3">
										<label for="honor_score4sup_file_date">วันที่นิเทศ *</label>
										<input type="date" name="honor_score4sup_file_date" class="form-control" min="2019-10-01" max="2020-09-30" required/>
									</div>
									<div class="col-md-6 mb-3">
										<label for="honor_score4sup_file_time">เวลาที่นิเทศ *</label>
										<input type="time" name="honor_score4sup_file_time" class="form-control" required/>
									</div>
									<div class="col-md-12 mb-3 center">
										<p>(สามารถรายงานข้อมูลได้ หลังจากได้รางวัลเกียรติคุณแล้ว)</p>
									</div>
									<div class="col-md-12 mb-3">
										<?php include('includes/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/affildropdown4honor_score4sup_location_new.php'); ?>.
									</div>
									<div class="col-md-12 mb-3">
										<label for="honor_score4sup_file">แนบหลักฐานการนิเทศ *</label>
										<input type="file" name="honor_score4sup_file" class="form-control" required/>
									</div>
									
									<div class="col-md-12 mb-3 m-t-10">
										<label for="honor_score4sup_text">รายงานการนิเทศ</label>
										<textarea class="col-md-12" rows="1" name="honor_score4sup_text"></textarea>
									</div>

									<div class="col-md-12 mb-3 m-t-10">
										<label for="honor_score4sup_text">รูปแบบการนิเทศ</label>
										<select name="" id="" class="form-control">
											<option value="" disabled selected >เลือกรูปแบบ</option>
											<option value="online">online</option>
											<option value="offline">offline</option>
										</select>
									</div>

									<hr>
									<div class="col-md-12 mb-3">
										<label><strong>รายละเอียดผู้รับการนิเทศ</strong></label>
									</div>
									<div class="col-md-6 mb-3">
										<label for="honor_score4sup_teafirstname">ชื่อ * (ไม่ต้องใส่คำนำหน้านาม)</label>
										<input type="text" name="honor_score4sup_teafirstname" class="form-control" required/>
									</div>
									<div class="col-md-6 mb-3">
										<label for="honor_score4sup_tealastname">นามสกุล *</label>
										<input type="text" name="honor_score4sup_tealastname" class="form-control" required/>
									</div>
									<div class="col-md-12 mb-3">
										<?php include('includes/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/occupdropdown4honor_score4sup_pupil_new.php'); ?>
									</div>
									<div class="col-md-12 mb-3">
										<?php include('includes/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/affildropdown4honor_score4sup_pupil_new.php'); ?>
									</div>
								</div>

								<div class="row m-t-12">
									<div class="col-md-12">
										<button type="submit" class="ml-auto btn hvr-hover col-md-12" style="border-color:transparent; border-radius:50px; padding:1px 0;"
											onmousemove="this.style.cursor='pointer'">
											ส่งข้อมูล
										</button>
									</div>
								</div>
								<input type="hidden" name="honor_score4sup_code" value="<?php echo $honor_score4sup_code; ?>">
							</form>
							
							<!-- new upload -->
							<script src="js/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/projectRegist4aff4honor_score4sup_location_new.js"></script>
							<script src="js/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/projectRegist4occ4honor_score4sup_pupil_new.js"></script>
							<script src="js/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/projectRegist4aff4honor_score4sup_pupil_new.js"></script> <?php
						}
						/* End Content for new file upload */
						///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						/* Start Content for ไฟล์ที่อัพโหลดไปแล้ว */
						$sqlhonor_score4sup_codeB = "SELECT * FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code LIKE 'B%' ";
						$reHS4SCB = mysqli_query($con, $sqlhonor_score4sup_codeB);

						$i=1;
						while($rowHS4SCB = mysqli_fetch_array($reHS4SCB)) {
							// Get code by honor_score4sup table
							$honor_score4sup_code = $rowHS4SCB['honor_score4sup_code'];
							// Get date time by honor_score4sup_codeB
							$honor_score4sup_date = $rowHS4SCB['honor_score4sup_date'];
							$createDate = new DateTime($honor_score4sup_date);
							$honor_score4sup_file_date = $createDate->format('Y-m-d');
							$honor_score4sup_file_time = $createDate->format('h:i');
							// Get file (directory ของไฟล์นิเทศ) by honor_score4sup_codeB
							$honor_score4sup_file = $rowHS4SCB['honor_score4sup_data2'];
							// Get pupil's name (ชื่อ-นามสกุล ครูที่ถูกนิเทศ) by honor_score4sup_codeB
							$honor_score4sup_teafirstname = substr($rowHS4SCB['honor_score4sup_data3'], 0, strpos($rowHS4SCB['honor_score4sup_data3'], ' '));
							$honor_score4sup_tealastname = substr($rowHS4SCB['honor_score4sup_data3'], strpos($rowHS4SCB['honor_score4sup_data3'], ' ')+1);
							// Get รายงานการนิเทศ by honor_score4sup_codeB
							$honor_score4sup_text = $rowHS4SCB['honor_score4sup_data6'];
							// Get check_status by honor_score4sup_codeB
							$honor_score4sup_check_status = $rowHS4SCB['honor_score4sup_check_status'];

							// Calculate interval date to let permission to แก้ไขรายงาน
							$honor_score4sup_check_status_date = date_create(substr($honor_score4sup_check_status, strrpos($honor_score4sup_check_status, ',')+1, 10));
							date_add($honor_score4sup_check_status_date, date_interval_create_from_date_string("7 days"));
							$date_deadline = strtotime(date_format($honor_score4sup_check_status_date, 'Y-m-d'));
							$date_today = strtotime('today');
							$date_dif = $date_deadline - $date_today;
							?>
							<form action="uploadhonor_score4sup.php?check=old" method="POST" enctype="multipart/form-data"> <?php
								if($i==1) { ?>
									<div class="row m-t-20">
										<div class="col-md-12 mb-3 fs-20 center bold underline">
											รายงานการนิเทศทั้งหมด
										</div>
									</div> <?php
								} ?>
								<div class="row m-t-10 p-b-10" style="border: 2px solid rgb(94,177,26);">
									<div class="col-md-12 mb-3 p-b-10 center bold fs-20">
										<?php
											if(strpos($honor_score4sup_check_status, 'ปฏิเสธ,') !== false) { ?>

												<p class="text-red1 bold">
													สถานะการตรวจ: ถูกปฏิเสธ เนื่องจาก <?php echo substr($honor_score4sup_check_status, strpos($honor_score4sup_check_status, ',')+1); ?>													
												</p>
												<?php
													if($date_dif > 0) { ?>

														<p class="text-red1 fs-17">(คุณเหลือวันที่จะสามารถแก้ไขรายงานนี้ได้อีก <?php echo $date_dif/60/60/24; ?> วัน)</p> <?php

													}else{ ?>

														<p class="text-red1 fs-17">(เลยกำหนดการแก้ไขมาแล้ว <?php echo abs($date_dif/60/60/24); ?> วัน)</p> <?php

													}
												?>
												<label for="honor_score4sup_header">รายการที่ <?php echo $i; ?></label> <?php

											}elseif(strpos($honor_score4sup_check_status, 'อนุมัติ,') !== false) { ?>

												<p class="text-green1 bold">สถานะการตรวจ: ผ่านแล้ว <i class="fas fa-check-circle"></i></p>
												<label for="honor_score4sup_header">รายการที่ <?php echo $i; ?></label> <?php

											}elseif(strpos($honor_score4sup_check_status, 'แก้ไข,') !== false) { ?>

												<p class="text-yellow2 bold">คุณแก้ไข เนื่องจาก <?php echo substr($honor_score4sup_check_status, strpos($honor_score4sup_check_status, ',')+1); ?></p>
												<label for="honor_score4sup_header">รายการที่ <?php echo $i; ?></label> <?php

											}else{ ?>

												<label for="honor_score4sup_header" class="p-t-10">รายการที่ <?php echo $i; ?></label> <?php

											}
										?>
									</div>
									<div class="col-md-6 mb-3"> <?php
										if(strpos($honor_score4sup_check_status, 'อนุมัติ,') !== false) { ?>
											<label for="honor_score4sup_file_date">วันที่นิเทศ *</label>
											<br>
											<label><?php echo date('d-m-Y', strtotime($honor_score4sup_file_date)); ?></label> <?php
										}else{ ?>
											<label for="honor_score4sup_file_date">วันที่นิเทศ *</label>
											<input type="date" name="honor_score4sup_file_date" class="form-control" value="<?php echo $honor_score4sup_file_date; ?>" min="2019-10-01" max="2020-09-30" required/> <?php
										} ?>
									</div>
									<div class="col-md-6 mb-3"> <?php
										if(strpos($honor_score4sup_check_status, 'อนุมัติ,') !== false) { ?>
											<label for="honor_score4sup_file_time">เวลาที่นิเทศ *</label>
											<br>
											<label><?php echo date('g:i A', strtotime($honor_score4sup_file_time)); ?></label> <?php
										}else{ ?>
											<label for="honor_score4sup_file_time">เวลาที่นิเทศ *</label>
											<input type="time" name="honor_score4sup_file_time" class="form-control" value="<?php echo $honor_score4sup_file_time; ?>" required/> <?php
										} ?>
									</div>
									<div class="col-md-12 mb-3 center">
										<p>(สามารถรายงานข้อมูลได้ หลังจากได้รางวัลเกียรติคุณแล้ว)</p>
									</div>
									<div class="col-md-12 mb-3">
										<?php
											// Get location (สถานที่ที่นิเทศ) by honor_score4sup_codeB
											$affil_code_location_old = substr($rowHS4SCB['honor_score4sup_data1'], 0, strpos($rowHS4SCB['honor_score4sup_data1'], ','));
											$affil_remark_location_old = substr($rowHS4SCB['honor_score4sup_data1'], strpos($rowHS4SCB['honor_score4sup_data1'], ',')+1);
											$affiliation_id_location_old = substr($affil_code_location_old, 0, 3);

											include('includes/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/affildropdown4honor_score4sup_location_old'.$i.'.php');
										?>
									</div>
									<div class="col-md-12 mb-3 m-t-10">
										<label for="honor_score4sup_file">แนบหลักฐานการนิเทศ *</label>
									</div>
									<div class="col-md-6 mb-3"> <?php
										if(strpos($honor_score4sup_check_status, 'อนุมัติ,') !== false) {

										}else{ ?>
											<input type="file" name="honor_score4sup_file" class="form-control"/> <?php
										} ?>
									</div>
									
									
									<?php
										if(strpos($honor_score4sup_check_status, 'อนุมัติ,') !== false) { ?>

											<div class="col-md-12 mb-3">
												<a class="col-md-12 block center stu-file-btn" target="_blank"
													href="<?php echo $honor_score4sup_file; ?>">
													ดูไฟล์แนบ <?php echo $i; ?>
												</a>
											</div> <?php

										}else{ ?>

											<div class="col-md-6 mb-3 m-t-10">
												<a class="col-md-12 block center stu-file-btn" target="_blank"
													href="<?php echo $honor_score4sup_file; ?>">
													ดูไฟล์แนบ <?php echo $i; ?>
												</a>
											</div> <?php
										}

									?>


									<div class="col-md-12 mb-3 m-t-10"> <?php
										if(strpos($honor_score4sup_check_status, 'อนุมัติ,') !== false) { ?>
											<label for="honor_score4sup_text">รายงานการนิเทศ</label>
											<label><?php echo $honor_score4sup_text; ?></label> <?php
										}else{ ?>
											<label for="honor_score4sup_text">รายงานการนิเทศ</label>
											<textarea class="col-md-12" rows="1" name="honor_score4sup_text"><?php echo $honor_score4sup_text; ?></textarea> <?php
										} ?>
									</div>

									<hr>

									<div class="col-md-12 mb-3">
										<label><strong>รายละเอียดผู้รับการนิเทศ</strong></label>
									</div>
									<div class="col-md-6 mb-3"> <?php
										if(strpos($honor_score4sup_check_status, 'อนุมัติ,') !== false) { ?>
											<label for="honor_score4sup_teafirstname">ชื่อ *</label>
											<br>
											<label><?php echo $honor_score4sup_teafirstname; ?></label> <?php
										}else{ ?>
											<label for="honor_score4sup_teafirstname">ชื่อ * (ไม่ต้องใส่คำนำหน้านาม)</label>
											<input type="text" name="honor_score4sup_teafirstname" class="form-control" value="<?php echo $honor_score4sup_teafirstname; ?>" required/> <?php
										} ?>
									</div>
									<div class="col-md-6 mb-3"> <?php
										if(strpos($honor_score4sup_check_status, 'อนุมัติ,') !== false) { ?>
											<label for="honor_score4sup_tealastname">นามสกุล *</label>
											<br>
											<label><?php echo $honor_score4sup_tealastname; ?></label> <?php
										}else{ ?>
											<label for="honor_score4sup_tealastname">นามสกุล *</label>
											<input type="text" name="honor_score4sup_tealastname" class="form-control" value="<?php echo $honor_score4sup_tealastname; ?>" required/> <?php
										} ?>
									</div>
									<div class="col-md-12 mb-3">
										<?php
											// Get pupil's occup_code (ตำแหน่ง ครูที่ถูกนิเทศ) by honor_score4sup_codeB
											$occup_code_pupil_old = substr($rowHS4SCB['honor_score4sup_data4'], 0, strpos($rowHS4SCB['honor_score4sup_data4'], ','));
											$occup_remark_pupil_old = substr($rowHS4SCB['honor_score4sup_data4'], strpos($rowHS4SCB['honor_score4sup_data4'], ',')+1);
											$occupation_id_pupil_old = substr($occup_code_pupil_old, 0 ,3);
											$pomain_id_pupil_old = substr($occup_code_pupil_old, 3, 1);

											include('includes/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/occupdropdown4honor_score4sup_pupil_old'.$i.'.php');
										?>
									</div>
									<div class="col-md-12 mb-3">
										<?php
											// Get pupil's affil_code (โรงเรียน ครูที่ถูกนิเทศ) by honor_score4sup_codeB
											$affil_code_pupil_old = substr($rowHS4SCB['honor_score4sup_data5'], 0, strpos($rowHS4SCB['honor_score4sup_data5'], ','));
											$affil_remark_pupil_old = substr($rowHS4SCB['honor_score4sup_data5'], strpos($rowHS4SCB['honor_score4sup_data5'], ',')+1);
											$affiliation_id_pupil_old = substr($affil_code_pupil_old, 0, 3);

											include('includes/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/affildropdown4honor_score4sup_pupil_old'.$i.'.php');
										?>
									</div>
									
									<div class="col-md-12 mb-3">
										<label for="uploadbtn" style="color:transparent">.</label>
										<?php
										
											if($honor_score_status == NULL || $honor_score_status == '') { ?>
												<button id="uploadbtn<?php echo $i; ?>" type="submit" class="col-md-12 stu-up-btn" onmousemove="this.style.cursor='pointer'"> อัพเดทรายงาน <?php echo $i; ?> (ถ้าทำการแนบไฟล์ซ้ำ จะทำให้ไฟล์เก่าหาย)</button> <?php
											}else{
												
												if(strpos($honor_score4sup_check_status, 'ปฏิเสธ,') !== false || strpos($honor_score4sup_check_status, 'แก้ไข,') !== false) {
													
													if($date_dif > 0) { ?>

														<button id="uploadbtn<?php echo $i; ?>" type="submit" class="col-md-12 stu-up-btn" style="background-color: rgb(216,163,23)" onmousemove="this.style.cursor='pointer'"> แก้ไขรายงาน <?php echo $i; ?> (ถ้าทำการแนบไฟล์ซ้ำ จะทำให้ไฟล์เก่าหาย)</button> <?php

													}else{ ?>

														<button id="uploadbtn<?php echo $i; ?>" type="button" class="col-md-12 stu-up-btn-nohover"> แก้ไขรายงาน <?php echo $i; ?> (เกิน 7 วัน ไม่สามารถแก้ไขได้แล้วค่ะ)</button> <?php

													}

												}else{ ?>
													<button id="uploadbtn<?php echo $i; ?>" type="button" class="col-md-12 stu-up-btn-nohover"> อัพเดทรายงาน <?php echo $i; ?> (ถ้าทำการแนบไฟล์ซ้ำ จะทำให้ไฟล์เก่าหาย)</button> <?php
												}

											}
										?>
									</div>

								</div>
								
								<input type="hidden" name="honor_score4sup_code" value="<?php echo $honor_score4sup_code; ?>">

								<input id="winScroll_input<?php echo $i; ?>" type="hidden" name="winScroll">
								<script>
									var winScroll_input<?php echo $i; ?> = document.getElementById("winScroll_input<?php echo $i; ?>");
									var uploadbtn<?php echo $i; ?> = document.getElementById("uploadbtn<?php echo $i; ?>");

									uploadbtn<?php echo $i; ?>.onmousemove = function() {
										var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
										winScroll_input<?php echo $i; ?>.value = winScroll;
									}
								</script>
							</form>
							<!-- ################################################################################################ -->
								<!-- JS for old upload -->
								<script src="js/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/projectRegist4aff4honor_score4sup_location_old<?php echo $i; ?>.js"></script>
								<script src="js/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/projectRegist4occ4honor_score4sup_pupil_old<?php echo $i; ?>.js"></script>
								<script src="js/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/projectRegist4aff4honor_score4sup_pupil_old<?php echo $i; ?>.js"></script> <?php
							$i++;
						}
						/* End Content for ไฟล์ที่อัพโหลดไปแล้ว  */
					?>
					<!-- End Sub-Center Content -->
					<!-- ################################################################################################ -->
				</div>
			</div>
			<!-- End Center Content -->
			<!-- ################################################################################################ -->
			<!-- Start Right Content -->
			<div class="col-sm-4 col-lg-4 mb-3">
				<div class="input-slip-upload">
					<div class="title-left" style="font-weight:1500">
						<h3>ผ่านการประเมินครองตน ครองคน ครองงาน อย่างน้อย 3 หมวด
							<?php
								if($honor_score4sup_part3 == 'Pass') {
									echo '<i class="fas fa-check-circle"></i>';
								}
							?>
						</h3>
					</div>
					<!-- ################################################################################################ -->
					<!-- Start Sub-Right Content -->
					<!-- Start Content for ประเมินศน.ดีของแผ่นขั้นเกียรติคุณ -->
					<div class="row">
						<div class="col-md-12 mb-3">
							<?php
								if(strpos($check_37, '37.') != '') { // Check if the thirty-seventh question was done
									if($numberpass_sub < 1) { // Check if all 3 categories were failed
										if(strtotime(date('d-m-Y')) <= strtotime($honor_score4sup_date_again)) { // ครูที่สอบตกไป จะได้เข้ามาทำอีกครั้งอีก15วัน $date1 คือ วันที่สอบตก $date2 คือ วันที่จะเข้ามาทำได้อีกครั้ง ?>
											<p class="m-t-10 text-black"><strong>สถานะประเมินศึกษานิเทศก์ดีของแผ่นดินขั้นเกียรติคุณ:</strong> ไม่ผ่าน</p>
											<p class="m-t-10 text-black"><strong>หมวดครองตน:</strong> <?php if($selfscore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$selfscore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$selfscore_avg.' /5 คะแนน)';} ?></p>
											<p class="m-t-10 text-black"><strong>หมวดครองคน:</strong> <?php if($peoplescore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$peoplescore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$peoplescore_avg.' /5 คะแนน)';} ?></p>
											<p class="m-t-10 text-black"><strong>หมวดครองงาน:</strong> <?php if($workscore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$workscore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$workscore_avg.' /5 คะแนน)';} ?></p>
											
											<p style="margin: 0; text-align: center;">
												เนื่องจากคุณไม่ผ่านประเมินขั้นเกียรติคุณ เมื่อวันที่ <?php echo $honor_score4sup_date; ?>
												<br>
												คุณจะสามารถเข้ามาประเมินได้อีกครั้ง ในวันที่ <?php echo $honor_score4sup_date_again; ?>
											</p> <?php
										}else{ ?>
											<a href="ประเมินศน.ดีของแผ่นดินขั้นเกียรติคุณ.php?CFP=ประเมินศน.ดีของแผ่นดินขั้นเกียรติคุณ_ทำอีกครั้ง" class="col-md-12 stu-file-btn">เริ่มทำประเมินอีกครั้ง</a> <?php
										}
									}else{ ?>
										<p class="m-t-10 text-black"><strong>สถานะประเมินศึกษานิเทศก์ดีของแผ่นดินขั้นเกียรติคุณ:</strong> ผ่าน</p>
										<p class="m-t-10 text-black"><strong>หมวดครองตน:</strong> <?php if($selfscore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$selfscore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$selfscore_avg.' /5 คะแนน)';} ?></p>
										<p class="m-t-10 text-black"><strong>หมวดครองคน:</strong> <?php if($peoplescore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$peoplescore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$peoplescore_avg.' /5 คะแนน)';} ?></p>
										<p class="m-t-10 text-black"><strong>หมวดครองงาน:</strong> <?php if($workscore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$workscore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$workscore_avg.' /5 คะแนน)';} ?></p>
										<form action="รายงานประเมินศน.ดีขั้นเกียรติคุณ.php">
											<button class="col-md-12 stu-file-btn">ดาวน์โหลดรายงานคะแนนขั้นเกียรติคุณ</button>
										</form> <?php
									} ?>
									<!-- ################################################################################################ -->
								</div> <?php

								}else{ ?>
									<div class="col-md-12 mb-3">
										<form action="ประเมินศน.ดีของแผ่นดินขั้นเกียรติคุณ.php">
											<label><strong>ประเมินศึกษานิเทศก์ดีของแผ่นดินขั้นเกียรติคุณ</strong></label>
											<?php
												if(mysqli_num_rows($reA) == 0) { ?>
													<button class="col-md-12 stu-file-btn">เริ่มทำประเมิน</button> <?php
												}else{ ?>
													<button class="col-md-12 stu-file-btn">ทำประเมินต่อ</button> <?php
												} ?>
										</form>
									</div>
								</div> <?php
								}
							?>
						</div>
					</div>
					<!-- End Content for ประเมินศน.ดีของแผ่นขั้นเกียรติคุณ -->
					<!-- ################################################################################################ -->
					<!-- Start Content for การเปลี่ยนแปลงเชิงประจักษ์ 1 พฤติกรรม/โครงการ/กิจกรรม -->
					<div class="title-left m-t-60" style="font-weight:1500">
						<h3>มีการเปลี่ยนแปลงเชิงประจักษ์ 1 พฤติกรรม/โครงการ/กิจกรรม
							<?php
								if($honor_score4sup_part4 == 'Ready') {
									echo '<i class="fas fa-check-circle"></i>';
								}
							?>
						</h3>
					</div>
					<div class="row">
						<div class="col-md-12 mb-3">
							<?php
								if(mysqli_num_rows($reD) == 0) { ?>
									<form action="addhonor_score4sup_part4-ศน.php" method="POST" enctype="multipart/form-data">
										<label class="bold underline center">การนำความรู้มาใช้ในการปฏิบัติงานอย่างจริงจังอย่างน้อย 1 พฤติกรรม/โครงการ/กิจกรรม</label>
										<div class="inline left">
											<p><strong>1. ท่านนำความรู้มาใช้ในการปฏิบัติงานอย่างจริงจัง ในระดับใด</strong></p>
											<br>
											<input type="radio" id="choice1" name="honor_score4sup_part4_1" value="most" required>
											<label for="choice1">มากที่สุด</label>
											<br>
											<input type="radio" id="choice2" name="honor_score4sup_part4_1" value="much" required>
											<label for="choice2">มาก</label>
											<br>
											<input type="radio" id="choice3" name="honor_score4sup_part4_1" value="neutral" required>
											<label for="choice3">ปานกลาง</label>
											<br>
											<input type="radio" id="choice4" name="honor_score4sup_part4_1" value="less" required>
											<label for="choice4">น้อย</label>
											<br>
											<input type="radio" id="choice5" name="honor_score4sup_part4_1" value="least" required>
											<label for="choice5">น้อยที่สุด</label>
											<br>
										</div>

										<label class="bold m-t-10">2. ก่อนการอบรมท่านมีคุณสมบัติ/ทักษะ/สมรรถนะ/พฤติกรรมนั้นอย่างไร</label>
										<input type="text" class="col-12" name="honor_score4sup_part4_2" required>

										<label class="bold m-t-10">3. หลังการอบรมท่านมีคุณสมบัติ/ทักษะ/สมรรถนะ/พฤติกรรมนั้นอย่างไร</label>
										<input type="text" class="col-12" name="honor_score4sup_part4_3" required>

										<label class="bold m-t-10">4. การนำความรู้ไปใช้ในการปฏิบัติงานอย่างจริงจังนั้น คืออะไร จงบรรยายพร้อมภาพประกอบไม่เกิน 2 หน้า A4</label>
										<input type="file" class="col-12 form-control" name="honor_score4sup_part4_4" required>

										<label class="bold m-t-10">5. เอกสารหรือหลักฐาน เช่น ภาพ วีดีทัศน์ หรือบทสัมภาษณ์ผู้เกี่ยวข้องที่ แสดงให้เห็นถึงการเปลี่ยนแปลงอย่างชัดเจน</label>
										<input type="file" class="col-12 form-control" name="honor_score4sup_part4_5" required>

										<button class="col-md-12 stu-up-btn m-t-20">ส่งการเปลี่ยนแปลงเชิงประจักษ์</button>
									</form> <?php
								}elseif(mysqli_num_rows($reD) == 1) { ?>
									<form action="addhonor_score4sup_part4-ศน.php" method="POST" enctype="multipart/form-data">
										<label class="bold underline center">การนำความรู้มาใช้ในการปฏิบัติงานอย่างจริงจังอย่างน้อย 1 พฤติกรรม/โครงการ/กิจกรรม</label>
										<div class="inline left">
											<p><strong>1. ท่านนำความรู้มาใช้ในการปฏิบัติงานอย่างจริงจัง ในระดับใด</strong></p>
											<br>
											<input type="radio" id="choice1" name="honor_score4sup_part4_1" value="most" <?php if($honor_score4sup_part4_1 == 'most'){echo 'checked';} ?> required>
											<label for="choice1">มากที่สุด</label>
											<br>
											<input type="radio" id="choice2" name="honor_score4sup_part4_1" value="much" <?php if($honor_score4sup_part4_1 == 'much'){echo 'checked';} ?> required>
											<label for="choice2">มาก</label>
											<br>
											<input type="radio" id="choice3" name="honor_score4sup_part4_1" value="neutral" <?php if($honor_score4sup_part4_1 == 'neutral'){echo 'checked';} ?> required>
											<label for="choice3">ปานกลาง</label>
											<br>
											<input type="radio" id="choice4" name="honor_score4sup_part4_1" value="less" <?php if($honor_score4sup_part4_1 == 'less'){echo 'checked';} ?> required>
											<label for="choice4">น้อย</label>
											<br>
											<input type="radio" id="choice5" name="honor_score4sup_part4_1" value="least" <?php if($honor_score4sup_part4_1 == 'least'){echo 'checked';} ?> required>
											<label for="choice5">น้อยที่สุด</label>
											<br>
										</div>

										<label class="bold m-t-10">2. ก่อนการอบรมท่านมีคุณสมบัติ/ทักษะ/สมรรถนะ/พฤติกรรมนั้นอย่างไร</label>
										<input type="text" class="col-12" name="honor_score4sup_part4_2" value="<?php echo $honor_score4sup_part4_2; ?>" required>

										<label class="bold m-t-10">3. หลังการอบรมท่านมีคุณสมบัติ/ทักษะ/สมรรถนะ/พฤติกรรมนั้นอย่างไร</label>
										<input type="text" class="col-12" name="honor_score4sup_part4_3" value="<?php echo $honor_score4sup_part4_3; ?>" required> <?php

										if($honor_score4sup_part4_4 == '') { ?>
											<label class="bold m-t-10">4. การนำความรู้ไปใช้ในการปฏิบัติงานอย่างจริงจังนั้น คืออะไร จงบรรยายพร้อมภาพประกอบไม่เกิน 2 หน้า A4</label>
											<input type="file" class="col-12 form-control" name="honor_score4sup_part4_4" required> <?php
										}else{ ?>
											<label class="bold m-t-10">4. การนำความรู้ไปใช้ในการปฏิบัติงานอย่างจริงจังนั้น คืออะไร จงบรรยายพร้อมภาพประกอบไม่เกิน 2 หน้า A4</label>
											<div class="row col-md-12 center">
												<div class="col-md-6 mb-3">
													<input type="file" class="form-control" name="honor_score4sup_part4_4" />
												</div>
												<div class="col-md-6 mb-3 m-t-10">
													<a href="<?php echo $honor_score4sup_part4_4; ?>" class="col-12 block stu-file-btn" target="_blank">ดูไฟล์แนบ</a>
												</div>
											</div> <?php
										}

										if($honor_score4sup_part4_5 == '') { ?>
											<label class="bold m-t-10">5. เอกสารหรือหลักฐาน เช่น ภาพ วีดีทัศน์ หรือบทสัมภาษณ์ผู้เกี่ยวข้องที่ แสดงให้เห็นถึงการเปลี่ยนแปลงอย่างชัดเจน</label>
											<input type="file" class="col-12 form-control" name="honor_score4sup_part4_5" required> <?php
										}else{ ?>
											<label class="bold m-t-10">5. เอกสารหรือหลักฐาน เช่น ภาพ วีดีทัศน์ หรือบทสัมภาษณ์ผู้เกี่ยวข้องที่ แสดงให้เห็นถึงการเปลี่ยนแปลงอย่างชัดเจน</label>
											<div class="row col-md-12 center">
												<div class="col-md-6 mb-3">
													<input type="file" class="form-control" name="honor_score4sup_part4_5" />
												</div>
												<div class="col-md-6 mb-3 m-t-10">
													<a href="<?php echo $honor_score4sup_part4_5; ?>" class="col-12 block stu-file-btn" target="_blank">ดูไฟล์แนบ</a>
												</div>
											</div> <?php
										}

										if($honor_score_status == 'ส่งให้มูลนิธิตรวจสอบ') { ?>
											<button type="button" class="col-md-12 stu-up-btn-nohover m-t-20">ส่งการเปลี่ยนแปลงเชิงประจักษ์</button> <?php
										}else{ ?>
											<button type="submit" class="col-md-12 stu-up-btn m-t-20">ส่งการเปลี่ยนแปลงเชิงประจักษ์</button> <?php
										} ?>
									</form> <?php
								}
							?>
						</div>
					</div>
					<!-- End Content for การเปลี่ยนแปลงเชิงประจักษ์ 1 พฤติกรรม/โครงการ/กิจกรรม -->
					<!-- End Sub-Right Content -->
					<!-- ################################################################################################ -->
				</div>
			</div>
			<!-- End Right Content -->
			<!-- ################################################################################################ -->
		</div>
	</div>
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

	
</body>
</html>