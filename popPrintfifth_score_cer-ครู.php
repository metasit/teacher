<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		$sqllogin = "SELECT * FROM `login` WHERE ID='$ID' ";
		$relogin = mysqli_query($con, $sqllogin);
		$rowlogin = mysqli_fetch_array($relogin);

		$fifth_score_status = $rowlogin['fifth_score_status'];

		if(strpos($fifth_score_status, 'Approve แล้ว') !== false) { // เช็คว่า ได้รับการ Approve ไปแล้วหรือยัง ถ้ามีแล้วให้เข้าหน้านี้ได้

		}else{
			echo '<script>';
				echo "alert('ต้องผ่านโครงการครูดีของแผ่นดินชั้นที่ 5 ก่อนนะคะ ถึงเข้าใช้ระบบหน้านี้ได้');";
				echo "window.history.go(-1)";
			echo '</script>';
		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">

	<style>
		/* The Modal (background) */
		.modal {
			display: none; /* Hidden by default */
			position: fixed; /* Stay in place */
			z-index: 5; /* Sit on top */
			padding-top: 250px; /* Location of the box */
			left: 0;
			top: 0;
			width: 100%; /* Full width */
			height: 100%; /* Full height */
			overflow: auto; /* Enable scroll if needed */
			background-color: rgb(0,0,0); /* Fallback color */
			background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
			
		}
		/* Modal Content */
		#modal-content {
			background-color: white;
			margin: auto;
			padding: 20px;
			border: 10px solid rgb(94,177,26);
			border-radius: 10px;
			width: 50%;
		}
		/* The Close Button */
		#close {
			color: #aaaaaa;
			float: right;
			font-size: 28px;
			font-weight: bold;
		}
		#close:hover, #close:focus {
			color: #000;
			text-decoration: none;
			cursor: pointer;
		}
		.center_text_input > input {
			margin: 0 auto;
			margin-left: auto;
			margin-right: auto;
		}
	</style>
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="โครงการครูดีของแผ่นดิน.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าโครงการครูดีของแผ่นดิน</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<br>
				<span class="login100-form-title fs-30 lh-1-1">
					<?php
						$sqllogin = "SELECT * FROM `login` WHERE ID='$ID' ";
						$relogin = mysqli_query($con, $sqllogin);
						$rowlogin = mysqli_fetch_array($relogin);

						if($_SESSION['level']=="memberGeneral") { ?>
							เนื่องจากคุณเป็นสมาชิกระดับทั่วไป
							<br>
							คุณสามารถพิมพ์เกียรติบัตรแบบธรรมดาได้ค่ะ
							<br>
							<a id="onlineCerBtn" class="btn2 hov-pointer">แบบออนไลน์</a>
							<br><br>
							คุณสามารถได้รับเกียรติบัตรแบบพิเศษ โดยการเป็นสมาชิกระดับเงิน, ทอง หรือ เพชร <a href="บำรุงค่าสมาชิก.php" target="_blank" class="readall fs-18">ได้ที่นี้</a>
							
							<!-- Modal for member-upgrade motivation -->
							<div id="motivationModal" class="modal">
								<div id="modal-content">
									<!-- ################################################################################################ -->
									<span id="close">&times;</span>
									<p class="fs-18">ขอความร่วมมือในการบำรุงสมาชิก</p>

									<a href="printfifth_score4tea_cer.php?CFP=popPrintfifth_score4tea_cer" target="_blank" class="btn BG-red1 fs-15">ยกเลิก</a>
									<a href="บำรุงค่าสมาชิก.php" class="btn BG-green1 fs-15">ยืนยัน</a>
									<!-- ################################################################################################ -->
								</div>
							</div>
							<!-- JS for motivationModal box -->
							<script>
								// Get the motivationModal
								var motivationModal = document.getElementById("motivationModal");
								// Get the button that opens the motivationModal
								var onlineCerBtn = document.getElementById("onlineCerBtn");
								// Get the <span> element that closes the motivationModal
								var closeBtn = document.getElementById("close");
								// Get the button that submit reason for motivationModal
								var cancelBtn = document.getElementById("cancelBtn");
								///////////////////////////////////////////////////////////////////////////////////////////////////////////
								// When the user clicks the button, open the motivationModal
								onlineCerBtn.onclick = function() {
									motivationModal.style.display = "block";
								}
								// When the user clicks on <closeBtn> (x), close the motivationModal
								closeBtn.onclick = function() {
									motivationModal.style.display = "none";
								}
								// When the user clicks on <cancelBtn>, close the motivationModal
								cancelBtn.onclick = function() {
									motivationModal.style.display = "none";
								}
							</script> <?php

						}elseif($_SESSION['level']=="memberSilver") { ?>
							เนื่องจากคุณเป็นสมาชิกระดับพิเศษ
							<br>
							ยินดีด้วยค่ะ นอกจากคุณจะสามารถพิมพ์เกียรติบัตรออนไลน์ได้เองแล้ว
							<br>
							คุณได้รับเกียรติบัตรแบบพิเศษ ทางมูลนิธิจะส่งไปให้ตามที่อยู่ที่ให้ไว้ตอนสมัครสมาชิกค่ะ
							<br>
							<a id="onlineCerBtn" class="btn2 hov-pointer">แบบออนไลน์</a>
							
							<!-- Modal for member-upgrade motivation -->
							<div id="motivationModal" class="modal">
								<div id="modal-content">
									<!-- ################################################################################################ -->
									<span id="close">&times;</span>
									<p class="fs-18">ขอความร่วมมือในการบำรุงสมาชิก</p>

									<a href="printfifth_score4tea_cer.php?CFP=popPrintfifth_score4tea_cer" target="_blank" class="btn BG-red1 fs-15">ยกเลิก</a>
									<a href="บำรุงค่าสมาชิก.php" class="btn BG-green1 fs-15">ยืนยัน</a>
									<!-- ################################################################################################ -->
								</div>
							</div>
							<!-- JS for motivationModal box -->
							<script>
								// Get the motivationModal
								var motivationModal = document.getElementById("motivationModal");
								// Get the button that opens the motivationModal
								var onlineCerBtn = document.getElementById("onlineCerBtn");
								// Get the <span> element that closes the motivationModal
								var closeBtn = document.getElementById("close");
								// Get the button that submit reason for motivationModal
								var cancelBtn = document.getElementById("cancelBtn");
								///////////////////////////////////////////////////////////////////////////////////////////////////////////
								// When the user clicks the button, open the motivationModal
								onlineCerBtn.onclick = function() {
									motivationModal.style.display = "block";
								}
								// When the user clicks on <closeBtn> (x), close the motivationModal
								closeBtn.onclick = function() {
									motivationModal.style.display = "none";
								}
								// When the user clicks on <cancelBtn>, close the motivationModal
								cancelBtn.onclick = function() {
									motivationModal.style.display = "none";
								}
							</script> <?php

						}elseif($_SESSION['level']=="admin" || $_SESSION['level']=="memberGold" || $_SESSION['level']=="memberDiamond") { ?>
							เนื่องจากคุณเป็นสมาชิกระดับพิเศษ
							<br>
							ยินดีด้วยค่ะ นอกจากคุณจะสามารถพิมพ์เกียรติบัตรออนไลน์ได้เองแล้ว
							<br>
							คุณได้รับเกียรติบัตรแบบพิเศษ ทางมูลนิธิจะส่งไปให้ตามที่อยู่ที่ให้ไว้ตอนสมัครสมาชิกค่ะ
							<br>
							<a href="printfifth_score4tea_cer.php?CFP=popPrintfifth_score4tea_cer" target="_blank" class="btn2">แบบออนไลน์</a> <?php
						}else{
							header("Location: javascript:history.go(-1);");
						} ?>
						
						<br>
						กรณีที่ท่านยังคงมีนักเรียนที่ร่วมโครงการเด็กดีของแผ่นดินขั้นพื้นฐานตกค้างการตรวจความดี ท่านสามารถดำเนินการต่อได้โดยการกดปุ่มด้านล่าง
						<br>
						<a href="ระบบติดตามสถานะนักเรียนขั้นพื้นฐาน-ครู.php" target="_blank" class="btn2">เข้าสู่ระบบติดตามสถานะนักเรียนขั้นพื้นฐาน</a> <?php
					?>
				</span>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>