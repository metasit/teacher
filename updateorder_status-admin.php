

<?php
	session_start();
	require_once('condb.php');

	$order_status = $_GET['order_status'];
	$order_group = $_GET['order_group'];
	$slip_image = $_GET['slip_image'];
	$win_scroll=$_GET['win_scroll'];

	$sqlorders = "SELECT * FROM `orders` WHERE order_group='$order_group' ";
	$reorders = mysqli_query($con,$sqlorders);
	$roworders = mysqli_fetch_array($reorders);

	if(empty($slip_image)) {
		$sql = "UPDATE `orders` SET order_status='รอการชำระเงิน' WHERE order_group='$order_group' ";
		$res= $con->query($sql) or die($con->error); //Check error
		echo '<script>';
			echo "alert('คุณครูยังไม่ได้ทำการแนบหลักฐานการโอน จึงไม่สามารถอัพเดทเป็นสถานะนี้ได้ค่ะ');";
			echo "window.location.replace('ระบบหลังบ้านร้านค้า.php?win_scroll=$win_scroll')";
		echo '</script>';
	}else{
		if($order_status == 'จัดส่งแล้ว' && is_null($roworders['shipping_code'])) {
			echo "<script>";
				echo "alert(\"ต้องอัพเดทรหัสขนส่งก่อน ถึงจะเปลี่ยนเป็นสถานะนี้ได้ค่ะ\");";
				echo "window.location.replace('ระบบหลังบ้านร้านค้า.php?win_scroll=$win_scroll')";
			echo "</script>";
		}else{
		
			/* Update Status */
			$sql = "UPDATE `orders` SET order_status='$order_status' WHERE order_group='$order_group' ";
			$res= $con->query($sql) or die($con->error); //Check error

			/* Send Email to update status */
			// Set Value for Email
			$order_number = $roworders['order_number'];
			$contact_email = $roworders['contact_email'];
			$shipping_code = $roworders['shipping_code'];

			echo $order_number.' '.$contact_email.' '.$shipping_code;

			switch($order_status) {
				case 'กำลังจัดส่ง':
					$header_title = 'ยืนยันยอดเงินแล้ว กำลังเตรียมจัดส่งค่ะ';
					$order_status_color = 'red';
				break;
				case 'จัดส่งแล้ว':
					$header_title = 'ส่งสินค้าแล้ว กำลังอยู่ระหว่างการจัดส่งโดย บริษัท ไปรษณีย์ไทย จำกัด ค่ะ';
					$order_status_color = 'red';
					$shipping_code_text = ' <p><strong>รหัสติดตามสถานะการขนส่ง: </strong>'.$shipping_code.'</p> ';
				break;
				case 'สินค้าถึงปลายทางแล้ว':
					$header_title = 'ส่งสินค้าถึงปลายทางเรียบร้อย';
					$order_status_color = 'green';
				break;
			}

			/* SUM all products price */
			$sqltotal="SELECT SUM(product_price_sale*product_amount) AS price_total FROM `orders` WHERE order_number='$order_number' ";
			$retotal=mysqli_query($con,$sqltotal);
			$rowtotal=mysqli_fetch_array($retotal);
			$price_total=$rowtotal['price_total'];
			/* Send order summary by email */
			$sqlorders = "SELECT * FROM `orders` WHERE order_number='$order_number' ";
			$reorders = mysqli_query($con,$sqlorders);
			
			$shipping_kind = 'Free';//$roworders['shipping_kind'];
			/* Set shipping_kind */
			/*
			if($shipping_kind == 'normal') {
				$shipping_cost = 50;
			}else{
				$shipping_cost = 80;
			}
			/* Set grand_total */
			$grand_total = number_format($price_total+$shipping_cost,2);
			/* Set shipping_address */
			$shipping_address = $roworders['shipping_address'];
			/* Set contact detail */
			$contact_name = $roworders['contact_name'];
			$contact_number = $roworders['contact_number'];

			$strTo = $contact_email;
			$strSubject = "มูลนิธิครูดีของแผ่นดิน :: รายงานการสั่งซื้อเลขที่ ".$order_number."  ";
			$strHeader = "Content-type: text/html; charset=UTF-8\n"; // or windows-874 //
			$strHeader .= "From: thaisuprateacher@gmail.com\n";
			$htmlContent = '
												<html>
												<head>
														<title>รายงานการสั่งซื้อสินค้า</title>
												</head>
												<body>
													<h1>'.$header_title.'</h1>
													<p><strong>เลขที่ใบสั่งซื้อ: </strong>'.$order_number.'</p>
													<p><strong>สถานะ: </strong><span style="color:'.$order_status_color.';">'.$order_status.'</span></p>
													'.$shipping_code_text.'
													<table cellspacing="0" style="width: 100%; font-size: 15px; text-align: center;">								
														<tr style="background-color: rgb(72,160,0); color: white;">
															<th>รายการที่</th>
															<th>สินค้า</th>
															<th>จำนวน</th>
															<th>ราคาสุทธิ</th>
														</tr>
														';
														$i=1;
														while($roworders = $reorders->fetch_assoc()) {
															/* Set product_image, product_name, product_size, product_detail */
															$product_id = $roworders['product_id'];
															$sql="SELECT * FROM shelf WHERE product_id='$product_id' ";
															$result=mysqli_query($con,$sql);
															$row=mysqli_fetch_array($result);
			$htmlContent .= '			
														<tr>
															<td>'.$i.'</td>
															<td>'.$row['product_name'].'</td>
															<td>'.$roworders['product_amount'].'</td>
															<td>'.number_format($roworders['product_price_sale']*$roworders['product_amount'],2).'</td>
														</tr>
											';
														$i++; }
			$htmlContent .= '		
													</table>
													<div style="width:50%; color:black; background-color:rgb(240,240,240); border:15px solid rgb(240,240,240); border-radius:8px; margin-top:30px;">
														<!-- Total price -->
														<h3>สรุปยอดการสนับสนุน</h3>
														<div class="d-flex">
															<div class="ml-auto font-weight-bold">'.number_format($price_total,2).' บาท </div>
														</div>
														<hr>
														<!-- Shipping choice confirmation -->
														<div class="d-flex">
															<h3>วิธีการจัดส่ง</h3>
															<label>
											';





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////



								/* ฟรีค่าจัดส่ง ระหว่าง 13 ก.ย. - 31 ธ.ค. 2563 */
								
								$htmlContent .= '
								ฟรีค่าจัดส่ง<br>
								ระยะเวลาการจัดส่ง: 3 - 7 วันทำการ
						';

/*							
												if($shipping_kind == 'normal') {
$htmlContent .= '
										+ ค่าจัดส่ง 50 บาท
										พัสดุลงทะเบียน<br>
										ระยะเวลาการจัดส่ง: 3 - 7 วันทำการ
								';
										}else{
$htmlContent .= '
										+ ค่าจัดส่ง 80 บาท
										พัสดุด่วนพิเศษ EMS<br>
										ระยะเวลาการจัดส่ง: 1 - 3 วันทำการ
								';
										}
*/




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////






			$htmlContent .= '
															</label>
														</div>
														<hr>
														<!-- Grand Total (Total price + Shipping cost) -->
														<div class="d-flex gr-total">
															<div class="ml-auto h5">
																<h3>สรุปยอดทั้งหมด</h3>
																	'.$grand_total.'
																บาท
																</div>
															</div>
															<hr>
															<!-- Shipping Address -->
															<div class="d-flex">
																<h3>ที่อยู่ในการจัดส่ง</h3>
																<div class="location-box">
																	'.$shipping_address.'
																</div>
															</div>
															<hr>
															<!-- Receiver Name -->
															<div class="d-flex">
																<h3>ชื่อ-นามสกุล ผู้รับ</h3>
																'.$contact_name.'
															</div>
															<hr>
															<!-- Receiver Phone Number -->
															<div class="d-flex">
																<h3>เบอร์ติดต่อ ผู้รับ</h3>
																<div class="phonenum-box">
																	'.$contact_number.'
																</div>
															</div>
														</div>
													</div>
												</body>
												</html>
											';
			$flgSend = mail($strTo,$strSubject,$htmlContent,$strHeader);

			header('location: ระบบหลังบ้านร้านค้า.php?win_scroll='.$win_scroll);
		}
	}

?>