<?php 
	session_start();

	if(isset($_SESSION['ID'])) {

		if($_SESSION['level'] == 'memberDiamond') {

			echo '<script>';
				echo "alert('ระดับของท่านอยู่ขั้นสูงสุดแล้วค่ะ');";
				echo "window.location.replace('ระบบหลังบ้านบำรุงสมาชิก.php')";
			echo '</script>';

		}

	}else{

		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';

	}

?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>


<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="ส่งหลักฐานการโอน_บำรุงค่าสมาชิก-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom4Home.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="เกี่ยวกับการบริจาค.php"> ภาพรวมสนับสนุนมูลนิธิ</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="บำรุงค่าสมาชิก.php"> บำรุงค่าสมาชิก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> ส่งหลักฐานการโอน</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ส่งหลักฐานการโอน -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
	<!-- ################################################################################################ -->
	<!-- Start Slip Upload  -->
	<div class="cart-box-main">
    <div class="container">
      <div class="row">
				<!-- ################################################################################################ -->
				<!-- Start Left Content -->
				<div class="col-sm-6 col-lg-6 mb-3">
					<div class="slip-page">
						<div class="title-left">
							<h3>ช่องทางการบำรุงสมาชิก</h3>
						</div>
						<div class="row">
							<div class="col-md-6 mb-3">
								<img src="images/มูลนิธิครูดีของแผ่นดิน KTB Logo.jpg" alt="Logo KTB">
							</div>
							<div class="col-md-6 mb-3">
								<p>
									<strong>ธนาคารกรุงไทย</strong>
									<br>
									<strong>ชื่อบัญชี:</strong> มูลนิธิครูดีของแผ่นดิน สาขาถนนวิสุทธิกษัตริย์
									<br>
									<strong>เลขที่บัญชี:</strong> 006 – 0 – 20390 – 0
								</p>
							</div>
						</div>
						<div class="center m-t-30 m-b-30">
							<img style="width:50%;" src="images/คิวอาร์โคด บัญชีมูลนิธิ.jpg" alt="คิวอาร์โคด บัญชีมูลนิธิ">
						</div>
					</div>
				</div>
				<!-- End Left Content -->
				<!-- ################################################################################################ -->
				<!-- Start Right Content -->
				<div class="col-sm-6 col-lg-6 mb-3">
					<div class="col-md-12 col-lg-12">
						<div class="input-slip-upload">
							<div class="title-left">
								<h3>แจ้งบำรุงค่าสมาชิก</h3>
							</div>
							<!-- Start form input -->
							<form action="uploadslip_membership.php" method="POST" enctype="multipart/form-data">
								<div class="row m-t-20">
									<div class="col-md-6 mb-3">
										<label for="date_membership">วันที่ชำระเงิน *</label>
										<input type="date" name="date_membership" class="form-control" placeholder="" min='2020-04-14' max='<?php echo date("Y-m-d") ?>' required/>
									</div>
									<div class="col-md-6 mb-3">
										<label for="time_membership">เวลา (โดยประมาณ) *</label>
										<input type="time" name="time_membership" class="form-control" placeholder="" required/>
									</div>
								</div>
								<div class="row m-t-20">
									<div class="col-md-6 mb-3">
										<label for="membership_topic">ต้องการจะปรับระดับเป็น *</label>
										<select id="membership_topic" name="membership_topic" class="form-control" required>
											<option disabled="disabled" selected="selected" value="">โปรดเลือก</option>
											<?php
												if($_SESSION['level'] == 'memberSilver') { ?>

													<option value="memberGold">ระดับทอง</option>
													<option value="memberDiamond">ระดับเพชร</option> <?php

												}elseif($_SESSION['level'] == 'memberGold') { ?>

													<option value="memberDiamond">ระดับเพชร</option> <?php

												}else{ ?>

													<option value="memberSilver">ระดับเงิน</option>
													<option value="memberGold">ระดับทอง</option>
													<option value="memberDiamond">ระดับเพชร</option> <?php

												}
											?>
										</select>
									</div>
									<div class="col-md-6 mb-3">
										<label for="membership_slip_image">หลักฐานการโอน *</label>
										<input type="file" id="slip_image" name="slip_image" class="form-control" accept="image/*" required/>
									</div>
								</div>
								<div class="row m-t-20">
									<div class="col-md-6 mb-3">
										<label for="bill_tax_kind">นามในการออกใบเสร็จ *</label>
										<select id="bill_tax_kind" name="bill_tax_kind" class="form-control" onchange="PickBillIN(this.value);" required>
											<option disabled="disabled" selected="selected" value="">โปรดเลือก</option>
											<option value="Personal">บุคคล</option>
											<option value="Corporation">นิติบุคคล บริษัท/ห้าง/ร้าน</option>
											<option value="Government">หน่วยงานราชการ</option>
										</select>
									</div>
									<div class="col-md-6 mb-3" style="display:none" id="ID-Personal">
										<label for="ID-Personal">เลขบัตรประชาชน *</label>
										<input type="text" pattern="[0-9]{13,13}" maxlength="13" title="โปรดใส่ตัวเลข13ตัว" name="bill_tax_id_Personal" class="form-control" id="ID-Personal-required" />
									</div>
									<div class="col-md-6 mb-3" style="display:none" id="ID-Corporation">
										<label for="ID-Corporation">เลขประจำตัวผู้เสียภาษี *</label>
										<input type="text" pattern="[0-9]{13,13}" maxlength="13" title="โปรดใส่ตัวเลข13ตัว" name="bill_tax_id_Corporation" class="form-control" id="ID-Corporation-required" />
									</div>
									<div class="col-md-6 mb-3" style="display:none" id="ID-Government">
										<label for="ID-Government">ชื่อหน่วยงานของท่าน *</label>
										<input type="text" title="โปรดใส่ชื่อหน่วยงานของท่าน" name="bill_tax_id_Government" class="form-control" id="ID-Government-required" />
									</div>
								</div>
								<div class="center m-t-30 m-b-30 fs-16">
									*กรุณาตรวจทานรายละเอียดให้ถูกต้องอีกครั้ง ก่อนยืนยันการชำระเงิน
								</div>
								<div class="shopping-box">
									<button type="submit" name="submit" class="ml-auto btn hvr-hover">แจ้งบำรุงค่าสมาชิก</button>

									<input type="hidden" name="order_group" value="<?php echo $order_group ?>">
								</div>
							</form>
							<!-- End form input -->
						</div>
					</div>
      	</div>
				<!-- End Right Content -->
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
  <!-- End Slip Upload -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ส่งหลักฐานการโอน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->


<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<script src="js/pickBillType.js"></script>

</body>
</html>