<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start NoAccount Session -->
<?php
      if (empty($_SESSION['email']))
      {
?>
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
          <li class="faicon-login"><a href="login-en.php"><i class="fas fa-user-lock"></i> <text1>Log in</text1></a></li>
          <li class="faicon-login"><a href="signup-en.php"><i class="fas fa-file-signature"></i> <text1>Sign up</text1></a></li>
          <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
            <ul>
              <li><a href="หอเกียรติยศ006.php">ภาษาไทย</a></li>
              <!--<li><a href="#">Chinese</a></li>-->
            </ul>
          </li>
          <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
              <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
        </ul>
      </nav>
      <!-- End NoAccount Session -->
      <!-- ################################################################################################ -->
      <!-- Start Admin Session -->
<?php
      }
      elseif ($_SESSION['email'])
      {
        if ($_SESSION["level"]=="admin")
        {
?>
        <nav id="mainav2" class="PreMenu_fl_right">
          <ul class="PreMenufaico2">
            <li style="background-color:rgb(228,0,0);"><i class="fas fa-user-edit"></i> Admin</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ006.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Admin Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member General Session -->
<?php
        if ($_SESSION["level"]=="memberGeneral")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ006.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member General Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member SILVER Session -->
<?php
        if ($_SESSION["level"]=="memberSilver")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> Silver Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ006.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member SILVER Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member GOLD Session -->
<?php
        if ($_SESSION["level"]=="memberGold")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> Gold Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ006.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member GOLD Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member DIAMOND Session -->
<?php
        if ($_SESSION["level"]=="memberDiamond")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> Diamond Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ006.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
      }
?>
      <!-- End Member DIAMOND Session -->
      <!-- ################################################################################################ -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index-en.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index-en.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index-en.php">Home</a></li>
          <li><a href="เกี่ยวกับเรา-en.php">About us</a></li>
          <li class="joinus-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>Join us</a>
            <ul>
              <li><a href="ร่วมโครงการ-en.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <li><a href="โครงการครูดีของแผ่นดิน-en.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา-en.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA-en.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน-en.php">โครงการอาสาของแผ่นดิน</a></li>
            </ul>
          </li>
          
          <li><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">Innovation Library</a></li>
          <li><a href="ติดต่อเรา-en.php">Contact us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="หอเกียรติยศรวมlatest-en.php">Hall of Fame</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> ทิศทางการการสร้างคนดีให้แผ่นดิน โดยผู้บริหารระดับสูงกระทรวงศึกษาธิการ...</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - หอเกียรติยศ006 -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <article style="text-align:center" >
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px;">
        <strong>ทิศทางการการสร้างคนดีให้แผ่นดิน โดยผู้บริหารระดับสูงกระทรวงศึกษาธิการ<br>วันที่ 14 ธันวาคม 2563 ณ หอประชุมคุรุสภา กรุงเทพมหานคร<br>เนื่องในวาระครบรอบ 2 ปีการก่อตั้งมูลนิธิครูดีของแผ่นดิน</strong>
      </p>
      <!--
      <div class="bgvdo">
        <video width="100%" controls>
          <source src="images/วีดีโอนิสัยการทิ้งขยะ.mp4" type="video/mp4">
        </video>
      </div>
      -->
    </article>
    <p class="font-x2" style="text-align:right">โดย ดร.อโณทัย ไทยวรรณศรี ผู้อำนวยการสำนักพัฒนานวัตกรรมการจัดการศึกษา สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน</p>
    <p class="font-x3"><span style="color:rgb(180,147,31); line-height:30px; text-align:right;"><strong>บทความ</strong></span></p>
    <p class="font-x2" style="font-family:ChulaCharasNew; line-height:30pt; text-align:left;">
      กราบเรียนพลเอกเอกชัย ศรีวิลาศ ประธานมูลนิธิครูดีของแผ่นดิน ท่านผู้มีเกียรติทุกๆท่านนะครับ ในประเด็นที่อยากจะพูดคุยกับทุกๆท่าน สมาชิกครูดีของแผ่นดิน คือ เรื่องของการใช้อำนาจในโรงเรียน สมัยก่อนเรายังมองไม่เห็นระบบชัดเจน จึงเป็นการจัดการเรียนการสอนทั่วไป แต่เรื่องการใช้อำนาจในโรงเรียน สถานการณ์นี้มีความจำเป็นอย่างยิ่งในปัจจุบันครับ 
      <br><br>
      อะไรบ้างที่บ่งบอกการใช้อำนาจในโรงเรียน ดูตัวอย่างจากนักเรียนได้ครับ เมื่อสักครู่มี VTR ปัญหาเด็กแต่งกายไม่เป็นระเบียบ  เด็กถูกรังแก ขาดความเห็นอกเห็นใจซึ่งกันและกัน อะไรต่างๆ เหล่านี้แสดงให้เห็นว่าพฤติกรรมของเด็กอยากมีความสำคัญ อยากมีบทบาทเป็นที่สนใจ อยากให้ตัวเองเป็นผู้มีอำนาจเหนือคนอื่น  อันนี้มาจากตัวเด็กเองนะครับ
      <br><br>
      แต่ถ้ามองในส่วนของคุณครูเราจะสังเกตเวลาเราเข้าไปทำงาน โรงเรียนที่บ่งบอกการใช้อำนาจ ส่วนใหญ่คุณครูจะต่างคนต่างดูแลห้องเรียนของตัวเอง ขาดความเอาใจใส่ภาพรวม ว่ามีเหตุการณ์อะไรเกิดขึ้นโรงเรียน ถ้าไม่ใช่ห้องเรียนของฉัน ไม่ใช่โครงการของฉัน ฉันไม่เกี่ยวข้อง นอกนั้นเป็นหน้าที่ของผู้อำนวยการโรงเรียน 
      <br><br>
      และอีกตัวอย่างหนึ่งที่มองเห็นได้ชัดเจน คือ โรงเรียนส่งเสริมกิจกรรมการใช้อำนาจในโรงเรียนด้วยหรือเปล่า และหลายครั้งเราก็ไม่รู้ว่ากิจกรรมต่างๆ ที่จัดขึ้นนั้น ส่งผลต่อการใช้อำนาจในโรงเรียนด้วยหรือไม่ ยกตัวอย่าง เช่น ข่าวล่าสุดว่ามีการจัดงานต้อนรับผู้อำนวยการโรงเรียนคนใหม่ และมีพิธีการให้นักเรียนก้มกราบ เหมือนต้อนรับเจ้าหน้าที่ชั้นสูง อันนี้เป็นพฤติกรรมที่ก่อให้เกิดปรากฏการณ์เลียนแบบในโรงเรียน 
      <br><br>
      สมัยก่อนที่เราเป็นครู เวลาเราไม่อยู่ห้องเรียนก็จะฝากหัวหน้าห้อง อ้าว เธอดูเพื่อนในชั้นเรียนให้ครูหน่อย เราก็จะเห็นพฤติกรรมของหัวหน้าห้องว่ามีการจดชื่อ ใครเสียงดัง บางทีถือไม้เรียวแทนเราก็มี เป็นต้น อันนี้คือระบบการใช้อำนาจโดยไม่รู้ตัว คือเด็กเกิดพฤติกรรมเลียนแบบการใช้อำนาจของครู ฉะนั้นวิธีการคือทำอย่างไร ให้วัฒนธรรมการใช้อำนาจในโรงเรียนหมดไป ซึ่งสิ่งนี้มีความสำคัญมาก 
      <br><br>
      นโยบายที่ออกมาในรูปแบบกระบวนการหรือวิธีการต่างๆ ในช่วง 2-3 ปี มานี้ ตัวอย่างเช่น ขบวนการ PLC ถึงแม้ว่าจะผูกไว้ในเรื่องของวิทยฐานะต่างๆ บางทีเราก็เห็นคุณครูทำผิดวัตถุประสงค์ เช่น การทำรายงาน ว.21 บอกว่าการจัดชั้นเรียน มีการประชุมจัดชั้นเรียน มีการประชุมไปทัศนศึกษาจัดเป็นกระบวนการ PLC อย่างนี้ไม่ใช่ กระบวนการ PLC ที่แท้จริง คือ การที่ท่านผู้บริหาร คุณครู มาร่วมพูดคุยกันในเรื่องของพฤติกรรมเด็ก ในเรื่องการเรียนการสอน 
      <br><br>
      ซึ่งกระบวนการเหล่านี้ ตัวผู้อำนวยการและคุณครูต้องมีฐานะเท่าเทียมกัน ไม่เหมือนการประชุมสั่งการทั่วไป ซึ่งกระบวนการแบบนี้ถ้าเกิดขึ้นบ่อยๆ หรือลงลึกไปในระดับของนักเรียนด้วยแล้ว เช่นเมื่อสักครู่น้องนักเรียนของโรงเรียนดอนเมือง อธิบายเรื่องของแอคทีฟ อันนี้คือกระบวนอันหนึ่งที่ทำให้เด็กมีส่วนร่วมในการแสดงความคิดเห็นต่างๆ ซึ่งเป็นสิ่งที่ดี
      <br><br>
      ภาพเหล่านี้จะช่วยลดบทบาทการใช้อำนาจในโรงเรียนได้ครับ ผมขอชื่นชมคุณครูทั้งสองท่าน ซึ่งถือว่าเป็นคุณครูตัวแทนรุ่นใหม่ที่ใช้กระบวนการลดอำนาจในส่วนที่เป็นพฤติกรรมส่วนตัวของคนเรา ซึ่งหลายๆ ครั้งอาจจะเกี่ยวพันกับพฤติกรรมของเด็ก ส่งผลกระทบมาถึงตัวเราด้วย ทำให้บางทีคุณครูหมดความอดทนจะแสดงอารมณ์หงุดหงิดออกมา ทำให้เกิดเป็นข่าวอะไรต่างๆ และขอชื่นชมอีกหลายท่านนะครับ อย่างท่านผู้อำนวยการ สพม.21 ท่านสมใจ ท่านทำเป็นระบบเลย เอาเรื่องจิตศึกษาเข้ามาช่วย ซึ่งถ้ามีโอกาสอยากจะเชิญท่านสมใจมานำเสนอให้กับพวกเราได้ทราบนะครับ ขออนุญาตได้นำเรียน เรื่องที่สำคัญเรื่องการใช้อำนาจในโรงเรียนครับ 
      <br><br>
      ชื่นชมทางมูลนิธิ และปีนี้เป็นปีที่ให้ความสำคัญ ทราบว่าจะทำเรื่องการวัดผลและการพัฒนา สอดคล้องกับผู้แทนขององค์กรปกครองส่วนท้องถิ่น เมื่อวานมีการประชุมที่ กพฐ.  ว่าเรากำลังทำเรื่องหลักสูตรการศึกษาขั้นพื้นฐานอยู่ สมรรถนะของตัวคุณครู และเห็นยุทธศาสตร์ที่ลึกซึ้งของมูลนิธิ ที่ผมเองยังต้องถามนอกเวทีว่า ทำอย่างไรทางสมาคมศึกษานิเทศก์ได้ เพราะศึกษานิเทศก์เรามีทุกสังกัด ทุกพื้นที่ และได้ทีมศึกษานิเทศก์มาเป็นผู้ที่สอดส่องดูแล ตรวจสอบดูว่าคนไหนที่ทำงานดี เสียสละ อุทิศตน ซึ่งมีอยู่มากมาย ในหลายรูปแบบที่บ่อยครั้งเราแข่งขันหรือประกวดครูที่มีผลงาน ไม่ว่าจะเป็นในส่วนของการรับสมัครการทำเอกสารอะไรขึ้นมา แต่จะมีคุณครูอีกหลายท่านที่อยู่ในหลายพื้นที่ ที่ไม่อยากจะเปิดเผยตัวเอง 
      <br><br>
      อย่างเช่นที่ผมอยู่ที่ ตาก 2 พื้นที่ชายแดนมีคุณครูที่เค้าอยู่บนดอย เค้าไม่ทำวิทยฐานะ ตั้งแต่ชำนาญการเค้าก็ไม่ทำ ชำนาญการพิเศษเค้าก็ไม่ทำ เค้าก็มีความสุขอยู่ได้ ผมเชื่อว่าศึกษานิเทศก์เข้าไปถึง แต่เราไปทำในนามของวิธีการในเชิงบวก สนับสนุนคนดี อาจจะอีกรูปแบบหนึ่ง ซึ่งตอนนี้ในเรื่องของเครือข่าย ยกตัวอย่างเช่น กพฐ. เค้าจะมีชมรมสตรองที่ไปดูแลในพื้นที่ เราทำในเชิงบวก ซึ่งถือว่าเป็นเรื่องดี แล้วก็ในส่วนของ สพฐ.เองก็ยินดีจะมาสำรวจ โดยเฉพาะสำนักพัฒนานวัตกรรม รับผิดชอบในเรื่องของคุณธรรมทั้งหมด โรงเรียนคุณธรรม โรงเรียนวิถีพุทธ ยินดีที่เป็นส่วนที่เกี่ยวกัน ขอบคุณครับ
    </p>
  </main>
</div>
<!-- End Content 01 - หอเกียรติยศ006 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear">
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Related Department</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Foundation of Thai Suprateacher</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End Copyright Tab -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Back2Top Button -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Back2Top Button -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>