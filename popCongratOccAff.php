<?php
	session_start();
	require_once('condb.php');

	$ID = $_SESSION['ID'];
	$sqllogin = "SELECT * FROM `login` WHERE ID='$ID' ";
	$relogin = mysqli_query($con,$sqllogin);
	$rowlogin = mysqli_fetch_array($relogin);

	var_dump($_POST);
	exit();

?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<!-- ################################################################################################ -->
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="index.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าหลัก</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<!-- Start show step -->
				<?php
					if($_GET['CFP'] != 30) { // ถ้าไม่ใช่ User ใหม่ หรือ ไม่ได้มาจากหน้าที่ User พึงสมัครใหม่มา แล้วกดสมัครโครงการ ?>
						<div class="login100-form validate-form">
							<div class="center">
								<span class="dot BG-green1"></span>
								<span class="line BG-green1"></span>
								<span class="dot BG-green1"></span>
								<span class="line BG-green1"></span>
								<span class="dot BG-green1"></span>
							</div>
							<div class="right">
								<p style="color:rgb(94,177,26);">ขั้นตอนที่ 3: เสร็จสิ้น</p>
							</div>
						</div> <?php
					}
				?>
				<!-- End show step -->
				<!-- ################################################################################################ -->
				<div class="signup-stat m-t-20">
					<h8>การสมัครเข้าร่วมโครงการสมบูรณ์</h8>
					<p>คุณสามารถเลื่อนระดับสมาชิก <a href="บำรุงค่าสมาชิก.php">ได้ที่นี่</a></p>
				</div>
				<!-- ################################################################################################ -->
				<span class="login100-form-title fs-30 lh-1-1">
					<a href="all_project.php" class="btn2">ดูโครงการของมูลนิธิ</a>
				</span>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>