<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start NoAccount Session -->
<?php
      if (empty($_SESSION['email']))
      {
?>
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
          <li class="faicon-login"><a href="login-en.php"><i class="fas fa-user-lock"></i> <text1>Log in</text1></a></li>
          <li class="faicon-login"><a href="signup-en.php"><i class="fas fa-file-signature"></i> <text1>Sign up</text1></a></li>
          <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
            <ul>
              <li><a href="หอเกียรติยศ004.php">ภาษาไทย</a></li>
              <!--<li><a href="#">Chinese</a></li>-->
            </ul>
          </li>
          <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
              <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
        </ul>
      </nav>
      <!-- End NoAccount Session -->
      <!-- ################################################################################################ -->
      <!-- Start Admin Session -->
<?php
      }
      elseif ($_SESSION['email'])
      {
        if ($_SESSION["level"]=="admin")
        {
?>
        <nav id="mainav2" class="PreMenu_fl_right">
          <ul class="PreMenufaico2">
            <li style="background-color:rgb(228,0,0);"><i class="fas fa-user-edit"></i> Admin</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ004.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Admin Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member General Session -->
<?php
        if ($_SESSION["level"]=="memberGeneral")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ004.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member General Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member SILVER Session -->
<?php
        if ($_SESSION["level"]=="memberSilver")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> Silver Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ004.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member SILVER Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member GOLD Session -->
<?php
        if ($_SESSION["level"]=="memberGold")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> Gold Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ004.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member GOLD Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member DIAMOND Session -->
<?php
        if ($_SESSION["level"]=="memberDiamond")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> Diamond Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ004.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
      }
?>
      <!-- End Member DIAMOND Session -->
      <!-- ################################################################################################ -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index-en.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index-en.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index-en.php">Home</a></li>
          <li><a href="เกี่ยวกับเรา-en.php">About us</a></li>
          <li class="joinus-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>Join us</a>
            <ul>
              <li><a href="ร่วมโครงการ-en.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <li><a href="โครงการครูดีของแผ่นดิน-en.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา-en.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA-en.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน-en.php">โครงการอาสาของแผ่นดิน</a></li>
            </ul>
          </li>
          
          <li><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">Innovation Library</a></li>
          <li><a href="ติดต่อเรา-en.php">Contact us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="หอเกียรติยศรวมlatest-en.php">Hall of Fame</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> ทิศทางการการสร้างคนดีให้แผ่นดิน โดยผู้บริหารระดับสูงกระทรวงศึกษาธิการ...</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - หอเกียรติยศ004 -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <article style="text-align:center" >
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px;">
        <strong>ทิศทางการการสร้างคนดีให้แผ่นดิน โดยผู้บริหารระดับสูงกระทรวงศึกษาธิการ<br>วันที่ 14 ธันวาคม 2563 ณ หอประชุมคุรุสภา กรุงเทพมหานคร<br>เนื่องในวาระครบรอบ 2 ปีการก่อตั้งมูลนิธิครูดีของแผ่นดิน</strong>
      </p>
      <!--
      <div class="bgvdo">
        <video width="100%" controls>
          <source src="images/วีดีโอนิสัยการทิ้งขยะ.mp4" type="video/mp4">
        </video>
      </div>
      -->
    </article>
    <p class="font-x2" style="text-align:right">โดย ท่านตติยา ใจบุญ ผู้อำนวยการศูนย์วิทยาศาสตร์เพื่อการศึกษา กศน.</p>
    <p class="font-x3"><span style="color:rgb(180,147,31); line-height:30px; text-align:right;"><strong>บทความ</strong></span></p>
    <p class="font-x2" style="font-family:ChulaCharasNew; line-height:30pt; text-align:left;">
      ขอกราบเรียนท่านประธาน สวัสดีครูดีทุกท่าน ดิฉันมาในนาม กศน. ซึ่งได้รับหมอบหมายจากท่านเลขา จริงๆ แล้วครู กศน. มีประมาณหลักหมื่นคน  ครู กศน.ไม่ใช่เฉพาะที่เป็นครูสอนการศึกษาขั้นพื้นฐานอย่างเดียวนะคะ เราจะสอนตามอัธยาศัยด้วย รูปแบบของครูแบบดิฉันจริงๆไม่ได้เป็นครูจริง แต่เป็นนักวิชาการศึกษา จริงๆ ก็สอนแบบครู มีหลักความเป็นครู กศน. อย่างมาก ดิฉันทำราชการในตำแหน่งนี้มา 35 ปี ไม่เคยย้ายไปไหน อยู่ที่ศูนย์วิทยาศาสตร์เพื่อการศึกษาท้องฟ้าจำลอง ซึ่งหลายท่านอาจจะไม่ทราบว่าที่นี่คือ กศน. ด้วย  
      <br><br>
      กศน. คือ สำนักงานการศึกษานอกระบบ การศึกษานอกระบบที่มาเด็กมาลงทะเบียนขั้นพื้นฐานประมาณ 9 แสนคน การศึกษาตามอัธยาศัยเป็นการจัดการเรียนโดยหลักศูนย์วิทยาศาสตร์ทั่วประเทศ 20 แห่ง และห้องสมุดประชาชนทั่วประเทศ 900 แห่ง แล้วยังมีทีวีทางวิทยุศึกษา ปี 1 จริงๆ แล้วลูกค้าของเรา คือ นักศึกษาขั้นพื้นฐาน ซึ่งต้องเรียนทั้งอัธยาศัย ทั้งเรียนพื้นฐาน และครูต้องจัดทั้งอัธยาศัยให้ด้วย เพราะฉะนั้นเวลาเรามองต้องคิดว่าหัวใจของการสร้างคนดี เราต้องเข้าใจนักศึกษาผู้เรียนของเราทั้งหมดด้วย 
      <br><br>
      กรณีที่เป็นนักศึกษาพื้นฐานที่จะมาเรียนเพื่อได้วุฒิการศึกษา เราต้องรู้ว่าเค้าเป็นใครบ้าง เดี๋ยวนี้เป็นนักเรียนที่ออกนอกระบบมาอายุน้อยๆ ก็มีเยอะ ผู้ใหญ่เหมือนน้อยลงนะคะ นักเรียนนอกระบบจะมีปัญหาชีวิตหลายๆ แบบ คือหลักๆ เราต้องเข้าใจเค้ามากๆ ครูเราจริงๆ แล้ว ถ้านับว่าเป็นครูดี ดิฉันก็ขอยืนยันว่าครู กศน. เป็นครูที่มีจิตใจดี ที่เมตตากรุณาสูงมาก มีการอุทิศตัวสูงมาก มีส่วนน้อยที่เค้าได้เป็นข้าราชการที่ถาวร เรียกว่าหลักหมื่นคน ส่วนหนึ่งเป็นพนักงานราชการ เป็นพนักงานจ้างเหมาบริการด้วยซ้ำ ซึ่งเป็นความยากลำบากพอสมควร แต่เราก็ยืนยันว่าดี เพราะฉะนั้นจริงๆ แล้ว กศน.ในปี งบประมาณ 2563 ท่านรัฐมนตรีช่วยกนกวรรณ ท่านได้มอบนโยบาย เรียกว่า ว้าว Wow ต้องบอกว่า Good  teacher  คือพัฒนาครูของเรา ซึ่งจริงๆ จิตใจดีอยู่แล้ว ให้มีความสามารถมากขึ้น เพราะต้องเราเข้าใจเด็กของเราด้วย 
      <br><br>
      สาเหตุที่เรียนเพราะ 1. อยากได้วุฒิการศึกษา อยากประกอบอาชีพ หรือคนที่มาตามอัธยาศัย เราจะต้องเสริมความรู้ตรงนี้ให้ครู เพราะว่า ครู กศน. ต้องไม่หยุดเรียนรู้ เพราะว่าเราอยู่กับประชาชน เราใกล้ชิดประชาชน แล้วเราก็มีความเชื่ออีกอย่างว่า การที่เด็กดีหรือนักศึกษาเราหรือประชาชนที่รับบริการเราเป็นคนดีได้ นอกจากครูดีแล้วต้องมีคนที่บ้านดีด้วย ทั้งเป็นคุณพ่อคุณแม่ 
      <br><br>
      แล้วอีกส่วนหนึ่งนะคะ ที่กศน.ไม่เคยลืมเลย คือ เราต้องเสริมสร้างชุมชนที่ดี อันนี้ครูของเราจะได้รับการพัฒนาทักษะที่เข้าถึงชุมชน ความทันสมัย แนวโน้มใหม่ของเด็กอยากเป็นอะไร ดิฉันไปอ่านว่าจริงๆ แล้วเด็กสมัยใหม่อยากเรียนรู้ภาษาหลายภาษา เราจึงต้องพัฒนาครูของเราให้รู้ตรงนี้ หรือรู้เรื่องที่ทันสมัย อย่างตอนนี้ทุกอย่างเป็นดิจิทัล เป็น AI อะไรหลายอย่าง  เราก็พยายามอบรมเรื่องตรงนี้ ให้ทันสมัย นอกจากนั้นในเรื่องเข้าสู่ชุมชน เราจะเพิ่มเรื่องภาษาเข้าไปด้วย กิจกรรมจิตอาสาของ กศน. จะมีบ่อยมาก หรือแม้แต่บุคลากรของ กศน.ทั้งหลาย จะเป็นเรื่องที่เราทำโดยเป็นอันหนึ่งอันเดียวกับชุมชน ดิฉันมีกรณีศึกษาเล่าให้ฟังนิดหน่อย 
      <br><br>
      ในเรื่องการทำงานกับชุมชน ดิฉันอยู่ศูนย์วิทยาศาสตร์ท้องฟ้าจำลอง เรามีการประกวดโครงงานประจำปีเป็นโครงงานวิทยาศาสตร์ นักศึกษา กศน. ผู้ใหญ่ ทีมที่ชนะเลิศเป็นที่เป็นช่างอ็อกหม้อน้ำไฟฟ้า อยู่ชุมชนแถวสวนหลวง เค้าทำโครงงานเรื่องรถรักการอ่านเคลื่อนที่โดยอนุรักษ์พลังงาน เวลาเค้าออกแบบโครงงาน เค้าไปช่วยกันคิดในชุมชนซึ่งน่ารักมาก โดยใช้นักศึกษาเพียง 3 คน แต่ครูนำไปถึงชุมชน ออกแบบรถรักการอ่าน โดยได้เศษเหล็กมาดีไซน์ จนได้รางวัลชนะเลิศ การทดลองเพิ่มประสิทธิภาพของรถ เค้าเข้าไปขับรถในชุมชน ดิฉันคิดว่าการที่จะเป็นคนดี เราจะต้องมีทักษะเรื่องมวลชนสัมพันธ์ด้วย อันนี้ก็เป็นมุมมองของ กศน.
      <br><br>
      จริงๆ แล้วทาง กศน.จะมีประกวดให้รางวัลครูของเราเป็นประจำ กำลังมองว่าครูที่ได้รางวัลน่าจะได้ร่วมเข้าเป็นสมาชิกเครือข่ายนี้และอีกอย่างหนึ่ง กรณีศึกษาต่างๆ ที่ทางเครือข่ายนี้มีอยู่แล้ว กับครูที่มาเรียนรู้น่าจะมีฐานข้อมูลที่ใช้ร่วมกันได้ อย่างที่ท่านท้องถิ่นว่านะคะ จะแลกเปลี่ยนเรียนรู้ซึ่งกันและกัน อีกอย่างหนึ่งทาง กศน. ทุกๆ แบบเราจะต้องบูรณาการความเป็นคนดีเข้าไปอย่างจริงจังและชัดเจน เราคิดว่าจะเผยแพร่กลับมาทางเครือข่าย คิดว่าน่าจะเป็นไปได้มากที่สุดคะ ส่วนเรื่องขยายมากขึ้นเรื่อยๆ ในอนาคต คุณครูที่ได้รางวัลเริ่มต้นจากตรงนี้ ได้มีโอกาสมาแลกเปลี่ยนเรียนรู้ซึ่งกันและกันคะ
    </p>
  </main>
</div>
<!-- End Content 01 - หอเกียรติยศ004 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear">
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Related Department</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Foundation of Thai Suprateacher</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End Copyright Tab -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Back2Top Button -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Back2Top Button -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>