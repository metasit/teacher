

<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");

	$ID_sup = $_GET['ID'];
	$honor_score_status = $_GET['honor_score_status'];
	$winScroll = $_GET['winScroll'];
	$honor_score_date = date("Y-m-d H:i:s");

	$ID_ad = $_SESSION['ID'];

	$sqlcheck = "SELECT honor_score_remark FROM `login` WHERE ID='$ID_sup' ";
	$recheck = mysqli_query($con, $sqlcheck);
	$rowcheck = mysqli_fetch_array($recheck);

	if($honor_score_status == 'ปฏิเสธ') {
		$honor_score_remark = $rowcheck['honor_score_remark']; // เหตุผลที่ปฏิเสธ

		if(is_null($honor_score_remark) || empty($honor_score_remark)) {
			echo '<script>';
				echo "alert('กรุณาใส่เหตุผลที่ปฏิเสธที่ช่องหมายเหตุด้วยค่ะ ข้อความตรงนี้จะไปแสดงให้ศน.แก้ไขรับทราบเพื่อดำเนินการแก้ไข');";
				echo "window.location.replace('ระบบหลังบ้านศน.ขั้นเกียรติคุณ.php?winScroll=$winScroll')";
			echo '</script>';
		}else{
			/* Update Status */
			$sql = "UPDATE `login` SET honor_score_status='ปฏิเสธ', honor_score_date='$honor_score_date' WHERE ID='$ID_sup' ";
			$res = $con->query($sql) or die($con->error);

			/* Log Admin Action */
			$sqllog = "INSERT INTO `adscorelog` (`ID_ad`,`adscorelog_task`,`ID_user`,`adscorelog_detail`) 
			VALUES ('$ID_ad', 'ปฏิเสธ', '$ID_sup', 'ศน.,ขั้นเกียรติคุณ,$honor_score_remark') ";
			$relog = $con->query($sqllog) or die($con->error);

			header('location: ระบบหลังบ้านศน.ขั้นเกียรติคุณ.php?winScroll='.$winScroll);
		}
	}else{
		/* Update Status */
		$sql = "UPDATE `login` SET honor_score_status='Approve แล้ว', honor_score_date='$honor_score_date' WHERE ID='$ID_sup' ";
		$res= $con->query($sql) or die($con->error);
		/* Log Admin Action */
		$sqllog = "INSERT INTO `adscorelog` (`ID_ad`,`adscorelog_task`,`ID_user`,`adscorelog_detail`) VALUES ('$ID_ad','Approve แล้ว','$ID_sup','ศน.,ขั้นเกียรติคุณ') ";
		$relog = $con->query($sqllog) or die($con->error);
		
		header('location: ระบบหลังบ้านศน.ขั้นเกียรติคุณ.php?winScroll='.$winScroll);
		
	}
?>