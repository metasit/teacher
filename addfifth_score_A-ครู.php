<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	

	$ID = $_SESSION['ID'];

	$sqlfifth_score4tea = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='A' ";
	$reFS4T = mysqli_query($con, $sqlfifth_score4tea);
	$rowFS4T = mysqli_fetch_array($reFS4T);

	$CFP = $_GET['CFP'];

	/* Count row to check that we need to Insert or Update */
	$countrow = mysqli_num_rows($reFS4T);

	if($countrow == 0) { // If countrow=0, means no data add before so need to INSERT
		/* Prepare value for INSERT */
		$fifth_score4tea_code = 'A'; // Set category of fifth_score to A
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if($CFP == '57') { // Special Case: 55-57. 3 questions in a row
			$fifth_score_A = $_GET['fifth_score_A_55'].','.$_GET['fifth_score_A_56'].','.$_GET['fifth_score_A_57'];
			$fifth_score4tea_data = ',55.'.$_GET['fifth_score_A_55'].',56.'.$_GET['fifth_score_A_56'].',57.'.$_GET['fifth_score_A_57'];
			
		}else{ // Normal pattern of question
			$fifth_score_A = $_GET['fifth_score_A'];
			$fifth_score4tea_data = ','.$CFP.'.'.$fifth_score_A; // Set data to Insert in table (Possible for this case is only 1, the first question is answered)
		}

		$sql = "INSERT INTO `fifth_score4tea` (ID, fifth_score4tea_code, fifth_score4tea_data) VALUES ('$ID', '$fifth_score4tea_code', '$fifth_score4tea_data') ";
		$re = $con->query($sql) or die($con->error); //Check error
	}else{
		/* Prepare value for UPDATE */
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if($CFP == '57') { // Special Case: 55-57. 3 questions in a row
			$fifth_score_A = $_GET['fifth_score_A_55'].','.$_GET['fifth_score_A_56'].','.$_GET['fifth_score_A_57'];
			$fifth_score4tea_data = $rowFS4T['fifth_score4tea_data'].',55.'.$_GET['fifth_score_A_55'].',56.'.$_GET['fifth_score_A_56'].',57.'.$_GET['fifth_score_A_57'];
			
		}else{ // Normal pattern of question
			$fifth_score_A = $_GET['fifth_score_A'];
			$fifth_score4tea_data = $rowFS4T['fifth_score4tea_data'].','.$CFP.'.'.$fifth_score_A; // Set data to Update in table
		}

		$fifth_score4tea_date = date("Y-m-d H:i:s");

		$sql = "UPDATE `fifth_score4tea` SET fifth_score4tea_data='$fifth_score4tea_data', fifth_score4tea_date='$fifth_score4tea_date' WHERE ID='$ID' AND fifth_score4tea_code='A' ";
		$re = $con->query($sql) or die($con->error); //Check error
	}

	/* Finish all job. Send to another page */
	if($CFP == 79) {
		include('includes/calfifth_score-ครู.php');
		$fifth_score4tea_detail = ','.$selfscore_avg.','.$peoplescore_avg.','.$workscore_avg;

		$sql = "UPDATE `fifth_score4tea` SET fifth_score4tea_detail='$fifth_score4tea_detail' WHERE ID='$ID' AND fifth_score4tea_code='A' ";
		$re = $con->query($sql) or die($con->error); //Check error

		// Log User Action
		$sql = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='A' ";
		$re = mysqli_query($con, $sql);
		$row = mysqli_fetch_array($re);
		$scorelog_task = 'ทำประเมิน';
		$scorelog_detail = 'ครู,ชั้น5';
		$scorelog_ans = $row['fifth_score4tea_data'];

		$sqllog = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail, scorelog_ans) VALUES ('$ID', '$scorelog_task', '$scorelog_detail', '$scorelog_ans') ";
		$relog = $con->query($sqllog) or die($con->error); //Check error
		
		echo '<script>';
			echo "alert('คุณทำประเมินครูดีชั้นที่ 5 เสร็จแล้วค่ะ');";
			echo "window.location.replace('โครงการครูดีของแผ่นดินชั้นที่5-lobby.php')";
		echo '</script>';
	}elseif($CFP == 54) {
		$nextpage = '55-57';
		$CFP = '56';
		header("location: ประเมินครูดีของแผ่นดินชั้นที่5-ข้อ$nextpage.php?CFP=$CFP");
	}elseif($CFP == 57) {
		$nextpage = '58';
		$CFP = '57';
		header("location: ประเมินครูดีของแผ่นดินชั้นที่5-ข้อ$nextpage.php?CFP=$CFP");
	}else{
		$nextpage = $CFP+1;
		header("location: ประเมินครูดีของแผ่นดินชั้นที่5-ข้อ$nextpage.php?CFP=$CFP");
	}

?>