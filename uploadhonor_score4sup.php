<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if($_GET['check'] == 'new') { 
			/* POST valuable from input */
			// Set $honor_score4sup_date
			$honor_score4sup_date = $_POST['honor_score4sup_file_date'].' '.$_POST['honor_score4sup_file_time'];
			// Set $honor_score4sup_code & $honor_score4sup_file_no
			$honor_score4sup_code = $_POST['honor_score4sup_code'];
			$honor_score4sup_file_no = intval(substr($honor_score4sup_code, 1));
			// Set $honor_score4sup_data1 (affil_code, สถานที่ที่นิเทศ)
			include('includes/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/getaffil_code4honor_score4sup_location_new.php');
			$honor_score4sup_data1 = $affil_code_location_new.','.$affil_remark_location_new;
			// Set $honor_score4sup_data2 (file location)
			$target_dir = 'images/honor_score4sup/codeB/ID'.$ID.'/'; // Set target_directory
			
			if(!is_dir($target_dir)) { // if there's not folder in target_directory
				mkdir($target_dir); // Create folder name is today_date
			}
			
			$target_file = $target_dir.$honor_score4sup_code.basename($_FILES["honor_score4sup_file"]["name"]); // Set target_file
			$honor_score4sup_data2 = $target_file;
			// Set $honor_score4sup_data3 (ชื่อ-นามสกุล ผู้ถูกนิเทศ)
			$honor_score4sup_data3 = $_POST['honor_score4sup_teafirstname'].' '.$_POST['honor_score4sup_tealastname'];
			// Set $honor_score4sup_data4 (occup_code, ตำแหน่ง ผู้ถูกนิเทศ) and // Set $honor_score4sup_data5 (affil_code, สังกัด ผู้ถูกนิเทศ)
			include('includes/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/getoccup_code4honor_score4sup_pupil_new.php');
			$honor_score4sup_data4 = $occup_code.','.$occup_remark;
			// Set $honor_score4sup_data5
			include('includes/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/getaffil_code4honor_score4sup_pupil_new.php');
			$honor_score4sup_data5 = $affil_code_pupil_new.','.$affil_remark_pupil_new;
			// Set $honor_score4sup_data6 (รายงานการนิเทศ เป็น Text)
			$honor_score4sup_data6 = $_POST['honor_score4sup_text'];
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* Check that same place and same date? if yes, terminate this request */
			$createDate = new DateTime($honor_score4sup_date);
			$date4check = $createDate->format('Y-m-d');
			
			$sqlhonor_score4sup = "SELECT * FROM `honor_score4sup` WHERE `ID`='$ID' 
			AND `honor_score4sup_code` LIKE 'B%' 
			AND `honor_score4sup_data1`='$honor_score4sup_data1' 
			AND `honor_score4sup_date` LIKE '%$date4check%' 
			AND `honor_score4sup_code`!='$honor_score4sup_code' ";
			$reHS4S = mysqli_query($con, $sqlhonor_score4sup);
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if(mysqli_num_rows($reHS4S) != 0) {
				echo '<script>';
					echo "alert('ในการนิเทศ ณ สถานที่หนึ่งในหนึ่งวัน จะนับเป็นการนิเทศเพียง 1 งานเท่านั้น จึงไม่สามารถแนบการนิเทศนี้ได้ค่ะ');";
					echo "window.location.replace('โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php')";
				echo '</script>';
			}else{

				move_uploaded_file($_FILES["honor_score4sup_file"]["tmp_name"], $target_file);

				$sql = "INSERT INTO `honor_score4sup` (ID, honor_score4sup_code, honor_score4sup_data1, honor_score4sup_data2, honor_score4sup_data3, honor_score4sup_data4, honor_score4sup_data5, honor_score4sup_data6, honor_score4sup_date) 
				VALUES ('$ID', '$honor_score4sup_code', '$honor_score4sup_data1', '$honor_score4sup_data2', '$honor_score4sup_data3', '$honor_score4sup_data4', '$honor_score4sup_data5', '$honor_score4sup_data6', '$honor_score4sup_date') ";
				$re = $con->query($sql) or die($con->error); //Check error

				// Log User Action
				$scorelog_task = 'ส่งหลักฐานการนิเทศ'.$honor_score4sup_code;
				$scorelog_detail = 'ศน.,เกียรติคุณ'.','.$honor_score4sup_date.','.$honor_score4sup_data1.','.$honor_score4sup_data2.','.$honor_score4sup_data3.','.$honor_score4sup_data4.','.$honor_score4sup_data5.','.$honor_score4sup_data6;
		
				$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', '$scorelog_task', '$scorelog_detail') ";
				$re = $con->query($sql) or die($con->error); //Check error

				echo '<script>';
					echo "alert('ข้อมูลการนิเทศ ถูกส่งเรียบร้อยแล้วค่ะ');";
					echo "window.location.replace('โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php')";
				echo '</script>';
				
			}










		}elseif($_GET['check'] == 'old') {
			/* POST valuable from input */
			// Set $honor_score4sup_date
			$honor_score4sup_date = $_POST['honor_score4sup_file_date'].' '.$_POST['honor_score4sup_file_time'];
			// Set $honor_score4sup_code & $honor_score4sup_file_no
			$honor_score4sup_code = $_POST['honor_score4sup_code'];
			$honor_score4sup_file_no = intval(substr($honor_score4sup_code, 1));
			// Set $honor_score4sup_data1 (affil_code, สถานที่ที่นิเทศ)
			include('includes/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/getaffil_code4honor_score4sup_location_old.php');

			$honor_score4sup_data1 = $affil_code_location_old.','.$affil_remark_location_old;
			// Set $honor_score4sup_data3 (ชื่อ-นามสกุล ผู้ถูกนิเทศ)
			$honor_score4sup_data3 = $_POST['honor_score4sup_teafirstname'].' '.$_POST['honor_score4sup_tealastname'];
			// Set $honor_score4sup_data5 (affil_code, สังกัด ผู้ถูกนิเทศ)
			include('includes/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/getaffil_code4honor_score4sup_pupil_old.php');

			$honor_score4sup_data5 = $affil_code_pupil_old.','.$affil_remark_pupil_old;
			// Set $honor_score4sup_data6 (รายงานการนิเทศ เป็น Text)
			$honor_score4sup_data6 = $_POST['honor_score4sup_text'];
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* Check that same place and same date? if yes, terminate this request */
			$createDate = new DateTime($honor_score4sup_date);
			$date4check = $createDate->format('Y-m-d');
			
			$sqlhonor_score4sup = "SELECT * FROM `honor_score4sup` WHERE `ID`='$ID' 
			AND honor_score4sup_code LIKE 'B%' 
			AND honor_score4sup_code!= '$honor_score4sup_code'
			AND honor_score4sup_data1='$honor_score4sup_data1' 
			AND honor_score4sup_date LIKE '%$date4check%' ";
			$reHS4S = mysqli_query($con, $sqlhonor_score4sup);
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if(mysqli_num_rows($reHS4S) != 0) {
				echo mysqli_num_rows($reHS4S).'<br>'.$honor_score4sup_data1.'<br>'.$honor_score4sup_code;
				/*
				echo '<script>';
					echo "alert('ในการนิเทศ ณ สถานที่หนึ่งในหนึ่งวัน จะนับเป็นการนิเทศเพียง 1 งานเท่านั้น จึงไม่สามารถแนบการนิเทศนี้ได้ค่ะ');";
					echo "window.location.replace('โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php?winScroll=".$_POST['winScroll']."')";
				echo '</script>';
				*/
			}else{

				// Set $honor_score4sup_data2 (file location)
				if($_FILES["honor_score4sup_file"]["size"] > 0) { // ถ้า size > 0 หมายความว่า user มีการแนบไฟล์อัพโหลดเข้ามา ให้ดำเนินการ save file ลง DB
					$target_dir = 'images/honor_score4sup/codeB/ID'.$ID.'/'; // Set target_directory
					
					if(!is_dir($target_dir)) { // if there's not folder in target_directory
						mkdir($target_dir); // Create folder name is today_date
					}
					
					$target_file = $target_dir.$honor_score4sup_code.basename($_FILES["honor_score4sup_file"]["name"]); // Set target_file
					$honor_score4sup_data2 = $target_file;

					move_uploaded_file($_FILES["honor_score4sup_file"]["tmp_name"], $target_file);
				}else{
					$sqlhonor_score4sup_data2 = "SELECT honor_score4sup_data2 FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code='$honor_score4sup_code' ";
					$reHS4SD2 = mysqli_query($con, $sqlhonor_score4sup_data2);
					$rowHS4SD2 = mysqli_fetch_array($reHS4SD2);

					$honor_score4sup_data2 = $rowHS4SD2['honor_score4sup_data2'];
				}
				// Set $honor_score4sup_data4 (occup_code, ตำแหน่ง ผู้ถูกนิเทศ)
				include('includes/โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby/getoccup_code4honor_score4sup_pupil_old.php');
				$honor_score4sup_data4 = $occup_code.','.$occup_remark;




				// Start UPDATE data on DB
				$sqlhonor_score4sup_check_status = "SELECT honor_score4sup_check_status FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code='$honor_score4sup_code' ";
				$reHS4SCS = mysqli_query($con, $sqlhonor_score4sup_check_status);
				$rowHS4SCS = mysqli_fetch_array($reHS4SCS);

				$honor_score4sup_check_status = $rowHS4SCS['honor_score4sup_check_status'];

				if($honor_score4sup_check_status == NULL || is_null($honor_score4sup_check_status) || $honor_score4sup_check_status == '' || empty($honor_score4sup_check_status)) {

						$sql = "UPDATE `honor_score4sup` SET 
						`honor_score4sup_data1`='$honor_score4sup_data1', 
						`honor_score4sup_data2`='$honor_score4sup_data2', 
						`honor_score4sup_data3`='$honor_score4sup_data3', 
						`honor_score4sup_data4`='$honor_score4sup_data4', 
						`honor_score4sup_data5`='$honor_score4sup_data5', 
						`honor_score4sup_data6`='$honor_score4sup_data6', 
						`honor_score4sup_date`='$honor_score4sup_date'
						WHERE ID='$ID' AND honor_score4sup_code='$honor_score4sup_code' ";
						$re = $con->query($sql) or die($con->error); //Check error

						// Log User Action
						$scorelog_task = 'อัพเดทหลักฐานการนิเทศ'.$honor_score4sup_code;
						$scorelog_detail = 'ศน.,เกียรติคุณ'.','.$honor_score4sup_date.','.$honor_score4sup_data1.','.$honor_score4sup_data2.','.$honor_score4sup_data3.','.$honor_score4sup_data4.','.$honor_score4sup_data5.','.$honor_score4sup_data6;
						
						$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', '$scorelog_task', '$scorelog_detail') ";
						$re = $con->query($sql) or die($con->error); //Check error

						echo '<script>';
							echo "alert('ข้อมูลการนิเทศ ถูกอัพเดทเรียบร้อยแล้วค่ะ');";
							echo "window.location.replace('โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php?winScroll=".$_POST['winScroll']."')";
						echo '</script>';

				}else{

					/* Set $honor_score4sup_check_status */
					if(strpos($honor_score4sup_check_status, 'ปฏิเสธ,') !== false || strpos($honor_score4sup_check_status, 'แก้ไข,') !== false) {

						$first_comma_pos = strpos($honor_score4sup_check_status, ',');
						$last_comma_pos = strrpos($honor_score4sup_check_status, ',');
						$length_btw_comma = $last_comma_pos-$first_comma_pos;

						$honor_score4sup_check_status = 'แก้ไข,'.substr($honor_score4sup_check_status, $first_comma_pos+1, $length_btw_comma-1).','.date('d-m-Y H:i:s');

						$sql = "UPDATE `honor_score4sup` SET 
						`honor_score4sup_data1`='$honor_score4sup_data1', 
						`honor_score4sup_data2`='$honor_score4sup_data2', 
						`honor_score4sup_data3`='$honor_score4sup_data3', 
						`honor_score4sup_data4`='$honor_score4sup_data4', 
						`honor_score4sup_data5`='$honor_score4sup_data5', 
						`honor_score4sup_data6`='$honor_score4sup_data6', 
						`honor_score4sup_date`='$honor_score4sup_date',
						`honor_score4sup_check_status`='$honor_score4sup_check_status'
						WHERE ID='$ID' AND honor_score4sup_code='$honor_score4sup_code' ";
						$re = $con->query($sql) or die($con->error); //Check error

						// Log User Action
						$scorelog_task = 'แก้ไขรายงานการนิเทศ,'.$honor_score4sup_code;
						$scorelog_detail = 'ศน.,เกียรติคุณ'.','.$honor_score4sup_date.','.$honor_score4sup_data1.','.$honor_score4sup_data2.','.$honor_score4sup_data3.','.$honor_score4sup_data4.','.$honor_score4sup_data5.','.$honor_score4sup_data6;
						
						$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', '$scorelog_task', '$scorelog_detail') ";
						$re = $con->query($sql) or die($con->error); //Check error

						echo '<script>';
							echo "alert('ข้อมูลการนิเทศ ถูกแก้ไขแล้วค่ะ');";
							echo "window.location.replace('โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php?winScroll=".$_POST['winScroll']."')";
						echo '</script>';

					}else{
						echo '<script>';
							echo "alert('เกิดข้อผิดพลาดในการแก้ไขรายงาน กรุณาติดต่อมูลนิธิ // Error01: uploadhonor_score4sup.php');";
							echo "window.location.replace('โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php?winScroll=".$_POST['winScroll']."')";
						echo '</script>';
					}

				}

			}
		}
			
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
	
?>