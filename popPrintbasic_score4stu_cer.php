<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		$sqllogin = "SELECT * FROM `login` WHERE ID='$ID' ";
		$relogin = mysqli_query($con, $sqllogin);
		$rowlogin = mysqli_fetch_array($relogin);

		$basic_score_status = $rowlogin['basic_score_status'];

		if(strpos($basic_score_status, 'Approve แล้ว') !== false) { // เช็คว่า ได้รับการ Approve ไปแล้วหรือยัง ถ้ามีแล้วให้เข้าหน้านี้ได้

		}else{
			echo '<script>';
				echo "alert('ต้องผ่านโครงการเด็กดีของแผ่นดินขั้นพื้นฐานก่อนนะคะ ถึงเข้าใช้ระบบหน้านี้ได้');";
				echo "window.history.go(-1)";
			echo '</script>';
		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}

?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="โครงการเด็กดีของแผ่นดิน.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าโครงการเด็กดีของแผ่นดิน</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/เด็กดีของแผ่นดิน Logo.jpg" style="width: 70%;" alt="เด็กดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<br>
				<span class="login100-form-title fs-30 lh-1-1">
					เก่งมากเลยค่ะ ^^ ผ่านโครงการเด็กดีของแผ่นดินขั้นพื้นฐานแล้วนะคะ
					<br><br>
					พิมพ์เกียรติบัตรได้เลยค่ะ
					<br>
					<a href="printbasic_score4stu_cer.php?CFP=popPrintbasic_score4stu_cer" target="_blank" class="btn2">พิมพ์เกียรติบัตร</a>
				</span>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>