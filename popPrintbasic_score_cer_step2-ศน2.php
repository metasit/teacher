<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	/* Permanantly Confirm user's fistname and lastname (Cannot change anymore) */
	if($_POST['CFP'] == 20) { // If come from page popPrintbasic_score_cer_step1-ศน.php and click confirm firstname, lastname and school button
		
		if(empty($_POST['firstname'])) {
			$newfirstname = $_SESSION['firstname'];
		}else{
			$newfirstname = $_POST['firstname'];
		}
		if(empty($_POST['lastname'])) {
			$newlastname = $_SESSION['lastname'];
		}else{
			$newlastname = $_POST['lastname'];
		}
		$school = $_POST['school'];
		$cflist = $_SESSION['cflist'].'1*2*4*';

		$sql = "UPDATE `login` SET `firstname`='$newfirstname', `lastname`='$newlastname', `cflist`='$cflist'
		, `basic_score_status`='Approve แล้ว/ยืนยันแล้ว' WHERE ID='$ID' ";
		$res = $con->query($sql) or die($con->error); //Check error

		$sqllogin = "SELECT * FROM `login` WHERE ID='$ID' ";
		$relogin = mysqli_query($con,$sqllogin);
		$rowlogin = mysqli_fetch_array($relogin);

		$_SESSION['firstname'] = $rowlogin['firstname'];
		$_SESSION['lastname'] = $rowlogin['lastname'];
		$_SESSION['school'] = $rowlogin['school'];

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		/* Save เกียนติบัตรออนไลน์ with user name */
		$jpg_image = imagecreatefromjpeg('images/เกียรติบัตร/พื้นฐาน/ศน/เกียรติบัตรขั้นพื้นฐาน-ศน.jpg'); // Create Image From Existing File
		$font_color = imagecolorallocate($jpg_image, 0, 0, 0); // Set font color
		$font_path = 'layout/styles/fonts/THSarabunIT๙ Bold.ttf'; // Set font file path
		/* Set Text that need to be Printed On Image */
		//$name = $_SESSION['firstname'].' '.$_SESSION['lastname']; // Set name
		$name = 'นายสาวสมศรี ณ อยุธยา';
		//$school = $rowlogin['school']; // Set school name
		$school = 'โรงเรียนมูลนิธิครูดีของแผ่นดิน';
		$code_cer = 'คดผ.6300000002'; // Set Running Number for basic certificate
		//Set date
		$sqladscorelog = "SELECT * FROM `adscorelog` WHERE ID='$ID' AND adscorelog_task='Approve แล้ว' AND adscorelog_detail='ขั้นพื้นฐาน' ORDER BY adscorelog_id DESC LIMIT 1 ";
		$readscorelog = mysqli_query($con,$sqladscorelog);
		$rowadscorelog = mysqli_fetch_array($readscorelog);
		$date = new DateTime($rowadscorelog['adscorelog_date']);
		$date = $date->format("Y-m-d"); // Set date format

		/* Array for Thai Date */
		$arabicnum = array("1","2","3","4","5","6","7","8","9","0");
		$thainum = array("๑","๒","๓","๔","๕","๖","๗","๘","๙","๐");
		$test = str_replace($numthai,$numarabic,$message);
		/* Array for Thai Month */
		$month_arr=array(
			"1"=>"มกราคม",
			"2"=>"กุมภาพันธ์",
			"3"=>"มีนาคม",
			"4"=>"เมษายน",
			"5"=>"พฤษภาคม",
			"6"=>"มิถุนายน", 
			"7"=>"กรกฎาคม",
			"8"=>"สิงหาคม",
			"9"=>"กันยายน",
			"10"=>"ตุลาคม",
			"11"=>"พฤศจิกายน",
			"12"=>"ธันวาคม"
		);
		$date_cer = str_replace($arabicnum, $thainum, date('j', strtotime($date))).' '.$month_arr[date('n', strtotime($date))].' '.str_replace($arabicnum, $thainum, (date('Y', strtotime($date))+543)); // Set certicate date in Thai

		$font_size = 40; //Set font size
		$angle = 0; //Set angle
		/* Set x-position on certificate for name */
		$dimen4name = imagettfbbox($font_size, $angle, $font_path, $name);
		$text4name = (abs($dimen4name[4] - $dimen4name[0]))/2;
		$x4name = 758 - $text4name;
		/* Set x-position on certificate for name */
		$dimen4school = imagettfbbox($font_size, $angle, $font_path, $school);
		$text4school = (abs($dimen4school[4] - $dimen4school[0]))/2;
		$x4school = 758 - $text4school;
		/* Print firstname, lastname, school and date on certificate */
		imagettftext($jpg_image, $font_size, $angle, $x4name, 402, $font_color, $font_path, $name);
		imagettftext($jpg_image, $font_size, $angle, $x4school, 465, $font_color, $font_path, $school);
		imagettftext($jpg_image, $font_size, $angle, 620, 708, $font_color, $font_path, $date_cer);
		imagettftext($jpg_image, 30, $angle, 1150, 150, $font_color, $font_path, $code_cer);

		$target_dir = 'images/เกียรติบัตร/พื้นฐาน/ศน/'.$date.'/'; // Set target_directory

		if(!is_dir($target_dir)) { // if there's not folder in target_directory
			mkdir($target_dir); // Create folder name is today_date
		}

		imagejpeg($jpg_image,'images/เกียรติบัตร/พื้นฐาน/ศน/'.$date.'/'.$name.'.jpg');// Send Image to Browser or save in directory on client
		imagedestroy($jpg_image); // Clear Memory

	}else{
		$name = $_SESSION['firstname'].' '.$_SESSION['lastname'];
		//Set date
		$sqladscorelog = "SELECT * FROM `adscorelog` WHERE ID_user='$ID' AND adscorelog_task='Approve แล้ว' AND adscorelog_detail='ขั้นพื้นฐาน' ORDER BY adscorelog_id DESC LIMIT 1 ";
		$readscorelog = mysqli_query($con,$sqladscorelog);
		$rowadscorelog = mysqli_fetch_array($readscorelog);
		$date = new DateTime($rowadscorelog['adscorelog_date']);
		$date = $date->format("Y-m-d"); // Set date format
	}

?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าโครงการศึกษานิเทศก์ดีของแผ่นดิน</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<span class="login100-form-title fs-30 lh-1-1">
					คุณยืนยันการใช้ชื่อ-นามสกุล
					<br>
					<strong><?php echo $name; ?></strong>
					<br><br>
					<?php if($_SESSION['level']=="memberGeneral") { ?>
									เนื่องจากคุณเป็นสมาชิกระดับทั่วไป
									<br>
									ยินดีด้วยค่ะ คุณสามารถพิมพ์เกียรติบัตรแบบธรรมดาได้ค่ะ
									<br>
									<a href="<?php echo 'images/เกียรติบัตร/พื้นฐาน/ศน/'.$date.'/'.$name.'.jpg'; ?>" target="_blank" class="btn2">แบบออนไลน์</a>
									<br><br>
									คุณสามารถได้รับเกียรติบัตรแบบพิเศษ โดยการเป็นสมาชิกระดับเงิน, ทอง หรือ เพชร <a href="บำรุงค่าสมาชิก.php" target="_blank" class="readall fs-18">ได้ที่นี้</a>
									<br>
									<a href="images/เกียรติบัตร/พื้นฐาน/ครู/เกียรติบัตร ปี62.jpg" target="_blank" class="btn2">ตัวอย่างแบบพิเศษ ปี62</a>
					<?php }elseif($_SESSION['level']=="admin" || $_SESSION['level']=="memberSilver" || $_SESSION['level']=="memberGold" || $_SESSION['level']=="memberDiamond") { ?>
									เนื่องจากคุณเป็นสมาชิกระดับพิเศษ
									<br>
									ยินดีด้วยค่ะ นอกจากคุณจะสามารถพิมพ์เกียรติบัตรออนไลน์ได้เองแล้ว
									<br>
									คุณได้รับเกียรติบัตรแบบพิเศษ ทางมูลนิธิจะส่งไปให้ตามที่อยู่ที่ให้ไว้ตอนสมัครสมาชิกค่ะ
									<br>
									<a href="<?php echo 'images/เกียรติบัตร/พื้นฐาน/ศน/'.$date.'/'.$name.'.jpg'; ?>" target="_blank" class="btn2">แบบออนไลน์</a>
									<a href="images/เกียรติบัตร/พื้นฐาน/ครู/เกียรติบัตร ปี62.jpg" target="_blank" class="btn2">ตัวอย่างแบบพิเศษ ปี62</a>
					<?php }else{
									header("Location: javascript:history.go(-1);");
								} ?>
				</span>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>