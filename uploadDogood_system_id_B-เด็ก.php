<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($_SESSION['ID'])) {

			$basic_score4stu_system_id = 'B';
			$basic_score4stu_file_no = $_POST['basic_score4stu_file_no'];
			$basic_score4stu_text = $_POST['basic_score4stu_text'];

			$name = $_SESSION['firstname'].' '.$_SESSION['lastname'];
			
			$basic_score4stu_file_date = $_POST['basic_score4stu_file_date'].' '.$_POST['basic_score4stu_file_time'];
			$createDate = new DateTime($basic_score4stu_file_date);
			$date_do4savefile = $createDate->format('Y-m-d');

			$sqlbasic_score4stu = "SELECT * FROM `basic_score4stu` WHERE ID='$ID' AND `basic_score4stu_file_date` LIKE '%$date_do4savefile%' AND basic_score4stu_file_no!='$basic_score4stu_file_no' ";
			$reBS4S = mysqli_query($con,$sqlbasic_score4stu);
			$rowBS4S = mysqli_fetch_array($reBS4S);
			
			if($rowBS4S['basic_score4stu_id'] != 0) {
				echo '<script>';
					echo "alert('วันที่ทำความดี ไม่สามารถซ้ำกันได้');";
					echo "window.location.replace('แนบไฟล์ทำความดี-เด็ก.php')";
				echo '</script>';
			}else{

				$sqlbasic_score4stu_system_id_B = "SELECT * FROM `basic_score4stu` WHERE ID='$ID' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
				$reBS4SSB = mysqli_query($con, $sqlbasic_score4stu_system_id_B);

				/* Set maxbasic_score4stu_file_no */
				if(mysqli_num_rows($reBS4SSB) <= 14) {
					if(mysqli_num_rows($reBS4SSB) == 0 ) {
						$maxbasic_score4stu_file_no = 0;
					}else{
						$sqlmaxbasic_score4stu_file_no = "SELECT MAX(basic_score4stu_file_no) AS maxbasic_score4stu_file_no FROM `basic_score4stu` WHERE ID='$ID' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
						$remax = mysqli_query($con, $sqlmaxbasic_score4stu_file_no);
						$rowmax = $remax->fetch_assoc();
						$maxbasic_score4stu_file_no = $rowmax['maxbasic_score4stu_file_no'];
					}
				}

				if($maxbasic_score4stu_file_no <= 14) {
					$target_dir = 'images/ไฟล์เด็กขั้นพื้นฐาน/'.$name.'/'; // Set target_directory

					if(!is_dir($target_dir)) { // if there's not folder in target_directory
						mkdir($target_dir); // Create folder name is today_date
					}

					$target_file = $target_dir.$date_do4savefile.basename($_FILES["basic_score4stu_file"]["name"]); // Save image in the target folder

					if($maxbasic_score4stu_file_no >= $basic_score4stu_file_no) { // ถ้าค่า max ของ file_no มีค่ามากกว่าหรือเท่ากับ file_no ที่ user กด upload เข้ามา แสดงว่า มีไฟล์ no. นี้อยู่ใน DB อยู่แล้ว จึงให้ทำการ UPDATE

						if(move_uploaded_file($_FILES["basic_score4stu_file"]["tmp_name"], $target_file)) {

							$sqlbasic_score4stu_check_status = "SELECT basic_score4stu_check_status FROM `basic_score4stu` WHERE ID='$ID' AND basic_score4stu_system_id='$basic_score4stu_system_id' AND basic_score4stu_file_no='$basic_score4stu_file_no' ";
							$reBS4SCS = mysqli_query($con, $sqlbasic_score4stu_check_status);
							$rowBS4SCS = mysqli_fetch_array($reBS4SCS);

							$basic_score4stu_check_status = $rowBS4SCS['basic_score4stu_check_status'];

							if($basic_score4stu_check_status == NULL || is_null($basic_score4stu_check_status) || $basic_score4stu_check_status == '' || empty($basic_score4stu_check_status)) {

								$sql = "UPDATE `basic_score4stu` SET basic_score4stu_file='$target_file', basic_score4stu_file_date='$basic_score4stu_file_date' 
								WHERE ID='$ID' AND basic_score4stu_system_id='$basic_score4stu_system_id' AND basic_score4stu_file_no='$basic_score4stu_file_no' ";
								$re = $con->query($sql) or die($con->error); //Check error

								/* Log User Action */
								$scorelog_task = 'อัพเดทไฟล์,'.$basic_score4stu_system_id.$basic_score4stu_file_no;
								$scorelog_detail = 'เด็ก,ขั้นพื้นฐาน,'.$target_file.','.$basic_score4stu_text;
								$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', '$scorelog_task', '$scorelog_detail') ";
								$re = $con->query($sql) or die($con->error); //Check error

								echo '<script>';
									echo "alert('อัพเดทไฟล์แล้วค่ะ');";
									echo "window.location.replace('แนบไฟล์ทำความดี-เด็ก.php')";
								echo '</script>';

							}else{

								/* Set $basic_score4stu_check_status */
								if(strpos($basic_score4stu_check_status, 'ปฏิเสธ,') !== false) {
									$basic_score4stu_check_status = 'แก้ไขไฟล์,'.date('d-m-Y H:i:s');
								}elseif(strpos($basic_score4stu_check_status, 'แก้ไขไฟล์,') !== false || strpos($basic_score4stu_check_status, 'แก้ไขรายงาน,') !== false) {

									$file_index = 'แก้ไขไฟล์,'.date('d-m-Y H:i:s');

									if(strpos($basic_score4stu_check_status, 'แก้ไขรายงาน,') !== false) {
										$text_index = ','.substr($basic_score4stu_check_status, strpos($basic_score4stu_check_status, 'แก้ไขรายงาน,'), 53);
									}else{
										$text_index = '';
									}
									
									$basic_score4stu_check_status = $file_index.$text_index;
								}

								$sql = "UPDATE `basic_score4stu` SET basic_score4stu_file='$target_file', basic_score4stu_file_date='$basic_score4stu_file_date', basic_score4stu_check_status='$basic_score4stu_check_status' 
								WHERE ID='$ID' AND basic_score4stu_system_id='$basic_score4stu_system_id' AND basic_score4stu_file_no='$basic_score4stu_file_no' ";
								$re = $con->query($sql) or die($con->error); //Check error

								/* Log User Action */
								$scorelog_task = 'แก้ไขไฟล์,'.$basic_score4stu_system_id.$basic_score4stu_file_no;
								$scorelog_detail = 'เด็ก,ขั้นพื้นฐาน,'.$target_file.','.$basic_score4stu_text;
								$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', '$scorelog_task', '$scorelog_detail') ";
								$re = $con->query($sql) or die($con->error); //Check error

								echo '<script>';
									echo "alert('แก้ไขไฟล์แล้วค่ะ');";
									echo "window.location.replace('แนบไฟล์ทำความดี-เด็ก.php')";
								echo '</script>';

							}

							
						}else{
							echo '<script>';
								echo "alert('Something went wrong. Uploaded file wasn't succeeded.<br>Please contact developer  to take care of');";
								echo "<script>window.history.go(-1)</script>";
							echo '</script>';
						}
						
					}elseif($maxbasic_score4stu_file_no < $basic_score4stu_file_no) { // ถ้าค่า max ของ file_no มีค่าน้อยกว่า file_no ที่ user กด upload เข้ามา แสดงว่า เป็นครั้งแรกที่ upload ไฟล์ no. นี้ ให้ทำการ INSERT

						if(move_uploaded_file($_FILES["basic_score4stu_file"]["tmp_name"], $target_file)) {
							$sql = "INSERT INTO `basic_score4stu` (ID, basic_score4stu_system_id, basic_score4stu_file_no, basic_score4stu_file, basic_score4stu_file_date, basic_score4stu_text) 
							VALUES ('$ID', '$basic_score4stu_system_id', '$basic_score4stu_file_no', '$target_file', '$basic_score4stu_file_date', '$basic_score4stu_text') ";
							$re = $con->query($sql) or die($con->error); //Check error

							/* Log User Action */
							$scorelog_task = 'แนบไฟล์,'.$basic_score4stu_system_id.$basic_score4stu_file_no;
							$scorelog_detail = 'เด็ก,ขั้นพื้นฐาน,'.$target_file.','.$basic_score4stu_text;
							$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', '$scorelog_task', '$scorelog_detail') ";
							$re = $con->query($sql) or die($con->error); //Check error

							echo '<script>';
								echo "alert('แนบไฟล์แล้วค่ะ');";
								echo "window.location.replace('แนบไฟล์ทำความดี-เด็ก.php')";
							echo '</script>';
							
						}else{
							echo '<script>';
								echo "alert('Something went wrong. Uploaded file wasn't succeeded.<br>Please contact developer  to take care of');";
								echo "<script>window.history.go(-1)</script>";
							echo '</script>';
						}
						
					}else{
						echo '<script>';
							echo "alert('Something went wrong. error01.<br>Please contact developer  to take care of this issue');";
							echo "window.location.replace('แนบไฟล์ทำความดี-เด็ก.php')";
						echo '</script>';
					}
				}else{
					echo '<script>';
						echo "alert('Something went wrong. file upload number is exceed.<br>Please contact developer  to take care of this issue');";
						echo "window.location.replace('แนบไฟล์ทำความดี-เด็ก.php')";
					echo '</script>';
				}

			}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
	
?>