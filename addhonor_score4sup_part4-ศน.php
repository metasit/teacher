<?php

	session_start();
	require_once('condb.php');

	date_default_timezone_set("Asia/Bangkok");
	error_reporting(~E_NOTICE);
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if(isset($_POST['honor_score4sup_part4_1'])) {

			$sqlhonor_score4sup_codeD = "SELECT honor_score4sup_data4 FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code='D' ";
			$reD = mysqli_query($con, $sqlhonor_score4sup_codeD);
			$rowD = mysqli_fetch_array($reD);

			$honor_score4sup_code = 'D';
			$honor_score4sup_data1 = $_POST['honor_score4sup_part4_1'];
			$honor_score4sup_data2 = $_POST['honor_score4sup_part4_2'];
			$honor_score4sup_data3 = $_POST['honor_score4sup_part4_3'];

			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			$date2day = date('Y-m-d');
			$target_dir = 'images/honor_score4sup/codeD/'.$date2day;

			if(!is_dir($target_dir)) { // if there's not folder in target_directory
				mkdir($target_dir); // Create folder name is today_date
			}


			/* 4. การนำความรู้ไปใช้ในการปฏิบัติงานอย่างจริงจังนั้น คืออะไร จงบรรยายพร้อมภาพประกอบไม่เกิน 2 หน้า A4 ($honor_score4sup_data4) */
			if($_FILES["honor_score4sup_part4_4"]["size"] ==  0 && $rowD['honor_score4sup_data4'] != '') {
				$honor_score4sup_data4 = $rowD['honor_score4sup_data4'];
			}else{
				$target_file = $target_dir.'/ID'.$ID.' '.basename($_FILES["honor_score4sup_part4_4"]["name"]); // Save image in the target folder

				if(move_uploaded_file($_FILES["honor_score4sup_part4_4"]["tmp_name"], $target_file)) {
					$honor_score4sup_data4 = $target_file;
				}else{
					echo '<script>';
						echo "alert('เกิดข้อผิดพลาดในการแนบไฟล์ข้อ4 กรุณาติดต่อมูลนิธิ // Error01: addhonor_score4sup_part4-ศน.php');";
						echo "window.location.replace('โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php')";
					echo '</script>';
				}
			}

			/* 5. เอกสารหรือหลักฐาน เช่น ภาพ วีดีทัศน์ หรือบทสัมภาษณ์ผู้เกี่ยวข้องที่ แสดงให้เห็นถึงการเปลี่ยนแปลงอย่างชัดเจน ($honor_score4sup_data5) */
			if($_FILES["honor_score4sup_part4_5"]["size"] ==  0 && $rowD['honor_score4sup_data5'] != '') {
				$honor_score4sup_data5 = $rowD['honor_score4sup_data5'];
			}else{
				$target_file = $target_dir.'/ID'.$ID.' '.basename($_FILES["honor_score4sup_part4_5"]["name"]); // Save image in the target folder

				if(move_uploaded_file($_FILES["honor_score4sup_part4_5"]["tmp_name"], $target_file)) {
					$honor_score4sup_data5 = $target_file;
				}else{
					echo '<script>';
						echo "alert('เกิดข้อผิดพลาดในการแนบไฟล์ข้อ5 กรุณาติดต่อมูลนิธิ // Error02: addhonor_score4sup_part4-ศน.php');";
						echo "window.location.replace('โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php')";
					echo '</script>';
				}
			}
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			if(mysqli_num_rows($reD) == 0) {
				$sql = "INSERT INTO `honor_score4sup` (ID, honor_score4sup_code, honor_score4sup_data1, honor_score4sup_data2, honor_score4sup_data3, honor_score4sup_data4, honor_score4sup_data5) 
				VALUES ('$ID', '$honor_score4sup_code', '$honor_score4sup_data1', '$honor_score4sup_data2', '$honor_score4sup_data3', '$honor_score4sup_data4', '$honor_score4sup_data5') ";
				$re = $con->query($sql) or die($con->error); //Check error
			}else{
				$sql = "UPDATE `honor_score4sup` SET honor_score4sup_data1='$honor_score4sup_data1', honor_score4sup_data2='$honor_score4sup_data2', honor_score4sup_data3='$honor_score4sup_data3', honor_score4sup_data4='$honor_score4sup_data4', honor_score4sup_data5='$honor_score4sup_data5' 
				WHERE ID='$ID' AND honor_score4sup_code='$honor_score4sup_code' ";
				$re = $con->query($sql) or die($con->error); //Check error
			}

			/* Log User Action */
			$ID_user = $_SESSION['ID'];
			$scorelog_task = 'ส่งการเปลี่ยนแปลงเชิงประจักษ์'.$honor_score4sup_code;
			$scorelog_detail = 'ศน.,ขั้นเกียรติคุณ,'.$honor_score4sup_data1.','.$honor_score4sup_data2.','.$honor_score4sup_data3.','.$honor_score4sup_data4.','.$honor_score4sup_data5;

			$sqllog="INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID_user', '$scorelog_task', '$scorelog_detail') ";
			$relog = $con->query($sqllog) or die($con->error); //Check error

			header('location: โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php');

		}else{
			echo '<script>';
				echo "alert('เกิดข้อผิดพลาดในส่งข้อมูล ลองส่งการเปลี่ยนแปลงเชิงประจักษ์อีกครั้ง ถ้ายังคงพบปัญหาเดิม กรุณาติดต่อมูลนิธิ // Error03: addhonor_score4sup_part4-ศน.php');";
				echo "window.location.replace('login.php')";
			echo '</script>';
		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}

?>