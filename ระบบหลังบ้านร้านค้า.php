<?php 
	session_start();
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if(strpos($_SESSION['permis_system'], '*all*') !== false || strpos($_SESSION['permis_system'], '*D00*') !== false || strpos($_SESSION['permis_system'], '*D01*') !== false) {
			require_once('condb.php');
			$email = $_SESSION['email'];
		}else{
			header("Location: index.php");
		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['win_scroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="index-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Home.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="สนับสนุนของที่ระลึก.php">ร้านค้า</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ระบบหลังบ้านร้านค้า</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ระบบหลังบ้านร้านค้า -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row" style="margin:0 20px; overflow-x:auto;">
			<div class="col-lg-12">
				<div class="table-main table-responsive" style="margin-bottom:200px">
					<!-- ################################################################################################ -->
					<table class="table" style="background-color:rgb(240,240,240);">
						<thead>
							<tr>
								<th>ลำดับ</th>
								<th>ชื่อผู้รับ</th>
								<th>วันที่สั่งซื้อ</th>
								<th>เวลา</th>
								<th>ราคาสุทธิ</th>
								<th>สถานะ</th>
								<th>รายละเอียด<br>คำสั่งซื้อ</th>
								<th>รูปSlip</th>
								<th>อัพเดท</th>
								<th>รหัสขนส่ง</th>
								<th>Print</th>
								<th>หมายเหตุ</th>
							</tr>
						</thead>
						<tbody class="disable-margin">
							<?php
							$sqlmax = "SELECT DISTINCT MAX(order_group) AS max_order_group FROM orders ";
							$resultmax = mysqli_query($con,$sqlmax);
							$rowmax = mysqli_fetch_array($resultmax);
							$max = $rowmax['max_order_group'];

							if($max == NULL) { ?>
								</tbody>
							</table>
								<div class="no-order">
									<p>ยังไม่มีคำสั่งซื้อ</p>
								</div>
							<?php }else{
								/* Set number product per page */
								$results_per_page = 50;

								/* Set $page */
								if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };

								/* Set start number for each page */
								$start_from = ($page-1) * $results_per_page;

								/* Call orders only one row of order_group */
								$sql1 = "SELECT DISTINCT order_group FROM `orders` ORDER BY order_date DESC LIMIT $start_from, $results_per_page ";
								$result1 = $con->query($sql1);
								
								$i = $start_from+1; // Get number of row
								while($row1 = $result1->fetch_assoc()) {
									/* Call orders only the same value of order_group */
									$order_group = $row1['order_group'];
									$sql2 = "SELECT * FROM orders WHERE order_group='$order_group' ";
									$result2 = mysqli_query($con,$sql2);
									$row2 = mysqli_fetch_array($result2);

									/* Set shipping-cost value */
									
									if($row2['shipping_kind'] == 'normal') {
										$shipping_cost = '50';
									}elseif($row2['shipping_kind'] == 'ems') {
										$shipping_cost = '80';
									}elseif($row2['shipping_kind'] == 'Free') {
										$shipping_cost = 0;
									}else{
										echo 'error: others kind of shipping_kind, not normal or ems//'.$email.'//'.$order_group;
									}

									/* Set price-total value */
									$sql3 = "SELECT SUM(product_price_sale*product_amount) AS pricetotal FROM orders WHERE order_group='$order_group' ";
									$result3 = mysqli_query($con,$sql3);
									$row3 = mysqli_fetch_array($result3);
									$price_total = $row3['pricetotal'];
									/* Set grand_total value */
									$grand_total = number_format($price_total+$shipping_cost,2);

									/* Call login table for check level of user */
									$ID_user = $row2['ID'];
									$sql4 = "SELECT level FROM `login` WHERE ID='$ID_user' ";
									$re4 = mysqli_query($con, $sql4);
									$row4 = mysqli_fetch_array($re4); ?>

									<tr>
										<!-- Order Group -->
										<td class="price-pr">
											<p><?php echo $i; ?></p>
										</td>
										<!-- Buyer Name -->
										<?php
											$contact_firstname = substr(trim($row2["contact_name"]), 0, strpos(trim($row2["contact_name"]), '<br>'));
											$contact_lastname = substr(trim($row2["contact_name"]), strrpos(trim($row2["contact_name"]), '<br>')+4);
											$contact_name = $contact_firstname.'<br>'.$contact_lastname;
											if($row4)
											if(in_array("level", array_keys($row4)))
											
											if($row4['level'] == 'admin') { ?>
														<td class="name-pr lh-1-0" style="background-color:rgb(228,0,0);">
															<p style="color:white;"><?php echo $contact_name; ?></p>
														</td>
										<?php }elseif($row4['level'] == 'memberGeneral') { ?>
														<td class="name-pr lh-1-0" style="background-color:white;">
															<p><?php echo $contact_name; ?></p>
														</td>
										<?php }elseif($row4['level'] == 'memberSilver') { ?>
														<td class="name-pr lh-1-0" style="background-color:rgb(169,169,169);">
															<p style="color:white;"><?php echo $contact_name; ?></p>
														</td>
										<?php }elseif($row4['level'] == 'memberGold') { ?>
														<td class="name-pr lh-1-0" style="background-color:rgb(180,147,31);">
															<p style="color:white;"><?php echo $contact_name; ?></p>
														</td>
										<?php }elseif($row4['level'] == 'memberDiamond') { ?>
														<td class="name-pr lh-1-0" style="background-color:rgb(52,52,53);">
															<p style="color:white;"><?php echo $contact_name; ?></p>
														</td>
										<?php }else{ ?>
														<td class="name-pr lh-1-0">
															<p><?php echo $row2["email"]; ?></p>
														</td>
										<?php } ?>
										<!-- Date -->
										<td class="price-pr">
											<p><?php echo date("d-m-Y", strtotime($row2['order_date'])); ?></p>
										</td>
										<!-- Time -->
										<td class="price-pr">
											<p><?php echo date("H:i", strtotime($row2['order_date'])); ?></p>
										</td>
										<!-- Grand Total -->
										<td class="price-pr">
											<p><?php echo $grand_total; ?></p>
										</td>
										<!-- Status -->
										<td class="name-pr">
											<?php if($row2['order_status'] == 'สินค้าถึงปลายทางแล้ว') { ?>
															<p><i class="fas fa-check-circle"></i><br><?php echo $row2['order_status']; ?></p>
											<?php }else{ ?>
															<p><?php echo $row2['order_status']; ?></p>
											<?php } ?>
										</td>
										<!-- Order Detail -->
										<td class="print-btn">
											<a href="รายละเอียดคำสั่งซื้อ-admin.php?order_group=<?php echo $row2['order_group'];?>" target="_blank" class="tooltip3"><i class="fas fa-clipboard-list"><span class="tooltiptext3">ดูรายละเอียดคำสั่งซื้อ</span></i></a>
										</td>
										<!-- Slip Upload -->
										<td class="slip-upload-btn">
											<?php if(is_null($row2['slip_image']) || empty($row2['slip_image'])) { ?>
															<a href="#" style="background-color:transparent; color:black; font-weight:700; border:none;" onclick="return false">รอหลักฐานการโอน</a>
											<?php }else{ ?>
															<a href="<?php echo $row2['slip_image'];?>" target="_blank">ดูหลักฐานการโอน</a>
											<?php	} ?>
										</td>
										<!-- Update Status Button -->
										<td>
											<div class="up-stat-sty">
												<button class="up-stat-btn"><i class="fas fa-caret-down"></i></button>
												<div class="dropdown">
													<?php if($row2['order_status'] == 'กำลังตรวจสอบ') { ?>
																	<a onclick="getWinScroll_changeStatusBtn('กำลังจัดส่ง','<?php echo $row2['order_group'];?>','<?php echo $row2['slip_image'];?>')">หลักฐานถูกต้อง</a>
																	<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">จัดส่งแล้ว</a>
																	<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">สินค้าถึงปลายทางแล้ว</a>
													<?php	}elseif($row2['order_status'] == 'กำลังจัดส่ง') { ?>
																	<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">หลักฐานถูกต้อง</a>
																	<a onclick="getWinScroll_changeStatusBtn('จัดส่งแล้ว',<?php echo $row2['order_group'];?>,'<?php echo $row2['slip_image'];?>')">จัดส่งแล้ว</a>
																	<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">สินค้าถึงปลายทางแล้ว</a>
													<?php }elseif($row2['order_status'] == 'จัดส่งแล้ว') { ?>
																	<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">หลักฐานถูกต้อง</a>
																	<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">จัดส่งแล้ว</a>
																	<a onclick="getWinScroll_changeStatusBtn('สินค้าถึงปลายทางแล้ว',<?php echo $row2['order_group'];?>,'<?php echo $row2['slip_image'];?>')">สินค้าถึงปลายทางแล้ว</a>
													<?php }elseif($row2['order_status'] == 'สินค้าถึงปลายทางแล้ว') { ?>
																	<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">หลักฐานถูกต้อง</a>
																	<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">จัดส่งแล้ว</a>
																	<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">สินค้าถึงปลายทางแล้ว</a>
													<?php	}else{ ?>
																	<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">รอแนบSlip</a>
													<?php	} ?>
												</div>
											</div>
										</td>
										<!-- Shipping Tracking Code -->
										<form action="addshipping_code.php" method="POST">
											<td class="order_remark-sty">
												<textarea rows="1" cols="16" maxlength="13" minlength="13" name="shipping_code"><?php echo $row2['shipping_code']; ?></textarea>
												<div class="order-detail-btn">
													<input type="hidden" name="order_group" value=<?php echo $row2['order_group']; ?>>
													<input class="order_remark-submit" type="submit" value="บันทึก">
												</div>
											</td>
										</form>
										<!-- Printing -->
										<td class="print-btn">
											<a href="ใบเสร็จC.php?order_group=<?php echo $row2['order_group'];?>" target="_blank" class="tooltip1"><i class="fas fa-print"><span class="tooltiptext1">พิมพ์ใบเสร็จ</span></i></a>
											<a href="ใบปะหน้า.php?order_group=<?php echo $row2['order_group'];?>" target="_blank" class="tooltip2"><i class="fas fa-money-check"><span class="tooltiptext2">พิมพ์ใบปะหน้า</span></i></a>
										</td>
										<!-- Remark -->
										<form action="addorder_remark.php" method="POST">
											<td class="order_remark-sty">
												<textarea rows="1" cols="15" name="order_remark"><?php echo $row2['order_remark']; ?></textarea>
												<div class="order-detail-btn">
													<input type="hidden" name="order_group" value=<?php echo $row2['order_group']; ?>>
													<input class="order_remark-submit" type="submit" value="บันทึก">
												</div>
											</td>
										</form>
									</tr> <?php
									$i++;
								} ?>
								</tbody>
								</table> <?php
							}
						?>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ระบบหลังบ้านร้านค้า -->
<?php
	/* Calculate total pages with results */
	$sqlcount = "SELECT DISTINCT COUNT(order_group) AS total FROM `orders` WHERE email!='$email' ";
	$resultcount = $con->query($sqlcount);
	$rowcount = $resultcount->fetch_assoc();

	$total_pages = ceil($rowcount["total"] / $results_per_page);
?>
<div class="wrapper row3 coloured" style="background-color:rgb(226,255,224);">
	<main class="hoc container clear">
		<div class="content">
			<nav class="pagination">
				<ul>
					<?php
						for ($j=1; $j<=$total_pages; $j++) {  // print links for all pages
							if ($j==$page)
							{
								echo "<li class='current'>";
							}else{
								echo "<li>";
							}
							echo "<a href='ระบบหลังบ้านร้านค้า.php?page=".$j."'";
							if ($j==$page)  echo " class='curPage'";
							echo ">".$j."</a> ";
						};
					?>
					</li>

				</ul>
			</nav>
		</div>
	</main>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<!-- JS for getWinScroll -->
<script src="js/getWinScroll_changeStatusBtn.js"></script>

</body>
</html>