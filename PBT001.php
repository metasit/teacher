<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
	$sql_list_project  ="SELECT * FROM join_project";
    $list_project = $con->query($sql_list_project);
?>

<!DOCTYPE html>

<html lang="thai">
<!-- To declare your language - read more here: https://www.w3.org/International/questions/qa-html-language-declarations -->

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
        <?php if (strpos($_SESSION['email'], '@') !== false) { //Check Account is signed in
                if ($_SESSION["level"]=="admin") { //Admin Session ?>
								<li style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
									<ul>
										<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION["level"]=="memberGeneral") { //General Member Session ?>
                <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> สมาชิกทั่วไป</li>
          <?php }elseif ($_SESSION["level"]=="memberSilver") { //Member Silver Session ?>
                <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</li>
          <?php }elseif ($_SESSION["level"]=="memberGold") { //Member Gold Session ?>
                <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</li>
          <?php }elseif ($_SESSION["level"]=="memberDiamond") { //Member Diamond Session ?>
                <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</li>
          <?php } ?>
                <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
          <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
                <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
        <?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
                  <ul>
                    <li><a href="index-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
                    <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึก</a></li>
                    
                  </ul>
                </li>
        </ul>
      </nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li class="active"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>

              <?php 
                while($row = $list_project->fetch_assoc()){
                  $id_join_project = $row['id_join_project'];
                  $title_menu = $row['title_menu']; 
                
              ?>
              <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
              <?php } ?>

              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
							<li class="active"><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา<i class="fas fa-caret-right" style="margin-left:90px"></i></a>
								<ul style="position:absolute; left:305px;">
									<li class="active"><a href="PublicTraining.php">Public Training</a></li>
									<li><a href="InhouseTraining.php">Inhouse Training</a></li>
								</ul>
							</li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="all_project.php">ร่วมโครงการ</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li class="linkst"><a href="โครงการอบรมสัมมนา.php">โครงการอบรมสัมมนา</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li class="linkst"><a href="PublicTraining.php">Public Training</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ศาสตร์แห่งพระราชาเพื่อการพัฒนาองค์การ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - PBT001 -->
<div class="wrapper row3">
  <main class="hoc container clear">
		<!-- Start Content -->
    <article style="text-align:center; border-bottom:3px solid #59A209; padding-bottom:50px;">
			<p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px;"><strong>ศาสตร์แห่งพระราชาเพื่อการพัฒนาองค์การ</strong></p>
			<img src="images/Public Training/PBT001/PBT001img06.jpg" alt="PBT001img06">
			<br><br>
			<img src="images/Public Training/PBT001/PBT001img10.jpg" alt="PBT001img10">
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>ชื่อหลักสูตร</strong> : ศาสตร์แห่งพระราชาเพื่อการพัฒนาองค์การ
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>ความเป็นมาและความสําคัญ</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;หลักการทรงงานของพระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช ที่ทรงยึดหลักผลประโยชน์ของปวงชนและการพัฒนาคนเป็นสำคัญ เป็นกระบวนการทำงานแบบบูรณาการและกระบวนการคิดบนรากฐานของ 
				“การเข้าใจมนุษย์” ในการสร้างสรรค์นวัตกรรมจากความเข้าใจ ศึกษาผู้คนด้วยการสังเกต และใช้เป็นแนวทางในการแก้ไขปัญหา ซึ่งคือหลักการ <strong>“เข้าใจ เข้าถึง พัฒนา”</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เพื่อเป็นการสืบสานพระราชปณิธานและน้อมเกล้าน้อมกระหม่อมรำลึกในพระมหากรุณาธิคุณ ของพระองค์ จึงมีการนำหลักการทรงงานมาศึกษาอย่างลึกซึ้ง แล้วนำมาพัฒนาปรับใช้กับตนเองและองค์การ 
				เกิดการเรียนรู้กระบวนการคิดบนรากฐานของการเข้าใจมนุษย์ การเข้าถึงข้อมูลเพื่อให้การสร้างสรรค์นั้นตอบสนองความต้องการ การพัฒนาด้วยความรู้และภูมิปัญญาที่ไม่จำกัด ตลอดจนการทดลองและปรับปรุงจน
				ได้ผลลัพธ์ที่ยั่งยืนและสามารถประยุกต์ใช้ได้กับงานต่างๆ 
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>วัตถุประสงค์</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1.	เพื่อสร้างความเข้าใจหลักศาสตร์แห่งพระราชาให้ลึกซึ้ง
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2.	เพื่อให้ผู้เข้าอบรมนำหลักศาสตร์แห่งพระราชาไปประยุกต์ใช้ในการดําเนินชีวิตของตนได้อย่างเหมาะสม
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3.	เพื่อให้ผู้เข้าอบรมนำหลักศาสตร์แห่งพระราชาไปประยุกต์ใช้ในการปรับปรุงพัฒนาองค์การให้มีประสิทธิภาพและเกิดประสิทธิผลอย่างยั่งยืน
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>กลุ่มเป้าหมาย</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1.	บุคคลทั่วไปที่สนใจน้อมนำศาสตร์แห่งพระราชามาปรับใช้พัฒนาตนเอง
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2.	องค์การต่างๆ ทั้งภาครัฐและเอกชนที่สนใจน้อมนำศาสตร์แห่งพระราชามาปรับใช้ในการพัฒนาองค์การ
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3.	ผู้บริหารและครูที่สนใจน้อมนำศาสตร์แห่งพระราชามาปรับใช้ในการพัฒนาตนเองและสถานศึกษา
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>เนื้อหาสาระของหลักสูตร</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1.	การดำรงตนตามแนวทางคุณธรรม จริยธรรม และธรรมาภิบาล
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2.	ศาสตร์แห่งพระราชา
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3.	แนวทางการพัฒนาตามศาสตร์พระราชา “เข้าใจ เข้าถึง พัฒนา”
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4.	หลักปรัชญาเศรษฐกิจพอเพียงและการประยุกต์ใช้ในชีวิตประจำวัน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;5.	การประยุกต์ใช้ศาสตร์แห่งพระราชากับการพัฒนาองค์การที่เป็นสากล
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>วิธีการอบรม</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1.	การบรรยาย
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2.	การประชุมเชิงปฏิบัติการ
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3.	การประชุมกลุ่มย่อย
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;4.	ศึกษาดูงาน
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>ตารางอบรม</strong>
			</p>
			<div class="table-main table-responsive">
				<table class="table" style="background-color:rgb(240,240,240);">
					<thead>
						<tr>
							<th>วันที่</th>
							<th>09.00-12.00</th>
							<th>13.30-16.30</th>
							<th>18.00-20.00</th>
						</tr>
					</thead>
					<tbody style="line-height:50px">
						<tr style="cursor:context-menu" onMouseOver="this.style.backgroundColor='rgba(72,160,0,0.5)'" onMouseOut="this.style.backgroundColor='rgb(240,240,240)'">
							<td>วันที่1</td>
							<td>พิธีเปิด<br>การดำรงตนตามแนวทางคุณธรรม จริยธรรม และธรรมาภิบาล</td>
							<td>ศาสตร์แห่งพระราชา</td>
							<td>ถอดบทเรียน</td>
						</tr>
						<tr style="cursor:context-menu" onMouseOver="this.style.backgroundColor='rgba(72,160,0,0.5)'" onMouseOut="this.style.backgroundColor='rgb(240,240,240)'">
							<td>วันที่2</td>
							<td>แนวทางการพัฒนาตามศาสตร์พระราชา “เข้าใจ เข้าถึง พัฒนา”</td>
							<td>หลักปรัชญาเศรษฐกิจพอเพียงและการประยุกต์ใช้ในชีวิตประจำวัน</td>
							<td>ถอดบทเรียน</td>
						</tr>
						<tr style="cursor:context-menu" onMouseOver="this.style.backgroundColor='rgba(72,160,0,0.5)'" onMouseOut="this.style.backgroundColor='rgb(240,240,240)'">
							<td>วันที่3</td>
							<td>ศึกษาดูงาน</td>
							<td>ศึกษาดูงาน</td>
							<td>พักผ่อนตามอัธยาศัย</td>
						</tr>
						<tr style="cursor:context-menu" onMouseOver="this.style.backgroundColor='rgba(72,160,0,0.5)'" onMouseOut="this.style.backgroundColor='rgb(240,240,240)'">
							<td>วันที่4</td>
							<td>การประยุกต์ใช้ศาสตร์แห่งพระราชากับการพัฒนาองค์การที่เป็นสากล</td>
							<td>สรุปบทเรียน</td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
			<strong>หมายเหตุ:</strong> กําหนดการอาจเปลี่ยนแปลงได้ตามความเหมาะสม
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>ตัวชี้วัดความสําเร็จ</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1.	ผู้เข้าอบรมอย่างน้อยร้อยละ 80 มีความเข้าใจหลักศาสตร์แห่งพระราชาและสามารถถ่ายทอดได้อย่างชัดเจน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2.	ผู้เข้าอบรมอย่างน้อยร้อยละ 80 นำเสนอวิธีการดําเนินชีวิตตามศาสตร์แห่งพระราชา หลักปรัชญาเศรษฐกิจพอเพียง หลักธรรมาภิบาล และคุณธรรมจริยธรรมตามหลักศาสนาของตนได้
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;3.	ผู้เข้าอบรมอย่างน้อยร้อยละ 80 นำเสนอวิธีการนำศาสตร์แห่งพระราชา หลักปรัชญาเศรษฐกิจพอเพียงมาปรับปรุงพัฒนาองค์การของตนเองได้
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>การติดตามผล</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้เข้าอบรมรายงานผลการนำศาสตร์แห่งพระราชาไปประยุกต์ใช้กับตนเองและองค์การอย่างน้อยเดือนละ 1 ครั้ง ภายในระยะเวลา 6 เดือน ผ่านระบบเฟซบุ๊คและไลน์กลุ่ม
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>ผลที่คาดว่าจะได้รับ</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้เข้าอบรมมีความเข้าใจศาสตร์แห่งพระราชาอย่างชัดเจน มีการดำรงตนอยู่ในศีลธรรมและหลักธรรมาภิบาล สามารถนำความรู้ที่ได้รับมาประยุกต์ใช้กับตนเองและปรับปรุงพัฒนาการทำงานขององค์การให้มีประสิทธิภาพมากยิ่งขึ้น
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>ผลกระทบที่เกิดขึ้น</strong>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;1.	องค์การหรือบุคคลรอบข้างของผู้เข้าอบรมได้แบบอย่างที่ดีของการปฏิบัติตนตามแนวทางศาสตร์พระราชา
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;2.	องค์การมีการพัฒนาอย่างเป็นรูปแบบ ที่เกิดจากความเข้าใจของบุคลากรในองค์การ มีการเรียนรู้และพัฒนาร่วมกัน สามารถพึ่งพาตนเองได้ และเป็นการพัฒนาที่ยั่งยืน
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>จำนวนที่สามารถรองรับได้</strong>&nbsp;&nbsp;&nbsp;&nbsp;60 – 100 คน
			</p>
			<p class="fs-30" style="font-family:RSUText; line-height:30pt; text-align:left;">
				<strong>ค่าลงทะเบียน</strong>&nbsp;&nbsp;&nbsp;&nbsp;ท่านละ 6,720 บาท/คน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;-	ที่พัก 3 คืน  อาหาร 7 มื้อ อาหารว่าง 8 มื้อ
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;-	ค่าเดินทางศึกษาดูงาน
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;-	เอกสารและอุปกรณ์ประกอบการอบรมตลอดการอบรม และเกียรติบัตร
			</p>
			<ul class="fs-32 textlink" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
				<li><a href="docs/ร่วมโครงการ/อบรมสัมมนา/Public Training/หลักสูตรศาสตร์แห่งพระราชาเพื่อการพัฒนาองค์กร.pdf">ดาวน์โหลดรายละเอียดหลักสูตร</a></li>
			</ul>
		</article>
		<!-- End Content -->
		<!-- ################################################################################################ -->
		<!-- Start Gallery -->
    <p class="font-x3"><span style="color:rgb(180,147,31); line-height:30px;"><strong>คลังภาพ</strong></span></p>
    <div id="gallery" style="margin-top:50px">
      <ul class="nospace clear">
        <li class="one_third first zoom11"><a href="images/Public Training/PBT001/PBT001img02.jpg"><img src="images/Public Training/PBT001/PBT001img02.jpg" alt="PBT001img02"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT001/PBT001img03.jpg"><img src="images/Public Training/PBT001/PBT001img03.jpg" alt="PBT001img03"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT001/PBT001img04.jpg"><img src="images/Public Training/PBT001/PBT001img04.jpg" alt="PBT001img04"></a></li>
			</ul>
			<ul class="nospace clear">
        <li class="one_third first zoom11"><a href="images/Public Training/PBT001/PBT001img05.jpg"><img src="images/Public Training/PBT001/PBT001img05.jpg" alt="PBT001img05"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT001/PBT001img07.jpg"><img src="images/Public Training/PBT001/PBT001img07.jpg" alt="PBT001img07"></a></li>
        <li class="one_third zoom11"><a href="images/Public Training/PBT001/PBT001img08.jpg"><img src="images/Public Training/PBT001/PBT001img08.jpg" alt="PBT001img08"></a></li>
			</ul>
			<ul class="nospace clear">
        <li class="one_third first zoom11"><a href="images/Public Training/PBT001/PBT001img09.jpg"><img src="images/Public Training/PBT001/PBT001img09.jpg" alt="PBT001img09"></a></li>
      </ul>
		</div>
		<!-- End Gallery -->
  </main>
</div>
<!-- End Content 00 - PBT001 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>