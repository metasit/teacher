<?php
	session_start();
	require_once('condb.php');
	if(!isset($_SESSION['email'])) {
		header("Location: javascript:history.go(-1);");
	}

	$email = $_SESSION['email'];
	$sqllogin = "SELECT * FROM `login` WHERE email='$email' ";
	$relogin = mysqli_query($con,$sqllogin);
	$rowlogin = mysqli_fetch_array($relogin);

	$province =  explode(" ",$rowlogin['docaddress'])[2] ;
	$district = explode(" ",$rowlogin['docaddress'])[1];
	$district_sub = explode(" ",$rowlogin['docaddress'])[0];
	$zip_code = explode(" ",$rowlogin['docaddress'])[3];
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="index.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าหลัก</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" name="formeditprofile" action="saveeditprofile.php" method="POST">
					<!-- ################################################################################################ -->
					<span class="login100-form-title">
						<strong>ข้อมูลของฉัน</strong><br>จัดการข้อมูลส่วนตัวคุณเพื่อความปลอดภัยของบัญชีผู้ใช้นี้<br>ใส่ข้อมูลใหม่แทนที่ข้อมูลเก่าและกดบันทึก เพื่อเปลี่ยนแปลงข้อมูลส่วนตัว
					</span>
					<!-- Email -->
					<div class="wrap-input100">
						<div class="input100 text-black BG-white" style="line-height:45px"><?php echo $rowlogin['email'];  ?> (email)</div>
						<span class="symbol-input100">
							<i class="fas fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100">
						<div class="input100 text-black BG-white" style="line-height:45px"><?php echo $rowlogin['code_persanal']; ?> (เลขบัตรประชาชน)    </div>
						<span class="symbol-input100">
							<i class="fas fa-user" aria-hidden="true"></i>
						</span>
					</div>
					<!-- Password -->
					<!-- <div class="wrap-input100 validate-input">
						<input class="input100 text-black" type="password" name="password" value="<?php echo $rowlogin['password']; ?>   " pattern=".{8,}" title="โปรดใส่จำนวนรหัสผ่านเท่ากับหรือมากกว่า 8 ตัว" />
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div> -->
					<!-- Firstname -->
					<div class="wrap-input100">
						<!--
						<input class="input100 text-black" type="text" name="firstname" value="<?php echo $rowlogin['firstname']; ?>" max-lenght="100"/>
						-->
						<div class="input100 text-black BG-white" style="line-height:45px"><?php echo $rowlogin['firstname']; ?> (ชื่อ) </div>
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- Lastname -->
					<div class="wrap-input100">
						<!--
						<input class="input100 text-black" type="text" name="lastname" value=" max-lenght="100"/>
						-->
						<div class="input100 text-black BG-white" style="line-height:45px"><?php echo $rowlogin['lastname']; ?> (นามสกุล)</div>
						<span class="symbol-input100">
							<i class="fas fa-file-signature" aria-hidden="true"></i>
						</span>
					</div>
					<!-- Phone number -->
					<div class="wrap-input100 validate-input">
						<!-- <input 
						placeholder = "เบอร์โทรศัพย์"
						class="input100 text-black" 
						type="text" 
						name="phonenum" 
						value="<?php echo $rowlogin['phonenum']; ?>" 
						maxlength="10" pattern="[0][0-9]{9}"
						title="โปรดใส่จำนวนตัวเลขให้ครบ 10 ตัว (รวมเลข 0 ตัวแรก)"  
						require /> -->

						<input 
					id="tel_ck"
					class="input100" 
					value="<?php echo $rowlogin['phonenum']; ?>"
					type="text" name="phonenum" class="form-control" 
					placeholder="เบอร์โทรศัพท์" pattern="[0][0-9]{9}" maxlength="10"
					title="โปรดใส่จำนวนตัวเลขให้ครบ 10 ตัว (รวมเลข 0 ตัวแรก)" required />


						<span class="symbol-input100">
							<i class="fas fa-mobile-alt" aria-hidden="true"></i>
						</span>
						<em>
					</div>

					<div class="wrap-input100 validate-input">
						
					<select name="position" class="form-control" style="margin-bottom:10px; height:40px" onchange="PickPrename(this.value);" required>
					<option   value="" disabled="disabled" selected="selected">ตำแหน่ง</option>
					<option  <?php  if($rowlogin['occup_code'] == "OcAAAA") echo "selected";   ?> value="OcAAAA">ครูอัตราจ้าง</option>
					<option  <?php  if($rowlogin['occup_code'] == "OcAAAB") echo "selected";   ?>  value="OcAAAB">ครูผู้ช่วย</option>
					<option  <?php  if($rowlogin['occup_code'] == "OcAAAD") echo "selected";   ?>  value="OcAAAD">ครูชำนาญการ</option>
					<option  <?php  if($rowlogin['occup_code'] == "OcAAAE") echo "selected";   ?>  value="OcAAAE">ครูชำนาญการพิเศษ</option>
					<option  <?php  if($rowlogin['occup_code'] == "OcAAAG") echo "selected";   ?>  value="OcAAAG">ครูเชี่ยวชาญพิเศษ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcAABB") echo "selected";   ?>  value="OcAABB">ครู กศน.ตำบล</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcAABC") echo "selected";   ?>  value="OcAABC">ครู ศูนย์การเรียนรู้ชุมชน</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcAABD") echo "selected";   ?>  value="OcAABD">ครู อาสาสมัคร</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcAABE") echo "selected";   ?>  value="OcAABE">ครู สอนคนพิการ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcAABF") echo "selected";   ?>  value="OcAABF">ครู ประกาศนียบัตรวิชาชีพ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcAABO") echo "selected";   ?>  value="OcAABO">อื่น ๆ ระบุ....</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcAADA") echo "selected";   ?>  value="OcAADA">อาจารย์</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcAADB") echo "selected";   ?>  value="OcAADB">ผู้ช่วยศาสตราจารย์</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcAADC") echo "selected";   ?>  value="OcAADC">รองศาสตราจารย์</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcAADD") echo "selected";   ?>  value="OcAADD">ศาสตราจารย์</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcABAA") echo "selected";   ?>  value="OcABAA">รองผู้อำนวยการโรงเรียนเอกชน</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcABAB") echo "selected";   ?>  value="OcABAB">รองผู้อำนวยการชำนาญการ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcABAC") echo "selected";   ?>  value="OcABAC">รองผู้อำนวยการชำนาญการพิเศษ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcABAD") echo "selected";   ?>  value="OcABAD">รองผู้อำนวยการเชี่ยวชาญ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcABBA") echo "selected";   ?>  value="OcABBA">ผู้อำนวยการโรงเรียนเอกชน</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcABBB") echo "selected";   ?>  value="OcABBB">ผู้อำนวยการชำนาญการ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcABBC") echo "selected";   ?>  value="OcABBC">ผู้อำนวยการชำนาญการพิเศษ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcABBD") echo "selected";   ?>  value="OcABBD">ผู้อำนวยการเชี่ยวชาญ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcABBE") echo "selected";   ?>  value="OcABBE">ผู้อำนวยการเชี่ยวชาญพิเศษ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACAA") echo "selected";   ?>  value="OcACAA">รองผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาชำนาญการพิเศษ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACAB") echo "selected";   ?>  value="OcACAB">รองผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACAC") echo "selected";   ?>  value="OcACAC">ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACAD") echo "selected";   ?>  value="OcACAD">ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญพิเศษ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACBA") echo "selected";   ?>  value="OcACBA">รองศึกษาธิการจังหวัด</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACBB") echo "selected";   ?>  value="OcACBB">ศึกษาธิการจังหวัด</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACBC") echo "selected";   ?>  value="OcACBC">รองศึกษาธิการภาค</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACBD") echo "selected";   ?>  value="OcACBD">ศึกษาธิการภาค</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACCA") echo "selected";   ?>  value="OcACCA">รองผู้อำนวยการสำนัก</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACCA") echo "selected";   ?>  value="OcACCA">ผู้อำนวยการสำนัก</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACCA") echo "selected";   ?>  value="OcACCA">ผู้เชี่ยวชาญ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACCA") echo "selected";   ?>  value="OcACCA">ผู้ตรวจราชการ/ที่ปรึกษาระดับ 10</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACCA") echo "selected";   ?>  value="OcACCA">ปลัด/รองปลัด/อธิบดี/รองอธิบดี</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACDA") echo "selected";   ?>  value="OcACDA">รองคณบดี</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACDB") echo "selected";   ?>  value="OcACDB">คณบดี</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACDC") echo "selected";   ?>  value="OcACDC">รองอธิการบดี</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcACDD") echo "selected";   ?>  value="OcACDD">อธิการบดี</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcADAA") echo "selected";   ?>  value="OcADAA">ศึกษานิเทศก์ชำนาญการ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcADAB") echo "selected";   ?>  value="OcADAB">ศึกษานิเทศก์ชำนาญการพิเศษ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcADAC") echo "selected";   ?>  value="OcADAC">ศึกษานิเทศก์เชี่ยวชาญ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcADAD") echo "selected";   ?>  value="OcADAD">ศึกษานิเทศก์เชี่ยวชาญพิเศษ</option>
                    <option <?php  if($rowlogin['occup_code'] == "OcAO") echo "selected";   ?>  value="OcAO">อื่นๆ ระบุ </option>
				</select>
						<em>
					</div>


					
					<div class="mb-3">
						<select name="affiliation_id" id="affiliation_id" class="form-control" style="height:40px;" required>
							<option value="" disabled="disabled" selected="selected">เลือกสังกัด</option>
							<!--<option value="AfA">สำนักงานปลัดกระทรวงศึกษาธิการ</option>-->
							<option value="AfB">สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน</option>
							<option value="AfC">สำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน</option>
							<option value="AfD">สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย</option>
							<option value="AfE">สำนักงานคณะกรรมการการอาชีวศึกษา</option>
							<option value="AfF">สำนักงานคณะกรรมการการอุดมศึกษา</option>
							<option value="AfG">กระทรวงมหาดไทย กรมส่งเสริมการปกครองส่วนท้องถิ่น</option>
							<option value="AfH">กรุงเทพมหานคร</option>
							<option value="AfI">เมืองพัทยา</option>
							<option value="AfJ">สำนักงานตำรวจแห่งชาติ</option>
							<option value="AfO">อื่นๆ โปรดระบุ....</option>
						</select>
						<label for="affsub_id" id="affsubhead" class="center m-t-10" style="display:none">สังกัดย่อย</label>
						<select name="affsub_id" id="affsub_id" class="form-control" style="display:none; height:40px;">
							<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
						</select>
						<div id="affsub_text" style="display:none">
							<div class="wrap-input100 m-t-10">
								<input class="input100" name="affsub_ans_id" id="affsub_ans_id" type="text" class="form-control" placeholder="โปรดระบุสังกัด" />
								<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
							</div>
						</div>
						<select name="affsub2_id" id="affsub2_id" class="form-control" style="display:none; height:40px;">
							<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
						</select>
						<select name="affsub3_id" id="affsub3_id" class="form-control" style="display:none; height:40px;">
							<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
						</select>
						<select name="affsub4_id" id="affsub4_id" class="form-control" style="display:none; height:40px;">
							<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
						</select>
						<div id="affsub4_text" style="display:none">
							<div class="wrap-input100 m-t-10">
								<input class="input100" name="affsub4_ans_id" id="affsub4_ans_id" type="text" class="form-control" placeholder="โปรดระบุสังกัด" />
								<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
							</div>
						</div>
					</div>

					<!-- Documentary Address -->
					<!-- <div class="wrap-input100 validate-input">
						<input class="input100 text-black" type="text" name="docaddress" value="<?php echo $rowlogin['docaddress']; ?>"/>
						<span class="symbol-input100">
							<i class="fas fa-address-book" aria-hidden="true"></i>
						</span>
					</div> -->


					<div class="font-10 font-weight-bold text-dark mb-2 pl-2 mt-3 ">ที่อยู่จัดส่งเอกสาร</div>
				<div class="wrap-input100 validate-input mt-3">
					<input class="input100"  type="text" name="docaddress" class="form-control" placeholder="ที่อยู่จัดส่งเอกสาร" max-lenght="100" required />
					<span class="symbol-input100">
						<i class="fas fa-file-signature" aria-hidden="true"></i>
					</span>
				</div>

				<div class=" mt-3 wrap-input100 validate-input">
				<select id="province_token"   name="province" class="form-control" style="margin-bottom:10px; height:40px"  required>
					<option value="" selected> เลือกจังหวัด </option>
					<option value="กรุงเทพมหานคร">กรุงเทพมหานคร</option>
					<option value="กระบี่">กระบี่ </option>
					<option value="กาญจนบุรี">กาญจนบุรี </option>
					<option value="กาฬสินธุ์">กาฬสินธุ์ </option>
					<option value="กำแพงเพชร">กำแพงเพชร </option>
					<option value="ขอนแก่น">ขอนแก่น</option>
					<option value="จันทบุรี">จันทบุรี</option>
					<option value="ฉะเชิงเทรา">ฉะเชิงเทรา </option>
					<option value="ชัยนาท">ชัยนาท </option>
					<option value="ชัยภูมิ">ชัยภูมิ </option>
					<option value="ชุมพร">ชุมพร </option>
					<option value="ชลบุรี">ชลบุรี </option>
					<option value="เชียงใหม่">เชียงใหม่ </option>
					<option value="เชียงราย">เชียงราย </option>
					<option value="ตรัง">ตรัง </option>
					<option value="ตราด">ตราด </option>
					<option value="ตาก">ตาก </option>
					<option value="นครนายก">นครนายก </option>
					<option value="นครปฐม">นครปฐม </option>
					<option value="นครพนม">นครพนม </option>
					<option value="นครราชสีมา">นครราชสีมา </option>
					<option value="นครศรีธรรมราช">นครศรีธรรมราช </option>
					<option value="นครสวรรค์">นครสวรรค์ </option>
					<option value="นราธิวาส">นราธิวาส </option>
					<option value="น่าน">น่าน </option>
					<option value="นนทบุรี">นนทบุรี </option>
					<option value="บึงกาฬ">บึงกาฬ</option>
					<option value="บุรีรัมย์">บุรีรัมย์</option>
					<option value="ประจวบคีรีขันธ์">ประจวบคีรีขันธ์ </option>
					<option value="ปทุมธานี">ปทุมธานี </option>
					<option value="ปราจีนบุรี">ปราจีนบุรี </option>
					<option value="ปัตตานี">ปัตตานี </option>
					<option value="พะเยา">พะเยา </option>
					<option value="พระนครศรีอยุธยา">พระนครศรีอยุธยา </option>
					<option value="พังงา">พังงา </option>
					<option value="พิจิตร">พิจิตร </option>
					<option value="พิษณุโลก">พิษณุโลก </option>
					<option value="เพชรบุรี">เพชรบุรี </option>
					<option value="เพชรบูรณ์">เพชรบูรณ์ </option>
					<option value="แพร่">แพร่ </option>
					<option value="พัทลุง">พัทลุง </option>
					<option value="ภูเก็ต">ภูเก็ต </option>
					<option value="มหาสารคาม">มหาสารคาม </option>
					<option value="มุกดาหาร">มุกดาหาร </option>
					<option value="แม่ฮ่องสอน">แม่ฮ่องสอน </option>
					<option value="ยโสธร">ยโสธร </option>
					<option value="ยะลา">ยะลา </option>
					<option value="ร้อยเอ็ด">ร้อยเอ็ด </option>
					<option value="ระนอง">ระนอง </option>
					<option value="ระยอง">ระยอง </option>
					<option value="ราชบุรี">ราชบุรี</option>
					<option value="ลพบุรี">ลพบุรี </option>
					<option value="ลำปาง">ลำปาง </option>
					<option value="ลำพูน">ลำพูน </option>
					<option value="เลย">เลย </option>
					<option value="ศรีสะเกษ">ศรีสะเกษ</option>
					<option value="สกลนคร">สกลนคร</option>
					<option value="สงขลา">สงขลา </option>
					<option value="สมุทรสาคร">สมุทรสาคร </option>
					<option value="สมุทรปราการ">สมุทรปราการ </option>
					<option value="สมุทรสงคราม">สมุทรสงคราม </option>
					<option value="สระแก้ว">สระแก้ว </option>
					<option value="สระบุรี">สระบุรี </option>
					<option value="สิงห์บุรี">สิงห์บุรี </option>
					<option value="สุโขทัย">สุโขทัย </option>
					<option value="สุพรรณบุรี">สุพรรณบุรี </option>
					<option value="สุราษฎร์ธานี">สุราษฎร์ธานี </option>
					<option value="สุรินทร์">สุรินทร์ </option>
					<option value="สตูล">สตูล </option>
					<option value="หนองคาย">หนองคาย </option>
					<option value="หนองบัวลำภู">หนองบัวลำภู </option>
					<option value="อำนาจเจริญ">อำนาจเจริญ </option>
					<option value="อุดรธานี">อุดรธานี </option>
					<option value="อุตรดิตถ์">อุตรดิตถ์ </option>
					<option value="อุทัยธานี">อุทัยธานี </option>
					<option value="อุบลราชธานี">อุบลราชธานี</option>
					<option value="อ่างทอง">อ่างทอง </option>
					<option value="อื่นๆ">อื่นๆ</option>
				</select>
					<!-- <input class="input100" type="text" name="province" class="form-control" placeholder="จังหวัด" required /> -->

				</div>
				
				
				<div class="wrap-input100 validate-input">

				<!-- <select name="district" id="district" class=" d-none form-control">
					<option value="">เลือกอำเภอ</option>
				</select> -->
				<select name="district" id="district" class=" d-none form-control" style="margin-bottom:10px; height:40px"  required>
					<option value="" disabled="disabled" selected="selected">เลือกอำเภอ</option>
				</select>

				<!-- <input class="input100" type="text" name="district_sub" class="form-control" placeholder="อำเภอ" required />
					<span class="symbol-input100">
						<i class="fas fa-address-book" aria-hidden="true"></i>
					</span> -->


				</div>
				
				<div class="wrap-input100 validate-input">
					<select name="district_sub" id="district_sub" class=" d-none  form-control" style="margin-bottom:10px; height:40px"  required>
						<!-- <option value="" disabled="disabled" selected="selected">เลือกอำเภอ</option> -->
					</select>	
				
				<!-- <select name="district_sub" id="district_sub" class=" d-none form-control"> -->
						<!-- <option value="">เลือกอำเภอ</option> -->
					<!-- </select>	 -->
				<!-- <input class="input100" type="text" name="district" class="form-control" placeholder="ตำบล" required />
					<span class="symbol-input100">
						<i class="fas fa-address-book" aria-hidden="true"></i>
					</span> -->
				</div>


				<div class="wrap-input100 validate-input">
					<input  class="input100" type="text" id="zip_code_token" name="zipcode" class="form-control" placeholder="รหัสไปรษณีย์" required />
					<span class="symbol-input100">
						<i class="fas fa-address-book" aria-hidden="true"></i>
					</span>
				</div>


					<!-- Birthdate -->
					<div class="wrap-input100 validate-input">
						<input class="input100 text-black" type="date" name="birthdate" value="<?php echo $rowlogin["birthdate"]; ?>"
						min='1950-01-01' max='2002-01-01'/>
						<span class="symbol-input100">
							<i class="fas fa-birthday-cake" aria-hidden="true"></i>
						</span>
					</div>
					<!-- Submit button -->
					<div class="container-login100-form-btn">
						<input type="hidden" name="check" value="1">
						<button class="login100-form-btn" type="submit" id="register">
							บันทึก
						</button>
					</div>
					<!-- ################################################################################################ -->
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	<script src="js/projectRegist4occ.js"></script>
<script>

	  if(localStorage.getItem('production')=="1") {
        window.url = "https://localhost/";
      }
      else if(localStorage.getItem('production')=="2"){
        window.url = "https://www.thaisuprateacher.org/dev/";
      }

	function PickPrename(val) {
		var elePrename = document.getElementById('pre_name');
		var elePrenameText = document.getElementById('pre_name_text');
		var elePrenameTextReq = document.getElementById('pre_name_text_required');
		if(val == 'O') {
			elePrenameText.style.display = 'block';
			elePrenameTextReq.required = true;
		}else{
			elePrenameText.style.display = 'none';
			elePrenameText.value = '';
			elePrenameTextReq.required = false;
		}
	}


	let url_token
		if(localStorage.getItem('production')=="1") {
			url_token = `${window.url}Thaisuprateacher/signup_data.php`
		}
		else {
			url_token = `${window.url}signup_data.php`
		}

	let province_token = document.getElementById('province_token')
	province_token.addEventListener('change',e =>{
		console.log(e.target.value);
		var formData = new FormData();
		formData.append('province', e.target.value);
		if(e.target.value!=""){
			let district = document.getElementById('district');
			 district.classList.remove("d-none");
		}
		fetch(url_token, {
		  method:"post",
		  body:formData
		})
		.then(function(response) {
		  return response.json();
		})
		.then((value)=>{
			console.log(value,'value');
			let str = "<option value =''>เลือกอำเภอ</option>";
			let district = document.getElementById('district')
			district.innerHTML = '';

			value.forEach(value_str => {
					console.log(value_str.district);
				str+= `<option value='${value_str.district+"/"+value_str.zip_code}' >`+value_str.district+"</option>";
			});

			district.innerHTML = str;
		})
		.catch(function(error) {
		  console.log("Request failed", error);
		});
	})

	let district = document.getElementById('district');
	district.addEventListener('change',e =>{
		
		console.log(e.target.value);
		var formData = new FormData();
		if(e.target.value!=""){
			let district_sub = document.getElementById('district_sub');
			 district_sub.classList.remove("d-none");
		}

		// let zip_code_token = document.getElementById('zip_code_token');
		// zip_code_token.value = e.target.value.split("/")[1];

		formData.append('district', e.target.value);
		if(e.target.value!=""){
			let district_sub = document.getElementById('district');
			district_sub.classList.remove("d-none");
		}
		fetch(url_token, {
		  method:"post",
		  body:formData
		})
		.then(function(response) {
		  return response.json();
		})
		.then((value)=>{
			// console.log(value);
			console.log(value,'value');
			let str = "<option value =''>เลือกตำบล</option>";
			let district = document.getElementById('district_sub')
			district.innerHTML = '';
			value.forEach(value_str => {
					console.log(value_str.district_sub);
				str+= `<option value='${value_str.district_sub+"/"+value_str.zip_code}' >`+value_str.district_sub+"</option>";
			});
			
			district.innerHTML = str;
		})
		.catch(function(error) {
		  console.log("Request failed", error);
		});
	})

	let district_sub = document.getElementById('district_sub');
	district_sub.addEventListener('change',e =>{
	
		let zip_code_token = document.getElementById('zip_code_token');
		zip_code_token.value = e.target.value.split("/")[1];
	})

	// let btn_register = document.getElementById('register');
	// btn_register.addEventListener('click',e =>{
	// 	if(document.getElementById('district').value == ""  ||  document.getElementById('district_sub').value == ""  ){
	// 		alert("กรุณาเลือก จังหวัด  อำเภอ ตำบล ใหม่ ")
	// 	}
	// })


</script>

<script src="js/jquery.min.js"></script>
<script src="js/projectRegist4affA.js" type="text/javascript"></script>
	
<!--===============================================================================================-->	
	<!-- <script src="vendor/jquery/jquery-3.2.1.min.js"></script> -->
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>