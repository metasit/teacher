<?php 
	session_start();
	if(!isset($_SESSION['email'])) { // Check if user doesn't log in, then go to login page
		header('location: login.php');
	}else{
			if($_SESSION['level'] == 'admin') {
				include ('condb.php');
				$order_group = $_GET['order_group'];

				$sqlorders1="SELECT * FROM orders WHERE order_group='$order_group' ";
				$resultorders1=mysqli_query($con,$sqlorders1);
				$roworders1 = $resultorders1->fetch_assoc();

				/* Check that order not รอการชำระเงิน status */
				if($roworders1['order_status'] == 'รอการชำระเงิน') {
					echo '<script>';
						echo "alert('คุณครูยังไม่ได้ทำการแนบหลักฐานการโอน จึงไม่สามารถพิมพ์ใบเสร็จได้ค่ะ');";
						echo "window.location.replace('ระบบหลังบ้านร้านค้า.php?win_scroll=$win_scroll')";
					echo '</script>';
				}else{
					if($roworders['shipping_kind'] == 'normal') {
						$shipping_cost = 50;
					}else{
						$shipping_cost = 80;
					}

					$sqltotal="SELECT SUM(product_price*product_amount) AS price_total FROM orders WHERE order_group='$order_group' ";
					$resulttotal=mysqli_query($con,$sqltotal);
					$rowtotal=mysqli_fetch_array($resulttotal);
				}
			}else{
				header("Location: javascript:history.go(-1);");
			}
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
	
<style>
	div	{
		font-family: Angsana New;
		color: black
	}
	th, td {
		font-family: Angsana New;
		font-size:23px;
		color:black;
		text-align: center;
		font-weight: 500;
	}
</style>
</head>

<body class="BG-diamond">
<div class="hoc clear" style="width:150px;">
		<div class="BG-green1 m-t-50 center fs-20" style="color:white; padding:10px; border-radius:5px; cursor:pointer; font-family:KanitLight;" 
		onmousemove="this.style.backgroundColor='transparent'; this.style.border='1px solid white';" 
		onmouseout="this.style.backgroundColor='rgb(72,160,0)'; this.style.border='none';"
		onclick="javascript:this.style.display='none'; window.print();">พิมพ์ใบเสร็จ</div>
</div>

<div class="BG-white m-l-r-auto m-t-50 m-b-50" style="width:984px;">
	<!-- ต้นฉบับ -->
	<div class="hoc clear p-t-60 p-b-80">
		<div style="width:20%; display:inline-block; text-align:center; margin-bottom:50px;">
			<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Footer.png" alt="มูลนิธิครูดีของแผ่นดิน Logo Footer.png" style="width:150px;">
		</div>
		<div style="width:50%; display:inline-block; text-align:left; margin-left:20px; font-size:23px; font-weight:700; line-height:40px; vertical-align:-45px;">
			มูลนิธิครูดีของแผ่นดิน(สำนักงานใหญ่)
			<br>
			103 ถนนสุวิสุทธิ์กษัตริย์ แขวงบางขุนพรหม เขตพระนคร กรุงเทพฯ 10200
			<br>
			เลขประจำตัวผู้เสียภาษี 099-3-00038742-2
		</div>
		<div style="width:19.4%; display:inline-block; text-align:center; font-size:50px; font-weight:700;">
			ต้นฉบับ
		</div>


		<div style="width:100%; margin-top:20px; font-size:23px; line-height:30px;">
			<div style="text-align:center;">
				ใบสำคัญรับเงิน/ใบกำกับภาษี
				<br>
				RECEIPT/TAX INVOICE
			</div>
			<div style="width:80%; display:inline-block; text-align:right;">
					วันที่
			</div>
			<div style="width:19.4%; display:inline-block; text-align:center;">
				<?php
					$month_arr=array(
						"1"=>"มกราคม",
						"2"=>"กุมภาพันธ์",
						"3"=>"มีนาคม",
						"4"=>"เมษายน",
						"5"=>"พฤษภาคม",
						"6"=>"มิถุนายน", 
						"7"=>"กรกฎาคม",
						"8"=>"สิงหาคม",
						"9"=>"กันยายน",
						"10"=>"ตุลาคม",
						"11"=>"พฤศจิกายน",
						"12"=>"ธันวาคม"                 
				);
				
				echo date("d", strtotime($roworders1['slip_date']))." ".$month_arr[date("n", strtotime($roworders1['slip_date']))]." ".(date("Y", strtotime($roworders1['slip_date']))+543);
			?>
			</div>
			<div style="width:80%; display:inline-block; text-align:right;">
					เลขที่
			</div>
			<div style="width:19.4%; display:inline-block; text-align:center;">
				.......................................
			</div>
				
			<div style="width:25%; display:inline-block; text-align:left;">
				ได้รับเงินจาก
			</div>
			<div style="width:74.4%; display:inline-block; text-align:left;">
				<?php echo $roworders['contact_name']; ?>
			</div>
			<div style="width:25%; display:inline-block; text-align:left;">
				ที่อยู่
			</div>
			<div style="width:74.4%; display:inline-block; text-align:left;">
				<?php echo $roworders['shipping_address']; ?>
			</div>
			<div style="width:25%; display:inline-block; text-align:left;">
				เลขประจาตัวผู้เสียภาษีอากร
			</div>
			<div style="width:74.4%; display:inline-block; text-align:left;">
				...............................................
			</div>

			<table style="width:100%; margin-top:40px;">
				<thead>
					<tr>
						<th>ลำดับที่</th>
						<th>รายการ</th>
						<th>จำนวน</th>
						<th>ราคา</th>
					</tr>
				</thead>
				<tbody>
						<?php $i=1;
							$sqlorders="SELECT * FROM orders WHERE order_group='$order_group' ";
							$resultorders=mysqli_query($con,$sqlorders);

							while($roworders = $resultorders->fetch_assoc()) {
								$product_id = $roworders['product_id'];
								$sqlshelf="SELECT * FROM shelf WHERE product_id='$product_id' ";
								$resultshelf=mysqli_query($con,$sqlshelf);
								$rowshelf=mysqli_fetch_array($resultshelf);
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td style="text-align:left;"><?php echo $rowshelf['product_name']; ?></td>
									<td><?php echo $roworders['product_amount']; ?></td>
									<td style="text-align:right; padding-right:0;"><?php echo number_format($roworders['product_amount']*$roworders['product_price'],2); ?></td>
								</tr>
						<?php	$i++; } ?>
				</tbody>
			</table>
			<?php
				$price_tax = $rowtotal['price_total']+$shipping_cost;
				$price_notax = $price_tax*100/107;
				$tax = $price_tax - $price_notax;
				/* convert currency number to thai word */
								function Convert($amount_number)
				{
						$amount_number = number_format($amount_number, 2, ".","");
						$pt = strpos($amount_number , ".");
						$number = $fraction = "";
						if ($pt === false) 
								$number = $amount_number;
						else
						{
								$number = substr($amount_number, 0, $pt);
								$fraction = substr($amount_number, $pt + 1);
						}
						
						$ret = "";
						$baht = ReadNumber ($number);
						if ($baht != "")
								$ret .= $baht . "บาท";
						
						$satang = ReadNumber($fraction);
						if ($satang != "")
								$ret .=  $satang . "สตางค์";
						else 
								$ret .= "ถ้วน";
						return $ret;
				}

				function ReadNumber($number)
				{
						$position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
						$number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
						$number = $number + 0;
						$ret = "";
						if ($number == 0) return $ret;
						if ($number > 1000000)
						{
								$ret .= ReadNumber(intval($number / 1000000)) . "ล้าน";
								$number = intval(fmod($number, 1000000));
						}
						
						$divider = 100000;
						$pos = 0;
						while($number > 0)
						{
								$d = intval($number / $divider);
								$ret .= (($divider == 10) && ($d == 2)) ? "ยี่" : 
										((($divider == 10) && ($d == 1)) ? "" :
										((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
								$ret .= ($d ? $position_call[$pos] : "");
								$number = $number % $divider;
								$divider = $divider / 10;
								$pos++;
						}
						return $ret;
				}
			?>
			<div style="width:74.4%; display:inline-block; margin-top:40px; text-align:right; font-weight:700;">
				ค่าขนส่ง
			</div>
			<div style="width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php echo number_format($shipping_cost,2); ?>
			</div>
			<div style="width:74.4%; display:inline-block; margin-top:10px; text-align:right; font-weight:700;">
				ราคารวม
			</div>
			<div style="width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php echo number_format($price_notax,2); ?>
			</div>
			<div style="width:74.4%; display:inline-block; text-align:right; font-weight:700;">
				ราคาภาษีมูลค่าเพิ่ม
			</div>
			<div style="width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php echo number_format($tax,2); ?>
			</div>
			<div style="width:49.5%; display:inline-block; text-align:right; font-weight:700;">
				(<?php echo Convert($price_tax); ?>)
			</div>
			<div style="width:24.4%; display:inline-block; text-align:right; font-weight:700;">
				ราคารวมสุทธิ
			</div>
			<div style="width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php echo number_format($price_tax,2); ?>
			</div>


			<div style="width:25%; display:inline-block; text-align:center; margin-top:20px;">
				..............................................
			</div>
			<div style="width:74.4%; display:inline-block; text-align:center;">
			</div>
			<div style="width:25%; display:inline-block; text-align:center;">
				เจ้าหน้าที่การเงิน
			</div>
			<div style="width:74.4%; display:inline-block; text-align:center;">
			</div>
		</div>
	</div>

	<!-- สำเนา -->
	<div class="hoc clear p-t-60 p-b-80">
		<div style="width:20%; display:inline-block; text-align:center; margin-bottom:50px;">
			<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Footer.png" alt="มูลนิธิครูดีของแผ่นดิน Logo Footer.png" style="width:150px;">
		</div>
		<div style="width:50%; display:inline-block; text-align:left; margin-left:20px; font-size:23px; font-weight:700; line-height:40px; vertical-align:-45px;">
			มูลนิธิครูดีของแผ่นดิน(สำนักงานใหญ่)
			<br>
			103 ถนนสุวิสุทธิ์กษัตริย์ แขวงบางขุนพรหม เขตพระนคร กรุงเทพฯ 10200
			<br>
			เลขประจำตัวผู้เสียภาษี 099-3-00038742-2
		</div>
		<div style="width:19.4%; display:inline-block; text-align:center; font-size:50px; font-weight:700;">
			สำเนา
		</div>

		<div style="width:100%; margin-top:20px; font-size:23px; line-height:30px;">
			<div style="text-align:center;">
				ใบสำคัญรับเงิน/ใบกำกับภาษี
				<br>
				RECEIPT/TAX INVOICE
			</div>
			<div style="width:80%; display:inline-block; text-align:right;">
					วันที่
			</div>
			<div style="width:19.4%; display:inline-block; text-align:center;">
				<?php
					$month_arr=array(
						"1"=>"มกราคม",
						"2"=>"กุมภาพันธ์",
						"3"=>"มีนาคม",
						"4"=>"เมษายน",
						"5"=>"พฤษภาคม",
						"6"=>"มิถุนายน", 
						"7"=>"กรกฎาคม",
						"8"=>"สิงหาคม",
						"9"=>"กันยายน",
						"10"=>"ตุลาคม",
						"11"=>"พฤศจิกายน",
						"12"=>"ธันวาคม"                 
				);
					echo date("d", strtotime($roworders1['slip_date']))." ".$month_arr[date("n", strtotime($roworders1['slip_date']))]." ".(date("Y", strtotime($roworders1['slip_date']))+543);
				?>
			</div>
			<div style="width:80%; display:inline-block; text-align:right;">
					เลขที่
			</div>
			<div style="width:19.4%; display:inline-block; text-align:center;">
				.......................................
			</div>
				
			<div style="width:25%; display:inline-block; text-align:left;">
				ได้รับเงินจาก
			</div>
			<div style="width:74.4%; display:inline-block; text-align:left;">
				<?php echo $roworders['contact_name']; ?>
			</div>
			<div style="width:25%; display:inline-block; text-align:left;">
				ที่อยู่
			</div>
			<div style="width:74.4%; display:inline-block; text-align:left;">
				<?php echo $roworders['shipping_address']; ?>
			</div>
			<div style="width:25%; display:inline-block; text-align:left;">
				เลขประจาตัวผู้เสียภาษีอากร
			</div>
			<div style="width:74.4%; display:inline-block; text-align:left;">
				...............................................
			</div>

			<table style="width:100%; margin-top:40px;">
				<thead>
					<tr>
						<th>ลำดับที่</th>
						<th>รายการ</th>
						<th>จำนวน</th>
						<th>ราคา</th>
					</tr>
				</thead>
				<tbody>
						<?php $i=1;
							$sqlorders="SELECT * FROM orders WHERE order_group='$order_group' ";
							$resultorders=mysqli_query($con,$sqlorders);

							while($roworders = $resultorders->fetch_assoc()) {
								$product_id = $roworders['product_id'];
								$sqlshelf="SELECT * FROM shelf WHERE product_id='$product_id' ";
								$resultshelf=mysqli_query($con,$sqlshelf);
								$rowshelf=mysqli_fetch_array($resultshelf);
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td style="text-align:left;"><?php echo $rowshelf['product_name']; ?></td>
									<td><?php echo $roworders['product_amount']; ?></td>
									<td style="text-align:right; padding-right:0;"><?php echo number_format($roworders['product_amount']*$roworders['product_price'],2); ?></td>
								</tr>
						<?php	$i++; } ?>
				</tbody>
			</table>
			<?php
				$price_tax = $rowtotal['price_total']+$shipping_cost;
				$price_notax = $price_tax*100/107;
				$tax = $price_tax - $price_notax;
			?>
			<div style="width:74.4%; display:inline-block; margin-top:40px; text-align:right; font-weight:700;">
				ค่าขนส่ง
			</div>
			<div style="width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php echo number_format($shipping_cost,2); ?>
			</div>
			<div style="width:74.4%; display:inline-block; margin-top:10px; text-align:right; font-weight:700;">
				ราคารวม
			</div>
			<div style="width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php echo number_format($price_notax,2); ?>
			</div>
			<div style="width:74.4%; display:inline-block; text-align:right; font-weight:700;">
				ราคาภาษีมูลค่าเพิ่ม
			</div>
			<div style="width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php echo number_format($tax,2); ?>
			</div>
			<div style="width:49.5%; display:inline-block; text-align:right; font-weight:700;">
				(<?php echo Convert($price_tax); ?>)
			</div>
			<div style="width:24.4%; display:inline-block; text-align:right; font-weight:700;">
				ราคารวมสุทธิ
			</div>
			<div style="width:25%; display:inline-block; text-align:right; font-weight:700;">
				<?php echo number_format($price_tax,2); ?>
			</div>


			<div style="width:25%; display:inline-block; text-align:center; margin-top:20px;">
				..............................................
			</div>
			<div style="width:74.4%; display:inline-block; text-align:center;">
			</div>
			<div style="width:25%; display:inline-block; text-align:center;">
				เจ้าหน้าที่การเงิน
			</div>
			<div style="width:74.4%; display:inline-block; text-align:center;">
			</div>
		</div>
	</div>


</div>

</body>
</html>