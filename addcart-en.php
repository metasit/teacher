<?php session_start();
			require_once('condb.php');
			date_default_timezone_set("Asia/Bangkok");

if(!isset($_SESSION['email'])) { // The case of No Member first time add cart
	/* Set nomem_group */
	$sqlmax = "SELECT DISTINCT MAX(nomem_group) AS max_nomem_group FROM `cart` ";
	$resultmax = mysqli_query($con,$sqlmax);
	$rowmax = mysqli_fetch_array($resultmax);
	
	/* Set email and nomem_group valiable */
	$nomem_group = $rowmax['max_nomem_group']+1;
	$email = 'GUEST'.' '.$nomem_group;
	$_SESSION['email'] = $email;
	$_SESSION['nomem_group'] = $nomem_group;
}else{ // The case of Member loged in or No Member second time add cart
	$email = $_SESSION['email'];
	if(isset($_SESSION['nomem_group'])) {
		$nomem_group = $_SESSION['nomem_group'];
	}else{
		$nomem_group = 0;
	}
}

error_reporting(~E_NOTICE);
$product_id = $_GET['product_id'];
$product_name = $_GET['product_name'];
$product_image = $_GET['product_image'];
$product_amount = $_GET['product_amount'];
$win_scroll = $_GET['win_scroll'];

if($_GET['addcart'] == 1) { // if addcart button has been submited
	$sqlse = "SELECT * FROM `shelf` WHERE product_id='$product_id' ";
	$resultse = mysqli_query($con,$sqlse);
	$rowse = mysqli_fetch_array($resultse);
	// Set product_price as customer level
	if($_SESSION['level']=='memberGeneral' || $_SESSION['level']=='admin') {
		$product_price = $rowse['product_price_mem'];
	}elseif($_SESSION['level']=='memberSilver' || $_SESSION['level']=='memberGold' || $_SESSION['level']=='memberDiamond') {
		$product_price = $rowse['product_price_highmem'];
	}else{
		$product_price = $rowse['product_price_nomem'];
	}
	if($_SESSION['level']=='memberGeneral' || $_SESSION['level']=='admin') {
		$product_price_sale = $rowse['product_price_mem_sale'];
	}elseif($_SESSION['level']=='memberSilver' || $_SESSION['level']=='memberGold' || $_SESSION['level']=='memberDiamond') {
		$product_price_sale = $rowse['product_price_highmem_sale'];
	}else{
		$product_price_sale = $rowse['product_price_nomem_sale'];
	}
	
	$sqlcart = "SELECT * FROM `cart` WHERE email='$email' AND product_id='$product_id' ";
	$resultcart = mysqli_query($con,$sqlcart);
	$rowcart = mysqli_fetch_array($resultcart);

	if($rowcart['email'] != $email && $rowcart['product_id'] != $product_id) { // if the product is the first time added, then insert all new whole row to cart table
		$sql="INSERT INTO `cart` (`email`,`nomem_group`,`product_id`,`product_name`,`product_image`,`product_price`,`product_price_sale`,`product_amount`,`product_total`)
		 VALUES ('$email','$nomem_group','$product_id','$product_name','$product_image','$product_price','$product_price_sale','1','$product_price_sale')";
		$res= $con->query($sql) or die($con->error); //Check error
		if($res) {
			header('location: สนับสนุนของที่ระลึก-en.php?win_scroll='.$win_scroll);
		}else{
			echo '<script>Something wents wrong<br>
				Please contact developer of Thaisuprateacher <br>to find out the issue<br>
			  Thank you</script>';
		}
	}else{ //if the product is the same in cart, then update only amount to existing
		$product_amount = $rowcart['product_amount']+1;
		$product_total = $rowcart['product_price']*$product_amount;
		$sql = "UPDATE `cart` SET nomem_group='$nomem_group', product_amount='$product_amount', product_total='$product_total' WHERE email='$email' AND product_id='$product_id' ";
		$res = $con->query($sql) or die($con->error); //Check error
		if($res) {
			header('location: สนับสนุนของที่ระลึก-en.php?win_scroll='.$win_scroll);
		}else{
			echo '<script>Something wents wrong<br>
				Please contact developer of Thaisuprateacher <br>to find out the issue<br>
			  Thank you</script>';
		}
	}

}else{
	echo '<script>Something wents wrong<br>
				Please contact developer of Thaisuprateacher <br>to find out the issue<br>
			  Thank you</script>';
}

?>