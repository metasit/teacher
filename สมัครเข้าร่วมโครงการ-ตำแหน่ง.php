<?php
	session_start();
	require_once('condb.php');

	if(isset($_SESSION['ID'])) {

	}else{

		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php?CFP=30')";
		echo '</script>';

	}

?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<!--
				<div class="textlink3 PreMenu_fl_right" style="padding:2px;">
					<a href="login.php">ไปหน้าเข้าสู่ระบบสมาชิก</a>
				</div>
				################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" id="Ocform" action="addOcc.php" method="POST" enctype="multipart/form-data">
					<!-- ################################################################################################ -->
					<span class="login100-form-title">
						สมัครเข้าร่วมโครงการ
					</span>
					<!-- ################################################################################################ -->
					<!-- Start show step -->
					<div class="center">
						<span class="dot BG-green1"></span>
						<span class="line"></span>
						<span class="dot"></span>
						<span class="line"></span>
						<span class="dot"></span>
					</div>
					<div class="left">
						<p style="color:rgb(94,177,26);">ขั้นตอนที่ 1: เลือกอาชีพ และตำแหน่ง</p>
					</div>
					<!-- End show step -->
					<!-- ################################################################################################ -->
					<!-- Start อาชีพ, ตำแหน่ง Content -->
					<!-- เลือกอาชีพ -->
					<label for="Oc" class="center m-t-10 text-black bold">อาชีพ</label>
					<select id="Oc" name="occupation_id" class="form-control" style="height:40px" onchange="PickOccupation(this.value);" required>
						<option value="" disabled="disabled" selected="selected">เลือกอาชีพ</option>
						<option value="OcA">ข้าราชการ พนักงานราชการ ลูกจ้างประจำ อัตราจ้าง ประเภทครูและบุคลากรทางการศึกษา</option>
						<option value="OcB">ข้าราชการ พนักงานราชการ ลูกจ้างประจำ อัตราจ้าง ที่ไม่ใช่ประเภทครูและบุคลากรทางการศึกษา</option>
						<option value="OcC">ข้าราชการบำนาญ/เกษียณ</option>
						<option value="OcD">เจ้าของธุรกิจ</option>
						<option value="OcE">พนักงานบริษัท/รับจ้าง</option>
						<option value="OcF">นักเรียน/นักศึกษา</option>
						<option value="OcG">อาชีพอิสระ</option>
						<option value="OcO">อื่นๆ โปรดระบุ....</option>
					</select>
					<!-- เลือกตำแหน่งหลัก -->
					<label for="position" id="pohead" class="center m-t-10 text-black bold" style="display:none">ตำแหน่ง</label>
					<!-- OcA List -->
					<select id="pomain4OcA" name="pomain_id" class="form-control" style="display:none; height:40px;" onchange="PickPoMain4OcA(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ผู้ปฏิบัติหน้าที่สอน</option>
						<option value="B">ผู้บริหารสถานศึกษา</option>
						<option value="C">ผู้บริหารการศึกษา</option>
						<option value="D">บุคลากรทางการศึกษาอื่น</option>
							<!-- <option value="O">อื่นๆ ระบุ</option> -->
					</select>
					<!-- OcB List -->
					<div id="pomain4OcB" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcB1" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcB2" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcB3" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่) *" />
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- OcC List -->
					<div id="pomain4OcC" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcC1" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcC2" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcC3" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่) *" />
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- OcD List -->
					<div id="pomain4OcD" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcD1" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcD2" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcD3" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่) *" />
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- OcE List -->
					<div id="pomain4OcE" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcE1" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcE2" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcE3" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่) *" />
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- OcF List -->
					<select id="pomain4OcF" name="pomain_id" class="form-control" style="display:none; height:40px;" onchange="PickPoMain4OcF(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ประธาน/รองประธาน/กรรมการ นักเรียน-นักศึกษา</option>
						<option value="B">ประธาน/รองประธาน/กรรมการ ชมรม-ชุมนุม-สโมสร-กิจกรรม</option>
						<option value="C">หัวหน้า/รองหัวหน้า ห้อง-กลุ่ม-ชั้นปี</option>
						<option value="D">นักเรียน/นักศึกษาทั่วไป</option>
					</select>
					<!-- OcG List -->
					<div id="pomain4OcG" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcG1" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcG2" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcG3" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่) *" />
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- OcO List -->
					<div id="pomain4OcO" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcO1" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcO2" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcO3" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่) *" />
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- เลือกตำแหน่งย่อยขั้น1 -->
					<!-- OcAA List -->
					<select id="posub4OcAA" name="posub1_id" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAA(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ครู</option>
						<option value="B">ครู กศน.</option>
						<option value="C">ครู ตชด.</option>
						<option value="D">อาจารย์</option>
						<option value="O">อื่นๆ ระบุ</option>
					</select>
					<!-- OcAB List -->
					<select id="posub4OcAB" name="posub1_id" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAB(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองผู้อำนวยการสถานศึกษา</option>
						<option value="B">ผู้อำนวยการสถานศึกษา</option>
						<option value="C">เจ้าของสถานศึกษา/ผู้รับใบอนุญาต</option>
						
						<option value="E">ครูใหญ่ โรงเรียน ตชด.</option>
						<option value="O">อื่นๆ ระบุ</option>
					</select>
					<!-- OcAC List -->
					<select id="posub4OcAC" name="posub1_id" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAC(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">สำนักงานเขตพื้นที่การศึกษา</option>
						<option value="B">ศึกษาธิการ</option>
						<option value="C">ผู้บริหารส่วนกลางกระทรวง</option>
						<option value="D">สถาบันอุดมศึกษา</option>
						<option value="E">สำนักงาน กศน.</option>
						<option value="F">สำนักงานการศึกษาเอกชน</option>
						<option value="O">อื่นๆ ระบุ</option>
					</select>
					<!-- OcAD List -->
					<select id="posub4OcAD" name="posub1_id" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAD(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ศึกษานิเทศก์</option>
						<option value="O">อื่นๆ ระบุ</option>
					</select>
					<!-- OcAO List ****************** ************* ***************** ************************ --> 
					<div id="posub4OcAO" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub1_id_ans" type="text" class="form-control" placeholder="ตำแหน่งXX" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- OcAO List ****************** ************* ***************** ************************ --> 
					<!-- เลือกตำแหน่งย่อยขั้น2 -->
					<!-- OcAAA List -->
					<select id="posub4OcAAA" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ครูอัตราจ้าง</option>
						<option value="B">ครูผู้ช่วย</option>
						<option value="C">ครู</option>
						<option value="D">ครูชำนาญการ</option>
						<option value="E">ครูชำนาญการพิเศษ</option>
						<option value="F">ครูเชี่ยวชาญ</option>
						<option value="G">ครูเชี่ยวชาญพิเศษ</option>
					</select>
					<!-- OcAAB List -->
					<select id="posub4OcAAB" name="posub2_id" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAAB(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ครู</option>
						<option value="B">ครู กศน.ตำบล</option>
						<option value="C">ครู ศูนย์การเรียนรู้ชุมชน</option>
						<option value="D">ครู อาสาสมัคร</option>
						<option value="E">ครู สอนคนพิการ</option>
						<option value="F">ครู ประกาศนียบัตรวิชาชีพ</option>
						<option value="G">ครูอัตราจ้าง</option>
						<option value="H">ครูผู้ช่วย</option>
						<option value="I">ครูชำนาญการ</option>
						<option value="J">ครูชำนาญการพิเศษ</option>
						<option value="K">ครูเชี่ยวชาญ</option>
						<option value="L">ครูเชี่ยวชาญพิเศษ</option>
						<option value="O">อื่น ๆ ระบุ....</option>
					</select>
					
					<!-- OcAABO List -->
					<div id="posub4OcAABO" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub3_id_ans" type="text" class="form-control" placeholder="ตำแหน่ง" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
				 	<!--OcAAC List -->
					<!-- No Content in this list -->
					<!-- OcAAD List -->
					<select id="posub4OcAAD" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">อาจารย์</option>
						<option value="B">ผู้ช่วยศาสตราจารย์</option>
						<option value="C">รองศาสตราจารย์</option>
						<option value="D">ศาสตราจารย์</option>
					</select>
					<div id="posub4OcAAO" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub2_id_ans" type="text" class="form-control" placeholder="ตำแหน่ง" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- OcABA List -->
					<select id="posub4OcABA" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองผู้อำนวยการโรงเรียนเอกชน</option>
						<option value="B">รองผู้อำนวยการชำนาญการ</option>
						<option value="C">รองผู้อำนวยการชำนาญการพิเศษ</option>
						<option value="D">รองผู้อำนวยการเชี่ยวชาญ</option>
					</select>
					<!-- OcABB List -->
					<select id="posub4OcABB" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ผู้อำนวยการโรงเรียนเอกชน</option>
						<option value="B">ผู้อำนวยการชำนาญการ</option>
						<option value="C">ผู้อำนวยการชำนาญการพิเศษ</option>
						<option value="D">ผู้อำนวยการเชี่ยวชาญ</option>
						<option value="F">ผู้อำนวยการเชี่ยวชาญพิเศษ</option>
					</select>
					<!-- OcABC List -->
					<!-- No Content in this list -->
					<!-- OcABD List
					<select id="posub4OcABD" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ผู้อำนวยการ กศน.ตำบล</option>
						<option value="B">ผู้อำนวนการ กศน.จังหวัด</option>
					</select>-->
					<div id="posub4OcABO" style="display:none">
					<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub4_id_ans" type="text" class="form-control" placeholder="ตำแหน่ง" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- OcABE List -->
					<!-- No Content in this list -->
					<!-- OcACA List -->
					<select id="posub4OcACA" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาชำนาญการพิเศษ</option>
						<option value="B">รองผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญ</option>
						<option value="C">ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญ</option>
						<option value="D">ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญพิเศษ</option>
					</select>
					<!-- OcACB List -->
					<select id="posub4OcACB" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองศึกษาธิการจังหวัด</option>
						<option value="B">ศึกษาธิการจังหวัด</option>
						<option value="C">รองศึกษาธิการภาค</option>
						<option value="D">ศึกษาธิการภาค</option>
					</select>
					<!-- OcACC List -->
					<select id="posub4OcACC" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองผู้อำนวยการสำนัก</option>
						<option value="B">ผู้อำนวยการสำนัก</option>
						<option value="C">ผู้เชี่ยวชาญ</option>
						<option value="D">ผู้ตรวจราชการ/ที่ปรึกษาระดับ 10</option>
						<option value="F">ปลัด/รองปลัด/อธิบดี/รองอธิบดี</option>
					</select>
					<!-- OcACD List -->
					<select id="posub4OcACD" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองคณบดี</option>
						<option value="B">คณบดี</option>
						<option value="C">รองอธิการบดี</option>
						<option value="D">อธิการบดี</option>
					</select>
					<select id="posub4OcACE" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองผู้อำนวยการสำนักงาน กศน.จังหวัด/กรุงเทพมหานครชำนาญการพิเศษ</option>
						<option value="B">รองผู้อำนวยการสำนักงาน กศน.จังหวัด/กรุงเทพมหานครเชี่ยวชาญ</option>
						<option value="C">ผู้อำนวยการสำนักงาน กศน.จังหวัด/กรุงเทพมหานครเชี่ยวชาญ</option>
						<option value="D">ผู้อำนวยการสำนักงาน กศน.จังหวัด/กรุงเทพมหานครเชี่ยวชาญพิเศษ</option>
					</select>
					<select id="posub4OcACF" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ผู้อำนวยการสำนักงานการศึกษาเอกชนอำเภอชำนาญการพิเศษ</option>
						<option value="B">ผู้อำนวยการสำนักงานการศึกษาเอกชนอำเภอเชี่ยวชาญ</option>
						<option value="C">รองผู้อำนวยการสำนักงานการศึกษาเอกชนจังหวัดชำนาญการพิเศษ</option>
						<option value="D">รองผู้อำนวยการสำนักงานการศึกษาเอกชนจังหวัดเชี่ยวชาญ</option>
						<option value="E">ผู้อำนวยการสำนักงานการศึกษาเอกชนจังหวัดเชี่ยวชาญ</option>
						<option value="F">ผู้อำนวยการสำนักงานการศึกษาเอกชนจังหวัดเชี่ยวชาญพิเศษ</option>
					</select>
					<div id="posub4OcACO" style="display:none">
					<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub5_id_ans" type="text" class="form-control" placeholder="ตำแหน่ง" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- OcADA List -->
					<select id="posub4OcADA" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ศึกษานิเทศก์ชำนาญการ</option>
						<option value="B">ศึกษานิเทศก์ชำนาญการพิเศษ</option>
						<option value="C">ศึกษานิเทศก์เชี่ยวชาญ</option>
						<option value="D">ศึกษานิเทศก์เชี่ยวชาญพิเศษ</option>
					</select>
					<div id="posub4OcADO" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub6_id_ans" type="text" class="form-control" placeholder="ตำแหน่ง" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- OcAOO List ****************** ************* ***************** ************************--> 
					<div id="posub4OcAOO" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub1_id_ans" type="text" class="form-control" placeholder="ตำแหน่ง" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- OcAOO  ****************** ************* ***************** ************************--> 
					<!-- End อาชีพ, ตำแหน่ง Content -->
					<!-- ################################################################################################ -->
					<div class="container-login100-form-btn m-t-20">
						<button class="login100-form-btn" style="width:30%" type="submit" id="register">
							ไปขั้นตอนต่อไป
						</button>
					</div>
					<!-- ################################################################################################ -->
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>

<!-- JS for multi-sub-dropdown -->
<script src="js/projectRegist4occ.js"></script>
</body>
</html>