<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if($_POST['CFP'] == 'สมัครต้นกล้าแทนนักเรียน_step1-ครู') {

			/* Set prename */
			$prename_stu = $_POST['prename'];
			/* Set firstname */
			if(substr(trim($_POST['firstname']), 0, 8) == 'ด.ช.' || substr(trim($_POST['firstname']), 0, 8) == 'ด.ญ.') {

				$firstname_stu = substr(trim($_POST['firstname']), 9);

			}else{

				$firstname_stu = trim($_POST['firstname']);

			}
			/* Set lastname */
			$lastname_stu = trim($_POST['lastname']);
			/* Set birthdate */
			$birthdate_stu = $_POST['birthdate'];
			/* Set special child status (เป็นเด็กพิเศษหรือเปล่า) */
			$spe_child_btn = $_POST['spe_child_btn'];
			if($spe_child_btn == 'on') {

				$spe_child_status = 'A,yes';

			}else{

				$spe_child_status = 'A,no';

			}
			/* Set pomain_id */
			$pomain_id = $_POST['pomain_id'];
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* ตรวจว่า เด็กคนนี้มีครูที่ปรึกษาไปหรือยัง จากชื่อ-นามสกุล และชื่อครูที่ปรึกษาในช่อง basic_score_total */
			$sqlcheck = "SELECT basic_score_text FROM `login` WHERE firstname='$firstname_stu' AND lastname='$lastname_stu' AND basic_score_total IS NOT NULL ";
			$recheck = mysqli_query($con, $sqlcheck);

			if(mysqli_num_rows($recheck) != 0) {

				$rowcheck = mysqli_fetch_array($recheck);

				echo '<script>';
					echo "alert('นักเรียนคนนี้ มีครูที่ปรึกษาแล้วค่ะ (ครูที่ปรึกษาของ ".$firstname_stu." ".$lastname_stu." คือ ".$rowcheck['basic_score_text'].") นักเรียน 1 คน สามารถมีครูที่ปรึกษาได้เพียงคนเดียวค่ะ');";
					echo "window.location.replace('สมัครต้นกล้าแทนนักเรียน_step1-ครู.php?CFP=สมัครต้นกล้าแทนนักเรียน_step2-ครู&pomain_id=".$pomain_id."&prename_stu=".$prename_stu."&firstname_stu=".$firstname_stu."&lastname_stu=".$lastname_stu."&birthdate_stu=".$birthdate_stu."&spe_child_btn=".$spe_child_btn." ')";
				echo '</script>';

			}else{				
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				if(strpos($spe_child_status, 'yes') !== false) { // กรณีที่เป็นเด็กพิเศษ ให้สามารถสมัครระบบต้นกล้าแห่งความดีได้ โดยที่ระบบไม่จำเป็นต้องเช็คอายุว่า ต่ำกว่า 9 ปีจริงหรือไม่

				}else{ // กรณีทั่วไป ให้เช็คก่อนว่า นักเรียนอายุต่ำกว่า 9 ปีจริงหรือไม่ ถ้าใช้ให้เข้า loop นี้ ให้สามารถสมัครระบบต้นกล้าแห่งความดีได้

					/* If system=A, need to check student age not exceed 9 years old */
					$birthdate_stu = date_create($birthdate_stu);
					$datetoday = date_create(date('Y-m-d'));
					
					$stu_age = date_diff($birthdate_stu, $datetoday)->y;
					$birthdate_stu = date_format($birthdate_stu, 'Y-m-d');

					if($stu_age >= 9) { // ถ้าอายุเกิน 9 ปี ให้เข้า loop นี้ คือ alert และส่งกลับไปหน้า สมัครต้นกล้าแทนนักเรียน_step1-ครู.php

						echo '<script>';
							echo "alert('อายุเกิน 9 ปี ไม่สามารถสมัครระบบการส่งงานเป็นต้นกล้าแห่งความดีได้ค่ะ');";
							echo "window.location.replace('สมัครต้นกล้าแทนนักเรียน_step1-ครู.php?CFP=สมัครต้นกล้าแทนนักเรียน_step2-ครู&prename_stu=".$prename_stu."&firstname_stu=".$firstname_stu."&lastname_stu=".$lastname_stu."&birthdate_stu=".$birthdate_stu."&spe_child_btn=".$spe_child_btn." ')";
						echo '</script>';

					}elseif($stu_age < 3) { // ถ้าอายุไม่ถึง 3 ปี ให้เข้า loop นี้ คือ alert และส่งกลับไปหน้า สมัครต้นกล้าแทนนักเรียน_step1-ครู.php

						echo '<script>';
							echo "alert('นักเรียนต้องมีอายุอย่างน้อย 3 ปี ถึงจะสามารถเข้าร่วมโครงการเด็กดีของแผ่นดินขั้นพื้นฐานได้ค่ะ');";
							echo "window.location.replace('สมัครต้นกล้าแทนนักเรียน_step1-ครู.php?CFP=สมัครต้นกล้าแทนนักเรียน_step2-ครู&prename_stu=".$prename_stu."&firstname_stu=".$firstname_stu."&lastname_stu=".$lastname_stu."&birthdate_stu=".$birthdate_stu."&spe_child_btn=".$spe_child_btn." ')";
						echo '</script>';

					}
				}
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/* INSERT หรือ UPDATE ค่าลง `login` */
				$sqllogin = "SELECT ID FROM `login` WHERE firstname='$firstname_stu' AND lastname='$lastname_stu' AND basic_score_remark2 LIKE '%B,yes%' ";
				$relogin = mysqli_query($con, $sqllogin);

				if(mysqli_num_rows($relogin) == 0) { // first time ครูสมัครระบบต้นกล้าให้นักเรียนคนนี้

					$basic_score_remark2 = $spe_child_status.',B,yes,';

					$sql = "INSERT INTO `login` (prename, firstname, lastname, birthdate, basic_score_remark2) VALUES ('$prename_stu', '$firstname_stu', '$lastname_stu', '$birthdate_stu', '$basic_score_remark2') ";
					$re = $con->query($sql) or die($con->error);

				}else{

					$rowlogin = mysqli_fetch_array($relogin);

					$ID_stu = $rowlogin['ID'];
					$basic_score_remark2 = $spe_child_status.',B,yes,';

					$sql = "UPDATE `login` SET prename='$prename_stu', firstname='$firstname_stu', lastname='$lastname_stu', birthdate='$birthdate_stu' WHERE ID='$ID_stu' ";
					$re = $con->query($sql) or die($con->error);

					/* Log User Action */
					$scorelog_task = 'สมัครต้นกล้าแทนนักเรียน_step1';
					$scorelog_detail = 'ครู,ชั้น5,เด็ก,ขั้นพื้นฐาน,'.$prename_stu.','.$firstname_stu.' '.$lastname_stu.','.$birthdate_stu.','.$basic_score_remark2;
					$scorelog_total = intval($ID_stu);
					$sqllog = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail, scorelog_total) VALUES ('$ID', '$scorelog_task', '$scorelog_detail', '$scorelog_total') ";
					$relog = $con->query($sqllog) or die($con->error);

				}

				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			}

		}elseif($_GET['CFP'] == 'สมัครต้นกล้าแทนนักเรียน_step3-ครู') {

			$prename_stu = $_GET['prename_stu'];
			$firstname_stu = $_GET['firstname_stu'];
			$lastname_stu = $_GET['lastname_stu'];
			$birthdate_stu = $_GET['birthdate_stu'];
			$spe_child_btn = $_GET['spe_child_btn'];
			$pomain_id = $_GET['pomain_id'];
			$ID_stu = $_GET['ID_stu'];

		}else{

			echo "<script>window.history.go(-1)</script>";

		}

	}else{

		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';

	}
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/เด็กดีของแผ่นดิน Logo.jpg" style="width:70%;" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" action="สมัครต้นกล้าแทนนักเรียน_step3-ครู.php" method="POST" enctype="multipart/form-data">
					<!-- ################################################################################################ -->
					<span class="login100-form-title">
						อาชีพและตำแหน่ง
					</span>
					<!-- ################################################################################################ -->
					<!-- Start show step -->
					<div class="center">
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
						<span class="line" style="width:25%"></span>
						<span class="dot"></span>
						<span class="line" style="width:25%"></span>
						<span class="dot"></span>
					</div>
					<div class="center">
						<p style="color:rgb(94,177,26);">ขั้นตอนที่ 2: เลือกตำแหน่ง</p>
					</div>
					<!-- End show step -->
					<!-- ################################################################################################ -->
					<!-- Start อาชีพ, ตำแหน่ง Content -->
					<!-- เลือกอาชีพ -->
					<label for="Oc" class="center m-t-10 text-black bold">อาชีพ</label>
					<select id="Oc" name="occupation_id" class="form-control" style="height:40px" required>
						<option value="" disabled="disabled" selected="selected">เลือกอาชีพ</option>
						<option value="OcF" selected="selected">นักเรียน/นักศึกษา</option>
					</select>
					<!-- เลือกตำแหน่งหลัก -->
					<label for="position" id="pohead" class="center m-t-10 text-black bold">ตำแหน่ง</label>
					<!-- OcF List -->
					<select id="pomain4OcF" name="pomain_id" class="form-control" style="height:40px;" required>
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A" <?php if($pomain_id=='A'){echo 'selected="selected" ';} ?> >ประธาน/รองประธาน/กรรมการ นักเรียน-นักศึกษา</option>
						<option value="B" <?php if($pomain_id=='B'){echo 'selected="selected" ';} ?> >ประธาน/รองประธาน/กรรมการ ชมรม-ชุมนุม-สโมสร-กิจกรรม</option>
						<option value="C" <?php if($pomain_id=='C'){echo 'selected="selected" ';} ?> >หัวหน้า/รองหัวหน้า ห้อง-กลุ่ม-ชั้นปี</option>
						<option value="D" <?php if($pomain_id=='D'){echo 'selected="selected" ';} ?> >นักเรียน/นักศึกษาทั่วไป</option>
					</select>
					<!-- End อาชีพ, ตำแหน่ง Content -->
					<!-- ################################################################################################ -->
					<div class="container-login100-form-btn m-t-20">
						<a href="สมัครต้นกล้าแทนนักเรียน_step1-ครู.php?
							CFP=สมัครต้นกล้าแทนนักเรียน_step2-ครู
							&pomain_id=<?php echo $pomain_id; ?>
							&prename_stu=<?php echo $prename_stu; ?>
							&firstname_stu=<?php echo $firstname_stu; ?>
							&lastname_stu=<?php echo $lastname_stu; ?>
							&birthdate_stu=<?php echo $birthdate_stu; ?>
							&spe_child_btn=<?php echo $spe_child_btn; ?>" 
							class="login100-form-btn" style="width:30%; margin:0 10px;">ย้อนกลับ
						</a>
						<button class="login100-form-btn" style="width:30%" type="submit" id="register">ยืนยัน</button>
					</div>
					<!-- ################################################################################################ -->

					<input type="hidden" name="prename_stu" value="<?php echo $prename_stu; ?>">
					<input type="hidden" name="firstname_stu" value="<?php echo $firstname_stu; ?>">
					<input type="hidden" name="lastname_stu" value="<?php echo $lastname_stu; ?>">
					<input type="hidden" name="birthdate_stu" value="<?php echo $birthdate_stu; ?>">
					<input type="hidden" name="spe_child_btn" value="<?php echo $spe_child_btn; ?>">
					<input type="hidden" name="ID_stu" value="<?php echo $ID_stu; ?>">
					<input type="hidden" name="CFP" value="สมัครต้นกล้าแทนนักเรียน_step2-ครู">
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>