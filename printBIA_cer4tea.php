<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if($_GET['CFP'] == 'มอบเกียรติบัตรBIA') {

			$affil_code = $_SESSION['affil_code'];

			$sqlbia_cer = "SELECT * FROM `bia_cer` WHERE `bia_cer_affil_code`='$affil_code' && `bia_cer_code` LIKE 'F%' && `bia_cer_detail`='$ID' ";
			$reBC = mysqli_query($con, $sqlbia_cer);
			
			if(mysqli_num_rows($reBC) == 0) { // first time click print BIA_cer4tea

				$sqlbia_cerF = "SELECT * FROM `bia_cer` WHERE `bia_cer_affil_code`='$affil_code' && `bia_cer_code` LIKE 'F%' ";
				$reBCF = mysqli_query($con, $sqlbia_cerF);

				$count_tea_cer = mysqli_num_rows($reBCF); // ตอนนี้มีครู Print ไปแล้วกี่ใบ

				$sqlbia_cerB = "SELECT * FROM `bia_cer` WHERE `bia_cer_affil_code`='$affil_code' && `bia_cer_code`='B' ";
				$reBCB = mysqli_query($con, $sqlbia_cerB);
				$rowBCB = mysqli_fetch_array($reBCB);

				$max_tea_cer = ceil($rowBCB['bia_cer_data1']/20); // หาจำนวนที่ครูสามารถ Print ได้มากที่สุดกี่ใบ

				if($count_tea_cer < $max_tea_cer) { // เช็คว่าจำนวนสิทธิ์ในการพิมพ์เกียรติบัตรครบหรือยัง

				

					/***********************************************************************************************************************************/
					/**************************************************** Start Print & Save เกียรติบัตร **************************************************/
					/***********************************************************************************************************************************/
	
					/* Save เกียรติบัตรออนไลน์ with user name */
					$jpg_image = imagecreatefromjpeg('images/เกียรติบัตร/BIA/BIA_cer_template ครู.jpg'); // Create Image From Existing File
					$font_color = imagecolorallocate($jpg_image, 0, 0, 0); // Set font color
					$font_path = 'layout/styles/fonts/THSarabunIT๙ Bold.ttf'; // Set font file path
					
					/* Set Text that need to be Printed On Image */
					$sqllogin = "SELECT `prename` FROM `login` WHERE `ID`='$ID' ";
					$relogin = mysqli_query($con, $sqllogin);
					$rowlogin = mysqli_fetch_array($relogin);
					// Set pename
					if($rowlogin['prename'] == 'A') {
						$prename = 'นาย';
					}elseif($rowlogin['prename'] == 'B') {
						$prename = 'นาง';
					}elseif($rowlogin['prename'] == 'C') {
						$prename = 'นางสาว';
					}elseif($rowlogin['prename'] == 'D') {
						$prename = 'ด.ช.';
					}elseif($rowlogin['prename'] == 'E') {
						$prename = 'ด.ญ.';
					}elseif($rowlogin['prename'] == 'O') {
						$prename = $rowlogin['prename_remark'];
					}
					$name = $_SESSION['firstname'].' '.$_SESSION['lastname']; // Set name
					$namewithprename = $prename.$name; // Set prename
	
					// Set School
					$sqlaffil_name = "SELECT `affil_name` FROM `login` WHERE `ID`='$ID' ";
					$reaffil_name = mysqli_query($con, $sqlaffil_name);
					$rowaffil_name = mysqli_fetch_array($reaffil_name);
	
					$affil_name = $rowaffil_name['affil_name'];

					if($affil_code == 'AfBAA00300156') {
						$school = 'โรงเรียนบางปะอิน ราชานุเคราะห์ ๑';
					}else{
						$school = 'โรงเรียน'.substr($affil_name, strrpos($affil_name, '*')+1);
					}

					// Set Running Number for basic certificate
					$year = substr(date('Y')+543,-2);
					$find_run_num = 'คสป.'.$year.'02%';

					$sqlbia_cer_codeI = "SELECT * FROM `bia_cer` WHERE `bia_cer_code` LIKE 'I' AND `bia_cer_data1` LIKE '$find_run_num' ORDER BY `bia_cer_id` DESC LIMIT 1 ";
					$reBCCI = mysqli_query($con, $sqlbia_cer_codeI);
					$rowBCCI = mysqli_fetch_array($reBCCI);
				
					$bia_cer_type = 'คสป.';
					$bia_cer_num = sprintf('%05d', intval(substr($rowBCCI['bia_cer_data1'], -5))+1);
					$bia_cer_run_num = $bia_cer_type.$year.'02'.$bia_cer_num;
	
					// Set date
					$date = date('Y-m-d');
	
					/* Array for Thai Date */
					$arabicnum = array("1","2","3","4","5","6","7","8","9","0");
					$thainum = array("๑","๒","๓","๔","๕","๖","๗","๘","๙","๐");
					$test = str_replace($numthai,$numarabic,$message);
					/* Array for Thai Month */
					$month_arr=array(
						"1"=>"มกราคม",
						"2"=>"กุมภาพันธ์",
						"3"=>"มีนาคม",
						"4"=>"เมษายน",
						"5"=>"พฤษภาคม",
						"6"=>"มิถุนายน", 
						"7"=>"กรกฎาคม",
						"8"=>"สิงหาคม",
						"9"=>"กันยายน",
						"10"=>"ตุลาคม",
						"11"=>"พฤศจิกายน",
						"12"=>"ธันวาคม"                 
					);
					$date_cer = str_replace($arabicnum, $thainum, date('j', strtotime($date))).' '.$month_arr[date('n', strtotime($date))].' '.str_replace($arabicnum, $thainum, (date('Y', strtotime($date))+543)); // Set certicate date in Thai
	
					$font_size = 40; //Set font size
					$angle = 0; //Set angle
					/* Set x-position on certificate for name */
					$dimen4name = imagettfbbox($font_size, $angle, $font_path, $namewithprename);
					$text4name = (abs($dimen4name[4] - $dimen4name[0]))/2;
					$x4name = 758 - $text4name;
					/* Set x-position on certificate for school */
					$dimen4school = imagettfbbox($font_size, $angle, $font_path, $school);
					$text4school = (abs($dimen4school[4] - $dimen4school[0]))/2;
					$x4school = 758 - $text4school;
					/* Print school name and date on certificate */
					imagettftext($jpg_image, $font_size, $angle, $x4name, 450, $font_color, $font_path, $namewithprename);
					imagettftext($jpg_image, $font_size, $angle, $x4school, 550, $font_color, $font_path, $school);
					imagettftext($jpg_image, $font_size, $angle, 600, 755, $font_color, $font_path, $date_cer);
					imagettftext($jpg_image, 27, $angle, 1260, 255, $font_color, $font_path, $bia_cer_run_num);
	
					$target_dir = 'images/เกียรติบัตร/BIA/'.$school.'/'; // Set target_directory
	
					if(!is_dir($target_dir)) { // if there's not folder in target_directory
						mkdir($target_dir); // Create folder name is today_date
					}
	
					$target_file = $target_dir.'BIA Cer Tea '.$name.'.jpg';
	
					imagejpeg($jpg_image, $target_file);// Send Image to Browser or save in directory on client
					imagedestroy($jpg_image); // Clear Memory
	
					/* INSERT data to `bia_cer` table */
					$bia_cer_affil_code = $affil_code;

					$bia_cer_code_F = 'F'.sprintf('%02d', $count_tea_cer+1);
					$bia_cer_data1_F = $target_file;
					$bia_cer_detail_F = $ID;

					$bia_cer_code_I = 'I';
					$bia_cer_data1_I = $bia_cer_run_num;
					$bia_cer_detail_I = NULL;

					$bia_cer_remark = 'IDคนทำ:'.$ID;

					$sql = "INSERT INTO `bia_cer` (`bia_cer_affil_code`, `bia_cer_code`, `bia_cer_data1`, `bia_cer_detail`, `bia_cer_remark`) 
					VALUES ('$bia_cer_affil_code', '$bia_cer_code_F', '$bia_cer_data1_F', '$bia_cer_detail_F', '$bia_cer_remark'),
					('$bia_cer_affil_code', '$bia_cer_code_I', '$bia_cer_data1_I', '$bia_cer_detail_I', '$bia_cer_remark') ";
					$res = $con->query($sql) or die($con->error); //Check error
	
					header('location: '.$target_file);
	
					/***********************************************************************************************************************************/
					/****************************************************** End Print & Save เกียรติบัตร **************************************************/
					/***********************************************************************************************************************************/

				}else{
					echo '<script>';
						echo "alert('จำนวนสิทธิ์การพิมพ์เกียรติบัตรประเภทครูเต็มแล้วค่ะ');";
						echo "window.location.replace('มอบเกียรติบัตรBIA.php')";
					echo '</script>';
				}


			}else{

				$rowBC = mysqli_fetch_array($reBC);
				header('location: '.$rowBC['bia_cer_data1']);

			}


		}else{
			echo "<script>window.history.go(-1)</script>";
		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>