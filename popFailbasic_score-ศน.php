<?php session_start();
			require_once('condb.php');
			$email = $_SESSION['email'];

	/* Call login table data from mySQL */
	$sql = "SELECT * FROM `login` WHERE email='$email' ";
	$re = mysqli_query($con,$sql);
	$row = mysqli_fetch_array($re);

	$last_basic_score_date = new DateTime($row['basic_score_date']);
	$next_basic_score_date = date_add($last_basic_score_date,date_interval_create_from_date_string('30 days'));
	$next_basic_score_date = $next_basic_score_date->format("d-m-Y");
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php#secA"><i class="fas fa-arrow-left"></i> กลับไปหน้าโครงการศึกษานิเทศก์ดีของแผ่นดิน</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
				<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
					<span class="login100-form-title fs-30 lh-1-8">
					ท่านได้คะแนน <?php echo $row['basic_score_total']; ?>/36 คะแนน
					<br>
					<?php
						$month_arr=array(
							"1"=>"มกราคม",
							"2"=>"กุมภาพันธ์",
							"3"=>"มีนาคม",
							"4"=>"เมษายน",
							"5"=>"พฤษภาคม",
							"6"=>"มิถุนายน", 
							"7"=>"กรกฎาคม",
							"8"=>"สิงหาคม",
							"9"=>"กันยายน",
							"10"=>"ตุลาคม",
							"11"=>"พฤศจิกายน",
							"12"=>"ธันวาคม"
						);
						?>
						ท่านได้คะแนนน้อยกว่าเกณฑ์ที่กำหนด กรุณาประเมินศึกษานิเทศก์ดีของแผ่นดิน ขั้นพื้นฐาน อีกครั้งหลังจากนี้ 30 วัน (ตั้งแต่วันที่
						<strong>
							<?php echo date("d", strtotime($next_basic_score_date))." ".$month_arr[date("n", strtotime($next_basic_score_date))]." ".(date("Y", strtotime($next_basic_score_date))+543); ?>
						</strong>
						เป็นต้นไป)
					</span>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>