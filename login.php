<?php
	session_start();

	if(isset($_SESSION['ID'])) {
		echo "<script>window.history.go(-1)</script>"; //Protect policy: No SESSION only that can accress this page
	}
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>

<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="index.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าหลัก</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_right" style="padding:2px;">
					<a href="signup.php">สมัครสมาชิก</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" name="formlogin" action="checklogin.php" method="POST" id="login">
					<!-- ################################################################################################ -->
					<input type="hidden" name="back2name" value="<?php echo $back2var; ?>"> <!-- URL value which need to send to checklogin.php -->
					<!-- ################################################################################################ -->
					<span class="login100-form-title">
						ระบบสมาชิก
					</span>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="email" name="email" class="form-control" placeholder="อีเมล" 
						pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$" title="Please enter a valid email address" required/>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fas fa-user" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="password" class="form-control" placeholder="รหัสผ่าน" 
						pattern=".{8,}" title="Please enter at least 8 characters" required/>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit" id="btn">
							เข้าสู่ระบบ
						</button>
					</div>
					<!-- ################################################################################################ -->
					<div class="text-center p-t-12">
						<span class="txt1">
							ลืม
						</span>
						<a class="txt2" href="forgetpassword.php">
							อีเมล / รหัสผ่าน?
						</a>
					</div>
					<!-- ################################################################################################ -->
					<?php
						if(isset($_GET['CFP'])) {
							if($_GET['CFP'] == 30) {
								?>
								<input type="hidden" name="CFP" value="30">
								<?php
							}
						}
					?>
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	 
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
	<script>
		// if(localStorage.getItem('production') == '1')  window.location.href='/dev/login.php'  else  window.location.href='/login.php'
		// if(localStorage.getItem('production') == '1')  window.location.href=''  else  window.location.href='/login.php'
	</script>

</body>
</html>