<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>
<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop-en.php'); ?>
                  <ul>
                    <li><a href="จดหมายรวมlatest.php">ภาษาไทย</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom-en.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false">Letter/Declaration</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 03 - จดหมาย -->
<div style="background-color:rgb(226,255,224);">
  <div class="hoc container clear">
    <table class="table1 table2" style="padding-bottom:20px;">
      <thead>
        <tr>
          <th</th>
          <th></th>
        </tr>
      </thead>
      <!-- ################################################################################################ -->
      <tbody>
        <!-- จดหมาย 024 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย024-en.php">จดหมายการทำหน้าที่อาสาฯ</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 023 -->
        <tr>
          <td>
						<a class="blink1">Hot</a>
            <a class="linkfortable" href="จดหมาย023-en.php">ประกาศรางวัลครูดีของแผ่นดินขั้นพื้นฐาน ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 022 -->
				<!--
        <tr>
          <td>
						<a class="blink1">Hot</a>
            <a class="linkfortable" href="จดหมาย022-en.php">ประกาศรางวัลครูดีของแผ่นดิน ชั้นที่ 5 ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
				-->
        <!-- จดหมาย 021 -->
        <tr>
          <td>
						<a class="blink1">Hot</a>
            <a class="linkfortable" href="จดหมาย021-en.php">ประกาศรางวัลศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 020 -->
				<!--
        <tr>
          <td>
						<a class="blink1">Hot</a>
            <a class="linkfortable" href="จดหมาย020-en.php">ประกาศรางวัลศึกษานิเทศก์ดี ขั้นเกียรติคุณ ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
				-->
        <!-- จดหมาย 019 -->
        <tr>
          <td>
            <a class="linkfortable" href="จดหมาย019-en.php">สพฐ. แจ้งแนวทางการคัดเลือกรางวัล คุรุชน คนคุณธรรม (ร่วมกับมูลนิธิครูดีของแผ่นดิน)</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
				<!-- จดหมาย 018 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย018-en.php">หนังสือโครงการเครือข่ายครูดีของแผ่นดิน ถึงหน่วยงานที่ MOU ร่วมกับมูลนิธิครูดีของแผ่นดิน</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 017 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย017-en.php">หนังสือแจ้งปฏิทินการดำเนินโครงการฯ ปี 2563</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
        <!-- จดหมาย 016 -->
        <tr>
          <td>
            <a class="linkfortable" href="จดหมาย016-en.php">ประกาศรางวัลครูดีของแผ่นดิน ชั้นที่ 4 ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
				<!-- จดหมาย 015 -->
        <tr>
          <td><a class="linkfortable" href="จดหมาย015-en.php">ทุนการศึกษาสำหรับเด็กและเยาวชนดีของแผ่นดิน ช่วงวิกฤตโรคติดต่อโควิด-๑๙</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<!-- End Content 03 - จดหมาย -->
<!-- ################################################################################################ -->
<!-- Start Pagination -->
<div class="wrapper row3 coloured" style="background-color:rgb(226,255,224);">
  <main class="hoc container clear">
    <div class="content">
      <nav class="pagination">
        <ul>
          <li class="current"><strong>3</strong></li>
          <li><a href="จดหมายรวม002-en.php">2</a></li>
          <li><a href="จดหมายรวม001-en.php">1</a></li>
        </ul>
      </nav>
    </div>
  </main>
</div>
<!-- End Pagination -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer-en.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>