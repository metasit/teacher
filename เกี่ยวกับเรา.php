<?php 
	session_start();
	require_once('condb.php');
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="เกี่ยวกับเรา-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom4Aboutus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="เกี่ยวกับเรา.php"> เกี่ยวกับเรา</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - มูลนิธิครูดีของแผ่นดิน -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <article class="group">
      <div class="one_first forth">
        <p class="fs-35 font-italic" style="font-family:RSUText; line-height:30pt;"><span style="color:rgb(72,160,0); font-size:45px;"><strong>มูลนิธิครูดีของแผ่นดิน</strong></span> (Foundation of Thai Suprateacher : FTST) องค์กรไม่แสวงหาผลกำไร จดทะเบียนอย่างเป็นทางการเมื่อวันที่ ๔ ธันวาคม พ.ศ.๒๕๖๐ มีสำนักงานใหญ่ ตั้งอยู่ที่ ๑๐๓ ถนนวิสุทธิ์กษัตริย์ แขวงบางขุนพรหม เขตพระนคร กรุงเทพมหานคร</p>
      </div>
    </article>
  </main>
</div>
<!-- End Content 01 - มูลนิธิครูดีของแผ่นดิน -->
<!-- ################################################################################################ -->
<!-- Start Content 02 - รูปหมู่ มูลนิธิครูดีของแผ่นดิน -->
<div>
	<img src="images/มูลนิธิครูดีของแผ่นดิน รูปหมู่.jpg" alt="มูลนิธิครูดีของแผ่นดิน รูปหมู่">
</div>
<!-- End Content 02 - รูปหมู่ มูลนิธิครูดีของแผ่นดิน -->
<!-- ################################################################################################ -->
<!-- Start Content 03 - วิสัยทัศน์ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <article style="text-align:center" >
        <p class="font-x3"><span style="color:rgb(180,147,31); line-height:50px;"><strong>วิสัยทัศน์</strong></span></p>
        <p class="fs-60 font-italic" style="font-family:RSUText; line-height:30pt;">“สร้าง<span style="color:rgb(72,160,0); font-size:33px;"><strong> คนดี</strong></span> ให้ <span style="color:rgb(34,55,226); font-size:33px"><strong>แผ่นดิน”</strong></span></p>
    </article>
  </main>
</div>
<!-- End Content 03 - วิสัยทัศน์ -->
<!-- ################################################################################################ -->
<!-- Start Content 04 - เครื่องหมาย -->
<div class="wrapper row3">
  <main class="hoc container2 clear">
    <article class="group">
      <div class="one_first second">
        <p class="font-x3"><span style="color:rgb(180,147,31); line-height:30px;"><strong>เครื่องหมาย</strong></span></p>
      </div>
      <div class="one_first forth">
        <p class="fs-40 font-italic" style="font-family:RSUText; line-height:30pt;">มีลักษณะเป็นรูป <span style="color:rgb(34,55,226); font-size:40px"><strong> หยดน้ำสีน้ำเงิน </strong></span> ภายในมี <span style="color:rgb(72,160,0); font-size:40px"><strong>ต้นกล้าแห่งดวงปัญญา</strong></span> ส่วน <span style="color:rgb(180,147,31); font-size:40px"><strong>ปลายหยดน้ำเป็นสีทอง</strong></span> ด้านล่างมีคำว่า มูลนิธิครูดีของแผ่นดิน</p>
      </div>
      <div class="one_quarter first">
        <img src="images/มูลนิธิครูดีของแผ่นดิน Logo.jpg" alt="มูลนิธิครูดีของแผ่นดิน Logo" style="width:250px;height:250px;">
      </div>
      <div class="three_quarter">
        <p class="font-x1" style="margin:0 0 50px 0">&emsp;&emsp;&emsp;มีความหมายว่า__________</p>
        <p class="fs-40" style="font-family:RSUText; line-height:30pt;"><span style="color:rgb(34,55,226);"><strong>หยดน้ำสีน้ำเงิน</strong></span> เปรียบเสมือนคุณครูผู้รักและมั่นคงในความดี จงรักภักดีต่อสถาบัน</p>
        <p class="fs-40" style="font-family:RSUText; line-height:30pt;"><span style="color:rgb(72,160,0);;"><strong>ต้นกล้าแห่งดวงปัญญา</strong></span> เปรียบเสมือนเด็กและเยาวชนที่ได้เติบโตขึ้นเป็นผู้มีดวงปัญญา</p>
        <p class="fs-40" style="font-family:RSUText; line-height:30pt;"><span style="color:rgb(180,147,31);"><strong>ปลายหยดน้ำเป็นสีทอง</strong></span> เปรียบเสมือนความเจริญรุ่งเรืองของสังคม</p>           
      </div>
    </article>
  </main>
</div>
<!-- End Content 04 - เครื่องหมาย -->
<!-- ################################################################################################ -->
<!-- Start Content 05 - วัตถุประสงค์ -->
<div class="wrapper row3" style="background-image:url('images/มูลนิธิครูดีของแผ่นดิน BG เกี่ยวกับเรา วัตถุประสงค์.jpg');">
  <main class="hoc container clear">
    <article class="group">
      <div class="one_first second">
        <p class="font-x3"><span style="color:rgb(180,147,31); line-height:50px; font-weight:700;">วัตถุประสงค์</span></p>
      </div>
      <ul class="fs-32" style="color:white; font-family:RSUText; line-height:30pt;">
        <li>ส่งเสริม สนับสนุน การยกระดับคุณภาพการศึกษาให้มีคุณภาพในระดับสากล</li>
        <li>ส่งเสริมสนับสนุนให้ครูและบุคลากรทางการศึกษาประพฤติตนเป็นแบบอย่างที่ดีของสังคม</li>
        <li>ดำเนินการ ส่งเสริม สนับสนุนให้ครู บุคลากรทางการศึกษา และบุคลากรที่เกี่ยวข้องร่วมกันสร้างคนดีให้แก่สังคม</li>
        <li>ส่งเสริม สนับสนุน พัฒนา และร่วมพัฒนาบุคลากรทางการศึกษา และบุคลากรในภาคส่วนต่างๆ ที่เกี่ยวข้องร่วมกับองค์กรทั้งรัฐ และเอกชน ทั้งภายในและภายนอกประเทศ โดยการอบรม สัมมนา ศึกษาดูงาน ผลิตหนังสือ สื่อการสอน และสื่อเทคโนโลยีต่างๆ เป็นต้น</li>
        <li>ดำเนินการหรือร่วมมือกับองค์กรการกุศลอื่น ๆ เพื่อสาธารณประโยชน์</li>
				<li>ไม่ดำเนินการเกี่ยวกับการเมืองแต่ประการใด</li>
      </ul>
    </article>
  </main>
</div>
<!-- End Content 05 - วัตถุประสงค์ -->
<!-- ################################################################################################ -->
<!-- Start Content 06 - คณะกรรมการ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <article class="group">
      <div class="sectiontitle">
        <p class="font-x3"><span style="color:rgb(180,147,31); line-height:30px;"><strong>คณะกรรมการ</strong></span></p>
        <br>
        <img src="images/มูลนิธิครูดีของแผ่นดิน บอร์ดบริหาร.jpg" alt="มูลนิธิครูดีของแผ่นดิน คณะกรรมการ">
      </div>
    </article>
  </main>
</div>
<!-- End Content 06 - คณะกรรมการ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>