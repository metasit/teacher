<?php 
	session_start();
	include ('condb.php');
	$sql_list_project  ="SELECT * FROM join_project";
	$list_project = $con->query($sql_list_project);
	if(!isset($_SESSION['email'])) { // Check if user doesn't log in, then go to login page
		header('location: login.php');
	}else{
		if($_SESSION['level'] == 'admin') {

	  
			$email = $_SESSION['email'];
	
			$doc_address = $_SESSION['docaddress'];
			$order_group = $_GET['order_group'];
			$shipping_cost = $_GET['shipping_cost'];
		}else{
			header("Location: javascript:history.go(-1);");
		}
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>


<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
        <?php if (strpos($_SESSION['email'], '@') !== false) { //Check Account is signed in
                if ($_SESSION["level"]=="admin") { //Admin Session ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
											<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*A*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*B*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
										</ul>
									</li>
          <?php }elseif ($_SESSION["level"]=="memberGeneral") { //General Member Session ?>
                <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> สมาชิกทั่วไป</li>
          <?php }elseif ($_SESSION["level"]=="memberSilver") { //Member Silver Session ?>
                <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</li>
          <?php }elseif ($_SESSION["level"]=="memberGold") { //Member Gold Session ?>
                <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</li>
          <?php }elseif ($_SESSION["level"]=="memberDiamond") { //Member Diamond Session ?>
                <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</li>
          <?php } ?>
                <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
          <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
                <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
        <?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
                  <ul>
                    <li><a href="index-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
                    <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึก</a></li>
                    
                  </ul>
                </li>
                <li class="faicon-basket"><i class="fas fa-caret-down"></i><i class="fa fa-shopping-basket"></i> <?php echo number_format($_SESSION['pricetotal'],2); ?>
									<ul>
										<li><a href="สนับสนุนของที่ระลึก.php" >ร้านค้า&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
										<li><a href="ตะกร้า.php">ดูของในตะกร้าและยืนยันคำสั่งซื้อ</a></li>
										
										<li><a href="ประวัติและสถานะการสั่งซื้อ.php">ประวัติและสถานะการสั่งซื้อ</a></li>
									</ul>
								</li>
        </ul>
      </nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
			  	   <?php 
					if($_SESSION['level'] == 'admin')
                    while($row = $list_project->fetch_assoc()){
                      $id_join_project = $row['id_join_project'];
                      $title_menu = $row['title_menu']; 
                  ?>
                  <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
                  <?php } ?>
              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="สนับสนุนของที่ระลึก.php">ร้านค้า</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">รายละเอียดคำสั่งซื้อ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - รายละเอียดคำสั่งซื้อ -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<div class="container">
			<!-- Start table of my product detail -->
			<div class="row">
				<div class="col-lg-12">
					<div class="table-main table-responsive">
						<!-- ################################################################################################ -->
						<table class="table">
							<thead>
								<tr>
									<th></th>
									<th class="product-explain">ของที่ระลึก</th>
									<th>วันที่สั่งซื้อ</th>
									<th>เวลา</th>
									<th>ราคาต่อชิ้น</th>
									<th>จำนวน</th>
									<th>ราคาสุทธิ</th>
									<th>สถานะ</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$sql1="SELECT SUM(product_price_sale*product_amount) AS pricetotal FROM orders WHERE order_group='$order_group' ";
								$result1=mysqli_query($con,$sql1);
								$row1=mysqli_fetch_array($result1);
								$price_total=$row1['pricetotal'];

								$sql2="SELECT * FROM orders WHERE order_group='$order_group' ";
								$result2=mysqli_query($con,$sql2);

								while($row2=mysqli_fetch_array($result2)) {
									/* Set shipping_address, contact_number to use outside while-loop and unset() at the end line */
									$_SESSION['shipping_kind'] = $row2['shipping_kind'];
									$_SESSION['shipping_address'] = $row2['shipping_address'];
									$_SESSION['contact_name'] = $row2['contact_name'];
									$_SESSION['contact_number'] = $row2['contact_number'];

									/* Set product_size and product_detail */
									$product_id = $row2['product_id'];
									$sql="SELECT * FROM shelf WHERE product_id='$product_id' ";
									$result=mysqli_query($con,$sql);
									$row=mysqli_fetch_array($result);

									/* Call login to set firstname and lastname */
									$user_email = $row2['email'];
									$sql4 = "SELECT * FROM login WHERE email='$user_email' ";
									$result4 = mysqli_query($con,$sql4);
									$row4 = mysqli_fetch_array($result4);
								?>
								<tr>
									<!-- Product Image -->
									<td class="thumbnail-img">
										<a href="#" onclick="return false">
											<img class="img-fluid" src="<?php echo $row['product_image']; ?>" alt="<?php echo $row['product_image']; ?>"/>
										</a>
									</td>
									<!-- Product Detail -->
									<td class="name-pr product-explain lh-1-0">
										<a href="#" onclick="return false">
											<?php echo $row['product_name']; ?>
											<div class="fs-15">
												<?php
													if(isset($row['product_size'])) { echo $row['product_size']; }
													if(isset($row['product_detail'])) { echo $row['product_detail']; }
													if(isset($row2['order_remark'])) { echo $row2['order_remark']; }
												?>
											</div>
										</a>
									</td>
									<!-- Order Date -->
									<td class="price-pr">
										<p><?php echo date("d-m-Y", strtotime($row2['order_date'])); ?></p>
									</td>
									<!-- Order Time -->
									<td class="price-pr">
										<p><?php echo date("H:i", strtotime($row2['order_date'])); ?></p>
									</td>
									<!-- Price per unit -->
									<td class="price-pr">
										<p><?php echo number_format($row2['product_price_sale'],2); ?></p>
									</td>
									<!-- Product Amount -->
									<td class="price-pr">
										<p><?php echo $row2['product_amount']; ?></p>
									</td>
									<!-- Total -->
									<td class="price-pr">
										<p><?php echo number_format($row2['product_price_sale']*$row2['product_amount'],2); ?></p>
									</td>
									<!-- Status -->
									<td class="name-pr">
										<?php if($row2['order_status'] == 'สินค้าถึงปลายทางแล้ว') { ?>
														<p><i class="fas fa-check-circle"></i> <?php echo $row2['order_status']; ?></p>
										<?php }else{ ?>
														<p><?php echo $row2['order_status']; ?></p>
										<?php } ?>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<!-- ################################################################################################ -->
					</div>
				</div>				
			</div>
			<!-- End table of my product detail -->
			<!-- ################################################################################################ -->
			<!-- Start Order Summary -->
			<div class="row my-5">
				<div class="col-lg-6 col-sm-12"></div>
					<div class="col-lg-6 col-sm-12">
						<div class="order-box" style="background-color:whitesmoke; border-radius:5px;">
							<!-- Total price -->
							<h3>สรุปยอดการสนับสนุน</h3>
							<div class="d-flex">
								<div class="ml-auto font-weight-bold"> <?php echo number_format($price_total,2); ?> บาท </div>
							</div>
							<hr>
							<!-- Shipping choice confirmation -->
							<div class="d-flex">
								<h3>วิธีการจัดส่ง</h3>
								<label>
									<?php
										if($_SESSION['shipping_kind'] == 'normal') { ?>
											+ ค่าจัดส่ง 50 บาท
											<img src="images/ThailandPost_Logo.png" alt="ThailandPost_Logo">
											พัสดุลงทะเบียน<br>
											ระยะเวลาการจัดส่ง: 3 - 7 วันทำการ <?php
										}elseif($_SESSION['shipping_kind'] == 'ems') { ?>
											+ ค่าจัดส่ง 80 บาท
											<img src="images/ThailandPost_Logo.png" alt="ThailandPost_Logo">
											พัสดุด่วนพิเศษ EMS<br>
											ระยะเวลาการจัดส่ง: 1 - 3 วันทำการ <?php
										}elseif($_SESSION['shipping_kind'] == 'Free') { ?>
											ฟรีค่าจัดส่ง
											<br>
											ระยะเวลาการจัดส่ง: 3 - 7 วันทำการ <?php
										}
									?>
								</label>
							</div>
							<hr>
							<!-- Grand Total (Total price + Shipping cost) -->
							<div class="d-flex gr-total">
								<div class="ml-auto h5">
									<h5>สรุปยอดทั้งหมด</h5>
									<?php
										if($_SESSION['shipping_kind'] == 'normal') {
											echo number_format($price_total+50,2);
										}elseif($_SESSION['shipping_kind'] == 'ems') {
											echo number_format($price_total+80,2);
										}else{
											echo number_format($price_total,2);
										}
									?> บาท
								</div>
							</div>
							<hr>
							<!-- Receiver Name comfirmation -->
							<div class="d-flex">
								<h3>ชื่อ-นามสกุล ผู้รับ</h3>
								<p><?php echo str_replace('<br>',' ',$_SESSION['contact_name']); ?></p>
							</div>
							<hr>
							<!-- Shipping Address comfirmation -->
							<div class="d-flex">
								<h3>ที่อยู่ในการจัดส่ง</h3>
								<div class="location-box">
									<?php echo $_SESSION['shipping_address']; ?>
								</div>
							</div>
							<hr>
							<!-- Receiver Phone Number comfirmation -->
							<div class="d-flex">
								<h3>เบอร์ติดต่อ ผู้รับ</h3>
								<div class="phonenum-box">
									<?php echo $_SESSION['contact_number']; ?>
								</div>
							</div>
						</div>
						<form action="ระบบหลังบ้านร้านค้า.php" class="col-12 d-flex shopping-box">
							<button  class="mr-auto btn hvr-hover">ย้อนกลับ</button>
						</form>
					</div>
			</div>
			<!-- End Order Summary -->
		</div>
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - รายละเอียดคำสั่งซื้อ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->


<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>






</body>
</html>

<?php
	unset($_SESSION['shipping_kind']);
	unset($_SESSION['shipping_address']);
	unset($_SESSION['contact_number']);
?>