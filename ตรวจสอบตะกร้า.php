<?php 
	session_start();
	require_once('condb.php');

	$email = $_SESSION['email'];
	$sql_list_project  ="SELECT * FROM join_project";
	$list_project = $con->query($sql_list_project);


	/* SUM all products price */
	$sqlcount="SELECT SUM(product_total) AS price_total FROM cart WHERE email='$email' ";
	$ressultcount=mysqli_query($con,$sqlcount);
	$rowcount=mysqli_fetch_array($ressultcount);
	$price_total=$rowcount['price_total'];

	if($price_total==0) { // Check if no product in cart, then go to shop page
		echo "<script>
			alert('คุณยังไม่มีสินค้าในตะกร้า สามารถเลือกสินค้าในร้านค้าของมูลนิธิได้เลยค่ะ');
			window.location.href='สนับสนุนของที่ระลึก.php';
			</script>";
	}

	$doc_address = $_SESSION['docaddress'];

	$sqlcart_id="SELECT * FROM cart WHERE email='$email' ";
	$recart_id=mysqli_query($con,$sqlcart_id);
	$rowcart_id=mysqli_fetch_array($recart_id);
	
	if(strpos($_SESSION['email'], 'GUEST') !== false) { // If Session is GUEST or No Member
		if(empty($_POST['nomem_address'] || $_POST['nomem_name'] || $_POST['nomem_email'] || $_POST['nomem_phonenum'] )) {
			header('location: ตะกร้า.php');
		}else{
			$cart_id = $rowcart_id['cart_id'];
			$new_address = $_POST['nomem_address'];
			$new_name = $_POST['nomem_name'];
			$contact_email = $_POST['nomem_email'];
			$new_phonenum = $_POST['nomem_phonenum'];
		}
	}else{
		$new_address = $_POST['new_address'];
		$new_name = $_SESSION['firstname'].' '.$_SESSION['lastname'];
		$new_phonenum = $_POST['new_phonenum'];
		$cart_id = $rowcart_id['cart_id'];
	}

	$shirtsize1 = $_POST['shirtsize1'];
	$shirtsize11 = $_POST['shirtsize11'];

	$sqlset1 = "UPDATE `cart` SET `cart_remark`='$shirtsize1' WHERE email='$email' AND product_id=1 ";
	$reset1 = $con->query($sqlset1) or die($con->error);
	$sqlset11 = "UPDATE `cart` SET `cart_remark`='$shirtsize11' WHERE email='$email' AND product_id=11 ";
	$reset11 = $con->query($sqlset11) or die($con->error);
?>


<!DOCTYPE html>

<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>


<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
        <?php if (strpos($_SESSION['email'], '@') !== false) {
								if ($_SESSION["level"]=="admin") { //Admin Session ?>
								<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
									<ul>
										<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
										<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
									</ul>
								</li>
					<?php }elseif ($_SESSION["level"]=="admin*A*") { ?>
								<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
									<ul>
										<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
									</ul>
								</li>
					<?php }elseif ($_SESSION["level"]=="admin*B*") { ?>
								<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
									<ul>
										<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION["level"]=="memberGeneral") { //General Member Session ?>
                <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> สมาชิกทั่วไป</li>
          <?php }elseif ($_SESSION["level"]=="memberSilver") { //Member Silver Session ?>
                <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</li>
          <?php }elseif ($_SESSION["level"]=="memberGold") { //Member Gold Session ?>
                <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</li>
          <?php }elseif ($_SESSION["level"]=="memberDiamond") { //Member Diamond Session ?>
                <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</li>
          <?php } ?>
                <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
          <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
                <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
        <?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
                  <ul>
                    <li><a href="ตรวจสอบตะกร้า-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
                    <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึก</a></li>
                  </ul>
                </li>
                <li class="faicon-basket"><i class="fas fa-caret-down"></i><i class="fa fa-shopping-basket"></i> <?php echo number_format($price_total,2); ?>
									<ul>
										<li><a href="สนับสนุนของที่ระลึก.php" >ร้านค้า&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
										<li><a href="ตะกร้า.php">ดูของในตะกร้าและยืนยันคำสั่งซื้อ</a></li>
										<li><a href="ประวัติและสถานะการสั่งซื้อ.php">ประวัติและสถานะการสั่งซื้อ</a></li>
									</ul>
								</li>
        </ul>
      </nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
	<!-- End Top-PreMenu -->
	
  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
			  <?php 
                while($row = $list_project->fetch_assoc()){
                  $id_join_project = $row['id_join_project'];
                  $title_menu = $row['title_menu']; 
              ?>
              <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
              <?php } ?>
              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="สนับสนุนของที่ระลึก.php">ร้านค้า</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li class="linkst"><a href="ตะกร้า.php">ตะกร้าของฉัน</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ยืนยันคำสั่งซื้อ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ตรวจสอบตะกร้า -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<div class="container">
			<!-- Start table of my product detail -->
			<div class="row">
				<div class="col-lg-12">
					<div class="table-main table-responsive">
						<!-- ################################################################################################ -->
						<?php
							$sqlcart = "SELECT `product_id` FROM cart WHERE email='$email' AND product_id IN ('1','11'); ";
							$recart = mysqli_query($con,$sqlcart);
							$rowcart = mysqli_fetch_array($recart);
						?>
						<table class="table">
							<thead>
								<tr>
									<th></th>
									<th class="product-explain">ของที่ระลึก</th>
									<?php if(isset($rowcart['product_id'])) { ?>
													<th>ขนาดเสื้อ</th>
									<?php } ?>
									<th>ราคา/ชิ้น</th>
									<th>จำนวน</th>
									<th>ราคารวม</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$results_per_page = 9; // number of results per page
								if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
								$start_from = ($page-1) * $results_per_page;

								$sql = "SELECT * FROM cart WHERE email='$email' ORDER BY product_id ASC LIMIT $start_from, ".$results_per_page;
								$rs_result = $con->query($sql);

								 // Set $re_result to arrange product in page
								while($row = $rs_result->fetch_assoc()) {
									/* Set product_size and product_detail by call sql_shelf with product_id */
									$product_id = $row['product_id'];
									$sql_shelf = "SELECT * FROM shelf WHERE product_id='$product_id' ";
									$rs_shelf = $con->query($sql_shelf);
									$row_shelf = $rs_shelf->fetch_assoc();
									?>
									<tr>
										<!-- Product Image -->
										<td class="thumbnail-img">
											<a href="#" onclick="return false">
												<img class="img-fluid" src="<?php echo $row['product_image']; ?>" alt="<?php echo $row['product_image']; ?>"/>
											</a>
										</td>
										<!-- Product Detail -->
										<td class="name-pr product-explain lh-1-0" <?php if(isset($rowcart['product_id']) && $row['product_id'] != 1 && $row['product_id'] != 11) {?> colspan="2" <?php } ?> >
											<a href="#" onclick="return false">
												<?php echo $row['product_name']; ?>
												<div class="fs-15">
													<?php
														if(isset($row_shelf['product_size'])) { echo $row_shelf['product_size']; }
														if(isset($row_shelf['product_detail'])) { echo $row_shelf['product_detail']; }
													?>
												</div>
											</a>
										</td>
										<!-- In case of Product Set, need to choose shirt size -->
										<?php if($row['product_id'] == 1 || $row['product_id'] == 11) { ?>
														<td>
															<p style="font-weight:700"><?php echo $row['cart_remark']; ?></p>
														</td>
										<?php	} ?>
										<!-- Price per unit -->
										<td class="price-pr">
											<p><?php echo number_format($row['product_price_sale'],2) ?></p>
										</td>
										<!-- Product Amount -->
										<td class="price-pr">
											<p><?php echo $row['product_amount'] ?></p>
										</td>
										<!-- Total -->
										<td class="price-pr">
											<p><?php echo number_format($row['product_price_sale']*$row['product_amount'],2) ?></p>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<!-- ################################################################################################ -->
					</div>
				</div>				
			</div>
			<!-- End table of my product detail -->
			<!-- ################################################################################################ -->
			<!-- Start Order Summary -->
			<div class="row my-5">
				<div class="col-lg-6 col-sm-12"></div>
				<div class="col-lg-6 col-sm-12">
					<div class="order-box">
						<!-- Total price -->
						<h3>สรุปยอดการสนับสนุน</h3>
						<div class="d-flex">
							<div class="ml-auto font-weight-bold"> <?php echo number_format($price_total,2); ?> บาท </div>
						</div>
						<hr>
						<!-- Shipping choice comfirmation -->
						<div class="d-flex">
							<h3>วิธีการจัดส่ง</h3>
							<label>
								
									ฟรีค่าจัดส่ง
										
									<br>
									ระยะเวลาการจัดส่ง: 3 - 7 วันทำการ
									
<!--
								<?php if($_POST['shipping_kind'] == 'normal') { ?>
									+ ค่าจัดส่ง 50 บาท
									<img src="images/ThailandPost_Logo.png" alt="ThailandPost_Logo">
									พัสดุลงทะเบียน<br>
									ระยะเวลาการจัดส่ง: 3 - 7 วันทำการ
								<?php }else{ ?>
									+ ค่าจัดส่ง 80 บาท
									<img src="images/ThailandPost_Logo.png" alt="ThailandPost_Logo">
									พัสดุด่วนพิเศษ EMS<br>
									ระยะเวลาการจัดส่ง: 1 - 3 วันทำการ
								<?php } ?>
-->
							</label>
						</div>
						<hr>
						<!-- Grand Total (Total price + Shipping cost) -->
						<div class="d-flex gr-total">
							<div class="ml-auto h5">
								<h5>สรุปยอดทั้งหมด</h5>
								<?php
								/*
									if($_POST['shipping_kind'] == 'normal') {
										echo number_format($price_total+50,2);
									}else{
										echo number_format($price_total+80,2);
									}
								*/

								echo number_format($price_total,2);
								?> บาท
							</div>
						</div>
						<hr>
						<!-- Shipping Address comfirmation -->
						<div class="d-flex">
							<h3>ที่อยู่ในการจัดส่ง</h3>
							<?php
								if($new_address == '') {
									echo $_SESSION['docaddress'];
								}else{
									echo $new_address;
								}
							?>
						</div>
						<hr>
						<!-- Receiver Name comfirmation -->
						<div class="d-flex">
							<h3>ชื่อ-นามสกุล ผู้รับ</h3>
							<?php echo $new_name;	?>
						</div>
						<hr>
						<!-- Receiver Email comfirmation -->
						<?php if(strpos($_SESSION['email'], 'GUEST') !== false) { ?>
						<div class="d-flex">
							<h3>อีเมล</h3>
							<?php echo $contact_email; ?>
						</div>
						<hr>
						<?php } ?>
						<!-- Receiver Phone Number comfirmation -->
						<div class="d-flex">
							<h3>เบอร์ติดต่อ ผู้รับ</h3>
							<?php
								if($new_phonenum == '') {
									echo $_SESSION['phonenum'];
								}else{
									echo $new_phonenum;
								}
							?>
						</div>
					</div>
					<div class="shopping-box container-basket-btn">
						<form action="ตะกร้า.php" method="">
							<button class="btn hvr-hover">ย้อนกลับ</button>
						</form>
						<?php if(strpos($_SESSION['email'], '@') !== false) { ?>
										<form action="addorderA.php" method="POST">
						<?php }elseif(strpos($_SESSION['email'], 'GUEST') !== false) { ?>
										<form action="addorderB.php" method="POST">
						<?php } ?>
											<input type="hidden" name="new_address" value="<?php echo $new_address; ?>">
											<input type="hidden" name="shipping_kind" value="<?php echo $_POST['shipping_kind'];//'Free'; ?>">
											<input type="hidden" name="new_name" value="<?php echo $new_name; ?>">
											<input type="hidden" name="new_phonenum" value="<?php echo $new_phonenum; ?>">
											<input type="hidden" name="contact_email" value="<?php echo $contact_email; ?>">
											<input type="hidden" name="cart_id" value="<?php echo $cart_id; ?>">
											<button type="submit" name="addorder" class="btn hvr-hover">ยืนยันการสั่งซื้อ</button>
										</form>
					</div>
				</div>
			</div>
			<!-- End Order Summary -->
		</div>
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ตรวจสอบตะกร้า -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->


<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>






</body>
</html>