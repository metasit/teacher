<?php
	session_start();
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if(strpos($_SESSION['permis_system'], '*all*') !== false || strpos($_SESSION['permis_system'], '*D00*') !== false || strpos($_SESSION['permis_system'], '*D03*') !== false) {
			
			require_once('condb.php');
			$email = $_SESSION['email'];

		}else{

			header("Location: javascript:history.go(-1);");

		}

	}else{

		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';

	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['win_scroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="ระบบหลังบ้านบริจาค-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom4Aboutus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ระบบหลังบ้านบริจาค</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ระบบหลังบ้านบริจาค -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row" style="margin:0 20px; overflow-x:auto;">
			<div class="col-lg-12">
				<div class="table-main table-responsive" style="margin-bottom:200px">
					<!-- ################################################################################################ -->
					<table class="table" style="background-color:rgb(240,240,240);">
						<thead>
							<tr>
								<th>ลำดับ</th>
								<th>ชื่อ</th>
								<th>วันที่แนบSlip</th>
								<th>เวลา</th>
								<th>สถานะ</th>
								<th>รูปSlip</th>
								<th>อัพเดท</th>
								<th>จำนวนเงิน</th>
								<th>Print</th>
								<th>หมายเหตุ</th>
							</tr>
						</thead>
						<tbody class="disable-margin">
							<?php
								/* Call login table data from mySQL */
								$sqldonate = "SELECT * FROM `donate` WHERE ID!='$ID' ORDER BY donate_date DESC ";
								$reDN = mysqli_query($con, $sqldonate);

								$maxrow = mysqli_num_rows($reDN); // Get number of row

								while($rowDN = $reDN->fetch_assoc()) {

									$donate_ID = $rowDN['ID'];
									$sqllogin = "SELECT * FROM `login` WHERE ID='$donate_ID' ";
									$relogin = mysqli_query($con, $sqllogin);
									$rowlogin = mysqli_fetch_array($relogin); ?>

									<tr>
										<!-- No. -->
										<td class="price-pr">
											<p><?php echo $maxrow; ?></p>
										</td>
										<!-- Name -->
										<td class="name-pr lh-1-0">
											<p>
												<?php
													if($rowlogin['firstname'] == '') {

														echo 'GUEST';

													}else{

														echo $rowlogin['firstname'].'<br>'.$rowlogin['lastname'];

													}
												?>
											</p>
										</td>
										<!-- Donate Date -->
										<td class="price-pr">
											<p><?php echo date("d-m-Y", strtotime($rowDN['donate_date'])); ?></p>
										</td>
										<!-- Donate Time -->
										<td class="price-pr">
											<p><?php echo date("H:i", strtotime($rowDN['donate_date'])); ?></p>
										</td>
										<!-- Status -->
										<td class="name-pr">
											<?php
												if($rowDN['donate_status'] == 'รอตรวจSlip') { ?>

													<p>รอตรวจSlip</p> <?php

												}elseif($rowDN['donate_status'] == 'Approve แล้ว'){ ?>

													<p><i class="fas fa-check-circle"></i><br>Approve แล้ว</p> <?php

												}elseif($rowDN['donate_status'] == 'ปฏิเสธSlip'){ ?>

													<p>ปฏิเสธSlip</p> <?php

												}
											?>
										</td>
										<!-- donate_slip_image -->
										<td class="slip-upload-btn">
											<a href="<?php echo $rowDN['donate_slip_image'];?>" target="_blank">ดูSlip</a>
										</td>
										<!-- Update Status Button -->
										<td>
											<div class="up-stat-sty">
												<button class="up-stat-btn"><i class="fas fa-caret-down"></i></button>
												<div class="dropdown">
													<a onclick="getWinScroll_changeStatusBtn_updatedonate_status('ปฏิเสธSlip', '<?php echo $rowDN['donate_id'];?>')">ปฏิเสธ</a>
													<a onclick="getWinScroll_changeStatusBtn_updatedonate_status('Approve แล้ว', '<?php echo $rowDN['donate_id'];?>')">Approve</a>
												</div>
											</div>
										</td>
										<!-- donate_amount -->
										<form action="adddonate_amount.php" method="POST">
											<td class="order_remark-sty" style="width: 100px;">
												<input type="number" name="donate_amount" class="form-control" style="text-align: center; height: 30px; font-size: 30px;" value="<?php echo $rowDN['donate_amount']; ?>" min="1" required />
												<div class="order-detail-btn">
													<input class="order_remark-submit" type="submit" value="บันทึก">

													<input type="hidden" name="donate_id" value=<?php echo $rowDN['donate_id']; ?>>
												</div>
											</td>
										</form>
										<!-- Printing -->
										<td class="print-btn">
											<?php
												if($rowDN['donate_status'] == 'Approve แล้ว' && $rowDN['donate_amount']) { ?>
												
													<a href="ใบเสร็จA.php?donate_ID=<?php echo $donate_ID;?>&donate_id=<?php echo $rowDN['donate_id'];?>" target="_blank" class="tooltip1"><i class="fas fa-print"><span class="tooltiptext1">พิมพ์ใบเสร็จ</span></i></a> <?php
												
												}else{ ?>
													
													<a href="#" onclick="return false" style="background-color: rgb(206,206,206); pointer-events: none;"><i class="fas fa-print"></i></a> <?php

												}
											?>
										</td>
										<!-- Remark -->
										<form action="adddonate_remark.php" method="POST">
											<td class="order_remark-sty">
												<textarea rows="1" cols="15" name="donate_remark" required><?php echo $rowDN['donate_remark']; ?></textarea>
												<div class="order-detail-btn">
													<input type="hidden" name="donate_id" value=<?php echo $rowDN['donate_id']; ?>>
													<input class="order_remark-submit" type="submit" value="บันทึก">
												</div>
											</td>
										</form>
									</tr> <?php
									$maxrow--;
								}
							?>
							</tbody>
						</table>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ระบบหลังบ้านบริจาค -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<!-- JS for getWinScroll -->
<script src="js/getWinScroll_changeStatusBtn.js"></script>

</body>
</html>