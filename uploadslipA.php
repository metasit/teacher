<?php	
	session_start();
	require_once("condb.php");
	date_default_timezone_set("Asia/Bangkok");

	if($_POST['CFP'] == 10) { // Protection Policy: come from page ส่งหลักฐานการโอนA.php


			$email = $_SESSION['email'];
			$ID = $_SESSION['ID'];
			$firstname = $_SESSION['firstname'];
			$lastname = $_SESSION['lastname'];
			/* Set Values for orders */
			$order_group = $_POST['order_group'];
			$date_pay = $_POST['date_pay'];
			$time_pay = $_POST['time_pay'];
			$pay_date = $date_pay.' '.$time_pay;
			$slip_date = date("Y-m-d H:i:s");
			/* Set Values for bill */
			$sqlsetbillvalue = "SELECT MAX(CAST(SUBSTRING(bill_code,-5) AS UNSIGNED)) AS max_bill_code FROM `bill` WHERE bill_type='IN' ";
			$resbv = $con->query($sqlsetbillvalue);
			$rowsbv = $resbv->fetch_assoc();

			$bill_type = 'IN';
			$slip_year = substr(date('Y',strtotime($slip_date))+543,-2);
			$bill_num = sprintf('%05d',$rowsbv['max_bill_code']+1);
			$bill_code = $bill_type.$slip_year.$bill_num;
			$bill_date = date("Y-m-d H:i:s");
			$bill_tax_kind = $_POST['bill_tax_kind'];
			if($_POST['bill_tax_id_Personal'] != '') {
				$bill_tax_id = $_POST['bill_tax_id_Personal'];
			}elseif($_POST['bill_tax_id_Corporation'] != '') {
				$bill_tax_id = $_POST['bill_tax_id_Corporation'];
			}else{
				$bill_tax_id = $_POST['bill_tax_id_Government'];
			}
			
			$target_dir = 'images/slips/'.$date_pay.'/'; // Set target_directory

			if(!is_dir($target_dir)) { // if there's not folder in target_directory
				mkdir($target_dir); // Create folder name is today_date
			}

			$target_file = $target_dir.basename($_FILES["slip_image"]["name"]); // Save image in the target folder

			if(move_uploaded_file($_FILES["slip_image"]["tmp_name"], $target_file)) {
				/* Save slip location in orders database */
				$sql = "UPDATE `orders` SET `order_status`='กำลังตรวจสอบ',`slip_image`='$target_file',`pay_date`='$pay_date',`slip_date`='$slip_date' WHERE email='$email' AND order_group='$order_group' ";
				$res= $con->query($sql) or die($con->error);
				/* Insert bill in bill database */
				$sqlbill = "INSERT INTO `bill` VALUES ('$ID','$order_group',NULL,'$bill_type','$bill_date','$bill_code','$bill_tax_kind','$bill_tax_id') ";
				$rebill= $con->query($sqlbill) or die($con->error);
				header('location: ประวัติและสถานะการสั่งซื้อ.php');
			}else{
				echo "<script>";
					echo "alert(\"โปรดติดต่อผู้พัฒนาระบบของมูลนิธิครูดีของแผ่นดิน<br>เพื่อดำเนินการแก้ไข<br>ขอบคุณครับ\");";
					echo "window.location.replace('สนับสนุนของที่ระลึก.php')";
				echo "</script>";
			}


	}else{
		echo "<script>window.history.go(-1)</script>";
	}
?>