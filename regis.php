<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="index.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าหลัก</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" name="formlogin" action="checklogin.php" method="POST" id="login">
					<!-- ################################################################################################ -->
					<span class="login100-form-title">
						สมัครสมาชิก
					</span>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="email" name="email" class="form-control" placeholder="อีเมล" required/>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fas fa-user" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="password" class="form-control" placeholder="รหัสผ่าน" required/>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					<!-- ################################################################################################ -->
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit" id="btn">
							เข้าสู่ระบบ
						</button>
					</div>
					<!-- ################################################################################################ -->
					<div class="text-center p-t-12">
						<span class="txt1">
							ลืม
						</span>
						<a class="txt2" href="forgetpassword.php">
							อีเมล / รหัสผ่าน?
						</a>
					</div>
					<!-- ################################################################################################ -->
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>