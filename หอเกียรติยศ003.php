<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
  $sql_list_project  ="SELECT * FROM join_project";
  $list_project = $con->query($sql_list_project);

?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start NoAccount Session -->
<?php
      if (empty($_SESSION['email']))
      {
?>
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
          <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
          <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
          <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
            <ul>
              <li><a href="หอเกียรติยศ003-en.php">English</a></li>
              <!--<li><a href="#">Chinese</a></li>-->
            </ul>
          </li>
          <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
        </ul>
      </nav>
      <!-- End NoAccount Session -->
      <!-- ################################################################################################ -->
      <!-- Start Admin Session -->
<?php
      }
      elseif ($_SESSION['email'])
      {
        if ($_SESSION["level"]=="admin")
        {
?>
        <nav id="mainav2" class="PreMenu_fl_right">
          <ul class="PreMenufaico2">
            <li style="background-color:rgb(228,0,0);"><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ003-en.php">English</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Admin Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member Session -->
<?php
        if ($_SESSION["level"]=="memberGeneral")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> สมาชิกทั่วไป</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ003-en.php">English</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member SILVER Session -->
<?php
        if ($_SESSION["level"]=="memberSilver")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ003-en.php">English</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member SILVER Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member GOLD Session -->
<?php
        if ($_SESSION["level"]=="memberGold")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ003-en.php">English</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member GOLD Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member DIAMOND Session -->
<?php
        if ($_SESSION["level"]=="memberDiamond")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
              <ul>
                <li><a href="หอเกียรติยศ003-en.php">English</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
              <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
          </ul>
        </nav>
<?php
        }
      }
?>
      <!-- End Member DIAMOND Session -->
      <!-- ################################################################################################ -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px; ">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <?php 
                    while($row = $list_project->fetch_assoc()){
                      $id_join_project = $row['id_join_project'];
                      $title_menu = $row['title_menu']; 
                  ?>
                  <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
              <?php } ?>
              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          </li>
          
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="หอเกียรติยศรวมlatest.php">หอเกียรติยศ</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> ทิศทางการการสร้างคนดีให้แผ่นดิน โดยผู้บริหารระดับสูงกระทรวงศึกษาธิการ...</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - หอเกียรติยศ016 -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <article style="text-align:center" >
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px;">
        <strong>ทิศทางการการสร้างคนดีให้แผ่นดิน โดยผู้บริหารระดับสูงกระทรวงศึกษาธิการ<br>วันที่ 14 ธันวาคม 2563 ณ หอประชุมคุรุสภา กรุงเทพมหานคร<br>เนื่องในวาระครบรอบ 2 ปีการก่อตั้งมูลนิธิครูดีของแผ่นดิน</strong>
      </p>
      <!--
      <div class="bgvdo">
        <video width="100%" controls>
          <source src="images/วีดีโอนิสัยการทิ้งขยะ.mp4" type="video/mp4">
        </video>
      </div>
    -->
    </article>
    <p class="font-x2" style="text-align:right">โดย ดร.บูรพาทิศ พลอยสุวรรณ์ รองเลขาธิการคุรุสภา</p>
    <p class="font-x3"><span style="color:rgb(180,147,31); line-height:30px;"><strong>บทความ</strong></span></p>
    <p class="font-x2" style="font-family:ChulaCharasNew; line-height:30pt; text-align:left;">
      กราบเรียนท่านผู้มีเกียรติทุกท่าน ตอนที่ผมได้รับมอบหมายจากท่านเลขาธิการคุรุสภา หรือท่าน ดร.วัฒนาพร ท่านรองเลขาธิการสภาการศึกษา ปัจจุบันปฏิบัติหน้าที่เป็นเลขาธิการคุรุสภา ท่านมีภารกิจสำคัญที่บอกให้ผมมา และได้บอกหัวข้อ วันนี้ทางครูดีของแผ่นดิน อยากให้คุยเสวนาเกี่ยวกับทิศทางการสร้างคนดี
      <br><br>
      เนื่องจากผมในชีวิตนี้เป็นครูเช่นเดียวกัน ได้ยินและเคยคิดเกี่ยวกับเรื่องคนดีมาพอสมควร และคิดว่าไม่แตกต่างจากท่านทั้งหลายในห้องนี้ คือเราต้องการมีคนดีในสังคมไทยและในสังคมโลกมากที่สุด ความเชื่อของผมเองในฐานะที่อยู่ในคุรุสภา ซึ่งเป็นสภาวิชาชีพเป็นองค์กรวิชาชีพครูก็มีความเชื่อจากกฎหมายที่เรียกว่า พระราชบัญญัติสภาครูและบุคลากรทางการศึกษา 2546 จริงๆ มีตั้งแต่ 2488 พูดถึงเรื่องครู คือ มีความเชื่อว่าการที่เราจะได้คนดีให้แก่สังคมนั้น เราต้องมีครูดี ถ้าเราได้ครูดีจะทำให้เกิดคนดี ครูดีสามารถสร้างคนดีได้ แต่คนดีนี้เป็นอย่างไร 
      <br><br>
      ในความคิดเห็นผม คนดีมีได้หลายระดับ ในแต่ละช่วงวัยไม่เหมือนกัน ตั้งแต่วัยเด็ก วัยรุ่น วัยทำงาน วัยผู้ใหญ่ วัยผู้สูงอายุ แต่ละวัยมีธรรมชาติไม่เหมือนกัน แตกต่างกัน เพราะฉะนั้นผมคิดว่าคนดีอาจจะดีได้ตามช่วงวัยของเขา ตามความเหมาะสมของช่วงวัย เพราะฉะนั้น ถ้าเราจะสร้างคนดีก็ต้องให้เหมาะสมกับวัยด้วยเช่นกัน อันนี้คือความเชื่อถัดมาในแง่ของการที่ว่าจะสร้างคนดีอย่างไร ผมยกตัวอย่างเช่น เคสเมื่อเช้าเด็กสองคนนี้ เค้ามีผลงานที่แสดงถึงความดีในวัยของเขา พอเขาโตขึ้นมาอาจจะต้องมีความดีอีกแบบหรือเสริมเพิ่มเติมไปจากนั้น เมื่อเรามองว่าคนดีในแต่ละวัยไม่เหมือนกัน แตกต่างกัน แต่อาจจะมีจุดร่วมกันอยู่ 
      <br><br>
      ทีนี้ผมมองว่า ในฐานะที่อยู่ในวงการศึกษา แล้วคนดีที่เราว่านี้ ควรจะเน้นไปที่ตรงไหน ผมก็เลยมองย้อนกลับไปที่ว่า ในรัฐธรรมนูญของประเทศไทยเราถือว่าเป็นกฎหมายทั้งหมด ได้พูดถึงผู้เรียน ผู้เรียนถือเป็นคนคนหนึ่งในสังคมที่เราต้องการให้เป็นคนดี ในรัฐธรรมนูญเขียนไว้ว่า “การศึกษาทั้งปวงต้องมุ่งให้ผู้เรียน เป็นคนดี มีวินัย ภูมิใจในชาติ สามารถเชี่ยวชาญได้ตามความถนัดของตน และมีความรับผิดชอบต่อตนเองต่อสังคม” ผมเลยคิดว่า นี่คือจุดมุ่งหมายของคนดีในส่วนที่เป็นผู้เรียน ในส่วนอื่นที่ไม่ใช่ผู้เรียนอาจจะมีอีกตามที่กำหนดไว้ในส่วนอื่นๆ 
      <br><br>
      ที่นี้เมื่อมาวิเคราะห์ต่อตัวผู้เรียนว่าต้องมีลักษณะแบบนี้แล้ว คนที่จะทำให้ผู้เรียนเป็นแบบนี้คือใคร ก็คือครู เพราะฉะนั้น ครู จึงต้องมีคุณลักษณะแบบนี้ด้วยเช่นกัน การที่ครูจะสามารถสร้างเด็กหรือผู้เรียนให้เป็นแบบไหนครูต้องเป็นแบบนั้นด้วย อันนี้เป็นความเชื่ออย่างหนึ่ง เพราะฉะนั้น ผมคิดว่า ครู จึงต้องเป็นคนดีด้วย 
      <br><br>
      ที่นี้เมื่อครูเป็นคนดี เรามองครูที่ดีอย่างไร คุรุสภาเองที่เป็นองค์กรวิชาชีพ มองว่าครูที่ดีจะต้องมีสิ่งที่เราเรียกว่าการประพฤติตนหรือการปฏิบัติตน เรียกอีกอย่างหนึ่งว่า จรรยาบรรณของวิชาชีพ มีอยู่ 5 ข้อ 1.จรรณยาบรรณเกี่ยวกับตนเอง ครูต้องมีวินัยต่อตนเอง ต้องมีการประพฤติปฏิบัติต่อตนเอง อันนี้เรียกว่า จรรณยาบรรณต่อตนเอง 2.จรรณยาบรรณต่อผู้เรียน ต้องรัก เมตตา เอาใจใส่ เด็กนักเรียนหรือผู้เรียน 3.จรรณยาบรรณต่อวิชาชีพ ต้องมีความเชื่อมั่นศรัทธาในวิชาชีพ ครูท่านแรกที่ออกมาท่านบอกว่าขับรถกำลังจะกลับบ้านอยากจะลาออก แต่ผมเชื่อว่าสิ่งที่ท่านอยู่จนถึงทุกวันนี้เพราะจิตวิญญาณ จิตใจต้องศรัทธา ต้องเชื่อมั่นในวิชาชีพของความเป็นครู 4.จรรณยาบรรณต่อผู้ร่วมประกอบวิชาชีพ เราทำงาน ไม่ได้ทำงานคนเดียวต้องมีความสามัคคี มีความร่วมมือกัน และ 5.จรรณยาบรรณต่อสังคม ครูต้องมีความรับผิดชอบต่อสังคมเช่นเดียวกับคนอื่นๆ ถ้าครูมีความประพฤติปฏิบัติตรงตามจรรณยาบรรณเหล่านี้แล้ว เชื่อว่าจะเป็นพื้นฐานของครูที่ดี 
      <br><br>
      แล้วเราจะสร้างอย่างไรให้ครูเป็นแบบนี้ ในส่วนของคุรุสภาผมมีบทบาทที่ผ่านมาสองสามบทบาท ทั้งตัวเองเป็นครูด้วย เป็นอาจารย์ที่สอนครูด้วย และมาเป็นผู้บริหารที่อยู่องค์กรวิชาชีพครู ผมคิดว่าการที่ทำให้คนเป็นคนดีได้ต้องร่วมมือกัน เริ่มตั้งแต่ตัวครูเอง ไม่มีใครไปเปลี่ยนแปลงใครได้ ถ้าเกิดว่าบุคคลคนนั้นไม่คิดอยากจะเปลี่ยนแปลงตัวเองก่อน เพราะฉะนั้นต้องเริ่มจากจุดนั้น 
      <br><br>
      ในขณะเดียวกันองค์กรที่เกี่ยวข้องต้องช่วยกัน ในส่วนของคุรุสภาเองผมคิดว่าจะทำหน้าที่ ถ้าครูที่ดีอยู่แล้ว เราจะทำหน้าที่ส่งเสริมสนับสนุน ยกย่อง ผดุงเกียรติครูดีเหล่านั้นให้ดียิ่งขึ้นไป ครูที่อาจจะยังไม่ดีหรือไม่ชัดเจน เราก็กำกับดูแลไม่ให้ทำไม่ดี อันนี้ก็ต้องมีบ้างในมาตรการที่จะกำกับดูแล คุรุสภาเองและองค์กรวิชาชีพจะมีเรื่องใบอนุญาต เพื่อที่จะกำกับไม่ให้ทำไม่ดี
      <br><br>
      การไม่ทำสิ่งที่ไม่ดีเป็นจุดเริ่มต้นของการเป็นคนดี ความเชื่อเหล่านี้โดยสรุป คุรุสภาเองตั้งใจมุ่งมั่นอย่างเต็มที่ ที่จะทำให้วิชาชีพครูเป็นวิชาชีพชั้นสูงอย่างแท้จริง และเป็นวิชาชีพที่ประกอบไปด้วยที่คนดี ครูดี ที่จะนำไปสู่การสร้างสังคมดีทั้งในวันนี้และวันข้างหน้า โดยใช้กระบวนการที่เป็นตามบทบาทหน้าที่ที่จะทำให้เกิดครูดีเหล่านั้น 
      <br><br>
      สถานที่แห่งนี้เป็นสัญลักษณ์หนึ่งของสิ่งที่ดีๆ ที่เกิดขึ้นกับครูด้วยกัน เมื่อปี 2500 ครูทั่วประเทศได้สละเงินเดือนคนละหนึ่งวันได้เงินประมาณหนึ่งล้านบาท สร้างหอประชุมแห่งนี้ โดยที่ไม่ได้ใช้งบประมาณแผ่นดินเลย เป็นเงินที่ครูเสียสละมาช่วยกัน อยู่มาตั้งแต่ พ.ศ.2500 จนถึงปัจจุบัน ผมว่านี่คือจิตวิญญาณส่วนหนึ่ง จริงอยู่การสละเงิน มันอาจจะดูเล็กน้อยไม่มีความหมายอะไร แต่ผมว่าเป็นการแสดงถึงการให้ คนเราถ้ารู้จักให้ ผมว่าเป็นส่วนเริ่มของการที่ทำให้คนเป็นคนดี
      <br><br>
      ประเด็นว่าคุรุสภามีส่วนในการดำเนินงานของมูลนิธิเครือข่ายอย่างไร ผมอยากจะกราบเรียกทุกท่านว่า คุรุสภาเองเห็นว่ามูลนิธิครูดีกับเครือข่ายครูดีเป็นส่วนหนึ่งของคุรุสภาครับ ผมคิดว่าเป็นส่วนที่แสดงถึงจิตวิญญาณของครูที่เป็นรูปธรรมชัดเจน ที่ผมพูดอย่างนี้เพราะเราเห็นภาพกิจกรรมโดยไม่ต้องไปอธิบาย เพราะฉะนั้นเมื่อคุรุสภาเห็นว่าเครือข่ายมูลนิธิเป็นส่วนหนึ่งของคุรุสภา ผมคิดว่าเครือข่ายก็ต้องร่วมมือกันทุกวิถีทาง ที่จะทำอย่างไรให้เจตนารมณ์ของมูลนิธิ เครือข่ายสามารถดำเนินการไปได้โดยบรรลุผล วิธีการคงมีหลากหลายวิธี ซึ่งขึ้นอยู่กับบทบาทหน้าที่ สถานการณ์ แต่สิ่งที่ผมคิดว่าคุรุสภาได้รับมอบหมายตามหน้าที่กฎหมายกำหนด คือ เพื่อมาเป็นตัวแทนผู้ประกอบวิชาชีพ ซึ่งหมายถึงเป็นตัวแทนของมูลนิธิด้วย เครือข่ายด้วย ในการที่จะดำเนินงานที่เกี่ยวกับครู อันนี้คือบทบาทหนึ่ง นอกจากบทบาทที่เป็นตัวแทนในการส่งเสริม สนับสนุน ยกย่องเป็นเกียรติ 
      <br><br>
      กับอีกส่วนหนึ่งที่ผมจะเรียนคือ คุรุสภาโดยบทบาทตามกฎหมายกำหนด คือ เป็นหน่วยที่จะดูแลเรื่องมาตรฐานวิชาชีพคุรุสภา ดูจากมาตรฐานวิชาชีพ สาระสำคัญของวิชาชีพพวกท่านทราบ แต่ทิศทางที่จะเกิดขึ้นในอนาคตอันใกล้ จริงๆ เกิดขึ้นแล้วและต่อเนื่องไปคือการยกระดับวิชาชีพ ให้ครูนอกจากเป็นวิชาชีพที่ได้ยอมรับในสังคมไทยอย่างแท้จริง ผมเชื่อว่าไม่มีอะไรมาแทนครูได้100% เพราะว่าครูยังมีจิตวิญญาณ ผมยังเชื่อว่าครูยังเป็นส่วนสำคัญของสังคม 
      <br><br>
      เพราะฉะนั้นการยกระดับครูให้เป็นที่ยอมรับในสังคมยังไม่พอ ที่ผ่านมาคุรุสภาได้กำหนดมาตรฐานที่เป็นกรอบสมรรถนะวิทยฐานะครูในระดับอาเซียน ตอนนี้ประเทศในอาเซียนมีมาตรฐานวิชาชีพร่วมกัน ที่ครูในอาเซียนเราสามารถแลกเปลี่ยนกัน อยู่ร่วมกันหรือทำงานเชื่อมโยงกันได้ หรือโอนย้ายถ่ายเทกันได้ ซึ่งคิดว่าในอนาคตคงขยายไปสู่ระดับโลก อย่างที่ท่านปะธานมูลนิธิได้พูดว่าโลกเปลี่ยนไปแล้ว 10 ปี ที่อาจจะเปลี่ยนช้า แต่ตอนนี้เปลี่ยนเร็วมาก เพราะฉะนั้นผมครูไทยเราต้องไป คงไม่ได้ดีแค่ระดับสังคมหรือประเทศ แต่ดีในระดับอาเซียนและดีในระดับโลกได้ ผมเชื่อมั่นอย่างนั้นครับ  เครือข่ายมูลนิธิจะเป็นกลไกหนึ่งที่จะทำให้ครูของไทยอยู่ในระดับโลกได้
    </p>
  </main>
</div>
<!-- End Content 01 - หอเกียรติยศ016 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End Copyright Tab -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Back2Top Button -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Back2Top Button -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>