<?php 
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	$CFP = $_GET['CFP'];

	if(isset($ID) && $CFP >= 22) {
		/* Check that this question did? */
		$sqlhonor_score4sup = "SELECT * FROM `honor_score4sup` WHERE ID='$ID' AND honor_score4sup_code='A' ";
		$reHS4S = mysqli_query($con, $sqlhonor_score4sup);
		$rowHS4S = mysqli_fetch_array($reHS4S);

		/* Program calculate the last question was answer */
		$a = strrpos($rowHS4S['honor_score4sup_data1'], ',')+1; // Find the last position of , and add more 1 position to ignore this ,
		$b = strrpos($rowHS4S['honor_score4sup_data1'], '.'); // Find the last position of .
		$c = $b-$a; // Find lenght between them
		$lastquestion = substr($rowHS4S['honor_score4sup_data1'], $a, $c); // Get the last question that finish last time

		if($rowHS4S['honor_score4sup_data1'] != '') {
			if($lastquestion < 22 || $lastquestion == 37) {
				echo "<script>window.history.go(-1)</script>";
			}
		}else{
			echo "<script>window.history.go(-1)</script>";
		}
		
	}else{
		echo "<script>window.history.go(-1)</script>";
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li><a href="all_project.php"> ร่วมโครงการ</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php"> โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการศน.ดีของแผ่นดินขั้นเกียรติคุณ-lobby.php"> ศึกษานิเทศก์ดีของแผ่นดินขั้นเกียรติคุณ</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> ประเมินศึกษานิเทศก์ดีของแผ่นดินขั้นเกียรติคุณ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ประเมินศน.ดีของแผ่นดินขั้นเกียรติคุณ -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<div class="hoc container clear center">
		<!-- ################################################################################################ -->
		<h8>ส่วนต่อไปแบบประเมินครองงาน</h8>
		<br>
		<p class="m-t-50 fs-30 lh-1-3" style="font-family:RSUText; text-align:left;">
			<strong>คำชี้แจง</strong>
			<br>
			ระดับ 1 หมายถึง มีความรู้ ความเข้าใจ ทักษะน้อย ไม่เพียงพอต่อการปฏิบัติงานได้ หรือผลงานไม่มีคุณภาพ
			<br>
			ระดับ 2 หมายถึง มีความรู้ ความเข้าใจ ทักษะปานกลาง สามารถนำไปปฏิบัติงานได้ด้วยตนเอง และมีผลงานที่มีคุณภาพพอใช้
			<br>
			ระดับ 3 หมายถึง มีความรู้ ความเข้าใจ ทักษะดี สามารถนำไปปฏิบัติงานได้ตามมาตรฐานที่กำหนด และมีผลงานที่มีคุณภาพดี
			<br>
			ระดับ 4 หมายถึง มีความรู้ ความเข้าใจ ทักษะดี ถึงดีมาก สามารถนำไปปฏิบัติงานได้เกินกว่ามาตรฐานที่กำหนด มีความคิดสร้างสรรค์ สามารถประยุกต์ใช้ แนะนำผู้อื่นได้ และมีผลงานที่มีคุณภาพดีถึงดีมาก
			<br>
			ระดับ 5 หมายถึง มีความรู้ ความเข้าใจ ทักษะดีมาก สามารถคิดค้นพัฒนานวัตกรรม สร้างองค์ความรู้ใหม่ เผยแพร่ผลงานให้เป็นที่ยอมรับ เป็นแบบอย่างที่ดี และมีผลงานที่มีคุณภาพดีมาก
		</p>
		<!-- ################################################################################################ -->
		<div class="one_first">
			<div class="btnJoin">
				<a href="ประเมินศน.ดีของแผ่นดินขั้นเกียรติคุณ-ครองงาน-ส่วน1.php?CFP=ส่วน0"><h6>ทำประเมินต่อ</h6></a>
			</div>
		</div>
	<!-- ################################################################################################ -->
	</div>
</div>
<!-- End Content 00 - ประเมินศน.ดีของแผ่นดินขั้นเกียรติคุณ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

</body>
</html>