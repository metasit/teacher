<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	

	if(isset($_SESSION['ID'])) {


			/* Case: File input */
			if($_GET['basic_score4stu2_code'] == 'A' || $_GET['basic_score4stu2_code'] == 'C') {
				$basic_score4stu2_code = $_GET['basic_score4stu2_code'];
				$name = $_SESSION['firstname'].' '.$_SESSION['lastname'];
				$target_dir = 'images/ไฟล์เด็กขั้นพื้นฐาน/'.$name.'/'; // Set target_directory
				
				if(!is_dir($target_dir)) { // if there's not folder in target_directory
					mkdir($target_dir); // Create folder name is today_date
				}
				$target_file = $target_dir.date('Y-m-d').' '.$basic_score4stu2_code.' '.basename($_FILES["basic_score4stu2_data"]["name"]); // Save image in the target folder
				if(move_uploaded_file($_FILES["basic_score4stu2_data"]["tmp_name"], $target_file)) {
					$scorelog_detail = 'เด็ก'.','.'ขั้นพื้นฐาน'.','.$basic_score4stu2_code.','.$target_file;
					$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', 'แนบหลักฐานประกอบ', '$scorelog_detail') ";
					$re = $con->query($sql) or die($con->error); //Check error

					$sqlcountrow = "SELECT count(*) AS countrow FROM `basic_score4stu2` WHERE ID='$ID' AND basic_score4stu2_code='$basic_score4stu2_code' ";
					$recountrow = mysqli_query($con,$sqlcountrow);
					$rowcountrow = mysqli_fetch_array($recountrow);
					
					if($rowcountrow['countrow'] != 0) {
						$sql = "UPDATE `basic_score4stu2` SET basic_score4stu2_data='$target_file' WHERE ID='$ID' AND basic_score4stu2_code='$basic_score4stu2_code' ";
						$re = $con->query($sql) or die($con->error); //Check error
					}else{
						$sql = "INSERT INTO `basic_score4stu2` (ID, basic_score4stu2_code, basic_score4stu2_data) VALUES ('$ID', '$basic_score4stu2_code', '$target_file') ";
						$re = $con->query($sql) or die($con->error); //Check error
					}
				}
				echo '<script>';
					echo "alert('แนบไฟล์เรียบร้อยค่ะ');";
					echo "window.location.replace('แนบหลักฐานประกอบการพิจารณา-เด็ก.php')";
				echo '</script>';
					
			/* Case: Do choice evaluation/exam input */
			}elseif($_GET['basic_score4stu2_code'] == 'B') {
				$basic_score4stu2_code = $_GET['basic_score4stu2_code'];

				if($_POST['basic_score1'] == 0 || $_POST['basic_score1'] == 1 || $_POST['basic_score2'] == 0 || $_POST['basic_score2'] == 1 || $_POST['basic_score3'] == 0 || $_POST['basic_score3'] == 1) {
					$basic_score4stu2_data = 'Fail,'.$_POST['basic_score1'].','.$_POST['basic_score2'].','.$_POST['basic_score3'];

					$scorelog_detail = 'เด็ก,ขั้นพื้นฐาน'.','.$basic_score4stu2_code.',ไม่ผ่าน';
					$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', 'แนบหลักฐานประกอบ', '$scorelog_detail') ";
					$re = $con->query($sql) or die($con->error); //Check error

					$sqlcountrow = "SELECT count(*) AS countrow FROM `basic_score4stu2` WHERE ID='$ID' AND basic_score4stu2_code='B' ";
					$recountrow = mysqli_query($con,$sqlcountrow);
					$rowcountrow = mysqli_fetch_array($recountrow);
					
					if($rowcountrow['countrow'] > 0) {
						$sql = "UPDATE `basic_score4stu2` SET basic_score4stu2_data='$basic_score4stu2_data' WHERE ID='$ID' AND basic_score4stu2_code='B' ";
						$re = $con->query($sql) or die($con->error); //Check error
					}else{
						$sql = "INSERT INTO `basic_score4stu2` (ID, basic_score4stu2_code, basic_score4stu2_data) VALUES ('$ID', 'B', '$basic_score4stu2_data') ";
						$re = $con->query($sql) or die($con->error); //Check error
					}
					echo '<script>';
						echo "alert('การประเมินของคุณไม่ผ่าน โอกาสปรับปรุงและพัฒนายังคงมีเสมอ ถ้าตัวเราไม่ท้อซะอย่าง เข้ามาประเมินใหม่ครั้งหน้านะ ^^');";
						echo "window.location.replace('แนบหลักฐานประกอบการพิจารณา-เด็ก.php')";
					echo '</script>';
				}else{
					$basic_score4stu2_data = 'Pass,'.$_POST['basic_score1'].','.$_POST['basic_score2'].','.$_POST['basic_score3'];

					$scorelog_detail = 'เด็ก,ขั้นพื้นฐาน'.','.$basic_score4stu2_code.',ผ่าน';
					$scorelog_total = $_POST['basic_score1']+$_POST['basic_score2']+$_POST['basic_score3'];
					$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail, scorelog_total, scorelog_ans) 
					VALUES ('$ID', 'แนบหลักฐานประกอบ', '$scorelog_detail', '$scorelog_total', '$basic_score4stu2_data') ";
					$re = $con->query($sql) or die($con->error); //Check error

					$sqlcountrow = "SELECT count(*) AS countrow FROM `basic_score4stu2` WHERE ID='$ID' AND basic_score4stu2_code='B' ";
					$recountrow = mysqli_query($con,$sqlcountrow);
					$rowcountrow = mysqli_fetch_array($recountrow);

					if($rowcountrow['countrow'] > 0) {
						$sql = "UPDATE `basic_score4stu2` SET basic_score4stu2_data='$basic_score4stu2_data' WHERE ID='$ID' AND basic_score4stu2_code='B' ";
						$re = $con->query($sql) or die($con->error); //Check error
					}else{
						$sql = "INSERT INTO `basic_score4stu2` (ID, basic_score4stu2_code, basic_score4stu2_data) VALUES ('$ID', 'B', '$basic_score4stu2_data') ";
						$re = $con->query($sql) or die($con->error); //Check error
					}
					echo '<script>';
						echo "alert('เก่งมากค่ะ!! ผ่านประเมินแล้วนะคะ เป็นเด็กเก่งและดี ให้ตัวเอง ให้พ่อแม่ ให้คุณครู และประเทศชาติต่อไปนะ สู้ๆจ้าาา ^^');";
						echo "window.location.replace('แนบหลักฐานประกอบการพิจารณา-เด็ก.php')";
					echo '</script>';
				}
			}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
	
?>