<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($_SESSION['ID'])) {

			$basic_score4stu_system_id = $_POST['basic_score4stu_system_id'];
			$basic_score4stu_file_no = $_POST['basic_score4stu_file_no'];
			$basic_score4stu_text = $_POST['basic_score4stu_text'];

			$scorelog_task = 'แนบไฟล์'.$basic_score4stu_file_no;
			$name = $_SESSION['firstname'].' '.$_SESSION['lastname'];
			
			$basic_score4stu_file_date = $_POST['basic_score4stu_file_date'].' '.$_POST['basic_score4stu_file_time'];
			$createDate = new DateTime($basic_score4stu_file_date);
			$date_do4savefile = $createDate->format('Y-m-d');

			$sqlbasic_score4stu = "SELECT * FROM `basic_score4stu` WHERE ID='$ID' AND `basic_score4stu_file_date` LIKE '%$date_do4savefile%' AND basic_score4stu_file_no!='$basic_score4stu_file_no' ";
			$reBS4S = mysqli_query($con,$sqlbasic_score4stu);
			$rowBS4S = mysqli_fetch_array($reBS4S);
			
			if($rowBS4S['basic_score4stu_id'] != 0) {
				echo '<script>';
					echo "alert('วันที่ทำความดี ไม่สามารถซ้ำกันได้');";
					echo "window.location.replace('แนบไฟล์ทำความดี-เด็ก.php')";
				echo '</script>';
			}else{

				$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', '$scorelog_task', '$scorelog_detail') ";
				$re = $con->query($sql) or die($con->error); //Check error

				$sqlcountrow = "SELECT count(*) AS countrow FROM `basic_score4stu` WHERE ID='$ID' ORDER BY basic_score4stu_file_date ASC ";
				$recountrow = mysqli_query($con,$sqlcountrow);
				$rowcountrow = mysqli_fetch_array($recountrow);

				if($rowcountrow['countrow'] < 14) {
					if($rowcountrow['countrow'] == 0 ) {
						$maxbasic_score4stu_file_no = 0;
					}else{
						$sqlmaxbasic_score4stu_file_no = "SELECT MAX(basic_score4stu_file_no) AS maxbasic_score4stu_file_no FROM `basic_score4stu` WHERE ID='$ID' ";
						$remax = mysqli_query($con, $sqlmaxbasic_score4stu_file_no);
						$rowmax = mysqli_fetch_array($remax);
						$maxbasic_score4stu_file_no = $rowmax['maxbasic_score4stu_file_no'];
					}
				}

				if($maxbasic_score4stu_file_no <= 14) {
					$target_dir = 'images/ไฟล์เด็กขั้นพื้นฐาน/'.$name.'/'; // Set target_directory

					if(!is_dir($target_dir)) { // if there's not folder in target_directory
						mkdir($target_dir); // Create folder name is today_date
					}

					$target_file = $target_dir.$date_do4savefile.basename($_FILES["basic_score4stu_file"]["name"]); // Save image in the target folder
					
					$scorelog_detail = 'เด็ก,ขั้นพื้นฐาน'.','.$basic_score4stu_system_id.','.$target_file;

					if($maxbasic_score4stu_file_no <= $basic_score4stu_file_no) {
						if(move_uploaded_file($_FILES["basic_score4stu_file"]["tmp_name"], $target_file)) {


							$sql = "UPDATE `basic_score4stu` SET basic_score4stu_file='$target_file', basic_score4stu_file_date='$basic_score4stu_file_date' 
							WHERE ID='$ID' AND basic_score4stu_file_no='$basic_score4stu_file_no' ";
							$re = $con->query($sql) or die($con->error); //Check error
						}else{
							echo '<script>';
								echo "alert('Something went wrong. Uploaded file wasn't succeeded.<br>Please contact developer Sornpakorn Sirakarnbandith to take care of');";
								echo "<script>window.history.go(-1)</script>";
							echo '</script>';
						}
						
					}elseif($maxbasic_score4stu_file_no > $basic_score4stu_file_no) {
						if(move_uploaded_file($_FILES["basic_score4stu_file"]["tmp_name"], $target_file)) {
							$sql = "INSERT INTO `basic_score4stu` (ID, basic_score4stu_system_id, basic_score4stu_file_no, basic_score4stu_file, basic_score4stu_file_date, basic_score4stu_text) 
							VALUES ('$ID', '$basic_score4stu_system_id', '$basic_score4stu_file_no', '$target_file', '$basic_score4stu_file_date', '$basic_score4stu_text') ";
							$re = $con->query($sql) or die($con->error); //Check error
						}else{
							echo '<script>';
								echo "alert('Something went wrong. Uploaded file wasn't succeeded.<br>Please contact developer Sornpakorn Sirakarnbandith to take care of');";
								echo "<script>window.history.go(-1)</script>";
							echo '</script>';
						}
						
					}else{
						echo '<script>';
							echo "alert('Something went wrong. error01.<br>Please contact developer Sornpakorn Sirakarnbandith to take care of this issue');";
							echo "window.location.replace('แนบไฟล์ทำความดี-เด็ก.php')";
						echo '</script>';
					}
				}else{
					echo '<script>';
						echo "alert('Something went wrong. file upload number is exceed.<br>Please contact developer Sornpakorn Sirakarnbandith to take care of this issue');";
						echo "window.location.replace('แนบไฟล์ทำความดี-เด็ก.php')";
					echo '</script>';
				}
				
				echo '<script>';
					echo "alert('แนบไฟล์แล้วค่ะ');";
					echo "window.location.replace('แนบไฟล์ทำความดี-เด็ก.php')";
				echo '</script>';

			}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
	
?>