

<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");

	$ID_tea = $_GET['ID_tea'];
	$fifth_score_status = $_GET['fifth_score_status'];
	$winScroll = $_GET['winScroll'];
	$page = $_GET['page'];
	$fifth_score_date = date("Y-m-d H:i:s");

	$ID_ad = $_SESSION['ID'];

	$sqlcheck = "SELECT * FROM `login` WHERE ID='$ID_tea' ";
	$recheck = mysqli_query($con, $sqlcheck);
	$rowcheck = mysqli_fetch_array($recheck);

	if($fifth_score_status == 'ปฏิเสธ') {

		$fifth_score_remark = $rowcheck['fifth_score_remark']; // เหตุผลที่ปฏิเสธ

		if($fifth_score_remark == '') {

			echo '<script>';
				echo "alert('กรุณาใส่เหตุผลที่ปฏิเสธที่ช่องหมายเหตุด้วยค่ะ ข้อความตรงนี้จะไปแสดงให้คุณครูหรือนักเรียนรับทราบเพื่อดำเนินการแก้ไข');";
				echo "window.location.replace('ระบบหลังบ้านครูชั้นที่5.php?winScroll=$winScroll&page=$page')";
			echo '</script>';

		}else{

			/* Update Status */
			$sql = "UPDATE `login` SET fifth_score_status='ปฏิเสธ', fifth_score_date='$fifth_score_date' WHERE ID='$ID_tea' ";
			$re = $con->query($sql) or die($con->error);

			/* Log Admin Action */
			$adscorelog_task = 'ปฏิเสธ';
			$ID_user = $ID_tea;
			$adscorelog_detail = 'ครู,ชั้นที่5,'.$fifth_score_remark;

			$sqllog = "INSERT INTO `adscorelog` (ID_ad, adscorelog_task, ID_user, adscorelog_detail) VALUES ('$ID_ad', '$adscorelog_task', '$ID_user', '$adscorelog_detail') ";
			$relog = $con->query($sqllog) or die($con->error);

			header('location: ระบบหลังบ้านครูชั้นที่5.php?winScroll='.$winScroll.'&page='.$page);

		}

	}else{

		/* Update Status */
		$sql = "UPDATE `login` SET fifth_score_status='Approve แล้ว', fifth_score_date='$fifth_score_date' WHERE ID='$ID_tea' ";
		$res= $con->query($sql) or die($con->error);

		/* Log Admin Action */
		$adscorelog_task = 'Approve แล้ว';
		$ID_user = $ID_tea;
		$adscorelog_detail = 'ครู,ชั้นที่5';

		$sqllog = "INSERT INTO `adscorelog` (ID_ad, adscorelog_task, ID_user, adscorelog_detail) VALUES ('$ID_ad', '$adscorelog_task', '$ID_user', '$adscorelog_detail') ";
		$relog = $con->query($sqllog) or die($con->error);
		
		header('location: ระบบหลังบ้านครูชั้นที่5.php?winScroll='.$winScroll.'&page='.$page);
		
	}
?>