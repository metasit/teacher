<?php 
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($_SESSION['ID'])) {
		$sqllogin = "SELECT * FROM `login` WHERE ID='$ID' ";
		$relogin = mysqli_query($con,$sqllogin);
		$rowlogin = mysqli_fetch_array($relogin);
		
		if($rowlogin['basic_score_status'] == 'ยืนยันแล้ว') {
			date_default_timezone_set("Asia/Bangkok");
		}else{
			echo "<script>window.history.go(-1)</script>"; //Protect policy
		}
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}

	$sqlbasic_score4stu2_codeB = "SELECT * FROM `basic_score4stu2` WHERE ID='$ID' AND basic_score4stu2_code='B' ";
	$reB = mysqli_query($con, $sqlbasic_score4stu2_codeB);
	$rowB = mysqli_fetch_array($reB);
	$check_fail = substr($rowB['basic_score4stu2_data'], 0, 4);
	
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>


<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li><a href="all_project.php"> ร่วมโครงการ</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการเด็กดีของแผ่นดิน.php"> โครงการเด็กดีของแผ่นดิน</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="registZidol.php"> เด็กดีขั้นพื้นฐาน</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">หลักฐานประกอบการพิจารณา</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - แนบหลักฐานประกอบการพิจารณา -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
	<!-- ################################################################################################ -->
	<!-- Start Slip Upload  -->


	<?php 
		
	?>
	<div class="cart-box-main">
    <div class="container">
      <div class="row">
				<!-- ################################################################################################ -->
				<!-- Start Left Content -->
				<div class="col-sm-6 col-lg-6 mb-3">
					<div class="slip-page">
						<div class="title-left">
							<h3>รายละเอียดหลักฐานประกอบการพิจารณา</h3>
						</div>
						<div class="row">
							<div class="col-md-12 mb-3">
								<p>
									<strong>1. เกียรติบัตรหลักสูตร Be Internet Awesome</strong>
									<br>
									เกียรติบัตรนี้ นักเรียนสามารถแนบเกียรติบัตรใด เกียรติบัตรหนึ่งที่ได้จากการเรียนรู้ตามหลักสูตร  Be Internet Awesome ซึ่งมาจาก 
									<a href="https://beinternetawesome.withgoogle.com/th_th" style="color:blue">https://beinternetawesome.withgoogle.com/th_th</a> หรือจากเมนู Be Internet Awesome ในเว็บนี้
								</p>
								<p class="p-t-20" style="border-top:2px solid white;">
									<strong>2. การประเมินคุณสมบัติพื้นฐานเด็กดีของแผ่นดิน</strong>
									<br>
									นักเรียนจะต้องผ่านการประเมินคุณสมบัติพื้นฐานเด็กดีของแผ่นดินตามเกณฑ์ที่กำหนด หากไม่ผ่าน ขอให้พัฒนาตนเอง และจะสามารถมาประเมินตนเองได้อีกครั้งในอีก 14 วันข้างหน้า
								</p>
								<p class="p-t-20" style="border-top:2px solid white;">
									<strong>3. เรียงความ หรือคลิปวิดีโอ เรื่องการเปลี่ยนแปลงของฉัน เมื่อเข้าร่วมโครงการ</strong>
									<br>
									เพื่อให้เห็นการเปลี่ยนแปลงของนักเรียน ว่าก่อนและหลังเข้าร่วมโครงการ นักเรียนมีการเปลี่ยนแปลงในประเด็นที่นักเรียนพัฒนาอย่างไรบ้าง หากเป็นเรียงความ ต้องมีความยาวไม่น้อยกว่า ครึ่งหน้า A4 แต่ไม่เกิน 1 หน้า A4 
									หากเป็นคลิปวิดีโอ ขอเป็นคลิป ที่มีความยาว ไม่เกิน 3 นาที
								</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End Left Content -->
				<!-- ################################################################################################ -->
				<!-- Start Right Content -->
				<div class="col-sm-6 col-lg-6 mb-3">
					<div class="col-md-12 col-lg-12">
						<div class="input-slip-upload">
							<div class="title-left">
								<h3>หลักฐานประกอบการพิจารณา</h3>
							</div>
							<!-- Start form input -->
							<?php
								$sqlbasic_score4stu2_codeA = "SELECT * FROM `basic_score4stu2` WHERE ID='$ID' AND basic_score4stu2_code='A' ";
								$reA = mysqli_query($con, $sqlbasic_score4stu2_codeA);
								$rowA = mysqli_fetch_array($reA);
							?>
							<form action="uploadOthers-เด็ก.php?basic_score4stu2_code=A" method="POST" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-12 mb-3">
										<label for="BIA"><strong>เกียรติบัตรหลักสูตร Be Internet Awesome</strong></label>
									</div>
									<div class="col-md-6 mb-3">
										<input type="file" id="BIA" name="basic_score4stu2_data" class="form-control" required />
									</div>
									<div class="col-md-6 mb-3 m-t-8">
										<?php
											if($rowA['basic_score4stu2_data'] != '') { ?>
												<button class="col-md-12 stu-file-btn" onclick="window.location.href='<?php echo $rowA['basic_score4stu2_data']; ?>'">ดูไฟล์แนบ</button> <?php
											}else{ ?>
												<button class="col-md-12 BG-silver" style="border:1px solid rgb(100,100,100); border-radius:50px;" onclick="return false">รอแนบไฟล์</button> <?php
											}
										?>
									</div>
									<div class="col-md-12 m-t-12">
										<button type="submit" class="col-md-12 stu-up-btn"
											onmousemove="this.style.cursor='pointer'">
											แนบไฟล์ <?php echo $i; ?> (ถ้าทำการแนบไฟล์ซ้ำ จะทำให้ไฟล์เก่าหาย)
										</button>
									</div>
								</div>
							</form>
							<!-- ################################################################################################ -->
							<?php
								// $sqlbasic_score4stu2_codeB = "SELECT * FROM `basic_score4stu2` WHERE ID='$ID' AND basic_score4stu2_code='B' ";
								// $reB = mysqli_query($con, $sqlbasic_score4stu2_codeB);
								// $rowB = mysqli_fetch_array($reB);
								// $check_fail = substr($rowB['basic_score4stu2_data'], 0, 4);
							?>
							<!-- <form action="ประเมินเด็กดีของแผ่นดินขั้นพื้นฐาน.php"> -->
								<div class="row m-t-20 p-t-20" style="border-top:2px solid white;">
									<div class="col-md-12 mb-3">
										<label for="evaluation"><strong>ทำประเมินโครงการเด็กดีขั้นพื้นฐาน</strong></label>
										<div class="col-md-12 mb-3">
										<?php
											if($check_fail == 'Pass') { ?>
												<button class="col-md-12" style="border:1px solid rgb(100,100,100); border-radius:50px;" onclick="return false">ผ่านประเมินแล้วค่ะ</button> <?php
											}else{ ?>
												<!-- <button class="col-md-12 stu-file-btn">เริ่มทำประเมิน</button>  -->
												<?php
											}
										?>
									</div>

										<div class="cart-box-main" style="<?php  if($check_fail !="Pass") {  echo " padding-top:70px!important; padding-bottom:70px!important; ";   }  else { echo " padding-top:0px!important; padding-bottom:0px!important; "; }  ?>" >
		<!-- Start table of my product detail -->
											<div class="row">
												<div class="col-lg-12">
													<div class="table-main  hoc ">
														<!-- ################################################################################################ -->
														<!-- <h2 class="center m-b-50">แบบประเมินคุณสมบัติพื้นฐานของเด็กดีของแผ่นดิน</h2> -->
													<?php if($check_fail !="Pass") {  ?>
														<form action="uploadOthers-เด็ก.php?basic_score4stu2_code=B" method="POST">
															<div class="fs-20">
																<p><strong>1. ปัจจุบันนักเรียน/นักศึกษาดื่มสุรามากน้อยเพียงใด</strong></p>
																
																<div style="display:flex;   align-items: center;  ">
																	<input   
																	style="margin-right:10px;" 
																	type="radio" 
																	id="never" 
																	name="basic_score1" value="3" required>
																	<label   for="never" style="font-size:15px; margin-right:83px;  padding-top:5px; ">ไม่เคยดื่มสุราเลย</label>
																	
																	<input 	style="margin-right:10px;"  type="radio" id="seldom" name="basic_score1" value="2" required>
																	<label for="seldom" style="font-size:15px; padding-top:5px;">เคยดื่มสุรา แต่ปัจจุบันเลิกแล้ว</label>


																</div>




																<div style="display:flex;  justify-content: center;    ">
																	<input style=" margin-top:10px; margin-right:10px;" type="radio" id="sometimes" name="basic_score1" value="1" required>
																	<label for="sometimes" style="font-size:15px; padding-top:5px; margin-right:10px;">ดื่มบ้างเป็นครั้งคราว เพื่อการเข้าสังคม</label>
																
																	<input style=" margin-top:10px; margin-right:10px;" type="radio" id="always" name="basic_score1" value="0" required>
																	<label for="always" style="font-size:15px; padding-top:5px">ดื่มเป็นปกติ เพราะยุคสมัยนี้ ต้องดื่มเป็น</label>
																</div>


																

																<p><strong>2. ปัจจุบันนักเรียน/นักศึกษาสูบบุหรี่มากน้อยเพียงใด</strong></p>
																<div style="display:flex;   align-items: center;  ">
																	<input style="margin-right:10px;"  type="radio" id="never2" name="basic_score2" value="3" required>
																	<label for="never2" style="font-size:15px; margin-right:45px; margin-top:5px; ">ไม่เคยสูบบุหรี่เลย</label>


																	<input style="margin-right:10px;" type="radio" id="seldom3" name="basic_score2" value="2" required>
																	<label for="seldom3" style="font-size:15px;  margin-top:5px; ">เคยสูบบุหรี่ แต่ปัจจุบันเลิกแล้ว</label>


																</div>
<!-- 
																<div style="display:flex;   align-items: center;  ">
																	<input style="margin-right:10px;" type="radio" id="seldom" name="basic_score2" value="2" required>
																	<label for="seldom" style="font-size:15px">เคยสูบบุหรี่ แต่ปัจจุบันเลิกแล้ว</label>
																</div> -->
																



																<div style="display:flex;   align-items: center;  ">
																	
																	<input style="margin-right:10px;"  type="radio" id="sometimes4" name="basic_score2" value="1" required>
																	<label for="sometimes4" style="font-size:15px; margin-right:26px; margin-top:8px; ">สูบบ้างเป็นครั้งคราว</label>

																	<input style="margin-right:10px;" type="radio" id="always5" name="basic_score2" value="0" required>
																	<label for="always5" style="font-size:15px; margin-top:8px; ">สูบเป็นปกติ</label>
																</div>

																<!-- <div style="display:flex;   align-items: center;  ">
																	<input style="margin-right:10px;" type="radio" id="always" name="basic_score2" value="0" required>
																	<label for="always" style="font-size:15px">สูบเป็นปกติ</label>
																</div> -->

																<p><strong>3. ปัจจุบันนักเรียน/นักศึกษายุ่งเกี่ยวกับยาเสพติด หรือการพนันมากน้อยเพียงใด</strong></p>
																<div style="display:flex;   align-items: center;  ">
																	
																	<input style="margin-right:10px;" type="radio" id="never6" name="basic_score3" value="3" required>
																	<label for="never6" style="font-size:15px; margin-right:102px; margin-top:10px; ">ไม่เคยเลย</label>
																	
																	<input style="margin-right:10px;" type="radio" id="seldom7" name="basic_score3" value="2" required>
																	<label for="seldom7" style="font-size:15px; margin-top:10px;">เคยยุ่งเกี่ยว แต่ปัจจุบันเลิกแล้ว</label>


																</div>
																

																<!-- <div style="display:flex;   align-items: center;  ">
																	<input style="margin-right:10px;" type="radio" id="seldom" name="basic_score3" value="2" required>
																	<label for="seldom" style="font-size:15px">เคยยุ่งเกี่ยว แต่ปัจจุบันเลิกแล้ว</label>
																</div> -->


																<div style="display:flex;   align-items: center;  ">
																	<input style="margin-right:10px;" type="radio" id="sometimes8" name="basic_score3" value="1" required>
																	<label for="sometimes8" style="font-size:15px; margin-right:10px; margin-top:10px;  ">ยุ่งเกี่ยวบ้างเป็นครั้งคราว </label>

																	<input style="margin-right:10px;" type="radio" id="always9" name="basic_score3" value="0" required>
																	<label for="always9" style="font-size:15px;  margin-top:10px;">ยุ่งเกี่ยวเป็นปกติ </label>
																</div>

																<!-- <div style="display:flex;   align-items: center;  ">
																	<input style="margin-right:10px;" type="radio" id="always" name="basic_score3" value="0" required>
																	<label for="always" style="font-size:10px;">ยุ่งเกี่ยวเป็นปกติ </label>
																</div> -->

															<button type="submit" class="btnJoin" style="color:white; cursor:pointer; width:80%; margin-top:50px;"><h1>ส่งคะแนนประเมิน</h1></button>

														</form>

														<?php  } ?>
														
														<!-- ################################################################################################ -->
													</div>
												</div>				
											</div>
											<!-- End table of my product detail -->
											<!-- ################################################################################################ -->
										</div>


									</div>

								</div>
							<!-- </form> -->
							<!-- ################################################################################################ -->
							<?php
								$sqlbasic_score4stu2_codeC = "SELECT * FROM `basic_score4stu2` WHERE ID='$ID' AND basic_score4stu2_code='C' ";
								$reC = mysqli_query($con, $sqlbasic_score4stu2_codeC);
								$rowC = mysqli_fetch_array($reC);
							?>
							<form action="uploadOthers-เด็ก.php?basic_score4stu2_code=C" method="POST" enctype="multipart/form-data">
								<div class="row m-t-20 p-t-20" style="border-top:2px solid white;">
									<div class="col-md-12 mb-3">
										<label for="change"><strong>เรียงความ/คลิปวีดีโอ เรื่อง การเปลี่ยนแปลงของฉันเมื่อเข้าร่วมโครงการ</strong></label>
									</div>
									<div class="col-md-6 mb-3">
										<input type="file" id="change" name="basic_score4stu2_data" class="form-control" required />
									</div>
									<div class="col-md-6 mb-3 m-t-8">
										<?php
											if($rowC['basic_score4stu2_data'] != '') { ?>
												<button class="col-md-12 stu-file-btn" onclick="window.location.href='<?php echo $rowC['basic_score4stu2_data']; ?>'">ดูไฟล์แนบ</button> <?php
											}else{ ?>
												<button class="col-md-12 BG-silver" style="border:1px solid rgb(100,100,100); border-radius:50px;" onclick="return false">รอแนบไฟล์</button> <?php
											}
										?>
									</div>
									<div class="col-md-12 m-t-12">
										<button type="submit" class="col-md-12 stu-up-btn"
											onmousemove="this.style.cursor='pointer'">
											แนบไฟล์ <?php echo $i; ?> (ถ้าทำการแนบไฟล์ซ้ำ จะทำให้ไฟล์เก่าหาย)
										</button>
									</div>
								</div>
							</form>
							<!-- End form input -->
						</div>
					</div>
      	</div>
				<!-- End Right Content -->
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>




  <!-- End Slip Upload -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - แนบหลักฐานประกอบการพิจารณา -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<script src="js/pickBillType.js"></script>
<script>


document.querySelector('');


</script>
</body>
</html>