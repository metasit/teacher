<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	//if(isset($_SESSION['ID'])) {
		// Set Occupation_id
		$occupation_id = $_POST['occupation_id'];
		// Set pomain_id
		$pomain_id = $_POST['pomain_id'];
		
		// Set pomain_idans
		$pomain_id_ans1 = $_POST['pomain_id_ansOcB1'].$_POST['pomain_id_ansOcC1'].$_POST['pomain_id_ansOcD1'].$_POST['pomain_id_ansOcE1'].$_POST['pomain_id_ansOcF1'].$_POST['pomain_id_ansOcG1'].$_POST['pomain_id_ansOcO1'].'*';
		$pomain_id_ans2 = $_POST['pomain_id_ansOcB2'].$_POST['pomain_id_ansOcC2'].$_POST['pomain_id_ansOcD2'].$_POST['pomain_id_ansOcE2'].$_POST['pomain_id_ansOcG2'].$_POST['pomain_id_ansOcO2'].'*';
		$pomain_id_ans3 = $_POST['pomain_id_ansOcB3'].$_POST['pomain_id_ansOcC3'].$_POST['pomain_id_ansOcD3'].$_POST['pomain_id_ansOcE3'].$_POST['pomain_id_ansOcG3'].$_POST['pomain_id_ansOcO3'].'*';
		$pomain_id_ans = $pomain_id_ans1.$pomain_id_ans2.$pomain_id_ans3;
		// Set posub1_id
		$posub1_id = $_POST['posub1_id'];
		// Set posub2_id
		$posub2_id = $_POST['posub2_id'];
		
		/* Prepare Occupation value to save to login table */
		if($occupation_id == 'OcA') {

			if($pomain_id == 'O') {

				$occup_code = 'OcAO';
				$occup_remark = $_POST['posub1_id_ans'];

			}elseif($pomain_id == 'A' && $posub1_id == 'B' && $posub2_id == 'O') {

				$occup_code = 'OcAABO';
				$occup_remark = $_POST['posub3_id_ans'].'*';

			}else{

				$occup_code = $occupation_id.$pomain_id.$posub1_id.$posub2_id;
				$occup_remark = $_POST['posub2_id_ans'].$_POST['posub4_id_ans'].$_POST['posub5_id_ans'].$_POST['posub6_id_ans'];

			}

		}elseif($occupation_id == 'OcF') {

			$occup_code = 'OcF'.$pomain_id;

		}else{

			$occup_code = $occupation_id;
			$occup_remark =	$pomain_id_ans;

		}

		include('includes/convert2occtext.php');
		$occup_name = $occupation_name.'*'.$pomain_name.'*'.$posub1_name.'*'.$posub2_name.'*'.$other_insert;

		$_SESSION['occup_code'] = $occup_code;
		$_SESSION['occup_name'] = $occup_name;
		$_SESSION['occup_remark'] = $occup_remark;

		$sqllogin = "UPDATE `login` SET occup_code='$occup_code', occup_name='$occup_name', occup_remark='$occup_remark' WHERE ID='$ID' ";
		$relogin = $con->query($sqllogin) or die($con->error); //Check error

		/* Go to Page for ครู */
		if($_GET['CFP'] == 'popPrintbasic_score_cer_step2OccAffReady-ครู') { // For user who we can get data from Skillmeo

			if($pomain_id == 'A' || $pomain_id == 'B') {

				header('location: popPrintbasic_score_cer_step3OccAffReadyA-ครู.php');

			}else{

				header('location: popPrintbasic_score_cer_step3OccAffReadyB-ครู.php');

			}

		}elseif($_GET['CFP'] == 'popPrintbasic_score_cer_step2-ครู') {

			if($pomain_id == 'A' || $pomain_id == 'B') {

				header('location: popPrintbasic_score_cer_step3A-ครู.php');

			}else{

				header('location: popPrintbasic_score_cer_step3B-ครู.php');

			}


		/* Go to Page for ศน */
		}elseif($_GET['CFP'] == 'popPrintbasic_score_cer_step2OccAffReady-ศน') { // For user who we can get data from Skillmeo

			if($pomain_id == 'A' || $pomain_id == 'B') {

				header('location: popPrintbasic_score_cer_step3OccAffReadyA-ศน.php');

			}else{

				header('location: popPrintbasic_score_cer_step3OccAffReadyB-ศน.php');

			}

		}elseif($_GET['CFP'] == 'popPrintbasic_score_cer_step2-ศน') {

			if($pomain_id == 'A' || $pomain_id == 'B') {

				header('location: popPrintbasic_score_cer_step3A-ศน.php');

			}else{

				header('location: popPrintbasic_score_cer_step3B-ศน.php');

			}


		/* Go to Page for เด็ก */
		}elseif($_GET['CFP'] == 'registZidol_step2') {
			
			header('location: registZidol_step3.php');

		}else{ // For exactly new user

			if($pomain_id == 'A' || $occupation_id == "OcF") {

				header('location: สมัครเข้าร่วมโครงการ-สังกัดA.php');

			}elseif($pomain_id == 'B' || $pomain_id == 'E' || $pomain_id == 'F') {

				header('location: สมัครเข้าร่วมโครงการ-สังกัดC.php');

			}elseif($pomain_id == 'C') {

				if($posub1_id == 'A') {

					header('location: สมัครเข้าร่วมโครงการ-สังกัด4OcACA.php');

				}elseif($posub1_id == 'B') {

					header('location: สมัครเข้าร่วมโครงการ-สังกัด4OcACB.php');

				}elseif($posub1_id == 'C') {

					header('location: สมัครเข้าร่วมโครงการ-สังกัด4OcACC.php');

				}elseif($posub1_id == 'D') {

					header('location: สมัครเข้าร่วมโครงการ-สังกัด4OcACD.php');

				}elseif($posub1_id == 'E') {

					header('location: สมัครเข้าร่วมโครงการ-สังกัด4OcACE.php');

				}elseif($posub1_id == 'F') {

					header('location: สมัครเข้าร่วมโครงการ-สังกัด4OcACF.php');

				}

			}else{

				header('location: สมัครเข้าร่วมโครงการ-สังกัดB.php');

			}

		}
/*
	}else{

		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';

	}
*/
?>