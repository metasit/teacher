<?php 
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
	date_default_timezone_set("Asia/Bangkok");
/*
	if(strpos($_SESSION['email'], '@') !== false) {
		/* Call login table data from mySQL *//*
		$sql = "SELECT * FROM `login` WHERE email='$email' ";
		$re = mysqli_query($con,$sql);
		$row = mysqli_fetch_array($re);

		if(isset($row['basic_score_total'])) {
			if($row['basic_score_total'] >= 23) {
				header('location: แนบหนังสือรับรองขั้นพื้นฐาน-ครู.php');
			}else{
				/* Let set DateTime format before check more than 30 days can do basic_score again *//*
				$last_basic_score_date = new DateTime($row['basic_score_date']);
				$today_basic_score_date = new DateTime('tomorrow');
				$last_basic_score_date = $last_basic_score_date->settime(0,0); // No need time to check
				$today_basic_score_date  = $today_basic_score_date->settime(0,0); // No need time to check
				$diff = date_diff($last_basic_score_date,$today_basic_score_date); // Find interval date between last did basic_score and today basic_score
				$check_basic_score_date = $diff->format("%a");

				if($check_basic_score_date < 30) {
					header('location: popDobasic_score_again-ครู.php');
				}
			}
		}
		
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบ เพื่อทำแบบประเมินคุณสมบัติพื้นฐานครูดีของแผ่นดินได้ค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}*/
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="index-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ประเมินครูดีของแผ่นดินชั้นที่5 -->
<div class="wrapper row3">
	<div class="hoc container clear">
		<!-- ################################################################################################ -->
		<div class="center">
			<h8 class="m-b-50">หมวด: ครองตน</h8>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-6 col-md-6 col-lg-6 fs-20">
				<p>ข้อที่ / ทั้งหมด</p>
				<p class="bold">1 / 79</p>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 right fs-20">
				<p>เหลือเวลา</p>
				<div>
					<p class="bold m-t-1" id="timer">45</p> วินาที
				</div>
			</div>
		</div>
	
		<div class="row p-t-20 p-l-10 m-t-20" style="border-top:2px solid rgb(16,192,0); border-radius:50px;">
			<div class="col-md-12 col-lg-12">
				<form action="uploadOthers-เด็ก.php?basic_score4stu2_code=B" method="POST">
					<div class="fs-20 inline" style="text-align:left;">
						<p><strong>1. ข้อใดตรงกับการปฏิบัติตนในการเอาใจใส่ต่อลูกศิษย์ของท่านมากที่สุด</strong></p>
						<br>
						<input type="radio" id="choice1" name="5_score1" value="1" required>
						<label for="choice1">ดูแลเฉพาะการเรียนในห้องเรียนเท่านั้น</label>
						<br>
						<input type="radio" id="choice2" name="5_score1" value="2" required>
						<label for="choice2">ดูแลการเรียนและความประพฤติเฉพาะในห้องเรียน</label>
						<br>
						<input type="radio" id="choice3" name="5_score1" value="3" required>
						<label for="choice3">ดูแลการเรียนและความประพฤติในห้องเรียนเป็นหลัก และนอกห้องเรียนเท่าที่เวลาเอื้ออำนวย</label>
						<br>
						<input type="radio" id="choice4" name="5_score1" value="4" required>
						<label for="choice4">ดูแลการเรียนและความประพฤติทั้งในห้องเรียนและนอกห้องเรียนอยู่บ่อยครั้ง</label>
						<br>
						<input type="radio" id="choice5" name="5_score1" value="5" required>
						<label for="choice5">ดูแลการเรียนและความประพฤติทั้งในห้องเรียนและนอกห้องเรียน ตลอดจนติดตามความเป็นอยู่ของนักเรียนอย่างสม่ำเสมอ</label>
						<br>
					</div>

					<button type="submit" class="btnJoin" style="color:white; cursor:pointer; width:80%; margin-top:50px;"><h7>ไปข้อถัดไป</h7></button>

				</form>
			</div>
		</div>
	</div>
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ประเมินครูดีของแผ่นดินชั้นที่5 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyrigth -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyrigth -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>
<!-- Countdown Timer -->
<script src="js/countdowntimer.js"></script>

</body>
</html>