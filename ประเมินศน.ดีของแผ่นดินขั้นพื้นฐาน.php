<?php 
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
	date_default_timezone_set("Asia/Bangkok");
	$sql_list_project  ="SELECT * FROM join_project";
	$list_project = $con->query($sql_list_project);


	if(strpos($_SESSION['email'], '@') !== false) {
		/* Call login table data from mySQL */
		$sql = "SELECT * FROM `login` WHERE email='$email' ";
		$re = mysqli_query($con,$sql);
		$row = mysqli_fetch_array($re);

		if(isset($row['basic_score_total'])) {
			if($row['basic_score_total'] >= 23) {
				header('location: แนบหนังสือรับรองขั้นพื้นฐาน-ศน.php');
			}else{
				/* Let set DateTime format before check more than 30 days can do basic_score again */
				$last_basic_score_date = new DateTime($row['basic_score_date']);
				$today_basic_score_date = new DateTime('tomorrow');
				$last_basic_score_date = $last_basic_score_date->settime(0,0); // No need time to check
				$today_basic_score_date  = $today_basic_score_date->settime(0,0); // No need time to check
				$diff = date_diff($last_basic_score_date,$today_basic_score_date); // Find interval date between last did basic_score and today basic_score
				$check_basic_score_date = $diff->format("%a");

				if($check_basic_score_date < 30) {
					header('location: popDobasic_score_again-ศน.php');
				}
			}
		}
		
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบ เพื่อทำแบบประเมินคุณสมบัติพื้นฐานศึกษานิเทศก์ดีของแผ่นดินได้ค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
				<?php if (strpos($_SESSION['email'], '@') !== false) {
								if ($_SESSION["level"]=="admin") { //Admin Session ?>
									<li style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
											<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*A*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*B*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
										</ul>
									</li>
          <?php }elseif ($_SESSION['level']=='memberGeneral') { //General Member Session ?>
								<li style="color:black; background-color:white;"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> สมาชิกทั่วไป</a>
									<ul>
										<li><a href="editprofile.php">แก้ไขข้อมูลส่วนตัว</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberSilver') { //Member Silver Session ?>
								<li style="color:white; background-color:rgb(169,169,169);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</a>
									<ul>
										<li><a href="editprofile.php">แก้ไขข้อมูลส่วนตัว</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberGold') { //Member Gold Session ?>
								<li style="color:white; background-color:rgb(180,147,31);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</a>
									<ul>
										<li><a href="editprofile.php">แก้ไขข้อมูลส่วนตัว</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberDiamond') { //Member Diamond Session ?>
								<li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</a>
									<ul>
										<li><a href="editprofile.php">แก้ไขข้อมูลส่วนตัว</a></li>
									</ul>
								</li>
          <?php } ?>
								<li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
        <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
                <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
				<?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
                  <ul>
                    <li><a href="ประเมินศน.ดีของแผ่นดินขั้นพื้นฐาน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
                    <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึก</a></li>
                  </ul>
                </li>
        </ul>
			</nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li class="active"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
			  <?php 
                while($row = $list_project->fetch_assoc()){
                  $id_join_project = $row['id_join_project'];
                  $title_menu = $row['title_menu']; 
              ?>
              <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
              <?php } ?>
              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li class="active"><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear" style="padding: 0 0 15px;">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li><a href="all_project.php"> ร่วมโครงการ</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php"> โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> ประเมินศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ประเมินศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row">
			<div class="col-lg-12">
				<div class="table-main table-responsive hoc container">
					<!-- ################################################################################################ -->
					<h2 class="center m-b-50">แบบประเมินคุณสมบัติพื้นฐานของศึกษานิเทศก์ดีของแผ่นดิน</h2>
					<form action="addbasic_score-ศน.php" method="POST">
						<table class="table" style="background-color:rgb(240,240,240);">
							<thead>
								<tr>
									<th rowspan="2">ที่</th>
									<th rowspan="2">ประเด็นพิจารณา</th>
									<th colspan="4">ระดับการปฏิบัติ</th>
								</tr>
								<tr>
									<th style="width:15%;">สม่ำเสมอ</th>
									<th style="width:15%;">บางครั้ง</th>
									<th style="width:15%;">นานๆครั้ง</th>
									<th style="width:15%;">ไม่เคยปฏิบัติ</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<!-- No. 1 -->
									<td class="price-pr">
										<p>1</p>
									</td>
									<!-- ประเด็นพิจารณา -->
									<td class="name-pr">
										กริยามารยาท สุภาพ อ่อนน้อม
									</td>
									<!-- ระดับการปฏิบัติ -->
									<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
										<input type="radio" id="always" class="basic_score_sty" name="basic_score1" value="3" style="width:25%;" required>
										<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score1" value="2" style="width:25%" required>
										<input type="radio" id="seldom" class="basic_score_sty" name="basic_score1" value="1" style="width:25%" required>
										<input type="radio" id="never" class="basic_score_sty" name="basic_score1" value="0" style="width:25%" required>
									</td>
								</tr>
								<tr>
									<!-- No. 2 -->
									<td class="price-pr">
										<p>2</p>
									</td>
									<!-- ประเด็นพิจารณา -->
									<td class="name-pr">
										แต่งกายทันสมัยตามแฟชั่น
									</td>
									<!-- ระดับการปฏิบัติ -->
									<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
										<input type="radio" id="always" class="basic_score_sty" name="basic_score2" value="0" style="width:25%;" required>
										<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score2" value="1" style="width:25%" required>
										<input type="radio" id="seldom" class="basic_score_sty" name="basic_score2" value="2" style="width:25%" required>
										<input type="radio" id="never" class="basic_score_sty" name="basic_score2" value="3" style="width:25%" required>
									</td>
								</tr>
								<tr>
									<!-- No. 3 -->
									<td class="price-pr">
										<p>3</p>
									</td>
									<!-- ประเด็นพิจารณา -->
									<td class="name-pr">
										ไม่พูดจาหยาบคาย แม้เวลาโกรธ
									</td>
									<!-- ระดับการปฏิบัติ -->
									<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
										<input type="radio" id="always" class="basic_score_sty" name="basic_score3" value="3" style="width:25%;" required>
										<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score3" value="2" style="width:25%" required>
										<input type="radio" id="seldom" class="basic_score_sty" name="basic_score3" value="1" style="width:25%" required>
										<input type="radio" id="never" class="basic_score_sty" name="basic_score3" value="0" style="width:25%" required>
									</td>
								</tr>
								<tr>
									<!-- No. 4 -->
									<td class="price-pr">
										<p>4</p>
									</td>
									<!-- ประเด็นพิจารณา -->
									<td class="name-pr">
										ใช้ความรุนแรงในการแก้ปัญหา
									</td>
									<!-- ระดับการปฏิบัติ -->
									<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
										<input type="radio" id="always" class="basic_score_sty" name="basic_score4" value="0" style="width:25%;" required>
										<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score4" value="1" style="width:25%" required>
										<input type="radio" id="seldom" class="basic_score_sty" name="basic_score4" value="2" style="width:25%" required>
										<input type="radio" id="never" class="basic_score_sty" name="basic_score4" value="3" style="width:25%" required>
									</td>
								</tr>
								<tr>
									<!-- No. 5 -->
									<td class="price-pr">
										<p>5</p>
									</td>
									<!-- ประเด็นพิจารณา -->
									<td class="name-pr">
										เล่นหวยใต้ดิน-การพนัน
									</td>
									<!-- ระดับการปฏิบัติ -->
									<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
										<input type="radio" id="always" class="basic_score_sty" name="basic_score5" value="0" style="width:25%;" required>
										<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score5" value="1" style="width:25%" required>
										<input type="radio" id="seldom" class="basic_score_sty" name="basic_score5" value="2" style="width:25%" required>
										<input type="radio" id="never" class="basic_score_sty" name="basic_score5" value="3" style="width:25%" required>
									</td>
								</tr>
								<tr>
									<!-- No. 6 -->
									<td class="price-pr">
										<p>6</p>
									</td>
									<!-- ประเด็นพิจารณา -->
									<td class="name-pr">
										ดื่มของมึนเมา/สูบบุหรี่ในที่สาธารณะ
									</td>
									<!-- ระดับการปฏิบัติ -->
									<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
										<input type="radio" id="always" class="basic_score_sty" name="basic_score6" value="0" style="width:25%;" required>
										<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score6" value="1" style="width:25%" required>
										<input type="radio" id="seldom" class="basic_score_sty" name="basic_score6" value="2" style="width:25%" required>
										<input type="radio" id="never" class="basic_score_sty" name="basic_score6" value="3" style="width:25%" required>
									</td>
								</tr>
								<tr>
									<!-- No. 7 -->
									<td class="price-pr">
										<p>7</p>
									</td>
									<!-- ประเด็นพิจารณา -->
									<td class="name-pr">
										นิเทศตรงเวลา-เต็มเวลา
									</td>
									<!-- ระดับการปฏิบัติ -->
									<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
										<input type="radio" id="always" class="basic_score_sty" name="basic_score7" value="3" style="width:25%;" required>
										<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score7" value="2" style="width:25%" required>
										<input type="radio" id="seldom" class="basic_score_sty" name="basic_score7" value="1" style="width:25%" required>
										<input type="radio" id="never" class="basic_score_sty" name="basic_score7" value="0" style="width:25%" required>
									</td>
								</tr>
								<tr>
									<!-- No. 8 -->
									<td class="price-pr">
										<p>8</p>
									</td>
									<!-- ประเด็นพิจารณา -->
									<td class="name-pr">
										เตรียมการนิเทศก่อนการนิเทศ
									</td>
									<!-- ระดับการปฏิบัติ -->
									<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
										<input type="radio" id="always" class="basic_score_sty" name="basic_score8" value="3" style="width:25%;" required>
										<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score8" value="2" style="width:25%" required>
										<input type="radio" id="seldom" class="basic_score_sty" name="basic_score8" value="1" style="width:25%" required>
										<input type="radio" id="never" class="basic_score_sty" name="basic_score8" value="0" style="width:25%" required>
									</td>
								</tr>
								<tr>
									<!-- No. 9 -->
									<td class="price-pr">
										<p>9</p>
									</td>
									<!-- ประเด็นพิจารณา -->
									<td class="name-pr">
										มีเทคนิคการนิเทศที่หลากหลาย
									</td>
									<!-- ระดับการปฏิบัติ -->
									<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
										<input type="radio" id="always" class="basic_score_sty" name="basic_score9" value="3" style="width:25%;" required>
										<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score9" value="2" style="width:25%" required>
										<input type="radio" id="seldom" class="basic_score_sty" name="basic_score9" value="1" style="width:25%" required>
										<input type="radio" id="never" class="basic_score_sty" name="basic_score9" value="0" style="width:25%" required>
									</td>
								</tr>
								<tr>
									<!-- No. 10 -->
									<td class="price-pr">
										<p>10</p>
									</td>
									<!-- ประเด็นพิจารณา -->
									<td class="name-pr">
										ช่วยเหลือผู้อื่น และงานส่วนรวมโดยไม่หวังผลตอบแทน
									</td>
									<!-- ระดับการปฏิบัติ -->
									<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
										<input type="radio" id="always" class="basic_score_sty" name="basic_score10" value="3" style="width:25%;" required>
										<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score10" value="2" style="width:25%" required>
										<input type="radio" id="seldom" class="basic_score_sty" name="basic_score10" value="1" style="width:25%" required>
										<input type="radio" id="never" class="basic_score_sty" name="basic_score10" value="0" style="width:25%" required>
									</td>
								</tr>
								<tr>
									<!-- No. 11 -->
									<td class="price-pr">
										<p>11</p>
									</td>
									<!-- ประเด็นพิจารณา -->
									<td class="name-pr">
										สอดแทรกคุณธรรมในการสอน
									</td>
									<!-- ระดับการปฏิบัติ -->
									<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
										<input type="radio" id="always" class="basic_score_sty" name="basic_score11" value="3" style="width:25%;" required>
										<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score11" value="2" style="width:25%" required>
										<input type="radio" id="seldom" class="basic_score_sty" name="basic_score11" value="1" style="width:25%" required>
										<input type="radio" id="never" class="basic_score_sty" name="basic_score11" value="0" style="width:25%" required>
									</td>
								</tr>
								<tr>
									<!-- No. 12 -->
									<td class="price-pr">
										<p>12</p>
									</td>
									<!-- ประเด็นพิจารณา -->
									<td class="name-pr">
										นิเทศแบบกัลยาณมิตร
									</td>
									<!-- ระดับการปฏิบัติ -->
									<td colspan="4" class="inline" style="text-align:center; font-size:0px;">
										<input type="radio" id="always" class="basic_score_sty" name="basic_score12" value="3" style="width:25%;" required>
										<input type="radio" id="sometimes" class="basic_score_sty" name="basic_score12" value="2" style="width:25%" required>
										<input type="radio" id="seldom" class="basic_score_sty" name="basic_score12" value="1" style="width:25%" required>
										<input type="radio" id="never" class="basic_score_sty" name="basic_score12" value="0" style="width:25%" required>
									</td>
								</tr>
							</tbody>
						</table>
						<p class="fs-20 lh-2-0">
							<strong>ความดีที่ภาคภูมิใจที่สุดในชีวิต</strong> * มีตัวอักษรได้ไม่เกิน1000ตัว
							<input type="text" name="basic_score_text" style="width:100%" maxlength="1000" title="สามารถมีตัวอักษรไม่เกิน1000ตัว"  required>
							<br>
							<strong>เกณฑ์การได้รับรางวัลศึกษานิเทศก์ดีของแผ่นดิน ขั้นพื้นฐาน</strong>
							<br>
							ต้องได้คะแนนรวมไม่น้อยกว่า ร้อยละ 75 คือ ได้ 27 คะแนนจาก 36 คะแนน
							<br>
							และมีเอกสารแนบจากผู้บังคับบัญชาว่า ไม่อยู่ในระหว่างการลงโทษทางวินัยหรือตั้งกรรมการสอบสวนวินัย
						</p>
						<input type="hidden" name="CFP" value="10">
						<button type="submit" class="btnJoin" style="color:white; cursor:pointer; width:80%;"><h1>ส่งคะแนนประเมิน</h1></button>
					</form>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ประเมินศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

</body>
</html>