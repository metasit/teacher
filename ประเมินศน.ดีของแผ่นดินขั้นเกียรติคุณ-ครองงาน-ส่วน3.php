<?php 
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$ID = $_SESSION['ID'];

	$CFP = $_GET['CFP'];

	if(isset($ID) && $CFP == 'ส่วน2') {
		
	}else{
		echo "<script>window.history.go(-1)</script>";
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="m-t-90"></div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ประเมินศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row">
			<div class="col-lg-12">
				<div class="table-main table-responsive hoc container">
					<!-- ################################################################################################ -->
					<div class="center">
						<h8 class="m-b-20">หมวด: ครองงาน</h8>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-6 col-md-6 col-lg-6 fs-20">
							<p>ข้อที่ / ทั้งหมด</p>
							<p class="bold">31-37 / 37</p>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6 right fs-20">
							<p>เหลือเวลา</p>
							<div>
								<p class="bold m-t-1" id="timer" onload="localStorage.getItem('currenttime')">.</p> วินาที
							</div>
						</div>
					</div>

					<form action="addhonor_score_A4work-ศน.php" method="GET">
						<table class="table m-t-20" style="background-color:rgb(240,240,240);">
							<thead>
								<tr>
									<th rowspan="2" style="width:5%;">ข้อ</th>
									<th rowspan="2" style="width:60%;">รายการประเมิน</th>
									<th colspan="5" style="width:35%;">ระดับคุณภาพ</th>
								</tr>
								<tr>
									<th>1</th>
									<th>2</th>
									<th>3</th>
									<th>4</th>
									<th>5</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<!-- No. 31 -->
									<td class="price-pr bold">
										<p>31</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										มีทักษะในการฟังอย่างลึกซึ้ง (Deep Listening)
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore31" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore31" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore31" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore31" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore31" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 32 -->
									<td class="price-pr bold">
										<p>32</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										มีทักษะในการเข้าใจ เห็นอกเห็นใจผู้อื่น (Empathy)
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore32" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore32" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore32" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore32" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore32" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 33 -->
									<td class="price-pr bold">
										<p>33</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										มีทักษะการสื่อสาร สร้างแรงจูงใจ เสริมพลังทีมงานให้ร่วมมือร่วมใจ (Synergy) นำการเปลี่ยนแปลง
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore33" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore33" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore33" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore33" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore33" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 34 -->
									<td class="price-pr bold">
										<p>34</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										มีทักษะการชี้แนะทางปัญญา (Cognitive Coaching) สามารถตั้งคำถามกระตุ้นการคิดให้ครูสามารถหาคำตอบและแนวทางที่เหมาะสมด้วยตนเอง
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore34" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore34" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore34" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore34" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore34" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 35 -->
									<td class="price-pr bold">
										<p>35</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										มีทักษะการชี้แนะทางปัญญา (Cognitive Coaching) สามารถให้ข้อมูลย้อนกลับและคำชี้แนะที่เป็นประโยชน์แก่ครู
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore35" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore35" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore35" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore35" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore35" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 36 -->
									<td class="price-pr bold">
										<p>36</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										สามารถสร้างเครือข่ายความร่วมมือทางการศึกษาอย่างน้อยในระดับพื้นที่
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore36" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore36" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore36" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore36" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore36" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 37 -->
									<td class="price-pr bold">
										<p>37</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										สามารถบริหารตนเอง บริหารคน และบริหารงานได้อย่างสมดุล เหมาะสม บนพื้นฐานความถูกต้องดีงาม
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore37" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore37" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore37" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore37" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore37" value="5" required></td>
								</tr>
							</tbody>
						</table>

						<input type="hidden" name="CFP" value="ส่วน3">
						<button type="submit" id="nextBtn" class="btnJoin" style="color:white; cursor:pointer; width:80%;"><h1>เสร็จสิ้นการทำประเมิน</h1></button>
					</form>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ประเมินศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script>
	// Set the timeout
	if(typeof localStorage.getItem('currenttime') !== 'undefined' && localStorage.getItem('currenttime') > 0) {
		if(localStorage.getItem('currenttime') < 315) {
			var currenttime = localStorage.getItem('currenttime');
		}
	}else{
		var currenttime = 316;
	}

	// Clear currenttime when ปุ่มข้อถัดไป has been selected
	var nextBtn = document.getElementById('nextBtn');
	nextBtn.onclick = function() {
		localStorage.clear('currenttime');
	}
	
	// Update the count down every 1 second
	setInterval(function() {
		if(currenttime > 0) {
			currenttime--;
		}

		localStorage.setItem('currenttime', currenttime);
		
		if(currenttime >= 0) {
			document.getElementById("timer").innerHTML = currenttime;
		}
			
		if(currenttime === 0) {
			window.location.replace('addhonor_score_A4work-ศน.php?CFP=ส่วน3&timeout=yes');
		}

	}, 1000);
</script>

<script>
	history.replaceState(null, null, 'ประเมินศน.ดีของแผ่นดินขั้นเกียรติคุณ-ครองงาน.php');
</script>

</body>
</html>