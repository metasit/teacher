<!-- dashboard for Join Program Name List -->
<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		
		if($_GET['CFP'] == 'dashboardJoinProNameList' || $_GET['CFP'] == 'dashboardJoinProNameScoreList') {

			if($_SESSION['vip_pass_status'] == 0) {

				echo '<script>';
					echo "alert('รหัสผ่านสำหรับการดูระบบนี้ ไม่ถูกต้องค่ะ');";
					echo "window.location.replace('dashboardJoinProNameList.php?CFP=dashboardJoinProNameScoreList')";
				echo '</script>';

			}elseif($_SESSION['vip_pass_status'] == 1) {

				///////////////////////////////////////////////////////////////////////////////////////////////////////
				/* Set program */
				$program = $_GET['program'];
				if($program == 'tea') {
					$program_name = 'โครงการครูดีของแผ่นดิน';
				}
				if($program == 'stu') {
					$program_name = 'โครงการเด็กดีของแผ่นดิน';
				}
				if($program == 'sup') {
					$program_name = 'โครงการศึกษานิเทศดีของแผ่นดิน';
				}
				///////////////////////////////////////////////////////////////////////////////////////////////////////
				/* Set list */
				$list = $_GET['list'];
				if($list == 'all') {
					$list_name = 'สมาชิกทั้งหมด';
				}
				if($list == 'basic_score_pass') {
					$list_name = 'ผ่านขั้นพื้นฐาน';
				}
				if($list == 'fifth_score_proc') {
					$list_name = 'ดำเนินการชั้นที่ 5';
				}
				if($list == 'fifth_score_pass') {
					$list_name = 'ผ่านชั้นที่ 5';
				}
				if($list == 'honor_score_proc') {
					$list_name = 'ดำเนินการขั้นเกียรติคุณ';
				}
				if($list == 'honor_score_pass') {
					$list_name = 'ผ่านขั้นเกียรติคุณ';
				}
				///////////////////////////////////////////////////////////////////////////////////////////////////////
				/* สังกัด ที่ User ต้องการดู */
				$affil_code_filter = $_GET['affil_code_filter'];
				///////////////////////////////////////////////////////////////////////////////////////////////////////

			}else{
				echo "<script>window.history.go(-1)</script>";
			}

		}else{
			echo "<script>window.history.go(-1)</script>";
		}
		
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">

	<!-- UI, Bootstrap 4.5.3 -->
	<link rel="stylesheet" href="css/bootstrap.min453.css" rel="stylesheet" type="text/css" media="all">
	

	<style>
		.affil-menu {
			background-color: rgba(72, 160, 0, 0.1);
			padding: 10px;
			border-radius: 5px;
		}
		.affil-menu-head {
			font-size: 20px;
			text-decoration: underline;
		}
		.affil-menu-head:hover {
			text-decoration: none;
		}
		.modal {
			top: 20%;
		}
	</style>
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['winScroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="index-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="dashboard.php">Dashboard</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li class="linkst">
			<a href="#" onclick="return false">รายชื่อและคะแนนผู้เข้าร่วม
				<?php
					echo $program_name.' - '.$list_name;
				?>
			</a>
		</li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row" style="margin: 0 100px; overflow-x: auto;">
			<section class="col-md-12 fs-40 bold text-gold center" style="line-height: 80px;">
				<?php
					echo $program_name.' - '.$list_name;
				?>
			</section>
			<!-- ################################################################################################ -->
			<!-- Start Downlaod button  -->
			<section class="col-md-12">
				<div class="right">
					<a href="createpdf4dashboardJoinProNameScoreList.php?
					program=<?php echo $program; ?>
					&program_name=<?php echo $program_name; ?>
					&list=<?php echo $list; ?>
					&list_name=<?php echo $list_name; ?>" 
					
					class="btn" target="_blank">ดาวน์โหลดข้อมูลเป็นไฟล์ PDF
				</a>
				</div>
			</section>
			<!-- End Download button -->
			<!-- ################################################################################################ -->
			<div class="col-md-12 m-t-20">
				<div class="table-main table-responsive" style="margin-bottom: 50px">
					<!-- ################################################################################################ -->
					<table class="table" style="background-color: rgb(240, 240, 240);">
						<thead>
							<tr>
								<th>ลำดับ</th>
								<th>ชื่อ-นามสกุล</th>
								<th>สังกัด</th>
								<?php
									if($program == 'tea' && $list == 'all') {
										
									}elseif($program == 'tea' && $list == 'basic_score_pass') { ?>

										<th>คะแนนประเมิน<?php echo '<br>(คะแนนเต็ม 30 คะแนน)'; ?></th> <?php

									}elseif($program == 'tea' && $list == 'fifth_score_proc') { ?>

										<th>คะแนนประเมิน<?php echo '<br>(แต่ละหมวดคะแนนเต็ม 5 คะแนน)<br>ประกอบด้วยหมวดครองตน, ครองคน และครองงาน'; ?></th> <?php

									}elseif($program == 'tea' && $list == 'fifth_score_pass') { ?>

										<th>คะแนนประเมิน<?php echo '<br>(แต่ละหมวดคะแนนเต็ม 5 คะแนน)<br>ประกอบด้วยหมวดครองตน, ครองคน และครองงาน'; ?></th> <?php

									}elseif($program == 'stu' && $list == 'all') {

									}elseif($program == 'stu' && $list == 'basic_score_pass') {

									}elseif($program == 'sup' && $list == 'all') {

									}elseif($program == 'sup' && $list == 'basic_score_pass') { ?>

										<th>คะแนนประเมิน<?php echo '<br>(คะแนนเต็ม 36 คะแนน)'; ?></th> <?php

									}elseif($program == 'sup' && $list == 'honor_score_proc') { ?>

										<th>คะแนนประเมิน<?php echo '<br>(คะแนนเต็ม 36 คะแนน)'; ?></th> <?php

									}elseif($program == 'sup' && $list == 'honor_score_pass') { ?>

										<th>คะแนนประเมิน<?php echo '<br>(คะแนนเต็ม 36 คะแนน)'; ?></th> <?php
													
									}
								?>
							</tr>
						</thead>
						<tbody class="disable-margin">
							<?php
								/* Set number per page */
								$results_per_page = 500;
								if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
								$start_from = ($page-1) * $results_per_page;

								/* Call data from mySQL */
								if($program == 'tea' && $list == 'all') {
									$sql = "SELECT firstname, lastname, affil_code, basic_score_total FROM `login` WHERE (occup_code LIKE 'OcAA%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin') 
									OR (occup_code LIKE 'OcAB%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									OR (occup_code LIKE 'OcAO%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page" ;
									
								}elseif($program == 'tea' && $list == 'basic_score_pass') {
									$sql = "SELECT firstname, lastname, affil_code, basic_score_total FROM `login` WHERE (occup_code LIKE 'OcAA%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									OR (occup_code LIKE 'OcAB%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									OR (occup_code LIKE 'OcAO%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'tea' && $list == 'fifth_score_proc') {
									$sql = "SELECT fifth_score4tea.fifth_score4tea_code, login.affil_code, login.firstname, login.lastname 
									FROM `fifth_score4tea` 
									INNER JOIN `login` ON fifth_score4tea.ID=login.ID 
									WHERE fifth_score4tea.fifth_score4tea_code='A' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin'
									ORDER BY login.firstname, login.lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'tea' && $list == 'fifth_score_pass') {
									$sql = "SELECT fifth_score4tea.fifth_score4tea_detail, login.affil_code, login.firstname, login.lastname 
									FROM `fifth_score4tea` 
									INNER JOIN `login` ON fifth_score4tea.ID=login.ID 
									WHERE (login.occup_code LIKE 'OcAA%' AND login.fifth_score_status LIKE '%Approve แล้ว%' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin')
									OR (login.occup_code LIKE 'OcAB%' AND login.fifth_score_status LIKE '%Approve แล้ว%' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin')
									ORDER BY login.firstname, login.lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'stu' && $list == 'all') {
									$sql = "SELECT firstname, lastname, affil_code FROM `login` WHERE occup_code LIKE 'OcF%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' 
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'stu' && $list == 'basic_score_pass') {
									$sql = "SELECT firstname, lastname, affil_code FROM `login` 
									WHERE occup_code LIKE 'OcF%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' 
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'sup' && $list == 'all') {
									$sql = "SELECT firstname, lastname, affil_code FROM `login` WHERE occup_code LIKE 'OcAD%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin'
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'sup' && $list == 'basic_score_pass') {
									$sql = "SELECT firstname, lastname, affil_code, basic_score_total FROM `login` WHERE occup_code LIKE 'OcAD%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin'
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'sup' && $list == 'honor_score_proc') {
									$sql = "SELECT honor_score4sup.honor_score4sup_code, login.firstname, login.lastname, login.affil_code 
									FROM `honor_score4sup` 
									INNER JOIN `login` ON honor_score4sup.ID=login.ID
									WHERE honor_score4sup.honor_score4sup_code='A' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin'
									ORDER BY login.firstname, login.lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'sup' && $list == 'honor_score_pass') {
									$sql = "SELECT honor_score4sup.honor_score4sup_data2, login.firstname, login.lastname, login.affil_code 
									FROM `honor_score4sup` 
									INNER JOIN `login` ON honor_score4sup.ID=login.ID
									WHERE login.occup_code LIKE 'OcAD%' AND login.honor_score_status LIKE '%Approve แล้ว%' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin' 
									ORDER BY login.firstname, login.lastname LIMIT $start_from, $results_per_page ";
												
								}

								$re = mysqli_query($con, $sql);

								$n = $start_from+1; // Get number of row
								while($row = $re->fetch_assoc()) {
									
									$name = trim($row['firstname']).' '.trim($row['lastname']);
									
									$affil_code = $row['affil_code'];
									include('includes/convert2afftext.php'); ?>

									<tr>
										<!-- No. -->
										<td class="price-pr">
											<p><?php echo $n ?></p>
										</td>
										<!-- Name -->
										<td class="name-pr lh-1-0">
											<p><?php echo $name; ?></p>
										</td>
										<!-- สังกัด-->
										<td class="name-pr lh-1-0">
											<p><?php echo $full_affiliation_name; ?></p>
										</td>
										<!-- คะแนน-->
										<?php
											if($program == 'tea' && $list == 'all') {
												
											}elseif($program == 'tea' && $list == 'basic_score_pass') { ?>

												<td class="price-pr">
													<p><?php echo $row['basic_score_total']; ?></p>
												</td> <?php

											}elseif($program == 'tea' && $list == 'fifth_score_proc') { ?>

												<td class="price-pr">
													<p><?php if($row['fifth_score4tea_detail'] == ''){echo 'กำลังดำเนินการ';}else{echo $row['fifth_score4tea_detail'];} ?></p>
												</td> <?php

											}elseif($program == 'tea' && $list == 'fifth_score_pass') { ?>

												<td class="price-pr">
													<p><?php echo str_replace(',', ' ', substr($row['fifth_score4tea_detail'], 1)); ?></p>
												</td> <?php

											}elseif($program == 'stu' && $list == 'all') {

											}elseif($program == 'stu' && $list == 'basic_score_pass') {

											}elseif($program == 'sup' && $list == 'all') {

											}elseif($program == 'sup' && $list == 'basic_score_pass') { ?>

												<td class="price-pr">
													<p><?php echo $row['basic_score_total']; ?></p>
												</td> <?php

											}elseif($program == 'sup' && $list == 'honor_score_proc') { ?>

												<td class="price-pr">
													<p><?php if($row['honor_score4sup_data2'] == ''){echo 'กำลังดำเนินการ';}else{echo $row['honor_score4sup_data2'];} ?></p>
												</td> <?php

											}elseif($program == 'sup' && $list == 'honor_score_pass') { ?>

												<td class="price-pr">
													<p><?php echo str_replace(',', ' ', substr($row['honor_score4sup_data2'], 1)); ?></p>
												</td> <?php
															
											}
										?>
									</tr> <?php
									$n++;
									$selfscore = 0;
									$peoplescore = 0;
									$workscore = 0;
									$k = 0;
								}
							?>
						</tbody>
					</table>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 -->
<?php
					if($program == 'tea' && $list == 'all') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE (occup_code LIKE 'OcAA%' AND affil_code LIKE '{$affil_code_filter}%') 
						OR (occup_code LIKE 'OcAB%' AND affil_code LIKE '{$affil_code_filter}%') ";
						
					}elseif($program == 'tea' && $list == 'basic_score_pass') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE (occup_code LIKE 'OcAA%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
						OR (occup_code LIKE 'OcAB%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin') ";

					}elseif($program == 'tea' && $list == 'fifth_score_proc') {
						$sql = "SELECT COUNT(login.ID) AS total FROM `fifth_score4tea` INNER JOIN `login` ON fifth_score4tea.ID=login.ID 
						WHERE fifth_score4tea.fifth_score4tea_code='A' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin' ";

					}elseif($program == 'tea' && $list == 'fifth_score_pass') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE (occup_code LIKE 'OcAA%' AND fifth_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
						OR (occup_code LIKE 'OcAB%' AND fifth_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin') ";

					}elseif($program == 'stu' && $list == 'all') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE occup_code LIKE 'OcF%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";

					}elseif($program == 'stu' && $list == 'basic_score_pass') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` 
						WHERE occup_code LIKE 'OcF%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";

					}elseif($program == 'sup' && $list == 'all') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE occup_code LIKE 'OcAD%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";

					}elseif($program == 'sup' && $list == 'basic_score_pass') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE occup_code LIKE 'OcAD%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";

					}elseif($program == 'sup' && $list == 'honor_score_proc') {
						$sql = "SELECT COUNT(login.ID) AS total FROM `honor_score4sup` INNER JOIN `login` ON honor_score4sup.ID=login.ID
						WHERE honor_score4sup.honor_score4sup_code='A' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin' ";

					}elseif($program == 'sup' && $list == 'honor_score_proc') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE occup_code LIKE 'OcAD%' AND honor_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";
									
					}
					$re = $con->query($sql);
					$row = $re->fetch_assoc();
					$total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results
?>
            <div class="wrapper row3 coloured" style="background-color:rgb(226,255,224);">
              <main class="hoc container clear">
                <div class="content">
                  <nav class="pagination">
                    <ul>
<?php
                        for ($n=1; $n<=$total_pages; $n++) {  // print links for all pages
                          if ($n==$page)
                          {
                            echo "<li class='current'>";
                          }else{
                            echo "<li>";
                          }
                          echo "<a href='dashboardJoinProNameList.php?page=".$n."&CFP=dashboard&program=".$program."&list=".$list."&affil_code_filter=".$affil_code_filter." ' ";
                          if ($n==$page)  echo " class='curPage'";
                          echo ">".$n."</a> ";
                        }; 
?>
                      </li>
                    </ul>
                  </nav>
                </div>
              </main>
            </div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script>
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})
</script>

</body>
</html>