<?php
	session_start();

	if(isset($_SESSION['ID'])) {

		if(strpos($_SESSION['permis_system'], '*all*') !== false || strpos($_SESSION['permis_system'], '*D00*') !== false || strpos($_SESSION['permis_system'], '*D02*') !== false) {
			
			require_once('condb.php');
			$ID = $_SESSION['ID'];

		}else{

			header("Location: javascript:history.go(-1);");

		}

	}else{

		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';

	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['win_scroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="ระบบหลังบ้านบำรุงสมาชิก-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ระบบหลังบ้านบำรุงสมาชิก</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ระบบหลังบ้าน -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row" style="margin:0 20px; overflow-x:auto;">
			<div class="col-lg-12">
				<div class="table-main table-responsive" style="margin-bottom:200px">
					<!-- ################################################################################################ -->
					<table class="table" style="background-color:rgb(240,240,240);">
						<thead>
							<tr>
								<th>ลำดับ</th>
								<th>ชื่อ</th>
								<th>วันที่โอน</th>
								<th>เวลา</th>
								<th>ปรับระดับเป็น</th>
								<th>สถานะ</th>
								<th>รูปSlip</th>
								<th>อัพเดท</th>
								<th>Print</th>
								<th>หมายเหตุ</th>
							</tr>
						</thead>
						<tbody class="disable-margin">
							<?php
								$sqlmembership = "SELECT * FROM `membership` WHERE ID!='$ID' ";
								$reMS = mysqli_query($con, $sqlmembership);
								
								$i=1;
								while($rowMS = $reMS->fetch_assoc()) {
									
									$membership_ID = $rowMS['ID'];
									$sqllogin = "SELECT * FROM `login` WHERE ID='$membership_ID' ";
									$relogin = mysqli_query($con, $sqllogin);
									$rowlogin = mysqli_fetch_array($relogin); ?>
									<tr>
										<!-- No. -->
										<td class="price-pr">
											<p><?php echo $i; ?></p>
										</td>
										<!-- Member Name -->
										<?php
											if($rowlogin['level'] == 'admin') { ?>

												<td class="name-pr lh-1-0" style="background-color:rgb(228,0,0);">
													<p style="color:white;"><?php echo $rowlogin['firstname'].' '.$rowlogin['lastname']; ?></p>
												</td> <?php

											}elseif($rowlogin['level'] == 'memberGeneral') { ?>

												<td class="name-pr lh-1-0" style="background-color:white;">
													<p><?php echo $rowlogin['firstname'].' '.$rowlogin['lastname']; ?></p>
												</td> <?php
											
											}elseif($rowlogin['level'] == 'memberSilver') { ?>

												<td class="name-pr lh-1-0" style="background-color:rgb(169,169,169);">
													<p style="color:white;"><?php echo $rowlogin['firstname'].' '.$rowlogin['lastname']; ?></p>
												</td>	<?php

											}elseif($rowlogin['level'] == 'memberGold') { ?>

												<td class="name-pr lh-1-0" style="background-color:rgb(180,147,31);">
													<p style="color:white;"><?php echo $rowlogin['firstname'].' '.$rowlogin['lastname']; ?></p>
												</td> <?php

											}elseif($rowlogin['level'] == 'memberDiamond') { ?>

												<td class="name-pr lh-1-0" style="background-color:rgb(52,52,53);">
													<p style="color:white;"><?php echo $rowlogin['firstname'].' '.$rowlogin['lastname']; ?></p>
												</td> <?php
											
											}else{ ?>

												<td class="name-pr lh-1-0">
													<p>มีบางอย่างผิดพลาด ติดต่อ Developer</p>
												</td> <?php

											}
										?>
										<!-- Date -->
										<td class="price-pr">
											<p><?php echo date("d-m-Y", strtotime($rowMS['membership_date'])); ?></p>
										</td>
										<!-- Time -->
										<td class="price-pr">
											<p><?php echo date("H:i", strtotime($rowMS['membership_date'])); ?></p>
										</td>
										<!-- ปรับระดับเป็น -->
										<?php
											if($rowMS['membership_topic'] == 'memberSilver') { ?>

												<td class="name-pr lh-1-0" style="background-color:rgb(169,169,169);">
													<p style="color:white;"><?php echo $rowMS['membership_topic']; ?></p>
												</td>	<?php

											}elseif($rowMS['membership_topic'] == 'memberGold') { ?>
											
												<td class="name-pr lh-1-0" style="background-color:rgb(180,147,31);">
													<p style="color:white;"><?php echo $rowMS['membership_topic']; ?></p>
												</td> <?php

											}else{ ?>
													
												<td class="name-pr lh-1-0">
													<p>มีบางอย่างผิดพลาด<br>ติดต่อ Developer</p>
												</td> <?php

											}
										?>
										<!-- Status -->
										<td class="name-pr">
											<?php
												if($rowMS['membership_status'] == 'Approve แล้ว') {?>
													
													<p><i class="fas fa-check-circle"></i><br><?php echo $rowMS['membership_status']; ?></p> <?php
												
												}else{ ?>

													<p><?php echo $rowMS['membership_status']; ?></p> <?php

												}
											?>
										</td>
										<!-- Slip Upload -->
										<td class="slip-upload-btn">
											<?php
												if(is_null($rowMS['membership_slip_image']) || empty($rowMS['membership_slip_image'])) { ?>

													<a href="#" style="background-color:transparent; color:black; font-weight:700; border:none;" onclick="return false">รอหลักฐานการโอน</a> <?php
												
												}else{ ?>
													
													<a href="<?php echo $rowMS['membership_slip_image'];?>" target="_blank">ดูหลักฐานการโอน</a> <?php

												}
											?>
										</td>
										<!-- Update Status Button -->
										<td>
											<div class="up-stat-sty">
												<button class="up-stat-btn"><i class="fas fa-caret-down"></i></button>
												<div class="dropdown">
													<a onclick="getWinScroll_changeStatusBtn_updatemembership_status('memberSilver', '<?php echo $rowMS['ID'];?>', '<?php echo $rowMS['membership_id'];?>')">ปรับเป็นระดับเงิน</a>
													<a onclick="getWinScroll_changeStatusBtn_updatemembership_status('memberGold', '<?php echo $rowMS['ID'];?>', '<?php echo $rowMS['membership_id'];?>')">ปรับเป็นระดับทอง</a>
													<a onclick="getWinScroll_changeStatusBtn_updatemembership_status('memberDiamond', '<?php echo $rowMS['ID'];?>', '<?php echo $rowMS['membership_id'];?>')">ปรับเป็นระดับเพชร</a>
													<a onclick="getWinScroll_changeStatusBtn_updatemembership_status('ปฏิเสธSlip', '<?php echo $rowMS['ID'];?>', '<?php echo $rowMS['membership_id'];?>')">ปฏิเสธ</a>
												</div>
											</div>
										</td>
										<!-- Printing -->
										<td class="print-btn">
											<?php
												if($rowMS['membership_status'] == 'Approve แล้ว') { ?>

													<a href="ใบเสร็จB.php?membership_ID=<?php echo $rowMS['ID'];?>&membership_id=<?php echo $rowMS['membership_id'];?>" target="_blank" class="tooltip1"><i class="fas fa-print"><span class="tooltiptext1">พิมพ์ใบเสร็จ</span></i></a> <?php
												
												}else{ ?>
													
													<a href="#" onclick="return false" style="background-color: rgb(206,206,206); pointer-events: none;"><i class="fas fa-print"></i></a> <?php

												}
											?>
										</td>
										<!-- Remark -->
										<form action="addmembership_remark.php" method="POST">
											<td class="order_remark-sty">
												<textarea rows="1" cols="15" name="membership_remark"><?php echo $rowMS['membership_remark']; ?></textarea>
												<div class="order-detail-btn">
													<input class="order_remark-submit" type="submit" value="บันทึก">

													<input type="hidden" name="membership_id" value="<?php echo $rowMS['membership_id']; ?>">
												</div>
											</td>
										</form>
									</tr> <?php
									$i++;
								} 
							?>
						</tbody>
					</table>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ระบบหลังบ้าน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<!-- JS for getWinScroll -->
<script src="js/getWinScroll_changeStatusBtn.js"></script>

</body>
</html>