<?php
	session_start();
	require_once('condb.php');
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" id="Affform" action="addAff.php?CFP=popPrintbasic_score_cer_step3B-ศน" method="POST" enctype="multipart/form-data">
					<!-- ################################################################################################ -->
					<span class="login100-form-title">
						สมัครเข้าร่วมโครงการ
					</span>
					<!-- ################################################################################################ -->
					<!-- Start show step -->
					<div class="center">
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
						<span class="line BG-green1" style="width:25%"></span>
						<span class="dot BG-green1"></span>
						<span class="line" style="width:25%"></span>
						<span class="dot"></span>
					</div>
					<div class="center">
						<p style="color:rgb(94,177,26);">ขั้นตอนที่ 3: ยืนยันหรือแก้ไขสังกัด</p>
					</div>
					<!-- End show step -->
					<!-- ################################################################################################ -->
					<!-- Start สังกัด Content -->
					<!-- เลือกสังกัด -->
					<label for="affiliation_id" class="center m-t-10 text-black bold">สังกัด</label>
					<select name="affiliation_id" id="affiliation_id" class="form-control" style="height:40px;" required>
						<option value="" disabled="disabled" selected="selected">เลือกสังกัด</option>
						<option value="AfA">สำนักงานปลัดกระทรวงศึกษาธิการ</option>
						<option value="AfB">สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน</option>
						<option value="AfC">สำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน</option>
						<option value="AfD">สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย</option>
						<option value="AfE">สำนักงานคณะกรรมการการอาชีวศึกษา</option>
						<option value="AfF">สำนักงานคณะกรรมการการอุดมศึกษา</option>
						<option value="AfG">กระทรวงมหาดไทย กรมส่งเสริมการปกครองส่วนท้องถิ่น</option>
						<option value="AfH">กรุงเทพมหานคร</option>
						<option value="AfI">เมืองพัทยา</option>
						<option value="AfJ">สำนักงานตำรวจแห่งชาติ</option>
						<option value="AfO">อื่นๆ โปรดระบุ....</option>
					</select>
					<label for="affsub_id" id="affsubhead" class="center m-t-10" style="display:none">สังกัดย่อย</label>
					<select name="affsub_id" id="affsub_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
					</select>
					<div id="affsub_text" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="affsub_ans_id" id="affsub_ans_id" type="text" class="form-control" placeholder="โปรดระบุสังกัด" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<select name="affsub2_id" id="affsub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
					</select>
					<select name="affsub3_id" id="affsub3_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
					</select>
					<select name="affsub4_id" id="affsub4_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
					</select>
					<div id="affsub4_text" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="affsub4_ans_id" id="affsub4_ans_id" type="text" class="form-control" placeholder="โปรดระบุสังกัด" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<!-- End สังกัด Content -->
					<div class="container-login100-form-btn m-t-20">
						<a href="สมัครเข้าร่วมโครงการ-ตำแหน่ง.php" class="login100-form-btn" style="width:30%; margin:0 10px;">ย้อนกลับ</a>
						<button class="login100-form-btn" style="width:40%; margin:0 10px;" type="submit" id="register">ยืนยัน</button>
					</div>
					<!-- ################################################################################################ -->
				</form>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>

<!-- JS for multi-sub-dropdown -->
<script src="js/jquery.min.js"></script>
<script src="js/projectRegist4affB.js" type="text/javascript"></script>

</body>
</html>