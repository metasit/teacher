<?php 
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		date_default_timezone_set("Asia/Bangkok");

		$sqllogin = "SELECT * FROM `login` WHERE ID='$ID' ";
		$relogin = mysqli_query($con, $sqllogin);
		$rowlogin = mysqli_fetch_array($relogin);

		$fifth_score_status = $rowlogin['fifth_score_status'];
		/* เช็คว่า ได้ขั้นพื้นฐานมาเมื่อไหร่ fifth_score4tea_part1 */
		// เรียก basic_score_remark เพื่อดูว่า เป็นกลุ่ม Fast-Track หรือไม่
		$basic_score_remark = $rowlogin['basic_score_remark'];

		// เรียก adscorelog เพื่อดูว่า ได้รับครูดีขั้นพื้นฐานมาเมื่อไหร่
		$sqladscorelog = "SELECT * FROM `adscorelog` WHERE ID_user='$ID' AND adscorelog_task='Approve แล้ว' AND adscorelog_detail='ขั้นพื้นฐาน' ORDER BY adscorelog_id DESC LIMIT 1 ";
		$readscorelog = mysqli_query($con,$sqladscorelog);
		$rowadscorelog = mysqli_fetch_array($readscorelog);

		$check_basic_score = mysqli_num_rows($readscorelog);
		if($check_basic_score == 0) { // เช็คว่า มีการ Approve ไปแล้วที่ adscorelog table ไปรึยัง
			if($basic_score_remark == 'Fast-Track') { // ถ้าไม่มี ให้เช็คอีกว่า เป็นกลุ่ม Fast-Track หรือไม่
				$basic_score_date = 'Fast-Track';
				$basic_score_status = 'Fast-Track';
				$expire_basic_score_date = 'Fast-Track';
			}else{ // ถ้าไม่มี แสดงว่า ไม่เคยได้รับการ Approve ครูขั้นพื้นฐานมาเลย
				$basic_score_date = 'ไม่พบข้อมูลในระบบ';
				$basic_score_status = 'ไม่พบข้อมูลในระบบ';
				$expire_basic_score_date = 'ไม่พบข้อมูลในระบบ';
			}
		}else{
			/* Find and Set the latest basic_score_date */
			$date = new DateTime($rowadscorelog['adscorelog_date']);
			$basic_score_date = $date->format("d-m-Y"); // Set date format
			/* Set the expire_basic_score_date */
			$expire_basic_score_date = $date->add(new DateInterval('P1Y1D')); // Find expire date for basic_score		
			/* Check that this user basic_score expire? and Set $basic_score_status */
			$date_today = new DateTime('today');
			if($expire_basic_score_date >= $date_today) {
				$basic_score_status = 'ได้รับการอนุมัติ'; // Set basic_score_status to Approved
			}else{
				$basic_score_status = 'หมดอายุ';
			}
			$expire_basic_score_date = $date->format("d-m-Y"); // Set date format
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/* เช็คว่า จำนวนนร.ที่ดูแล ผ่านเกณฑ์หรือไม่ fifth_score4tea_part2 */
		// จำนวนนร.ที่ครูคนนี้ดูแลทั้งหมด
		$sqlfifth_score4tea_codeB = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='B' ";
		$reB = mysqli_query($con, $sqlfifth_score4tea_codeB);
		$rowB = mysqli_fetch_array($reB);
		$stu_num = $rowB['fifth_score4tea_data'];

		// จำนวนนร.ขั้นพื้นฐาน ของครูคนนี้ทั้งหมดที่ได้รับการ Approve จากมูลนิธิแล้ว
		$affil_code = $_SESSION['affil_code'];
		$sqlcountstupass = "SELECT `basic_score_status` FROM `login` WHERE affil_code='$affil_code' AND basic_score_total='$ID' AND basic_score_status='ยืนยันแล้ว/Approve แล้ว' AND ID!='$ID' ";
		$recountstupass = mysqli_query($con, $sqlcountstupass);
		$countstupass = mysqli_num_rows($recountstupass);
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/* เช็คว่า ประเมินชั้น5 ผ่านรึยัง fifth_score4tea_part3 */
		if(substr($_SESSION['occup_code'], 0, 4) == 'OcAA') {

			$sqlfifth_score4tea_codeA = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='A' ";
			$reA = mysqli_query($con, $sqlfifth_score4tea_codeA);
			$rowA = mysqli_fetch_array($reA);
			$check_79 = $rowA['fifth_score4tea_data']; // Check that user was done all 79 question in evaluation?

			$date1 = new DateTime($rowA['fifth_score4tea_date']);
			$fifth_score4tea_date = $date1->format('d-m-Y');

			$date2 = new DateTime($rowA['fifth_score4tea_date']);
			$date2 = $date2->add(new DateInterval('P15D'));
			$fifth_score4tea_date_again = $date2->format('d-m-Y');

		}elseif(substr($_SESSION['occup_code'], 0, 4) == 'OcAB') {
			
			$sqlfifth_score4tea_codeA = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='A' ";
			$reA = mysqli_query($con, $sqlfifth_score4tea_codeA);
			$rowA = mysqli_fetch_array($reA);
			$check_32 = $rowA['fifth_score4tea_data']; // Check that user was done all 32 question in evaluation?

			$date1 = new DateTime($rowA['fifth_score4tea_date']);
			$fifth_score4tea_date = $date1->format('d-m-Y');

			$date2 = new DateTime($rowA['fifth_score4tea_date']);
			$date2 = $date2->add(new DateInterval('P15D'));
			$fifth_score4tea_date_again = $date2->format('d-m-Y');

		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/* เช็คว่า ทั้ง3part ผ่านรึยัง */
		// Part 1
		if(!isset($fifth_score4tea_part2)) $fifth_score4tea_part2 = "";
		if($basic_score_status == 'ได้รับการอนุมัติ' || $basic_score_status == 'Fast-Track') {
			$fifth_score4tea_part1 = 'Pass';
		}
		// Part 2
		if(substr($_SESSION['occup_code'], 0, 4) == 'OcAA') {
			if(ceil(number_format($countstupass/$stu_num*100, 2)) >= 50) {
				$fifth_score4tea_part2 = 'Pass';
			}
		}elseif(substr($_SESSION['occup_code'], 0, 4) == 'OcAB') {
			if($countstupass >= 5) {
				$fifth_score4tea_part2 = 'Pass';
			}
		}
		// Part 3
		if(substr($_SESSION['occup_code'], 0, 4) == 'OcAA') {
			if(strpos($check_79, '79.') !== false) {
				include('includes/calfifth_score-ครู.php');
				if($numberpass_sub >= 1) {
					$fifth_score4tea_part3 = 'Pass';
				}
			}
		}elseif(substr($_SESSION['occup_code'], 0, 4) == 'OcAB') {
			if(strpos($check_32, '32.') !== false) {
				include('includes/calfifth_score-ผู้บริหาร-ครู.php');
				if($numberpass_sub >= 1) {
					$fifth_score4tea_part3 = 'Pass';
				}
			}
		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>


<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการครูดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li><a href="all_project.php"> ร่วมโครงการ</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการครูดีของแผ่นดิน.php"> โครงการครูดีของแผ่นดิน</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="#" onclick="return false"> ครูดีของแผ่นดินชั้นที่ 5</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - แนบหลักฐานประกอบการพิจารณา -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
	<!-- ################################################################################################ -->
	<div style="max-width:80%; margin-right:auto; margin-left:auto; padding:100px 0 130px;">
		<div class="row">
			<!-- ################################################################################################ -->
			<!-- Start เมื่อทั้ง 3 ส่วนผ่าน ให้ครูกดปุ่มส่งให้เจ้าหน้าที่ตรวจสอบต่อไป -->
			<?php
				if($fifth_score4tea_part1 == 'Pass' && $fifth_score4tea_part2 == 'Pass' && $fifth_score4tea_part3 == 'Pass') {
					if($fifth_score_status == NULL || $fifth_score_status == '') { echo $fifth_score_status; ?>
						<div class="col-sm-12 col-lg-12 mb-3 fs-35 center" style="font-family: RSUText">
							<p class="bold" style="margin: 0;">
								เนื่องจากคุณผ่านทั้ง 3 ส่วน กดปุ่มนี้เพื่อให้เจ้าหน้าที่มูลนิธิตรวจสอบ
							</p>
							<br>
							<a href="sendcheckfifth_score4tea.php?ID=<?php echo $ID; ?>" class="btn m-b-20" style="padding: 10px;">ส่งให้มูลนิธิตรวจสอบ</a>
							<br>
						</div> <?php

					}elseif($fifth_score_status == 'ส่งให้มูลนิธิตรวจสอบ') { ?>
						<div class="col-sm-12 col-lg-12 mb-3 m-b-30 fs-20 center inline" style="font-family: RSUText">
							<div class="col-md-12">
								<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="มูลนิธิครูดีของแผ่นดิน PureLogo">
								<p><strong>สถานะเจ้าหน้าที่มูลนิธิ:</strong> รอมูลนิธิตรวจสอบ</p>
							</div>
						</div> <?php

					}elseif($fifth_score_status == 'Approve แล้ว') { ?>
						<div class="col-sm-12 col-lg-12 mb-3 m-b-30 fs-20 center inline" style="font-family: RSUText">
							<div class="col-md-12">
								<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="มูลนิธิครูดีของแผ่นดิน PureLogo">
								<p class="text-green1"><strong>สถานะเจ้าหน้าที่มูลนิธิ:</strong> ผ่านแล้ว <i class="fas fa-check-circle"></i></p>
							</div>
						</div> <?php

					}
				}
			?>
			<!-- End เมื่อทั้ง 3 ส่วนผ่าน ให้ครูกดปุ่มส่งให้เจ้าหน้าที่ตรวจสอบต่อไป -->
			<!-- ################################################################################################ -->
			<!-- Start Left Content -->
			<div class="col-sm-4 col-lg-4 mb-3">
				<div class="input-slip-upload">
					<div class="title-left">
						<h3>เคยได้รับรางวัลครูดีของแผ่นดินขั้นพื้นฐาน <?php if($fifth_score4tea_part1 == 'Pass'){echo '<i class="fas fa-check-circle"></i>';} ?></h3>
					</div>
					<!-- ################################################################################################ -->
					<p class="m-t-10 text-black"><strong>สถานะรางวัลครูดีของแผ่นดินขั้นพื้นฐาน:</strong> <?php echo $basic_score_status; ?></p>
					<p class="m-t-10 text-black"><strong>วันที่ได้รับครั้งล่าสุด:</strong> <?php echo $basic_score_date; ?></p>
					<p class="m-t-10 text-black"><strong>วันหมดอายุ:</strong> <?php echo $expire_basic_score_date; ?></p>
				</div>
			</div>
			<!-- End Left Content -->
			<!-- ################################################################################################ -->
			<!-- Start Center Content -->
			<div class="col-sm-4 col-lg-4 mb-3">
				<div class="input-slip-upload">
					<div class="title-left">
						<h3>มีเด็กดีของแผ่นดินขั้นพื้นฐาน ตามจำนวนที่กำหนด <?php if($fifth_score4tea_part2 == 'Pass'){echo '<i class="fas fa-check-circle"></i>';} ?></h3>
					</div>
					<!-- Start form input -->
					<!-- ################################################################################################ -->
					<?php
						if($fifth_score4tea_part2 == 'Pass') { ?>
							<div class="row m-b-20 p-b-10" style="border-bottom:2px solid white;">
								<div class="col-md-12 mb-3">
									<p class="m-t-10 text-black"><strong>สถานะมีเด็กดีของแผ่นดินขั้นพื้นฐานตามเกณฑ์:</strong> ผ่าน</p>
								</div>
							</div> <?php
						}
					?>
					<!-- ################################################################################################ -->
					<form action="updatefifth_score4tea-ครู.php?fifth_score4tea_code=B" method="POST">
						<div class="row">
							<div class="col-md-3 mb-3">
								<label><strong>จำนวน นร. :</strong></label>
							</div>
							<div class="col-md-3 mb-3">
								<?php
									if($stu_num == 0) { ?>
										<input type="number" name="fifth_score4tea_data" min="1" class="form-control" required /> <?php
									}else{ ?>
										<label><?php echo $stu_num; ?> คน</label> <?php
									}
								?>
							</div>
							<div class="col-md-6 mb-3">
								<?php
									if($stu_num == 0) { ?>
										<button class="col-md-12 stu-file-btn">อัพเดทจำนวน</button> <?php
									}else{ ?>
										<a class="col-md-12 stu-file-btn" style="background-color:gray">อัพเดทได้เพียงครั้งเดียว</a> <?php
									}
								?>
							</div>
						</div>
					</form>
					<!-- ################################################################################################ -->
					<?php
						if(substr($_SESSION['occup_code'], 0, 4) == 'OcAB') {
						
						}else{
							$sqlfifth_score4tea_codeC = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='C' ";
							$reC = mysqli_query($con, $sqlfifth_score4tea_codeC);
							$rowC = mysqli_fetch_array($reC); ?>

							<form action="updatefifth_score4tea-ครู.php?fifth_score4tea_code=C" method="POST" enctype="multipart/form-data">
								<div class="row m-t-20 p-t-20" style="border-top:2px solid white;">
									<div class="col-md-12 mb-3">
										<label><strong>แนบเอกสารยืนยันรายชื่อนักเรียนที่ดูแล</strong></label>
									</div>
									<?php
										if($rowC['fifth_score4tea_data'] != '') { ?>
											<div class="col-md-6 mb-3">
												<input type="file" id="studoc" name="fifth_score4tea_data" class="form-control" required />
											</div>
											<div class="col-md-6 mb-3 m-t-8">
												<button class="col-md-12 stu-file-btn" onclick="window.location.href='<?php echo $rowC['fifth_score4tea_data']; ?>'">ดูไฟล์แนบ</button>
											</div> <?php
										}else{ ?>
											<div class="col-md-12 mb-3">
												<input type="file" id="studoc" name="fifth_score4tea_data" class="form-control" required />
											</div> <?php
										}
									?>
									<div class="col-md-12 m-t-12">
										<button type="submit" class="col-md-12 stu-up-btn" onmousemove="this.style.cursor='pointer'">แนบเอกสาร</button>
									</div>
								</div>
							</form> <?php
						}
					?>
					<!-- ################################################################################################ -->
					<?php
						if($stu_num != 0) { ?>
							<div class="row m-t-20 p-t-20" style="border-top:2px solid white;">
								<div class="col-md-12 mb-3">
									<label><strong>จำนวนนักเรียนที่ผ่านแล้ว: </strong><?php echo $countstupass.'/'.$stu_num.' คน คิดเป็น '.number_format($countstupass/$stu_num*100, 2).' %'; ?></label>
								</div>
							</div> <?php
						}
					?>
					<!-- ################################################################################################ -->
					<div class="row m-t-20 p-t-20" style="border-top:2px solid white;">
						<div class="col-md-6 mb-3">
							<label><strong>ดูรายชื่อและติดตามสถานะนักเรียน</strong> (สามารถเลือกนักเรียนที่จะดูแล ได้ที่นี้)</label>
						</div>
						<div class="col-md-6 mb-3">
							<form action="ระบบติดตามสถานะนักเรียนขั้นพื้นฐาน-ครู.php">
								<button class="col-md-12 stu-file-btn">ดูรายชื่อ</button>
							</form>
						</div>
					</div>
					<!-- ################################################################################################ -->
					<!-- End form input -->
				</div>
			</div>
			<!-- End Center Content -->
			<!-- ################################################################################################ -->
			<!-- Start Right Content -->
			<div class="col-sm-4 col-lg-4 mb-3">
				<div class="input-slip-upload">
					<div class="title-left" style="font-weight:1500">
						<h3>ผ่านการประเมินครองตน ครองคน ครองงาน อย่างน้อย 1 หมวด
							<?php
								if($fifth_score4tea_part3 == 'Pass') {
									echo '<i class="fas fa-check-circle"></i>';
								}
							?>
						</h3>
					</div>
					<!-- Start form input -->
					<?php

						if(substr($_SESSION['occup_code'], 0, 4) == 'OcAA') { // ถ้าเป็นครูให้เข้าการประเมินของครู ?>
							<div class="row">
								<div class="col-md-12 mb-3"> <?php
									if(strpos($check_79, '79.') != '') { // Check if the seventy-ninth question was done
										if($numberpass_sub < 1) { // Check if all 3 categories were failed
											if(strtotime(date('d-m-Y')) <= strtotime($fifth_score4tea_date_again)) { // ครูที่สอบตกไป จะได้เข้ามาทำอีกครั้งอีก15วัน $date1 คือ วันที่สอบตก $date2 คือ วันที่จะเข้ามาทำได้อีกครั้ง ?>
												<p class="m-t-10 text-black"><strong>หมวดครองตน:</strong> <?php if($selfscore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$selfscore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$selfscore_avg.' /5 คะแนน)';} ?></p>
												<p class="m-t-10 text-black"><strong>หมวดครองคน:</strong> <?php if($peoplescore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$peoplescore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$peoplescore_avg.' /5 คะแนน)';} ?></p>
												<p class="m-t-10 text-black"><strong>หมวดครองงาน:</strong> <?php if($workscore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$workscore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$workscore_avg.' /5 คะแนน)';} ?></p>
												<p style="margin: 0; text-align: center;">
													เนื่องจากคุณไม่ผ่านประเมินชั้นที่ 5 เมื่อวันที่ <?php echo $fifth_score4tea_date; ?>
													<br>
													คุณจะสามารถเข้ามาประเมินได้อีกครั้ง ในวันที่ <?php echo $fifth_score4tea_date_again; ?>
												</p> <?php
											}else{ ?>
												<a href="ประเมินครูดีของแผ่นดินชั้นที่5.php?CFP=ประเมินครูดีของแผ่นดินชั้นที่5_ทำอีกครั้ง" class="col-md-12 stu-file-btn">เริ่มทำประเมินอีกครั้ง</a> <?php
											}
										}else{ ?>
											<p class="m-t-10 text-black"><strong>สถานะประเมินครูดีของแผ่นดินชั้นที่ 5:</strong> ผ่าน</p>
											<p class="m-t-10 text-black"><strong>หมวดครองตน:</strong> <?php if($selfscore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$selfscore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$selfscore_avg.' /5 คะแนน)';} ?></p>
											<p class="m-t-10 text-black"><strong>หมวดครองคน:</strong> <?php if($peoplescore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$peoplescore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$peoplescore_avg.' /5 คะแนน)';} ?></p>
											<p class="m-t-10 text-black"><strong>หมวดครองงาน:</strong> <?php if($workscore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$workscore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$workscore_avg.' /5 คะแนน)';} ?></p>
											<form action="รายงานประเมินครูดีชั้นที่5.php">
												<button class="col-md-12 stu-file-btn">ดาวน์โหลดรายงานคะแนนชั้นที่ 5</button>
											</form> <?php
										} ?>
										<!-- ################################################################################################ -->
								</div> <?php

									}else{ ?>
										<div class="col-md-12 mb-3">
											<form action="ประเมินครูดีของแผ่นดินชั้นที่5.php">
												<label><strong>ประเมินครูดีของแผ่นดินชั้นที่ 5</strong></label>
												<?php
													if(mysqli_num_rows($reA) == 0) { ?>
														<button class="col-md-12 stu-file-btn">เริ่มทำประเมิน</button> <?php
													}else{ ?>
														<button class="col-md-12 stu-file-btn">ทำประเมินต่อ</button> <?php
													} ?>
											</form>
										</div>
							</div> <?php
								}
						}elseif(substr($_SESSION['occup_code'], 0, 4) == 'OcAB') { // ถ้าเป็นผู้บริหารสถานศึกษาให้เข้าการประเมินของผู้บริหาร ?>
						
								<div class="row">
									<div class="col-md-12 mb-3"> <?php
										if(strpos($check_32, '32.') != '') { // Check if the seventy-ninth question was done
											if($numberpass_sub < 1) { // Check if all 3 categories were failed
												if(strtotime(date('d-m-Y')) <= strtotime($fifth_score4tea_date_again)) { // ครูที่สอบตกไป จะได้เข้ามาทำอีกครั้งอีก15วัน $date1 คือ วันที่สอบตก $date2 คือ วันที่จะเข้ามาทำได้อีกครั้ง ?>
													<p class="m-t-10 text-black"><strong>หมวดครองตน:</strong> <?php if($selfscore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$selfscore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$selfscore_avg.' /5 คะแนน)';} ?></p>
													<p class="m-t-10 text-black"><strong>หมวดครองคน:</strong> <?php if($peoplescore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$peoplescore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$peoplescore_avg.' /5 คะแนน)';} ?></p>
													<p class="m-t-10 text-black"><strong>หมวดครองงาน:</strong> <?php if($workscore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$workscore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$workscore_avg.' /5 คะแนน)';} ?></p>
													<p style="margin: 0; text-align: center;">
														เนื่องจากคุณไม่ผ่านประเมินชั้นที่ 5 เมื่อวันที่ <?php echo $fifth_score4tea_date; ?>
														<br>
														คุณจะสามารถเข้ามาประเมินได้อีกครั้ง ในวันที่ <?php echo $fifth_score4tea_date_again; ?>
													</p> <?php
												}else{ ?>
													<a href="ประเมินครูดีของแผ่นดินชั้นที่5-ผู้บริหาร.php?CFP=ประเมินครูดีของแผ่นดินชั้นที่5_ทำอีกครั้ง" class="col-md-12 stu-file-btn">เริ่มทำประเมินอีกครั้ง</a> <?php
												}
											}else{ ?>
												<p class="m-t-10 text-black"><strong>สถานะประเมินครูของแผ่นดินชั้นที่ 5 สำหรับผู้บริหารสถานศึกษา:</strong> ผ่าน</p>
												<p class="m-t-10 text-black"><strong>หมวดครองตน:</strong> <?php if($selfscore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$selfscore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$selfscore_avg.' /5 คะแนน)';} ?></p>
												<p class="m-t-10 text-black"><strong>หมวดครองคน:</strong> <?php if($peoplescore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$peoplescore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$peoplescore_avg.' /5 คะแนน)';} ?></p>
												<p class="m-t-10 text-black"><strong>หมวดครองงาน:</strong> <?php if($workscore_avg >= 3){echo 'ผ่าน (คุณได้คะแนน '.$workscore_avg.' /5 คะแนน)';}else{echo 'ต่ำกว่าเกณฑ์ (คุณได้คะแนน '.$workscore_avg.' /5 คะแนน)';} ?></p>
												<form action="รายงานประเมินครูดีชั้นที่5-ผู้บริหาร.php">
													<button class="col-md-12 stu-file-btn">ดาวน์โหลดรายงานคะแนนชั้นที่ 5</button>
												</form> <?php
											} ?>
											<!-- ################################################################################################ -->
									</div> <?php

										}else{ ?>
											<div class="col-md-12 mb-3">
												<form action="ประเมินครูดีของแผ่นดินชั้นที่5-ผู้บริหาร.php">
													<label><strong>ประเมินครูดีของแผ่นดินชั้นที่ 5 สำหรับผู้บริหารสถานศึกษา</strong></label>
													<?php
														if(mysqli_num_rows($reA) == 0) { ?>
															<button class="col-md-12 stu-file-btn">เริ่มทำประเมิน</button> <?php
														}else{ ?>
															<button class="col-md-12 stu-file-btn">ทำประเมินต่อ</button> <?php
														} ?>
												</form>
											</div>
								</div> <?php

								}

						}else{ // เคสอื่นๆที่ไม่ใช่ครูรัฐและผู้บริหารรัฐ และเข้ามาหน้านี้ได้ จะเข้า Loop นี้ ?>
							<p class="center bold fs-15 m-t-40" style="background-color: rgb(206, 206, 206); border: 1px solid black; border-radius: 30px; padding: 5px 0">คุณไม่ได้อยู่ในสถานะที่ระบบกำหนด จึงไม่สามารถทำประเมินได้ ถ้าตำแหน่งของท่านถูกต้อง กรุณาติดต่อเจ้าหน้าที่มูลนิธิ</p> <?php
						}
					?>
					<!-- End form input -->
				</div>
			</div>
			<!-- End Right Content -->
		</div>
	</div>
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - แนบหลักฐานประกอบการพิจารณา -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

</body>
</html>