<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		date_default_timezone_set("Asia/Bangkok");

		include('includes/calfifth_score-ผู้บริหาร-ครู.php'); 

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}

?>

<!DOCTYPE html>

<html lang="thai">
<!-- To declare your language - read more here: https://www.w3.org/International/questions/qa-html-language-declarations -->

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<!-- Start Style for Radar Chart -->
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
<link rel="stylesheet" href="layout/styles/radar-chart.css">

<style>
	.radar-chart .area {
		fill-opacity: 0.7;
	}
	.radar-chart.focus .area {
		fill-opacity: 0.3;
	}
	.radar-chart.focus .area.focused {
		fill-opacity: 0.9;
	}
	.area.ปี2563, .ปี2563 .circle {
		fill: rgb(255,247,176);
		stroke: none;
	}
	/*
	.area.argentina, .argentina .circle {
		fill: #ADD8E6;
		stroke: none;
	}
	*/
</style>
<!-- End Style for Radar Chart -->
<!-- Start Script for Radar Chart -->
<script type="text/javascript" src="js/d3.v3.js"></script>
<script type="text/javascript" src="js/radar-chart.js"></script>

<script type="text/javascript">
	RadarChart.defaultConfig.color = function() {};
	RadarChart.defaultConfig.radius = 1;
</script>

<script type="text/javascript">
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var allscore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "ครองตน", value: <?php echo $selfscore_avg; ?>}, 
				{axis: "ครองคน", value: <?php echo $peoplescore_avg; ?>}, 
				{axis: "ครองงาน", value: <?php echo $workscore_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var selfscore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "<?php echo $selfscore_sub1_topic; ?>", value: <?php echo $selfscore_sub1_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub2_topic; ?>", value: <?php echo $selfscore_sub2_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub3_topic; ?>", value: <?php echo $selfscore_sub3_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub4_topic; ?>", value: <?php echo $selfscore_sub4_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub5_topic; ?>", value: <?php echo $selfscore_sub5_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub6_topic; ?>", value: <?php echo $selfscore_sub6_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub7_topic; ?>", value: <?php echo $selfscore_sub7_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub8_topic; ?>", value: <?php echo $selfscore_sub8_avg; ?>}, 
				{axis: "<?php echo $selfscore_sub9_topic; ?>", value: <?php echo $selfscore_sub9_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var peoplescore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "<?php echo $peoplescore_sub10_topic; ?>", value: <?php echo $peoplescore_sub10_avg; ?>}, 
				{axis: "<?php echo $peoplescore_sub11_topic; ?>", value: <?php echo $peoplescore_sub11_avg; ?>}, 
				{axis: "<?php echo $peoplescore_sub12_topic; ?>", value: <?php echo $peoplescore_sub12_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var workscore = [
		{
			className: 'ปี2563',
			axes: [
				{axis: "<?php echo $workscore_sub13_topic; ?>", value: <?php echo $workscore_sub13_avg; ?>}, 
				{axis: "<?php echo $workscore_sub14_topic; ?>", value: <?php echo $workscore_sub14_avg; ?>}, 
				{axis: "<?php echo $workscore_sub15_topic; ?>", value: <?php echo $workscore_sub15_avg; ?>}, 
				{axis: "<?php echo $workscore_sub16_topic; ?>", value: <?php echo $workscore_sub16_avg; ?>}, 
				{axis: "<?php echo $workscore_sub17_topic; ?>", value: <?php echo $workscore_sub17_avg; ?>}, 
				{axis: "<?php echo $workscore_sub18_topic; ?>", value: <?php echo $workscore_sub18_avg; ?>}, 
				{axis: "<?php echo $workscore_sub19_topic; ?>", value: <?php echo $workscore_sub19_avg; ?>}
			]
		},
	];
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
</script>
<!-- End Script for Radar Chart -->
</head>

  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="index-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li><a href="all_project.php"> ร่วมโครงการ</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการครูดีของแผ่นดิน.php"> โครงการครูดีของแผ่นดิน</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการครูดีของแผ่นดินชั้นที่5-lobby.php"> ครูดีของแผ่นดินชั้นที่ 5</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="#" onclick="return false"> รายงานผลประเมินครูดีของแผ่นดินชั้นที่ 5</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - รายงานผลประเมินครูดีชั้นที่5 -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <article>
      <!-- ################################################################################################ -->
      <section class="text-gold center" style="line-height:80px;">
        <h7>รายงานผลประเมินครูดีของแผ่นดินชั้นที่ 5</h7><br>
      </section>
      <!-- ################################################################################################ -->
			<!-- Start การรายงานผลแบบตาราง -->
			<section class="table3">
				<table class="m-l-r-auto lh-1-0">
					<!-- ################################################################################################ -->
					<!-- Start หัวข้อตาราง -->
					<tr>
						<th class="center">หมวด</th>
						<th class="center">ตัวบ่งชี้</th>
						<th class="center">คะแนนเต็ม</th>
						<th class="center">คะแนนที่ได้</th>
					</tr>
					<!-- End หัวข้อตาราง -->
					<!-- ################################################################################################ -->
					<!-- Start เนื้อหาตาราง ข้อ 1-39 -->
					<!-- ตัวบ่งชี้ที่ 1 -->
					<tr class="text-black BG-yellow1">
						<td class="bold">ครองตน</td>
						<td style="text-align: left">1. <?php echo $selfscore_sub1_topic; ?></td>
						<td>5</td>
						<td><?php echo $selfscore_sub1_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 2 -->
					<tr class="text-black BG-yellow1">
						<td></td>
						<td style="text-align: left">2. <?php echo $selfscore_sub2_topic; ?></td>
						<td>5</td>
						<td><?php echo $selfscore_sub2_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 3 -->
					<tr class="text-black BG-yellow1">
						<td></td>
						<td style="text-align: left">3. <?php echo $selfscore_sub3_topic; ?></td>
						<td>5</td>
						<td><?php echo $selfscore_sub3_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 4 -->
					<tr class="text-black BG-yellow1">
						<td></td>
						<td style="text-align: left">4. <?php echo $selfscore_sub4_topic; ?></td>
						<td>5</td>
						<td><?php echo $selfscore_sub4_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 5 -->
					<tr class="text-black BG-yellow1">
						<td></td>
						<td style="text-align: left">5. <?php echo $selfscore_sub5_topic; ?></td>
						<td>5</td>
						<td><?php echo $selfscore_sub5_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 6 -->
					<tr class="text-black BG-yellow1">
						<td></td>
						<td style="text-align: left">6. <?php echo $selfscore_sub6_topic; ?></td>
						<td>5</td>
						<td><?php echo $selfscore_sub6_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 7 -->
					<tr class="text-black BG-yellow1">
						<td></td>
						<td style="text-align: left">7. <?php echo $selfscore_sub7_topic; ?></td>
						<td>5</td>
						<td><?php echo $selfscore_sub7_avg; ?></td>
					</tr>
					<!-- สรุป หมวดครองตน -->
					<tr class="text-black BG-white">
						<td colspan="2" rowspan="2" class="center bold">สรุปหมวดครองตน</td>
						<td class="bold">คะแนนเฉลี่ย</td>
						<?php
							if($selfscore_avg >= 3) { ?>
								<td class="BG-green2 bold"><?php echo $selfscore_avg; ?></td> <?php
							}else{ ?>
								<td class="BG-red1 bold"><?php echo $selfscore_avg; ?></td> <?php
							}
						?>
					</tr>
					
					<tr class="text-black BG-white">
						<td class="center bold">สถานะ</td>
						<?php
							if($selfscore_avg >= 3) { ?>
								<td class="BG-green2 bold"><?php echo 'ผ่าน'; ?></td> <?php
							}else{ ?>
								<td class="BG-red1 bold"><?php echo 'ไม่ผ่าน'; ?></td> <?php
							}
						?>
					</tr>
					<!-- ################################################################################################ -->
					<!-- ตัวบ่งชี้ที่ 8 -->
					<tr class="text-black BG-orange1">
						<td class="bold">ครองคน</td>
						<td style="text-align: left">8. <?php echo $peoplescore_sub8_topic; ?></td>
						<td>5</td>
						<td><?php echo $peoplescore_sub8_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 9 -->
					<tr class="text-black BG-orange1">
						<td></td>
						<td style="text-align: left">9. <?php echo $peoplescore_sub9_topic; ?></td>
						<td>5</td>
						<td><?php echo $peoplescore_sub9_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 10 -->
					<tr class="text-black BG-orange1">
						<td></td>
						<td style="text-align: left">10. <?php echo $peoplescore_sub10_topic; ?></td>
						<td>5</td>
						<td><?php echo $peoplescore_sub10_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 11 -->
					<tr class="text-black BG-orange1">
						<td></td>
						<td style="text-align: left">11. <?php echo $peoplescore_sub11_topic; ?></td>
						<td>5</td>
						<td><?php echo $peoplescore_sub11_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 12 -->
					<tr class="text-black BG-orange1">
						<td></td>
						<td style="text-align: left">12. <?php echo $peoplescore_sub12_topic; ?></td>
						<td>5</td>
						<td><?php echo $peoplescore_sub12_avg; ?></td>
					</tr>
					<!-- สรุป หมวดครองคน -->
					<tr class="text-black BG-white">
						<td colspan="2" rowspan="2" class="center bold">สรุปหมวดครองตน</td>
						<td class="bold">คะแนนเฉลี่ย</td>
						<?php
							if($peoplescore_avg >= 3) { ?>
								<td class="BG-green2 bold"><?php echo $peoplescore_avg; ?></td> <?php
							}else{ ?>
								<td class="BG-red1 bold"><?php echo $peoplescore_avg; ?></td> <?php
							}
						?>
					</tr>
					
					<tr class="text-black BG-white">
						<td class="center bold">สถานะ</td>
						<?php
							if($peoplescore_avg >= 3) { ?>
								<td class="BG-green2 bold"><?php echo 'ผ่าน'; ?></td> <?php
							}else{ ?>
								<td class="BG-red1 bold"><?php echo 'ไม่ผ่าน'; ?></td> <?php
							}
						?>
					</tr>
					<!-- ################################################################################################ -->
					<!-- ตัวบ่งชี้ที่ 13 -->
					<tr class="text-black BG-red2">
						<td class="bold">ครองงาน</td>
						<td style="text-align: left">13. <?php echo $workscore_sub13_topic; ?></td>
						<td>5</td>
						<td><?php echo $workscore_sub13_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 14 -->
					<tr class="text-black BG-red2">
						<td></td>
						<td style="text-align: left">14. <?php echo $workscore_sub14_topic; ?></td>
						<td>5</td>
						<td><?php echo $workscore_sub14_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 15 -->
					<tr class="text-black BG-red2">
						<td></td>
						<td style="text-align: left">15. <?php echo $workscore_sub15_topic; ?></td>
						<td>5</td>
						<td><?php echo $workscore_sub15_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 16 -->
					<tr class="text-black BG-red2">
						<td class="bold"></td>
						<td style="text-align: left">16. <?php echo $workscore_sub16_topic; ?></td>
						<td>5</td>
						<td><?php echo $workscore_sub16_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 17 -->
					<tr class="text-black BG-red2">
						<td></td>
						<td style="text-align: left">17. <?php echo $workscore_sub17_topic; ?></td>
						<td>5</td>
						<td><?php echo $workscore_sub17_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 18 -->
					<tr class="text-black BG-red2">
						<td></td>
						<td style="text-align: left">18. <?php echo $workscore_sub18_topic; ?></td>
						<td>5</td>
						<td><?php echo $workscore_sub18_avg; ?></td>
					</tr>
					<!-- ตัวบ่งชี้ที่ 19 -->
					<tr class="text-black BG-red2">
						<td></td>
						<td style="text-align: left">19. <?php echo $workscore_sub19_topic; ?></td>
						<td>5</td>
						<td><?php echo $workscore_sub19_avg; ?></td>
					</tr>
					<!-- สรุป หมวดครองตน -->
					<tr class="text-black BG-white">
						<td colspan="2" rowspan="2" class="center bold">สรุปหมวดครองตน</td>
						<td class="bold">คะแนนเฉลี่ย</td>
						<?php
							if($workscore_avg >= 3) { ?>
								<td class="BG-green2 bold"><?php echo $workscore_avg; ?></td> <?php
							}else{ ?>
								<td class="BG-red1 bold"><?php echo $workscore_avg; ?></td> <?php
							}
						?>
					</tr>
					
					<tr class="text-black BG-white">
						<td class="center bold">สถานะ</td>
						<?php
							if($workscore_avg >= 3) { ?>
								<td class="BG-green2 bold"><?php echo 'ผ่าน'; ?></td> <?php
							}else{ ?>
								<td class="BG-red1 bold"><?php echo 'ไม่ผ่าน'; ?></td> <?php
							}
						?>
					</tr>
					<!-- ################################################################################################ -->
				</table>
			</section>
			<!-- ################################################################################################ -->
			<div class="one_first m-t-50">
				<div class="btnJoin">
					<a href="popรายงานประเมินครูดีชั้นที่5แบบตาราง-ผู้บริหาร-ครู.php" class="fs-45 lh-1-5">ดาวน์โหลดคะแนนครูดีของแผ่นดินชั้นที่5 (แบบตาราง)</a>
				</div>
			</div>
			<!-- End การรายงานผลแบบตาราง -->
			<!-- ################################################################################################ -->
			<!-- ################################################################################################ -->
			<!-- Start การรายงานผลแบบ Radar Chart --><!--
			<section class="m-t-250">
				<div class="row center">

					<p class="fs-30 bold">แสดงคะแนนเป็นรายหมวด มีทั้งหมด 3 หมวด</p>
					<div class="col-md-3 chart_container_allscore"></div>
					<script type="text/javascript">
						RadarChart.draw(".chart_container_allscore", allscore);
					</script>

					<p class="fs-30 bold">หมวดครองตน</p>
					<div class="col-md-2 chart_container_selfscore" style="display: inline-block; padding: 10px;"></div>
					<script type="text/javascript">
						RadarChart.draw(".chart_container_selfscore", selfscore);
					</script>

					<p class="fs-30 bold">หมวดครองคน</p>
					<div class="col-md-2 chart_container_peoplescore" style="display: inline-block; padding: 0 10px;"></div>
					<script type="text/javascript">
						RadarChart.draw(".chart_container_peoplescore", peoplescore);
					</script>

					<p class="fs-30 bold">หมวดครองงาน</p>
					<div class="col-md-2 chart_container_workscore" style="display: inline-block; padding: 0 10px;"></div>
					<script type="text/javascript">
						RadarChart.draw(".chart_container_workscore", workscore);
					</script>

				</div>
			</section>
			<!-- ################################################################################################ --><!--
			<?php
				$sqlfifth_score4tea_codeE = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='E' ";
				$reFS4TE = mysqli_query($con, $sqlfifth_score4tea_codeE);
				$rowFS4TE = mysqli_fetch_array($reFS4TE);

				if(mysqli_num_rows($reFS4TE) == 0) { ?>
					<div class="one_first m-t-50">
						<div class="btnJoin">
							<a href="createpdf4fifth_score4tea_codeE.php" class="fs-50 lh-1-5">ดาวน์โหลดคะแนนครูดีของแผ่นดินชั้นที่5 (แบบ RadarChart)</a>
						</div>
					</div> <?php
				}else{ ?>
					<div class="one_first m-t-50">
						<div class="btnJoin">
							<a href="<?php echo $rowFS4TE['fifth_score4tea_data']; ?>" class="fs-50 lh-1-5">ดาวน์โหลดคะแนนครูดีของแผ่นดินชั้นที่5 (แบบ RadarChart)</a>
						</div>
					</div> <?php
				}
			?>
			<!-- End การรายงานผลแบบ Radar Chart -->
			<!-- ################################################################################################ -->
    </article>
  </main>
</div>
<!-- End Content 01 - รายงานผลประเมินครูดีชั้นที่5 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>