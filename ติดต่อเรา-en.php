<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop-en.php'); ?>
                  <ul>
                    <li><a href="ติดต่อเรา.php">ภาษาไทย</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Contactus-en.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb">
  <div class="hoc clear">
    <!-- ################################################################################################ -->
    <ul>
      <li><a href="index-en.php">Home</a></li>
      <i class="fas fa-angle-double-right"></i>
      <li><a href="ติดต่อเรา-en.php">Contact Us</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - Map&Basic Info. -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <article class="group">
      <div class="one_half first">
        <img src="images/มูลนิธิครูดีของแผ่นดิน แผนที่.jpg">
      </div>
      <div class="one_half" style="font-family:ChulaCharasNew; font-size:20px;">
        <p style="font-size:30px; color:rgb(34,55,226);"><strong>Foundation of Thai Suprateacher</strong></p>
        <p style="font-family:ChulaCharasNew;"><a href="https://www.google.co.th/maps/place/
          %E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%
          B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b763
          0b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th" style="color:black;">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม เขตพระนคร กรุงเทพ 10200</a></p>
        <br>
        <p style="font-family:ChulaCharasNew;"><strong style="color:black">Office Hours :</strong> Monday - Friday 10:00 AM - 6:00 PM</p>
        <p><strong style="color:black">Tel :</strong><a href="tel:02-001-1515" style="color:black;"> 02 001 1515</a></p>
        <p><strong style="color:black">Fax :</strong> 02 001 1368</p>
        <p><strong style="color:black">Email :</strong><a href="mailto:thaisuprateacher@gmail.com" style="color:black;"> thaisuprateacher@gmail.com</a></p>
        <br>
        <a class="contactusPageLinkFB" href="https://www.facebook.com/thaisuprateacher">
          <div style="padding:0 0 15px 0">
            <i class="fab fa-facebook-square" style="color:#3B5998"></i>
            https://www.facebook.com/thaisuprateacher
          </div>
        </a>
        <a class="contactusPageLinkLi" href="http://line.me/ti/p/%40Thaisuprateacher">
          <div style="padding:0 0 15px 0">          
            <i class="fab fa-line" style="color:#00b900;"></i>
            @thaisuprateacher
          </div>
        </a>
        <a class="contactusPageLinkYT" href="https://www.youtube.com/channel/UC0g5ewuybmFEkeOr2PETjaw">
          <div style="padding:0 0 15px 0">
            <i class="fa fa-youtube-square" style="color:#c4302b;"></i > 
            https://www.youtube.com/thaisuprateacher
          </div>
        </a>
      </div>
    </article>
  </main>
</div>
<!--End Content 01 - Map&Basic Info. -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer-en.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>