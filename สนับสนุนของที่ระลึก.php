<?php 
	session_start();
	require_once('condb.php');
	$sql_list_project  ="SELECT * FROM join_project";
	$list_project = $con->query($sql_list_project);

	$email = $_SESSION['email'];

	/* SUM all products price */
	$sqlcount="SELECT SUM(product_total) AS price_total FROM cart WHERE email='$email' ";
	$ressultcount=mysqli_query($con,$sqlcount);
	$rowcount=mysqli_fetch_array($ressultcount);
	$price_total=$rowcount['price_total'];
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['win_scroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
				<?php if (strpos($_SESSION['email'], '@') !== false) {
								if ($_SESSION["level"]=="admin") { //Admin Session ?>
								<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
									<ul>
										<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
										<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
									</ul>
								</li>
					<?php }elseif ($_SESSION["level"]=="admin*A*") { ?>
								<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
									<ul>
										<li><a href="ระบบหลังบ้านครูขั้นพื้นฐาน.php">ระบบหลังบ้านประเมินครู</a></li>
									</ul>
								</li>
					<?php }elseif ($_SESSION["level"]=="admin*B*") { ?>
								<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
									<ul>
										<li><a href="ระบบหลังบ้านร้านค้า.php">ระบบหลังบ้านร้านค้า</a></li>
									</ul>
								</li>
          <?php }elseif ($_SESSION['level']=='memberGeneral') { //General Member Session ?>
								<li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> สมาชิกทั่วไป</li>
          <?php }elseif ($_SESSION['level']=='memberSilver') { //Member Silver Session ?>
								<li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> สมาชิก ระดับเงิน</li>
          <?php }elseif ($_SESSION['level']=='memberGold') { //Member Gold Session ?>
								<li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> สมาชิก ระดับทอง</li>
          <?php }elseif ($_SESSION['level']=='memberDiamond') { //Member Diamond Session ?>
								<li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> สมาชิก ระดับเพชร</li>
          <?php } ?>
								<li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>ออกจากระบบ</text1></a></li>
        <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login.php"><i class="fas fa-user-lock"></i> <text1>เข้าสู่ระบบ</text1></a></li>
                <li class="faicon-login"><a href="signup.php"><i class="fas fa-file-signature"></i> <text1>สมัครสมาชิก</text1></a></li>
				<?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>ภาษาไทย</text1></a>
                  <ul>
                    <li><a href="สนับสนุนของที่ระลึก-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>สนับสนุนมูลนิธิ</donate></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค.php">ภาพรวมสนับสนุนมูลนิธิ</a></li>
                    <li><a href="บำรุงค่าสมาชิก.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li class="active"><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึก</a></li>
                  </ul>
                </li>
								<li class="faicon-basket"><i class="fas fa-caret-down"></i><i class="fa fa-shopping-basket"></i> <?php echo number_format($price_total,2); ?>
									<ul>
										<li><a href="สนับสนุนของที่ระลึก.php" >ร้านค้า</a></li>
										<li><a href="ตะกร้า.php">ดูของในตะกร้าและยืนยันคำสั่งซื้อ</a></li>
										<li><a href="ประวัติและสถานะการสั่งซื้อ.php">ประวัติและสถานะการสั่งซื้อ</a></li>
									</ul>
								</li>
        </ul>
			</nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index.php">หน้าแรก</a></li>
          <li><a href="เกี่ยวกับเรา.php">เกี่ยวกับเรา</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>ร่วมโครงการ</a>
            <ul>
              <li><a href="all_project.php">โครงการทั้งหมดของมูลนิธิ</a></li>
			  <?php 
                    while($row = $list_project->fetch_assoc()){
                      $id_join_project = $row['id_join_project'];
                      $title_menu = $row['title_menu']; 
                  ?>
                  <li><a href="<?php  echo "project_db.php?id=".$id_join_project ?>"><?php echo $title_menu; ?></a></li>
                  <?php } ?>
              <!-- <li><a href="โครงการครูดีของแผ่นดิน.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน.php">โครงการอาสาของแผ่นดิน</a></li> -->
            </ul>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest.php">คลังสื่อนวัตกรรม</a></li>
          <li><a href="ติดต่อเรา.php">ติดต่อเรา</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ร้านค้า</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - สนับสนุนของที่ระลึก -->
<div class="wrapper row3" style="background-image: linear-gradient(to right,rgb(255,255,255),rgba(72,160,0,0.4))">
	<!-- ################################################################################################ -->
	<!-- Start Shop Page  -->
	<div class="shop-box-inner">
		<div class="container">
			<div class="row">
				<!-- Start Shop-Left-Content -->
				<div class="col-xl-3 col-lg-3 col-sm-12 col-xs-12 sidebar-shop-left">
					<div class="product-categori">
						<!--
						<div class="search-product">
							<form action="#">
								<input class="form-control" placeholder="Search here..." type="text">
								<button type="submit"> <i class="fa fa-search"></i> </button>
							</form>
						</div>
						-->
						<div class="filter-sidebar-left">
							<div class="title-left">
								<h3>หมวดหมู่</h3>
							</div>
							<div class="list-group list-group-collapse list-group-sm list-group-tree" id="list-group-men" data-children=".sub-men">
								<div class="list-group-collapse sub-men">
									<div class="list-group-item list-group-item-action <?php if($_GET['product_cate'] == '') {echo ' active';} ?>" aria-expanded="true" aria-controls="sub-men1"
									onclick="getWinScroll_filterCate('')">ของที่ระลึก <small class="text-muted">(10)</small></div>
									<div class="collapse show" id="sub-men1" data-parent="#list-group-men">
										<div class="list-group">
											<div class="list-group-item list-group-item-action <?php if($_GET['product_cate'] == 'เซ็ตของที่ระลึก') {echo ' active';} ?>"
											onclick="getWinScroll_filterCate('เซ็ตของที่ระลึก')">เซ็ตของที่ระลึก <small class="text-muted">(1)</small></div>
											<div class="list-group-item list-group-item-action <?php if($_GET['product_cate'] == 'เสื้อ') {echo ' active';} ?>"
											onclick="getWinScroll_filterCate('เสื้อ')">เสื้อมูลนิธิครูดีของแผ่นดิน <small class="text-muted">(7)</small></div>
											<div class="list-group-item list-group-item-action <?php if($_GET['product_cate'] == 'เข็มกลัด') {echo ' active';} ?>"
											onclick="getWinScroll_filterCate('เข็มกลัด')">เข็มที่ระลึกครูดีของแผ่นดิน <small class="text-muted">(2)</small></div>
										</div>
									</div>
								</div>
								<!--
								<div class="list-group-collapse sub-men">
									<a class="list-group-item list-group-item-action" href="#sub-men2" data-toggle="collapse" aria-expanded="false" aria-controls="sub-men2">Footwear 
										<small class="text-muted">(50)</small>
									</a>
									<div class="collapse" id="sub-men2" data-parent="#list-group-men">
										<div class="list-group">
											<a href="#" class="list-group-item list-group-item-action">Sports Shoes <small class="text-muted">(10)</small></a>
											<a href="#" class="list-group-item list-group-item-action">Sneakers <small class="text-muted">(20)</small></a>
											<a href="#" class="list-group-item list-group-item-action">Formal Shoes <small class="text-muted">(20)</small></a>
										</div>
									</div>
								</div>
								<a href="#" class="list-group-item list-group-item-action"> Men  <small class="text-muted">(150) </small></a>
								<a href="#" class="list-group-item list-group-item-action">Accessories <small class="text-muted">(11)</small></a>
								<a href="#" class="list-group-item list-group-item-action">Bags <small class="text-muted">(22)</small></a>
								-->
							</div>
						</div>
						<!--
						<div class="filter-price-left">
								<div class="title-left">
										<h3>ราคา</h3>
								</div>
								<div class="price-box-slider">
										<div id="slider-range"></div>
										<p>
												<input type="text" id="amount" readonly style="border:0; color:#fbb714; font-weight:bold;">
												<button class="btn hvr-hover" type="submit">Filter</button>
										</p>
								</div>
						</div>
						<div class="filter-brand-left">
								<div class="title-left">
										<h3>Brand</h3>
								</div>
								<div class="brand-box">
										<ul>
												<li>
														<div class="radio radio-danger">
																<input name="survey" id="Radios1" value="Yes" type="radio">
																<label for="Radios1"> Supreme </label>
														</div>
												</li>
												<li>
														<div class="radio radio-danger">
																<input name="survey" id="Radios2" value="No" type="radio">
																<label for="Radios2"> A Bathing Ape </label>
														</div>
												</li>
												<li>
														<div class="radio radio-danger">
																<input name="survey" id="Radios3" value="declater" type="radio">
																<label for="Radios3"> The Hundreds </label>
														</div>
												</li>
												<li>
														<div class="radio radio-danger">
																<input name="survey" id="Radios4" value="declater" type="radio">
																<label for="Radios4"> Alife </label>
														</div>
												</li>
												<li>
														<div class="radio radio-danger">
																<input name="survey" id="Radios5" value="declater" type="radio">
																<label for="Radios5"> Neighborhood </label>
														</div>
												</li>
												<li>
														<div class="radio radio-danger">
																<input name="survey" id="Radios6" value="declater" type="radio">
																<label for="Radios6"> CLOT </label>
														</div>
												</li>
												<li>
														<div class="radio radio-danger">
																<input name="survey" id="Radios7" value="declater" type="radio">
																<label for="Radios7"> Acapulco Gold </label>
														</div>
												</li>
												<li>
														<div class="radio radio-danger">
																<input name="survey" id="Radios8" value="declater" type="radio">
																<label for="Radios8"> UNDFTD </label>
														</div>
												</li>
												<li>
														<div class="radio radio-danger">
																<input name="survey" id="Radios9" value="declater" type="radio">
																<label for="Radios9"> Mighty Healthy </label>
														</div>
												</li>
												<li>
														<div class="radio radio-danger">
																<input name="survey" id="Radios10" value="declater" type="radio">
																<label for="Radios10"> Fiberops </label>
														</div>
												</li>
										</ul>
								</div>
						</div>--> 
						<!-- Start Member Menu -->
						<div class="filter-sidebar-left">
							<div class="title-left">
								<h3>ระบบสมาชิก</h3>
							</div>
							<div class="member-area">
								<?php if (strpos($_SESSION['email'], '@') !== false) { //Check Account is signed in ?>
									<div class="welcome-area">
										<p>สวัสดีค่ะ</p>
										<?php echo $_SESSION['firstname'].' '.$_SESSION['lastname']; ?>
										<br><br>
										เชิญเลือกของที่ระลึก ได้เลยค่ะ
									</div>
								<?php }else{ // No Account Session or others ?>
								<div class="logo-area">
									<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
								</div>
								<div class="logIn-area">
									<p>เข้าสู่ระบบเพื่อทำรายการสนับสนุน<p>
									<a href="login.php"><i class="fas fa-user-lock"></i> เข้าสู่ระบบ</a>
								</div>
								<div class="signUp-area">
									<p>ยังไม่มีสมาชิก<br>สมัครสมาชิกโดยไม่มีค่าใช้จ่าย<p>
									<a href="signup.php"><i class="fas fa-file-signature"></i> สมัครสมาชิก (ฟรี)</a>
								</div>                  
								<?php } ?>
							</div>
						</div>
						<!-- End Member Menu -->
						<!-- ################################################################################################ -->
						<!-- Start Donation Channel Menu -->
						<div class="filter-sidebar-left">
							<div class="title-left">
								<h3>ช่องทางการบริจาคและชำระเงิน</h3>
							</div>
							<div class="channel-area">
								<div class="logo-area">
									<img src="images/มูลนิธิครูดีของแผ่นดิน KTB Logo.jpg" alt="Logo KTB">
								</div>
								<div class="detail-area">
									<p>
										<strong>ธนาคารกรุงไทย</strong>
										<br>
										<strong>ชื่อบัญชี:</strong> มูลนิธิครูดีของแผ่นดิน สาขาถนนวิสุทธิกษัตริย์
										<br>
										<strong>เลขที่บัญชี:</strong> 006 – 0 – 20390 – 0
									</p>
								</div>
								<div class="QRCode-area">
									<img src="images/คิวอาร์โคด บัญชีมูลนิธิ.jpg" alt="คิวอาร์โคด บัญชีมูลนิธิ">
								</div>
								<div class="howTo-area">
									<p>ดูขั้นตอนการสนับสนุนได้ที่นี้<p>
									<a href="ขั้นตอนการสนับสนุนของที่ระลึก.php">ขั้นตอนการสนับสนุน</a>
								</div>
							</div>
						</div>
						<!-- End Donation Channel Menu -->
						<!-- ################################################################################################ -->
						<!-- Start Upload Slip Menu -->
						<div class="filter-sidebar-left">
							<div class="title-left">
								<h3>แจ้งชำระเงินได้ที่นี้</h3>
							</div>
							<div class="slipUpload-area">
								<a href="ส่งหลักฐานการโอนB.php">แจ้งชำระเงิน</a>
							</div>
						</div>
						<!-- End Upload Slip Menu -->
						<!-- ################################################################################################ -->
					</div>
				</div>
				<!-- End Shop-Left-Content -->
				<!-- ################################################################################################ -->
				<!-- Start Shop-Right-Content -->
				<div class="col-xl-9 col-lg-9 col-sm-12 col-xs-12 shop-content-right">
					<div class="right-product-box">
						<!--
						<div class="product-item-filter row">
							<div class="col-12 col-sm-8 text-center text-sm-left">
								<div class="toolbar-sorter-right">
									<span>Sort by </span>
									<select id="basic" class="selectpicker show-tick form-control" data-placeholder="$ USD">
									<option data-display="Select">Nothing</option>
									<option value="1">Popularity</option>
									<option value="2">High Price → High Price</option>
									<option value="3">Low Price → High Price</option>
									<option value="4">Best Selling</option>
									</select>
								</div>
								<p>Showing all 4 results</p>
							</div>
						</div>
						-->
						<div class="row product-categorie-box">
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane fade show active" id="grid-view">
									<div class="row">
										<?php
										/* Set number product per page */
										$results_per_page = 12;
										if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
										$start_from = ($page-1) * $results_per_page;

										switch($_GET['product_cate'])
										{
											case 'เซ็ตของที่ระลึก':
												$sql = "SELECT * FROM shelf WHERE product_cate='เซ็ตของที่ระลึก' ORDER BY product_seq ASC LIMIT $start_from, ".$results_per_page;
											break;
											case 'เสื้อ':
												$sql = "SELECT * FROM shelf WHERE product_cate='เสื้อ' ORDER BY product_seq ASC LIMIT $start_from, ".$results_per_page;
											break;
											case 'เข็มกลัด':
												$sql = "SELECT * FROM shelf WHERE product_cate='เข็มกลัด' ORDER BY product_seq ASC LIMIT $start_from, ".$results_per_page;
											break;
											default:
												$sql = "SELECT * FROM shelf ORDER BY product_seq ASC LIMIT $start_from, ".$results_per_page;
										}
										$rs_result = $con->query($sql);
										$i = 1;
										while($row = $rs_result->fetch_assoc()) {
										?>
										<div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
											<div class="products-single">
												<div class="box-img-hover">
													<?php if($row['product_promote'] != '') { ?>
													<div class="type-lb">
														<a class="blink2"><?php echo $row['product_promote']; ?></a>
													</div>
													<?php } ?>
													<img src="<?php echo $row['product_image']; ?>" class="img-fluid" alt="<?php echo $row['product_image']; ?>">
													<div class="mask-icon">
														<!-- Set valuable for pass to getWinScroll_addCartBtn script -->
														<?php $x = $row['product_id']; ?>
														<?php $y = $row['product_name']; ?>
														<?php $z = $row['product_image']; ?>
														<!-- Add Cart button -->
														<div name="addcart" class="cart" 
															onclick="getWinScroll_addCartBtn(<?php echo $x; ?>,'<?php echo $y; ?>','<?php echo $z; ?>')">
															เพิ่มลงตะกร้า <i class="fas fa-shopping-cart fa-1x"></i>
														</div>
													</div>
												</div>
												<div class="why-text" style="height:90px">
													<!-- Product Name -->
													<h4>
														<?php echo $row['product_name']; ?>
														<!-- Product Size -->
														<div class="size">
															<?php if($row['product_size'] != '') {echo $row['product_size'];} ?>
														</div>
													</h4>
												</div>
												<!-- Product price -->
												<!-- Product price for No member -->
												<div class="why-text">
													<div class="w-full">
														<div class="dis-inline-block" style="width:48.46625766871166%;">
															ราคาบุคคลทั่วไป
														</div>
														<div class="dis-inline-block right" style="width:48.46625766871166%;">
															<h5>
																<?php
																	if(!$_SESSION['level']) {
																		echo number_format($row['product_price_nomem_sale'],2);
																	}else{
																		echo '<del>'.number_format($row['product_price_nomem_sale'],2).'</del>';
																	}
																?>
															</h5>
														</div>
													</div>
													<!-- Product price for Member -->
													<div class="w-full">
														<div class="dis-inline-block" style="width:48.46625766871166%;">
															ราคาสมาชิกทั่วไป
														</div>
														<div class="dis-inline-block right" style="width:48.46625766871166%;">
															<h5>
																<?php
																	if($_SESSION['level']=='memberGeneral' || $_SESSION['level']=='admin') {
																		echo number_format($row['product_price_mem_sale'],2);
																	}else{
																		echo '<del>'.number_format($row['product_price_mem_sale'],2).'</del>';
																	}
																?>
															</h5>
														</div>
													</div>
													<!-- Product price for High Level member -->
													<div class="w-full">
														<div class="dis-inline-block" style="width:48.46625766871166%;">
															ราคาสมาชิกตั้งแต่ระดับเงินขึ้นไป
														</div>
														<div class="dis-inline-block right" style="width:48.46625766871166%;">
															<h5>
																<?php
																	if($_SESSION['level']=='memberSilver' || $_SESSION['level']=='memberGold' || $_SESSION['level']=='memberDiamond') {
																		echo number_format($row['product_price_highmem_sale'],2);
																	}else{
																		echo '<del>'.number_format($row['product_price_highmem_sale'],2).'</del>';
																	}
																?>
															</h5>
														</div>
													</div>
												</div>
											</div>
										</div>
										<?php $i++; } ?>
									</div>
								</div>          
							</div>
						</div>
					</div>                  
				</div>							
				<!-- End Shop-Right-Content -->
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	<!-- End Shop Page -->
</div>
<!-- End Content 00 - สนับสนุนของที่ระลึก -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">หน่วยงานที่เกี่ยวข้อง</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">มูลนิธิครูดีของแผ่นดิน</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->


<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<!-- JS for getWinScroll -->
<script src="js/getWinScroll_filterCate.js"></script>
<script src="js/getWinScroll_addCartBtn.js"></script>

</body>
</html>