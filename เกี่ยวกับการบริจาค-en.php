<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start Account Session -->
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
				<?php if (strpos($_SESSION['email'], '@') !== false) {
								if ($_SESSION["level"]=="admin") { //Admin Session ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า-en.php">ระบบหลังบ้านร้านค้า</a></li>
											<li><a href="ระบบหลังบ้านประเมินครู-en.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*A*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านประเมินครู-en.php">ระบบหลังบ้านประเมินครู</a></li>
										</ul>
									</li>
						<?php }elseif ($_SESSION["level"]=="admin*B*") { ?>
									<li class="faicon-backsystem" style="background-color:rgb(228,0,0);"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-user-edit"></i> ผู้ดูแลระบบ</a>
										<ul>
											<li><a href="ระบบหลังบ้านร้านค้า-en.php">ระบบหลังบ้านร้านค้า</a></li>
										</ul>
									</li>
          <?php }elseif ($_SESSION['level']=='memberGeneral') { //General Member Session ?>
								<li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> General Member</li>
          <?php }elseif ($_SESSION['level']=='memberSilver') { //Member Silver Session ?>
								<li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> Silver Member</li>
          <?php }elseif ($_SESSION['level']=='memberGold') { //Member Gold Session ?>
								<li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> Gold Member</li>
          <?php }elseif ($_SESSION['level']=='memberDiamond') { //Member Diamond Session ?>
								<li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> Diamond Member</li>
          <?php } ?>
								<li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
        <?php }else{ // No Account Session or others ?>
                <li class="faicon-login"><a href="login-en.php"><i class="fas fa-user-lock"></i> <text1>Log in</text1></a></li>
                <li class="faicon-login"><a href="signup-en.php"><i class="fas fa-file-signature"></i> <text1>Sign up</text1></a></li>
				<?php } ?>
                <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
                  <ul>
                    <li><a href="เกี่ยวกับการบริจาค.php">ภาษาไทย</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
                </li>
                <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
                  <ul>
                    <li class="active"><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                    <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                    <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                    <li><a href="สนับสนุนของที่ระลึก-en.php">สนับสนุนของที่ระลึก</a></li>
                  </ul>
                </li>
        </ul>
			</nav>
      <!-- End Account Session -->
      <!-- ################################################################################################ -->
      <!-- Start Searching -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- End Searching -->
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:18px;" href="index.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li class="active"><a href="index-en.php">Home</a></li>
          <li><a href="เกี่ยวกับเรา-en.php">About us</a></li>
          <li><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>Join us</a>
            <ul>
              <li><a href="ร่วมโครงการ-en.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <li><a href="โครงการครูดีของแผ่นดิน-en.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา-en.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA-en.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน-en.php">โครงการอาสาของแผ่นดิน</a></li>
            </ul>
          </li>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">Innovation Library</a></li>
          <li><a href="ติดต่อเรา-en.php">Contact us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="เกี่ยวกับการบริจาค-en.php"> About Donation</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - เกี่ยวกับการบริจาค -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <article>
      <!-- ################################################################################################ -->
      <section class="text-gold center" style="line-height:80px;">
        <h8>สร้างคนดีให้แผ่นดิน สร้างนวัตกรรม สร้างการเปลี่ยนแปลง</h8>
      </section>
      <!-- ################################################################################################ -->
      <section class="fs-32 left m-t-30">
        <div class="font-RSUText font-italic">
          <strong><i class="fas fa-certificate"></i> เหตุผลที่...ทำไมต้องสนับสนุนมูลนิธิ</strong>
        </div>
        <div class="font-RSUText lh-1-8">
          <p>มูลนิธิครูดีของแผ่นดิน องค์กรไม่แสวงหาผลกำไร ได้จัดตั้งอย่างเป็นทางการเมื่อวันที่ 4 ธันวาคม พ.ศ.2560 มีวิสัยทัศน์ <strong>“สร้างคนดีให้แผ่นดิน”</strong> จัดทำโครงการต่างๆ มากมาย เช่น โครงการเครือข่ายครูดีของแผ่นดิน เจริญรอยตามเบื้องพระยุคลบาท โครงการเด็กดีของแผ่นดิน โครงการศึกษานิเทศก์ดีของแผ่นดิน ล เพื่อพัฒนาและยกย่อง ครูและบุคลากรทางการศึกษาให้ประพฤติตนเป็นคนดีของแผ่นดิน มีศีลธรรม มีทักษะการจัดการเรียนการสอนสำหรับผู้เรียนในศตวรรษที่ 21 ปัจจุบันมีสมาชิกแล้วกว่า 30,000 คนทั่วประเทศ</p>
          <p>การขับเคลื่อนที่ผ่านมานั้น... มูลนิธิครูดีของแผ่นดิน ได้จับมือกับองค์กรทางการศึกษามากมาย ทั้ง สำนักงานปลัดกระทรวงศึกษาธิการ สำนักงานคณะกรรมการศึกษาขั้นพื้นฐาน คุรุสภา องค์การปกครองส่วนท้องถิ่น สำนักงานเขตพื้นที่การศึกษา สถานศึกษาทั่วประเทศ เพื่อ <strong>สร้างคนดีให้แผ่นดิน สร้างนวัตกรรม สร้างการเปลี่ยนแปลง</strong> จนเป็นที่ยอมรับในวงการการศึกษามากขึ้น และมากขึ้นเรื่อยๆ ซึ่งการดำเนินการดังกล่าว นอกจากจะต้องมีหัวใจและอุดมการณ์ที่มุ่งมั่นแล้ว ยังต้องอาศัย “ทุนทรัพย์” ที่จะส่งเสริมให้ปณิธานความมุ่งมั่นของมูลนิธิครูดีของแผ่นดินประสบความสำเร็จ</p>
          <p>และ “คุณ” คือ บุคคลสำคัญที่จะทำสิ่งนั้นให้เป็นจริงได้</p>
          <p><strong>ทุกการบริจาควัดคุณค่าเป็นตัวเงินไม่ได้ แต่มีคุณค่าทางจิตใจอย่างสูงยิ่ง</strong></p>
          <p><strong>ทุกวัน... ยังมีครูดีของแผ่นดินจำนวนมาก</strong> ทั้งในพื้นที่ห่างไกลและขาดแคลน หรือแม้แต่ในสังคมเมืองใหญ่ รอคอยความหวังที่จะได้รับการสนับสนุนจากองค์กรหรือหน่วยงานใดหน่วยงานหนึ่ง ช่วยแก้ปัญหาความเหลื่อมล้ำที่บั่นทอนกำลังใจในการทำความดี ทำให้ไม่กล้าจะทำความดีอย่างองอาจ สง่างาม</p>
          <p><i>ขอเพียงให้ครูดีเหล่านั้นรู้ว่าท่านไม่ได้ต่อสู้เพียงลำพัง ยังมีแรงใจมากมายคอยหยัดสู้เคียงข้าง เป็นกำลังกาย กำลังใจ กำลังทรัพย์ และกำลังสติปัญญาคอยให้ความสนับสนุนอยู่เสมอมา</i></p>
        </div>
      </section>
      <!-- ################################################################################################ -->
      <section class="fs-32 left m-t-60">
        <div class="font-RSUText font-italic">
          <strong><i class="fas fa-certificate"></i> สิ่งที่ได้รับเมื่อสนับสนุนมูลนิธิครูดีของแผ่นดิน</strong>
        </div>
        <ul class="font-RSUText lh-1-8">
          <li>ความภาคภูมิใจในการเป็นส่วนหนึ่งที่ร่วมกัน “สร้างคนดีให้แผ่นดิน”</li>
          <li>คุณ คือ ผู้มอบโอกาสทางการศึกษาที่ดีให้กับเด็กและเยาวชนทั่วประเทศในการเติบโตเป็นพลเมืองดีของชาติ เป็นคนดีของแผ่นดิน</li>
          <li>ทุกบาททุกสตางค์ที่คุณบริจาคจะใช้ในการดำเนินโครงการต่างๆ ของมูลนิธิ โดยไม่หักค่าใช้จ่ายแต่อย่างใด</li>
          <li>ทุกการบริจาคของท่าน จะได้รับใบสำคัญรับเงินจากมูลนิธิ เกียรติบัตร และเข็มเชิดชูเกียรติ ดังนี้</li>
        </ul>
      </section>
      <!-- ################################################################################################ -->
      <!-- Start วิธีการสนับสนุน -->
      <div class="fs-32 m-t-60 m-l-30">
        <strong>วิธีการสนับสนุนมูลนิธิ</strong>
      </div>
      <section class="wrapper row3 m-t-20">
        <div style="background-color:rgb(0,6,27); padding:20px 20px 0; border-radius:10px;">
          <div class="group latest">
            <article class="one_third first row4">
              <div class="center zoom108">
                <figure><a href="บำรุงค่าสมาชิก.php"><img src="images/สมาชิกรายปี1.jpg" alt="สมาชิกรายปี1"></a></figure>
              </div>
              <div class="txtwrap">
                <h4 class="heading center" style="color:rgb(180,147,31);">บำรุงค่าสมาชิกรายปี</h4>
              </div>
            </article>
            <article class="one_third row4">
              <div class="center zoom108">
                <figure><a href="บริจาคสนับสนุนโครงการต่างๆ.php"><img src="images/มูลนิธิครูดีของแผ่นดิน ร่วมงาน_นักเรียนดี.jpg" alt="มูลนิธิครูดีของแผ่นดิน ร่วมงาน_นักเรียนดี"></a></figure>
              </div>
              <div class="txtwrap">
                <h4 class="heading center" style="color:rgb(180,147,31)">บริจาคสนับสนุนโครงการต่างๆ<br></h4>
              </div>
            </article>
            <article class="one_third row4">
              <div class="center zoom108">
                <figure><a href="สนับสนุนของที่ระลึก-en.php"><img src="images/ของที่ระลึก1.jpg" alt="ของที่ระลึก1"></a></figure>
              </div>
              <div class="txtwrap">
                <h4 class="heading center" style="color:rgb(180,147,31)">สนับสนุนของที่ระลึกมูลนิธิ</h4>
              </div>
            </article>
          </div>
        </div>
      </section>
      <!-- ################################################################################################ -->
    </article>
  </main>
</div>
<!-- End Content 01 - เกี่ยวกับการบริจาค -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear">
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Related Department</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Foundation of Thai Suprateacher</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>