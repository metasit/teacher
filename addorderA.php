<?php session_start();
			require_once('condb.php');

date_default_timezone_set("Asia/Bangkok");
error_reporting(~E_NOTICE);

$email = $_SESSION['email'];
$ID = $_SESSION['ID'];

/* SUM all products price */
$sqlcount="SELECT SUM(product_total) AS price_total FROM cart WHERE email='$email' ";
$ressultcount=mysqli_query($con,$sqlcount);
$rowcount=mysqli_fetch_array($ressultcount);
$price_total=$rowcount['price_total'];

/* Set order_group */
$sqlmax = "SELECT DISTINCT MAX(order_group) AS max_order_group FROM `orders` ";
$resultmax = mysqli_query($con,$sqlmax);
$rowmax = mysqli_fetch_array($resultmax);

/* Prepare valiable for INSERT data */
$order_group = $rowmax['max_order_group']+1;
$doc_address = $_SESSION['docaddress'];
$new_address = $_POST['new_address'];
$shipping_kind = 'Free';//$_POST['shipping_kind'];
$order_date = date("Y-m-d H:i:s");

/* Set shipping_address */
if(empty($new_address)) {
	$shipping_address = $doc_address;
}else{
	$shipping_address = $new_address;
}
/* Set shipping_kind *//*
if(empty($shipping_kind)) {
	$shipping_kind = 'ems';
	$shipping_cost = 80;
}else{
	$shipping_kind = $_POST['shipping_kind'];
	$shipping_cost = 50;
}
/* Set contact_name */
if(empty($_POST['new_name'])) {
	$contact_name = $_SESSION['fisrtname'].'<br>'.$_SESSION['lastname'];
}else{
	$new_name = $_POST['new_name'];
	$contact_name = str_replace(' ','<br>',$new_name);
}
/* Set contact_email */
if(empty($_POST['contact_email'])) {
	$contact_email = $_SESSION['email'];
}else{
	$contact_email = $_POST['contact_email'];
}
/* Set contact_number */
$new_phonenum = $_POST['new_phonenum'];
if(empty($new_phonenum)) {
	$contact_number = $_SESSION['phonenum'];
}else{
	$contact_number = $new_phonenum;
}

/* Set order_num */
if(strlen($_POST['cart_id']) == '3') {
	$order_num = '0'.$_POST['cart_id'];
}else{
	$order_num = $_POST['cart_id'];
}

$sqlcart = "SELECT * FROM `cart` WHERE email='$email' ";
$resultcart = $con->query($sqlcart);

while($rowcart = $resultcart->fetch_assoc()) {
	$product_id = $rowcart['product_id'];
	$product_price = $rowcart['product_price'];
	$product_price_sale = $rowcart['product_price_sale'];
	$product_amount = $rowcart['product_amount'];
	$order_remark = $rowcart['cart_remark'];

	$sql="INSERT INTO `orders` (`order_number`,`ID`,`email`,`order_group`,`product_id`,`product_price`,`product_price_sale`,`product_amount`,`shipping_kind`,`shipping_address`
	,`contact_name`,`contact_email`,`contact_number`,`order_status`,`order_date`,`order_remark`)
	 VALUES ('$order_num','$ID','$email','$order_group','$product_id','$product_price','$product_price_sale','$product_amount','$shipping_kind','$shipping_address'
	 ,'$contact_name','$contact_email','$contact_number','รอการชำระเงิน','$order_date','$order_remark')";

	$res= $con->query($sql) or die($con->error);
}

/* Send order summary by email */
$sql1 = "SELECT * FROM orders WHERE email='$email' AND order_group='$order_group' ";
$result1 = $con->query($sql1);

$grand_total = number_format($price_total+$shipping_cost,2);

$strTo = $email;
$strSubject = "มูลนิธิครูดีของแผ่นดิน :: รายงานการสั่งซื้อเลขที่ ".$order_num."  ";
$strHeader = "Content-type: text/html; charset=UTF-8\n"; // or windows-874 //
$strHeader .= "From: thaisuprateacher@gmail.com\n";
$htmlContent = '
									<html>
									<head>
											<title>รายงานการสั่งซื้อสินค้า</title>
									</head>
									<body>
										<h1>สั่งซื้อเรียบร้อย</h1>
										<p><strong>เลขที่ใบสั่งซื้อ: </strong>'.$order_num.'</p>
										<p><strong>สถานะ: </strong><span style="color: red;">รอการชำระเงิน</span></p>
										<table cellspacing="0" style="width: 100%; font-size: 15px; text-align: center;">								
											<tr style="background-color: rgb(72,160,0); color: white;">
												<th>รายการที่</th>
												<th>สินค้า</th>
												<th>จำนวน</th>
												<th>ราคาสุทธิ</th>
											</tr>
								';
											$i=1;
											while($row1 = $result1->fetch_assoc()) {
												/* Set product_image, product_name, product_size, product_detail */
												$product_id = $row1['product_id'];
												$sql="SELECT * FROM shelf WHERE product_id='$product_id' ";
												$result=mysqli_query($con,$sql);
												$row=mysqli_fetch_array($result);
$htmlContent .= '			
											<tr>
												<td>'.$i.'</td>
												<td>'.$row['product_name'].'</td>
												<td>'.$row1['product_amount'].'</td>
												<td>'.number_format($row1['product_price']*$row1['product_amount'],2).'</td>
											</tr>
								';
											$i++; }
$htmlContent .= '		
										</table>
										<div style="width:50%; color:black; background-color:rgb(240,240,240); border:15px solid rgb(240,240,240); border-radius:8px; margin-top:30px;">
											<!-- Total price -->
											<h3>สรุปยอดการสนับสนุน</h3>
											<div class="d-flex">
												<div class="ml-auto font-weight-bold">'.number_format($price_total,2).' บาท </div>
											</div>
											<hr>
											<!-- Shipping choice confirmation -->
											<div class="d-flex">
												<h3>วิธีการจัดส่ง</h3>
												<label>
								';





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////



								/* ฟรีค่าจัดส่ง ระหว่าง 13 ก.ย. - 31 ธ.ค. 2563 */
								
								$htmlContent .= '
								ฟรีค่าจัดส่ง<br>
								ระยะเวลาการจัดส่ง: 3 - 7 วันทำการ
						';

/*							
												if($shipping_kind == 'normal') {
$htmlContent .= '
										+ ค่าจัดส่ง 50 บาท
										พัสดุลงทะเบียน<br>
										ระยะเวลาการจัดส่ง: 3 - 7 วันทำการ
								';
										}else{
$htmlContent .= '
										+ ค่าจัดส่ง 80 บาท
										พัสดุด่วนพิเศษ EMS<br>
										ระยะเวลาการจัดส่ง: 1 - 3 วันทำการ
								';
										}
*/




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////






$htmlContent .= '
												</label>
											</div>
											<hr>
											<!-- Grand Total (Total price + Shipping cost) -->
											<div class="d-flex gr-total">
												<div class="ml-auto h5">
													<h3>สรุปยอดทั้งหมด</h3>
														'.$grand_total.'
													บาท
													</div>
												</div>
												<hr>
												<!-- Shipping Address -->
												<div class="d-flex">
													<h3>ที่อยู่ในการจัดส่ง</h3>
													<div class="location-box">
														'.$shipping_address.'
													</div>
												</div>
												<hr>
												<!-- Receiver Name -->
												<div class="d-flex">
													<h3>ชื่อ-นามสกุล ผู้รับ</h3>
													'.$contact_name.'
												</div>
												<hr>
												<!-- Receiver Phone Number -->
												<div class="d-flex">
													<h3>เบอร์ติดต่อ ผู้รับ</h3>
													<div class="phonenum-box">
														'.$contact_number.'
													</div>
												</div>
											</div>
											<form action="https://www.thaisuprateacher.org/ส่งหลักฐานการโอนB.php" style="margin-top:30px">
												<button style="color: white; background-color: rgb(72,160,0); font-weight: 700; font-size:20px; border-radius:15px; padding:15px; cursor:pointer; text-align:center;">คลิกเพื่อแจ้งชำระเงิน</button>
											</form>
										</div>
									</body>
									</html>
								';
$flgSend = mail($strTo,$strSubject,$htmlContent,$strHeader);

/* Clear all data in cart */
$sqldelete_cart = "DELETE FROM `cart` WHERE email='$email' ";
$resdelete_cart= $con->query($sqldelete_cart) or die($con->error);

header('location: ประวัติและสถานะการสั่งซื้อ.php');

?>