<?php session_start(); ?>

<!DOCTYPE html>

<html lang="thai">
<!-- To declare your language - read more here: https://www.w3.org/International/questions/qa-html-language-declarations -->

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <div class="btn PreMenu_fl_right" style="padding:4px 35px;">
        <a href="สนับสนุนมูลนิธิฯ-en.php">Donate</a>
      </div>
      <!-- ################################################################################################ -->
      <nav id="mainav2" class="fl_right">
        <ul class="PreMenufaico2">
          <li class="faicon-login"><a href="login-en.php"><i class="fas fa-user-lock"></i> Log in</a></li>
          <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down" style="margin-right:10px;"></i><i class="fas fa-language"></i> English</a>
            <ul>
              <li><a href="หอเกียรติยศ008.php">ภาษาไทย</a></li>
              <!--<li><a href="#">Chinese</a></li>-->
            </ul>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <div class="search1 fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index-en.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index-en.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px; ">
          <li><a href="index-en.php">Home</a></li>
          <li><a href="เกี่ยวกับเรา-en.php">About us</a></li>
          <li><a href="ร่วมโครงการฯ-en.php">Join us</a></li>
          <li class="active"><a href="หอเกียรติยศรวมlatest-en.php">Hall of fame</a></li>
          <li><a href="ติดต่อเรา-en.php">Contact us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="หอเกียรติยศรวมlatest-en.php">Hall of Fame</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="หอเกียรติยศ008-en.php"> ครูจ้างอย่างเรา ก็สามารถสร้างเด็กดีได้...</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - หอเกียรติยศ008 -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <article style="text-align:center; margin-bottom:100px;">
      <p class="font-x3"><span style="color:rgb(180,147,31); font-family:CHULALONGKORNReg; line-height:80px;">
        <strong>"ครูจ้างอย่างเรา ก็สามารถสร้างเด็กดีได้ เพราะต้นกล้าต้นเล็กๆสามารถเติบโตเป็นต้นไม้ใหญ่ที่แข็งแรง และกลายเป็นป่าที่อุดมสมบูรณ์ได้"</strong>
      </p>
      <div class="bgvdo">
        <video width="100%" controls>
          <source src="images/วีดีโอครูจ้างสร้างเด็ก.mp4" type="video/mp4">
        </video>
      </div>
    </article>
    <p class="font-x3"><span style="color:rgb(180,147,31); font-family:CHULALONGKORNReg; line-height:30px; text-align:left;"><strong>บทความ</strong></span></p>
    <p class="font-x2" style="font-family:ChulaCharasNew; line-height:30pt; text-align:left;">
      ปัจจุบันมีครูอัตราจ้างอยู่เกือบทุกโรงเรียน เชื่อไหมค่ะว่า ครูอัตราจ้าง ก็สร้างเด็กดีได้ สวัสดีค่ะ ดิฉันนางเดือนเพ็ญ บัวศรี ครูโรงเรียนวัดโบสถ์สมพรชัย จังหวัดพระนครศรีอยุธยา โรงเรียนของหนูเป็นโรงเรียนขนาดกลาง 
      ติดกับแม่น้ำเจ้าพระยา มีนักเรียน 150 คน ส่วนใหญ่เป็นนักเรียนที่มีสัญชาติกัมพูชา เนื่องจากโรงเรียนอยู่ใกล้กับโรงงานปุ๋ยที่มีผู้ปกครองชาวกัมพูชาจำนวนมาก คิดเป็นร้อยละ 70 ของนักเรียนในโรงเรียน
      <br><br>
      การจุดประกายความคิดในการเข้าร่วมโครงการ คือเรามองเห็นความสำคัญและกิจกรรมที่เกิดขึ้นกับตัวเด็กนักเรียนโดยตรง สามารถเปลี่ยนแปลงพฤติกรรมไปในทางทีดีได้ ถึงเป็นแรงผลักอย่างหนึ่งในการเข้าร่วมโครงการ
      <br><br>
      การดำเนินการเราทำแบบ 9 ขั้นตอนในการสร้างเด็กดีของแผ่นดี มีการจะประกายความคิดสร้างแรงบันดาลใจ ในการเปลี่ยนตัวเอง หนูประทับใจเด็กกัมพูชาคนคนหนึ่ง คือเขาจะเปลี่ยนแปลงตัวเองโดยการมาโรงเรียนเช้าทุกวันในระยะเวลาในการทำกิจกรรม 21 วัน เป็นมีวัน 1 เขามาโรงเรียนสาย เขารู้สึกเสียใจมากที่เขามาช้า หนูก็บอกเขาว่าไม่เป็นไรแต่หลังจากนี้ไปให้ตั้งใจมาโรงเรียนเช้า หลังจากนั้นเขาทำกิจกรรมจนครบ 21 วัน แล้วเขาก็คิดว่า การทำความดีมันง่ายนิดเดียว เขาก็บอกกับหนูว่าหนูขอตั้งความดีเพิ่มอีก 1 ข้อได้ไหม หนูก็บอกว่าได้เลย เขามีความภูมิใจในตัวเอง
      <br><br>
      สุดท้ายนี้ ครูจ้างอย่างเรา ก็สามารถเพราะต้นกล้าต้นเล็กๆ ที่มีคุณภาพที่จะโตขึ้นเป็นต้นไม้ใหญ่ที่แข็งแรงและกลายเป็นป่าที่อุดมสมบูรณ์ สวัสดีค่ะ
  </main>
</div>
<!-- End Content 01 - หอเกียรติยศ008 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear">
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="font-family:CHULALONGKORNReg; color:rgb(2,83,165);"><strong style="line-height:50px;">Related Department</strong></p>
      <div class="one_half first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        <a class="font-x1 footercontact" href="http://www.thaisuprateacherdonate.org/">
          <img src="images/มูลนิธิครูดีของแผ่นดิน inwshop Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Souvenir</p>
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="font-family:CHULALONGKORNReg; color:rgb(2,83,165);"><strong style="line-height:50px;">Foundation of Thai Suprateacher</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพฯ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- End Copyright Tab -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Back2Top Button -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Back2Top Button -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>