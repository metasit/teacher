<?php 
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	$question_num = 6; // Set question no. for this page
	require('includes/header4fifth_evaluation4exe.php');
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการครูดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="m-t-90"></div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ประเมินครูดีของแผ่นดินชั้นที่5 -->
<div class="wrapper row3">
	<div class="hoc container clear">
		<!-- ################################################################################################ -->
		<div class="center">
			<h8 class="m-b-50">หมวด: ครองตน</h8>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-6 col-md-6 col-lg-6 fs-20">
				<p>ข้อที่ / ทั้งหมด</p>
				<p class="bold"><?php echo $question_num; ?> / 32</p>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 right fs-20">
				<p>เหลือเวลา</p>
				<div>
					<p class="bold m-t-1" id="timer" onload="localStorage.getItem('currenttime')">.</p> วินาที
				</div>
			</div>
		</div>

		<div class="row p-t-20 p-l-10 m-t-20" style="border-top:2px solid rgb(16,192,0); border-radius:50px;">
			<div class="col-md-12 col-lg-12">
				<form action="addfifth_score_A-ผู้บริหาร-ครู.php" method="GET">
					<div class="fs-20 inline" style="text-align:left;">
						<p><strong><?php echo $question_num; ?>. ท่านมีพฤติกรรมที่แสดงออกถึงการมีจิตอาสา/จิตสาธารณะ ตรงกับข้อใดมากที่สุด (ตัวอย่างกิจกรรมจิตอาสา/จิตสาธารณะ เช่น การอนุรักษ์สิ่งแวดล้อม การรักษาสาธารณสมบัติ การประหยัดพลังงาน การช่วยเหลืองานบุญ)</strong></p>
						<br>
						<input type="radio" id="choice1" name="fifth_score_A" value="1" required>
						<label for="choice1">ปฏิบัติจิตอาสา/จิตสาธารณะตามที่ได้รับมอบหมาย</label>
						<br>
						<input type="radio" id="choice2" name="fifth_score_A" value="2" required>
						<label for="choice2">ปฏิบัติจิตอาสา/จิตสาธารณะตามที่ได้รับมอบหมาย และเข้าร่วมกิจกรรมจิตอาสา/จิตสาธารณะอื่นๆ โดยไม่หวังผลตอบแทน</label>
						<br>
						<input type="radio" id="choice3" name="fifth_score_A" value="3" required>
						<label for="choice3">ปฏิบัติจิตอาสา/จิตสาธารณะตามที่ได้รับมอบหมาย เข้าร่วม และชักชวนผู้อื่นทำกิจกรรมจิตอาสา/จิตสาธารณะอื่นๆ โดยไม่หวังผลตอบแทน</label>
						<br>
						<input type="radio" id="choice4" name="fifth_score_A" value="4" required>
						<label for="choice4">ปฏิบัติจิตอาสา/จิตสาธารณะตามที่ได้รับมอบหมาย เข้าร่วม ชักชวนผู้อื่น และเป็นผู้ริเริ่มกิจกรรมจิตอาสา/จิตสาธารณะอื่นๆ โดยไม่หวังผลตอบแทน</label>
						<br>
						<input type="radio" id="choice5" name="fifth_score_A" value="5" required>
						<label for="choice5">เป็นต้นแบบ และเป็นผู้นำการปฏิบัติจิตอาสา/จิตสาธารณะแก่บุคลากรในโรงเรียนและชุมชน</label>
						<br>
					</div>

					<button type="submit" id="nextBtn" class="btnJoin" style="color:white; cursor:pointer; width:80%; margin-top:50px;"><h7>ข้อถัดไป</h7></button>
					<input type="hidden" id="CFP" name="CFP" value="<?php echo $question_num; ?>">
				</form>
			</div>
		</div>
	</div>
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ประเมินครูดีของแผ่นดินชั้นที่5 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>
<!-- Countdown Timer -->
<script src="js/countdowntimer4fifth_evaluation4exe.js"></script>
<!-- Disable Back btn -->
<script src="js/disablebackbtn4fifth_evaluation4exe.js"></script>

</body>
</html>