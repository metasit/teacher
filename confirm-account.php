<?php
	session_start();
    require_once('condb.php');
    
	if(isset($_GET['id'])) {
        $ID = $_GET['id'];
		// echo "<script>window.history.go(-1)</script>";
	}
    else if(isset($_SESSION['id_account'])){
        $ID = $_SESSION['id_account'];
    }
    else{
		echo "<script>window.history.go(-1)</script>";
	}
    

        $sql_list = " SELECT * FROM login WHERE ID = '$ID' ";
        $list = $con->query($sql_list);
        while($row = $list->fetch_assoc()) {
            $doc = explode(" ",$row['docaddress']);
            $email = $row['email'];
            $prename = $row['prename'];
            $firstname = $row['firstname'];
            $lastname = $row['lastname'];
            $phonenum = $row['phonenum'];
            $docaddress = $row['docaddress'];
            $birthdate = $row['birthdate'];
            $ID = $row['ID'];
            $occup_code = $row['occup_code'];
            $code_persanal = "";
            $province = $doc[0];
            $district = $doc[1];
            $district_sub = $doc[2];
            $zip_code = $doc[3];
            // if($row['prename'] == 'A') {
            //     $prename = 'นาย';
            // }elseif($row['prename'] == 'B') {
            //     $prename = 'นาง';
            // }elseif($row['prename'] == 'C') {
            //     $prename = 'นางสาว';
            // }elseif($row['prename'] == 'D') {
            //     $prename = 'ด.ช.';
            // }elseif($row['prename'] == 'E') {
            //     $prename = 'ด.ญ.';
            // }elseif($row['prename'] == 'O') {
            //     $prename = $row['prename_remark'];
            // }
        }        
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
<style>
input[type="date"]:before {

content: " วันเกิด  " !important;
margin-right:15px;

}
input[type=date]:required:invalid::-webkit-datetime-edit {
    color: transparent;
}
input[type=date]:focus::-webkit-datetime-edit {
    color: black !important;
}
</style>
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<?php

?>
<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100">
			<!-- ################################################################################################ -->
			<div style="display:none; class=" textlink3 PreMenu_fl_left">
				<a href="index.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าหลัก</a>
			</div>
			<!-- ################################################################################################ -->
			<div class="textlink3 PreMenu_fl_right" style="padding:2px;">
				<a href="login.php" style="display:none;">ไปหน้าเข้าสู่ระบบสมาชิก</a>
			</div>
			<!-- ################################################################################################ -->
			<div class="login100-pic">
				<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
			</div>
			<!-- ################################################################################################ -->
			<form enctype="multipart/form-data" id="Ocform" class="login100-form validate-form" name="formsignup" action="check-confirm.php" method="POST">
                <input type="hidden" name="ID" value="<?php echo $ID ?>">
				<!-- ################################################################################################ -->
				<span class="login100-form-title">
					ระบบยืนยันตัวตน
				</span>

                <div class="login100-form-title">
                    ตรวจสอบ และยืนยันการใช้ชื่อ-นามสกุล, อาชีพ, ตำแหน่ง และสังกัด(หลังจากเปลี่ยนหรือยืนยันการใช้ข้อมูลทั้งหมดนี้ ท่านจะไม่สามารถเปลี่ยนแปลงได้อีกต่อไป โปรดตรวจสอบข้อมูลอย่างละเอียด)
                </div>
				<!-- ################################################################################################ -->
				<div class="wrap-input100 validate-input">
					<input  
                    value="<?php echo $email; ?>"
                    class="input100" type="email" name="email" class="form-control" placeholder="อีเมล" 
					pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$" title="โปรดใส่อีเมลให้ถูกต้อง" required />
					<span class="symbol-input100">
						<i class="fas fa-user" aria-hidden="true"></i>
					</span>
				</div>
				<!-- ################################################################################################ -->
				<!-- <div class="wrap-input100 validate-input">
					<input class="input100" type="password" name="password" class="form-control" placeholder="รหัสผ่าน" 
					pattern=".{8,}" title="โปรดใส่จำนวนรหัสผ่านเท่ากับหรือมากกว่า 8 ตัว" required />
					<span class="symbol-input100">
						<i class="fa fa-lock" aria-hidden="true"></i>
					</span>
				</div> -->
				<!-- ################################################################################################ -->
				<!-- <div class="wrap-input100 validate-input">
					<input class="input100" type="password" name="conpassword" class="form-control" placeholder="กรอกรหัสผ่านอีกครั้ง" 
					pattern=".{8,}" title="โปรดใส่จำนวนรหัสผ่านเท่ากับหรือมากกว่า 8 ตัว" required />
					<span class="symbol-input100">
						<i class="fa fa-lock" aria-hidden="true"></i>
					</span>
				</div> -->
				<!-- ################################################################################################ -->
				<select name="pre_name" class="form-control" style="margin-bottom:10px; height:40px" onchange="PickPrename(this.value);" required>
					<option value="" disabled="disabled" selected="selected">เลือกคำหน้านาม</option>
					<option <?php if($prename=="A") echo "selected";  ?> value="A">นาย</option>
					<option <?php if($prename=="B") echo "selected";  ?> value="B">นาง</option>
					<option <?php if($prename=="C") echo "selected";  ?> value="C">นางสาว</option>
					<option <?php if($prename=="D") echo "selected";  ?> value="D">ด.ช.</option>
					<option <?php if($prename=="E") echo "selected";  ?> value="E">ด.ญ.</option>
					<option <?php if($prename=="O") echo "selected";  ?> value="O">อื่นๆ</option>
				</select>

				

				<div id="pre_name_text" class="wrap-input100 validate-input" style="display:none">
					<input 
                    value =""
                    class="input100" type="text" 
                    id="pre_name_text_required" name="pre_name_text" class="form-control" placeholder="โปรดระบุคำหน้านาม" max-lenght="30" />
					<span class="symbol-input100">
						<i class="fas fa-file-signature" aria-hidden="true"></i>
					</span>
				</div>
				<!-- ################################################################################################ -->
				<div class="wrap-input100 validate-input">
					<input 
                    value="<?php  echo $firstname; ?>"
                    class="input100" type="text" name="firstname" class="form-control" placeholder="ชื่อ (ไม่ต้องใส่คำนำหน้า)" max-lenght="100" required />
					<span class="symbol-input100">
						<i class="fas fa-file-signature" aria-hidden="true"></i>
					</span>
				</div>
				<!-- ################################################################################################ -->
				<div class="wrap-input100 validate-input">
					<input 
                    value="<?php  echo $lastname; ?>"
                    class="input100" type="text" name="lastname" class="form-control" placeholder="นามสกุล" max-lenght="100" required />
					<span class="symbol-input100">
						<i class="fas fa-file-signature" aria-hidden="true"></i>
					</span>
				</div>



				<!-- ################################################################################################ -->
				<div class="wrap-input100 validate-input">
					<input 
					id="tel_ck"
                    value="<?php echo $phonenum; ?>"
                    class="input100" type="text" name="phonenum" class="form-control"
                    placeholder="เบอร์โทรศัพท์" pattern="[0][0-9]{9}" maxlength="10"
					title="โปรดใส่จำนวนตัวเลขให้ครบ 10 ตัว (รวมเลข 0 ตัวแรก)" required />
					<span class="symbol-input100">
						<i class="fas fa-mobile-alt" aria-hidden="true"></i>
					</span>
					<em>
				</div>

                <select name="position" class="form-control" style="margin-bottom:10px; height:40px" onchange="PickPrename(this.value);" required>
					<option   value="" disabled="disabled" selected="selected">ตำแหน่ง</option>
					<option  <?php  if($occup_code == "OcAAAA") echo "selected";   ?> value="OcAAAA">ครูอัตราจ้าง</option>
					<option  <?php  if($occup_code == "OcAAAB") echo "selected";   ?>  value="OcAAAB">ครูผู้ช่วย</option>
					<option  <?php  if($occup_code == "OcAAAD") echo "selected";   ?>  value="OcAAAD">ครูชำนาญการ</option>
					<option  <?php  if($occup_code == "OcAAAE") echo "selected";   ?>  value="OcAAAE">ครูชำนาญการพิเศษ</option>
					<option  <?php  if($occup_code == "OcAAAG") echo "selected";   ?>  value="OcAAAG">ครูเชี่ยวชาญพิเศษ</option>
                    <option <?php  if($occup_code == "OcAABB") echo "selected";   ?>  value="OcAABB">ครู กศน.ตำบล</option>
                    <option <?php  if($occup_code == "OcAABC") echo "selected";   ?>  value="OcAABC">ครู ศูนย์การเรียนรู้ชุมชน</option>
                    <option <?php  if($occup_code == "OcAABD") echo "selected";   ?>  value="OcAABD">ครู อาสาสมัคร</option>
                    <option <?php  if($occup_code == "OcAABE") echo "selected";   ?>  value="OcAABE">ครู สอนคนพิการ</option>
                    <option <?php  if($occup_code == "OcAABF") echo "selected";   ?>  value="OcAABF">ครู ประกาศนียบัตรวิชาชีพ</option>
                    <option <?php  if($occup_code == "OcAABO") echo "selected";   ?>  value="OcAABO">อื่น ๆ ระบุ....</option>
                    <option <?php  if($occup_code == "OcAADA") echo "selected";   ?>  value="OcAADA">อาจารย์</option>
                    <option <?php  if($occup_code == "OcAADB") echo "selected";   ?>  value="OcAADB">ผู้ช่วยศาสตราจารย์</option>
                    <option <?php  if($occup_code == "OcAADC") echo "selected";   ?>  value="OcAADC">รองศาสตราจารย์</option>
                    <option <?php  if($occup_code == "OcAADD") echo "selected";   ?>  value="OcAADD">ศาสตราจารย์</option>
                    <option <?php  if($occup_code == "OcABAA") echo "selected";   ?>  value="OcABAA">รองผู้อำนวยการโรงเรียนเอกชน</option>
                    <option <?php  if($occup_code == "OcABAB") echo "selected";   ?>  value="OcABAB">รองผู้อำนวยการชำนาญการ</option>
                    <option <?php  if($occup_code == "OcABAC") echo "selected";   ?>  value="OcABAC">รองผู้อำนวยการชำนาญการพิเศษ</option>
                    <option <?php  if($occup_code == "OcABAD") echo "selected";   ?>  value="OcABAD">รองผู้อำนวยการเชี่ยวชาญ</option>
                    <option <?php  if($occup_code == "OcABBA") echo "selected";   ?>  value="OcABBA">ผู้อำนวยการโรงเรียนเอกชน</option>
                    <option <?php  if($occup_code == "OcABBB") echo "selected";   ?>  value="OcABBB">ผู้อำนวยการชำนาญการ</option>
                    <option <?php  if($occup_code == "OcABBC") echo "selected";   ?>  value="OcABBC">ผู้อำนวยการชำนาญการพิเศษ</option>
                    <option <?php  if($occup_code == "OcABBD") echo "selected";   ?>  value="OcABBD">ผู้อำนวยการเชี่ยวชาญ</option>
                    <option <?php  if($occup_code == "OcABBE") echo "selected";   ?>  value="OcABBE">ผู้อำนวยการเชี่ยวชาญพิเศษ</option>
                    <option <?php  if($occup_code == "OcACAA") echo "selected";   ?>  value="OcACAA">รองผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาชำนาญการพิเศษ</option>
                    <option <?php  if($occup_code == "OcACAB") echo "selected";   ?>  value="OcACAB">รองผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญ</option>
                    <option <?php  if($occup_code == "OcACAC") echo "selected";   ?>  value="OcACAC">ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญ</option>
                    <option <?php  if($occup_code == "OcACAD") echo "selected";   ?>  value="OcACAD">ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญพิเศษ</option>
                    <option <?php  if($occup_code == "OcACBA") echo "selected";   ?>  value="OcACBA">รองศึกษาธิการจังหวัด</option>
                    <option <?php  if($occup_code == "OcACBB") echo "selected";   ?>  value="OcACBB">ศึกษาธิการจังหวัด</option>
                    <option <?php  if($occup_code == "OcACBC") echo "selected";   ?>  value="OcACBC">รองศึกษาธิการภาค</option>
                    <option <?php  if($occup_code == "OcACBD") echo "selected";   ?>  value="OcACBD">ศึกษาธิการภาค</option>
                    <option <?php  if($occup_code == "OcACCA") echo "selected";   ?>  value="OcACCA">รองผู้อำนวยการสำนัก</option>
                    <option <?php  if($occup_code == "OcACCA") echo "selected";   ?>  value="OcACCA">ผู้อำนวยการสำนัก</option>
                    <option <?php  if($occup_code == "OcACCA") echo "selected";   ?>  value="OcACCA">ผู้เชี่ยวชาญ</option>
                    <option <?php  if($occup_code == "OcACCA") echo "selected";   ?>  value="OcACCA">ผู้ตรวจราชการ/ที่ปรึกษาระดับ 10</option>
                    <option <?php  if($occup_code == "OcACCA") echo "selected";   ?>  value="OcACCA">ปลัด/รองปลัด/อธิบดี/รองอธิบดี</option>
                    <option <?php  if($occup_code == "OcACDA") echo "selected";   ?>  value="OcACDA">รองคณบดี</option>
                    <option <?php  if($occup_code == "OcACDB") echo "selected";   ?>  value="OcACDB">คณบดี</option>
                    <option <?php  if($occup_code == "OcACDC") echo "selected";   ?>  value="OcACDC">รองอธิการบดี</option>
                    <option <?php  if($occup_code == "OcACDD") echo "selected";   ?>  value="OcACDD">อธิการบดี</option>
                    <option <?php  if($occup_code == "OcADAA") echo "selected";   ?>  value="OcADAA">ศึกษานิเทศก์ชำนาญการ</option>
                    <option <?php  if($occup_code == "OcADAB") echo "selected";   ?>  value="OcADAB">ศึกษานิเทศก์ชำนาญการพิเศษ</option>
                    <option <?php  if($occup_code == "OcADAC") echo "selected";   ?>  value="OcADAC">ศึกษานิเทศก์เชี่ยวชาญ</option>
                    <option <?php  if($occup_code == "OcADAD") echo "selected";   ?>  value="OcADAD">ศึกษานิเทศก์เชี่ยวชาญพิเศษ</option>
                    <option <?php  if($occup_code == "OcAO") echo "selected";   ?>  value="OcAO">อื่นๆ ระบุ </option>
				</select>
                <select name="affiliation_id" id="affiliation_id" class="form-control" style="height:40px;" required>
						<option value="" disabled="disabled" selected="selected">เลือกสังกัด</option>
						<!--<option value="AfA">สำนักงานปลัดกระทรวงศึกษาธิการ</option>-->
						<option value="AfB">สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน</option>
						<option value="AfC">สำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน</option>
						<option value="AfD">สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย</option>
						<option value="AfE">สำนักงานคณะกรรมการการอาชีวศึกษา</option>
						<option value="AfF">สำนักงานคณะกรรมการการอุดมศึกษา</option>
						<option value="AfG">กระทรวงมหาดไทย กรมส่งเสริมการปกครองส่วนท้องถิ่น</option>
						<option value="AfH">กรุงเทพมหานคร</option>
						<option value="AfI">เมืองพัทยา</option>
						<option value="AfJ">สำนักงานตำรวจแห่งชาติ</option>
						<option value="AfO">อื่นๆ โปรดระบุ....</option>
					</select>
					<label for="affsub_id" id="affsubhead" class="center m-t-10" style="display:none">สังกัดย่อย</label>
					<select name="affsub_id" id="affsub_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
					</select>
					<div id="affsub_text" style="display:none">
						<div class="wrap-input100 m-t-10">

						<input class="input100" type="text"
						 name="affsub_ans_idx"  value="สังกัด"
						id="affsub_ans_id2" class="form-control" placeholder="โปรดระบุสังกัด" max-lenght="100" required />
						
						<!-- <input class="input100" type="text" name="affsub_ans_id" id="affsub_ans_id" class="form-control" placeholder="โปรดระบุสังกัด111" max-lenght="100" required /> -->
							<!-- <input require class="input100" name="affsub_ans_id" id="affsub_ans_id" type="text" class="form-control" placeholder="โปรดระบุสังกัด" /> -->
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div>
					<select name="affsub2_id" id="affsub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
					</select>
					<select name="affsub3_id" id="affsub3_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
					</select>
					<select name="affsub4_id" id="affsub4_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>
					</select>
					<!-- <div id="affsub4_text" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="affsub4_ans_idx" id="affsub4_ans_id" type="text" class="form-control" placeholder="โปรดระบุสังกัด" required/>
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div> -->


				<!-- ################################################################################################ -->
				<!-- <div class="wrap-input100 validate-input mt-2">
					<input 
                    value="<?php echo $docaddress; ?>"
                    class="input100" type="text" name="docaddress" class="form-control" placeholder="ที่อยู่สำหรับส่งเอกสาร" required />
					<span class="symbol-input100">
						<i class="fas fa-address-book" aria-hidden="true"></i>
					</span>
				</div> -->
				<div class="font-10 font-weight-bold text-dark mb-2 pl-2 mt-3 ">ที่อยู่จัดส่งเอกสาร</div>
				<div class="wrap-input100 validate-input mt-3">
					<input class="input100" type="text" name="docaddress" class="form-control" placeholder="ที่อยู่จัดส่งเอกสาร" max-lenght="100" required />
					<span class="symbol-input100">
						<i class="fas fa-file-signature" aria-hidden="true"></i>
					</span>
				</div>

				<div class=" mt-3 wrap-input100 validate-input">
				<select id="province_token"   name="province" class="form-control" style="margin-bottom:10px; height:40px"  required>
					<option value="" selected> เลือกจังหวัด </option>
					<option value="กรุงเทพมหานคร">กรุงเทพมหานคร</option>
					<option value="กระบี่">กระบี่ </option>
					<option value="กาญจนบุรี">กาญจนบุรี </option>
					<option value="กาฬสินธุ์">กาฬสินธุ์ </option>
					<option value="กำแพงเพชร">กำแพงเพชร </option>
					<option value="ขอนแก่น">ขอนแก่น</option>
					<option value="จันทบุรี">จันทบุรี</option>
					<option value="ฉะเชิงเทรา">ฉะเชิงเทรา </option>
					<option value="ชัยนาท">ชัยนาท </option>
					<option value="ชัยภูมิ">ชัยภูมิ </option>
					<option value="ชุมพร">ชุมพร </option>
					<option value="ชลบุรี">ชลบุรี </option>
					<option value="เชียงใหม่">เชียงใหม่ </option>
					<option value="เชียงราย">เชียงราย </option>
					<option value="ตรัง">ตรัง </option>
					<option value="ตราด">ตราด </option>
					<option value="ตาก">ตาก </option>
					<option value="นครนายก">นครนายก </option>
					<option value="นครปฐม">นครปฐม </option>
					<option value="นครพนม">นครพนม </option>
					<option value="นครราชสีมา">นครราชสีมา </option>
					<option value="นครศรีธรรมราช">นครศรีธรรมราช </option>
					<option value="นครสวรรค์">นครสวรรค์ </option>
					<option value="นราธิวาส">นราธิวาส </option>
					<option value="น่าน">น่าน </option>
					<option value="นนทบุรี">นนทบุรี </option>
					<option value="บึงกาฬ">บึงกาฬ</option>
					<option value="บุรีรัมย์">บุรีรัมย์</option>
					<option value="ประจวบคีรีขันธ์">ประจวบคีรีขันธ์ </option>
					<option value="ปทุมธานี">ปทุมธานี </option>
					<option value="ปราจีนบุรี">ปราจีนบุรี </option>
					<option value="ปัตตานี">ปัตตานี </option>
					<option value="พะเยา">พะเยา </option>
					<option value="พระนครศรีอยุธยา">พระนครศรีอยุธยา </option>
					<option value="พังงา">พังงา </option>
					<option value="พิจิตร">พิจิตร </option>
					<option value="พิษณุโลก">พิษณุโลก </option>
					<option value="เพชรบุรี">เพชรบุรี </option>
					<option value="เพชรบูรณ์">เพชรบูรณ์ </option>
					<option value="แพร่">แพร่ </option>
					<option value="พัทลุง">พัทลุง </option>
					<option value="ภูเก็ต">ภูเก็ต </option>
					<option value="มหาสารคาม">มหาสารคาม </option>
					<option value="มุกดาหาร">มุกดาหาร </option>
					<option value="แม่ฮ่องสอน">แม่ฮ่องสอน </option>
					<option value="ยโสธร">ยโสธร </option>
					<option value="ยะลา">ยะลา </option>
					<option value="ร้อยเอ็ด">ร้อยเอ็ด </option>
					<option value="ระนอง">ระนอง </option>
					<option value="ระยอง">ระยอง </option>
					<option value="ราชบุรี">ราชบุรี</option>
					<option value="ลพบุรี">ลพบุรี </option>
					<option value="ลำปาง">ลำปาง </option>
					<option value="ลำพูน">ลำพูน </option>
					<option value="เลย">เลย </option>
					<option value="ศรีสะเกษ">ศรีสะเกษ</option>
					<option value="สกลนคร">สกลนคร</option>
					<option value="สงขลา">สงขลา </option>
					<option value="สมุทรสาคร">สมุทรสาคร </option>
					<option value="สมุทรปราการ">สมุทรปราการ </option>
					<option value="สมุทรสงคราม">สมุทรสงคราม </option>
					<option value="สระแก้ว">สระแก้ว </option>
					<option value="สระบุรี">สระบุรี </option>
					<option value="สิงห์บุรี">สิงห์บุรี </option>
					<option value="สุโขทัย">สุโขทัย </option>
					<option value="สุพรรณบุรี">สุพรรณบุรี </option>
					<option value="สุราษฎร์ธานี">สุราษฎร์ธานี </option>
					<option value="สุรินทร์">สุรินทร์ </option>
					<option value="สตูล">สตูล </option>
					<option value="หนองคาย">หนองคาย </option>
					<option value="หนองบัวลำภู">หนองบัวลำภู </option>
					<option value="อำนาจเจริญ">อำนาจเจริญ </option>
					<option value="อุดรธานี">อุดรธานี </option>
					<option value="อุตรดิตถ์">อุตรดิตถ์ </option>
					<option value="อุทัยธานี">อุทัยธานี </option>
					<option value="อุบลราชธานี">อุบลราชธานี</option>
					<option value="อ่างทอง">อ่างทอง </option>
					<option value="อื่นๆ">อื่นๆ</option>
				</select>
					<!-- <input class="input100" type="text" name="province" class="form-control" placeholder="จังหวัด" required /> -->

				</div>
				
				
				<div class="wrap-input100 validate-input">

				<!-- <select name="district" id="district" class="  d-none form-control" require>
					<option value="">เลือกอำเภอ</option>
				</select> -->
				<select name="district" id="district" class=" d-none form-control" style="margin-bottom:10px; height:40px"  required>
					<option value="" disabled="disabled" selected="selected">เลือกอำเภอ</option>
				</select>

				<!-- <input class="input100" type="text" name="district_sub" class="form-control" placeholder="อำเภอ" required />
					<span class="symbol-input100">
						<i class="fas fa-address-book" aria-hidden="true"></i>
					</span> -->


				</div>
				
				<div class="wrap-input100 validate-input">
					<!-- <select name="district_sub" id="district_sub" class=" d-none form-control"> -->
						<!-- <option value="">เลือกอำเภอ</option> -->
					<!-- </select>	 -->

				<select name="district_sub" id="district_sub" class=" d-none  form-control" style="margin-bottom:10px; height:40px"  required>
					<!-- <option value="" disabled="disabled" selected="selected">เลือกอำเภอ</option> -->
				</select>

				<!-- <input class="input100" type="text" name="district" class="form-control" placeholder="ตำบล" required />
					<span class="symbol-input100">
						<i class="fas fa-address-book" aria-hidden="true"></i>
					</span> -->
				</div>


				<div class="wrap-input100 validate-input">
					<input  class="input100" type="text" id="zip_code_token" name="zipcode" class="form-control" placeholder="รหัสไปรษณีย์" required />
					<span class="symbol-input100">
						<i class="fas fa-address-book" aria-hidden="true"></i>
					</span>
				</div>



				<!-- ################################################################################################ -->
				<div class="wrap-input100 validate-input">
					<input 
                    value="<?php echo $birthdate;  ?>"
                    class="input100" type="date" name="birthdate" class="form-control" placeholder="วันเกิด"
					min='1950-01-01' max="<?php echo date('Y-m-d'); ?>" required />
					<span class="symbol-input100">
						<i class="fas fa-birthday-cake" aria-hidden="true"></i>
					</span>
				</div>

                <div class="wrap-input100 validate-input">
					<input 
                    plh
                    accept="image/x-png,image/gif,image/jpeg"
                    class="input100" style="padding-top:12px;"  
                    placeholder = ""
                    type="file" name="code_people" class="form-control" 
                    required />
					<span class="symbol-input100">
						<i class="fas fa-address-book" aria-hidden="true"></i>
					</span>
				</div>
                <div  class=" pl-4 my-2 text-dark font-10 font-weight-bold mb-0"> ถ่ายรูปหน้าตัวเองกับบัตรประชาชน  (เพื่อการตรวจสอบตัวตนที่ถูกต้อง)</div>
                
                <div class="wrap-input100 validate-input">
					<input 
					id="code_persanal"
                    value="<?php  echo  $code_persanal; ?>"
                    class="input100" 
                    type="number"
                    maxlength="13"
                    name="code_persanal"
                    class="form-control"
                    placeholder="เลขบัตรประชาชน 13 หลัก"
                    max-lenght="100" 
                    required />
					<span class="symbol-input100">
						<i class="fas fa-file-signature" aria-hidden="true"></i>
					</span>
				</div>
                
                <input type="hidden" name="CFP" value="20">
                <!-- <label for="affiliation_id" class="center m-t-10 text-black bold">สังกัด</label> -->
				

				<!-- ################################################################################################ -->
				<div class="container-login100-form-btn">
					<button class="login100-form-btn" type="submit" id="register">
                        ยืนยันตัวตน
					</button>
				</div>
				<!-- ################################################################################################ -->
				
					<!-- ################################################################################################ -->
					<!-- ################################################################################################ -->
					<!-- Start show step -->
					<!-- <div class="center">
						<span class="dot BG-green1"></span>
						<span class="line"></span>
						<span class="dot"></span>
						<span class="line"></span>
						<span class="dot"></span>
					</div> -->
					<!-- <div class="left">
						<p style="color:rgb(94,177,26);">ขั้นตอนที่ 1: เลือกอาชีพ และตำแหน่ง</p>
					</div> -->
					<!-- End show step -->
					<!-- ################################################################################################ -->
					<!-- Start อาชีพ, ตำแหน่ง Content -->


					<!-- ################################################################################################ -->

					<!-- End show step -->
					<!-- ################################################################################################ -->
					<!-- Start สังกัด Content -->
					<!-- เลือกสังกัด -->

					<!-- End สังกัด Content -->
					<!-- <div class="container-login100-form-btn m-t-20">
						<a href="สมัครเข้าร่วมโครงการ-ตำแหน่ง.php" class="login100-form-btn" style="width:30%; margin:0 10px;">ย้อนกลับ</a>
						<button class="login100-form-btn" style="width:40%; margin:0 10px;" type="submit" id="register">สมัครเข้าร่วมโครงการ</button>
					</div> -->




					<!-- เลือกอาชีพ -->
					<!-- <label for="Oc" class="center m-t-10 text-black bold">อาชีพ</label>
					<select id="Oc" name="occupation_id" class="form-control" style="height:40px" onchange="PickOccupation(this.value);" required>
						<option value="" disabled="disabled" selected="selected">เลือกอาชีพ</option>
						<option value="OcA">ข้าราชการ พนักงานราชการ ลูกจ้างประจำ อัตราจ้าง ประเภทครูและบุคลากรทางการศึกษา</option>
						<option value="OcB">ข้าราชการ พนักงานราชการ ลูกจ้างประจำ อัตราจ้าง ที่ไม่ใช่ประเภทครูและบุคลากรทางการศึกษา</option>
						<option value="OcC">ข้าราชการบำนาญ/เกษียณ</option>
						<option value="OcD">เจ้าของธุรกิจ</option>
						<option value="OcE">พนักงานบริษัท/รับจ้าง</option>
						<option value="OcF">นักเรียน/นักศึกษา</option>
						<option value="OcG">อาชีพอิสระ</option>
						<option value="OcO">อื่นๆ โปรดระบุ....</option>
					</select> -->
					<!-- เลือกตำแหน่งหลัก -->
					<label for="position" id="pohead" class="center m-t-10 text-black bold" style="display:none">ตำแหน่ง</label>
					<!-- OcA List -->
					<!-- <select id="pomain4OcA" name="pomain_id" class="form-control" style="display:none; height:40px;" onchange="PickPoMain4OcA(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ผู้ปฏิบัติหน้าที่สอน</option>
						<option value="B">ผู้บริหารสถานศึกษา</option>
						<option value="C">ผู้บริหารการศึกษา</option>
						<option value="D">บุคลากรทางการศึกษาอื่น</option>
					</select> -->


					<!-- OcB List -->
					<!-- <div id="pomain4OcB" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcB1" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcB2" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcB3" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่) *" />
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
						</div>
					</div> -->
					<!-- OcC List -->
					<!-- <div id="pomain4OcC" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcC1" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcC2" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcC3" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่) *" />
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
						</div>
					</div> -->
					<!-- OcD List -->
					<!-- <div id="pomain4OcD" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcD1" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcD2" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcD3" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่) *" />
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
						</div>
					</div> -->
					<!-- OcE List -->
					<!-- <div id="pomain4OcE" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcE1" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcE2" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcE3" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่) *" />
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
						</div>
					</div> -->
					<!-- OcF List -->
					<!-- <select id="pomain4OcF" name="pomain_id" class="form-control" style="display:none; height:40px;" onchange="PickPoMain4OcF(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ประธาน/รองประธาน/กรรมการ นักเรียน-นักศึกษา</option>
						<option value="B">ประธาน/รองประธาน/กรรมการ ชมรม-ชุมนุม-สโมสร-กิจกรรม</option>
						<option value="C">หัวหน้า/รองหัวหน้า ห้อง-กลุ่ม-ชั้นปี</option>
						<option value="D">นักเรียน/นักศึกษาทั่วไป</option>
					</select> -->
					<!-- OcG List -->
					<!-- <div id="pomain4OcG" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcG1" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcG2" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcG3" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่) *" />
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
						</div>
					</div> -->
					<!-- OcO List -->
					<!-- <div id="pomain4OcO" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcO1" class="form-control" placeholder="ตำแหน่งในสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcO2" class="form-control" placeholder="ชื่อสถานศึกษา/บริษัท/องค์กร" />
							<span class="symbol-input100"><i class="fas fa-school" aria-hidden="true"></i></span>
						</div>
						<div class="wrap-input100 m-t-10">
							<input class="input100" type="text" name="pomain_id_ansOcO3" class="form-control" placeholder="ที่อยู่สถานศึกษา/บริษัท/องค์กร (เลขที่) *" />
							<span class="symbol-input100"><i class="fas fa-building" aria-hidden="true"></i></span>
						</div>
					</div> -->
					<!-- เลือกตำแหน่งย่อยขั้น1 -->
					<!-- OcAA List -->
					<!-- <select id="posub4OcAA" name="posub1_id" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAA(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ครู</option>
						<option value="B">ครู กศน.</option>
						<option value="C">ครู ตชด.</option>
						<option value="D">อาจารย์</option>
						<option value="O">อื่นๆ ระบุ</option>
					</select> -->
					<!-- OcAB List -->
					<!-- <select id="posub4OcAB" name="posub1_id" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAB(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองผู้อำนวยการสถานศึกษา</option>
						<option value="B">ผู้อำนวยการสถานศึกษา</option>
						<option value="C">เจ้าของสถานศึกษา/ผู้รับใบอนุญาต</option>
						
						<option value="E">ครูใหญ่ โรงเรียน ตชด.</option>
						<option value="O">อื่นๆ ระบุ</option>
					</select> -->
					<!-- OcAC List -->
					<!-- <select id="posub4OcAC" name="posub1_id" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAC(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">สำนักงานเขตพื้นที่การศึกษา</option>
						<option value="B">ศึกษาธิการ</option>
						<option value="C">ผู้บริหารส่วนกลางกระทรวง</option>
						<option value="D">สถาบันอุดมศึกษา</option>
						<option value="E">สำนักงาน กศน.</option>
						<option value="F">สำนักงานการศึกษาเอกชน</option>
						<option value="O">อื่นๆ ระบุ</option>
					</select> -->
					<!-- OcAD List -->
					<!-- <select id="posub4OcAD" name="posub1_id" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAD(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ศึกษานิเทศก์</option>
						<option value="O">อื่นๆ ระบุ</option>
					</select> -->
					<!-- OcAO List ****************** ************* ***************** ************************ --> 
					<!-- <div id="posub4OcAO" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub1_id_ans" type="text" class="form-control" placeholder="ตำแหน่งXX" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div> -->
					<!-- OcAO List ****************** ************* ***************** ************************ --> 
					<!-- เลือกตำแหน่งย่อยขั้น2 -->
					<!-- OcAAA List -->
					<!-- <select id="posub4OcAAA" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="OcAAAA">ครูอัตราจ้าง</option>
						<option value="OcAAAB">ครูผู้ช่วย</option>
						<option value="OcAAAC">ครู</option>
						<option value="OcAAAD">ครูชำนาญการ</option>
						<option value="OcAAAE">ครูชำนาญการพิเศษ</option>
						<option value="OcAAAF">ครูเชี่ยวชาญ</option>
						<option value="OcAAAG">ครูเชี่ยวชาญพิเศษ</option>
					</select> -->
					<!-- OcAAB List -->
					<!-- <select id="posub4OcAAB" name="posub2_id" class="form-control" style="display:none; height:40px;" onchange="PickPoSub4OcAAB(this.value);">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ครู</option>
						<option value="OcAABB">ครู กศน.ตำบล</option>
						<option value="OcAABC">ครู ศูนย์การเรียนรู้ชุมชน</option>
						<option value="OcAABD">ครู อาสาสมัคร</option>
						<option value="OcAABE">ครู สอนคนพิการ</option>
						<option value="OcAABF">ครู ประกาศนียบัตรวิชาชีพ</option>
						<option value="OcAAAA">ครูอัตราจ้าง</option>
						<option value="OcAAAB">ครูผู้ช่วย</option>
						<option value="OcAAAD">ครูชำนาญการ</option>
						<option value="OcAAAE">ครูชำนาญการพิเศษ</option>
						<option value="OcAAAF">ครูเชี่ยวชาญ</option>
						<option value="OcAAAG">ครูเชี่ยวชาญพิเศษ</option>
						<option value="OcAABO">อื่น ๆ ระบุ....</option>
					</select> -->
					
					<!-- OcAABO List -->
					<!-- <div id="posub4OcAABO" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub3_id_ans" type="text" class="form-control" placeholder="ตำแหน่ง" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div> -->
				 	<!--OcAAC List -->
					<!-- No Content in this list -->
					<!-- OcAAD List -->
					<!-- <select id="posub4OcAAD" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="OcAADA">อาจารย์</option>
						<option value="OcAADB">ผู้ช่วยศาสตราจารย์</option>
						<option value="OcAADC">รองศาสตราจารย์</option>
						<option value="OcAADD">ศาสตราจารย์</option>
					</select>
					<div id="posub4OcAAO" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub2_id_ans" type="text" class="form-control" placeholder="ตำแหน่ง" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div> -->
					<!-- OcABA List -->
					<!-- <select id="posub4OcABA" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="OcABAA">รองผู้อำนวยการโรงเรียนเอกชน</option>
						<option value="OcABAB">รองผู้อำนวยการชำนาญการ</option>
						<option value="OcABAC">รองผู้อำนวยการชำนาญการพิเศษ</option>
						<option value="OcABAD">รองผู้อำนวยการเชี่ยวชาญ</option>
					</select> -->
					<!-- OcABB List -->
					<!-- <select id="posub4OcABB" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="OcABBA">ผู้อำนวยการโรงเรียนเอกชน</option>
						<option value="OcABBB">ผู้อำนวยการชำนาญการ</option>
						<option value="OcABBC">ผู้อำนวยการชำนาญการพิเศษ</option>
						<option value="OcABBD">ผู้อำนวยการเชี่ยวชาญ</option>
						<option value="OcABBE">ผู้อำนวยการเชี่ยวชาญพิเศษ</option>
					</select> -->
					<!-- OcABC List -->
					<!-- No Content in this list -->
					<!-- OcABD List
					<select id="posub4OcABD" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ผู้อำนวยการ กศน.ตำบล</option>
						<option value="B">ผู้อำนวนการ กศน.จังหวัด</option>
					</select>-->
					<!-- <div id="posub4OcABO" style="display:none">
					<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub4_id_ans" type="text" class="form-control" placeholder="ตำแหน่ง" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div> -->
					<!-- OcABE List -->
					<!-- No Content in this list -->
					<!-- OcACA List -->
					<!-- <select id="posub4OcACA" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="OcACAA">รองผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาชำนาญการพิเศษ</option>
						<option value="OcACAB">รองผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญ</option>
						<option value="OcACAC">ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญ</option>
						<option value="OcACAD">ผู้อำนวยการสำนักงานเขตพื้นที่การศึกษาเชียวชาญพิเศษ</option>
					</select> -->
					<!-- OcACB List -->
					<!-- <select id="posub4OcACB" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="OcACBA">รองศึกษาธิการจังหวัด</option>
						<option value="OcACBB">ศึกษาธิการจังหวัด</option>
						<option value="OcACBC">รองศึกษาธิการภาค</option>
						<option value="OcACBD">ศึกษาธิการภาค</option>
					</select> -->
					<!-- OcACC List -->
					<!-- <select id="posub4OcACC" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="OcACCA">รองผู้อำนวยการสำนัก</option>
						<option value="OcACCA">ผู้อำนวยการสำนัก</option>
						<option value="OcACCA">ผู้เชี่ยวชาญ</option>
						<option value="OcACCA">ผู้ตรวจราชการ/ที่ปรึกษาระดับ 10</option>
						<option value="OcACCA">ปลัด/รองปลัด/อธิบดี/รองอธิบดี</option>
					</select> -->
					<!-- OcACD List -->
					<!-- <select id="posub4OcACD" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="OcACDA">รองคณบดี</option>
						<option value="OcACDB">คณบดี</option>
						<option value="OcACDC">รองอธิการบดี</option>
						<option value="OcACDD">อธิการบดี</option>
					</select>
					<select id="posub4OcACE" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">รองผู้อำนวยการสำนักงาน กศน.จังหวัด/กรุงเทพมหานครชำนาญการพิเศษ</option>
						<option value="B">รองผู้อำนวยการสำนักงาน กศน.จังหวัด/กรุงเทพมหานครเชี่ยวชาญ</option>
						<option value="C">ผู้อำนวยการสำนักงาน กศน.จังหวัด/กรุงเทพมหานครเชี่ยวชาญ</option>
						<option value="D">ผู้อำนวยการสำนักงาน กศน.จังหวัด/กรุงเทพมหานครเชี่ยวชาญพิเศษ</option>
					</select>
					<select id="posub4OcACF" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ผู้อำนวยการสำนักงานการศึกษาเอกชนอำเภอชำนาญการพิเศษ</option>
						<option value="B">ผู้อำนวยการสำนักงานการศึกษาเอกชนอำเภอเชี่ยวชาญ</option>
						<option value="C">รองผู้อำนวยการสำนักงานการศึกษาเอกชนจังหวัดชำนาญการพิเศษ</option>
						<option value="D">รองผู้อำนวยการสำนักงานการศึกษาเอกชนจังหวัดเชี่ยวชาญ</option>
						<option value="E">ผู้อำนวยการสำนักงานการศึกษาเอกชนจังหวัดเชี่ยวชาญ</option>
						<option value="F">ผู้อำนวยการสำนักงานการศึกษาเอกชนจังหวัดเชี่ยวชาญพิเศษ</option>
					</select> -->
					<!-- <div id="posub4OcACO" style="display:none">
					<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub5_id_ans" type="text" class="form-control" placeholder="ตำแหน่ง" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div> -->
					<!-- OcADA List -->
					<!-- <select id="posub4OcADA" name="posub2_id" class="form-control" style="display:none; height:40px;">
						<option value="" disabled="disabled" selected="selected">เลือกตำแหน่ง</option>
						<option value="A">ศึกษานิเทศก์ชำนาญการ</option>
						<option value="B">ศึกษานิเทศก์ชำนาญการพิเศษ</option>
						<option value="C">ศึกษานิเทศก์เชี่ยวชาญ</option>
						<option value="D">ศึกษานิเทศก์เชี่ยวชาญพิเศษ</option>
					</select> -->
					<!-- <div id="posub4OcADO" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub6_id_ans" type="text" class="form-control" placeholder="ตำแหน่ง" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div> -->
					<!-- OcAOO List ****************** ************* ***************** ************************--> 
					<!-- <div id="posub4OcAOO" style="display:none">
						<div class="wrap-input100 m-t-10">
							<input class="input100" name="posub1_id_ans" type="text" class="form-control" placeholder="ตำแหน่ง" />
							<span class="symbol-input100"><i class="fas fa-id-card-alt" aria-hidden="true"></i></span>
						</div>
					</div> -->
					<!-- OcAOO  ****************** ************* ***************** ************************--> 
					<!-- End อาชีพ, ตำแหน่ง Content -->
					<!-- ################################################################################################ -->
					<!-- <div class="container-login100-form-btn m-t-20">
						<button class="login100-form-btn" style="width:30%" type="submit" id="register">
							ไปขั้นตอนต่อไป
						</button>
					</div>                 -->
			</form>
			<!-- ################################################################################################ -->
		</div>
	</div>
</div>

				<!-- ################################################################################################ -->
				<!-- <form class="login100-form validate-form" id="Ocform" action="addOcc.php" method="POST" enctype="multipart/form-data"> -->

					<!-- ################################################################################################ -->
				<!-- </form> -->
				<!-- ################################################################################################ -->
    <script src="js/projectRegist4occ.js"></script>
<script>
      if(localStorage.getItem('production')=="1") {
        window.url = "https://localhost/";
      }
      else if(localStorage.getItem('production')=="2"){
        window.url = "https://www.thaisuprateacher.org/dev/";
      }

	function PickPrename(val) {
		var elePrename = document.getElementById('pre_name');
		var elePrenameText = document.getElementById('pre_name_text');
		var elePrenameTextReq = document.getElementById('pre_name_text_required');
		if(val == 'O') {
			elePrenameText.style.display = 'block';
			elePrenameTextReq.required = true;
		}else{
			elePrenameText.style.display = 'none';
			elePrenameText.value = '';
			elePrenameTextReq.required = false;
		}
	}
		let url_token
		if(localStorage.getItem('production')=="1") {
			url_token = `${window.url}Thaisuprateacher/signup_data.php`
		}
		else {
			url_token = `${window.url}signup_data.php`
		}



	let province_token = document.getElementById('province_token')
	province_token.addEventListener('change',e =>{
		console.log(e.target.value);
		var formData = new FormData();
		formData.append('province', e.target.value);
		if(e.target.value!=""){
			let district = document.getElementById('district');
			 district.classList.remove("d-none");
		}
		fetch(url_token, {
		  method:"post",
		  body:formData
		})
		.then(function(response) {
		  return response.json();
		})
		.then((value)=>{
			console.log(value,'value');
			let str = "<option value =''>เลือกอำเภอ</option>";
			let district = document.getElementById('district')
			district.innerHTML = '';

			value.forEach(value_str => {
					console.log(value_str.district);
				str+= `<option value='${value_str.district+"/"+value_str.zip_code}' >`+value_str.district+"</option>";
			});

			district.innerHTML = str;
		})
		.catch(function(error) {
		  console.log("Request failed", error);
		});
	})

	let district = document.getElementById('district');
	
	district.addEventListener('change',e =>{
		console.log(e.target.value);
		var formData = new FormData();
		if(e.target.value!=""){
			let district_sub = document.getElementById('district_sub');
			 district_sub.classList.remove("d-none");
		}


		formData.append('district', e.target.value);
		if(e.target.value!=""){
			let district_sub = document.getElementById('district');
			district_sub.classList.remove("d-none");
		}
		fetch(url_token, {
		  method:"post",
		  body:formData
		})
		.then(function(response) {
		  return response.json();
		})
		.then((value)=>{
			// console.log(value);
			console.log(value,'value');
			let str = "<option value =''>เลือกตำบล</option>";
			let district = document.getElementById('district_sub')
			district.innerHTML = '';
			value.forEach(value_str => {
					console.log(value_str.district_sub);
				str+= `<option value='${value_str.district_sub+"/"+value_str.zip_code}' >`+value_str.district_sub+"</option>";
			});
			
			district.innerHTML = str;
		})
		.catch(function(error) {
		  console.log("Request failed", error);
		});

	})


	let district_sub = document.getElementById('district_sub');
	district_sub.addEventListener('change',e =>{
	
		let zip_code_token = document.getElementById('zip_code_token');
		zip_code_token.value = e.target.value.split("/")[1];
	})


	let code_persanal = document.getElementById('code_persanal')

	code_persanal.addEventListener('keyup',e =>{
		let ans = (Number.isInteger(parseInt(event.target.value)));
		if (ans) {
			let str = e.target.value;
			if(str.length >13){
				e.target.value = str.substr(0,13);
			}
		}
		else {
			e.target.value = e.target.value.substr(0,+e.target.value.length-1)
		}
	})

	let tel_ck = document.getElementById('tel_ck')
	tel_ck.addEventListener('keyup',e =>{
		let ans = (Number.isInteger(parseInt(event.target.value)));
		if (ans) {
			let str = e.target.value;
			if(str.length >10){
				e.target.value = str.substr(0,10);
			}
		}
		else {
			e.target.value = e.target.value.substr(0,+e.target.value.length-1)
		}
	})

	// let btn_register = document.getElementById('register');
	// btn_register.addEventListener('click',e =>{
	// 	if(document.getElementById('district').value == ""  ||  document.getElementById('district_sub').value == ""  ){
	// 		alert("กรุณาเลือก จังหวัด  อำเภอ ตำบล ใหม่ ")
	// 	}
	// })


</script>

<script src="js/jquery.min.js"></script>
<script src="js/projectRegist4affA.js" type="text/javascript"></script>

</body>
</html>