<?php
	session_start();
	if(isset($_SESSION['ID'])) {
		echo "<script>window.history.go(-1)</script>";
	}else{
		if($_POST['CFP'] != 20) { // not CFP signup.php || signup-en.php
			echo "<script>window.history.go(-1)</script>";
		}else{
			require_once("condb.php");
		}
	}
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>Foundation of Thai Suprateacher</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>

<body>

<?php
	$newEmail = trim($_POST["email"]);

	$strSQL = "SELECT * FROM `login` WHERE email = '$newEmail' "; //Let $strSQL to be a command that find email with user request in database
	$objQuery = mysqli_query($con,$strSQL); // Connect to database $con and do command as $strSQL
	$objResult = mysqli_fetch_array($objQuery);

	$newEmail = strtolower($_POST["email"]);
	$newPassword = $_POST["password"];
	$newConpassword = $_POST["conpassword"];
	$newPrename = $_POST['pre_name'];
	if($newPrename == 'O') {
		$newPrenameRemark = $_POST['pre_name_text'];
	}
	$newFirstname = $_POST["firstname"];
	$newLastname = $_POST["lastname"];
	$newPhonenum = $_POST["phonenum"];
	$Birthdate = $_POST["birthdate"];
	$dat = "-";
	$newBirthdate = str_replace("-",'',$Birthdate);
	$newDocaddress = $_POST["docaddress"];

	if($objResult)
	{
?>
	<!-- Start Email exists -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="signup-en.php"><i class="fas fa-arrow-left"></i> Back to Sign up</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<div class="signup-stat m-t-20">
					<h8>That email is taken.</h8>
					<p>Try another.</p>
				</div>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	<!-- End Email exists -->
	<?php
		}elseif($newPassword != $newConpassword) {
	?>
	<!-- Start Password non-match -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="signup-en.php"><i class="fas fa-arrow-left"></i> Back to Sign up</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<div class="signup-stat m-t-20">
					<h8>Those passwords didn't matched.</h8>
					<p>Please go back to Sign up and try again.</p>
				</div>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	<!-- End Password non-match -->
	<?php
	}else{
		$strSQL = "INSERT INTO `login` (`email`,`password`,`prename`,`prename_remark`,`firstname`,`lastname`,`phonenum`,`birthdate`,`docaddress`) 
		VALUES ('$newEmail','$newPassword','$newPrename','$newPrenameRemark','$newFirstname','$newLastname','$newPhonenum','$newBirthdate','$newDocaddress') ";

		$objQuery = mysqli_query($con,$strSQL);
	?>
	<!-- Start Password match and save to database -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="index-en.php"><i class="fas fa-arrow-left"></i> Back to Home</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<div class="signup-stat m-t-20">
					<h8>Sign up is COMPLETED.</h8>
					<p>Congratulations!! You are general member now.</p>
					<p>You can upgrade to premium account <a href="บำรุงค่าสมาชิก-en.php">here</a></p>
					<p>Project Registration <a href="login.php?CFP=30">here</a></p>
				</div>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	<!-- End Password match and save to databas -->
	<?php } ?>
</body>
</html>