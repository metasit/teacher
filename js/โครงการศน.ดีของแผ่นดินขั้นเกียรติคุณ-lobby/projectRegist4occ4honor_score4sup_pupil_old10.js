/* js for สมัครเข้าall_project.php */
// NOTE: Use for โครงการศึกษานิเทศก์ดีของแผ่นดิน.php เท่านั้น

/* Start อาชีพ, ตำแหน่ง */
//var Ocform = document.getElementById('Ocform'); // Set Occupation form to reset all input value
//var Oc = document.getElementById('Oc');
//////////////////////////////////////////////////////////////////////////////////////////////////////////
var elepomain4OcA10 = document.getElementById('pomain4OcA_pupil_old10');
var elepomain4OcB10 = document.getElementById('pomain4OcB_pupil_old10');
var elepomain4OcC10 = document.getElementById('pomain4OcC_pupil_old10');
var elepomain4OcD10 = document.getElementById('pomain4OcD_pupil_old10');
var elepomain4OcE10 = document.getElementById('pomain4OcE_pupil_old10');
var elepomain4OcF10 = document.getElementById('pomain4OcF_pupil_old10');
var elepomain4OcG10 = document.getElementById('pomain4OcG_pupil_old10');
var elepomain4OcO10 = document.getElementById('pomain4OcO_pupil_old10');
//////////////////////////////////////////////////////////////////////////////////////////////////////////
var eleposub4OcAA10 = document.getElementById('posub4OcAA_pupil_old10');
var eleposub4OcAB10 = document.getElementById('posub4OcAB_pupil_old10');
var eleposub4OcAC10 = document.getElementById('posub4OcAC_pupil_old10');
var eleposub4OcAD10 = document.getElementById('posub4OcAD_pupil_old10');
var eleposub4OcAO10 = document.getElementById('posub4OcAO_pupil_old10');
//////////////////////////////////////////////////////////////////////////////////////////////////////////
var eleposub4OcAAA10 = document.getElementById('posub4OcAAA_pupil_old10');
var eleposub4OcAAB10 = document.getElementById('posub4OcAAB_pupil_old10');

var eleposub4OcAAD10 = document.getElementById('posub4OcAAD_pupil_old10');
//////////////////////////////////////////////////////////////////////////////////////////////////////////
var eleposub4OcABA10 = document.getElementById('posub4OcABA_pupil_old10');
var eleposub4OcABB10 = document.getElementById('posub4OcABB_pupil_old10');

var eleposub4OcABD10 = document.getElementById('posub4OcABD_pupil_old10');

//////////////////////////////////////////////////////////////////////////////////////////////////////////
var eleposub4OcACA10 = document.getElementById('posub4OcACA_pupil_old10');
var eleposub4OcACB10 = document.getElementById('posub4OcACB_pupil_old10');
var eleposub4OcACC10 = document.getElementById('posub4OcACC_pupil_old10');
var eleposub4OcACD10 = document.getElementById('posub4OcACD_pupil_old10');
//////////////////////////////////////////////////////////////////////////////////////////////////////////
var eleposub4OcADA10 = document.getElementById('posub4OcADA_pupil_old10');
//////////////////////////////////////////////////////////////////////////////////////////////////////////
var eleposub4OcAABO10 = document.getElementById('posub4OcAABO_pupil_old10');
//////////////////////////////////////////////////////////////////////////////////////////////////////////


function PickOccupation_pupil_old10(val) { // When user pick occupation
	//var elePohead = document.getElementById('pohead');

	//Ocform.reset(); // Reset all input value
	//Oc.value = val;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	elepomain4OcA10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAA10.selectedIndex = 0;
	eleposub4OcAB10.selectedIndex = 0;
	eleposub4OcAC10.selectedIndex = 0;
	eleposub4OcAD10.selectedIndex = 0;
	eleposub4OcAO10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.selectedIndex = 0;
	eleposub4OcAAB10.selectedIndex = 0;
	
	eleposub4OcAAD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.selectedIndex = 0;
	eleposub4OcABB10.selectedIndex = 0;
	
	eleposub4OcABD10.selectedIndex = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.selectedIndex = 0;
	eleposub4OcACB10.selectedIndex = 0;
	eleposub4OcACC10.selectedIndex = 0;
	eleposub4OcACD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAA10.style.display = 'none';
	eleposub4OcAB10.style.display = 'none';
	eleposub4OcAC10.style.display = 'none';
	eleposub4OcAD10.style.display = 'none';
	eleposub4OcAO10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.style.display = 'none';
	eleposub4OcAAB10.style.display = 'none';
	
	eleposub4OcAAD10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.style.display = 'none';
	eleposub4OcABB10.style.display = 'none';
	
	eleposub4OcABD10.style.display = 'none';
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.style.display = 'none';
	eleposub4OcACB10.style.display = 'none';
	eleposub4OcACC10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAA10.required = false;
	eleposub4OcAB10.required = false;
	eleposub4OcAC10.required = false;
	eleposub4OcAD10.required = false;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.required = false;
	eleposub4OcAAB10.required = false;
	
	eleposub4OcAAD10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.required = false;
	eleposub4OcABB10.required = false;
	
	eleposub4OcABD10.required = false;
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.required = false;
	eleposub4OcACB10.required = false;
	eleposub4OcACC10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	//elePohead.style.display = 'block';
	if(val == 'OcA') {
		elepomain4OcA10.style.display = 'block';
		elepomain4OcB10.style.display = 'none';
		elepomain4OcC10.style.display = 'none';
		elepomain4OcD10.style.display = 'none';
		elepomain4OcE10.style.display = 'none';
		elepomain4OcF10.style.display = 'none';
		elepomain4OcG10.style.display = 'none';
		elepomain4OcO10.style.display = 'none';
		elepomain4OcA10.required = true;
	}else if(val == 'OcB') {
		elepomain4OcA10.style.display = 'none';
		elepomain4OcB10.style.display = 'block';
		elepomain4OcC10.style.display = 'none';
		elepomain4OcD10.style.display = 'none';
		elepomain4OcE10.style.display = 'none';
		elepomain4OcF10.style.display = 'none';
		elepomain4OcG10.style.display = 'none';
		elepomain4OcO10.style.display = 'none';
		elepomain4OcA10.required = false;
	}else if(val == 'OcC') {
		elepomain4OcA10.style.display = 'none';
		elepomain4OcB10.style.display = 'none';
		elepomain4OcC10.style.display = 'block';
		elepomain4OcD10.style.display = 'none';
		elepomain4OcE10.style.display = 'none';
		elepomain4OcF10.style.display = 'none';
		elepomain4OcG10.style.display = 'none';
		elepomain4OcO10.style.display = 'none';
		elepomain4OcA10.required = false;
	}else if(val == 'OcD') {
		elepomain4OcA10.style.display = 'none';
		elepomain4OcB10.style.display = 'none';
		elepomain4OcC10.style.display = 'none';
		elepomain4OcD10.style.display = 'block';
		elepomain4OcE10.style.display = 'none';
		elepomain4OcF10.style.display = 'none';
		elepomain4OcG10.style.display = 'none';
		elepomain4OcO10.style.display = 'none';
		elepomain4OcA10.required = false;
	}else if(val == 'OcE') {
		elepomain4OcA10.style.display = 'none';
		elepomain4OcB10.style.display = 'none';
		elepomain4OcC10.style.display = 'none';
		elepomain4OcD10.style.display = 'none';
		elepomain4OcE10.style.display = 'block';
		elepomain4OcF10.style.display = 'none';
		elepomain4OcG10.style.display = 'none';
		elepomain4OcO10.style.display = 'none';
		elepomain4OcA10.required = false;
	}else if(val == 'OcF') {
		elepomain4OcA10.style.display = 'none';
		elepomain4OcB10.style.display = 'none';
		elepomain4OcC10.style.display = 'none';
		elepomain4OcD10.style.display = 'none';
		elepomain4OcE10.style.display = 'none';
		elepomain4OcF10.style.display = 'block';
		elepomain4OcG10.style.display = 'none';
		elepomain4OcO10.style.display = 'none';
		elepomain4OcA10.required = false;
	}else if(val == 'OcG') {
		elepomain4OcA10.style.display = 'none';
		elepomain4OcB10.style.display = 'none';
		elepomain4OcC10.style.display = 'none';
		elepomain4OcD10.style.display = 'none';
		elepomain4OcE10.style.display = 'none';
		elepomain4OcF10.style.display = 'none';
		elepomain4OcG10.style.display = 'block';
		elepomain4OcO10.style.display = 'none';
		elepomain4OcA10.required = false;
	}else if(val == 'OcO') {
		elepomain4OcA10.style.display = 'none';
		elepomain4OcB10.style.display = 'none';
		elepomain4OcC10.style.display = 'none';
		elepomain4OcD10.style.display = 'none';
		elepomain4OcE10.style.display = 'none';
		elepomain4OcF10.style.display = 'none';
		elepomain4OcG10.style.display = 'none';
		elepomain4OcO10.style.display = 'block';
		elepomain4OcA10.required = false;
	}else{
		window.alert('Error PickOccupation_pupil_old10: Contact Developer ');
	}
}

function PickPoMain4OcA_pupil_old10(val) { // When user pick main-position
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAA10.selectedIndex = 0;
	eleposub4OcAB10.selectedIndex = 0;
	eleposub4OcAC10.selectedIndex = 0;
	eleposub4OcAD10.selectedIndex = 0;
	eleposub4OcAO10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.selectedIndex = 0;
	eleposub4OcAAB10.selectedIndex = 0;
	
	eleposub4OcAAD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.selectedIndex = 0;
	eleposub4OcABB10.selectedIndex = 0;
	
	eleposub4OcABD10.selectedIndex = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.selectedIndex = 0;
	eleposub4OcACB10.selectedIndex = 0;
	eleposub4OcACC10.selectedIndex = 0;
	eleposub4OcACD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAA10.style.display = 'none';
	eleposub4OcAB10.style.display = 'none';
	eleposub4OcAC10.style.display = 'none';
	eleposub4OcAD10.style.display = 'none';
	eleposub4OcAO10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.style.display = 'none';
	eleposub4OcAAB10.style.display = 'none';
	
	eleposub4OcAAD10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.style.display = 'none';
	eleposub4OcABB10.style.display = 'none';
	
	eleposub4OcABD10.style.display = 'none';
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.style.display = 'none';
	eleposub4OcACB10.style.display = 'none';
	eleposub4OcACC10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAA10.required = false;
	eleposub4OcAB10.required = false;
	eleposub4OcAC10.required = false;
	eleposub4OcAD10.required = false;
	eleposub4OcAO10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.required = false;
	eleposub4OcAAB10.required = false;
	
	eleposub4OcAAD10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.required = false;
	eleposub4OcABB10.required = false;
	
	eleposub4OcABD10.required = false;
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.required = false;
	eleposub4OcACB10.required = false;
	eleposub4OcACC10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	if(val == 'A') {
		eleposub4OcAA10.style.display = 'block';
		eleposub4OcAB10.style.display = 'none';
		eleposub4OcAC10.style.display = 'none';
		eleposub4OcAD10.style.display = 'none';
		eleposub4OcAO10.style.display = 'none';
		eleposub4OcAA10.required = true;
		eleposub4OcAB10.required = false;
		eleposub4OcAC10.required = false;
		eleposub4OcAD10.required = false;
	}else if(val == 'B') {
		eleposub4OcAA10.style.display = 'none';
		eleposub4OcAB10.style.display = 'block';
		eleposub4OcAC10.style.display = 'none';
		eleposub4OcAD10.style.display = 'none';
		eleposub4OcAO10.style.display = 'none';
		eleposub4OcAA10.required = false;
		eleposub4OcAB10.required = true;
		eleposub4OcAC10.required = false;
		eleposub4OcAD10.required = false;
	}else if(val == 'C') {
		eleposub4OcAA10.style.display = 'none';
		eleposub4OcAB10.style.display = 'none';
		eleposub4OcAC10.style.display = 'block';
		eleposub4OcAD10.style.display = 'none';
		eleposub4OcAO10.style.display = 'none';
		eleposub4OcAA10.required = false;
		eleposub4OcAB10.required = false;
		eleposub4OcAC10.required = true;
		eleposub4OcAD10.required = false;
	}else if(val == 'D') {
		eleposub4OcAA10.style.display = 'none';
		eleposub4OcAB10.style.display = 'none';
		eleposub4OcAC10.style.display = 'none';
		eleposub4OcAD10.style.display = 'block';
		eleposub4OcAO10.style.display = 'none';
		eleposub4OcAA10.required = false;
		eleposub4OcAB10.required = false;
		eleposub4OcAC10.required = false;
		eleposub4OcAD10.required = true;
	}else if(val == 'O'){
		eleposub4OcAA10.style.display = 'none';
		eleposub4OcAB10.style.display = 'none';
		eleposub4OcAC10.style.display = 'none';
		eleposub4OcAD10.style.display = 'none';
		eleposub4OcAO10.style.display = 'block';
	}else{
		window.alert('Error PickPoMain4OcA_pupil_old10: Contact Developer ');
	}
}

function PickPoMain4OcF_pupil_old10(val) { // When user pick main-position
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAA10.selectedIndex = 0;
	eleposub4OcAB10.selectedIndex = 0;
	eleposub4OcAC10.selectedIndex = 0;
	eleposub4OcAD10.selectedIndex = 0;
	eleposub4OcAO10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.selectedIndex = 0;
	eleposub4OcAAB10.selectedIndex = 0;
	
	eleposub4OcAAD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.selectedIndex = 0;
	eleposub4OcABB10.selectedIndex = 0;
	
	eleposub4OcABD10.selectedIndex = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.selectedIndex = 0;
	eleposub4OcACB10.selectedIndex = 0;
	eleposub4OcACC10.selectedIndex = 0;
	eleposub4OcACD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAA10.style.display = 'none';
	eleposub4OcAB10.style.display = 'none';
	eleposub4OcAC10.style.display = 'none';
	eleposub4OcAD10.style.display = 'none';
	eleposub4OcAO10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.style.display = 'none';
	eleposub4OcAAB10.style.display = 'none';
	
	eleposub4OcAAD10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.style.display = 'none';
	eleposub4OcABB10.style.display = 'none';
	
	eleposub4OcABD10.style.display = 'none';
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.style.display = 'none';
	eleposub4OcACB10.style.display = 'none';
	eleposub4OcACC10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAA10.required = false;
	eleposub4OcAB10.required = false;
	eleposub4OcAC10.required = false;
	eleposub4OcAD10.required = false;
	eleposub4OcAO10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.required = false;
	eleposub4OcAAB10.required = false;
	
	eleposub4OcAAD10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.required = false;
	eleposub4OcABB10.required = false;
	
	eleposub4OcABD10.required = false;
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.required = false;
	eleposub4OcACB10.required = false;
	eleposub4OcACC10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
}

function PickPoSub4OcAA_pupil_old10(val) { // When user pick sub-position 1
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.selectedIndex = 0;
	eleposub4OcAAB10.selectedIndex = 0;
	
	eleposub4OcAAD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.selectedIndex = 0;
	eleposub4OcABB10.selectedIndex = 0;
	
	eleposub4OcABD10.selectedIndex = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.selectedIndex = 0;
	eleposub4OcACB10.selectedIndex = 0;
	eleposub4OcACC10.selectedIndex = 0;
	eleposub4OcACD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.style.display = 'none';
	eleposub4OcAAB10.style.display = 'none';
	
	eleposub4OcAAD10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.style.display = 'none';
	eleposub4OcABB10.style.display = 'none';
	
	eleposub4OcABD10.style.display = 'none';
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.style.display = 'none';
	eleposub4OcACB10.style.display = 'none';
	eleposub4OcACC10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.required = false;
	eleposub4OcAAB10.required = false;
	
	eleposub4OcAAD10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.required = false;
	eleposub4OcABB10.required = false;
	
	eleposub4OcABD10.required = false;
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.required = false;
	eleposub4OcACB10.required = false;
	eleposub4OcACC10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	if(val == 'A') {
		eleposub4OcAAA10.style.display = 'block';
		eleposub4OcAAB10.style.display = 'none';
		
		eleposub4OcAAD10.style.display = 'none';
		eleposub4OcAAA10.required = true;
		eleposub4OcAAB10.required = false;
		
		eleposub4OcAAD10.required = false;
	}else if(val == 'B') {
		eleposub4OcAAA10.style.display = 'none';
		eleposub4OcAAB10.style.display = 'block';
		
		eleposub4OcAAD10.style.display = 'none';
		eleposub4OcAAA10.required = false;
		eleposub4OcAAB10.required = true;
		
		eleposub4OcAAD10.required = false;
	}else if(val == 'C') {
		eleposub4OcAAA10.style.display = 'none';
		eleposub4OcAAB10.style.display = 'none';
		
		eleposub4OcAAD10.style.display = 'none';
		eleposub4OcAAA10.required = false;
		eleposub4OcAAB10.required = false;
		
		eleposub4OcAAD10.required = false;
	}else if(val == 'D') {
		eleposub4OcAAA10.style.display = 'none';
		eleposub4OcAAB10.style.display = 'none';
		
		eleposub4OcAAD10.style.display = 'block';
		eleposub4OcAAA10.required = false;
		eleposub4OcAAB10.required = false;
		
		eleposub4OcAAD10.required = true;
	}else{
		window.alert('Error PickPoSub4OcAA_pupil_old10: Contact Developer ');
	}
}

function PickPoSub4OcAB_pupil_old10(val) { // When user pick sub-position 1
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.selectedIndex = 0;
	eleposub4OcAAB10.selectedIndex = 0;
	
	eleposub4OcAAD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.selectedIndex = 0;
	eleposub4OcABB10.selectedIndex = 0;
	
	eleposub4OcABD10.selectedIndex = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.selectedIndex = 0;
	eleposub4OcACB10.selectedIndex = 0;
	eleposub4OcACC10.selectedIndex = 0;
	eleposub4OcACD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.style.display = 'none';
	eleposub4OcAAB10.style.display = 'none';
	
	eleposub4OcAAD10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.style.display = 'none';
	eleposub4OcABB10.style.display = 'none';
	
	eleposub4OcABD10.style.display = 'none';
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.style.display = 'none';
	eleposub4OcACB10.style.display = 'none';
	eleposub4OcACC10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.required = false;
	eleposub4OcAAB10.required = false;
	
	eleposub4OcAAD10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.required = false;
	eleposub4OcABB10.required = false;
	
	eleposub4OcABD10.required = false;
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.required = false;
	eleposub4OcACB10.required = false;
	eleposub4OcACC10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	if(val == 'A') {
		eleposub4OcABA10.style.display = 'block';
		eleposub4OcABB10.style.display = 'none';
		
		eleposub4OcABD10.style.display = 'none';
		
		eleposub4OcABA10.required = true;
		eleposub4OcABB10.required = false;
		
		eleposub4OcABD10.required = false;
		
	}else if(val == 'B') {
		eleposub4OcABA10.style.display = 'none';
		eleposub4OcABB10.style.display = 'block';
		
		eleposub4OcABD10.style.display = 'none';
		
		eleposub4OcABA10.required = false;
		eleposub4OcABB10.required = true;
		
		eleposub4OcABD10.required = false;
		
	}else if(val == 'C') {
		// No C Content
	}else if(val == 'D') {
		eleposub4OcABA10.style.display = 'none';
		eleposub4OcABB10.style.display = 'none';
		
		eleposub4OcABD10.style.display = 'block';
		
		eleposub4OcABA10.required = false;
		eleposub4OcABB10.required = false;
		
		eleposub4OcABD10.required = true;
		
	}else if(val == 'E') {
		// No E Content
	}else{
		window.alert('Error PickPoSub4OcAB_pupil_old10: Contact Developer ');
	}
}

function PickPoSub4OcAC_pupil_old10(val) { // When user pick sub-position 1
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.selectedIndex = 0;
	eleposub4OcAAB10.selectedIndex = 0;
	
	eleposub4OcAAD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.selectedIndex = 0;
	eleposub4OcABB10.selectedIndex = 0;
	
	eleposub4OcABD10.selectedIndex = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.selectedIndex = 0;
	eleposub4OcACB10.selectedIndex = 0;
	eleposub4OcACC10.selectedIndex = 0;
	eleposub4OcACD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.style.display = 'none';
	eleposub4OcAAB10.style.display = 'none';
	
	eleposub4OcAAD10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.style.display = 'none';
	eleposub4OcABB10.style.display = 'none';
	
	eleposub4OcABD10.style.display = 'none';
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.style.display = 'none';
	eleposub4OcACB10.style.display = 'none';
	eleposub4OcACC10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.required = false;
	eleposub4OcAAB10.required = false;
	
	eleposub4OcAAD10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.required = false;
	eleposub4OcABB10.required = false;
	
	eleposub4OcABD10.required = false;
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.required = false;
	eleposub4OcACB10.required = false;
	eleposub4OcACC10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	if(val == 'A') {
		eleposub4OcACA10.style.display = 'block';
		eleposub4OcACB10.style.display = 'none';
		eleposub4OcACC10.style.display = 'none';
		eleposub4OcACD10.style.display = 'none';
		eleposub4OcACA10.required = true;
		eleposub4OcACB10.required = false;
		eleposub4OcACC10.required = false;
		eleposub4OcACD10.required = false;
	}else if(val == 'B') {
		eleposub4OcACA10.style.display = 'none';
		eleposub4OcACB10.style.display = 'block';
		eleposub4OcACC10.style.display = 'none';
		eleposub4OcACD10.style.display = 'none';
		eleposub4OcACA10.required = false;
		eleposub4OcACB10.required = true;
		eleposub4OcACC10.required = false;
		eleposub4OcACD10.required = false;
	}else if(val == 'C') {
		eleposub4OcACA10.style.display = 'none';
		eleposub4OcACB10.style.display = 'none';
		eleposub4OcACC10.style.display = 'block';
		eleposub4OcACD10.style.display = 'none';
		eleposub4OcACA10.required = false;
		eleposub4OcACB10.required = false;
		eleposub4OcACC10.required = true;
		eleposub4OcACD10.required = false;
	}else if(val == 'D') {
		eleposub4OcACA10.style.display = 'none';
		eleposub4OcACB10.style.display = 'none';
		eleposub4OcACC10.style.display = 'none';
		eleposub4OcACD10.style.display = 'block';
		eleposub4OcACA10.required = false;
		eleposub4OcACB10.required = false;
		eleposub4OcACC10.required = false;
		eleposub4OcACD10.required = true;
	}else{
		window.alert('Error PickPoSub4OcAC_pupil_old10: Contact Developer ');
	}
}

function PickPoSub4OcAD_pupil_old10(val) { // When user pick sub-position 1
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.selectedIndex = 0;
	eleposub4OcAAB10.selectedIndex = 0;
	
	eleposub4OcAAD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.selectedIndex = 0;
	eleposub4OcABB10.selectedIndex = 0;
	
	eleposub4OcABD10.selectedIndex = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.selectedIndex = 0;
	eleposub4OcACB10.selectedIndex = 0;
	eleposub4OcACC10.selectedIndex = 0;
	eleposub4OcACD10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.selectedIndex = 0;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.style.display = 'none';
	eleposub4OcAAB10.style.display = 'none';
	
	eleposub4OcAAD10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.style.display = 'none';
	eleposub4OcABB10.style.display = 'none';
	
	eleposub4OcABD10.style.display = 'none';
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.style.display = 'none';
	eleposub4OcACB10.style.display = 'none';
	eleposub4OcACC10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.style.display = 'none';
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAAA10.required = false;
	eleposub4OcAAB10.required = false;
	
	eleposub4OcAAD10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcABA10.required = false;
	eleposub4OcABB10.required = false;
	
	eleposub4OcABD10.required = false;
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcACA10.required = false;
	eleposub4OcACB10.required = false;
	eleposub4OcACC10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcADA10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	eleposub4OcAABO10.required = false;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	if(val == 'A') {
		eleposub4OcADA10.style.display = 'block';
		eleposub4OcADA10.required = true;
	}else{
		window.alert('Error PickPoSub4OcAC_pupil_old10: Contact Developer ');
	}
}

function PickPoSub4OcAAB_pupil_old10(val) {
	if(val == 'O') {
		eleposub4OcAABO10.style.display = 'block';
		eleposub4OcAABO10.required = true;
	}
}
/* End อาชีพ, ตำแหน่ง */