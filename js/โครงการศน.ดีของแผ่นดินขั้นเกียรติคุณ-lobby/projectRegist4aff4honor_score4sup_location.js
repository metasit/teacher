/* js for สมัครเข้าall_project.php */

/* Start สังกัด */
$(function(){
	var affiliationObject = $('#affiliation_id_location');
	var affsubObject = $('#affsub_id_location');
	var affsub2Object = $('#affsub2_id_location');
	var affsub3Object = $('#affsub3_id_location');
	var affsub4Object = $('#affsub4_id_location');
	var affsubtextObject = $('#affsub_text_location');
	var affsubansObject = $('#affsub_ans_id_location');
	var affsub4textObject = $('#affsub4_text_location');
	var affsub4ansObject = $('#affsub4_ans_id_location');

	// on change affiliation
	affiliationObject.on('change', function(){
			var affiliation_id_location = $(this).val();

			affsubObject.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
			affsubObject.hide();
			affsub2Object.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
			affsub2Object.hide();
			affsub3Object.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
			affsub3Object.hide();
			affsub4Object.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
			affsub4Object.hide();
			affsubansObject.attr('value', '');
			affsubtextObject.hide();
			affsub4ansObject.attr('value', '');
			affsub4textObject.hide();

			$('#affsubhead').show();
			if(affiliation_id_location == 'AfC' || affiliation_id_location == 'AfD' || affiliation_id_location == 'AfG') {
				affsubObject.show();
				affsubObject.attr('required', true);
				affsubObject.html('<option value="" disabled="disabled" selected="selected">เลือกจังหวัด</option>');
			}else if(affiliation_id_location == 'AfJ' || affiliation_id_location == 'AfO') {
				affsubtextObject.show();
				affsubtextObject.attr('required', true);
				affsubtextObject.html();
			}else{
				affsubObject.show();
				affsubObject.attr('required', true);
				affsubObject.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
			}
			
			$.get('get_affsub.php?affiliation_id=' + affiliation_id_location, function(data){
					var result = JSON.parse(data);
					$.each(result, function(index, item){
							affsubObject.append(
									$('<option></option>').val(item.affsub_id_location).html(item.affsub_name)
							);
					});
			});
	});

	// on change affsub
	affsubObject.on('change', function(){
		var affsub_id_location = $(this).val();
		var affiliation_id_location = affsub_id_location.substr(0,3);

		affsub2Object.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
		affsub2Object.hide();
		affsub3Object.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
		affsub3Object.hide();
		affsub4Object.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
		affsub4Object.hide();
		affsub4ansObject.attr('value', '');
		affsub4textObject.hide();
		
		if(affiliation_id_location == 'AfA' || affiliation_id_location == 'AfB' || affiliation_id_location == 'AfF') {
			affsub2Object.show();
			affsub2Object.attr('required', true);
			affsub2Object.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
		}else if(affiliation_id_location == 'AfC' || affiliation_id_location == 'AfD' || affiliation_id_location == 'AfG') {
			affsub2Object.show();
			affsub2Object.attr('required', true);
			affsub2Object.html('<option value="" disabled="disabled" selected="selected">เลือกเขต/อำเภอ</option>');
		}else if(affiliation_id_location == 'AfE') {
			affsub2Object.show();
			affsub2Object.attr('required', true);
			affsub2Object.html('<option value="" disabled="disabled" selected="selected">เลือกจังหวัด</option>');
		}else if(affiliation_id_location == 'AfH') {
			if(affsub_id_location == 'AfHA') {
				affsub2Object.show();
				affsub2Object.attr('required', true);
				affsub2Object.html(
					'<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>'+
					'<option value="AfHAA">สำนักงานเขต</option>'
				);
			}
		}

		$.get('get_affsub24'+affiliation_id_location+'.php?affsub_id=' + affsub_id_location, function(data){
				var result = JSON.parse(data);
				$.each(result, function(index, item){
						affsub2Object.append(
								$('<option></option>').val(item.affsub2_id_location).html(item.affsub2_name)
						);
				});
		});

	});

	// on change affsub2
	affsub2Object.on('change', function(){
		var affsub2_id_location = $(this).val();
		var affiliation_id_location = affsub2_id_location.substr(0,3);
		var affsub_id_location = affsub2_id_location.substr(0,4);

		affsub3Object.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
		affsub3Object.hide();
		affsub4Object.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
		affsub4Object.hide();
		affsub4ansObject.attr('value', '');
		affsub4textObject.hide();

		if(affiliation_id_location == 'AfB') {
			affsub3Object.show();
			affsub3Object.attr('required', true);
			affsub3Object.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
			$.get('get_affsub34'+affsub_id_location+'.php?affsub2_id=' + affsub2_id_location, function(data){
				var result = JSON.parse(data);
				$.each(result, function(index, item){
					affsub3Object.append(
						$('<option></option>').val(item.affsub3_id_location).html(item.affsub3_name)
					);
				});
			});
		}else if(affsub2_id_location == 'AfHAA') {
			affsub3Object.show();
			affsub3Object.attr('required', true);
			affsub3Object.html('<option value="" disabled="disabled" selected="selected">เลือกเขต</option>');
			$.get('get_affsub34'+affsub2_id_location+'.php?affsub2_id=' + affsub2_id_location, function(data){
				var result = JSON.parse(data);
				$.each(result, function(index, item){
					affsub3Object.append(
						$('<option></option>').val(item.affsub3_id_location).html(item.affsub3_name)
					);
				});
			});
		}else{

			if(affiliation_id_location == 'AfC' || affiliation_id_location == 'AfG') {
				affsub3Object.show();
				affsub3Object.attr('required', true);
				affsub3Object.html('<option value="" disabled="disabled" selected="selected">เลือกสถานศึกษา</option>');
			}else if(affiliation_id_location == 'AfD') {
				affsub3Object.show();
				affsub3Object.attr('required', true);
				affsub3Object.html('<option value="" disabled="disabled" selected="selected">เลือกแขวง/ตำบล</option>');
			}else if(affiliation_id_location == 'AfE') {
				affsub3Object.show();
				affsub3Object.attr('required', true);
				affsub3Object.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
			}
			$.get('get_affsub34'+affiliation_id_location+'.php?affsub2_id=' + affsub2_id_location, function(data){
				var result = JSON.parse(data);
				$.each(result, function(index, item){
					affsub3Object.append(
						$('<option></option>').val(item.affsub3_id_location).html(item.affsub3_name)
					);
				});
			});
		}
	});

	// on change affsub3
	affsub3Object.on('change', function(){
		var affsub3_id_location = $(this).val();
		var affiliation_id_location = affsub3_id_location.substr(0,3);
		var affsub_id_location = affsub3_id_location.substr(0,4);
		var affsub2_id_location = affsub3_id_location.substr(0,5);

		affsub4Object.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
		affsub4Object.hide();
		affsub4ansObject.attr('value', '');
		affsub4textObject.hide();

		if(affsub_id_location == 'AfBA') {
			affsub4Object.show();
			affsub4Object.attr('required', true);
			affsub4Object.html('<option value="" disabled="disabled" selected="selected">เลือกสังกัดย่อย</option>');
			$.get('get_affsub44'+affsub2_id_location+'.php?affsub3_id=' + affsub3_id_location, function(data){
				var result = JSON.parse(data);
				$.each(result, function(index, item){
					affsub4Object.append(
						$('<option></option>').val(item.affsub4_id_location).html(item.affsub4_name)
					);
				});
			});
		}else if(affiliation_id_location == 'AfD') {
			affsub4textObject.show();
			affsub4textObject.attr('required', true);
			affsub4textObject.html();

		}else if(affsub2_id_location == 'AfHAA'){
			affsub4Object.show();
			affsub4Object.attr('required', true);
			affsub4Object.html('<option value="" disabled="disabled" selected="selected">เลือกโรงเรียน</option>');
			$.get('get_affsub44'+affsub2_id_location+'.php?affsub3_id=' + affsub3_id_location, function(data){
				var result = JSON.parse(data);
				$.each(result, function(index, item){
					affsub4Object.append(
						$('<option></option>').val(item.affsub4_id_location).html(item.affsub4_name)
					);
				});
			});
		
		}else if(affiliation_id == 'AfG') {

			affsub4Object.show();
			affsub4Object.attr('required', true);
			affsub4Object.html('<option value="" disabled="disabled" selected="selected">เลือกโรงเรียน</option>');
			$.get('get_affsub44AfG.php?affsub3_id=' + affsub3_id, function(data){
				var result = JSON.parse(data);
				$.each(result, function(index, item){
					affsub4Object.append(
						$('<option></option>').val(item.affsub4_id).html(item.affsub4_name)
					);
				});
			});
		}


	});





});

/* End สังกัด */