var index = 0;

/* Feature to change poster automatically every 4 sec. */
carousel();
function carousel() {
  var i;
  var x = document.getElementsByClassName("imgSlides");
  var y = document.getElementsByClassName("pagi-style");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  for (i = 0; i < y.length; i++) {
    y[i].className = y[i].className.replace("check-current", "");
  }
  index++;
  if (index > x.length) {index = 1} // if the current image poster is the last one in array, let loop to beginner of the first image poster in array
  x[index-1].style.display = "block";
  y[index-1].className += "check-current";
  setTimeout(carousel,4000)
  
}

/* Feature to click Prev and Next button for poster slider */
showDivs(index);
function plusDivs(n) {
  showDivs(index += n);
}
function currentDiv(n) {
  showDivs(index = n);
}
function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("imgSlides");
  var y = document.getElementsByClassName("pagi-style");
  if (n > x.length) {index = 1}
  if (n < 1) {index = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  for (i = 0; i < y.length; i++) {
    y[i].className = y[i].className.replace("check-current", "");
  }
  x[index-1].style.display = "block";
  y[index-1].className += "check-current";
}