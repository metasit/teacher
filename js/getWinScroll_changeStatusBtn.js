/* getWinScroll function for updateorder_status-admin button on ระบบหลังบ้านร้านค้า.php */
function getWinScroll_changeStatusBtn(x,y,z) {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	window.location.href = 'updateorder_status-admin.php?win_scroll='+winScroll + '&order_status='+x + '&order_group='+y + '&slip_image='+z;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* getWinScroll function for updatedonate_status-admin button on ระบบหลังบ้านบริจาค.php */
function getWinScroll_changeStatusBtn_updatedonate_status(x,y) {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	window.location.href = 'updatedonate_status-admin.php?win_scroll='+winScroll + '&donate_status='+x + '&donate_id='+y;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* getWinScroll function for updatemembership_status-admin button on ระบบหลังบ้านบำรุงสมาชิก.php */
function getWinScroll_changeStatusBtn_updatemembership_status(x,y,z) {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	window.location.href = 'updatemembership_status-admin.php?win_scroll='+winScroll + '&membership_upgrade='+x + '&membership_ID='+y + '&membership_id='+z;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* getWinScroll function for update_basic_score_status-admin button on ระบบหลังบ้านครูขั้นพื้นฐาน.php */
function getWinScroll_changeStatusBtn_updatebasic_score_status(x,y,z) {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	window.location.href = 'updatebasic_score_status-admin.php?win_scroll='+winScroll + '&basic_score_status='+x + '&ID='+y + '&basic_score_doc='+z;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* getWinScroll function for update_basic_score4stu2_status-admin button on ระบบหลังบ้านเด็กพื้นฐาน-มีครูที่ปรึกษา.php */
function getWinScroll_changeStatusBtn_updatebasic_score4stu2_status(x,y,z) {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	window.location.href = 'updatebasic_score4stu2_status-admin.php?winScroll='+winScroll + '&basic_score_status='+x + '&ID='+y + '&page='+z;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* getWinScroll function for update_basic_score4stu2_status-admin button on ระบบหลังบ้านเด็กพื้นฐาน-ไม่มีครูที่ปรึกษา.php */
function getWinScroll_changeStatusBtn_updatebasic_score4stu2_status_notea(x,y,z) {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	window.location.href = 'updatebasic_score4stu2_status-ไม่มีครูที่ปรึกษา-admin.php?winScroll='+winScroll + '&basic_score_status='+x + '&ID='+y + '&page='+z;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* getWinScroll function for update_basic_score4stu2_status-admin button on ระบบหลังบ้านครูชั้นที่5.php */
function getWinScroll_changeStatusBtn_updatefifth_score4tea_status(x,y,z) {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	window.location.href = 'updatefifth_score4tea_status-admin.php?winScroll='+winScroll + '&fifth_score_status='+x + '&ID_tea='+y + '&page='+z;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////