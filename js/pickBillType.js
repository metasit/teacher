function PickBillIN(val){

 var elePer = document.getElementById('ID-Personal');
 var eleCor = document.getElementById('ID-Corporation');
 var eleGov = document.getElementById('ID-Government');
 var elePerReq = document.getElementById('ID-Personal-required');
 var eleCorReq = document.getElementById('ID-Corporation-required');
 var eleGovReq = document.getElementById('ID-Government-required');

 if(val=='Personal') {
	elePer.style.display='block';
	eleCor.style.display='none';
	eleGov.style.display='none';
	elePerReq.required=true;
	eleCorReq.required=false;
	eleGovReq.required=false;
 }else if(val=='Corporation') {
 	elePer.style.display='none';
	eleCor.style.display='block';
	eleGov.style.display='none';
	elePerReq.required=false;
	eleCorReq.required=true;
	eleGovReq.required=false;
 }else if(val=='Government') {
	elePer.style.display='none';
	eleCor.style.display='none';
	eleGov.style.display='block';
	elePerReq.required=false;
	eleCorReq.required=false;
	eleGovReq.required=true;
 }else{
	elePer.style.display='none';
	eleCor.style.display='none';
	eleGov.style.display='none';
	elePerReq.required=false;
	eleCorReq.required=false;
	eleGovReq.required=false;
 }
}