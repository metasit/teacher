// Set the timeout
if(typeof localStorage.getItem('currenttime') !== 'undefined' && localStorage.getItem('currenttime') > 0) {
	if(localStorage.getItem('currenttime') < 180) {
		var currenttime = localStorage.getItem('currenttime');
	}
}else{
	var currenttime = 181;
}

// Update the count down every 1 second
setInterval(function() {
	
	if(currenttime > 0) {
		currenttime--;
	}

	localStorage.setItem('currenttime', currenttime);

	// Clear currenttime when ปุ่มข้อถัดไป has been selected
	var nextBtn = document.getElementById('nextBtn');
	nextBtn.onclick = function() {
		localStorage.clear('currenttime');
	}
	
	if(currenttime >= 0) {
		document.getElementById("timer").innerHTML = currenttime;
	}
    
  if(currenttime === 0) {
		var currentpage = document.getElementById("CFP").value;
		if(currentpage < 79) {
			if(currentpage == 57) {
				window.location.replace('addfifth_score_A-ครู.php?CFP='+currentpage+'&fifth_score_A_55=0&fifth_score_A_56=0&&fifth_score_A_57=0');
			}else{
				window.location.replace('addfifth_score_A-ครู.php?CFP='+currentpage+'&fifth_score_A=0');
			}
		}else{
			window.location.replace('addfifth_score_A-ครู.php');
		}
	}

}, 1000);