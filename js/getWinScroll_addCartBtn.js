/* getWinScroll function for addcart button on สนับสนุนของที่ระลึก.php */
function getWinScroll_addCartBtn(x,y,z) {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	window.location.href = 'addcart.php?addcart=1&win_scroll='+winScroll + '&product_id='+x + '&product_name='+y + '&product_image='+z;
}

/* getWinScroll function for addcart button on สนับสนุนของที่ระลึก-en.php */
function getWinScroll_addCartBtn_en(x,y,z) {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	window.location.href = 'addcart-en.php?addcart=1&win_scroll='+winScroll + '&product_id='+x + '&product_name='+y + '&product_image='+z;
}