// Set the timeout
if(typeof localStorage.getItem('currenttime') !== 'undefined' && localStorage.getItem('currenttime') > 0) {
	if(localStorage.getItem('currenttime') < 45) {
		var currenttime = localStorage.getItem('currenttime');
	}
}else{
	var currenttime = 46;
}

// Clear currenttime when ปุ่มข้อถัดไป has been selected
var nextBtn = document.getElementById('nextBtn');
nextBtn.onclick = function() {
	localStorage.clear('currenttime');
}

// Update the count down every 1 second
setInterval(function() {
	if(currenttime > 0) {
		currenttime--;
	}

	localStorage.setItem('currenttime', currenttime);
	
	if(currenttime >= 0) {
		document.getElementById("timer").innerHTML = currenttime;
	}
    
  if(currenttime === 0) {
		var currentpage = document.getElementById("CFP").value;
		window.location.replace('addfifth_score_A-ผู้บริหาร-ครู.php?CFP='+currentpage+'&fifth_score_A=0');
	}

}, 1000);