<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];

  $number_count = "SELECT count(*) FROM letter";
  $result = $con->query($number_count);  
  while($row_token = $result->fetch_assoc()){
    $number_count =  $row_token['count(*)'];
  }
  $pg = 0;
  if(in_array("pg", array_keys($_GET))){
      $pg = $_GET['pg'];
  }
  else {
      $pg = 1;
  }

  if($pg==1){
      $start = $pg -1;
  }
  else {
      $start =(($pg - 1) * 8 ) +1;
  }
  $end = $pg *8;
  

  if($pg>3){
    $pg_token = ceil($number_count/8);
    $st = $pg -2;
    $en = $pg +2;
    for ($x=$st; $x <=$en ; $x++) { 
        if($x <=$pg_token)
        $paginetion[] =$x; 
    } 
  }
  else {
      if(true){
        $pg_token = ceil($number_count/8);
        for ($x=1; $x <=$pg_token ; $x++) { 
          if($x <=$pg_token)
          $paginetion[] = $x;
        } 
      }
  }
  

  

  if($start!= 0) $start = $start - 1;
  $sql_letter_list  ="SELECT * FROM 	letter  order by id_letter desc limit $start ,8  ";
  $list_letter = $con->query($sql_letter_list);
  
?>

<!DOCTYPE html>
<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="จดหมายรวมlatest-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="จดหมายรวมlatest.php">จดหมาย/ประกาศ</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 03 - จดหมาย -->
<div style="background-color:rgb(226,255,224);">
  <div class="hoc container clear">
    <table class="table1 table2" style="padding-bottom:20px;">
      <thead>
        <tr>
          <th</th>
          <!-- <th></th> -->
        </tr>
      </thead>
      <!-- ################################################################################################ -->
      <tbody>
        <!-- จดหมาย 024 -->
        <?php 

            function yearCover($year){
              $token_year = explode("/",$year);
              $day = $token_year[0];
              $month = $token_year[1];
              $year_token =  $token_year[2] +543;
              // return $token_year[2];
              return $day."/".$month."/".$year_token;
            }
              function Date_cover($str){
                $month = ["","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฏาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม "];
                $number =  explode("-",$str)[1];
                $month = $month[$number+0];
                $day = explode("-",$str)[2];
                $year = explode("-",$str)[0];
                return $year." ".$month." ".$day;
              }

        while($row = $list_letter->fetch_assoc()) {
          $id = $row['id_letter'];
          $status = $row['status'];
          ?>
          <tr>
            <td>
            <a  style="width:100%; display:flex;" class="linkfortable" href="<?php echo "letter_detail.php?id=".$id ?>">
            
            <span style="margin-right:auto; display:flex;">
              <span  style="" >
                <?php  echo $row['title']; ?>

                <?php  if($status){ ?>
                
                  <?php 
                    if($status=="1"){
                      echo "<span class='blink1'> new </span> ";
                    }
                    elseif($status=="2") {
                      echo "<span class='blink1'> hot </span>";
                    }
                  ?>
                </span>
                <?php } ?>
              </span>
              <!--  -->
            </span>
            <span style=""><?php echo  yearCover(Date_cover(explode(" ",$row['day_open'])[0])); ?></span>
            
          </a></td>
            <!-- <td style="color:rgb(238,195,38,0.8); text-align:right;"></td> -->
          </tr>
          <?php 
        }
        ?>
        <!-- จดหมาย 023 -->


        <!-- <tr>
          <td>
						<a class="blink1">Hot</a>
            <a class="linkfortable" href="จดหมาย023.php">ประกาศรางวัลครูดีของแผ่นดินขั้นพื้นฐาน ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr> -->


        <!-- จดหมาย 022 -->
				<!--
        <tr>
          <td>
						<a class="blink1">Hot</a>
            <a class="linkfortable" href="จดหมาย022.php">ประกาศรางวัลครูดีของแผ่นดิน ชั้นที่ 5 ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
				-->
        <!-- จดหมาย 021 -->


        <!-- <tr>
          <td>
						<a class="blink1">Hot</a>
            <a class="linkfortable" href="จดหมาย021.php">ประกาศรางวัลศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr> -->


        <!-- จดหมาย 020 -->
				<!--
        <tr>
          <td>
						<a class="blink1">Hot</a>
            <a class="linkfortable" href="จดหมาย020.php">ประกาศรางวัลศึกษานิเทศก์ดี ขั้นเกียรติคุณ ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr>
				-->
        <!-- จดหมาย 019 -->
        <!-- <tr>
          <td>
            <a class="linkfortable" href="จดหมาย019.php">สพฐ. แจ้งแนวทางการคัดเลือกรางวัล คุรุชน คนคุณธรรม (ร่วมกับมูลนิธิครูดีของแผ่นดิน)</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr> -->
				<!-- จดหมาย 018 -->
        <!-- <tr>
          <td><a class="linkfortable" href="จดหมาย018.php">หนังสือโครงการเครือข่ายครูดีของแผ่นดิน ถึงหน่วยงานที่ MOU ร่วมกับมูลนิธิครูดีของแผ่นดิน</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr> -->
        <!-- จดหมาย 017 -->
        <!-- <tr>
          <td><a class="linkfortable" href="จดหมาย017.php">หนังสือแจ้งปฏิทินการดำเนินโครงการฯ ปี 2563</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr> -->
        <!-- จดหมาย 016 -->
        <!-- <tr>
          <td>
            <a class="linkfortable" href="จดหมาย016.php">ประกาศรางวัลครูดีของแผ่นดิน ชั้นที่ 4 ปี 2563</a>
          </td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr> -->
				<!-- จดหมาย 015 -->
        <!-- <tr>
          <td><a class="linkfortable" href="จดหมาย015.php">ทุนการศึกษาสำหรับเด็กและเยาวชนดีของแผ่นดิน ช่วงวิกฤตโรคติดต่อโควิด-๑๙</a></td>
          <td style="color:rgb(238,195,38,0.8); text-align:right;"></td>
        </tr> -->
      </tbody>
    </table>
  </div>
</div>
<!-- End Content 03 - จดหมาย -->
<!-- ################################################################################################ -->
<!-- Start Pagination -->
<div class="wrapper row3 coloured" style="background-color:rgb(226,255,224);">
  <main class="hoc container clear">
    <div class="content">
      <nav class="pagination">
        <ul>
        <?php  foreach ($paginetion as $key => $value) { ?>
                <li class="<?php  if($value==$pg) echo "current" ?>" ><a href="จดหมายรวมlatest.php?pg=<?php echo $value   ?>"><?php echo $value; ?></a></li>
        <?php }  ?>
          <!-- <li class="current"><strong>3</strong></li>
          <li><a href="จดหมายรวม002.php">2</a></li>
          <li><a href="จดหมายรวม001.php">1</a></li> -->
        </ul>
      </nav>
    </div>
  </main>
</div>
<!-- End Pagination -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>