<?php 
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$ID = $_SESSION['ID'];

	$CFP = $_GET['CFP'];

	if(isset($ID) && $CFP == 'ส่วน1') {
		
	}else{
		echo "<script>window.history.go(-1)</script>";
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="m-t-90"></div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ประเมินศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row">
			<div class="col-lg-12">
				<div class="table-main table-responsive hoc container">
					<!-- ################################################################################################ -->
					<div class="center">
						<h8 class="m-b-20">หมวด: ครองงาน</h8>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-6 col-md-6 col-lg-6 fs-20">
							<p>ข้อที่ / ทั้งหมด</p>
							<p class="bold">26-30 / 37</p>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6 right fs-20">
							<p>เหลือเวลา</p>
							<div>
								<p class="bold m-t-1" id="timer" onload="localStorage.getItem('currenttime')">.</p> วินาที
							</div>
						</div>
					</div>

					<form action="addhonor_score_A4work-ศน.php" method="GET">
						<table class="table m-t-20" style="background-color:rgb(240,240,240);">
							<thead>
								<tr>
									<th rowspan="2" style="width:5%;">ข้อ</th>
									<th rowspan="2" style="width:60%;">รายการประเมิน</th>
									<th colspan="5" style="width:35%;">ระดับคุณภาพ</th>
								</tr>
								<tr>
									<th>1</th>
									<th>2</th>
									<th>3</th>
									<th>4</th>
									<th>5</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<!-- No. 26 -->
									<td class="price-pr bold">
										<p>26</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										ความสามารถในการการคิดอย่างมีเหตุผล คิดเชิงระบบ ภายใต้กรอบของกฎหมาย ระเบียบ และศีลธรรม
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore26" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore26" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore26" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore26" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore26" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 27 -->
									<td class="price-pr bold">
										<p>27</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										ความสามารถในการคิดเชิงสร้างสรรค์ คิดเชิงนวัตกรรม เปิดกว้างทางความคิด กล้าริเริ่มสิ่งใหม่ เพื่อรองรับการเปลี่ยนแปลงที่เกิดขึ้น
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore27" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore27" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore27" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore27" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore27" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 28 -->
									<td class="price-pr bold">
										<p>28</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										ความสามารถในการนำบทเรียนในอดีตมาพัฒนางานและตัดสินใจสร้างผลลัพธ์ที่ท้าทายในอนาคต
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore28" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore28" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore28" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore28" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore28" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 29 -->
									<td class="price-pr bold">
										<p>29</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										ความสามารถในการสร้างสรรค์ และพัฒนา แนวทาง/ผลงานอย่างต่อเนื่อง จนเกิดแนวทาง/ผลงานที่มีคุณภาพสูงขึ้นเป็นลำดับ โดยไม่สร้างความเดือดร้อนให้ตนเอง และผู้อื่น
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore29" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore29" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore29" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore29" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore29" value="5" required></td>
								</tr>
								<tr>
									<!-- No. 30 -->
									<td class="price-pr bold">
										<p>30</p>
									</td>
									<!-- รายการประเมิน -->
									<td class="name-pr bold" style="text-align: left;">
										ความสามารถในการนำนวัตกรรมทางการนิเทศการศึกษา ส่งผลต่อการพัฒนาผู้บริหาร ครู และคุณภาพการศึกษาอย่างแท้จริง
									</td>
									<!-- ระดับคุณภาพ -->
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore30" value="1" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore30" value="2" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore30" value="3" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore30" value="4" required></td>
									<td class="inline center"><input type="radio" class="basic_score_sty" name="workscore30" value="5" required></td>
								</tr>
							</tbody>
						</table>

						<input type="hidden" name="CFP" value="ส่วน2">
						<button type="submit" id="nextBtn" class="btnJoin" style="color:white; cursor:pointer; width:80%;"><h1>ส่วนถัดไป</h1></button>
					</form>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ประเมินศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script>
	// Set the timeout
	if(typeof localStorage.getItem('currenttime') !== 'undefined' && localStorage.getItem('currenttime') > 0) {
		if(localStorage.getItem('currenttime') < 225) {
			var currenttime = localStorage.getItem('currenttime');
		}
	}else{
		var currenttime = 226;
	}

	// Clear currenttime when ปุ่มข้อถัดไป has been selected
	var nextBtn = document.getElementById('nextBtn');
	nextBtn.onclick = function() {
		localStorage.clear('currenttime');
	}
	
	// Update the count down every 1 second
	setInterval(function() {
		if(currenttime > 0) {
			currenttime--;
		}

		localStorage.setItem('currenttime', currenttime);
		
		if(currenttime >= 0) {
			document.getElementById("timer").innerHTML = currenttime;
		}
			
		if(currenttime === 0) {
			window.location.replace('addhonor_score_A4work-ศน.php?CFP=ส่วน2&timeout=yes');
		}

	}, 1000);
</script>

<script>
	history.replaceState(null, null, 'ประเมินศน.ดีของแผ่นดินขั้นเกียรติคุณ-ครองงาน.php');
</script>

</body>
</html>