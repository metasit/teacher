<?php 
	session_start();
	if(!isset($_SESSION['email'])) { // Check if user doesn't log in, then go to login page
		header('location: login.php');
	}else{
			if($_SESSION['level'] == 'admin') { // admin can access this SESSION only
				require_once('condb.php');
				$order_group = $_GET['order_group'];

				$sqlorders1="SELECT * FROM orders WHERE order_group='$order_group' ";
				$resultorders1=mysqli_query($con,$sqlorders1);
				$roworders1=$resultorders1->fetch_assoc();

				if(is_null($roworders1['slip_image']) || empty($roworders1['slip_image'])) { // Check that order not รอการชำระเงิน status
					echo '<script>';
						echo "alert('ยังไม่ได้ทำการแนบหลักฐานการโอน จึงไม่สามารถพิมพ์ใบเสร็จได้ค่ะ');";
						echo "window.location.replace('ระบบหลังบ้านสนับสนุนมูลนิธิ.php?win_scroll=$win_scroll')";
					echo '</script>';
				}else{ // SESSION allow to access this page
					/* Set shipping_cost value */
					if($roworders1['shipping_kind'] == 'normal') {
						$shipping_cost = 50;
					}else{
						$shipping_cost = 80;
					}
					/*
					$shipping_cost = 0;
					*/
					/* Set price_total value */
					$sqltotal="SELECT SUM(product_price_sale*product_amount) AS price_total FROM orders WHERE order_group='$order_group' ";
					$resulttotal=mysqli_query($con,$sqltotal);
					$rowtotal=mysqli_fetch_array($resulttotal);
					/* Set sale_total value */
					$sqlsale="SELECT SUM(product_price-product_price_sale) AS sale_total FROM orders WHERE order_group='$order_group' ";
					$resale=mysqli_query($con,$sqlsale);
					$rowsale=mysqli_fetch_array($resale);
					/* Call bill table from database */
					$sqlbill="SELECT * FROM bill WHERE order_group='$order_group' ";
					$rebill=mysqli_query($con,$sqlbill);
					$rowbill=$rebill->fetch_assoc();
					/* NOTE: Create function to use in ใบเสร็จC */
					/* convert currency number to thai word */
									function Convert($amount_number)
					{
							$amount_number = number_format($amount_number, 2, ".","");
							$pt = strpos($amount_number , ".");
							$number = $fraction = "";
							if ($pt === false) 
									$number = $amount_number;
							else
							{
									$number = substr($amount_number, 0, $pt);
									$fraction = substr($amount_number, $pt + 1);
							}
							
							$ret = "";
							$baht = ReadNumber ($number);
							if ($baht != "")
									$ret .= $baht . "บาท";
							
							$satang = ReadNumber($fraction);
							if ($satang != "")
									$ret .=  $satang . "สตางค์";
							else
									$ret .= "ถ้วน";
							return $ret;
					}

					function ReadNumber($number)
					{
							$position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
							$number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
							$number = $number + 0;
							$ret = "";
							if ($number == 0) return $ret;
							if ($number > 1000000)
							{
									$ret .= ReadNumber(intval($number / 1000000)) . "ล้าน";
									$number = intval(fmod($number, 1000000));
							}
							
							$divider = 100000;
							$pos = 0;
							while($number > 0)
							{
									$d = intval($number / $divider);
									$ret .= (($divider == 10) && ($d == 2)) ? "ยี่" : 
											((($divider == 10) && ($d == 1)) ? "" :
											((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
									$ret .= ($d ? $position_call[$pos] : "");
									$number = $number % $divider;
									$divider = $divider / 10;
									$pos++;
							}
							return $ret;
					}
				}
			}else{
				header("Location: javascript:history.go(-1);");
			}
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
	
<style>
	div	{
		font-family: Angsana New;
		color: black
	}
	th, td {
		font-family: Angsana New;
	font-size:23px;
		color:black;
		text-align: center;
		font-weight: 500;
	}
</style>
</head>

<body class="BG-diamond">
<div class="hoc clear" style="width:150px;">
		<div class="BG-green1 m-t-50 center fs-20" style="color:white; padding:10px; border-radius:5px; cursor:pointer; font-family:KanitLight;" 
		onmousemove="this.style.backgroundColor='transparent'; this.style.border='1px solid white';" 
		onmouseout="this.style.backgroundColor='rgb(72,160,0)'; this.style.border='none';"
		onclick="javascript:this.style.display='none'; window.print();">พิมพ์ใบเสร็จ</div>
</div>

<div class="BG-white m-l-r-auto m-t-50 m-b-50" style="width:984px;">
	<!-- Start ต้นฉบับ -->
	<?php include('includes/ใบเสร็จCTop.php'); ?>
		ต้นฉบับ
	<?php include('includes/ใบเสร็จCBottom.php'); ?>
	<!-- End ต้นฉบับ -->
	<!-- ################################################################################################ -->
	<!-- ################################################################################################ -->
	<!-- ################################################################################################ -->
	<!-- Start สำเนา -->
	<?php include('includes/ใบเสร็จCTop.php'); ?>
		สำเนา
	<?php include('includes/ใบเสร็จCBottom.php'); ?>
	<!-- End สำเนา -->

</div>

</body>
</html>