<?php 
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		$sqlfifth_score4tea_codeA = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='A' ";
		$reA = mysqli_query($con, $sqlfifth_score4tea_codeA);
		$rowA = mysqli_fetch_array($reA);
		if($rowA['fifth_score4tea_remark'] == 'ผ่าน') {
			echo '<script>';
				echo "alert('คุณผ่านแล้วค่ะ');";
				echo "window.location.replace('index.php')";
			echo '</script>';
		}
		if($_GET['CFP'] == 'ประเมินครูดีของแผ่นดินชั้นที่5_ทำอีกครั้ง') {
			$sqldelete = "DELETE FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='A' ";
			$resdelete = $con->query($sqldelete) or die($con->error);
		}
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li><a href="all_project.php"> ร่วมโครงการ</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการครูดีของแผ่นดิน.php"> โครงการครูดีของแผ่นดิน</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการครูดีของแผ่นดินชั้นที่5-lobby.php"> ครูดีของแผ่นดินชั้นที่ 5</a></li>
		<i class="fas fa-angle-double-right"></i>
    <li><a href="#" onclick="return false"> ประเมินครูดีของแผ่นดินชั้นที่ 5</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ประเมินครูดีของแผ่นดินขั้นพื้นฐาน -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<div class="hoc container clear center">
		<!-- ################################################################################################ -->
		<h8>แบบประเมินครองตน ครองคน ครองงาน</h8>
		<br>
		<h9>เพื่อการพิจารณารับรางวัลครูดีของแผ่นดิน</h9>
		<br><br>
		<h9>ฉบับผู้เข้าร่วมโครงการ</h9>
		<p class="m-t-50 fs-30 lh-1-3" style="font-family:RSUText; text-align:left;">
			<strong>คำชี้แจง</strong>
			<br>
			1.	คำถามมีทั้งหมด 3 หมวด รวมทั้งสิ้น 79 ข้อ เมื่อท่านตอบคำถามแล้ว ท่านจะไม่สามารถกลับมาแก้ไขคำตอบได้
			<br>
			2.	กรุณาตอบภายในเวลาที่กำหนด (1 ข้อ ไม่เกิน 45 วินาที) หากท่านตอบไม่ทันเวลาที่กำหนด ท่านจะได้คะแนน 0 คะแนนในข้อนั้นๆ
			<br>
			3.	กรุณาตอบคำถามด้วยตัวท่านเองเท่านั้น
			<br>
			4.	กรุณาตามคำถามจากสิ่งที่ท่านได้ปฏิบัติจริงในชีวิตประจำวัน ห้ามตอบตามทฤษฎีหรือสิ่งที่ควรจะเป็น
			<br>
			5.	หากกรรมการตรวจสอบได้ว่าท่านตอบไม่ตรงกับสิ่งที่ท่านปฏิบัติจริง จะถือว่าท่านให้ข้อมูลเท็จ ซึ่งจะส่งผลให้ท่านถูกตัดสิทธิ์การรับรางวัลทันที
		</p>
		<!-- ################################################################################################ -->
		<div class="one_first">
			<div class="btnJoin">
				<?php
					if(mysqli_num_rows($reA) == 0) { ?>
						<a href="ประเมินครูดีของแผ่นดินชั้นที่5-ข้อ1.php?CFP=0"><h6>เริ่มทำประเมิน</h6></a> <?php
					}else{ ?>
						<a href="ประเมินครูดีของแผ่นดินชั้นที่5-ข้อ1.php?CFP=0"><h6>ทำประเมิน</h6></a> <?php
					}
				?>
			</div>
		</div>
	<!-- ################################################################################################ -->
	</div>
</div>
<!-- End Content 00 - ประเมินครูดีของแผ่นดินขั้นพื้นฐาน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

</body>
</html>