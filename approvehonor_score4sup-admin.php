<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$now_date = date("d-m-Y H:i:s");

	$ID = $_SESSION['ID']; // admin ID
	$ID_sup = $_GET['ID_sup']; // supervisor ID

	/* Set all value */
	$winScroll = $_GET['winScroll'];
	$honor_score4sup_id = $_GET['honor_score4sup_id'];

	$honor_score4sup_check_status = 'อนุมัติ,'.$now_date;

	/* Update status in honor_score4sup table */
	$sql = "UPDATE `honor_score4sup` SET `honor_score4sup_check_status`='$honor_score4sup_check_status' WHERE `honor_score4sup_id`='$honor_score4sup_id' ";
	$res= $con->query($sql) or die($con->error); //Check error

	/* Log User Action */
	$scorelog_task = 'ตรวจรายงานการนิเทศ,'.$_GET['honor_score4sup_code'];
	$scorelog_detail = 'ศน.,เกียรติคุณ,'.$honor_score4sup_check_status;
	$scorelog_total = $ID_sup; // ID of supervisor who รายงานการนิทเศ

	$sqllog="INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail, scorelog_total) 
	VALUES ('$ID', '$scorelog_task', '$scorelog_detail', '$scorelog_total') ";
	$relog = $con->query($sqllog) or die($con->error); //Check error

	header('location: รายงานการนิเทศ-admin.php?winScroll='.$winScroll.'&ID_sup='.$_GET['ID_sup'].'&sup_name='.$_GET['sup_name'].'&sup_email='.$_GET['sup_email']);
?>