<!-- dashboard for Join Program Name List -->
<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		
		if($_GET['CFP'] == 'dashboard' || $_GET['CFP'] == 'dashboardJoinProNameList' || $_GET['CFP'] == 'dashboardJoinProNameScoreList') {

			///////////////////////////////////////////////////////////////////////////////////////////////////////
			/* Set program */
			$program = $_GET['program'];
			if($program == 'tea') {
				$program_name = 'โครงการครูดีของแผ่นดิน';
			}
			if($program == 'stu') {
				$program_name = 'โครงการเด็กดีของแผ่นดิน';
			}
			if($program == 'sup') {
				$program_name = 'โครงการศึกษานิเทศดีของแผ่นดิน';
			}
			///////////////////////////////////////////////////////////////////////////////////////////////////////
			/* Set list */
			$list = $_GET['list'];
			if($list == 'all') {
				$list_name = 'สมาชิกทั้งหมด';
			}
			if($list == 'basic_score_pass') {
				$list_name = 'ผ่านขั้นพื้นฐาน';
			}
			if($list == 'fifth_score_proc') {
				$list_name = 'ดำเนินการชั้นที่ 5';
			}
			if($list == 'fifth_score_pass') {
				$list_name = 'ผ่านชั้นที่ 5';
			}
			if($list == 'honor_score_proc') {
				$list_name = 'ดำเนินการขั้นเกียรติคุณ';
			}
			if($list == 'honor_score_pass') {
				$list_name = 'ผ่านขั้นเกียรติคุณ';
			}
			///////////////////////////////////////////////////////////////////////////////////////////////////////
			/* สังกัด ที่ User ต้องการดู */
			$affil_code_filter = $_GET['affil_code_filter'];
			///////////////////////////////////////////////////////////////////////////////////////////////////////



			if(isset($_GET['vip_submit_btn'])) {

				/*
					$vip_pass_status = 0: Wrong password
					$vip_pass_status = 1: Correct password. Then, open SESSION for this account
				*/

				$sqlcheck_vip_permis = "SELECT ID FROM `dash` WHERE ID='$ID' ";
				$reCVPe = mysqli_query($con, $sqlcheck_vip_permis);

				if(mysqli_num_rows($reCVPe) == 1) {

					$vip_pass = $_GET['vip_pass'];
					$sqlcheck_vip_pass = "SELECT vip_pass FROM `dash` WHERE ID='$ID' AND vip_pass='$vip_pass' ";
					$reCVPa = mysqli_query($con, $sqlcheck_vip_pass);

					if(mysqli_num_rows($reCVPa) == 1) {

						$_SESSION['vip_pass_status'] = 1;

						echo '<script>';
							echo "alert('เข้าสู่ระบบการดูคะแนนรายบุคคล และสามารถดาวโหลดข้อมูลในรูปแบบไฟล์ PDF ได้ค่ะ');";
							echo "window.location.replace('dashboardJoinProNameScoreList.php?CFP=dashboardJoinProNameList&program=".$program."&list=".$list."&affil_code_filter=".$affil_code_filter."')";
						echo '</script>';

					}elseif(mysqli_num_rows($reCVPa) == 0) {

						$_SESSION['vip_pass_status'] = 0;

						echo '<script>';
							echo "alert('รหัสผ่านไม่ถูกต้อง');";
						echo '</script>';

					}

				}elseif(mysqli_num_rows($reCVPe) == 0){

					echo '<script>';
						echo "alert('คุณไม่ได้รับสิทธิ์ในการดูคะแนนรายบุคคล และดาวน์โหลดข้อมูล ถ้าคุณควรจะได้รับสิทธิ์ กรุณาติดต่อเจ้าหน้าที่มูลนิธิฯ');";
					echo '</script>';

				}
				
			}elseif($_SESSION['vip_pass_status'] == 1) {

				header('location: dashboardJoinProNameScoreList.php?CFP=dashboardJoinProNameList&program='.$program.'&list='.$list.'&affil_code_filter='.$affil_code_filter);

			}

		}else{
			echo "<script>window.history.go(-1)</script>";
		}
		
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">

	<!-- UI, Bootstrap 4.5.3 -->
	<link rel="stylesheet" href="css/bootstrap.min453.css" rel="stylesheet" type="text/css" media="all">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

	<style>
		.affil-menu {
			background-color: rgba(72, 160, 0, 0.1);
			padding: 10px;
			border-radius: 5px;
		}
		.affil-menu-head {
			font-size: 20px;
			text-decoration: underline;
		}
		.affil-menu-head:hover {
			text-decoration: none;
		}
		.modal {
			top: 20%;
		}
		.btn {
			border: none;
		}
	</style>
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['winScroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="index-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="dashboard.php">Dashboard</a></li>
    <i class="fas fa-angle-double-right"></i>
		<li class="linkst">
			<a href="#" onclick="return false">รายชื่อผู้เข้าร่วม
				<?php
					echo $program_name.' - '.$list_name;
				?>
			</a>
		</li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row" style="margin:0 100px; overflow-x:auto;">
			<section class="col-md-12 fs-40 bold text-gold center" style="line-height:80px;">
				<?php
					echo $program_name.' - '.$list_name;
				?>
			</section>
			<!-- ################################################################################################ -->
			<!-- Start vip button  -->
			<section class="col-md-12">

				<div class="right">
					<!-- Button for password trigger modal -->
					<button type="button" class="btn" name="vip_submit_btn" data-toggle="modal" data-target="#staticBackdrop">ดูคะแนนรายบุคคล และดาวน์โหลดข้อมูล</button>
				</div>

				<!-- ################################################################################################ -->

				<?php
					if($_SESSION['vip_pass_status'] == 0) { ?>

						<!-- Password modal for wrong password SESSION -->
						<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									<div class="modal-header BG-green3">
										<h5 class="modal-title" id="staticBackdropLabel">คุณใส่รหัสผ่านไม่ถูกต้อง กรุณาใส่รหัสผ่านอีกครั้ง</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<form action="dashboardJoinProNameList.php" method="GET">
										<div class="modal-body">
											<input type="password" name="vip_pass" class="form-control" placeholder="รหัสผ่าน" required/>
										</div>
										<div class="modal-footer BG-green3">
											<input type="hidden" name="CFP" value="dashboardJoinProNameList">
											<input type="hidden" name="program" value="<?php echo $program; ?>">
											<input type="hidden" name="list" value="<?php echo $list; ?>">
											<input type="hidden" name="affil_code_filter" value="<?php echo $affil_code_filter; ?>">

											<button type="reset" class="btn BG-red1 no-bo" data-dismiss="modal">ยกเลิก</button>
											<button type="submit" class="btn BG-green1 no-bo" name="vip_submit_btn">ยืนยัน</button>
										</div>
									</form>
								</div>
							</div>
						</div> <?php
							
					}else{ ?>

						<!-- Password modal for first time -->
						<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									<div class="modal-header BG-green3">
										<h5 class="modal-title" id="staticBackdropLabel">ใส่รหัสเพื่อเข้าสู่ระบบ</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<form action="dashboardJoinProNameList.php" method="GET">
										<div class="modal-body">
											<input type="password" name="vip_pass" class="form-control" placeholder="รหัสผ่าน" required/>
										</div>
										<div class="modal-footer BG-green3">
											<input type="hidden" name="CFP" value="dashboardJoinProNameList">
											<input type="hidden" name="program" value="<?php echo $program; ?>">
											<input type="hidden" name="list" value="<?php echo $list; ?>">
											<input type="hidden" name="affil_code_filter" value="<?php echo $affil_code_filter; ?>">

											<button type="reset" class="btn BG-red1 no-bo" data-dismiss="modal">ยกเลิก</button>
											<button type="submit" class="btn BG-green1 no-bo" name="vip_submit_btn">ยืนยัน</button>
										</div>
									</form>
								</div>
							</div>
						</div> <?php

					}
				?>
			</section>
			<!-- End vip button  -->
			<!-- ################################################################################################ -->
			<div class="col-md-12 m-t-20">
				<div class="table-main table-responsive" style="margin-bottom:50px">
					<!-- ################################################################################################ -->
					<table class="table" style="background-color:rgb(240,240,240);">
						<thead>
							<tr>
								<th>ลำดับ</th>
								<th>ชื่อ-นามสกุล</th>
								<th>สังกัด</th>
							</tr>
						</thead>
						<tbody class="disable-margin">
							<?php
								/* Set number per page */
								$results_per_page = 500;
								if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
								$start_from = ($page-1) * $results_per_page;

								/* Call data from mySQL */
								if($program == 'tea' && $list == 'all') {
									$sql = "SELECT firstname, lastname, affil_code FROM `login` WHERE (occup_code LIKE 'OcAA%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin') 
									OR (occup_code LIKE 'OcAB%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									OR (occup_code LIKE 'OcAO%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page" ;
									
								}elseif($program == 'tea' && $list == 'basic_score_pass') {
									$sql = "SELECT firstname, lastname, affil_code FROM `login` WHERE (occup_code LIKE 'OcAA%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									OR (occup_code LIKE 'OcAB%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									OR (occup_code LIKE 'OcAO%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'tea' && $list == 'fifth_score_proc') {
									$sql = "SELECT fifth_score4tea.fifth_score4tea_code, login.affil_code, login.firstname, login.lastname 
									FROM `fifth_score4tea` INNER JOIN `login` ON fifth_score4tea.ID=login.ID 
									WHERE fifth_score4tea.fifth_score4tea_code='A' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin'
									ORDER BY login.firstname, login.lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'tea' && $list == 'fifth_score_pass') {
									$sql = "SELECT firstname, lastname, affil_code FROM `login` WHERE (occup_code LIKE 'OcAA%' AND fifth_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									OR (occup_code LIKE 'OcAB%' AND fifth_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'stu' && $list == 'all') {
									$sql = "SELECT firstname, lastname, affil_code FROM `login` WHERE occup_code LIKE 'OcF%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' 
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'stu' && $list == 'basic_score_pass') {
									$sql = "SELECT firstname, lastname, affil_code FROM `login` 
									WHERE occup_code LIKE 'OcF%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' 
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'sup' && $list == 'all') {
									$sql = "SELECT firstname, lastname, affil_code FROM `login` WHERE occup_code LIKE 'OcAD%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin'
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'sup' && $list == 'basic_score_pass') {
									$sql = "SELECT firstname, lastname, affil_code FROM `login` WHERE occup_code LIKE 'OcAD%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin'
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'sup' && $list == 'honor_score_proc') {
									$sql = "SELECT honor_score4suphonor_score4sup_code, login.firstname, login.lastname, login.affil_code 
									FROM `honor_score4sup` INNER JOIN `login` ON honor_score4sup.ID=login.ID
									WHERE honor_score4sup.honor_score4sup_code='A' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin'
									ORDER BY login.firstname, login.lastname LIMIT $start_from, $results_per_page ";

								}elseif($program == 'sup' && $list == 'honor_score_pass') {
									$sqlCount_Total_Sup_Pass_honor_score = "SELECT firstname, lastname, affil_code FROM `login` WHERE occup_code LIKE 'OcAD%' AND honor_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' 
									ORDER BY firstname, lastname LIMIT $start_from, $results_per_page ";
												
								}

								$re = mysqli_query($con, $sql);

								$n = $start_from+1; // Get number of row
								while($row = $re->fetch_assoc()) {
									
									$name = trim($row['firstname']).' '.trim($row['lastname']);
									
									$affil_code = $row['affil_code'];
									include('includes/convert2afftext.php'); ?>

									<tr>
										<!-- No. -->
										<td class="price-pr">
											<p><?php echo $n ?></p>
										</td>
										<!-- Name -->
										<td class="name-pr lh-1-0">
											<p><?php echo $name; ?></p>
										</td>
										<!-- สังกัด-->
										<td class="name-pr lh-1-0">
											<p><?php echo $full_affiliation_name; ?></p>
										</td>
									</tr> <?php
									$n++;
									$selfscore = 0;
									$peoplescore = 0;
									$workscore = 0;
									$k = 0;
								}
							?>
						</tbody>
					</table>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 -->
<?php
					if($program == 'tea' && $list == 'all') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE (occup_code LIKE 'OcAA%' AND affil_code LIKE '{$affil_code_filter}%') 
						OR (occup_code LIKE 'OcAB%' AND affil_code LIKE '{$affil_code_filter}%') ";
						
					}elseif($program == 'tea' && $list == 'basic_score_pass') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE (occup_code LIKE 'OcAA%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
						OR (occup_code LIKE 'OcAB%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin') ";

					}elseif($program == 'tea' && $list == 'fifth_score_proc') {
						$sql = "SELECT COUNT(login.ID) AS total FROM `fifth_score4tea` INNER JOIN `login` ON fifth_score4tea.ID=login.ID 
						WHERE fifth_score4tea.fifth_score4tea_code='A' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin' ";

					}elseif($program == 'tea' && $list == 'fifth_score_pass') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE (occup_code LIKE 'OcAA%' AND fifth_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin')
						OR (occup_code LIKE 'OcAB%' AND fifth_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin') ";

					}elseif($program == 'stu' && $list == 'all') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` 
						WHERE occup_code LIKE 'OcF%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";

					}elseif($program == 'stu' && $list == 'basic_score_pass') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` 
						WHERE occup_code LIKE 'OcF%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";

					}elseif($program == 'sup' && $list == 'all') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` WHERE occup_code LIKE 'OcAD%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";

					}elseif($program == 'sup' && $list == 'basic_score_pass') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` 
						WHERE occup_code LIKE 'OcAD%' AND basic_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";

					}elseif($program == 'sup' && $list == 'honor_score_proc') {
						$sql = "SELECT COUNT(ID) AS total FROM `honor_score4sup` INNER JOIN `login` ON honor_score4sup.ID=login.ID
						WHERE honor_score4sup.honor_score4sup_code='A' AND login.affil_code LIKE '{$affil_code_filter}%' AND login.level!='admin' ";

					}elseif($program == 'sup' && $list == 'honor_score_pass') {
						$sql = "SELECT COUNT(ID) AS total FROM `login` 
						WHERE occup_code LIKE 'OcAD%' AND honor_score_status LIKE '%Approve แล้ว%' AND affil_code LIKE '{$affil_code_filter}%' AND level!='admin' ";
									
					}
					$re = $con->query($sql);
					$row = $re->fetch_assoc();
					$total_pages = ceil($row["total"] / $results_per_page); // calculate total pages with results
?>
            <div class="wrapper row3 coloured" style="background-color:rgb(226,255,224);">
              <main class="hoc container clear">
                <div class="content">
                  <nav class="pagination">
                    <ul>
<?php
                        for ($n=1; $n<=$total_pages; $n++) {  // print links for all pages
                          if ($n==$page)
                          {
                            echo "<li class='current'>";
                          }else{
                            echo "<li>";
                          }
                          echo "<a href='dashboardJoinProNameList.php?page=".$n."&CFP=dashboard&program=".$program."&list=".$list."&affil_code_filter=".$affil_code_filter." ' ";
                          if ($n==$page)  echo " class='curPage'";
                          echo ">".$n."</a> ";
                        }; 
?>
                      </li>
                    </ul>
                  </nav>
                </div>
              </main>
            </div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

</body>
</html>