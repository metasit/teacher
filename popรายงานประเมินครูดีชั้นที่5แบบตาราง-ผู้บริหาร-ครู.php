<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		$name = $_SESSION['firstname'].' '.$_SESSION['lastname'];
	}else{
		header("location: javascript:history.go(-1);");
	}
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="รายงานประเมินครูดีชั้นที่5-ผู้บริหาร.php"><i class="fas fa-arrow-left"></i> กลับไปหน้ารายงานประเมินครูดีชั้นที่ 5</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/มูลนิธิครูดีของแผ่นดิน Logo login.png" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<span class="login100-form-title fs-30 lh-1-1">
					<?php
						$sqllogin = "SELECT * FROM `login` WHERE ID='$ID' ";
						$relogin = mysqli_query($con, $sqllogin);
						$rowlogin = mysqli_fetch_array($relogin);

						if($_SESSION['level'] == "memberGeneral" || $_SESSION['level'] == "memberSilver") { ?>

							สามารถดาวน์โหลดไฟล์ได้เฉพาะสมาชิกระดับทองขึ้นไปเท่านั้น หากต้องการบำรุงสมาชิก <a href="บำรุงค่าสมาชิก.php" class="readall fs-18">กรุณาคลิ๊กที่นี่</a> <?php
						
						}elseif($_SESSION['level'] == "memberGold" || $_SESSION['level'] == "memberDiamond" || $_SESSION['level'] == "admin") { ?>

							เนื่องจากคุณเป็นสมาชิกระดับทองขึ้นไป คุณจะสามารถดาวน์โหลดไฟล์ได้
							<br> <?php

							$sqlfifth_score4tea_codeD = "SELECT * FROM `fifth_score4tea` WHERE ID='$ID' AND fifth_score4tea_code='D' ";
							$reFS4TD = mysqli_query($con, $sqlfifth_score4tea_codeD);
							$rowFS4TD = mysqli_fetch_array($reFS4TD);
							
							if(mysqli_num_rows($reFS4TD) == 0) { ?>

								<a href="createpdf4fifth_score4exe_codeD.php" target="_blank" class="btn2 m-t-20">กดเพื่อดาวน์โหลด</a> <?php

							}else{ ?>
								
								<a href="<?php echo $rowFS4TD['fifth_score4tea_data']; ?>" target="_blank" class="btn2 m-t-20">กดเพื่อดาวน์โหลด</a> <?php

							}
						}else{
							header("Location: javascript:history.go(-1);");
						}
					?>
				</span>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>