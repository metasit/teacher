<?php	
	session_start();
	require_once("condb.php");
	date_default_timezone_set("Asia/Bangkok");
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

			/* Set values for insert into membership table */
			// Set membership_topic
			$membership_topic = $_POST['membership_topic'];
			// Set membership_date
			$date_membership = $_POST['date_membership']; 
			$time_membership = $_POST['time_membership'];
			$membership_date = $date_membership.' '.$time_membership;
			// Set bill_tax_kind
			$bill_tax_kind = $_POST['bill_tax_kind'];
			// Set bill_tax_id
			if($_POST['bill_tax_id_Personal'] != '') {

				$bill_tax_id = $_POST['bill_tax_id_Personal'];

			}elseif($_POST['bill_tax_id_Corporation'] != '') {

				$bill_tax_id = $_POST['bill_tax_id_Corporation'];

			}elseif($_POST['bill_tax_id_Government'] != ''){

				$bill_tax_id = $_POST['bill_tax_id_Government'];

			}else{

				$bill_tax_id = NULL;
				
			}

			$target_dir = 'images/slips/membership/'.$date_membership.'/'; // Set target_directory

			if(!is_dir($target_dir)) { // if there's not folder in target_directory
				mkdir($target_dir); // Create folder name is today_date
			}

			$target_file = $target_dir.'ID'.$ID.' '.basename($_FILES["slip_image"]["name"]); // Save image in the target folder

			if (move_uploaded_file($_FILES["slip_image"]["tmp_name"], $target_file)) {

				/* Save slip location in membership table */
				$sql = "INSERT INTO `membership` (`ID`, `membership_topic`, `membership_status`, `membership_slip_image`, `membership_date`, `bill_tax_kind`, `bill_tax_id`) 
				VALUES ('$ID', '$membership_topic', 'รอตรวจSlip', '$target_file', '$membership_date', '$bill_tax_kind', '$bill_tax_id') ";
				$res= $con->query($sql) or die($con->error);

				echo '<script>';
					echo "alert(\"ทำรายการแจ้งการบำรุงสมาชิกเรียบร้อยแล้วค่ะ\");";
					echo "window.location.replace('บำรุงค่าสมาชิก.php')";
				echo '</script>';

			}else{

				echo "<script>";
					echo "alert(\"โปรดติดต่อผู้พัฒนาระบบของมูลนิธิครูดีของแผ่นดิน เพื่อดำเนินการแก้ไข ขอบคุณครับ\");";
					echo "window.location.replace('บริจาคสนับสนุนโครงการต่างๆ.php')";
				echo "</script>";

			}

	}else{

		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
		
	}
?>