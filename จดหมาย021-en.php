<?php

	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];

?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
                  <ul>
                    <li><a href="จดหมาย021.php">ภาษาไทย</a></li>
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="จดหมายรวมlatest-en.php">Letter/Declaration</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="จดหมาย021-en.php"> ประกาศรางวัลศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน ปี 2563</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - จดหมาย021 -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <article style="text-align:center" >
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px;">
        <strong>ประกาศรางวัลศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน ปี 2563</strong>
      </p>
      <ul class="font-x2plus textlink" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
        <li><iframe src="docs/จดหมาย/จดหมาย21/ศน.ดีของแผ่นดินขั้นพื้นฐาน.pdf" width="100%" height="1200px">ประกาศรางวัลศึกษานิเทศก์ดีของแผ่นดินขั้นพื้นฐาน ปี 2563</iframe></li>
      </ul>
    </article>
  </main>
</div>
<!-- End Content 01 - จดหมาย021 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>