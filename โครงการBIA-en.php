<?php
	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>Foundation of Thai Suprateacher</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<header class="main-header"> <!-- Fixed Menu Feature -->
  <!-- Start Top-PreMenu -->
  <div class="PreMenuBG row1">
    <main class="hoc clear">
      <!-- ################################################################################################ -->
      <ul class="PreMenufaico PreMenu_fl_left">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher/"><i class="fab fa-facebook-f"></i></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i></a></li>
      </ul>
      <!-- ################################################################################################ -->
      <!-- Start NoAccount Session -->
<?php
      if (empty($_SESSION['email']))
      {
?>
      <nav id="mainav2" class="PreMenu_fl_right">
        <ul class="PreMenufaico2">
          <li class="faicon-login"><a href="login-en.php"><i class="fas fa-user-lock"></i> <text1>Log in</text1></a></li>
          <li class="faicon-login"><a href="signup-en.php"><i class="fas fa-file-signature"></i> <text1>Sign up</text1></a></li>
          <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
            <ul>
              <li><a href="โครงการBIA.php">ภาษาไทย</a></li>
              <!--<li><a href="#">Chinese</a></li>-->
            </ul>
          </li>
          <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
            <ul>
              <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
              <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
              <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
              <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
              
            </ul>
          </li>
        </ul>
      </nav>
      <!-- End NoAccount Session -->
      <!-- ################################################################################################ -->
      <!-- Start Admin Session -->
<?php
      }
      elseif ($_SESSION['email'])
      {
        if ($_SESSION["level"]=="admin")
        {
?>
        <nav id="mainav2" class="PreMenu_fl_right">
          <ul class="PreMenufaico2">
            <li style="background-color:rgb(228,0,0);"><i class="fas fa-user-edit"></i> Admin</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการBIA.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Admin Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member General Session -->
<?php
        if ($_SESSION["level"]=="memberGeneral")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:black; background-color:white;"><i class="fas fa-user-check"></i> Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการBIA.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member General Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member SILVER Session -->
<?php
        if ($_SESSION["level"]=="memberSilver")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(169,169,169);"><i class="fas fa-user-check"></i> Silver Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการBIA.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member SILVER Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member GOLD Session -->
<?php
        if ($_SESSION["level"]=="memberGold")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(180,147,31);"><i class="fas fa-user-check"></i> Gold Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการBIA.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
?>
        <!-- End Member GOLD Session -->
        <!-- ################################################################################################ -->
        <!-- Start Member DIAMOND Session -->
<?php
        if ($_SESSION["level"]=="memberDiamond")
        {
?>
        <nav id="mainav2" class="fl_right">
          <ul class="PreMenufaico2">
            <li style="color:white; background-color:rgb(52,52,53); border:1.5px solid white;"><i class="fas fa-user-check"></i> Diamond Member</li>
            <li class="faicon-login"><a href="logout.php"><i class="fas fa-sign-out-alt"></i> <text1>Log out</text1></a></li>
            <li class="faicon-language"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-language"></i> <text1>English</text1></a>
              <ul>
                <li><a href="โครงการBIA.php">ภาษาไทย</a></li>
                <!--<li><a href="#">Chinese</a></li>-->
              </ul>
            </li>
            <li class="btn donate-btn"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i><i class="fas fa-hand-holding-heart"></i> <donate>Donate</donate></a>
              <ul>
                <li><a href="เกี่ยวกับการบริจาค-en.php">เกี่ยวกับการบริจาค</a></li>
                <li><a href="บำรุงค่าสมาชิก-en.php">บำรุงค่าสมาชิก</a></li>
                <li><a href="บริจาคสนับสนุนโครงการต่างๆ-en.php">บริจาคสนับสนุนโครงการต่างๆ</a></li>
                <li><a href="สนับสนุนของที่ระลึก.php">สนับสนุนของที่ระลึกมูลนิธิ</a></li>
                
              </ul>
            </li>
          </ul>
        </nav>
<?php
        }
      }
?>
      <!-- End Member DIAMOND Session -->
      <!-- ################################################################################################ -->
      <div class="search1 PreMenu_fl_right">
        <script async src="https://cse.google.com/cse.js?cx=012269214474321810257:7gat9k9ui6l"></script>
        <div class="gcse-search"></div>
      </div>
      <!-- ################################################################################################ -->
    </main>
  </div>
  <!-- End Top-PreMenu -->

  <!-- Start Top-Menu -->
  <div class="MenuBG row2">
    <div class="hoc clear">
      <!-- ################################################################################################ -->
      <nav id="logoname" class="fl_left">
        <ul style="font-size:20px;">
          <li><a style="vertical-align:12px;" href="index-en.php"><img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="logo"></a></li>
          <li>
            <a style="padding-left:5px;" href="index-en.php"> มูลนิธิครูดีของแผ่นดิน</a><br>
            <a style="padding-left:5px;" href="index-en.php"> Foundation of Thai Suprateacher</a>
          </li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" style="font-size:18px;">
          <li><a href="index-en.php">Home</a></li>
          <li><a href="เกี่ยวกับเรา-en.php">About us</a></li>
          <li class="active"><a href="#" onclick="return false"><i class="fas fa-caret-down"></i>Join us</a>
            <ul>
              <li><a href="ร่วมโครงการ-en.php">โครงการทั้งหมดของมูลนิธิ</a></li>
              <li><a href="โครงการครูดีของแผ่นดิน-en.php">โครงการครูดีของแผ่นดิน</a></li>
              <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">โครงการเด็กดีของแผ่นดิน</a></li>
              <li><a href="โครงการศึกษานิเทศก์ดีของแผ่นดิน-en.php">โครงการศึกษานิเทศก์ดีของแผ่นดิน</a></li>
              <li><a href="โครงการอบรมสัมมนา-en.php">โครงการอบรม/สัมมนา</a></li>
              <li><a href="โครงการBIA-en.php">โครงการ Be Internet Awesome</a></li>
              <li><a href="โครงการอาสาของแผ่นดิน-en.php">โครงการอาสาของแผ่นดิน</a></li>
            </ul>
          </li>
          <li><a href="คลังสื่อนวัตกรรมรวมlatest-en.php">Innovation Library</a></li>
          <li><a href="ติดต่อเรา-en.php">Contact us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- End Top-Menu -->
</header>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index-en.php">Home</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="ร่วมโครงการ-en.php"> Join us</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="โครงการBIA-en.php"> โครงการ Be Internet Awesome</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - รายละเอียดโครงการBIA -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <article style="text-align:center; padding-bottom:50px;">
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px"><strong>ทำไมต้องเรียนรู้ Be internet awesome (by google) </strong></p>
      <img src="images/BInAw Poster02.png" alt="BInAw Poster02">
      <p class="fs-32" style="font-family:RSUText; line-height:30pt; text-align:left;">
        เป็นที่ทราบกันดีว่าในยุคเทคโนโลยีปัจจุบัน เราต่างเริ่มตระหนักถึงปัญหาของเด็กไทยกับพิษภัยจากอินเตอร์เน็ตมากขึ้น ซึ่งเปรียบเสมือนดาบสองคมที่มีทั้งคุณอนันต์และโทษมหันต์ในเวลาเดียวกัน การใช้อินเตอร์เน็ต
        นั้นก่อประโยชน์มหาศาล แต่ก็แอบแฝงไปด้วยอันตรายต่างๆ มากมาย ไม่ว่าจะเป็นเรื่องการเข้าถึงข้อมูลที่ไม่เหมาะสมของเด็ก ค่านิยมผิดๆ ในเรื่องเพศ การล่อลวงในวงสนทนา นอกจากนี้ยังมีเว็บไซต์ที่เต็มไปด้วย
        เรื่องลามก และเรื่องความรุนแรงต่างๆ มากมาย สื่อสีดำและสีเทามีมากมายในเกมส์โทรศัพท์มือถือที่อยู่ใกล้บุตรหลานเราเสียยิ่งกว่าพ่อแม่ ซึ่งยากต่อการควบคุมตรวจสอบได้ อาชญากรรมทางอินเตอร์เน็ตเหล่านี้เป็นภัย
        ที่อันตรายมากพอที่จะกำหนดเส้นทางอนาคตของเด็กได้เลยทีเดียว 
        <br><br>
        ดังนั้น ก่อนที่เด็กไทย โดยเฉพาะบุตรหลานของเราจะตกเป็นเหยื่อของภัยอินเตอร์เน็ตไปมากกกว่านี้ พ่อแม่ หรือครูอาจารย์ที่อยู่ใกล้ชิด จำเป็นอย่างยิ่งที่จะต้องทำความเข้าใจภัยอันตรายต่างๆ อันเนื่องมาจากการ
        เปลี่ยนแปลงที่เกิดขึ้นอย่างรวดเร็วในโลกดิจิตอล เพื่อที่จะรู้เท่าทัน และปรับตัวเองให้รู้จักตั้งรับกับภัยเงียบที่คอยคุกคามอย่างชาญฉลาด ให้พ่อแม่ ครูอาจารย์ ได้ตระหนักถึงความสำคัญในฐานะผู้ปกป้อง คุ้มครอง 
        ให้เด็กและเยาวชนไม่หลงเดินทางผิด คอยประคับประคองให้เดินออกจากปัญหา และพาไปยังหนทางในการดำเนินชีวิตที่ถูกต้องดีงามได้
        <br><br>
        <strong>Be Internet Awesome</strong> จะช่วยสอนให้เด็กๆ ได้รู้จักกับพื้นฐานสำคัญของการเป็นพลเมืองยุคดิจิทัลและความปลอดภัย เพื่อช่วยให้เด็กๆ เป็นนักสำรวจที่ปลอดภัยและมั่นใจในโลกออนไลน์ เป็นทั้งเครื่องมือและ
        แหล่งข้อมูล ให้ท่องอินเตอร์เน็ตอย่างที่เรียกว่า <strong>“เล่นอย่างปลอดภัย เรียนอย่างปลอดภัย และใช้อย่างปลอดภัย”</strong>
        <br><br>
        <strong>พื้นฐานสำคัญ</strong> หลักจรรยาบรรณสูงสุดของยอดนักท่องอินเตอร์เน็ต
        <br>
        <strong>1. Be Internet Smart คิดก่อนแชร์</strong>
        ข่าวต่างๆ (ทั้งดีและไม่ดี) แพร่กระจายในโลกอินเทอร์เน็ตได้อย่างรวดเร็ว และหากไม่คิดให้รอบคอบ เด็กๆ อาจตกอยู่ในสถานการณ์ที่ยากลำบาก ซึ่งอาจมีผลกระทบในระยะยาว ดูวิธีแชร์ข้อมูลกับคนที่รู้จักและไม่รู้จักได้เลย
        <br><br>
        <strong><u>สื่อสารอย่างมีความรับผิดชอบ</u></strong>
        <ul class="fs-32" style="font-family:RSUText; line-height:30pt; text-align:left;">
          <li>ส่งเสริมการคิดก่อนแชร์โดยใช้หลักคิดง่ายๆ ว่าการสื่อสารออนไลน์ก็เหมือนกับการสื่อสารแบบตัวต่อตัว ถ้าไม่ใช่เรื่องที่น่าพูด ก็แปลว่าไม่ใช่เรื่องที่น่าโพสต์</li>
          <li>สร้างคู่มือเกี่ยวกับประเภทการสื่อสารที่ เหมาะสม (และไม่เหมาะสม)</li>
          <li>เก็บรายละเอียดเกี่ยวกับครอบครัวและเพื่อนๆ ไว้เป็นความลับ</li>
        </ul>
        <img src="images/มูลนิธิครูดีของแผ่นดิน GoogleSmart.jpg" alt="GoogleSmart">
      </p>
      <p class="fs-32" style="font-family:RSUText; line-height:30pt; text-align:left;">
        <br>
        <strong>2. Be Internet Alert  ไม่ตกหลุมพรางกลลวง</strong>
        คุณต้องช่วยให้เด็กๆ เข้าใจว่าผู้คนและสิ่งต่างๆ ที่เกิดขึ้นในโลกออนไลน์อาจไม่ได้เป็นอย่างที่เห็น ความสามารถในการแยกแยะระหว่างข้อมูลจริงและเท็จเป็นบทเรียนที่สำคัญอย่างยิ่งต่อความปลอดภัยในโลกออนไลน์
        <br>
        <img src="images/มูลนิธิครูดีของแผ่นดิน GoogleAlert.jpg" alt="GoogleAlert">
        <br><br>
        <strong>3. Be Internet Strong เก็บข้อมูลไว้เป็นความลับ</strong>
        ความเป็นส่วนตัวและความปลอดภัยส่วนบุคคลในโลกออนไลน์และออฟไลน์นั้นก็สำคัญไม่แพ้กัน การปกป้องข้อมูลที่สำคัญช่วยให้เด็กๆ หลีกเลี่ยงความเสียหายต่ออุปกรณ์ ชื่อเสียง และความสัมพันธ์ได้
        <br>
        <img src="images/มูลนิธิครูดีของแผ่นดิน GoogleStrong.jpg" alt="GoogleStrong">
        <br><br>
        <strong>4.	Be Internet Kind เป็นคนดีเท่จะตาย</strong>
        อินเทอร์เน็ตเป็นกระบอกเสียงอันทรงพลังในการเผยแพร่เรื่องที่ดีและไม่ดี ช่วยให้เด็กๆ เลือกทางที่ถูกต้องโดยใช้แนวคิด “จงปฏิบัติต่อผู้อื่นเหมือนที่อยากให้ผู้อื่นปฏิบัติต่อตัวเรา” กับการทำสิ่งต่างๆ ในโลกออนไลน์ 
        ซึ่งจะช่วยให้ได้ผลลัพธ์ที่ดีสำหรับผู้อื่นและกำจัดพฤติกรรมการกลั่นแกล้งให้หมดไป
        <br>
        <img src="images/มูลนิธิครูดีของแผ่นดิน GoogleKind.jpg" alt="GoogleKind">
        <br><br>
        <strong>5.	Be Internet Brave สงสัยเมื่อไร ก็ถามได้เลย</strong>
        บทเรียนหนึ่งที่ควรจะต้องสอนเด็กๆ ไม่ว่าจะพบสิ่งใดในโลกออนไลน์ก็คือ เมื่อใดก็ตามที่มีคำถาม พวกเขาควรพูดคุยกับผู้ใหญ่ที่ไว้วางใจได้อย่างสบายใจ ซึ่งผู้ใหญ่สนับสนุนพฤติกรรมนี้ได้ด้วยการส่งเสริม
        การพูดคุยแบบเปิดเผยทั้งที่บ้านและในชั้นเรียน
        <br>
        <img src="images/มูลนิธิครูดีของแผ่นดิน GoogleBrave.jpg" alt="GoogleBrave">
      </p>
      <!-- ################################################################################################ -->
      <!-- Start VDOs Section -->
      <section class="m-t-80 line2-t-green1">
        <p class="font-x3" style="color:rgb(180,147,31); line-height:50px">
          <strong>กูเกิลเปิดตัว Be Internet Awesome บทเรียนออนไลน์<br>สอนใช้อินเทอร์เน็ตอย่างปลอดภัย</strong>
        </p>
        <iframe width="640px" height="360px" src="https://www.youtube.com/embed/SJtfNNEb_s0"></iframe>
        <p class="font-x3" style="color:rgb(180,147,31); line-height:80px">
          <strong>รายการพุธเช้า ข่าวสพฐ. ตอน BeInternetAwesome วันที่ 18 มี.ค.63</strong>
        </p>
        <iframe width="640px" height="360px" src="https://www.youtube.com/embed/wL3mYFuIk3k"></iframe>
      </section>
      <!-- End VDOs Section -->
      <!-- ################################################################################################ -->
      <ul class="fs-32 textlink m-t-50 p-t-50 line2-t-green1" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
          <li><a href="docs/ร่วมโครงการ/โครงการ Be Internet Awesome/google.pdf#toolbar=0">ดาวน์โหลดภาพรวมการดำเนินโครงการ สำหรับโรงเรียนที่มาสัมมนาเมื่อวันที่ 14 กุมภาพันธ์ ณ โรงเรียนพญาไท</a></li>
          <li><a href="docs/ร่วมโครงการ/โครงการ Be Internet Awesome/หลักสูตรBIAv2.pdf#toolbar=0">ดาวน์โหลดหลักสูตร Be Internet Awesome</a></li>
          <li><a href="https://docs.google.com/presentation/d/1dOuojTuWWBsBLjDt5oxWSAOJeNbr15dx088zkFwfJ50/edit#slide=id.g6efa38c6ab_0_4211">ดาวน์โหลดสไลด์การอบรม Be Internet Awesome</a></li>
          <li><a href="popBIA.php">ยื่นหลักฐานเพื่อรับเกียรติบัตรจากมูลนิธิ</a></li>
          <li><a href="docs/ร่วมโครงการ/โครงการ Be Internet Awesome/หนังสือ Be Internet Awesome ถึงสำนักงานเขตพื้นที่การศึกษา.pdf#toolbar=0">หนังสือจาก สพฐ. ถึงสำนักงานเขตพื้นที่การศึกษา เรื่องประชาสัมพันธ์โครงการ Be Internet Awesome</a></li>
          <li><a href="docs/ร่วมโครงการ/โครงการ Be Internet Awesome/googleสพฐ 18Mar2020.pdf#toolbar=0">ดาวน์โหลด Powerpoint รายการพุธเช้าข่าวสพฐ เมื่อวันที่ 18 มีนาคม 2563</a></li>
					<li><a href="http://bia.scurvethai.com/">ดาวน์โหลดสไลด์การสอน Be Internet Awesome  5 บท 26 กิจกรรม</a></li>
        </ul>
    </article>
    <div class="one_first">
      <div class="btnJoin">
        <a href="https://beinternetawesome.withgoogle.com/th_th"><h6>เข้าสู่เว็ปไซต์ Be Internet Awesome by Google</h6></a>
      </div>
    </div>
  </main>
</div>
<!-- End Content 01 - รายละเอียดโครงการBIA -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer -->
<div class="wrapper row7">
  <footer id="footer" class="hoc clear">
    <!-- ################################################################################################ -->
    <div class="one_half first">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Related Department</strong></p>
      <div class="one_first first center">
        <a class="font-x1 footercontact" href="https://beinternetawesome.withgoogle.com/th_th">
          <img src="images/มูลนิธิครูดีของแผ่นดิน BIn Footer Logo.png" alt="inwshop Logo" style="width:100px; height:100px;">
          <p>Be Internet Awesome</p>
        </a>
      </div>
      <div class="one_half center">
        
          
          
        </a>
      </div>
    </div>
    <div class="one_half">
      <p class="font-x3 center" style="color:rgb(2,83,165);"><strong style="line-height:50px;">Foundation of Thai Suprateacher</strong></p>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
            <a class="footercontact font-x1" href="https://www.google.co.th/maps/place/%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%99%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%B9%E0%B8%94%E0%B8%B5%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%81%E0%B8%9C%E0%B9%88%E0%B8%99%E0%B8%94%E0%B8%B4%E0%B8%99/@13.7635366,100.5017138,17z/data=!3m1!4b1!4m5!3m4!1s0x30e2996bce6fc8f1:0xa56b7630b40aad00!8m2!3d13.7635366!4d100.5039025?hl=th">103 ถนนวิสุทธิกษัตริย์ แขวงบางขุนพรหม
              เขตพระนคร กรุงเทพ 10200</a>
          </address>
        </li>
        <li><i class="fa fa-phone"></i><a class="footercontact font-x1" href="tel:02-001-1515"> +66 (02) 001 1515</a></li>
        <li><i class="fas fa-envelope"></i><a class="footercontact font-x1" href="mailto:thaisuprateacher@gmail.com"> thaisuprateacher@gmail.com</a></li>
      </ul>
      <ul class="faico clear">
        <li class="faicon-facebook"><a href="https://www.facebook.com/thaisuprateacher"><i class="fab fa-facebook-f"></i> <socialtext>Facebook</socialtext></a></li>
        <li class="faicon-line"><a href="http://line.me/ti/p/%40Thaisuprateacher"><i class="fab fa-line"></i> <socialtext>Line</socialtext></a></li>
        <li class="faicon-youtube"><a href="https://www.youtube.com/channel/UC1kI7ZF9mEqdo0kacYrt57g"><i class="fab fa-youtube"></i> <socialtext>Youtube</socialtext></a></li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- End Footer -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Copyright Tab -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align:center">Copyright &copy; 2020 - All Rights Reserved</p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- End Copyright Tab -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>