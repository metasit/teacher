<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$now_date = date("d-m-Y H:i:s");

	$ID = $_SESSION['ID']; // teacher ID
	$ID_stu = $_GET['ID_stu']; // student ID

	/* Set all value */
	$winScroll = $_GET['winScroll'];
	$basic_score4stu_id = $_GET['basic_score4stu_id'];

	$basic_score4stu_check_status = 'อนุมัติ,'.$now_date;

	/* Update status in basic_score4stu table */
	$sql = "UPDATE `basic_score4stu` SET `basic_score4stu_check_status`='$basic_score4stu_check_status' WHERE `basic_score4stu_id`='$basic_score4stu_id' ";
	$res= $con->query($sql) or die($con->error); //Check error

	/* Log User Action */
	$scorelog_task = 'ตรวจความดีเด็กพื้นฐาน,'.$_GET['basic_score4stu_system_id'].$_GET['basic_score4stu_file_no'];
	$scorelog_detail = 'ครู,ชั้น5,'.$basic_score4stu_check_status;
	$scorelog_total = $ID_stu; // ID of student who do Good

	$sqllog="INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail, scorelog_total) 
	VALUES ('$ID', '$scorelog_task', '$scorelog_detail', '$scorelog_total') ";
	$relog = $con->query($sqllog) or die($con->error); //Check error

	header('location: รายละเอียดนักเรียนทำความดี-ครู.php?winScroll='.$winScroll.'&ID_stu='.$ID_stu.'&system_id='.$_GET['basic_score4stu_system_id']);
?>