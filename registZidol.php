<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="โครงการเด็กดีของแผ่นดิน.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าโครงการเด็กดีของแผ่นดิน</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/เด็กดีของแผ่นดิน Logo.jpg" style="width:70%" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" id="Affform" action="addZidol.php?CFP=registZidol" method="POST" enctype="multipart/form-data">
					<!-- ################################################################################################ -->
					<?php
						$affil_code = $_SESSION['affil_code'];

						$sqlbasic_score_ans = "SELECT `basic_score_total`, `basic_score_text` FROM `login` WHERE ID='$ID' ";
						$reBSA = mysqli_query($con, $sqlbasic_score_ans);
						$rowBSA = mysqli_fetch_array($reBSA);

						$teacher_ID = $rowBSA['basic_score_total'];
						$teacher_name = $rowBSA['basic_score_text'];

						$sqlteacher = "SELECT * FROM `login` WHERE ID='$teacher_ID' AND affil_code='$affil_code' ";
						$reteacher = $con->query($sqlteacher);
						$rowteacheremail = mysqli_fetch_array($reteacher);

						$sqlstudent = "SELECT * FROM `login` WHERE `ID`='$ID' ";
						$restudent = $con->query($sqlstudent);
						$rowstudent = mysqli_fetch_array($restudent);

						/* Set system_id */
						$basic_score_ans = $rowstudent['basic_score_ans'];

						$basic_score4stu_system_id = substr($basic_score_ans, 0, 1);
					?>
					<label for="teacher_id" id="teacherhead" class="center m-t-10 bold">ชื่อคุณครูที่ปรึกษา (ถ้ามี)</label>
					<select name="teacher_ID" id="teacher_id" class="form-control" style="height:40px; color:rgb(150,150,150);" required>
						<option value="" disabled="disabled" selected="selected">เลือกครูที่ปรึกษา</option>
						<?php
							if($teacher_name != '') { ?>
								<option value="<?php echo $teacher_ID; ?>" selected="selected"><?php echo $teacher_name.' ('.$rowteacheremail['email'].')'; ?></option> <?php
							}

							$sqltealist = "SELECT * FROM `login` WHERE (occup_code LIKE 'OcAA%' AND affil_code='$affil_code')
							OR (occup_code LIKE 'OcAB%' AND affil_code='$affil_code') ";
							$retealist = $con->query($sqltealist);

							while($rowtealist = $retealist->fetch_assoc()) {
								$name = $rowtealist['firstname'].' '.$rowtealist['lastname']; ?>
								<option value="<?php echo $rowtealist['ID']; ?>"><?php echo $name.' ('.$rowtealist['email'].')'; ?></option> <?php
							}
						?>
					</select>
					<?php
						if($rowstudent['basic_score_status'] == 'ยืนยันแล้ว') { ?>
							<div class="container-login100-form-btn">
								<button class="btn2">อัพเดทชื่อครูที่ปรึกษา</a>
							</div> <?php
						}
					?>
				</form>
				<!-- ################################################################################################ -->
				<div class="container-login100-form-btn m-t-20">
					<a href="แนบหลักฐานประกอบการพิจารณา-เด็ก.php" target="_blank" class="btn2" style="margin:0 5px">แนบหลักฐานประกอบการพิจารณา</a>
					<a href="แนบไฟล์ทำความดี-เด็ก.php" target="_blank" class="btn2" style="margin:0 5px">แนบไฟล์ทำความดี</a>
				</div>
				<?php
					if($basic_score4stu_system_id == 'A') {
						// NOTE: Check ว่ามีการแนบไฟล์ต้นกล้าแห่งความดีมารึยัง ถ้าใช่ ให้ทำการขึ้น Content ตาม basic_score_status
						$sqlcheck_A = "SELECT `ID` FROM `basic_score4stu` WHERE ID='$ID' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
						$recheck_A = mysqli_query($con, $sqlcheck_A);

						if(mysqli_num_rows($recheck_A) != 0) {

							if(isset($rowstudent['basic_score_total'])) { // ถ้าเด็กมีครูที่ปรึกษา

								if($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p><strong>สถานะการตรวจ:</strong> เก่งมากเลยค่ะ ^^ ผ่านโครงการเด็กดีของแผ่นดินขั้นพื้นฐานแล้วนะคะ</p>
									</div> <?php
								
								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว') { // กรณีเด็กทำการยืนยันชื่อ-นามสกุล ตำแหน่ง และสังกัดตัวเองเรียบร้อยแล้ว ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p>เนื่องจากคุณส่งไฟล์ต้นกล้าแห่งความดีเรียบร้อยแล้ว กดปุ่มด้านล่างเพื่อให้ครูที่ปรึกษาตรวจสอบเพื่อรับเกียรติบัตรขั้นพื้นฐานค่ะ</p>
										<a href="cf_basic_score-เด็ก.php" class="btn2" style="margin:0 5px">กดเพื่อให้ครูที่ปรึกษาตรวจสอบ</a>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/กำลังตรวจสอบ' || $rowstudent['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p><strong>สถานะการตรวจ:</strong> กำลังตรวจสอบ</p>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ') { ?>
									<div class="login100-form-title lh-2-0">
										<p>หลังจากได้ทำการแก้ไขเรียบร้อยแล้วให้ทำการส่งเพื่อให้ตรวจสอบอีกครั้ง</p>
										<a href="cf_basic_score-เด็ก.php?basic_score_status=ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ&basic_score_remark=<?php echo $rowstudent['basic_score_remark'] ?>" class="btn2" style="margin:0 5px">กดเพื่อให้ตรวจสอบอีกครั้ง</a>
										<p style="color: rgb(231, 59, 59)"><strong>สถานะการตรวจ:</strong> ปฏิเสธ เนื่องจาก<?php echo $rowstudent['basic_score_remark'] ?></p>
									</div> <?php
								}

							}else{ // ถ้าเด็กไม่มีครูที่ปรึกษา

								if($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p><strong>สถานะการตรวจ:</strong> เก่งมากเลยค่ะ ^^ ผ่านโครงการเด็กดีของแผ่นดินขั้นพื้นฐานแล้วนะคะ</p>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p>เนื่องจากคุณส่งไฟล์ต้นกล้าแห่งความดีเรียบร้อยแล้ว กดปุ่มด้านล่างเพื่อให้เจ้าหน้าที่มูลนิธืตรวจสอบเพื่อรับเกียรติบัตรขั้นพื้นฐานค่ะ</p>
										<a href="cf_basic_score-เด็ก.php" class="btn2" style="margin:0 5px">กดเพื่อให้เจ้าหน้าที่ตรวจสอบ</a>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/กำลังตรวจสอบ' || $rowstudent['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p><strong>สถานะการตรวจ:</strong> กำลังตรวจสอบ</p>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ') { ?>
									<div class="login100-form-title lh-2-0">
										<p>หลังจากได้ทำการแก้ไขเรียบร้อยแล้วให้ทำการส่งเพื่อให้ตรวจสอบอีกครั้ง</p>
										<a href="cf_basic_score-เด็ก.php?basic_score_status=ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ&basic_score_remark=<?php echo $rowstudent['basic_score_remark'] ?>" class="btn2" style="margin:0 5px">กดเพื่อให้ตรวจสอบอีกครั้ง</a>
										<p style="color: rgb(231, 59, 59)"><strong>สถานะการตรวจ:</strong> ปฏิเสธ เนื่องจาก<?php echo $rowstudent['basic_score_remark'] ?></p>
									</div> <?php
								}
							}
						}



					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


					}elseif($basic_score4stu_system_id == 'B') {
						// NOTE: Check ว่ามีการแนบไฟล์ทำความดีมา14ไฟล์รึยัง ถ้าใช่ ให้ทำการขึ้น Content ตาม basic_score_status
						$sqlcountrow = "SELECT count(*) AS countrow FROM `basic_score4stu` WHERE ID='$ID' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
						$recountrow = mysqli_query($con,$sqlcountrow);
						$rowcountrow = mysqli_fetch_array($recountrow);

						if($rowcountrow['countrow'] >= 14) {

							if(isset($rowstudent['basic_score_total'])) { // ถ้าเด็กมีครูที่ปรึกษา

								if($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p><strong>สถานะการตรวจ:</strong> เก่งมากเลยค่ะ ^^ ผ่านโครงการเด็กดีของแผ่นดินขั้นพื้นฐานแล้วนะคะ</p>
									</div> <?php
								
								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว') { // กรณีเด็กทำการยืนยันชื่อ-นามสกุล ตำแหน่ง และสังกัดตัวเองเรียบร้อยแล้ว ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p>เนื่องจากคุณส่งไฟล์ทำความดีครบ 14 ไฟล์ กดปุ่มด้านล่างเพื่อให้ครูที่ปรึกษาตรวจสอบเพื่อรับเกียรติบัตรขั้นพื้นฐานค่ะ</p>
										<a href="cf_basic_score-เด็ก.php" class="btn2" style="margin:0 5px">กดเพื่อให้ครูที่ปรึกษาตรวจสอบ</a>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/กำลังตรวจสอบ' || $rowstudent['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p><strong>สถานะการตรวจ:</strong> กำลังตรวจสอบ</p>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ') { ?>
									<div class="login100-form-title lh-2-0">
										<p>หลังจากได้ทำการแก้ไขเรียบร้อยแล้วให้ทำการส่งเพื่อให้ตรวจสอบอีกครั้ง</p>
										<a href="cf_basic_score-เด็ก.php?basic_score_status=ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ&basic_score_remark=<?php echo $rowstudent['basic_score_remark'] ?>" class="btn2" style="margin:0 5px">กดเพื่อให้ตรวจสอบอีกครั้ง</a>
										<p style="color: rgb(231, 59, 59)"><strong>สถานะการตรวจ:</strong> ปฏิเสธ เนื่องจาก<?php echo $rowstudent['basic_score_remark'] ?></p>
									</div> <?php
								}

							}else{ // ถ้าเด็กไม่มีครูที่ปรึกษา

								if($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p><strong>สถานะการตรวจ:</strong> เก่งมากเลยค่ะ ^^ ผ่านโครงการเด็กดีของแผ่นดินขั้นพื้นฐานแล้วนะคะ</p>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p>เนื่องจากคุณส่งไฟล์ทำความดีครบ 14 ไฟล์ กดปุ่มด้านล่างเพื่อให้เจ้าหน้าที่มูลนิธืตรวจสอบเพื่อรับเกียรติบัตรขั้นพื้นฐานค่ะ</p>
										<a href="cf_basic_score-เด็ก.php" class="btn2" style="margin:0 5px">กดเพื่อให้เจ้าหน้าที่ตรวจสอบ</a>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/กำลังตรวจสอบ' || $rowstudent['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p><strong>สถานะการตรวจ:</strong> กำลังตรวจสอบ</p>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ') { ?>
									<div class="login100-form-title lh-2-0">
										<p>หลังจากได้ทำการแก้ไขเรียบร้อยแล้วให้ทำการส่งเพื่อให้ตรวจสอบอีกครั้ง</p>
										<a href="cf_basic_score-เด็ก.php?basic_score_status=ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ&basic_score_remark=<?php echo $rowstudent['basic_score_remark'] ?>" class="btn2" style="margin:0 5px">กดเพื่อให้ตรวจสอบอีกครั้ง</a>
										<p style="color: rgb(231, 59, 59)"><strong>สถานะการตรวจ:</strong> ปฏิเสธ เนื่องจาก<?php echo $rowstudent['basic_score_remark'] ?></p>
									</div> <?php
								}
							}
						}

					}
					
				?>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>