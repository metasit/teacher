<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		/* Set name */
		$ID_stu = $_POST['ID_stu'];

		$sqlstu = "SELECT * FROM `login` WHERE ID='$ID_stu' ";
		$restu = mysqli_query($con, $sqlstu);
		$rowstu = mysqli_fetch_array($restu);

		$name = $rowstu['firstname'].' '.$rowstu['lastname'];
		/* Set $basic_score4stu2_code */
		$basic_score4stu2_code = $_GET['basic_score4stu2_code'];

		$target_dir = 'images/ไฟล์เด็กขั้นพื้นฐาน/'.$name.'/'; // Set target_directory
		
		if(!is_dir($target_dir)) { // if there's not folder in target_directory
			mkdir($target_dir); // Create folder name is today_date
		}

		$target_file = $target_dir.date('Y-m-d').' '.$basic_score4stu2_code.' '.basename($_FILES["basic_score4stu2_data"]["name"]); // Save image in the target folder

		if(move_uploaded_file($_FILES["basic_score4stu2_data"]["tmp_name"], $target_file)) {

			$scorelog_task = 'แนบหลักฐานประกอบแทนนักเรียน';
			$scorelog_detail = 'ครู,ชั้น5,เด็ก,ขั้นพื้นฐาน,'.$basic_score4stu2_code.','.$target_file;
			$scorelog_total = $ID_stu;
			$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail, scorelog_total) VALUES ('$ID', '$scorelog_task', '$scorelog_detail', '$scorelog_total') ";
			$re = $con->query($sql) or die($con->error); //Check error

			$sqlcountrow = "SELECT count(*) AS countrow FROM `basic_score4stu2` WHERE ID='$ID_stu' AND basic_score4stu2_code='$basic_score4stu2_code' ";
			$recountrow = mysqli_query($con,$sqlcountrow);
			$rowcountrow = mysqli_fetch_array($recountrow);
			
			if($rowcountrow['countrow'] != 0) {
				$sql = "UPDATE `basic_score4stu2` SET basic_score4stu2_data='$target_file' WHERE ID='$ID_stu' AND basic_score4stu2_code='$basic_score4stu2_code' ";
				$re = $con->query($sql) or die($con->error); //Check error
			}else{
				$basic_score4stu2_remark = 'IDครูที่ปรึกษา:'.$ID;

				$sql = "INSERT INTO `basic_score4stu2` (ID, basic_score4stu2_code, basic_score4stu2_data, basic_score4stu2_remark) VALUES ('$ID_stu', '$basic_score4stu2_code', '$target_file', '$basic_score4stu2_remark') ";
				$re = $con->query($sql) or die($con->error); //Check error
			}
		}
		echo '<script>';
			echo "alert('แนบไฟล์เรียบร้อยค่ะ');";
			echo "window.location.replace('แนบหลักฐานประกอบการพิจารณาแทนนักเรียน-ครู.php?ID_stu=".$ID_stu."')";
		echo '</script>';

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
	
?>