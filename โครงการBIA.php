<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];
  // มอบเกียรติบัตรBIA_step1.php
?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการBIA-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="all_project.php"> ร่วมโครงการ</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="โครงการBIA.php"> โครงการ Be Internet Awesome</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - รายละเอียดโครงการBIA -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <article style="text-align:center; padding-bottom:50px;">
      <p class="font-x3"><span style="color:rgb(180,147,31); line-height:80px"><strong>ทำไมต้องเรียนรู้ Be internet awesome (by google) </strong></p>
      <img src="images/BInAw Poster02.png" alt="BInAw Poster02">
      <p class="fs-32" style="font-family:RSUText; line-height:30pt; text-align:left;">
        เป็นที่ทราบกันดีว่าในยุคเทคโนโลยีปัจจุบัน เราต่างเริ่มตระหนักถึงปัญหาของเด็กไทยกับพิษภัยจากอินเตอร์เน็ตมากขึ้น ซึ่งเปรียบเสมือนดาบสองคมที่มีทั้งคุณอนันต์และโทษมหันต์ในเวลาเดียวกัน การใช้อินเตอร์เน็ต
        นั้นก่อประโยชน์มหาศาล แต่ก็แอบแฝงไปด้วยอันตรายต่างๆ มากมาย ไม่ว่าจะเป็นเรื่องการเข้าถึงข้อมูลที่ไม่เหมาะสมของเด็ก ค่านิยมผิดๆ ในเรื่องเพศ การล่อลวงในวงสนทนา นอกจากนี้ยังมีเว็บไซต์ที่เต็มไปด้วย
        เรื่องลามก และเรื่องความรุนแรงต่างๆ มากมาย สื่อสีดำและสีเทามีมากมายในเกมส์โทรศัพท์มือถือที่อยู่ใกล้บุตรหลานเราเสียยิ่งกว่าพ่อแม่ ซึ่งยากต่อการควบคุมตรวจสอบได้ อาชญากรรมทางอินเตอร์เน็ตเหล่านี้เป็นภัย
        ที่อันตรายมากพอที่จะกำหนดเส้นทางอนาคตของเด็กได้เลยทีเดียว 
        <br><br>
        ดังนั้น ก่อนที่เด็กไทย โดยเฉพาะบุตรหลานของเราจะตกเป็นเหยื่อของภัยอินเตอร์เน็ตไปมากกกว่านี้ พ่อแม่ หรือครูอาจารย์ที่อยู่ใกล้ชิด จำเป็นอย่างยิ่งที่จะต้องทำความเข้าใจภัยอันตรายต่างๆ อันเนื่องมาจากการ
        เปลี่ยนแปลงที่เกิดขึ้นอย่างรวดเร็วในโลกดิจิตอล เพื่อที่จะรู้เท่าทัน และปรับตัวเองให้รู้จักตั้งรับกับภัยเงียบที่คอยคุกคามอย่างชาญฉลาด ให้พ่อแม่ ครูอาจารย์ ได้ตระหนักถึงความสำคัญในฐานะผู้ปกป้อง คุ้มครอง 
        ให้เด็กและเยาวชนไม่หลงเดินทางผิด คอยประคับประคองให้เดินออกจากปัญหา และพาไปยังหนทางในการดำเนินชีวิตที่ถูกต้องดีงามได้
        <br><br>
        <strong>Be Internet Awesome</strong> จะช่วยสอนให้เด็กๆ ได้รู้จักกับพื้นฐานสำคัญของการเป็นพลเมืองยุคดิจิทัลและความปลอดภัย เพื่อช่วยให้เด็กๆ เป็นนักสำรวจที่ปลอดภัยและมั่นใจในโลกออนไลน์ เป็นทั้งเครื่องมือและ
        แหล่งข้อมูล ให้ท่องอินเตอร์เน็ตอย่างที่เรียกว่า <strong>“เล่นอย่างปลอดภัย เรียนอย่างปลอดภัย และใช้อย่างปลอดภัย”</strong>
        <br><br>
        <strong>พื้นฐานสำคัญ</strong> หลักจรรยาบรรณสูงสุดของยอดนักท่องอินเตอร์เน็ต
        <br>
        <strong>1. Be Internet Smart คิดก่อนแชร์</strong>
        ข่าวต่างๆ (ทั้งดีและไม่ดี) แพร่กระจายในโลกอินเทอร์เน็ตได้อย่างรวดเร็ว และหากไม่คิดให้รอบคอบ เด็กๆ อาจตกอยู่ในสถานการณ์ที่ยากลำบาก ซึ่งอาจมีผลกระทบในระยะยาว ดูวิธีแชร์ข้อมูลกับคนที่รู้จักและไม่รู้จักได้เลย
        <br><br>
        <strong><u>สื่อสารอย่างมีความรับผิดชอบ</u></strong>
        <ul class="fs-32" style="font-family:RSUText; line-height:30pt; text-align:left;">
          <li>ส่งเสริมการคิดก่อนแชร์โดยใช้หลักคิดง่ายๆ ว่าการสื่อสารออนไลน์ก็เหมือนกับการสื่อสารแบบตัวต่อตัว ถ้าไม่ใช่เรื่องที่น่าพูด ก็แปลว่าไม่ใช่เรื่องที่น่าโพสต์</li>
          <li>สร้างคู่มือเกี่ยวกับประเภทการสื่อสารที่ เหมาะสม (และไม่เหมาะสม)</li>
          <li>เก็บรายละเอียดเกี่ยวกับครอบครัวและเพื่อนๆ ไว้เป็นความลับ</li>
        </ul>
        <img src="images/มูลนิธิครูดีของแผ่นดิน GoogleSmart.jpg" alt="GoogleSmart">
      </p>
      <p class="fs-32" style="font-family:RSUText; line-height:30pt; text-align:left;">
        <br>
        <strong>2. Be Internet Alert  ไม่ตกหลุมพรางกลลวง</strong>
        คุณต้องช่วยให้เด็กๆ เข้าใจว่าผู้คนและสิ่งต่างๆ ที่เกิดขึ้นในโลกออนไลน์อาจไม่ได้เป็นอย่างที่เห็น ความสามารถในการแยกแยะระหว่างข้อมูลจริงและเท็จเป็นบทเรียนที่สำคัญอย่างยิ่งต่อความปลอดภัยในโลกออนไลน์
        <br>
        <img src="images/มูลนิธิครูดีของแผ่นดิน GoogleAlert.jpg" alt="GoogleAlert">
        <br><br>
        <strong>3. Be Internet Strong เก็บข้อมูลไว้เป็นความลับ</strong>
        ความเป็นส่วนตัวและความปลอดภัยส่วนบุคคลในโลกออนไลน์และออฟไลน์นั้นก็สำคัญไม่แพ้กัน การปกป้องข้อมูลที่สำคัญช่วยให้เด็กๆ หลีกเลี่ยงความเสียหายต่ออุปกรณ์ ชื่อเสียง และความสัมพันธ์ได้
        <br>
        <img src="images/มูลนิธิครูดีของแผ่นดิน GoogleStrong.jpg" alt="GoogleStrong">
        <br><br>
        <strong>4.	Be Internet Kind เป็นคนดีเท่จะตาย</strong>
        อินเทอร์เน็ตเป็นกระบอกเสียงอันทรงพลังในการเผยแพร่เรื่องที่ดีและไม่ดี ช่วยให้เด็กๆ เลือกทางที่ถูกต้องโดยใช้แนวคิด “จงปฏิบัติต่อผู้อื่นเหมือนที่อยากให้ผู้อื่นปฏิบัติต่อตัวเรา” กับการทำสิ่งต่างๆ ในโลกออนไลน์ 
        ซึ่งจะช่วยให้ได้ผลลัพธ์ที่ดีสำหรับผู้อื่นและกำจัดพฤติกรรมการกลั่นแกล้งให้หมดไป
        <br>
        <img src="images/มูลนิธิครูดีของแผ่นดิน GoogleKind.jpg" alt="GoogleKind">
        <br><br>
        <strong>5.	Be Internet Brave สงสัยเมื่อไร ก็ถามได้เลย</strong>
        บทเรียนหนึ่งที่ควรจะต้องสอนเด็กๆ ไม่ว่าจะพบสิ่งใดในโลกออนไลน์ก็คือ เมื่อใดก็ตามที่มีคำถาม พวกเขาควรพูดคุยกับผู้ใหญ่ที่ไว้วางใจได้อย่างสบายใจ ซึ่งผู้ใหญ่สนับสนุนพฤติกรรมนี้ได้ด้วยการส่งเสริม
        การพูดคุยแบบเปิดเผยทั้งที่บ้านและในชั้นเรียน
        <br>
        <img src="images/มูลนิธิครูดีของแผ่นดิน GoogleBrave.jpg" alt="GoogleBrave">
      </p>
      <!-- ################################################################################################ -->
      <!-- Start VDOs Section -->
      <section class="m-t-80 line2-t-green1">
        <p class="font-x3" style="color:rgb(180,147,31); line-height:50px">
          <strong>กูเกิลเปิดตัว Be Internet Awesome บทเรียนออนไลน์<br>สอนใช้อินเทอร์เน็ตอย่างปลอดภัย</strong>
        </p>
        <iframe width="640px" height="360px" src="https://www.youtube.com/embed/SJtfNNEb_s0"></iframe>
        <p class="font-x3" style="color:rgb(180,147,31); line-height:80px">
          <strong>รายการพุธเช้า ข่าวสพฐ. ตอน BeInternetAwesome วันที่ 18 มี.ค.63</strong>
        </p>
        <iframe width="640px" height="360px" src="https://www.youtube.com/embed/wL3mYFuIk3k"></iframe>
      </section>
      <!-- End VDOs Section -->
      <!-- ################################################################################################ -->
      <ul class="fs-32 textlink m-t-50 p-t-50 line2-t-green1" style="font-family:RSUText; line-height:30pt; text-align:left; list-style-type:none;">
				<li><a href="docs/ร่วมโครงการ/โครงการ Be Internet Awesome/google.pdf#toolbar=0">ดาวน์โหลดภาพรวมการดำเนินโครงการ สำหรับโรงเรียนที่มาสัมมนาเมื่อวันที่ 14 กุมภาพันธ์ ณ โรงเรียนพญาไท</a></li>
				<li><a href="docs/ร่วมโครงการ/โครงการ Be Internet Awesome/หลักสูตรBIAv2.pdf#toolbar=0">ดาวน์โหลดหลักสูตร Be Internet Awesome</a></li>
				<li><a href="https://docs.google.com/presentation/d/1dOuojTuWWBsBLjDt5oxWSAOJeNbr15dx088zkFwfJ50/edit#slide=id.g6efa38c6ab_0_4211">ดาวน์โหลดสไลด์การอบรม Be Internet Awesome</a></li>
				<!-- <li><a href="popBIA.php">ยื่นหลักฐานเพื่อรับเกียรติบัตรจากมูลนิธิ</a></li> -->
				<li><a href="docs/ร่วมโครงการ/โครงการ Be Internet Awesome/หนังสือ Be Internet Awesome ถึงสำนักงานเขตพื้นที่การศึกษา.pdf#toolbar=0">หนังสือจาก สพฐ. ถึงสำนักงานเขตพื้นที่การศึกษา เรื่องประชาสัมพันธ์โครงการ Be Internet Awesome</a></li>
				<li><a href="docs/ร่วมโครงการ/โครงการ Be Internet Awesome/googleสพฐ 18Mar2020.pdf#toolbar=0">ดาวน์โหลด Powerpoint รายการพุธเช้าข่าวสพฐ เมื่อวันที่ 18 มีนาคม 2563</a></li>
				<li><a href="http://bia.scurvethai.com/">ดาวน์โหลดสไลด์การสอน Be Internet Awesome  5 บท 26 กิจกรรม</a></li>
				<?php
					if(!isset($_SESSION['occup_code']) || !isset($_SESSION['affil_code']) || 
					$_SESSION['occup_code'] == NULL || $_SESSION['affil_code'] == NULL ||
					$_SESSION['occup_code'] == '' || $_SESSION['affil_code'] == '') { ?>
						<li><a href="สมัครเข้าร่วมโครงการ-ตำแหน่ง.php">สมัครเข้าร่วมโครงการ</a></li> <?php
					}
				?>
			</ul>
		</article>
		<!-- Start Print เกียรติบัตร BIA -->
		<?php
			$sqllogin = "SELECT * FROM `login` 
			WHERE (ID='$ID'AND occup_code LIKE 'OcAA%' AND basic_score_status='Approve แล้ว/ยืนยันแล้ว') 
			OR (ID='$ID'AND occup_code LIKE 'OcAA%' AND affil_code!='')
			OR (ID='$ID'AND occup_code LIKE 'OcAA%' AND affil_code IS NOT NULL)
			OR (ID='$ID' AND occup_code LIKE 'OcAB%' AND basic_score_status='Approve แล้ว/ยืนยันแล้ว')
			OR (ID='$ID'AND occup_code LIKE 'OcAB%' AND affil_code!='')
			OR (ID='$ID'AND occup_code LIKE 'OcAB%' AND affil_code IS NOT NULL) ";
			$relogin = mysqli_query($con, $sqllogin);

			if(mysqli_num_rows($relogin) != 0 || !isset($ID)) { ?>
				<div class="one_first">
					<div class="btnJoin">
						<a href="มอบเกียรติบัตรBIA_step1.php"><h6>รับเกียรติบัตร Be Internet Awesome</h6></a>
					</div>
				</div> <?php
			}
		?>
		<!-- End Print เกียรติบัตร BIA -->

    <div class="one_first m-t-50">
      <div class="btnJoin">
        <a href="https://beinternetawesome.withgoogle.com/th_th"><h6>เข้าสู่เว็ปไซต์ Be Internet Awesome by Google</h6></a>
      </div>
		</div>
		
  </main>
</div>
<!-- End Content 01 - รายละเอียดโครงการBIA -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>