<?php 
	session_start();
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if(strpos($_SESSION['permis_system'], '*all*') !== false || strpos($_SESSION['permis_system'], '*C00*') !== false || strpos($_SESSION['permis_system'], '*C01*') !== false) {
			// If admin level, can access this page
			require_once('condb.php');
			date_default_timezone_set("Asia/Bangkok");
		}else{
			header("Location: javascript:history.go(-1);");
		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['winScroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li class="linkst"><a href="#" onclick="return false">ระบบหลังบ้านเด็กพื้นฐาน-มีครูที่ปรึกษา</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ระบบหลังบ้านประเมินครู -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row" style="margin:0 20px; overflow-x:auto;">
			<div class="col-lg-12">
				<div class="table-main table-responsive" style="margin-bottom:200px">
					<!-- ################################################################################################ -->
					<table class="table" style="background-color:rgb(240,240,240);">
						<thead>
							<tr>
								<th>ลำดับ</th>
								<th>ชื่อนร.</th>
								<th>ชื่อครู</th>
								<th>วันที่ส่งเรื่อง</th>
								<th>สถานะ</th>
								<th>BIA</th>
								<th>คะแนนประเมิน</th>
								<th>เรียงความ/วีดีโอ</th>
								<th>อัพเดท</th>
								<th>หมายเหตุ</th>
							</tr>
						</thead>
						<tbody class="disable-margin">
							<?php
								/* Set number product per page */
								$results_per_page = 50;

								/* Set $page */
								if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };

								/* Set start number for each page */
								$start_from = ($page-1) * $results_per_page;

								/* Call login table data from mySQL */
								$sql = "SELECT * FROM `login` INNER JOIN `scorelog` ON login.ID=scorelog.scorelog_total 
								WHERE (login.occup_code LIKE 'OcF%' AND login.basic_score_status LIKE 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ%' AND scorelog.scorelog_task='ส่งให้มูลนิธิตรวจสอบ' AND scorelog.scorelog_detail='ครู,ชั้น5,เด็ก,พื้นฐาน') 
								OR (login.occup_code LIKE 'OcF%' AND login.basic_score_status='ยืนยันแล้ว/Approve แล้ว' AND scorelog.scorelog_task='ส่งให้มูลนิธิตรวจสอบ' AND scorelog.scorelog_detail='ครู,ชั้น5,เด็ก,พื้นฐาน') 
								ORDER BY scorelog.scorelog_date DESC LIMIT $start_from, $results_per_page ";
								$re = mysqli_query($con, $sql);

								$i = $start_from+1; // Get number of row
								while($row = $re->fetch_assoc()) {
									
									$ID_stu = $row['scorelog_total'];
									$basic_score4stu_system_id = substr($row['basic_score_ans'], 0, 1);

									$teacher_firstname = substr(trim($row['basic_score_text']), 0, strpos(trim($row['basic_score_text']), ' '));
									$teacher_lastname = substr(trim($row['basic_score_text']), strrpos(trim($row['basic_score_text']), ' ')+1);
									$teacher_name = $teacher_firstname.'<br>'.$teacher_lastname;

									$sqlbasic_score4stu2_codeA = "SELECT * FROM `basic_score4stu2` WHERE ID='$ID_stu' AND basic_score4stu2_code='A' ";
									$reA = mysqli_query($con, $sqlbasic_score4stu2_codeA);
									$rowA = mysqli_fetch_array($reA);

									$sqlbasic_score4stu2_codeB = "SELECT * FROM `basic_score4stu2` WHERE ID='$ID_stu' AND basic_score4stu2_code='B' ";
									$reB = mysqli_query($con, $sqlbasic_score4stu2_codeB);
									$rowB = mysqli_fetch_array($reB);
									
									$sqlbasic_score4stu2_codeC = "SELECT * FROM `basic_score4stu2` WHERE ID='$ID_stu' AND basic_score4stu2_code='C' ";
									$reC = mysqli_query($con, $sqlbasic_score4stu2_codeC);
									$rowC = mysqli_fetch_array($reC); ?>
									<tr>
										<!-- No. -->
										<td class="price-pr">
											<p><?php echo $i; ?></p>
										</td>
										<!-- Student Name -->
										<td class="name-pr lh-1-0">
											<p><?php echo $row['firstname'].'<br>'.$row["lastname"]; ?></p>
										</td>
										<!-- Teacher Name -->
										<td class="name-pr lh-1-0">
											<p><?php	echo $teacher_name; ?></p>
										</td>
										<!-- วันที่วันที่ส่งเรื่อง -->
										<td class="price-pr">
											<p><?php echo date("d-m-Y", strtotime($row['scorelog_date'])); ?></p>
										</td>
										<!-- Status -->
										<td class="name-pr">
											<?php
												if($row['basic_score_status']  == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ') { ?>
													<p style="color: rgb(180,147,31)">รอมูลนิธิตรวจสอบ</p> <?php
												}elseif($row['basic_score_status']  == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ') { ?>
													<p>ปฏิเสธ</p> <?php
												}elseif($row['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
													<p style="color: rgb(72,160,0)"><i class="fas fa-check-circle"></i><br>ผ่านแล้ว</p> <?php
												}elseif($row['basic_score_status'] == 'ยืนยันแล้ว/ไม่ผ่าน') { ?>
													<p>ไม่ผ่าน</p> <?php
												}
											?>
										</td>
										<!-- เกียรติบัตร BIA (Code A) -->
										<td class="slip-upload-btn">
											<?php
												if(mysqli_num_rows($reA) == 0) { ?>
													<a href="#" style="background-color:transparent; color:black; font-weight:700; border: none;" onclick="return false">รอแนบเอกสาร</a> <?php
												}else{ ?>
													<a href="<?php echo $rowA['basic_score4stu2_data'];?>" target="_blank">ดูเกียรติบัตร</a> <?php
												}
											?>
										</td>
										<!-- คะแนนประเมิน (Code B) -->
										<td class="name-pr lh-1-0">
											<?php
												if($basic_score4stu_system_id == 'A') { ?>
													<p style="color:rgb(206, 206, 206)">ระบบต้นกล้า</p> <?php
												}elseif($basic_score4stu_system_id == 'B') {
													if(mysqli_num_rows($reB) == 0) { ?>
														<p>รอทำประเมิน</p> <?php
													}else{
														if(substr($rowB['basic_score4stu2_data'], 0, strpos($rowB['basic_score4stu2_data'], ',')) == 'Pass')  {
															$basic_score4stu2_codeB_status = 'ผ่าน';
														}else{
															$basic_score4stu2_codeB_status = 'ไม่ผ่าน';
														}	?>
														<p><?php echo $basic_score4stu2_codeB_status; ?></p> <?php
													}
												}
											?>
										</td>
										<!-- เรียงความ/วีดีโอ การเปลี่ยนแปลงตัวเองหลังเข้าโครงการ (Code C) -->
										<td class="slip-upload-btn">
											<?php
												if(mysqli_num_rows($reC) == 0) { ?>
													<a href="#" style="background-color:transparent; color:black; font-weight:700; border: none;" onclick="return false">รอแนบเอกสาร</a> <?php
												}else{ ?>
													<a href="<?php echo $rowC['basic_score4stu2_data'];?>" target="_blank">ดูเรียงความ/คลิปวีดีโอ</a> <?php
												}
											?>
										</td>
										<!-- Update Status Button -->
										<td>
											<div class="up-stat-sty">
												<button class="up-stat-btn"><i class="fas fa-caret-down"></i></button>
												<div class="dropdown">
													<?php
														if(strpos($row['basic_score_status'], 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ') !== false) { ?>
															<a onclick="getWinScroll_changeStatusBtn_updatebasic_score4stu2_status('ปฏิเสธ', '<?php echo $ID_stu; ?>', '<?php echo $page; ?>')">ปฏิเสธ</a>
															<a onclick="getWinScroll_changeStatusBtn_updatebasic_score4stu2_status('Approve แล้ว', '<?php echo $ID_stu; ?>', '<?php echo $page; ?>')">Approve</a> <?php
														}elseif($row['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
															<a href="#" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">Approve แล้ว</a> <?php
														}
													?>
												</div>
											</div>
										</td>
										<!-- Remark -->
										<form action="addbasic_score4stu2_remark.php" method="POST">
											<td class="order_remark-sty">
												<textarea rows="1" cols="15" name="basic_score_remark" required><?php echo $row['basic_score_remark']; ?></textarea>
												<div class="order-detail-btn">
													<input type="hidden" name="ID" value=<?php echo $ID_stu; ?>>
													
													<input id="winScroll_input" type="hidden" name="winScroll">
													<input id="page" type="hidden" name="page" value="<?php echo $page; ?>">
													<input class="order_remark-submit" type="submit" id="basic_score_remark_submitbtn" value="บันทึก">

													<script>
														var winScroll_input = document.querySelectorAll("#winScroll_input");
														var basic_score_remark_submitbtn = document.querySelectorAll("#basic_score_remark_submitbtn");

														basic_score_remark_submitbtn[<?php echo $i-1; ?>].onmousemove = function() {
															var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
															winScroll_input[<?php echo $i-1; ?>].value = winScroll;
														}
													</script>
												</div>
											</td>
										</form>
									</tr> <?php
									$i++;
								}
							?>
						</tbody>
					</table>
					<!-- ################################################################################################ -->
				</div>
			</div>				
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ระบบหลังบ้านประเมินครู -->
<?php
	/* Calculate total pages with results */
	$sqlcount = "SELECT COUNT(login.ID) AS total FROM `login` INNER JOIN `scorelog` ON login.ID=scorelog.scorelog_total 
	WHERE (login.occup_code LIKE 'OcF%' AND login.basic_score_status LIKE 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ%' AND scorelog.scorelog_task='ส่งให้มูลนิธิตรวจสอบ' AND scorelog.scorelog_detail='ครู,ชั้น5,เด็ก,พื้นฐาน') 
	OR (login.occup_code LIKE 'OcF%' AND login.basic_score_status='ยืนยันแล้ว/Approve แล้ว' AND scorelog.scorelog_task='ส่งให้มูลนิธิตรวจสอบ' AND scorelog.scorelog_detail='ครู,ชั้น5,เด็ก,พื้นฐาน')" ;
	$resultcount = $con->query($sqlcount);
	$rowcount = $resultcount->fetch_assoc();
	$total_pages = ceil($rowcount["total"] / $results_per_page);
?>
<div class="wrapper row3 coloured" style="background-color:rgb(226,255,224);">
	<main class="hoc container clear">
		<div class="content">
			<nav class="pagination">
				<ul>
					<?php
						for ($j=1; $j<=$total_pages; $j++) {  // print links for all pages
							if ($j==$page)
							{
								echo "<li class='current'>";
							}else{
								echo "<li>";
							}
							echo "<a href='ระบบหลังบ้านเด็กพื้นฐาน-มีครูที่ปรึกษา.php?page=".$j."'";
							if ($j==$page)  echo " class='curPage'";
							echo ">".$j."</a> ";
						};
					?>
					</li>

				</ul>
			</nav>
		</div>
	</main>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/inewsticker.js"></script>
<script src="js/bootsnav.js."></script>
<script src="js/images-loded.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/custom.js"></script>

<!-- JS for getWinScroll -->
<script src="js/getWinScroll_changeStatusBtn.js"></script>

</body>
</html>