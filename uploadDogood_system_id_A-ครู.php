<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		$ID_stu = $_POST['ID_stu'];
		$basic_score4stu_system_id = 'A';
		$basic_score4stu_text = $_POST['basic_score4stu_text'];

		$sqllogin = "SELECT * FROM `login` WHERE ID='$ID_stu' ";
		$relogin = mysqli_query($con, $sqllogin);
		$rowlogin = mysqli_fetch_array($relogin);

		$name = $rowlogin['firstname'].' '.$rowlogin['lastname'];
		
		$basic_score4stu_file_date = $_POST['basic_score4stu_file_date'].' '.$_POST['basic_score4stu_file_time'];
		$createDate = new DateTime($basic_score4stu_file_date);
		$date_do4savefile = $createDate->format('Y-m-d');

		$sqlbasic_score4stu_system_id_A = "SELECT * FROM `basic_score4stu` WHERE ID='$ID_stu' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
		$reBS4SSA = mysqli_query($con, $sqlbasic_score4stu_system_id_A);
		$rowBS4SSA = mysqli_fetch_array($reBS4SSA);

		if(mysqli_num_rows($reBS4SSA) != 0) { // not the first time click to upload file
			$target_dir = 'images/ไฟล์เด็กขั้นพื้นฐาน/'.$name.'/'; // Set target_directory

			if(!is_dir($target_dir)) { // if there's not folder in target_directory
				mkdir($target_dir); // Create folder name is today_date
			}

			$target_file = $target_dir.$date_do4savefile.basename($_FILES["basic_score4stu_file"]["name"]); // Save image in the target folder

			if(move_uploaded_file($_FILES["basic_score4stu_file"]["tmp_name"], $target_file)) {

				$basic_score4stu_check_status = $rowBS4SSA['basic_score4stu_check_status'];

				if($basic_score4stu_check_status == NULL || is_null($basic_score4stu_check_status) || $basic_score4stu_check_status == '' || empty($basic_score4stu_check_status)) {

					$sql = "UPDATE `basic_score4stu` SET basic_score4stu_file='$target_file', basic_score4stu_file_date='$basic_score4stu_file_date' 
					WHERE ID='$ID_stu' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
					$re = $con->query($sql) or die($con->error); //Check error

					/* Log User Action */
					$scorelog_task = 'อัพเดทไฟล์,'.$basic_score4stu_system_id;
					$scorelog_detail = 'เด็ก,ขั้นพื้นฐาน,'.$target_file.','.$basic_score4stu_text;
					$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', '$scorelog_task', '$scorelog_detail') ";
					$re = $con->query($sql) or die($con->error); //Check error

					echo '<script>';
						echo "alert('อัพเดทไฟล์แล้วค่ะ');";
						echo "window.location.replace('แนบไฟล์ต้นกล้าแทนนักเรียน-ครู.php?ID_stu=$ID_stu&system_id=$basic_score4stu_system_id')";
					echo '</script>';

				}else{

					/* Set $basic_score4stu_check_status */
					if(strpos($basic_score4stu_check_status, 'ปฏิเสธ,') !== false) {
						$basic_score4stu_check_status = 'แก้ไขไฟล์,'.date('d-m-Y H:i:s');
					}elseif(strpos($basic_score4stu_check_status, 'แก้ไขไฟล์,') !== false || strpos($basic_score4stu_check_status, 'แก้ไขรายงาน,') !== false) {

						$file_index = 'แก้ไขไฟล์,'.date('d-m-Y H:i:s');

						if(strpos($basic_score4stu_check_status, 'แก้ไขรายงาน,') !== false) {
							$text_index = ','.substr($basic_score4stu_check_status, strpos($basic_score4stu_check_status, 'แก้ไขรายงาน,'), 53);
						}else{
							$text_index = '';
						}
						
						$basic_score4stu_check_status = $file_index.$text_index;
					}

					$sql = "UPDATE `basic_score4stu` SET basic_score4stu_file='$target_file', basic_score4stu_file_date='$basic_score4stu_file_date', basic_score4stu_check_status='$basic_score4stu_check_status' 
					WHERE ID='$ID_stu' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
					$re = $con->query($sql) or die($con->error); //Check error

					/* Log User Action */
					$scorelog_task = 'แก้ไขไฟล์,'.$basic_score4stu_system_id;
					$scorelog_detail = 'เด็ก,ขั้นพื้นฐาน,'.$target_file.','.$basic_score4stu_text;
					$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', '$scorelog_task', '$scorelog_detail') ";
					$re = $con->query($sql) or die($con->error); //Check error

					echo '<script>';
						echo "alert('แก้ไขไฟล์แล้วค่ะ');";
						echo "window.location.replace('แนบไฟล์ต้นกล้าแทนนักเรียน-ครู.php?ID_stu=$ID_stu&system_id=$basic_score4stu_system_id')";
					echo '</script>';

				}

			}else{
				echo '<script>';
					echo "alert('Something went wrong. Uploaded file wasn't succeeded.<br>Please contact developer  to take care of');";
					echo "<script>window.history.go(-1)</script>";
				echo '</script>';
			}

		}else{ // first time click to upload file
			$target_dir = 'images/ไฟล์เด็กขั้นพื้นฐาน/'.$name.'/'; // Set target_directory

			if(!is_dir($target_dir)) { // if there's not folder in target_directory
				mkdir($target_dir); // Create folder name is today_date
			}

			$target_file = $target_dir.$date_do4savefile.basename($_FILES["basic_score4stu_file"]["name"]); // Save image in the target folder

			if(move_uploaded_file($_FILES["basic_score4stu_file"]["tmp_name"], $target_file)) {
				$sql = "INSERT INTO `basic_score4stu` (ID, basic_score4stu_system_id, basic_score4stu_file, basic_score4stu_file_date, basic_score4stu_text) 
				VALUES ('$ID_stu', '$basic_score4stu_system_id', '$target_file', '$basic_score4stu_file_date', '$basic_score4stu_text') ";
				$re = $con->query($sql) or die($con->error); //Check error

				/* Log User Action */
				$scorelog_task = 'แนบไฟล์,'.$basic_score4stu_system_id;
				$scorelog_detail = 'เด็ก,ขั้นพื้นฐาน,'.$target_file.','.$basic_score4stu_text;
				$sql = "INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', '$scorelog_task', '$scorelog_detail') ";
				$re = $con->query($sql) or die($con->error); //Check error

				echo '<script>';
					echo "alert('แนบไฟล์แล้วค่ะ');";
					echo "window.location.replace('แนบไฟล์ต้นกล้าแทนนักเรียน-ครู.php?ID_stu=$ID_stu&system_id=$basic_score4stu_system_id')";
				echo '</script>';
				
			}else{
				echo '<script>';
					echo "alert('Something went wrong. Uploaded file wasn't succeeded.<br>Please contact developer  to take care of');";
					echo "<script>window.history.go(-1)</script>";
				echo '</script>';
			}

		}



	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
	
?>