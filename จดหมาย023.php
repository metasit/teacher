<?php

	session_start();
	require_once('condb.php');
	$email = $_SESSION['email'];

?>

<!DOCTYPE html>

<html lang="thai">

<head>
<title>มูลนิธิครูดีของแผ่นดิน</title>
<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<style>
p.big {
line-height: 30px;
}
</style>
</head>
  
  
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="จดหมาย023-en.php">English</a></li>
                  </ul>
<?php include('includes/headerBottom.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul >
    <li><a href="index.php">หน้าแรก</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="จดหมายรวมlatest.php">จดหมาย/ประกาศ</a></li>
    <i class="fas fa-angle-double-right"></i>
    <li><a href="จดหมาย023.php"> ประกาศรางวัลครูดีของแผ่นดินขั้นพื้นฐาน ปี 2563</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 01 - จดหมาย023 -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <article style="text-align: center" >
      <p class="font-x3"><span style="color: rgb(180,147,31); line-height: 80px;">
        <strong>ประกาศรางวัลครูดีของแผ่นดินขั้นพื้นฐาน ปี 2563</strong>
      </p>
      <ul class="font-x2plus textlink" style="font-family: RSUText; line-height: 30pt; text-align: left; list-style-type: none;">
        <li><a href="https://drive.google.com/drive/folders/1wrx-ff1M4_JeIwAEhsyiLSp3L1jI-YJj?usp=sharing" target="_blank">ประกาศรางวัลครูดีของแผ่นดินขั้นพื้นฐาน ปี 2563</a></li>
      </ul>
    </article>
  </main>
</div>
<!-- End Content 01 - จดหมาย023 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- JAVASCRIPTS -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script src="js/custom.js"></script>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
</body>
</html>