<?php
/* 
	input need: $affiliation_id && $affil_code
*/
if($affiliation_id == 'AfA') {
	$affiliation_name = 'สำนักงานปลัดกระทรวงศึกษาธิการ';
	$affsub_id = substr($affil_code,0,4);
	$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id' ";
	$reaffiliation = mysqli_query($con, $sqlaffiliation);
	$rowaffiliation = mysqli_fetch_array($reaffiliation);
	$affsub_name = $rowaffiliation['affsub_name'];

	$sqlaffsub24AfA = "SELECT `affsub2_name` FROM `affsub24AfA` WHERE affsub2_id='$affil_code' ";
	$reaffsub24AfA = mysqli_query($con, $sqlaffsub24AfA);
	$rowaffsub24AfA = mysqli_fetch_array($reaffsub24AfA);
	$affsub2_name = $rowaffsub24AfA['affsub2_name'];
}elseif($affiliation_id == 'AfB') {
	$affiliation_name = 'สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน';
	$affsub_id = substr($affil_code,0,4);
	$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id' ";
	$reaffiliation = mysqli_query($con, $sqlaffiliation);
	$rowaffiliation = mysqli_fetch_array($reaffiliation);
	$affsub_name = $rowaffiliation['affsub_name'];

	$affsub2_id = substr($affil_code,0,5);
	$sqlaffsub24AfB = "SELECT `affsub2_name` FROM `affsub24AfB` WHERE affsub2_id='$affsub2_id' ";
	$reaffsub24AfB = mysqli_query($con, $sqlaffsub24AfB);
	$rowaffsub24AfB = mysqli_fetch_array($reaffsub24AfB);
	$affsub2_name = $rowaffsub24AfB['affsub2_name'];

	if($affsub_id == 'AfBA') {
		$affsub3_id = substr($affil_code,0,8);
		$sqlaffsub34AfBA = "SELECT `affsub3_name` FROM `affsub34AfBA` WHERE affsub3_id='$affsub3_id' ";
		$reaffsub34AfBA = mysqli_query($con, $sqlaffsub34AfBA);
		$rowaffsub34AfBA = mysqli_fetch_array($reaffsub34AfBA);
		$affsub3_name = $rowaffsub34AfBA['affsub3_name'];
		
		if($affsub2_id == 'AfBAA') {
			$affsub4_id = substr($affil_code,0,13);
			$sqlaffsub44AfBAA = "SELECT `affsub4_name` FROM `affsub44AfBAA` WHERE affsub4_id='$affsub4_id' ";
			$reaffsub44AfBAA = mysqli_query($con, $sqlaffsub44AfBAA);
			$rowaffsub44AfBAA = mysqli_fetch_array($reaffsub44AfBAA);
			$affsub4_name = $rowaffsub44AfBAA['affsub4_name'];
		}elseif($affsub2_id == 'AfBAB') {
			$affsub4_id = substr($affil_code,0,13);
			$sqlaffsub44AfBAB = "SELECT `affsub4_name` FROM `affsub44AfBAB` WHERE affsub4_id='$affsub4_id' ";
			$reaffsub44AfBAB = mysqli_query($con, $sqlaffsub44AfBAB);
			$rowaffsub44AfBAB = mysqli_fetch_array($reaffsub44AfBAB);
			$affsub4_name = $rowaffsub44AfBAB['affsub4_name'];
		}
		
	}elseif($affsub_id == 'AfBB') {
		$affsub3_id = substr($affil_code,0,10);
		$sqlaffsub34AfBB = "SELECT `affsub3_name` FROM `affsub34AfBB` WHERE affsub3_id='$affsub3_id' ";
		$reaffsub34AfBB = mysqli_query($con, $sqlaffsub34AfBB);
		$rowaffsub34AfBB = mysqli_fetch_array($reaffsub34AfBB);
		$affsub3_name = $rowaffsub34AfBB['affsub3_name'];
	}
}elseif($affiliation_id == 'AfC') {
	$affiliation_name = 'สำนักงานคณะกรรมการส่งเสริมการศึกษาเอกชน';
	$affsub_id = substr($affil_code,0,5);
	$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id' ";
	$reaffiliation = mysqli_query($con, $sqlaffiliation);
	$rowaffiliation = mysqli_fetch_array($reaffiliation);
	$affsub_name = $rowaffiliation['affsub_name'];

	$affsub2_id = substr($affil_code,0,8);
	$sqlaffsub24AfC = "SELECT `affsub2_name` FROM `affsub24AfC` WHERE affsub2_id='$affsub2_id' ";
	$reaffsub24AfC = mysqli_query($con, $sqlaffsub24AfC);
	$rowaffsub24AfC = mysqli_fetch_array($reaffsub24AfC);
	$affsub2_name = $rowaffsub24AfC['affsub2_name'];

	$affsub3_id = substr($affil_code,0,13);
	$sqlaffsub34AfC = "SELECT `affsub3_name` FROM `affsub34AfC` WHERE affsub3_id='$affsub3_id' ";
	$reaffsub34AfC = mysqli_query($con, $sqlaffsub34AfC);
	$rowaffsub34AfC = mysqli_fetch_array($reaffsub34AfC);
	$affsub3_name = $rowaffsub34AfC['affsub3_name'];
}elseif($affiliation_id == 'AfD') {
	$affiliation_name = 'สำนักงานส่งเสริมการศึกษานอกระบบและการศึกษาตามอัธยาศัย';
	$affsub_id = substr($affil_code,0,5);
	$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id' ";
	$reaffiliation = mysqli_query($con, $sqlaffiliation);
	$rowaffiliation = mysqli_fetch_array($reaffiliation);
	$affsub_name = $rowaffiliation['affsub_name'];

	$affsub2_id = substr($affil_code,0,9);
	$sqlaffsub24AfD = "SELECT `affsub2_name` FROM `affsub24AfD` WHERE affsub2_id='$affsub2_id' ";
	$reaffsub24AfD = mysqli_query($con, $sqlaffsub24AfD);
	$rowaffsub24AfD = mysqli_fetch_array($reaffsub24AfD);
	$affsub2_name = $rowaffsub24AfD['affsub2_name'];

	$affsub3_id = substr($affil_code,0,13);
	$sqlaffsub34AfD = "SELECT `affsub3_name` FROM `affsub34AfD` WHERE affsub3_id='$affsub3_id' ";
	$reaffsub34AfD = mysqli_query($con, $sqlaffsub34AfD);
	$rowaffsub34AfD = mysqli_fetch_array($reaffsub34AfD);
	$affsub3_name = $rowaffsub34AfD['affsub3_name'];

	$affsub4_ans_id = $affil_remark;
}elseif($affiliation_id == 'AfE') {
	$affiliation_name = 'สำนักงานคณะกรรมการการอาชีวศึกษา';
	$affsub_id = substr($affil_code,0,5);
	$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id' ";
	$reaffiliation = mysqli_query($con, $sqlaffiliation);
	$rowaffiliation = mysqli_fetch_array($reaffiliation);
	$affsub_name = $rowaffiliation['affsub_name'];

	$affsub2_id = substr($affil_code,0,6);
	$sqlaffsub24AfE = "SELECT `affsub2_name` FROM `affsub24AfE` WHERE affsub2_id='$affsub2_id' ";
	$reaffsub24AfE = mysqli_query($con, $sqlaffsub24AfE);
	$rowaffsub24AfE = mysqli_fetch_array($reaffsub24AfE);
	$affsub2_name = $rowaffsub24AfE['affsub2_name'];

	$affsub3_id = substr($affil_code,0,10);
	$sqlaffsub34AfE = "SELECT `affsub3_name` FROM `affsub34AfE` WHERE affsub3_id='$affsub3_id' ";
	$reaffsub34AfE = mysqli_query($con, $sqlaffsub34AfE);
	$rowaffsub34AfE = mysqli_fetch_array($reaffsub34AfE);
	$affsub3_name = $rowaffsub34AfE['affsub3_name'];
}elseif($affiliation_id == 'AfF') {
	$affiliation_name = 'สำนักงานคณะกรรมการการอุดมศึกษา';
	$affsub_id = substr($affil_code,0,4);
	$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id' ";
	$reaffiliation = mysqli_query($con, $sqlaffiliation);
	$rowaffiliation = mysqli_fetch_array($reaffiliation);
	$affsub_name = $rowaffiliation['affsub_name'];

	$affsub2_id = substr($affil_code,0,7);
	$sqlaffsub24AfF = "SELECT `affsub2_name` FROM `affsub24AfF` WHERE affsub2_id='$affsub2_id' ";
	$reaffsub24AfF = mysqli_query($con, $sqlaffsub24AfF);
	$rowaffsub24AfF = mysqli_fetch_array($reaffsub24AfF);
	$affsub2_name = $rowaffsub24AfF['affsub2_name'];
}elseif($affiliation_id == 'AfG') {
	$affiliation_name = 'กระทรวงมหาดไทย กรมส่งเสริมการปกครองส่วนท้องถิ่น';
	$affsub_id = substr($affil_code,0,5);
	$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id' ";
	$reaffiliation = mysqli_query($con, $sqlaffiliation);
	$rowaffiliation = mysqli_fetch_array($reaffiliation);
	$affsub_name = $rowaffiliation['affsub_name'];

	$affsub2_id = substr($affil_code,0,8);
	$sqlaffsub24AfG = "SELECT `affsub2_name` FROM `affsub24AfG` WHERE affsub2_id='$affsub2_id' ";
	$reaffsub24AfG = mysqli_query($con, $sqlaffsub24AfG);
	$rowaffsub24AfG = mysqli_fetch_array($reaffsub24AfG);
	$affsub2_name = $rowaffsub24AfG['affsub2_name'];

	$affsub3_id = substr($affil_code,0,11);
	$sqlaffsub34AfG = "SELECT `affsub3_name` FROM `affsub34AfG` WHERE affsub3_id='$affsub3_id' ";
	$reaffsub34AfG = mysqli_query($con, $sqlaffsub34AfG);
	$rowaffsub34AfG = mysqli_fetch_array($reaffsub34AfG);
	$affsub3_name = $rowaffsub34AfG['affsub3_name'];

	$affsub4_id = substr($affil_code,0,15);
	$sqlaffsub44AfG = "SELECT `affsub4_name` FROM `affsub44AfG` WHERE affsub4_id='$affsub4_id' ";
	$reaffsub44AfG = mysqli_query($con, $sqlaffsub44AfG);
	$rowaffsub44AfG = mysqli_fetch_array($reaffsub44AfG);
	$affsub4_name = $rowaffsub44AfG['affsub4_name'];
}elseif($affiliation_id == 'AfH') {
	$affiliation_name = 'กรุงเทพมหานคร';
	$affsub_id = substr($affil_code,0,4);
	$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id' ";
	$reaffiliation = mysqli_query($con, $sqlaffiliation);
	$rowaffiliation = mysqli_fetch_array($reaffiliation);
	$affsub_name = $rowaffiliation['affsub_name'];

	if($affsub_id == 'AfHA') {
		$affsub2_id = 'AfHAA';
		$affsub2_name = 'สำนักงานเขต';

		$affsub3_id = substr($affil_code,0,8);
		$sqlaffsub34AfHAA = "SELECT `affsub3_name` FROM `affsub34AfHAA` WHERE affsub3_id='$affsub3_id' ";
		$reaffsub34AfHAA = mysqli_query($con, $sqlaffsub34AfHAA);
		$rowaffsub34AfHAA = mysqli_fetch_array($reaffsub34AfHAA);
		$affsub3_name = $rowaffsub34AfHAA['affsub3_name'];

		$affsub4_id = substr($affil_code,0,11);
		$sqlaffsub44AfHAA = "SELECT `affsub4_name` FROM `affsub44AfHAA` WHERE affsub4_id='$affsub4_id' ";
		$reaffsub44AfHAA = mysqli_query($con, $sqlaffsub44AfHAA);
		$rowaffsub44AfHAA = mysqli_fetch_array($reaffsub44AfHAA);
		$affsub4_name = $rowaffsub44AfHAA['affsub4_name'];
	}
}elseif($affiliation_id == 'AfI') {
	$affiliation_name = 'เมืองพัทยา';
	$affsub_id = substr($affil_code,0,4);
	$sqlaffiliation = "SELECT `affsub_name` FROM `affiliation` WHERE affsub_id='$affsub_id' ";
	$reaffiliation = mysqli_query($con, $sqlaffiliation);
	$rowaffiliation = mysqli_fetch_array($reaffiliation);
	$affsub_name = $rowaffiliation['affsub_name'];
}elseif($affiliation_id == 'AfJ') {
	$affiliation_name = 'สำนักงานตำรวจแห่งชาติ';
	$affsub4_ans_id = $affil_remark;
}else{
	$affiliation_name = 'อื่นๆ';
	$affsub4_ans_id = $affil_remark;
}

?>