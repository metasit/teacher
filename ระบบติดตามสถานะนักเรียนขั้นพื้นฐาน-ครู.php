<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		date_default_timezone_set("Asia/Bangkok");
		$affil_code = $_SESSION['affil_code'];

		$sqltea = "SELECT * FROM `login` WHERE ID='$ID' ";
		$retea = mysqli_query($con, $sqltea);
		$rowtea = mysqli_fetch_array($retea);

		$fifth_score_status = $rowtea['fifth_score_status'];
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
	<style>.fs-1 {font-size: 1px;}</style>
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['win_scroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear">
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="all_project.php"> ร่วมโครงการ</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการครูดีของแผ่นดิน.php"> โครงการครูดีของแผ่นดิน</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการครูดีของแผ่นดินชั้นที่5-lobby.php"> ครูดีของแผ่นดินชั้นที่ 5</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="#" onclick="return false"> ระบบติดตามสถานะนักเรียนขั้นพื้นฐาน</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ระบบติดตามสถานะนักเรียนขั้นพื้นฐาน -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<!-- Start table of my product detail -->
		<div class="row" style="margin: 0 20px; overflow-x:auto;">
			<?php
				if($fifth_score_status == '') { ?>

					<div class="col-lg-11 right">
						<a href="สมัครต้นกล้าแทนนักเรียน_step1-ครู.php" class="btn"><i class="fas fa-plus"></i> สมัครระบบต้นกล้าแห่งความดีแทนนักเรียน</a>
					</div> <?php
					
				}
			?>
			<div class="col-lg-12 m-t-40">
				<div class="table-main table-responsive" style="margin-bottom:200px">
					<!-- ################################################################################################ -->
					<table class="table" style="background-color:rgb(240,240,240);">
						<thead>
							<tr>
								<th>ลำดับ</th>
								<th>ชื่อนักเรียน</th>
								<th>วันที่เริ่ม</th>
								<th>เวลาที่เหลือ</th>
								<th>วันที่ผ่าน</th>
								<th>สถานะ</th>
								<th>ระบบ</th>
								<th>รายละเอียด</th>
								<th class="BG-gold1">เกียรติบัตร</th>
							</tr>
						</thead>
						<tbody class="disable-margin">
							<?php
								$sqllogin = "SELECT * FROM `login` WHERE occup_code LIKE 'OcF%' AND affil_code='$affil_code' AND basic_score_total='$ID' AND ID!='$ID'  ";
								$relogin = mysqli_query($con, $sqllogin);
								
								if(mysqli_num_rows($relogin) == 0) { ?>

									</tbody>
								</table>
									<div class="no-order">
										<p>ยังไม่พบรายชื่อนักเรียน</p>
									</div> <?php

								}else{

									$i=1;
									while($rowlogin = $relogin->fetch_assoc()) {

										$ID_stu = $rowlogin['ID'];

										$sqladscorelog = "SELECT * FROM `adscorelog` WHERE ID_user='$ID_stu' AND adscorelog_task='Approve แล้ว' AND adscorelog_detail='เด็ก,ขั้นพื้นฐาน' ORDER BY adscorelog_date LIMIT 1 ";
										$readscorelog = mysqli_query($con, $sqladscorelog);
										$rowadscorelog = mysqli_fetch_array($readscorelog); ?>

										<tr>
											<!-- No. -->
											<td class="price-pr">
												<p><?php echo $i; ?></p>
											</td>
											<!-- Student Name -->
											<td class="price-pr">
												<p class="bold"><?php echo $rowlogin['firstname'].' '.$rowlogin['lastname']; ?></p>
												<p style="font-size: 15px"><?php echo '('.$rowlogin['email'].')'; ?></p>
											</td>
											<!-- วันที่เริ่ม -->
											<td class="price-pr">
												<?php
													if($rowlogin['basic_score_status'] != 'ยืนยันแล้ว/Approve แล้ว') { ?>
														<p><?php echo date("d-m-Y", strtotime($rowlogin['basic_score_date'])); ?></p> <?php
													}else{ ?>
														<p>-</p> <?php
													}
												?>
											</td>
											<!-- เวลาที่เหลือ -->
											<td class="price-pr">
												<?php
													if($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
														<p>-</p> <?php
													}else{
														/* Find and Set the latest basic_score_date */
														$basic_score_date = new DateTime($rowlogin['basic_score_date']);
														$basic_score_date->settime(0,0); // No need time to check
														$basic_score_date->format('Y-m-d');
														/* Set the expire_basic_score_date */
														$expire_basic_score_date = $basic_score_date->add(new DateInterval('P30D')); // Find expire date for basic_score
														$date_today = new DateTime('today');
														/* หาว่าเหลือเวลาอีกกี่วัน */
														$date_remain = date_diff($date_today, $expire_basic_score_date)->d;

														if($date_remain >= 0) { ?>
															<p><?php echo $date_remain; ?></p> <?php
														}else{ ?>
															<p><?php echo 0; ?></p> <?php
														}
													}
												?>
											</td>
											<!-- วันที่ผ่าน -->
											<td class="price-pr">
												<?php
													if($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
														<p><?php echo date("d-m-Y", strtotime($rowlogin['basic_score_date'])); ?></p> <?php
													}else{ ?>
														<p>-</p> <?php
													}
												?>
											</td>
											<!-- Status -->
											<td class="name-pr">
												<?php
													if($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
														<p style="color:rgb(72,160,0);"><i class="fas fa-check-circle"></i><br><?php echo ' ผ่านแล้ว' ?></p> <?php

													}elseif($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/กำลังตรวจสอบ') { ?>
														<p><?php echo 'รอตรวจ'; ?></p> <?php

													}elseif($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ') { ?>
														<p><?php echo 'รอมูลนิธิตรวจสอบ'; ?></p> <?php

													}elseif($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ') { ?>
														<p><?php echo 'โดนปฏิเสธ'; ?></p> <?php
													
													}elseif($rowlogin['basic_score_status'] == 'ยืนยันแล้ว') { ?>
														<p><?php echo 'กำลังดำเนินการ'; ?></p> <?php

													}else{ ?>
														<p><?php echo 'ไม่ผ่าน'; ?></p> <?php
													}
												?>
											</td>
											<!-- System -->
											<?php
												$system_id = substr($rowlogin['basic_score_ans'],0,1);
												if($system_id == 'A') {
													$system_name = 'ระบบต้นกล้าแห่งความดี';
												}elseif($system_id == 'B') {
													$system_name = 'ระบบทฤษฎี 21 วัน<br>(เนื่องจากโควิด 19 เหลือ 14 ครั้ง)';
												}

											?>
											<td class="price-pr">
												<p><?php echo $system_name; ?></p>
											</td>
											<!-- รายละเอียดนักเรียนทำความดีเป็นรายบุคคล -->
											<td class="print-btn">
												<form action="รายละเอียดนักเรียนทำความดี-ครู.php" method="POST" target="_blank">

													<input type="hidden" name="ID_stu" value="<?php echo $rowlogin['ID'];?>">
													<input type="hidden" name="system_id" value="<?php echo $system_id;?>">
													<button class="tooltip3"><i class="fas fa-clipboard-list"><span class="tooltiptext3">ดูรายละเอียด</span></i></button>
												</form>
											</td>
											<!-- พิมพ์เกียรติบัตร -->
											<?php
												if($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
													<td class="print-btn">
														<form action="printbasic_score4stu_cer-ครู.php" method="POST" target="_blank">
															
															<input type="hidden" name="ID_stu" value="<?php echo $rowlogin['ID'];?>">
															<input type="hidden" name="system_id" value="<?php echo $system_id;?>">
															<input type="hidden" name="CFP" value="ระบบติดตามสถานะนักเรียนขั้นพื้นฐาน-ครู">
															<button class="tooltip3"><i class="fas fa-certificate"><span class="tooltiptext3">พิมพ์เกียรติบัตร</span></i></button>
														</form>
													</td> <?php
												}else{ ?>
													<td class="print-btn">
														<p>-</p>
													</td> <?php
												} ?>
										</tr> <?php
										$i++;
									} ?>
						</tbody>
					</table> <?php
								}
							?>
					<!-- ################################################################################################ -->
				</div>
			</div>
		</div>
		<!-- End table of my product detail -->
		<!-- ################################################################################################ -->
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ระบบติดตามสถานะนักเรียนขั้นพื้นฐาน -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<!-- JS for getWinScroll -->
<script src="js/getWinScroll_changeStatusBtn.js"></script>

</body>
</html>