<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {

		if($_GET['CFP'] == 'registZidol_step4') {
			date_default_timezone_set("Asia/Bangkok");
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* If system=A, need to check student age not exceed 9 years old */
			$birthdate = new DateTime($_SESSION['birthdate']);
			$datetoday = new DateTime(date('Y-m-d'));
			$birthdate->settime(0,0); // No need time to check
			$birthdate->format('Y-m-d');
			
			$stu_age = date_diff($birthdate, $datetoday)->y;

			if($_POST['system'] == 'A') {
				if($stu_age >= 9) {
					/* เรียก basic_score_remark2 จาก login เพื่อดูว่าเป็นเด็กพิเศษหรือไม่ */
					$sqlbasic_score_remark2 = "SELECT basic_score_remark2 FROM `login` WHERE ID='$ID' AND basic_score_remark2 LIKE 'A%' ";
					$rebasic_score_remark2 = $con->query($sqlbasic_score_remark2);

					if(mysqli_num_rows($rebasic_score_remark2) == 1) { // เคสที่เป็นเด็กพิเศษ หรือ basic_score_remark2 มี code A อยู่ ให้เข้า loop นี้ คือ ไม่ต้องส่งกลับไปหน้า registZidol_step4.php
						
					}else{ // เคสทั่วไปก็ให้เข้า loop นี้ คือ ห้ามอายุเกิน9ขวบ
						echo '<script>';
							echo "alert('อายุเกิน 9 ปี ไม่สามารถเลือกระบบต้นกล้าแห่งความดีได้ค่ะ');";
							echo "window.location.replace('registZidol_step4.php')";
						echo '</script>';
					}

					
				}elseif($stu_age < 3) { // ถ้าอายุไม่ถึง 3 ปี ให้เข้า loop นี้ คือ alert และส่งกลับไปหน้า สมัครต้นกล้าแทนนักเรียน_step1-ครู.php
					echo '<script>';
						echo "alert('ต้องมีอายุอย่างน้อย 3 ปี ถึงจะสามารถเข้าร่วมโครงการเด็กดีของแผ่นดินขั้นพื้นฐานได้ค่ะ');";
						echo "window.location.replace('registZidol_step4.php')";
					echo '</script>';
				}
			}
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			$datetime=date("Y-m-d H:i:s");
			$basic_score_ans = $_POST['system'].'*'.$_POST['devmain'].'*'.$_POST['devsub'];
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if(isset($_POST['teacher_ID'])) {
				$teacher_ID = $_POST['teacher_ID'];

				$sqlteacher_name = "SELECT firstname, lastname FROM `login` WHERE ID='$teacher_ID' ";
				$reteacher_name = $con->query($sqlteacher_name);
				$rowteacher_name = $reteacher_name->fetch_assoc();
	
				$teacher_name = $rowteacher_name['firstname'].' '.$rowteacher_name['lastname'];
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				$sql = "UPDATE `login` SET basic_score_status='ยืนยันแล้ว', basic_score_total='$teacher_ID', basic_score_text='$teacher_name', basic_score_ans='$basic_score_ans', basic_score_date='$datetime' WHERE ID='$ID' ";
				$re = $con->query($sql) or die($con->error); //Check error
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/* Log User Action */
				$sqllog="INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', 'สมัครเข้าร่วมโครงการ', 'เด็ก,พื้นฐาน,$teacher_ID,$basic_score_ans') ";
				$relog = $con->query($sqllog) or die($con->error); //Check error
			}else{
				$sql = "UPDATE `login` SET basic_score_status='ยืนยันแล้ว', basic_score_ans='$basic_score_ans', basic_score_date='$datetime' WHERE ID='$ID' ";
				$re = $con->query($sql) or die($con->error); //Check error
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/* Log User Action */
				$sqllog="INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', 'สมัครเข้าร่วมโครงการ', 'เด็ก,พื้นฐาน,No Teacher,$basic_score_ans') ";
				$relog = $con->query($sqllog) or die($con->error); //Check error
			}
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			echo '<script>';
				echo "alert('สมัครเข้าร่วมโครงการเรียบร้อยค่ะ');";
				echo "window.location.replace('registZidol.php')";
			echo '</script>';
			
		}elseif($_GET['CFP'] == 'registZidol') {
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			$teacher_ID = $_POST['teacher_ID'];

			$sqlteacher_name = "SELECT firstname, lastname FROM `login` WHERE ID='$teacher_ID' ";
			$reteacher_name = $con->query($sqlteacher_name);
			$rowteacher_name = $reteacher_name->fetch_assoc();

			$teacher_name = $rowteacher_name['firstname'].' '.$rowteacher_name['lastname'];
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			$sql = "UPDATE `login` SET basic_score_total='$teacher_ID', basic_score_text='$teacher_name' WHERE ID='$ID' ";
			$re = $con->query($sql) or die($con->error); //Check error
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/* Log User Action */
			$sqllog="INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail) VALUES ('$ID', 'อัพเดทครูที่ปรึกษา', 'เด็ก,พื้นฐาน,$teacher_ID') ";
			$relog = $con->query($sqllog) or die($con->error); //Check error
			header('location: registZidol.php');
			
		}
	
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>