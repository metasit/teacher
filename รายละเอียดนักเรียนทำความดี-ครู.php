<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		date_default_timezone_set("Asia/Bangkok");

		if(isset($_POST['ID_stu'])) {
			$ID_stu = $_POST['ID_stu'];
		}else{
			$ID_stu = $_GET['ID_stu'];
		}
		
		if(isset($_POST['system_id'])) {
			$basic_score4stu_system_id = $_POST['system_id'];
		}else{
			$basic_score4stu_system_id = $_GET['system_id'];
		}
		
	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">

	
	<style>
		/* The Modal (background) */
		.modal {
			display: none; /* Hidden by default */
			position: fixed; /* Stay in place */
			z-index: 5; /* Sit on top */
			padding-top: 250px; /* Location of the box */
			left: 0;
			top: 0;
			width: 100%; /* Full width */
			height: 100%; /* Full height */
			overflow: auto; /* Enable scroll if needed */
			background-color: rgb(0,0,0); /* Fallback color */
			background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
			
		}
		/* Modal Content */
		.modal-content {
			background-color: white;
			margin: auto;
			padding: 20px;
			border: 10px solid rgb(94,177,26);
			border-radius: 10px;
			width: 50%;
		}
		/* The Close Button */
		.close, #close4devtopic {
			color: #aaaaaa;
			float: right;
			font-size: 28px;
			font-weight: bold;
		}
		.close:hover, .close:focus, #close4devtopic:hover, #close4devtopic:focus {
			color: #000;
			text-decoration: none;
			cursor: pointer;
		}
		.center_text_input > input {
			margin: 0 auto;
			margin-left: auto;
			margin-right: auto;
		}
		/***********************************************************************************************************/
		/* Update devtopic Style */
		.cart-box-main .up-stat-sty4devtopic .up-stat-btn4devtopic {
			width: 100%;
			text-align: center;
			padding-right: 0;
			border: none;
			cursor: pointer;
		}
		.cart-box-main .up-stat-sty4devtopic .up-stat-btn4devtopic .fa-caret-down {
			margin-left: auto;
		}
		.cart-box-main .up-stat-sty4devtopic .dropdown_thispage4devtopic {
			display: none;
			position: absolute;
			z-index: 4;
		}
		.cart-box-main .up-stat-sty4devtopic:hover .dropdown_thispage4devtopic {
			display: block;
		}
		.cart-box-main .up-stat-sty4devtopic:hover .dropdown_thispage4devtopic a {
			background-color: rgba(72,160,0,0.9);
			color: white;
			display: block;
			text-align: center;
			font-size: 18px;
			padding: 15px 45px; /* Difference from Update Status Button Style here */
			margin-left: -53px; /* Difference from Update Status Button Style here */
			cursor: pointer;
			border-bottom: 2px solid rgb(2,83,165);
		}
		.cart-box-main .up-stat-sty4devtopic:hover .dropdown_thispage4devtopic .reject-btn-sty4devtopic:hover {
			background-color: rgb(185, 0, 0);
		}
		.cart-box-main .up-stat-sty4devtopic:hover .dropdown_thispage4devtopic .approve-btn-sty4devtopic:hover {
			background-color: rgb(2,83,165);
		}
		/***********************************************************************************************************/
		/* Update Status Button Style */
		.cart-box-main .up-stat-sty .up-stat-btn {
			width: 100%;
			text-align: center;
			padding-right: 0;
			border: none;
			cursor: pointer;
		}
		.cart-box-main .up-stat-sty .up-stat-btn .fa-caret-down {
			margin-left: auto;
		}
		.cart-box-main .up-stat-sty .dropdown_thispage {
			display: none;
			position: absolute;
			z-index: 4;
		}
		.cart-box-main .up-stat-sty:hover .dropdown_thispage {
			display: block;
		}
		.cart-box-main .up-stat-sty:hover .dropdown_thispage a {
			background-color: rgba(72,160,0,0.9);
			color: white;
			display: block;
			text-align: initial;
			font-size: 18px;
			padding: 15px 45px;
			margin-left: -10px;
			cursor: pointer;
			border-bottom: 2px solid rgb(2,83,165);
		}
		.cart-box-main .up-stat-sty:hover .dropdown_thispage .reject-btn-sty:hover {
			background-color: rgb(185, 0, 0);
		}
		.cart-box-main .up-stat-sty:hover .dropdown_thispage .approve-btn-sty:hover {
			background-color: rgb(2,83,165);
		}
	</style>
</head>

<body id="top" onLoad="window.scroll(0, <?php echo $_GET['winScroll']; ?>)">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="margin-for-fixedMenu"></div>

<!-- Start breadcrumb -->
<div id="breadcrumb" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <ul>
		<li><a href="index.php">หน้าแรก</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="all_project.php"> ร่วมโครงการ</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการครูดีของแผ่นดิน.php"> โครงการครูดีของแผ่นดิน</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="โครงการครูดีของแผ่นดินชั้นที่5-lobby.php"> โครงการครูดีของแผ่นดินชั้นที่ 5</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="ระบบติดตามสถานะนักเรียนขั้นพื้นฐาน-ครู.php"> ระบบติดตามสถานะนักเรียนขั้นพื้นฐาน</a></li>
		<i class="fas fa-angle-double-right"></i>
		<li><a href="#" onclick="return false"> รายละเอียดนักเรียนทำความดี</a></li>
  </ul>
  <!-- ################################################################################################ -->
</div>
<!-- End breadcrumb -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 -->
<div class="wrapper row3">
	<!-- ################################################################################################ -->
	<!-- Start Cart  -->
	<div class="cart-box-main">
		<div style="max-width: 80%; margin: 30px auto 130px;">
			<!-- Start table of my product detail -->
			<div class="row">
				<div class="col-lg-12">
					<div class="table-main table-responsive">
						<!-- ################################################################################################ -->
						<?php
							$sqllogin = "SELECT * FROM `login` WHERE `ID`='$ID_stu' ";
							$relogin = mysqli_query($con, $sqllogin);
							$rowlogin = mysqli_fetch_array($relogin);

							$sqlbasic_score4stu_system_id_C = "SELECT * FROM `basic_score4stu` WHERE ID='$ID_stu' AND basic_score4stu_system_id='C' ORDER BY basic_score4stu_id DESC LIMIT 1  ";
							$reBS4SC = mysqli_query($con, $sqlbasic_score4stu_system_id_C);
							$rowBS4SC = mysqli_fetch_array($reBS4SC);

							// Set to show อนุมัติ and ปฏิเสธ status
							$basic_score4stu_check_status = $rowBS4SC['basic_score4stu_check_status'];

						?>
						<section class="text-gold center" style="line-height:80px;">
							<h8>ตารางแสดงและติดตามสถานะการทำความดี โครงการเด็กดีของแผ่นดินขั้นพื้นฐาน</h8>
						</section>
						<div class="fs-20 inline lh-1-7">
							<div class="bold">ชื่อนักเรียน:</div><?php echo ' '.$rowlogin['firstname'].' '.$rowlogin['lastname'].' ('.$rowlogin['email'].')'; ?>
							<br>
							<div class="bold">สิ่งที่ต้องการพัฒนาตนเอง</div>
							<!-- ################################################################################################ -->
							<br>
							<?php
								/* Set devmain_name & devmain_sub to show on page */
								$basic_score_ans = $rowlogin['basic_score_ans'];
								// Set devmain_name
								$devmain_id = substr($basic_score_ans, 2, 1);
								if($devmain_id == 'A') {
									$devmain_name = 'การรู้จัก/จัดการตนเอง';
								}elseif($devmain_id == 'B') {
									$devmain_name = 'การเข้าใจผู้อื่น';
								}elseif($devmain_id == 'C') {
									$devmain_name = 'การเข้าสังคม';
								}elseif($devmain_id == 'D') {
									$devmain_name = 'การสื่อสารและการสร้างแรงบันดาลใจ';
								}
								// Set devsub_name
								$devsub_name = substr($rowlogin['basic_score_ans'], 4);

								// Set $new_devsub (ถ้ามี)
								$new_devsub_index = substr($basic_score4stu_check_status, strpos($basic_score4stu_check_status, ',')+1);
								$new_devsub = substr($new_devsub_index, 0, strpos($new_devsub_index, ','));

								$new_devsub_date = substr($basic_score4stu_check_status, strrpos($basic_score4stu_check_status, ',')+1);

							?>
							<div class="bold">หัวข้อหลัก:</div><?php echo ' '.$devmain_name.' '; ?>
							<br>
							<div class="bold">หัวข้อย่อย:</div><?php echo ' '.$devsub_name.' '; ?>

							<?php
								if($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/กำลังตรวจสอบ') { ?>
									<!-- Start Update btn for devmain & devsub -->
									(ตรวจหัวข้อการพัฒนาตรงนี้
									<div class="up-stat-sty4devtopic">
										<button class="up-stat-btn4devtopic" style="padding: 0 10px"><i class="fas fa-caret-down"></i></button>
										<div class="dropdown_thispage4devtopic">
											<!-- ################################################################################################ -->
											<!-- Trigger/Open The Modal -->
											<a id="rejectBtn4devtopic" class="reject-btn-sty4devtopic">ปฏิเสธ</a>
											<!-- ################################################################################################ -->
											<?php
												if(strpos($basic_score4stu_check_status, 'อนุมัติ,') !== false) { ?>
													<a class="approve-btn-sty4devtopic" onclick="return false" style="cursor:no-drop; background-color:rgb(2,83,165)">อนุมัติ</a> <?php
												}else{ ?>
													<a onclick="approveBtn4devtopic('<?php echo $ID_stu; ?>', '<?php echo $new_devsub; ?>', '<?php echo $basic_score_ans; ?>', '<?php echo $basic_score4stu_system_id; ?>') "
														class="approve-btn-sty4devtopic">อนุมัติ
													</a> <?php
												}
											?>
											<!-- ################################################################################################ -->
										</div>
									</div>
									) <?php
								}

								if(strpos($basic_score4stu_check_status, 'ปฏิเสธ,') !== false) { ?>
									<div class="text-red1"> <?php
										echo str_replace(',', ' ', $basic_score4stu_check_status); ?>
									</div> <?php
								}elseif(strpos($basic_score4stu_check_status, 'อนุมัติ,') !== false) { ?>
									<div class="text-green1"> <?php
										echo str_replace(',', ' ', $basic_score4stu_check_status); ?> <i class="fas fa-check-circle"></i>
									</div> <?php
								}elseif(strpos($basic_score4stu_check_status, 'แก้ไขไฟล์,') !== false) { ?>
									<div class="text-yellow2"> <?php
										echo 'นร.ได้ทำการแก้ไขเป็น "'.$new_devsub.'" เมื่อ '.$new_devsub_date; ?>
									</div> <?php
								}
							?>

							<!-- End Update btn for devsub -->

							<?php
								/* Update basic_score_status in login table */

								if($basic_score4stu_system_id == 'A') {
									// NOTE: นับว่า มีการอนุมัติไป2ไฟล์แล้วหรือไม่ (ไฟล์ต้นกล้า1ไฟล์ และสถานะการตรวจ devsub) ถ้าใช่ ให้ขึ้นปุ่ม ส่งให้มูลนิธิ ตรวจสอบ
									$sqlcount1 = "SELECT `ID` FROM `basic_score4stu` WHERE ID='$ID_stu' ";
									$recount1 = mysqli_query($con, $sqlcount1);

									if(mysqli_num_rows($recount1) == 1) {
										if($rowlogin['basic_score_status'] == 'ยืนยันแล้ว') { ?>
											<div class="row BG-gray2 center">
												<div class="col-md-12" style="font-family: TextRSU">
													<p>
														เนื่องจากคุณได้ทำการแนบไฟล์ต้นกล้าแห่งความดี คุณสามารถกดปุ่มนี้เพื่อให้เจ้าหน้าที่มูลนิธิตรวจสอบ
														<br>
														(โปรดตรวจทานอย่างละเอียด หลังจากที่ท่านกดปุ่มนี้ ท่านจะไม่สามารถแก้ไขข้อมูลอะไรได้อีก จะมีผลต่อการได้รับเกียรติบัตรอย่างถาวร)
													</p>
													<a href="sendcheckbasic_score4stu.php?ID_stu=<?php echo $ID_stu; ?>&basic_score4stu_system_id=<?php echo $basic_score4stu_system_id; ?>" class="btn m-b-20">ส่งให้มูลนิธิตรวจสอบ</a>
												</div>
											</div> <?php
										
										}elseif($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ') { ?>
											<div class="row BG-gray2 center">
												<div class="col-md-12">
													<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="มูลนิธิครูดีของแผ่นดิน PureLogo">
													<p><strong>สถานะเจ้าหน้าที่มูลนิธิ:</strong> รอมูลนิธิตรวจสอบ</p>
												</div>
											</div> <?php

										}elseif($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ') { ?>
											<div class="row BG-gray2 center">
												<div class="col-md-12">
													<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="มูลนิธิครูดีของแผ่นดิน PureLogo">
													<p class="text-red1"><strong>สถานะเจ้าหน้าที่มูลนิธิ:</strong> ปฏิเสธ เนื่องจาก<?php echo $rowlogin['basic_score_remark']; ?></p>
												</div>
											</div> <?php

										}elseif($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
											<div class="row BG-gray2 center">
												<div class="col-md-12">
													<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="มูลนิธิครูดีของแผ่นดิน PureLogo">
													<p class="text-green1"><strong>สถานะเจ้าหน้าที่มูลนิธิ:</strong> ผ่านแล้ว <i class="fas fa-check-circle"></i></p>
												</div>
											</div> <?php
										}
									}


								/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


								}elseif($basic_score4stu_system_id == 'B') {
									// NOTE: นับว่า มีการอนุมัติไป15ไฟล์แล้วหรือไม่ (ไฟล์ความดี14ไฟล์ และสถานะการตรวจ devsub) ถ้าใช่ ให้ขึ้นปุ่ม ส่งให้มูลนิธิ ตรวจสอบ
									$sqlcount15 = "SELECT `ID` FROM `basic_score4stu` WHERE ID='$ID_stu' AND basic_score4stu_check_status LIKE 'อนุมัติ%' ";
									$recount15 = mysqli_query($con, $sqlcount15);

									if(mysqli_num_rows($recount15) == 15) {
										if($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/กำลังตรวจสอบ') { ?>
											<div class="row BG-gray2 center">
												<div class="col-md-12" style="font-family: TextRSU">
													<p>
														เนื่องจากคุณทำการอนุมัติครบทั้งหัวข้อการพัฒนา และรายการทำความดีทั้ง 14 รายการ คุณสามารถกดปุ่มนี้เพื่อให้เจ้าหน้าที่มูลนิธิตรวจสอบ
														<br>
														(โปรดตรวจทานอย่างละเอียด หลังจากที่ท่านกดปุ่มนี้ ท่านจะไม่สามารถแจ้งนักเรียนให้แก้ไขข้อมูลอะไรได้อีก จะมีผลต่อการได้รับเกียรติบัตรอย่างถาวร)
													</p>
													<a href="sendcheckbasic_score4stu.php?ID_stu=<?php echo $ID_stu; ?>&basic_score4stu_system_id=<?php echo $basic_score4stu_system_id; ?>" class="btn m-b-20">ส่งให้มูลนิธิตรวจสอบ</a>
												</div>
											</div> <?php
										
										}elseif($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ') { ?>
											<div class="row BG-gray2 center">
												<div class="col-md-12">
													<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="มูลนิธิครูดีของแผ่นดิน PureLogo">
													<p><strong>สถานะเจ้าหน้าที่มูลนิธิ:</strong> รอมูลนิธิตรวจสอบ</p>
												</div>
											</div> <?php

										}elseif($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ') { ?>
											<div class="row BG-gray2 center">
												<div class="col-md-12">
													<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="มูลนิธิครูดีของแผ่นดิน PureLogo">
													<p class="text-red1"><strong>สถานะเจ้าหน้าที่มูลนิธิ:</strong> ปฏิเสธ เนื่องจาก<?php echo $rowlogin['basic_score_remark']; ?></p>
												</div>
											</div> <?php

										}elseif($rowlogin['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
											<div class="row BG-gray2 center">
												<div class="col-md-12">
													<img src="images/มูลนิธิครูดีของแผ่นดิน Logo Resize.png" alt="มูลนิธิครูดีของแผ่นดิน PureLogo">
													<p class="text-green1"><strong>สถานะเจ้าหน้าที่มูลนิธิ:</strong> ผ่านแล้ว <i class="fas fa-check-circle"></i></p>
												</div>
											</div> <?php
										}
									}
								}
							?>
						</div>
						<!-- ################################################################################################ -->
						<table class="table m-t-15" style="table-layout: auto; overflow-x: auto;">
							<thead>
								<tr>
									<th>ลำดับ</th>
									<th>วันที่</th>
									<th>เวลา</th>
									<th>รูปภาพ</th>
									<th>รายงาน</th>
									<?php
										if($basic_score4stu_system_id == 'A') {

										}elseif($basic_score4stu_system_id == 'B') { ?>
											<th>สถานะการตรวจ</th>
											<th class="BG-gold1">อัพเดทสถานะ</th> <?php
										}
									?>
								</tr>
							</thead>
							<tbody>
								<?php

									$sqlbasic_score4stu_and_login = "SELECT * FROM `basic_score4stu` INNER JOIN `login` ON basic_score4stu.ID=login.ID WHERE login.ID='$ID_stu' AND basic_score4stu.basic_score4stu_system_id='$basic_score4stu_system_id' ORDER BY basic_score4stu.basic_score4stu_file_date ASC ";
									$reBS4Snl = mysqli_query($con, $sqlbasic_score4stu_and_login);

									if(mysqli_num_rows($reBS4Snl) != 0) {

										$i=1;
										while($rowBS4Snl = mysqli_fetch_array($reBS4Snl)) { ?>
											<tr>
												<!-- No. -->
												<td class="price-pr">
													<p><?php echo $i; ?></p>
												</td>
												<!-- Do Good Date -->
												<td class="price-pr">
													<p><?php echo date("d-m-Y", strtotime($rowBS4Snl['basic_score4stu_file_date'])); ?></p>
												</td>
												<!--Do Good Time -->
												<td class="price-pr">
													<p><?php echo date("H:i", strtotime($rowBS4Snl['basic_score4stu_file_date'])); ?></p>
												</td>
												<!-- ไฟล์แนบ หรือรูปภาพเด็กทำความดี -->
												<td class="thumbnail-img">
													<a href="<?php echo $rowBS4Snl['basic_score4stu_file'];?>" target="_blank">
														<img src="<?php echo $rowBS4Snl['basic_score4stu_file'];?>" class="logo zoom108" style="width: 200px" alt="<?php echo $rowBS4Snl['basic_score4stu_file'];?>">
													</a>
												</td>
												<!-- รายงาน -->
												<td class="price-pr lh-1-7">
													<?php
														if(is_null($rowBS4Snl['basic_score4stu_text']) || empty($rowBS4Snl['basic_score4stu_text'])) { ?>
															<p class="text-red1">รอนร.เขียนรายงาน</p> <?php
														}else{ ?>
															<p><?php echo $rowBS4Snl['basic_score4stu_text']; ?></p> <?php
														}
													?>
												</td>
												<!-- สถานะการตรวจ -->
												<?php
													if($basic_score4stu_system_id == 'A') {

													}elseif($basic_score4stu_system_id == 'B') { ?>
														<td class="price-pr lh-1-7"> <?php
															if($rowBS4Snl['basic_score_status'] == 'ยืนยันแล้ว') {
																if(is_null($rowBS4Snl['basic_score4stu_check_status']) || empty($rowBS4Snl['basic_score4stu_check_status'])) { ?>
																	<p>-</p> <?php
																}elseif(strpos($rowBS4Snl['basic_score4stu_check_status'], 'ปฏิเสธ,') !== false) { ?>
																	<p class="text-red1"><?php echo str_replace(',', '<br>', $rowBS4Snl['basic_score4stu_check_status']); ?></p> <?php
																}elseif(strpos($rowBS4Snl['basic_score4stu_check_status'], 'อนุมัติ,') !== false) { ?>
																	<p class="text-green1"><?php echo str_replace(',', '<br>', $rowBS4Snl['basic_score4stu_check_status']); ?> <i class="fas fa-check-circle"></i></p> <?php
																}elseif(strpos($rowBS4Snl['basic_score4stu_check_status'], 'แก้ไข') !== false) { ?>
																	<p class="text-yellow2"><?php echo str_replace(',', '<br>', $rowBS4Snl['basic_score4stu_check_status']); ?></p> <?php
																}

															}else{
																if(is_null($rowBS4Snl['basic_score4stu_check_status']) || empty($rowBS4Snl['basic_score4stu_check_status'])) { ?>
																	<p>รอตรวจ</p> <?php
																}elseif(strpos($rowBS4Snl['basic_score4stu_check_status'], 'ปฏิเสธ,') !== false) { ?>
																	<p class="text-red1"><?php echo str_replace(',', '<br>', $rowBS4Snl['basic_score4stu_check_status']); ?></p> <?php
																}elseif(strpos($rowBS4Snl['basic_score4stu_check_status'], 'อนุมัติ,') !== false) { ?>
																	<p class="text-green1"><?php echo str_replace(',', '<br>', $rowBS4Snl['basic_score4stu_check_status']); ?> <i class="fas fa-check-circle"></i></p> <?php
																}elseif(strpos($rowBS4Snl['basic_score4stu_check_status'], 'แก้ไข') !== false) { ?>
																	<p class="text-yellow2"><?php echo str_replace(',', '<br>', $rowBS4Snl['basic_score4stu_check_status']); ?></p> <?php
																}
															} ?>
														</td> <?php
													}
												?>
												<!-- Update Status Button -->
												<?php
													if($rowBS4Snl['basic_score_status'] == 'ยืนยันแล้ว/กำลังตรวจสอบ') { ?>
														<td>
															<div class="up-stat-sty">
																<button class="up-stat-btn"><i class="fas fa-caret-down"></i></button>
																	<div class="dropdown_thispage">
																		<!-- ################################################################################################ -->
																		<!-- Trigger/Open The Modal -->
																		<a id="rejectBtn" class="reject-btn-sty">ปฏิเสธ</a>
																		<!-- ################################################################################################ -->
																		<a onclick="approveBtn('<?php echo $ID_stu; ?>', '<?php echo $rowBS4Snl['basic_score4stu_system_id']; ?>', '<?php echo $rowBS4Snl['basic_score4stu_file_no']; ?>', 
																			'<?php echo $rowBS4Snl['basic_score4stu_id'] ?>') "
																			class="approve-btn-sty">อนุมัติ
																		</a>
																		<!-- ################################################################################################ -->
																	</div>
																	<!-- The Reject Modal for  (Need to move out from dropdown_thispage class's child because if inside, the modal will get hover when move out but we want reject modal window stay in place) -->
																	<div id="rejectModal" class="modal">
																		<div class="modal-content">
																			<span class="close">&times;</span>
																			<p class="fs-18"><strong>เหตุผลในการปฏิเสธ</strong> (เหตุผลตรงนี้จะนำไปแสดงให้นักเรียนแก้ไขได้ถูกต้อง)</p>
																			<!-- ################################################################################################ -->
																			<form id="rejectMoalForm" action="rejectbasic_score4stu-ครู.php" method="POST">
																				<div class="center_text_input fs-18 m-b-20">
																					<input type="text" name="reject_reason" placeholder="เหตุผลในการปฏิเสธ" style="width: 70%; height: 40px; padding: 0 8px" required>
																				</div>

																				<button class="submitBtn btn BG-green1 fs-15" style="cursor: pointer; border: transparent;">ยืนยัน</button>
																				<button class="cancelBtn btn BG-red1 fs-15" type="reset" style="cursor: pointer; border: transparent;">ยกเลิก</button>

																				<input type="hidden" name="ID_stu" value="<?php echo $ID_stu; ?>">
																				<input type="hidden" name="basic_score4stu_system_id" value="<?php echo $rowBS4Snl['basic_score4stu_system_id']; ?>">
																				<input type="hidden" name="basic_score4stu_file_no" value="<?php echo $rowBS4Snl['basic_score4stu_file_no']; ?>">
																				<input id="winScroll_input" type="hidden" name="winScroll">
																				<input type="hidden" name="basic_score4stu_id" value="<?php echo $rowBS4Snl['basic_score4stu_id'] ?>">
																			</form>
																			<!-- ################################################################################################ -->
																		</div>
																	</div>
															</div>
														</td>
														<!-- JS for rejectModal box -->
														<script>
															// Get the rejectModal
															var rejectModal = document.querySelectorAll("#rejectModal");
															// Get the button that opens the rejectModal
															var btn = document.querySelectorAll("#rejectBtn");
															// Get the <span> element that closes the rejectModal
															var closeBtn = document.getElementsByClassName("close")[<?php echo $i-1; ?>];
															// Get the button that submit reason for rejectModal
															var cancelBtn = document.getElementsByClassName("cancelBtn")[<?php echo $i-1; ?>];
															// Get the button that submit reason for rejectModal
															var submitBtn = document.getElementsByClassName("submitBtn")[<?php echo $i-1; ?>];
															// Get the input winScroll
															var winScroll_input = document.querySelectorAll("#winScroll_input");
															// Get the dropdown_thispage
															// var dropdown_thispage = document.getElementsByClassName("dropdown_thispage")[<?php echo $i-1; ?>];
															// Get rejectModal form
															var rejectMoalForm = document.querySelectorAll("#rejectMoalForm");
															///////////////////////////////////////////////////////////////////////////////////////////////////////////
															// When the user clicks the button, open the rejectModal
															btn[<?php echo $i-1; ?>].onclick = function() {
																//dropdown_thispage[<?php echo $i-1; ?>].style.backgroundColor = "yellow";
																rejectModal[<?php echo $i-1; ?>].style.display = "block";
															}
															// When the user clicks on <closeBtn> (x), close the rejectModal
															closeBtn.onclick = function() {
																rejectModal[<?php echo $i-1; ?>].style.display = "none";
															}
															// When the user clicks on <cancelBtn>, close the rejectModal
															cancelBtn.onclick = function() {
																rejectModal[<?php echo $i-1; ?>].style.display = "none";
															}
															// When the user clicks on ยืนยัน button on rejectModal form
															submitBtn.onmouseover = function() {
																var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
																winScroll_input[<?php echo $i-1; ?>].value = winScroll;
															}
															// When the user clicks on อนุมัติ button
															function approveBtn(a, b, c, d) {
																var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
																window.location.href = "approvebasic_score4stu-ครู.php?winScroll="+winScroll + "&ID_stu="+a + 
																"&basic_score4stu_system_id="+b + "&basic_score4stu_file_no="+c + "&basic_score4stu_id="+d;
															}
														</script> <?php

													}elseif($rowBS4Snl['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ' || $rowBS4Snl['basic_score_status']  == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ' || $rowBS4Snl['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') {
														
														if($basic_score4stu_system_id == 'A') {

														}elseif($basic_score4stu_system_id == 'B') { ?>
															<td class="price-pr">
																<p>-</p>
															</td> <?php
														}
														
													}elseif($rowBS4Snl['basic_score_status'] == 'ยืนยันแล้ว') {
														
														if($basic_score4stu_system_id == 'A') {

														}elseif($basic_score4stu_system_id == 'B') { ?>
															<td class="price-pr">
																<p>รอนักเรียน<br>กดปุ่มให้ตรวจ</p>
															</td> <?php
														}
													}
												?>
											</tr> <?php
											$i++;
										}

									}else{
										
										if($basic_score4stu_system_id == 'A') { ?>
												</tbody>
											</table>
												<div class="no-order">
													<p>ยังไม่พบว่ามีการแนบไฟล์ต้นกล้าแห่งความดีเข้ามาในระบบ</p>
												</div> <?php
										}elseif($basic_score4stu_system_id == 'B') { ?>
												</tbody>
											</table>
												<div class="no-order">
													<p>ยังไม่พบว่านักเรียนมีการแนบไฟล์ทำความดีเข้ามาในระบบ</p>
												</div> <?php
										}
									}
								?>
							</tbody>
						</table> <?php
						if($basic_score4stu_system_id == 'A') { ?>
							<div class="no-order">
								<p>เนื่องจากเป็นระบบการส่งงานแบบต้นกล้าแห่งความดี คุณครูสามารถแนบไฟล์แทนได้ค่ะ</p>
								<form action="registZidol-ครู.php" method="POST" target="_blank">
									<button type="submit" class="btn hov-pointer" style="border: none">เข้าสู่ระบบแนบไฟล์แทนนักเรียน</button>

									<input type="hidden" name="CFP" value="รายละเอียดนักเรียนทำความดี-ครู">
									<input type="hidden" name="ID_stu" value="<?php echo $ID_stu; ?>">
									<input type="hidden" name="basic_score4stu_system_id" value="<?php echo $basic_score4stu_system_id; ?>">
								</form>
								<br><br>
							</div> <?php
						} ?>
						<!-- ################################################################################################ -->
					</div>
				</div>
			</div>
			<!-- End table of my product detail -->
		</div>
	</div>
	<!-- End Cart -->
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>


<!-- The Reject Modal for devmain & devsub (Need to move out from dropdown_thispage class's child because if inside, the modal will get hover when move out but we want reject modal window stay in place) -->
<div id="rejectModal4devtopic" class="modal">
	<div class="modal-content">
		<span id="close4devtopic">&times;</span>
		<p class="fs-18 text-black center"><strong>เหตุผลในการปฏิเสธ</strong> (เหตุผลตรงนี้จะนำไปแสดงให้นักเรียนแก้ไขได้ถูกต้อง)</p>
		<!-- ################################################################################################ -->
		<form id="rejectMoalForm4devtopic" action="rejectbasic_score4stu4devtopic-ครู.php" method="POST">
			<div class="center_text_input fs-18 m-b-20">
				<input type="text" name="reject_reason" placeholder="เหตุผลในการปฏิเสธ" style="width: 70%; height: 40px; padding: 0 8px" required>
			</div>

			<div class="center">
				<button id="submitBtn4devtopic" class="btn BG-green1 fs-15" style="cursor: pointer; border: transparent;">ยืนยัน</button>
				<button id="cancelBtn4devtopic" class="btn BG-red1 fs-15" type="reset" style="cursor: pointer; border: transparent;">ยกเลิก</button>
			</div>

			<input type="hidden" name="devsub_name" value="<?php echo $devsub_name; ?>">
			<input type="hidden" name="basic_score4stu_system_id" value="<?php echo $basic_score4stu_system_id; ?>">
			<input type="hidden" name="ID_stu" value="<?php echo $ID_stu; ?>">
			<input id="winScroll_input4devtopic" type="hidden" name="winScroll">
		</form>
		<!-- ################################################################################################ -->
	</div>
</div>
<script>
	// Get the rejectModal
	var rejectModal4devtopic = document.getElementById("rejectModal4devtopic");
	// Get the button that opens the rejectModal
	var rejectBtn4devtopic = document.getElementById("rejectBtn4devtopic");
	// Get the <span> element that closes the rejectModal
	var closeBtn4devtopic = document.getElementById("close4devtopic");
	// Get the button that submit reason for rejectModal
	var cancelBtn4devtopic = document.getElementById("cancelBtn4devtopic");
	// Get the button that submit reason for rejectModal
	var submitBtn4devtopic = document.getElementById("submitBtn4devtopic");
	// Get the input winScroll
	var winScroll_input4devtopic = document.getElementById("winScroll_input4devtopic");
	// Get the dropdown_thispage
	// var dropdown_thispage = document.getElementsByClassName("dropdown_thispage");
	// Get rejectModal form
	var rejectMoalForm4devtopic = document.getElementById("rejectMoalForm4devtopic");
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	// When the user clicks the button, open the rejectModal
	rejectBtn4devtopic.onclick = function() {
		rejectModal4devtopic.style.display = "block";
	}
	// When the user clicks on <closeBtn> (x), close the rejectModal
	closeBtn4devtopic.onclick = function() {
		rejectModal4devtopic.style.display = "none";
	}
	// When the user clicks on <cancelBtn>, close the rejectModal
	cancelBtn4devtopic.onclick = function() {
		rejectModal4devtopic.style.display = "none";
	}
	// When the user clicks on ยืนยัน button on rejectModal form
	submitBtn4devtopic.onmouseover = function() {
		var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
		winScroll_input4devtopic.value = winScroll;
	}
	// When the user clicks on อนุมัติ button
	function approveBtn4devtopic(x, y, z, a) {
		var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
		window.location.href = "approvebasic_score4stu4devtopic-ครู.php?winScroll="+winScroll + "&ID_stu="+x + "&new_devsub="+y + "&basic_score_ans="+z + "&basic_score4stu_system_id="+a;
	}
</script>

</body>
</html>