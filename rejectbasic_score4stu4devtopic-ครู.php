<?php
	session_start();
	require_once('condb.php');
	date_default_timezone_set("Asia/Bangkok");
	$now_date = date("d-m-Y H:i:s");

	$ID = $_SESSION['ID']; // teacher ID
	$ID_stu = $_POST['ID_stu']; // student ID

	/* Set all value */
	$winScroll = $_POST['winScroll'];
	$reject_reason = $_POST['reject_reason'];
	$devsub_name = $_POST['devsub_name'];

	$basic_score4stu_system_id = 'C'; // Set basic_score4stu_system_id to save to database
	$basic_score4stu_text = $devsub_name;
	$basic_score4stu_file_date = date("Y-m-d H:i:s");
	$basic_score4stu_check_status = 'ปฏิเสธ,'.$reject_reason.','.$now_date;

	/* Check ว่ามี basic_score4stu_id แล้วหรือไม่ ถ้ามีแล้ว ให้ Update แต่ถ้ายังให้ INSERT */
	$sqlbasic_score4stu_check_C = "SELECT * FROM `basic_score4stu` WHERE ID='$ID_stu' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
	$reBS4SCC = $con->query($sqlbasic_score4stu_check_C) or die($con->error); //Check error

	if(mysqli_num_rows($reBS4SCC) == 0) { // first time click to update devtopic
		$sql = "INSERT INTO `basic_score4stu` (ID, basic_score4stu_system_id, basic_score4stu_file_date, basic_score4stu_text, basic_score4stu_check_status) VALUES ('$ID_stu', '$basic_score4stu_system_id', '$basic_score4stu_text', '$basic_score4stu_file_date', '$basic_score4stu_check_status') ";
		$res= $con->query($sql) or die($con->error); //Check error
	}else{
		$sql = "UPDATE `basic_score4stu` SET `basic_score4stu_file_date`='$basic_score4stu_file_date', `basic_score4stu_text`='$basic_score4stu_text', `basic_score4stu_check_status`='$basic_score4stu_check_status' WHERE ID='$ID_stu' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
		$res= $con->query($sql) or die($con->error); //Check error
	}

	/* Log User Action */
	$scorelog_task = 'ตรวจความดีเด็กพื้นฐาน,C';
	$scorelog_detail = 'ครู,ชั้น5,'.$basic_score4stu_check_status;
	$scorelog_total = $ID_stu; // ID of student who do Good

	$sqllog="INSERT INTO `scorelog` (ID, scorelog_task, scorelog_detail, scorelog_total) 
	VALUES ('$ID', '$scorelog_task', '$scorelog_detail', '$scorelog_total') ";
	$relog = $con->query($sqllog) or die($con->error); //Check error




	$system_id = $_POST['basic_score4stu_system_id']; // Set system_id to pass valuable to another page
	header('location: รายละเอียดนักเรียนทำความดี-ครู.php?winScroll='.$winScroll.'&ID_stu='.$ID_stu.'&system_id='.$system_id);
?>