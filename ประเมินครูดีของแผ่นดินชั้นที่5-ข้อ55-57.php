<?php 
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	$question_num = 57; // NOTE: This page is special case. 3 questions in 1 pages
	require('includes/header4fifth_evaluation.php');
?>

<!DOCTYPE html>
<html lang="thai">

<head>
  <title>มูลนิธิครูดีของแผ่นดิน</title>
  <link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Header -->
<?php include('includes/headerTop.php'); ?>
									<ul>
                    <li><a href="โครงการเด็กดีของแผ่นดิน-en.php">English</a></li>
                    <!--<li><a href="#">Chinese</a></li>-->
                  </ul>
<?php include('includes/headerBottom4Joinus.php'); ?>
<!-- End Header -->

<div class="m-t-90"></div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Content 00 - ประเมินครูดีของแผ่นดินชั้นที่5 -->
<div class="wrapper row3">
	<div class="hoc container clear">
		<!-- ################################################################################################ -->
		<div class="center">
			<h8 class="m-b-50">หมวด: ครองงาน</h8>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-6 col-md-6 col-lg-6 fs-20">
				<p>ข้อที่ / ทั้งหมด</p>
				<p class="bold">55-57 / 79</p>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 right fs-20">
				<p>เหลือเวลา</p>
				<div>
					<p class="bold m-t-1" id="timer" onload="localStorage.getItem('currenttime')">.</p> วินาที
				</div>
			</div>
		</div>

		<div class="row p-t-20 p-l-10 m-t-20" style="border-top:2px solid rgb(16,192,0); border-radius:50px;">
			<div class="col-md-12 col-lg-12">
				<form action="addfifth_score_A-ครู.php" method="GET">
					<div class="fs-20 inline" style="text-align:left;">
						<p>
							อ่านข้อความต่อไปนี้ แล้วตอบคำถามข้อ 55-57
							<br><br>
							“ผู้เรียนชั้นมัธยมศึกษาจำนวน 200 คน อาสาสมัครไปประชุมสัมมนาในวันหยุดสุดสัปดาห์ที่จังหวัดอยุธยา  หัวข้อของการประชุมคือ มวลมนุษยชาติและวิถีทางนำไปสู่สันติสุขของโลก ซึ่งเป็นปัญหาที่ผู้เรียนได้เลือกขึ้นมา เนื่องจากเป็นปัญหาที่สำคัญในปัจจุบัน”
						</p>
						<p><strong>55. ผู้เรียนกลุ่มนี้มีความสนใจต่อปัญหาทางสังคมอย่างจริงจังมากกว่าผู้เรียนมัธยมศึกษาโดยทั่วไป” ท่านคิดว่าจริงหรือไม่</strong></p>
						<br>
						<input type="radio" id="choice1" name="fifth_score_A_55" value="0" required>
						<label for="choice1">จริง</label>
						<br>
						<input type="radio" id="choice2" name="fifth_score_A_55" value="5" required>
						<label for="choice2">น่าจะเป็นจริง</label>
						<br>
						<input type="radio" id="choice3" name="fifth_score_A_55" value="0" required>
						<label for="choice3">ข้อมูลยังไม่เพียงพอ</label>
						<br>
						<input type="radio" id="choice4" name="fifth_score_A_55" value="0" required>
						<label for="choice4">น่าจะไม่จริง</label>
						<br>
						<input type="radio" id="choice5" name="fifth_score_A_55" value="0" required>
						<label for="choice5">ไม่จริง</label>
						<br>
						
						<p><strong>56. ผู้เรียนส่วนใหญ่กลุ่มนี้ไม่เคยอภิปรายในหัวข้อประชุมครั้งนี้ในโรงเรียนมาก่อน” ท่านคิดว่าจริงหรือไม่</strong></p>
						<br>
						<input type="radio" id="choice1" name="fifth_score_A_56" value="0" required>
						<label for="choice1">จริง</label>
						<br>
						<input type="radio" id="choice2" name="fifth_score_A_56" value="0" required>
						<label for="choice2">น่าจะเป็นจริง</label>
						<br>
						<input type="radio" id="choice3" name="fifth_score_A_56" value="0" required>
						<label for="choice3">ข้อมูลยังไม่เพียงพอ</label>
						<br>
						<input type="radio" id="choice4" name="fifth_score_A_56" value="5" required>
						<label for="choice4">น่าจะไม่จริง</label>
						<br>
						<input type="radio" id="choice5" name="fifth_score_A_56" value="0" required>
						<label for="choice5">ไม่จริง</label>
						<br>
						
						<p><strong>57. “ผู้เรียนกลุ่มนี้มาจากทุกภาคของประเทศ” ท่านคิดว่าจริงหรือไม่</strong></p>
						<br>
						<input type="radio" id="choice1" name="fifth_score_A_57" value="0" required>
						<label for="choice1">จริง</label>
						<br>
						<input type="radio" id="choice2" name="fifth_score_A_57" value="0" required>
						<label for="choice2">น่าจะเป็นจริง</label>
						<br>
						<input type="radio" id="choice3" name="fifth_score_A_57" value="5" required>
						<label for="choice3">ข้อมูลยังไม่เพียงพอ</label>
						<br>
						<input type="radio" id="choice4" name="fifth_score_A_57" value="0" required>
						<label for="choice4">น่าจะไม่จริง</label>
						<br>
						<input type="radio" id="choice5" name="fifth_score_A_57" value="0" required>
						<label for="choice5">ไม่จริง</label>
						<br>
					</div>
					
					<button type="submit" id="nextBtn" class="btnJoin" style="color:white; cursor:pointer; width:80%; margin-top:50px;"><h7>ข้อถัดไป</h7></button>
					<input type="hidden" id="CFP" name="CFP" value="57">
				</form>
			</div>
		</div>
	</div>
	<!-- ################################################################################################ -->
</div>
<!-- End Content 00 - ประเมินครูดีของแผ่นดินชั้นที่5 -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Start Footer&Copyright -->
<?php include('includes/footer.php'); ?>
<!-- End Footer&Copyright -->

<!-- Javascript -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>
<!-- Countdown Timer -->
<script src="js/countdowntimer180.js"></script>

</body>
</html>