<?php
	session_start();
	require_once('condb.php');
	$ID = $_SESSION['ID'];

	if(isset($ID)) {
		if($_POST['CFP'] == 'รายละเอียดนักเรียนทำความดี-ครู' || $_GET['CFP'] == 'แนบหลักฐานประกอบการพิจารณาแทนนักเรียน-ครู') { // เข้าหน้านี้ได้เฉพาะที่มาจากหน้า รายละเอียดนักเรียนทำความดี-ครู.php เท่านั้น

			if($_POST['basic_score4stu_system_id'] == 'A' || $_GET['basic_score4stu_system_id'] == 'A') { // เข้าหน้านี้ได้เฉพาะที่มาจากหน้า ครูชั้น5 ที่จะเข้ามาแนบไฟล์แทนนักเรียนต้นกล้า เท่านั้น

				if(isset($_POST['ID_stu'])) {
					$ID_stu = $_POST['ID_stu'];
				}else{
					$ID_stu = $_GET['ID_stu'];
				}
				$affil_code = $_SESSION['affil_code'];

			}else{
				echo "<script>window.history.go(-1)</script>";
			}
		}else{
			echo "<script>window.history.go(-1)</script>";
		}

	}else{
		echo '<script>';
			echo "alert('กรุณาเข้าสู่ระบบค่ะ');";
			echo "window.location.replace('login.php')";
		echo '</script>';
	}
?>

<!DOCTYPE html>
<html lang="thai">
<head>
	<title>มูลนิธิครูดีของแผ่นดิน</title>
	<link rel="icon" href="images/มูลนิธิครูดีของแผ่นดิน PureLogo.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="layout/styles/all.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="layout/styles/layout.css">
</head>


<body>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- ################################################################################################ -->
				<div class="textlink3 PreMenu_fl_left">
					<a href="ระบบติดตามสถานะนักเรียนขั้นพื้นฐาน-ครู.php"><i class="fas fa-arrow-left"></i> กลับไปหน้าระบบติดตามสถานะนักเรียนขั้นพื้นฐาน</a>
				</div>
				<!-- ################################################################################################ -->
				<div class="login100-pic js-tilt">
					<img src="images/เด็กดีของแผ่นดิน Logo.jpg" style="width:70%" alt="มูลนิธิครูดีของแผ่นดิน Logo">
				</div>
				<!-- ################################################################################################ -->
				<form class="login100-form validate-form" action="#" enctype="multipart/form-data">
					<!-- ################################################################################################ -->
					<span class="login100-form-title bold">
						ระบบแนบไฟล์แทนนักเรียน (ต้นกล้าแห่งความดี)
					</span>
					<!-- ################################################################################################ -->
					<?php
						$sqlstu = "SELECT * FROM `login` WHERE ID='$ID_stu' ";
						$restu = mysqli_query($con, $sqlstu);
						$rowstu = mysqli_fetch_array($restu);

						$stu_name = $rowstu['firstname'].' '.$rowstu['lastname'];
						$stu_email = $rowstu['email'];
						$teacher_name = $rowstu['basic_score_text'];

						/* Set system_id */
						$stu_basic_score_ans = $rowstu['basic_score_ans'];
						$stu_basic_score4stu_system_id = substr($stu_basic_score_ans, 0, 1);
					?>
					<label for="student" id="stydenthead" class="center m-t-10 text-black"><strong>ชื่อนักเรียน:</strong> <?php echo $stu_name; ?></label>
					<label for="teacher_id" id="teacherhead" class="center text-black"><strong>ชื่อคุณครูที่ปรึกษา:</strong> <?php echo $teacher_name.' ('.$_SESSION['email'].')'; ?></label>
				</form>
				<!-- ################################################################################################ -->
				<div class="container-login100-form-btn m-t-20">
					<form action="แนบหลักฐานประกอบการพิจารณาแทนนักเรียน-ครู.php" method="POST" target="_blank">
						<button type="submit" class="btn2" style="margin:0 5px 10px 5px;">แนบหลักฐานประกอบการพิจารณา</button>

						<input type="hidden" name="ID_stu" value="<?php echo $ID_stu; ?>">
					</form>
					<form action="แนบไฟล์ต้นกล้าแทนนักเรียน-ครู.php" method="POST" target="_blank">
						<button type="submit" class="btn2" style="margin:0 5px 10px 5px;">แนบไฟล์ต้นกล้าแห่งความดี</button>

						<input type="hidden" name="ID_stu" value="<?php echo $ID_stu; ?>">
					</form>
				</div>
				<?php
					if($stu_basic_score4stu_system_id == 'A') {
						// NOTE: Check ว่ามีการแนบไฟล์ต้นกล้าแห่งความดีมารึยัง ถ้าใช่ ให้ทำการขึ้น Content ตาม basic_score_status
						$sqlcheck_A = "SELECT `ID` FROM `basic_score4stu` WHERE ID='$ID_stu' AND basic_score4stu_system_id='$stu_basic_score4stu_system_id' ";
						$recheck_A = mysqli_query($con, $sqlcheck_A);

						if(mysqli_num_rows($recheck_A) != 0) {

							if($rowstu['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
								<div class="login100-form-title fs-30 lh-1-1">
									<p><strong>สถานะการตรวจ:</strong> <?php echo $stu_name.' ผ่านโครงการเด็กดีขั้นพื้นฐานแล้วค่ะ'; ?> </p>
								</div> <?php

							}elseif($rowstu['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ') { ?>
								<div class="login100-form-title fs-30 lh-1-1">
									<p><strong>สถานะการตรวจ:</strong> กำลังตรวจสอบ</p>
								</div> <?php

							}elseif($rowstu['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ') { ?>
								<div class="login100-form-title lh-2-0">
									<p>หลังจากได้ทำการแก้ไขเรียบร้อยแล้วให้ทำการส่งเพื่อให้ตรวจสอบอีกครั้ง</p>
									<a href="cf_basic_score-เด็ก.php?basic_score_status=ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ&basic_score_remark=<?php echo $rowstu['basic_score_remark'] ?>" class="btn2" style="margin:0 5px">กดเพื่อให้ตรวจสอบอีกครั้ง</a>
									<p style="color: rgb(231, 59, 59)"><strong>สถานะการตรวจ:</strong> ปฏิเสธ เนื่องจาก<?php echo $rowstu['basic_score_remark'] ?></p>
								</div> <?php
							}


						}



					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

					/* หน้านี้ยังไม่ได้ใช้ basic_score4stu_system_id == 'B' เพราะเขียนโปรแกรมให้ตาม Requirement ของมูลนิธิ ให้ครูเข้ามาได้เฉพาะระบบต้นกล้าเท่านั้น */
					}elseif($basic_score4stu_system_id == 'B') {
						// NOTE: Check ว่ามีการแนบไฟล์ทำความดีมา14ไฟล์รึยัง ถ้าใช่ ให้ทำการขึ้น Content ตาม basic_score_status
						$sqlcountrow = "SELECT count(*) AS countrow FROM `basic_score4stu` WHERE ID='$ID_stu' AND basic_score4stu_system_id='$basic_score4stu_system_id' ";
						$recountrow = mysqli_query($con,$sqlcountrow);
						$rowcountrow = mysqli_fetch_array($recountrow);

						if($rowcountrow['countrow'] >= 14) {

							if(isset($rowstudent['basic_score_total'])) { // ถ้าเด็กมีครูที่ปรึกษา

								if($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p><strong>สถานะการตรวจ:</strong> เก่งมากเลยค่ะ ^^ ผ่านโครงการเด็กดีของแผ่นดินขั้นพื้นฐานแล้วนะคะ</p>
									</div> <?php
								
								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว') { // กรณีเด็กทำการยืนยันชื่อ-นามสกุล ตำแหน่ง และสังกัดตัวเองเรียบร้อยแล้ว ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p>เนื่องจากคุณส่งไฟล์ทำความดีครบ 14 ไฟล์ กดปุ่มด้านล่างเพื่อให้ครูที่ปรึกษาตรวจสอบเพื่อรับเกียรติบัตรขั้นพื้นฐานค่ะ</p>
										<a href="cf_basic_score-เด็ก.php" class="btn2" style="margin:0 5px">กดเพื่อให้ครูที่ปรึกษาตรวจสอบ</a>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/กำลังตรวจสอบ' || $rowstudent['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p><strong>สถานะการตรวจ:</strong> กำลังตรวจสอบ</p>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ') { ?>
									<div class="login100-form-title lh-2-0">
										<p>หลังจากได้ทำการแก้ไขเรียบร้อยแล้วให้ทำการส่งเพื่อให้ตรวจสอบอีกครั้ง</p>
										<a href="cf_basic_score-เด็ก.php?basic_score_status=ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ&basic_score_remark=<?php echo $rowstudent['basic_score_remark'] ?>" class="btn2" style="margin:0 5px">กดเพื่อให้ตรวจสอบอีกครั้ง</a>
										<p style="color: rgb(231, 59, 59)"><strong>สถานะการตรวจ:</strong> ปฏิเสธ เนื่องจาก<?php echo $rowstudent['basic_score_remark'] ?></p>
									</div> <?php
								}

							}else{ // ถ้าเด็กไม่มีครูที่ปรึกษา

								if($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/Approve แล้ว') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p><strong>สถานะการตรวจ:</strong> เก่งมากเลยค่ะ ^^ ผ่านโครงการเด็กดีของแผ่นดินขั้นพื้นฐานแล้วนะคะ</p>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p>เนื่องจากคุณส่งไฟล์ทำความดีครบ 14 ไฟล์ กดปุ่มด้านล่างเพื่อให้เจ้าหน้าที่มูลนิธืตรวจสอบเพื่อรับเกียรติบัตรขั้นพื้นฐานค่ะ</p>
										<a href="cf_basic_score-เด็ก.php" class="btn2" style="margin:0 5px">กดเพื่อให้เจ้าหน้าที่ตรวจสอบ</a>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/กำลังตรวจสอบ' || $rowstudent['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ') { ?>
									<div class="login100-form-title fs-30 lh-1-1">
										<p><strong>สถานะการตรวจ:</strong> กำลังตรวจสอบ</p>
									</div> <?php

								}elseif($rowstudent['basic_score_status'] == 'ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ') { ?>
									<div class="login100-form-title lh-2-0">
										<p>หลังจากได้ทำการแก้ไขเรียบร้อยแล้วให้ทำการส่งเพื่อให้ตรวจสอบอีกครั้ง</p>
										<a href="cf_basic_score-เด็ก.php?basic_score_status=ยืนยันแล้ว/ส่งให้มูลนิธิตรวจสอบ/ปฏิเสธ&basic_score_remark=<?php echo $rowstudent['basic_score_remark'] ?>" class="btn2" style="margin:0 5px">กดเพื่อให้ตรวจสอบอีกครั้ง</a>
										<p style="color: rgb(231, 59, 59)"><strong>สถานะการตรวจ:</strong> ปฏิเสธ เนื่องจาก<?php echo $rowstudent['basic_score_remark'] ?></p>
									</div> <?php
								}
							}
						}

					}
					
				?>
				<!-- ################################################################################################ -->
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>